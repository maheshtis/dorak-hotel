<?php

/*
 * @data 15-mar-2016
 * @author tis
 * @functions:

  getHotelChain()            this method is used to fetch hotel chain from DB.
  addHotelChain()            this method is used to add hotel chain name into database.
  getHotelChainList()        this method is used to fetch hotel chain list from database.
  delHotelChain()            this method is used to delete hotel chain from database.
  updateHotelChain()  	    this method is used to update hotel chain entity in database .
  addHotelPurpose()          this method is used to add hotel purpose attributes into entity_attributes table .
  getHotelPurpose ()         this method is used to fetch hotel purpose entity from entity_attributes table.
  getHotelPurposeList()       this method is used to fetch hotel purpose +list name from entity_attributes table .
  delHotelPurpose ()          this method is used to delete hotel purpose name from entity_attributes table
  updateHotelPurpose()       this method is used to update hotel purpose entity into entity_attributes table.
  addHotelPropertyType()     this method is used to add hotel property name into entity_attributes table.
  getHotelPropertyType()     this method is used to fetch hotel property name from entity_attributes table
  getHotelPropertyTypeList()  this method is used to return hotel property list from entity_attributes table.
  delHotelPropertyType()      this method is used to delete hotel property type list from entity_attributes table.
  updateHotelPropertyType()   this method is used to update hotel property from entity_attributes table
  addHotelFacility()         this method is used to update hotel property in entity_attributes table.
  getHotelFacility ()        this method is used to fetch hotel facility from entity_attributes table.
  getHotelFacilityList()     this method is used to get hotel facility list from entity_attributes table
  delHotelFacility()        this method is used to delete hotel facility from entity_attributes table
  updateHotelFacility()     this method is used to  update hotel facility inside entity_attributes table
  addRoomFacility()         this method is used to add room facility in to entity_attributes table.
  getRoomFacility()         this method is used to fetch room facility details from DB.
  getRoomFacilityList()     this method is used to  fetch hotel room facility list from entity_attributes table
  delRoomFacility ()        this method is used to delete hotel room facility from entity_attributes table
  updateRoomFacility()      this method is used to  update hotel room facility inside entity_attributes table
  addCurrency ()            this method is used to add currency name into currency table.
  getCurrency   ()          this method is used to fetch currency name from currency table.
  getCurrencyList  ()        this method is used to delete currency  from currencies table.
  deleteCurencyCodesData ()  this method is used to delete currency records  from currency table.
  updateCurrency ()          this method is used to update currency inside currencies table.
  importCurencyData()        this method is used to import currencies into currencies table.
  addCurencyData()           this method is used to add country  into countries table
  getCountry ()              this method is used to fetch country name from country  table DB.
  checkCountryExists ()      this method is used to check if country exists or not using country id and name in country  table.
  checkCountryCodeExists()   this method is used to validate existing country using country code and id inside country  table.
  getCountryCode ()          this method is used to fetch country code name from database
  getCountryName()           this method is used to fetch country  name from country table.
  getCountriesList  ()       this method is used to fetch country list name from countries table.
  delCountry()             this method is used to delete countries from countries table
  updateCountry ()           this method is used to update country record in countries table.
  addCity ()                  this method is used to add country city name in country_cities table.
  getCity()                 this method is used to fetch city name from country_cities table
  checkCityExists ()          this method is used to check if city exista or not in country_cities table.
  checkCityCodeExists()      this method is used to check if city exists or not using city code,city id and country id in country_cities table
  addDistrict ()               this method is used to add districts into table
  getDistrict()                  this method is used to fetch district name from city_districts table
  checkDistrictExists()         this method is used to get district id from city_districts table
  getCityDistricts()            this method is used for district list from city_districts table
  getDistrictList()            this method is used for district list from city_districts table using country id and city id
  delDistrict()                    this method is used to  delete district chain from city_districts table
  delCityDistricts()               this method is used to  delete city from city_districts table
  delCountryCityDistricts()         this method is used to  delete country from city_districts table
  updateDistrict()                this method is used to update district name inside city_districts table
  addMarket()                       this method is used to add market name in entity_attributes table
  getMarket()                     this method is used to fetch market list component from entity_attributes table.
  delMarket()                      this method is used to delete market from entity_attributes table
  updateMarket()                 this method is used to update market function inside entity_attributes table.
  addTravelType()                   this method is used to add market entity into entity_attributes table
  getTravelType()                    this method is used to fetch travel type record from DB.
  getTravelTypeList()              this method is used to fetch travel details from entity_attributes table.
  delTravelType()                 this method is used to delete travel type entity from entity_attributes table
  updateTravelType()               this method is used to update travel name entity inside entity_attributes table.
  addRoomType()                      this method used is used to fetch room type title from entity_attributes table
  getRoomTypesList()               this method is used  to fetch room type title list from entity_attributes table
  delRoomType()                    this method is used to delete room type from entity_attributes table
  updateRoomType()                  this method is used to update room type inside entity_attributes table
  addMealPlan()                      this method is used to add meal plan value into entity_attributes table
  getMealPlan()                     this method is used to fetch meal plan from entity_attributes table
  getMealPlansList()                this method is used to fetch meal plan list into entity_attributes table.
  delMealPlan()                    this method is used to delete meal plan from entity_attributes table
  updateMealPlan()              this method is used to update meal plan inside entity_attributes table
  addRole()                     this method is used to add role into role table DB.
  getRole()                     this method is used to fetch role from role table value
  getRolesList()              this method used to get role list from role table
  delRole()                   this method is used to delete role from role table
  updateRole()                 this method is used to update role inside role table.
  addPosition()                  this method is used to update role inside role table.
  getPosition()                 this method is used to fetch position from entity_attributes
  delPosition()              this method is used to delete position from entity_attributes
  updatePosition()              this method is used to update position in entity_attributes.
  addStarRating()              this method is used to add star rating into entity_attributes
  getStarRating()               this method is used to fetch star ratings from entity_attributes.
  getStarRatingList()             this method is used to fetch star rating from entity_attributes
  delStarRating()                  this method is used to del star rating from entity_attributes
  updateStarRating()             this method is used to add dorak rating into entity_attributes
  getDorakRating ()                  this method is used to fetch dorak rating from entity_attributes
  getDorakRatingList()             this method is used to fetch dorak rating from entity_attributes
  delDorakRating()                 this method is used to delete dorak rating from entity_attributes.
  updateDorakRating()               this method is used to update dorak rating in entity_attributes.
  addStatus()                       this method is used to add status into entity_attributes.
  getStatus()                      this method is used to fetch status from entity_attributes.
  getStatusList()                    this method is used to fetch status list from entity_attributes.
  delStatus()                         this method is used to delete status from entity_attributes.
  updateStatus()                       this method is used to update status in DB.
  addDepartment()                      this method is used to add department into entity_attributes.
  getDepartment()                      this method is used to fetch department entity from entity_attributes.
  getDepartmentsList()                    this method is used to fetch department list from entity_attributes.
  delDepartment()                       this method is used to delete department entity from entity_attributes.
  updateDepartment()                  this method is used to update department entity in entity_attributes table.
  addCancellationDuration()          this method is used to add cancellation duration time into entity_attributes table.
  getCancellationDuration()           this method is used to fetch cancel duration value from entity_attributes table.
  getCancellationDurationList()             this method is used to get cancellation list from entity_attributes table.
  delCancellationDuration()                this method is used to delete cancellation duration from entity_attributes table.
  updateCancellationDuration()                    this method is used to update cancellation record in entity_attributes table.
  addPaymentDuration()                this method is used to add payment info in entity_attributes table.
  getPaymentDuration()                   this method is used to fetch payment value from entity_attributes table.
  getPaymentDurationList()                      this method is used to fetch payment duration entity from entity_attributes table.
  delPaymentDuration()                      this method is used to delete payment duration entity from entity_attributes table.
  updatePaymentDuration()                  this method is used to update payment duration in entity_attributes table.
  addAgeGroup()                        this method is used to add age group into entity_attributes table.
  getAgeGroup()                        this method is used to fetch age group entity from entity_attributes table.
  getAgeGroupList()                     this method is used to fetch age group list from entity_attributes table.
  delAgeGroup()                              this method is used to delete age group title from  entity_attributes table.
  updateAgeGroup()                        this method is used to update age group title in entity_attributes table.
  addCompany()                              this method is used to insert company's name into company's table.
  addCompanyMarkets()                      this method is used to insert company market data in company_market's table.
  getCompanyMarkets()                    this method is used to fetch company market id from company_market's table.
  getCompany()                         this method is used to fetch company's name  from company  table record.
  getCompanyDetails()                     this method is used to fetch company's details as per query.
  getCompanyLogo()                         this method is used to fetch company logo from company's table in DB.
  getCompanyList()                       this method used to fetch company name and id from database.
  delCompany()                       this method is used to  delete company entity from database.
  delCompanyMarkets()               this method is used to  delete company market entity from company_markets table.
  delLoction_point()                    this method is used to  delete location  from hotel_location_points table
  updateLoction()                                this method is used to update hotel location point in hotel_location_points  table.
  checkLoctionExists()                        this method is used to verify whether hotel location point exists or not in hotel_location_points table.
  addLoctionPoint()                           this method is used to add location point in hotel_location_points table.
  get_location_point()                        this method fetches hotel location using country code and city code.
 * @description This model  is for  add/ edit delete hotels setting related stuff.
 */

class Setting_model extends AdminModel {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * @method getHotelChain() method used to fetch hotel chain result.
     * @param varchar $title The Hotel Chain title - name of hotel chain.
     * @param int $chain_id The chain id - hotel chain is fetched using chain id.
     * @todo this method is used to fetch hotel chain from DB.
     * @return last inserted id if condition is true else will return false.
     */
    function getHotelChain($title, $chain_id = '') {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where('entity_type', $this->config->item('attribute_hotel_chain'));
        if ($chain_id != "")
            $this->db->where("id !=", $chain_id);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }

    /**
     * @method addHotelChain() method used to insert hotel chain entity.
     * @param int $chain_id The chain id - hotel chain added using chain id.
     * @todo this method is used to add hotel chain name into database.
     * @return last inserted id is the output if condition is true else will return false.
     */
    function addHotelChain($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getHotelChainList() method to fetch hote chain list.
     * @todo this method is used to fetch hotel chain list from database.
     * @return output is hotel chain list in the form of array.
     */
    function getHotelChainList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where('entity_type', $this->config->item('attribute_hotel_chain'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method delHotelChain() method used to delete hotel chain.
     * @param int $chainId The id of the hotel chain  which needs to be deleted
     * @todo this method is used to delete hotel chain from database.
     * @return output deletes hotel chain through chain id.
     */
    function delHotelChain($chainId) {
        $this->db->where('id', $chainId);
        $this->db->where('entity_type', $this->config->item('attribute_hotel_chain'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * @method updateHotelChain() method used to update hotel chain entity 
     * @param int $chainId The chain id - contains chain id records.
     * @param varchar $data chain record - contains chain record data.
     * @todo this method is used to update hotel chain entity in database .
     * @return output updates hotel chain entity in DB.
     */
    function updateHotelChain($chainId, $data) {
        $this->db->where('id', $chainId);
        $this->db->where('entity_type', $this->config->item('attribute_hotel_chain'));
        $this->db->update('entity_attributes', $data);
    }

    /**
     * @method addHotelPurpose() method used to insert purpose type of the hotel like business hotel etc.
     * @param varchar $data The purpose title - name of purpose title listed in DB.
     * @todo this method is used to add hotel purpose attributes into entity_attributes table .
     * @return output is last inserted id else will return false.
     */
    function addHotelPurpose($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getHotelPurpose() method used to fetch purpose like business hotel from DB.
     * @param varchar $title purpose title - name of purpose title listed in DB.
     * @param int $eid The purpose id - primary key.
     * @todo this method is used to fetch hotel purpose entity from entity_attributes table.
     * @return last value inserted is the output if condition is true else will return false.
     */
    function getHotelPurpose($title, $eid = '') {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_hotel_purpose'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getHotelPurposeList() method used to fetch hotel purpose list.
     * @todo this method is used to fetch hotel purpose +list name from entity_attributes table .
     * @return purpose list in the form of an array is returned for true condition else false is returned.
     */
    function getHotelPurposeList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where('entity_type', $this->config->item('attribute_hotel_purpose'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method delHotelPurpose() method used to delete the purpose.
     * @param int $chainId purpose id - used to del hotel purpose using purpose id.
     * @todo this method is used to delete hotel purpose name from entity_attributes table 
     * @return hotel purpose value will be deleted as per output.
     */
    function delHotelPurpose($chainId) {
        $this->db->where('id', $chainId);
        $this->db->where('entity_type', $this->config->item('attribute_hotel_purpose'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * @method updateHotelPurpose() method used to update hotel purpose entity in DB.
     * @param int $dbId The purpose id - primary key.
     * @param varchar $newdata The purpose record -  contains purpose records.
     * @todo this method is used to update hotel purpose entity into entity_attributes table.
     * @return returns true value.
     */
    function updateHotelPurpose($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where('entity_type', $this->config->item('attribute_hotel_purpose'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * @method addHotelPropertyType() method used to insert hotel property type in DB.
     * @param array() $data The chain id - hotel property type added using chain id.
     * @todo this method is used to add hotel property name into entity_attributes table.
     * @return output is last inserted id for true condition else false is returned.
     */
    function addHotelPropertyType($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * Common function to verify already assigned attributes.
     * @param string $table 
     * @param int $checkid
     * @param string $field
     * @param string $selected_feild
     * @todo this method is used to verify entity already assigned for any hotel which is not in database table.
     * @return output provided already assigned attributes if true else returns false.
     */
    function isAttributeTypeAssigned($table = '', $check_id = null, $field = '', $select_feild = '') {
        if (!empty($select_feild))
            $this->db->select($select_feild);

        if (!empty($field) && !empty($check_id))
            $this->db->where($field, $check_id);

        if (!empty($table))
            $query = $this->db->get($table);
//        echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    /*
     * @method getHotelPropertyType() method used to fetch property list from DB.
     * @param varchar $title property title - contains property title.
     * @param int $eid property id - property type fetched through property id.
     * @todo this method is used to fetch hotel property name from entity_attributes table 
     * @return output is last inserted value else will return false.
     */

    function getHotelPropertyType($title, $eid = '') {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_property_types'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getHotelPropertyTypeList() method used to list the propertytype list.
     * @todo this method is used to return hotel property list from entity_attributes table. 
     * @return property list in the array form is returned as outout if true.
     */
    function getHotelPropertyTypeList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_property_types'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method delHotelPropertyType() method used to delete the property type in DB.
     * @param int $dId The property id - property type data deleted using property id.
     * @todo this method is used to delete hotel property type list from entity_attributes table. 
     * @return hotel property type id deleted as output if true.
     */
    function delHotelPropertyType($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_property_types'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * @updateHotelPropertyType() method used to update property type in hotel DB.
     * @param int $dbId The property id - hotel property type updation.
     * @param varchar $newdata The property record - contains records related to property type.
     * @todo this method is used to update hotel property from entity_attributes table
     * @return updated prperty type attribute if condistion is true else will return false.
     */
    function updateHotelPropertyType($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_property_types'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * @method addHotelFacility() method to insert hotel facility in DB.
     * @param array() $data holds facility title in the form of an array
     * @todo this method is used to update hotel property in entity_attributes table.
     * @return output is last inserted value if true.
     */
    function addHotelFacility($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getHotelFacility() method used to fetch hotel facility title from entity_attributes table
     * @param array() $title hold The facility title in the form of an array
     * @param int $eid The facility id - primary key.
     * @todo this method is used to fetch hotel facility from entity_attributes table.
     * @return output is last inserted id for true condition else returns false.
     */
    function getHotelFacility($title, $eid = '') {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_facilities'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getHotelFacilityList() method used to fetch hotel facility list from entity_attributes
     * @todo this method is used to get hotel facility list from entity_attributes table
     * @return array(), return output is hotel facility in the form of an array() if condition is true else will return false.
     */
    function getHotelFacilityList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_facilities'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delHotelFacility() method used to delete hotel facility
     * @param int $dId The facility id - primary key.
     * @todo this method is used to delete hotel facility from entity_attributes table
     * @return  facility gets deleted for true condition else will return false.
     */
    function delHotelFacility($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_facilities'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * @method updateHotelFacility() used to update hotel facility inside entity_attributes table.
     * @param int $dbId The facility id - primary key.
     * @param varchar $newdata facility record -  contains facility records.
     * @todo this method is used to  update hotel facility inside entity_attributes table
     * @return updates hotel facility as output for true condition else will return false.
     */
    function updateHotelFacility($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_facilities'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * addRoomFacility() method used to insert room facility in DB.
     * @param array() $data holds facility data in the form of an array
     * @todo this method is used to add room facility in to entity_attributes table.
     * @return return vaue is last inserted id for true else will return false.
     */
    function addRoomFacility($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getRoomFacility() used to fetch the room facility.
     * @param array()  $title holds The room facility title in the form of an array
     * @param int $eid The room facility id -  primary key.
     * @todo this method is used to fetch room facility details from DB.
     * @return  fetches room facility details if true else will return false.
     */
    function getRoomFacility($title, $eid = '') {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_complementy_service'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * getRoomFacilityList() method used to fetch room facility list.
     * @todo this method is used to  fetch hotel room facility list from entity_attributes table
     * @return room facility in the form of an array is returned OR false.
     */
    function getRoomFacilityList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_complementy_service'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delRoomFacility() method to delete hotel room facility.
     * @param int $dId The room facility id - primary key.
     * @todo this method is used to delete hotel room facility from entity_attributes table
     * @return deletes room facility details from DB if true.
     */
    function delRoomFacility($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_complementy_service'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * @method updateRoomFacility() method used to update room facility in DB.
     * @param int $dbId The room facility id -  primary key._
     * @param array() $newdata hold room facility title in the form of an array
     * @todo this method is used to  update hotel room facility inside entity_attributes table
     * @return updates room facility details as end result for true condition.
     */
    function updateRoomFacility($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_complementy_service'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * @method addCurrency() method used to insert currency in DB.
     * @param array() $data holds the currency in the form of an array
     * @todo this method is used to add currency name into currency table.
     * @return returns last inserted id for true else will return false.
     */
    function addCurrency($data) {
        if ($this->db->insert('currencies', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getCurrency() method to fetch currency details.
     * @param varchar $code hold The currency code - contains code currency value.
     * @param int $usval hold The currency value - fetches currency using currency value.
     * @param int $eid hold The currency id -  primary key.
     * @todo this method is used to fetch currency name from currency table.
     * @return returns last inserted id as result for true else returns false.
     */
    function getCurrency($code, $usval = '', $eid = '') {
        $this->db->select('id');
        $this->db->where("code", $code);
        if ($usval != '')
            $this->db->where("currency_usd_value", $usval);

        if ($eid != "")
            $this->db->where("id !=", $eid);

        $query = $this->db->get('currencies');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getCurrencyList() used to fetch list of currency code, id and currency value.
     * @todo this method is used to return the currency code, id and currency value from currencies table.
     * @return currency list in  form of an array is returned as output for true else false.
     */
    function getCurrencyList() {
        $this->db->select('id,code,currency_to_tr,tr_to_currency');
        $this->db->order_by('code');
        $query = $this->db->get('currencies');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method delCurrency() method used to delete the currency from DB.
     * @param int $dId holds the currency code - primary key in currency table.
     * @todo this method is used to delete currency  from currencies table.
     * @return output deletes currency data for true condition else return false.
     */
    function delCurrency($dId) {
        $this->db->where('code', $dId);
        return $this->db->delete('currencies');
    }

    /**
     * @method deleteCurencyCodesData() method used to delete the currency using currency code.
     * @param array $delCodesArray hold the currency code in the form of an array().
     * @todo this method is used to delete currency records  from currency table.
     * @return return value is currency value data for true condition else return false.
     */
    function deleteCurencyCodesData($delCodesArray) {
        $this->db->where_in('code', $delCodesArray);
        return $this->db->delete('currencies');
    }

    /**
     * @method updateCurrency() method used to update currency entity.
     * @param int $dbId The currency id - primary key.
     * @param array() $newdata holds currency record 
     * @todo this method is used to update currency inside currencies table.
     * @return updated currency value as output for true else returns false.
     */
    function updateCurrency($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->update('currencies', $newdata);
    }

    /**
     * @method importCurencyData() method used to import currencies.
     * @param array $data holds the currency record in the form of an array
     * @todo this method is used to import currencies into currencies table.
     * @return returns either true or false.
     */
    function importCurencyData($data) {
        // clear old data
        $this->db->truncate('currencies');
        // import new data
        return $this->db->insert_batch('currencies', $data);
    }

    /**
     * @method addCurencyData() method used to insert currencies.
     * @param array $data holds the currency record in the form of an array
     * @todo this method is used to add currency into currencies table.
     * @return returns either true or false.
     */
    function addCurencyData($data) {
        return $this->db->insert_batch('currencies', $data);
    }

    /**
     * @method addCountry() method used to insert country.
     * @param array $data holds the country record in the form of an array
     * @todo this method is used to add country  into countries table
     * @return returns value for add country if true else false.
     */
    function addCountry($data) {
        if ($this->db->insert('countries', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getCountry() method used to fetch country id 
     * @param varchar $cname The country title - name of country.
     * @todo this method is used to fetch country name from country  table DB.
     * @return returns country table for true else returns false.
     */
    function getCountry($cname) {
        $this->db->select('id');
        $this->db->where("country_name", $cname);
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method checkCountryExists() method used to validate if country exists or not.
     * @param int $Id The country id - validating country records.
     * @param varchar $name The country name - contains country name records.
     * @todo this method is used to check if country exists or not using country id and name in country  table.
     * @return returns last inserted value for true.
     */
    function checkCountryExists($Id = '', $name) {
        $this->db->select('id');
        if ($Id != '')
            $this->db->where("id !=", $Id);
        $this->db->where("country_name", $name);
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method checkCountryCodeExists() method to validate existing country code.
     * @param int $Id The country id - validate country code using country id.
     * @param varchar $ccode The country code - primary key.
     * @todo this method is used to validate existing country using country code and id inside country  table.
     * @return last inserted id is he output.
     */
    function checkCountryCodeExists($Id = '', $ccode) {
        $this->db->select('id');
        if ($Id != '')
            $this->db->where("id !=", $Id);
        $this->db->where("country_code", $ccode);
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getCountryCode() method used to fetch the country code.
     * @param int $cid holds the country  id - used in fetching country code.
     * @todo this method is used to fetch country code name from database 
     * @return returns country_code|false.
     */
    function getCountryCode($cid) {
        $this->db->select('country_code');
        $this->db->where("id", $cid);
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->country_code;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getCountryName() method used to fetch country name.
     * @param int $cid holds the country  id - contains id records.
     * @todo this method is used to fetch country  name from country table.
     * @return  returns country name|false.
     */
    function getCountryName($cid) {
        $this->db->select('country_name');
        $this->db->where("id", $cid);
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->country_name;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getCountriesList() method used to get fetch country listing.
     * @todo this method is used to fetch country list name from countries table.
     * @return array() country name in the form of an array is returned else return false.
     */
    function getCountriesList() {
        $this->db->select('id,country_code as code,country_name');
        $this->db->order_by('country_name', 'ASC');
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delCountry() method used to delete the countries from countries table
     * @param int $dId The country  id
     * @todo this method is used to delete countries from countries table 
     * @return returns del country info else return false 
     */
    function delCountry($dId) {
        $this->delCities($dId); // delete cities of the selected cuntry
        $this->db->where('id', $dId);
        return $this->db->delete('countries');
    }

    /**
     * updateCountry() method used to update the countries.
     * @param int $dbId hold country id - primary key.
     * @param array $newdata holds the country record in the form of an array()
     * @todo this method is used to update country record in countries table.
     * @return updates country info as output else return false.
     */
    function updateCountry($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->update('countries', $newdata);
    }

    /**
     * @method addCity() method used to insert country city.
     * @param array  $data hold country city in the form of an array.
     * @todo this method is used to add country city name in country_cities table.
     * @return last inserted id is retured for true.
     */
    function addCity($data) {
        if ($this->db->insert('country_cities', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getCity() method used to fetch city name 
     * @param varchar $cname The city name - contains city name records.
     * @todo this method is used to fetch city name from country_cities table 
     * @return return value is cityname for true condition.
     */
    function getCity($cname) {
        $this->db->select('id');
        $this->db->where("city_name", $cname);
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method checkCityExists() method to validate if city exists or not 
     * @param varchar $name The city name - contains city name records.
     * @param int $cid The city id -  primary key.
     * @todo this method is used to check if city exista or not in country_cities table.
     * @return returns last inserted id else return false.
     */
    function checkCityExists($name, $cid = '', $cuntryId = '') {
        $this->db->select('id');
        $this->db->where("city_name", $name);
        if ($cid != '')
            $this->db->where("id !=", $cid);
        if ($cuntryId != '')
            $this->db->where("country_id", $cuntryId);
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method checkCityCodeExists() method used to check if city exists or not 
     * @param varchar $ccode The city  - primary key.
     * @param int $cid The city id - validation of city id using primary key.
     * @param int $cuntryId The country id - validation of country id using primary key.
     * @todo this method is used to check if city exists or not using city code,city id and country id in country_cities table 
     * @return ast inserted id is returned for true condition.
     */
    function checkCityCodeExists($ccode, $cid = '', $cuntryId = '') {
        $this->db->select('id');
        if ($cid != '')
            $this->db->where("id !=", $cid);

        if ($cuntryId != '')
            $this->db->where("country_id", $cuntryId);

        $this->db->where("city_code", $ccode);
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getCityByCode() method used to fetch  city id thorough city code.
     * @param varchar $ccode The city code - primary key.
     * @todo this method is used to return city id using city code in country_cities table 
     * @return return value is last inserted id else return false.
     */
    function getCityByCode($ccode) {
        $this->db->select('id');
        $this->db->where("city_code", $ccode);
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getCitiesList() method used to fetch id,city name, and city code
     * @param id $cuntryId country id - fetches city records using country id.
     * @todo this method is used to return id,city name, and city code using country id in country_cities table.
     * @return cities in the form of an array is the result output for true condition.
     */
    function getCitiesList($cuntryId) {
        $this->db->select('id,city_name,city_code');
        $this->db->order_by('city_name', 'ASC');
        $this->db->where("country_id", $cuntryId);
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delCity() method used to delete district 
     * @param int $dId The district id - contains district table details.
     * @todo this method is used to delete district from country_cities table 
     * @return updates delcity info for true output else returns false.
     */
    function delCity($dId) {
        $this->delCityDistricts($dId);
        $this->db->where('id', $dId);
        return $this->db->delete('country_cities');
    }

    /**
     * delCities() method used to delete country city.
     * @param int $cuntryId country id - primary key of country table.
     * @todo this method is used to delete city from country_cities table using country id
     * @return returns with updated info for true.
     */
    function delCities($cuntryId) {//  by passing country id
        $this->delCountryCityDistricts($cuntryId);
        $this->db->where('country_id', $cuntryId);
        return $this->db->delete('country_cities');
    }

    /**
     * updateCity() method used to update country city
     * @param int $dbId the city id - primary key.
     * @param array $newdata hold the city name in the form of an array.
     * @todo this method is used to update country city form country_cities table using city id.
     * @return returns with updated city info for true else return false.
     */
    function updateCity($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->update('country_cities', $newdata);
    }

    /**
     * @method getCityName() method used to fetch the city name
     * @param int $cId The city id - primary key.
     * @todo this method is used to fetch city name from country_cities table 
     * @return output is city name for true else returns false.
     */
    function getCityName($cId) {
        $this->db->select('city_name');
        $this->db->where("id", $cId);
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->city_name;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method addDistrict() method used to insert district value.
     * @param array $data holds the districts record in the form of an array()
     * @todo this method is used to add districts into table  
     * @return output is last inserted id for true else returns false.
     */
    function addDistrict($data) {
        if ($this->db->insert('city_districts', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getDistrict() method used to fetch district name
     * @param int $cid The district id - primary key.
     * @param varchar $dname The district name - contains district name records.
     * @todo this method is used to fetch district name from city_districts table 
     * @return output is last inserted id for true else returns false.
     */
    function getDistrict($dname, $cid = '') {
        $this->db->select('id');
        $this->db->where("district_name", $dname);
        if ($cid != '')
            $this->db->where("city_id", $cid);
        $query = $this->db->get('city_districts');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method checkDistrictExists() method used to check if district exists or not.
     * @param int $cuntryId The country id
     * @param int $cid The city id
     * @param int $did The district id - primary key.
     * @param varchar $name The district name
     * @todo this method is used to get district id from city_districts table 
     * @return output is last inserted id for true else returns false.
     */
    function checkDistrictExists($name, $cuntryId = '', $cid = '', $did = '') {
        $this->db->select('id');
        $this->db->where("district_name", $name);
        if ($cid != '')
            $this->db->where("city_id", $cid);
        if ($did != '')
            $this->db->where("id !=", $did);
        if ($cuntryId != '')
            $this->db->where("country_id", $cuntryId);
        $query = $this->db->get('city_districts');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getCityDistricts() method used to select district name and id
     * @todo this method is used for district list from city_districts table
     * @return district list in the form of an array or true else return false.
     */
    function getCityDistricts($cityId) {
        $this->db->select('id,district_name');
        $this->db->where("city_id", $cityId);
        $this->db->order_by('district_name', 'ASC');
        $query = $this->db->get('city_districts');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method getDistrictList() method used to select district name
     * @todo this method is used for district list from city_districts table using country id and city id
     * @return district list in the form of an array is returned as output for true else return false.
     */
    function getDistrictList($cuntryId, $cityId) {
        $this->db->select('id,district_name');
        $this->db->where("country_id", $cuntryId);
        $this->db->where("city_id", $cityId);
        $this->db->order_by('district_name', 'ASC');
        $query = $this->db->get('city_districts');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method delDistrict() method used to delete the district
     * @param int $dId district id - primary key of city district table.
     * @todo this method is used to  delete district chain from city_districts table
     * @return output is last inserted id for true else returns false.
     */
    function delDistrict($dId) {
        $this->db->where('id', $dId);
        return $this->db->delete('city_districts');
    }

    /**
     * @method delCityDistricts() method used to delete the city.
     * @param int $city_id hold city id - primary key.
     * @todo this method is used to  delete city from city_districts table
     * @return if true will delete city district else will return false.
     */
    function delCityDistricts($city_id) {//  by passing country id
        $this->db->where('city_id', $city_id);
        return $this->db->delete('city_districts');
    }

    /**
     * @method delCountryCityDistricts() method used to delete district using country code.
     * @param int $country_id holds country id - primary key.
     * @todo this method is used to  delete country from city_districts table
     * @return output is last inserted id for true else returns false.
     */
    function delCountryCityDistricts($country_id) {
        $this->db->where('country_id', $country_id);
        return $this->db->delete('city_districts');
    }

    /**
     * @method updateDistrict() method used to update the district name
     * @param int $dbId The district id -  primary key.
     * @param array() $newdata holds district name in the form of an array
     * @todo this method is used to update district name inside city_districts table 
     * @return output updates district as per records for true else return false.
     */
    function updateDistrict($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->update('city_districts', $newdata);
    }

    /**
     * @method addMarket() method used to insert market in DB.
     * @param array() $data hold market title in the form of an array
     * @todo this method is used to add market name in entity_attributes table 
     * @return returns last inserted value as output for true.
     */
    function addMarket($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method addMarket() method used to insert market in DB.
     * @param array() $data hold market title in the form of an array
     * @todo this method is used to add market name in entity_attributes table 
     * @return returns last inserted value as output for true.
     */
    function getMarket($cname, $eid = '') {
        $this->db->select('id');
        $this->db->where("entity_title", $cname);
        $this->db->where("entity_type", $this->config->item('attribute_market'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method addMarket() method used to insert market in DB.
     * @param array() $data hold market title in the form of an array
     * @todo this method is used to add market name in entity_attributes table 
     * @return returns last inserted value as output for true.
     */
    function getMarketList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_market'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delMarket() method used to delete  market from DB.
     * @param int $dId The market id - primary key.
     * @todo this method is used to delete market from entity_attributes table 
     * @return output deletes market from DB. for true else returns false.
     */
    function delMarket($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_market'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * updateMarket() method used to update market value.
     * @param int $dbId The market id - primary key.
     * @param array() $newdata holds markets in the form of an array
     * @todo this method is used to update market function inside entity_attributes table.
     * @return output deletes market from DB for true else returns false.
     */
    function updateMarket($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_market'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * addTravelType() method used to insert travel type in DB
     * @param array() $data holds travel title - contains record of travel type DB.
     * @todo this method is used to add market entity into entity_attributes table 
     * @return output value is last inserted id or value else return false.
     */
    function addTravelType($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * getTravelType() method used to fetch travel type record
     * @param varchar $cname The travel title - contains record of travel type DB.
     * @param int $eid The travel id - primary key.
     * @todo this method is used to fetch travel type record from DB.
     * @return returns last inserted id value.
     */
    function getTravelType($cname, $eid = "") {
        $this->db->select('id');
        $this->db->where("entity_title", $cname);
        $this->db->where("entity_type", $this->config->item('attribute_travel_type'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * getTravelTypeList() method used to fetch travel type list.
     * @todo this method is used to fetch travel details from entity_attributes table.
     * @return an array() which holds travel title else return false.
     */
    function getTravelTypeList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where('entity_type', $this->config->item('attribute_travel_type'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delTravelType() method used to delete travel type
     * @param int $dId The travel type id - primary key.
     * @todo this method is used to delete travel type entity from entity_attributes table
     * @return output is updated for true else return false.
     */
    function delTravelType($dId) {
        $this->db->where('id', $dId);
        $this->db->where('entity_type', $this->config->item('attribute_travel_type'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * updateTravelType() method is used to update travel type.
     * @param int $dbId The travel id -  primary key.
     * @param array() $newdata hold travel  record - type of travel records.
     * @todo this method is used to update travel name entity inside entity_attributes table.
     * @return output is updated for true else it returns false.

     */
    function updateTravelType($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where('entity_type', $this->config->item('attribute_travel_type'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * addRoomType() method is used to insert room type
     * @param varchar $data The room type title - room type record table.
     * @todo this method is used to add room type title into entity_attributes table
     * @return outut is added inserted id for true else return false.
     */
    function addRoomType($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * getRoomType() method used to fetch the room type
     * @param varchar $cname The room type title - room type record table.
     * @param int $eid The room type id - primary key.
     * @todo this method used is used to fetch room type title from entity_attributes table
     * @return outut is fetched inserted id for true else return false.
     */
    function getRoomType($cname, $eid = "") {
        $this->db->select('id');
        $this->db->where("entity_title", $cname);
        $this->db->where("entity_type", $this->config->item('attribute_room_types'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * getRoomTypesList() method used to fetch room type entity.
     * @todo this method is used  to fetch room type title list from entity_attributes table
     * @return room types title in the form of an array() OR return false.
     */
    function getRoomTypesList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_room_types'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delRoomType() method used to delete room type fom DB.
     * @param int $dId The room type id - primary key.
     * @todo this method is used to delete room type from entity_attributes table
     * @return output value is updated room type info for true else return false.
     */
    function delRoomType($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_room_types'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * @method updateRoomType() method used to update room type
     * @param int $dbId The room type id - primary key.
     * @param array() $newdata hold room type record in the form of an array().
     * @todo this method is used to update room type inside entity_attributes table
     * @return output value is updated room type info for true else return false.
     */
    function updateRoomType($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_room_types'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * @method addMealPlan() method used to insert meal plan.
     * @param array() $data meal title in the form of an array()
     * @todo this method is used to add meal plan value into entity_attributes table
     * @return returns last inserted value for true else return false.
     */
    function addMealPlan($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getMealPlan() method used to fetch  meal plan.
     * @param varchar $cname The meal title - includes meal plans record.
     * @param int $eid The meal id - primary key.  
     * @todo this method is used to fetch meal plan from entity_attributes table
     * @return return value is fetched list of meal plan for true else return false.
     */
    function getMealPlan($cname, $eid = "") {
        $this->db->select('id');
        $this->db->where("entity_title", $cname);
        $this->db->where("entity_type", $this->config->item('attribute_meal_plan'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * getMealPlansList() method used to fetch meal plan list.
     * @todo this method is used to fetch meal plan list into entity_attributes table.
     * @return meal plan list in the form of an array() is returned as output else false.
     */
    function getMealPlansList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_meal_plan'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method delMealPlan() method used to delete meal plan.
     * @param int $dId The meal id - primary key.
     * @todo this method is used to delete meal plan from entity_attributes table
     * @return return value is del meal plan records for true else return false.
     */
    function delMealPlan($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_meal_plan'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * updateMealPlan() method used to update meal plan.
     * @param int $dbId The meal id -  primary key.
     * @param array $newdata hold meal plan in the form of an array()
     * @todo this method is used to update meal plan inside entity_attributes table
     * @return value is updated meal plan for true else return false.

     */
    function updateMealPlan($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_meal_plan'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * @method addRole() method used to insert role in DB.
     * @param array() $data hold role title in the form of an array
     * @todo this method is used to add role into role table DB.
     * @return last returned id is output for true.
     */
    function addRole($data) {
        if ($this->db->insert('role', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getRole() method used to fetch role.
     * @param $cname[] The role title - name of designation.
     * @param int $eid The role id - primary key.
     * @todo this method is used to fetch role from role table value
     * @return last returned id is output for true else return false.
     */
    function getRole($cname, $eid = "") {
        $this->db->select('id');
        $this->db->where("title", $cname);
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('role');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * getRolesList() method used to fetch role list.
     * @todo this method used to get role list from role table
     * @return roles in the form of an array is returned else false.
     */
    function getRolesList() {
        $this->db->select('id,title,access_level_id');
        $this->db->order_by('title');
        $query = $this->db->get('role');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method delRole() method used to delete the role
     * @param int $dId The role id
     * @todo this method is used to delete role from role table
     * @return for true role is deleted else return false.
     */
    function delRole($dId) {
        $this->db->where('id', $dId);
        return $this->db->delete('role');
    }

    /**
     * @method delRole() method used to delete the role
     * @param int $dId The role id - primary key.
     * @todo this method is used to delete role from role table
     * @return for true role is deleted else return false.
     */
    function updateRole($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->update('role', $newdata);
    }

    /**
     * updateRole() method used to update the role
     * @param int $dbId role id -  primary key.
     * @param array $newdata hold roles in the form of an array.
     * @todo this method is used to update role inside role table.
     * @return  updated role for true condition else return false.
     */
    function addPosition($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * getPosition() method used to fetch position
     * @param array $cname[] holds The position title
     * @param int $eid The position id
     * @todo this method is used to fetch position from role entity_attributes
     * @return returns last inserted id from DB for true condition.
     */
    function getPosition($cname, $eid = "") {
        $this->db->select('id');
        $this->db->where("entity_title", $cname);
        $this->db->where("entity_type", $this->config->item('attribute_hotel_positions'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getPositionsList() method used to fetch postionlist table
     * @todo this method is used to fetch position from entity_attributes
     * @return value returned will be position list for true condition.
     */
    function getPositionsList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_hotel_positions'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delPosition() method used to delete position enity from DB.
     * @param int $dId The position id
     * @todo this method is used to delete position from entity_attributes
     * @return value returned is true as it deletes position.
     */
    function delPosition($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_hotel_positions'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * updatePosition() method used to update position in DB.
     * @param int $dbId The position id
     * @param array() $newdata hold position record in the form of an array
     * @todo this method is used to update position in entity_attributes.
     * @return value returned is true as it updates position else it returns false.
     */
    function updatePosition($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_hotel_positions'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * addStarRating() method used to insert star ratings
     * @param array() $data hold star ratings in the form of an array
     * @todo this method is used to add star rating into entity_attributes
     * @return fetches last inserted id for true condition else return false.
     */
    function addStarRating($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * getStarRating() method used to fetch star ratings.
     * @param varchar $cname star ratings title - list of records.
     * @param int $eid The star ratings id - primary key.
     * @todo this method is used to fetch star ratings from entity_attributes
     * @return output is last returned value for true condition else will return false.
     */
    function getStarRating($cname, $eid = "") {
        $this->db->select('id');
        $this->db->where("entity_title", $cname);
        $this->db->where("entity_type", $this->config->item('attribute_star_ratings'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * getStarRatingList() method used to fetch star rating list.
     * @return star ratings -  list of records.
     * @todo this method is used to fetch star rating from entity_attributes
     * @return star rating in the form of an array is returned for true condition else return false
     */
    function getStarRatingList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_star_ratings'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method delStarRating() method used to delete star ratings from DB.
     * @param int $dId The star rating id -  primary key.
     * @todo this method is used to del star rating from entity_attributes
     * @return deletes star rating as output for true condition else will return false.
     */
    function delStarRating($dId) {
        $this->db->where('entity_title', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_star_ratings'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * updateStarRating() method is used to update star rating in DB.
     * @param int $dbId The star rating id - primary key.
     * @param varchar $data The star rating record - list of records.
     * @todo this method is used to update star rating inside entity_attributes
     * @return deletes star rating as output for true condition else will return false.
     */
    function updateStarRating($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_star_ratings'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * addDorakRating() method is used to insert dorak rating
     * @param varchar $data The dorak rating title -  data list of table.
     * @todo this method is used to add dorak rating into entity_attributes
     * @return return value is dorak rating for true else it returns false.
     */
    function addDorakRating($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * getDorakRating() method is used to fetch dorak rating from DB.
     * @param varchar $cname The dorak rating title - data list of table.
     * @param int $eid The dorak rating id - primary key.
     * @todo this method is used to fetch dorak rating from entity_attributes
     * @return return value will be last inserted id for true condition else will return false.
     */
    function getDorakRating($cname, $eid = "") {
        $this->db->select('id');
        $this->db->where("entity_title", $cname);
        $this->db->where("entity_type", $this->config->item('attribute_dorak_ratings'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * getDorakRatingList() method is used to fetch dorak rating list.
     * @todo this method is used to fetch dorak rating from entity_attributes
     * @return dorak ratings in the form of an array is returned for true else it will return false.
     */
    function getDorakRatingList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_dorak_ratings'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method delDorakRating() method is used to delete OR eliminate the dorak rating from DB.
     * @param int $dId The dorak rating id -  primary key
     * @todo this method is used to delete dorak rating from entity_attributes.
     * @return output value is the deleted rating if condition is true else will return false.
     */
    function delDorakRating($dId) {
        $this->db->where('entity_title', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_dorak_ratings'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * updateDorakRating() method is used to update dorak rating in DB.
     * @param int $dbId dorak rating id - primary key
     * @param varchar $newdata dorak rating record -  data list
     * @todo this method is used to update dorak rating in entity_attributes.
     * @return output value is the updated dorak rating .
     */
    function updateDorakRating($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_dorak_ratings'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * @method addStatus() method used to insert status in DB.
     * @param varchar $data status title - primary key of insert_id.
     * @todo this method is used to add status into entity_attributes.
     * @return output value is last inserted id if condition is true else will return false.
     */
    function addStatus($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * getStatus() method used to fetch status from DB.
     * @param varchar $cname status title - title of company name.
     * @param int $eid status id - primary key.
     * @todo this method is used to fetch status from entity_attributes.
     * @return  output value is the desired status if condistion is true else will return false.
     */
    function getStatus($cname, $eid = "") {
        $this->db->select('id');
        $this->db->where("entity_title", $cname);
        $this->db->where("entity_type", $this->config->item('attribute_hotel_status'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getStatusList() method used to fetch status list.
     * @todo this method is used to fetch status list from entity_attributes.
     * @return output is status list in the form of an array if true else false is returned.
     */
    function getStatusList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_hotel_status'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delStatus() method used to delete the status
     * @param int $dId The status id - primary key of  attribute_hotel_status.
     * @todo this method is used to delete status from entity_attributes.
     * @return output value is either true or false.
     */
    function delStatus($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_hotel_status'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * updateStatus() method used to update the status in DB.
     * @param int $dbId The status id - primary key.
     * @param array() $newdata hold status record in the form of an array
     * @todo this method is used to update status in DB.
     * @return return value adds the department value else will return false.
     */
    function updateStatus($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * addDepartment() method to insert department entity in DB.
     * @param array() $data holds department title  in the form of an array.
     * @todo this method is used to add department into entity_attributes.
     * @return return value adds the department value else will return false.
     */
    function addDepartment($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * getDepartment() method to fetch the department entity.
     * @param varchar $cname The department title - fetches department details.
     * @param int $eid The department id - primary key.
     * @todo this method is used to fetch department entity from entity_attributes.
     * @return output value will be last inserted id if condition is true else will return false.
     */
    function getDepartment($cname, $eid = "") {
        $this->db->select('id');
        $this->db->where("entity_title", $cname);
        $this->db->where("entity_type", $this->config->item('attribute_deppartment'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getDepartmentsList() method to fetch the list of department entity.
     * @todo this method is used to fetch department list from entity_attributes.
     * @return eturn value is departments in the form of an arry if condition is true else will return false.
     */
    function getDepartmentsList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_deppartment'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delDepartment() method to delete the department entity.
     * @param int $dId The department id - primary key.
     * @todo this method is used to delete department entity from entity_attributes.
     * @return return value is del department if condition is true else will return false.
     */
    function delDepartment($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_deppartment'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * updateDepartment() method used to update department entity.
     * @param int $dbId The department id - primary key.
     * @param varchar $newdata The department record - reord updation.
     * @todo this method is used to update department entity in entity_attributes table.
     * @return output is updated values of department if condition is true else will return false.
     */
    function updateDepartment($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_deppartment'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * addCancellationDuration() method to insert cancellation duration time.
     * @param array() $data holds cancellation title - fetches cancellation duration .
     * @todo this method is used to add cancellation duration time into entity_attributes table.
     * @return output is last returned id if true else will return false.
     */
    function addCancellationDuration($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * getCancellationDuration() method to fetch cancel duration value.
     * @param array() $cname hold cancellation title - 
     * @param int $eid cancellation id - primary key.
     * @todo this method is used to fetch cancel duration value from entity_attributes table.
     * @return last return id if true else will return false.
     */
    function getCancellationDuration($cname, $eid = "") {
        $this->db->select('id');
        $this->db->where("entity_title", $cname);
        $this->db->where("entity_type", $this->config->item('attribute_duration_cancellation'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getCancellationDurationList() method too fetch cancel duration list.
     * @todo this method is used to get cancellation list from entity_attributes table.
     * @return output value is cancellation record in the form of array else will return false
     */
    function getCancellationDurationList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where('entity_type', $this->config->item('attribute_duration_cancellation'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delCancellationDuration() method to delete the cancellation duration.
     * @param int $dId The cancellation id - table of attribute_duration_cancellation.
     * @todo this method is used to delete cancellation duration from entity_attributes table.
     * @return output value is true if successful else will return false.
     */
    function delCancellationDuration($dId) {
        $this->db->where('id', $dId);
        $this->db->where('entity_type', $this->config->item('attribute_duration_cancellation'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * updateCancellationDuration() method is used to update cancellation entity.
     * @param int $dbId The cancellation id  - primary key of attribute_duration_cancellation.
     * @param array() $newdata hold cancellation record in the form of array
     * @todo  this method is used to update cancellation record in entity_attributes table.
     * @return output value is updated cancellation record if true else will return false.
     */
    function updateCancellationDuration($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where('entity_type', $this->config->item('attribute_duration_cancellation'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * addPaymentDuration() method used to insert payment duration in DB.
     * @param array() $data holds payment title in the form of an array.
     * @todo this method is used to add payment info in entity_attributes table.
     * @return output is last inserted id if true else will return false.
     */
    function addPaymentDuration($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getPaymentDuration() method used to fetch payment value from DB.
     * @param array() $cname hold  payment duration title in the form of an array
     * @param int $eid The payment id - location table of entity_attributes.
     * @todo this method is used to fetch payment value from entity_attributes table.
     * @return output is last inserted value if true else wil return false.
     */
    function getPaymentDuration($cname, $eid = "") {
        $this->db->select('id');
        $this->db->where("entity_title", $cname);
        $this->db->where("entity_type", $this->config->item('attribute_duration_payment'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * getPaymentDurationList() method used to fetch the list of payment duration.
     * @todo this method is used to fetch payment duration entity from entity_attributes table.
     * @return payment options in the form of an array is returned if true else will return false.
     */
    function getPaymentDurationList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_duration_payment'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delPaymentDuration() method to delete the paymtent duration entity.
     * @param int $dId The payment id - location table of attribute_duration_payment.
     * @todo this method is used to delete payment duration entity from entity_attributes table.
     * @return output will delete payment duration if true else will return false.
     */
    function delPaymentDuration($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_duration_payment'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * @method updatePaymentDuration() method used to update payment duration.
     * @param int $dbId  payment id - location table of attribute_duration_payment.
     * @param array() $newdata holds payment record in the form of an array
     * @todo this method is used to update payment duration in entity_attributes table.
     * @return output will be updated payment duration info if true else will return false.

     */
    function updatePaymentDuration($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_duration_payment'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * @method addAgeGroup() method used to insert age group options in DB.
     * @param array() $data holds age title in the form of an array.
     * @todo this method is used to add age group into entity_attributes table.
     * @return return value will be updated age group data if true else will return false.
     */
    function addAgeGroup($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * getAgeGroup() method used to fetch the age group options
     * @param array() $cname hold age group options title in the form of an array()
     * @param int $eid The age group option id - primary key.
     * @todo this method is used to fetch age group entity from entity_attributes table.
     * @return output will be age group data if true else will return false.
     */
    function getAgeGroup($cname, $eid = "") {
        $this->db->select('id');
        $this->db->where("entity_title", $cname);
        $this->db->where("entity_type", $this->config->item('attribute_child_group'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * getAgeGroupList() method fetches the list of age group options.
     * @todo this method is used to fetch age group list from entity_attributes table.
     * @return age group title in the form of an array or else return false.
     */
    function getAgeGroupList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_child_group'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method delAgeGroup() method used to delete the age group options.
     * @param int $dId The age group options id - primary key
     * @todo this method is used to delete age group title from  entity_attributes table.
     * @return  output value is updated age group list if true else will return false.
     */
    function delAgeGroup($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_child_group'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * updateAgeGroup() method used to update the age group entity.
     * @param int $dbId The age group options id
     * @param array() $newdata hold age group title records in the form of an array.
     * @todo this method is used to update age group title in entity_attributes table.
     * @return output value is updated age group list if true else will return false.
     */
    function updateAgeGroup($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_child_group'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * @method addCompany() method used to insert company name in DB.
     * @param array() $data holds company name in the form of an array.
     * @todo this method is used to insert company's name into company's table.
     * @return return value is last inserted id if true else will return false.
     */
    function addCompany($data) {
        if ($this->db->insert('companies', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method addCompanyMarkets() method used to insert company market value in the records.
     * @param array() $data holds company market in the form of an array
     * @todo this method is used to insert company market data in company_market's table.
     * @return value will be added market data in the updated records if true else will return false.
     */
    function addCompanyMarkets($data) {
        if ($this->db->insert_batch('company_markets', $data)) {
            return true;
        }
        return false;
    }

    /**
     * @method getCompanyMarkets() method used to fetch the market id
     * @param varchar $title The chain title
     * @param int $cId hold company id
     * @todo this method is used to fetch company market id from company_market's table.
     * @return value will be resultant compnay market data if true else will return false.
     */
    function getCompanyMarkets($cId) {
        $this->db->select('market_id');
        $this->db->where("company_id", $cId);
        $query = $this->db->get('company_markets');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method getCompany() method used to fetch company's name from records.
     * @param varchar $cname company name - table of companies.
     * @todo this method is used to fetch company's name  from company  table record.
     * @return output is last inserted value if true else will return false.
     */
    function getCompany($cname) {
        $this->db->select('id');
        $this->db->where("company_name", $cname);
        $query = $this->db->get('companies');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getCompanyDetails() method used to fetch id, company name, address, country code, city, head, company  logo primary company id and status
     * @param varchar $cname company name - table of companies.
     * @todo this method is used to fetch company's details as per query.
     * @return output result is details of a company if true else will return false.
     */
    function getCompanyDetails($cid) {
        $this->db->select('id,company_name,address,country_code,city,head,company_logo,primary_company_id,status');
        $this->db->where("id", $cid);
        $query = $this->db->get('companies');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getCompanyLogo() method used to extract company logo
     * @param int $cid company id - primary key of companies.
     * @todo this method is used to fetch company logo from company's table in DB.
     * @return string output value is logo of company if result is true else will return false.
     */
    function getCompanyLogo($cid) {
        $this->db->select('company_logo');
        $this->db->where("id", $cid);
        $query = $this->db->get('companies');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->company_logo;
        } else {
            return false;
        }
        return false;
    }

    /**
     * getCompanyList() method used to fetch company id and name as per query.
     * @todo this method used to fetch company name and id from database.
     * @return output is company name and id if true else will return false.
     */
    function getCompanyList() {
        $this->db->select('id,company_name as title');
        $this->db->order_by('company_name');
        $query = $this->db->get('companies');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delCompany() method used to delete the company value.
     * @param int $did hold company id - primary key of companies.
     * @todo this method is used to  delete company entity from database.
     * @return output is deleted company value if true else ill return false.
     */
    function delCompany($dId) {
        $this->delCompanyMarkets($dId);
        $this->db->where('id', $dId);
        return $this->db->delete('companies');
    }

    /**
     * delCompanyMarkets() method used to delete the company market entity from company_markets table.
     * @param int $cmpId hold company id - primary key of company_markets.
     * @todo this method is used to  delete company market entity from company_markets table.
     * @return output will be deleted company market entity else will return false.
     */
    function delCompanyMarkets($cmpId) {
        $this->db->where('company_id', $cmpId);
        return $this->db->delete('company_markets');
    }

    /**
     * updateCompany() method used to update the compony
     * @param int $dId hold location id - hotel_location_points table.
     * @todo this method is used to  update compony  from company table 
     */
    function updateCompany($dbId, $newdata) {
        $this->db->where('id', $dbId);
        return $this->db->update('companies', $newdata);
    }

    /**
     * delLoction_point() method used to delete the location
     * @param int $dId hold location id
     * @todo this method is used to  delete location  from hotel_location_points table 
     * @return output is deleted location point value if true else will return false.
     */
    function delLoction_point($dId) {
        $this->db->where('id', $dId);
        return $this->db->delete('hotel_location_points');
    }

    /**
     * updateLoction() method used to update the location in DB.
     * @param int $dbId location  id - primary key in updateLocation table.
     * @param array() $newdata holds location in the form of an array.
     * @todo this method is used to update hotel location point in hotel_location_points  table.
     */
    function updateLoction($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->update('hotel_location_points', $newdata);
    }

    /**
     * checkLoctionExists() used method checks if hotel location exists or not
     * @param int $cuntryId country id - primary key.
     * @param int $cid city id - location exist table
     * @param int $did district id - district table.
     * @param string $name holds location name
     * @todo this method is used to verify whether hotel location point exists or not in hotel_location_points table.
     * @return output is last inserted id else will return false.
     */
    function checkLoctionExists($name, $cuntryId = '', $cid = '', $did = '') {
        $this->db->select('id');
        $this->db->where("point_name", $name);
        if ($cid != '')
            $this->db->where("city_id", $cid);
        if ($did != '')
            $this->db->where("id !=", $did);
        if ($cuntryId != '')
            $this->db->where("country_id", $cuntryId);
        $query = $this->db->get('hotel_location_points');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method addLoctionPoint() method used here inserts location point.
     * @param array() $data holds the data in the form of an array
     * @todo this method is used to add location point in hotel_location_points table.
     * @return output value will be last inserted id else will reutn false.
     */
    function addLoctionPoint($data) {
        if ($this->db->insert('hotel_location_points', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method get_location_point()  used to fetch hotel location point.
     * @param int $countryid country id is primary key.
     * @param int $cityId city id location able of hotel_location_points.
     * @todo this method fetches hotel location using country code and city code.
     * @return output value will be hotel location in the form of an array else will return false.
     */
    function get_location_point($cuntryId, $cityId) {
        $this->db->select('id,point_name');
        $this->db->where("country_id", $cuntryId);
        $this->db->where("city_id", $cityId);
        $this->db->order_by('point_name', 'ASC');
        $query = $this->db->get('hotel_location_points');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
}

