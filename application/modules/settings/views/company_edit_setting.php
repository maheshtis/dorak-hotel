<h2>Edit Company : <?php echo $companyDetail->company_name?></h2>

<div class="setting-room-type">
<div class="mian-head setting-section">
<?php echo form_open_multipart($editAction,array('class' => 'company_edit', 'id' => 'company_edit'));?>

<div class="dd">
<div class="dt">
<label>Company Name <span class="vali-star">*</span></label>
<?php echo form_input($edit_company_name)?>
<?php echo form_error('company_name'); ?>
</div>
<div class="dt comp-logo">
<label>Company Logo</label>
<div class="input-group">
	<span class="input-group-btn">
	<span class="btn btn-primary btn-file">
	<img src="<?php echo base_url(); ?>assets/themes/default/images/upload-now.jpg" alt="upload"> Upload Now 
	<?php echo form_upload('logo_file','',' data-validation="mime dimension" tabindex="2"  data-validation-dimension="max300" data-validation-allowing="png,jpeg,jpg,gif,svg"'); ?> 
	<?php echo form_error('logo_file'); ?>
	<input type="text" class="form-control input-file-postion" readonly>
     </span>

 </div>
 <?php 
if($companyDetail->company_logo!="")
{
?>
<a class="logo-icon"href="javascript:void(0);" data-toggle="modal" data-target="#setting-comp-1" > <img src="<?php echo base_url(); ?><?php echo $logo_directory.'/'.$companyDetail->company_logo;?>" width="30" height="30"alt="logo"></a>
<?php
}
?> 
</div>
<div class="dt">
<label>Primary Company</label>
<?php echo form_dropdown('primary_company_id',$primary_companies_options,$companyDetail->primary_company_id,' data-validation="required" data-live-search="true" tabindex="5" id="country_code" class="selectpicker" ')?>   
<?php echo form_error('primary_company_id'); ?>
</div>
</div>

<div class="dd">
<div class="dt textarea-setting">
<label>Address <span class="vali-star">*</span></label>
<?php echo form_textarea($edit_company_address,$companyDetail->address,'tabindex="4"')?>
</div>
<div class="dt city-setting">
<label>Country</label>
<?php echo form_dropdown('country_code',$country_id_options,$companyDetail->country_code,' data-validation="required" data-live-search="true" tabindex="5" id="country_code" class="selectpicker" onchange=getCountryCities(this)')?>   
<?php echo form_error('country_code'); ?>
</div>
<div class="dt city-setting">
<label>City</label>
<div id='city_container'>                                  
<?php  echo form_dropdown('city',$city_options,$companyDetail->city,' data-validation="required" id="city_name" data-live-search="true" tabindex="6" class="selectpicker" ')?>   
<?php echo form_error('city'); ?>
</div>                                     
</div>

</div>

<div class="dd">
<div class="dt">
<label>Head</label>
<?php echo form_input($edit_company_head)?>
</div>
<div class="dt">
<label>Market</label>
<?php  
echo form_dropdown('market_id[]',$markets_options,$companyMarketIds,' class="datacheckoptions" multiple title="Select" data-selected-text-format="count > 2" tabindex="8" ')?>   
<?php echo form_error('market_id'); ?>                                        
</div>
<div class="dt">
<label>Status</label>
<?php  echo form_dropdown('company_status',$statusOptions,$companyDetail->status,' data-validation="required"   tabindex="9" class="selectpicker" ')?>   
<?php echo form_error('company_status'); ?>
</div>
</div>
<input type="submit" value="Save" class="submit-button1">
<div class="cancel_block">
<a class="submit-button1" href="<?php echo base_url(); ?>settings/company">Cancel </a>
</div>
<?php   echo form_close();?>
</div>
</div>

<div class="modal fade cumpany_logo" id="setting-comp-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body cf">
<?php 
if($companyDetail->company_logo!="")
{
?>
<img src="<?php echo base_url(); ?><?php echo $logo_directory.'/'.$companyDetail->company_logo;?>" alt="logo">
<?php
}
?> 
</div>
</div>
</div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
  <script>
  function getCountryCities(ele)
		{
		var baseUrl ="<?php echo base_url()?>";
		var ccode = $(ele).val();
		jQuery.ajax({
		type: "POST",
		url: baseUrl + "index.php/ajaxController/getCountryCities",
		data: {country_code: ccode},
		success: function(res) {
		if (res)
		{
		jQuery("div#city_container").html(res);
		$("div#city_container select").selectpicker();
		}
		}
		});
		}

  $(document).ready(function (){	  
	$.validate({
	form : '#company_edit',
	modules : 'file'
	});  
	$('select.datacheckoptions').multiselect({ 
        numberDisplayed: 1, 
		nonSelectedText: 'Check an option!'		
    });
	 $('.btn-file :file').on('fileselect', function(event, numFiles, label){
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });	
	 
  });
  
    $(document).on('change', '.btn-file :file', function() {
	var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
	});
  
  
</script>