<h2>Company</h2>
<div class="search-box">
<a data-toggle="modal" data-target="#setting-comp-1" class="add-hotel img-pop" href="javascript:void(0);">Add Company</a>
</div>

<div class="setting-room-type">
<div class="mian-head setting-section">
<?php echo form_open($addAction,array('class' => 'section_edit', 'id' => 'section_edit'));?>

<table id="hotel-section-edit-table" class="table settings-table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
<thead>
<tr>
<th>Company</th>
<th class="action-col">Action</th>               
</tr>
</thead>
<tbody>
<?php if($listData && count($listData)>0){
	foreach($listData as $sData)
	{
	?>
	<tr><td><label id="label-block-<?php echo $sData->id?>"><?php echo $sData->title;?></label>
	</td>
	<td>
	<span class="edit-del-action">
	<a title="Edit" rel="<?php echo $sData->id?>" href="<?php echo $editAction ?>/<?php echo $sData->id?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-edit.jpg" alt="edit"></a> 
	<a title="Delete" href="<?php echo $delAction ?>/<?php echo $sData->id?>/<?php echo $sData->title?>" onclick="return confirm('Are you sure you want to remove <?php echo !empty($sData->title) ? $sData->title : '';?> company?');"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-remove.jpg" alt="remove"></a>
	</span></td></tr>
	<?php 
	} 
	} 
	?>
</tbody>
</table>
<?php   echo form_close();?>
</div>
</div>

<div class="modal fade newcoustomer" id="setting-comp-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h3 class="modal-title" id="myModalLabel">Add Company</h3>
</div>
<div class="modal-body cf">
<?php echo form_open_multipart($addAction,array('class' => 'company_add', 'id' => 'company_add'));?>

<div class="dd">
<div class="dt">
<label>Company Name<span class="vali-star">*</span></label>
<?php echo form_input($company_name)?>
<?php echo form_error('company_name'); ?>
</div>
<div class="dt comp-logo">
<label>Company Logo</label>
<div class="input-group">
	<span class="input-group-btn">
	<span class="btn btn-primary btn-file">
	<img src="<?php echo base_url(); ?>assets/themes/default/images/upload-now.jpg" alt="upload"> Upload Now 
	<?php echo form_upload('logo_file','',' data-validation="mime dimension" tabindex="2" data-validation-dimension="max300" data-validation-allowing="png,jpeg,jpg,gif,svg"'); ?> 
	<?php echo form_error('logo_file'); ?>
	<input type="text" class="form-control input-file-postion" readonly>
     </span>										
   </div>
</div>
<div class="dt">
<label>Primary Company</label>

<?php echo form_dropdown('primary_company_id',$primary_companies_options,$posted_primary_company_id,' data-validation="required" data-live-search="true" tabindex="5" id="country_code" class="selectpicker" ')?>   
<?php echo form_error('primary_company_id'); ?>
</div>
</div>

<div class="dd">
<div class="dt textarea-setting">
<label>Address <span class="vali-star">*</span></label>
<?php echo form_textarea($company_address,$posted_address,'tabindex="4"')?>
</div>
<div class="dt city-setting">
<label>Country</label>
<?php echo form_dropdown('country_code',$country_id_options,$posted_country_code,' data-validation="required" data-live-search="true" tabindex="5" id="country_code" class="selectpicker" onchange=getCountryCities(this)')?>   
<?php echo form_error('country_code'); ?>
</div>
<div class="dt city-setting">
<label>City</label>
<div id='city_container'>                                  
<?php  echo form_dropdown('city',$city_options,$posted_city,' data-validation="required" id="city_name" data-live-search="true" tabindex="6" class="selectpicker" ')?>   
<?php echo form_error('city'); ?>
</div>                                     
</div>

</div>

<div class="dd">
<div class="dt">
<label>Head</label>
<?php echo form_input($company_head)?>
</div>
<div class="dt">
<label>Market</label>
 <?php  echo form_dropdown('market_id[]',$markets_options,$posted_market_id,' class="datacheckoptions" multiple title="Select" data-selected-text-format="count > 2" tabindex="8" title="Select" ')?>   
<?php echo form_error('market_id'); ?>
                                          
</div>
<div class="dt">
<label>Status</label>
<?php  echo form_dropdown('company_status',$statusOptions,$posted_company_status,' data-validation="required"   tabindex="9" class="selectpicker" ')?>   
<?php echo form_error('company_status'); ?>
</div>

</div>

<h3>Add Member</h3>
<ul class="main-head">
<li>Name </li>
<li>Position</li>
<li>Email</li>
</ul>
<ul>
<li>
<div class="dd">
<div class="dt_member_wraper">
<div class="dt">
<?php echo form_input($member_name); ?> 
<?php echo form_error('member[1][name]'); ?>
</div>
<div class="dt">
 <?php  echo form_dropdown('member[1][position]',$position_options, $posted_position,' id="contact_1_position"  class="selectpicker"')?>   
	<?php echo form_error('member[1][position]'); ?>
</div>
<div class="dt">
<?php echo form_input($member_email); ?> 
<?php echo form_error('member[1][email]'); ?>
</div>
</div>
<div class="add_lnk">
 <a href="javascript:void(0)" class='add_member_row'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="add"></a>
</div>
</div>
</li>
</ul>

<input type="submit" value="Save" class="submit-button1">
<?php   echo form_close();?>
</div>

</div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
  <script>
  
  function getCountryCities(ele)
		{
		var baseUrl ="<?php echo base_url()?>";
		var ccode = $(ele).val();
		jQuery.ajax({
		type: "POST",
		url: baseUrl + "index.php/ajaxController/getCountryCities",
		data: {country_code: ccode},
		success: function(res) {
		if (res)
		{
		jQuery("div#city_container").html(res);
		$("div#city_container select").selectpicker();
		}
		}
		});
		}

  $(document).ready(function (){	  
	$.validate({
	form : '#section_edit'
	});  
	
	$.validate({
	form : '#company_add',
	modules : 'file'
	});  
	$('select.datacheckoptions').multiselect({ 
        numberDisplayed: 1, 
		nonSelectedText: 'Check an option!'		
    });
	 $('.btn-file :file').on('fileselect', function(event, numFiles, label){
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });	
	 
   var table = $('#hotel-section-edit-table').DataTable(
   {
   "order": [[ 0, "asc" ]],
	  "searching": false,
		"lengthMenu": [[10, 25, 50, -1], [" 10 Per Page"," 25 Per Page", " 50 Per Page", "All"]],
		language : {
        sLengthMenu: "View: _MENU_"
	},
	"columns": [
    { "width": "90%" },
    { "width": "10%" }
	],
  "aoColumns": [
		null,
		{ "bSortable": false }
		]
	}
   );
   
   var baseUrl ="<?php echo base_url()?>";
   	 var arw = 1; //Initial field counter is 2
     $('.add_member_row').click(function(e){ //Once add more link is clicked      
            e.preventDefault();
			arw++; //Increment field counter
           var fieldHTML= getNextMemberRow(arw,baseUrl);  
    });
    
    $('.dt_member_wraper').on('click', '.remove_lnk', function(e){
		 //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        arw--; //Decrement field counter
    });
	
	$("#city_name").change(function(){
			var el=$(this).parent('div').parent();
			el.addClass('des');
			var rm  = el.find('span.form-error');
			rm.remove();
});

  });
  
    $(document).on('change', '.btn-file :file', function() {
	var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
	});
	
function getNextMemberRow(n,baseUrl)
	{
		/* get new contact row using ajax */		
		jQuery.ajax({
		type: "POST",
		url: baseUrl + "index.php/ajaxController/getNextMemberRow",
		data: {row_num: n},
		success: function(res) {
		if (res)
		{
		$('.dt_member_wraper').append(res); 
		$('.dt_member_wraper select').selectpicker();
		callbackValidator();
		}
		else{
			return '';
			}
		}
	});
}	
	
  
</script>