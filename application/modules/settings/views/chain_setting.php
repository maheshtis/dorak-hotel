<h2>Hotel Chain</h2>
<div class="search-box">
<a class="add-hotel" href="javascript:void(0);" id="imageid">Add Hotel Chain</a>
<div id="toggle">
    <div class = pull-right id = "import_curid">
        <img src="<?php echo base_url(); ?>assets/themes/default/images/close-estimate.png" alt="add">
    </div>
<?php echo form_open_multipart($addAction,array('class' => 'hotel_chainadd', 'id' => 'hotel_chainadd'));?>
<?php echo form_input($chain_name)?>
<input class="ok" type="submit" value="OK">
<?php   echo form_close();?>
</div>
</div>
<div class="setting-room-type">
<div class="mian-head setting-section">
<?php echo form_open($addAction,array('class' => 'section_edit', 'id' => 'section_edit'));?>

<table id="hotel-chain-table" class="table settings-table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
<thead>
<tr>
<th>Name</th>
<th class="action-col">Action</th>               
</tr>
</thead>
<tbody>

<?php if($hotel_chains && count($hotel_chains)>0){
	foreach($hotel_chains as $chain)
	{
	?>
	<tr><td><label id="label-block-<?php echo $chain->id?>"><?php echo $chain->entity_title;?></label><span class="edit-block" style="display:none;" id="edit-block-<?php echo $chain->id?>"><input type="text" maxlength = "50" data-validation-length="min3" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation="required,custom,length" id="chain_<?php echo $chain->id?>" name="chain_title<?php echo $chain->id?>" value="<?php echo $chain->entity_title;?>">
	<a class="save_button" id="savelnk_<?php echo $chain->id?>" onclick="inlinesaveAction(<?php echo $chain->id;?>)">save</a>
	<a class="cancel_button" id="cancellnk_<?php echo $chain->id?>" onclick="cancelsaveAction(<?php echo $chain->id;?>)">cancel</a>
	</span></td>
	<td>
	<span class="edit-del-action">
	<a title="Edit" rel="<?php echo $chain->id?>" href="javascript:void(0);" onclick="toggleEdit(<?php echo $chain->id?>)"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-edit.jpg" alt="edit"></a> 
        <a title="Delete" href="<?php echo $delAction ?>/<?php echo $chain->id;?>/<?php echo $chain->entity_title;?>" onclick="return confirm('Are you sure you want to remove <?php echo !empty($chain->entity_title) ? $chain->entity_title : '';?> hotel chain?');"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-remove.jpg" alt="remove"></a>
	</span></td></tr>
	<?php 
	} 
	} 
	?>
</tbody>
</table>
<?php   echo form_close();?>


</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
  <script>
  $(document).on('click','#imageid',function(){
    var $this= $(this);
    $('#toggle').toggle();   
   $.validate({
	form : '#hotel_chainadd'
	});
  });
  
  $(document).ready(function (){
      
	$.validate({
	form : '#section_edit'
	}); 
	
   var table = $('#hotel-chain-table').DataTable(
   {
	"aoColumns": [
      null,
      { "bSortable": false }
    ],
   "order": [[ 0, "asc" ]],
	  "searching": false,
		"lengthMenu": [[10, 25, 50, -1], [" 10 Per Page"," 25 Per Page", " 50 Per Page", "All"]],
		language : {
        sLengthMenu: "View: _MENU_"
  },
  "columns": [
    { "width": "90%" },
    { "width": "10%" }
  ]
  }
   );
  });
  
  function inlinesaveAction(rowid)
  { 
	$('#chain_'+rowid).validate(function(valid, elem) {
	if(valid)
	 {
	 var newval= $('#chain_'+rowid).val();
	 $('#edit-block-'+rowid).hide();  
	 $('#label-block-'+rowid).show(); 
	 jQuery.ajax({
		type: "POST",
		url:"<?php echo $editAction?>",
		data: {newname: newval,id: rowid},
		success: function(res) {			
		if (res=='exists')
		{
		alert(newval +' Already exists.');
		}
		else if(res==newval)
		{
		$('#chain_'+rowid).val(res);	
		$('#label-block-'+rowid).html(res);
		}
		}
		});
	  }
	  });	
  }
  function toggleEdit(rowid)
  {
	   $('#edit-block-'+rowid).toggle(); 
	   $('#label-block-'+rowid).toggle(); 	   
  }
   function cancelsaveAction(rowid)
  {
	$('#edit-block-'+rowid).hide();  
	 $('#label-block-'+rowid).show();   
  }
  
</script>