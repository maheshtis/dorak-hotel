<h2>Country </h2>
<div class="search-box">
<a class="add-hotel" href="javascript:void(0);" id="imageid">Add Country</a>
<div id="toggle">
    <div class = pull-right id = "import_curid">
        <img src="<?php echo base_url(); ?>assets/themes/default/images/close-estimate.png" alt="add">
    </div>
<?php echo form_open_multipart($addAction,array('class' => 'country_add', 'id' => 'country_add'));?>
<?php echo form_input($add_input_name)?>
<?php echo form_input($add_input_fld2)?>
<input class="ok" type="submit" value="OK">
<?php   echo form_close();?>
</div>
</div>
<div class="setting-room-type">
<?php echo form_open($addAction,array('class' => 'country_edit', 'id' => 'country_edit'));?>

<div class="mian-head setting-section">
<table id="hotel-country-table" class="table settings-table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
<thead>
<tr>
<th>Country</th>
<th>Code</th>
<th class="action-col" style="width:200px;">Action</th>               
</tr>
</thead>
<tbody>
<?php if($listData && count($listData)>0){
	foreach($listData as $sData)
	{
	?>
	<tr>
	<td><label id="label-block-<?php echo $sData->id?>">
	<?php echo $sData->country_name;?></label><span class="edit-block" style="display:none;" id="edit-block-<?php echo $sData->id?>">
	<input type="text" maxlength = "50" id="new-title-<?php echo $sData->id?>" name="new_title_<?php echo $sData->id?>" data-validation-length="min2" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation="required,custom,length"  value="<?php echo $sData->country_name;?>">
	</span></td>
	<td>
	<label id="label-cval-block-<?php echo $sData->id?>">
	<?php echo $sData->code;?></label><span class="edit-block" style="display:none;" id="edit-cval-block-<?php echo $sData->id?>">
	<input type="text" maxlength = "8" id="new-cval-<?php echo $sData->id?>" data-validation-length="min2" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation="required,custom,length" name="new_cval_<?php echo $sData->id?>" value="<?php echo $sData->code;?>">
	</td>
	
	<td>
	<span class="action-span" id="additional-action-blok-<?php echo $sData->id?>" style="display:none;">
	<a class="save_button"  id="savelnk_cval-<?php echo $sData->id?>" onclick="inlinesaveAction(<?php echo $sData->id;?>)">save</a>
	<a class="cancel_button" id="cancellnk_<?php echo $sData->id?>" onclick="cancelsaveAction(<?php echo $sData->id;?>)">cancel</a>
	</span>
	<span class="edit-del-action">
	<a title="Edit" rel="<?php echo $sData->id?>" href="javascript:void(0);" onclick="toggleEdit(<?php echo $sData->id?>)"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-edit.jpg" alt="edit"></a> 
	<a title="Delete" href="<?php echo $delAction ?>/<?php echo $sData->id?>/<?php echo $sData->code?>/<?php echo $sData->country_name?>" onclick="return confirm('Are you sure you want to remove <?php echo !empty($sData->country_name) ? $sData->country_name : '';?> country?');"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-remove.jpg" alt="remove"></a>
	<a title="Cities"  href="<?php echo $addCitiesUrl?>/<?php echo $sData->id?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-city.jpg" alt="Cities"></a> 
	
	
	</span></td></tr>
	<?php 
	} 
	} 
	?>
</tbody>
</table>
</div>
<?php   echo form_close();?>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
<script>
  $(document).on('click','#imageid',function(){
    var $this= $(this);
    $('#toggle').toggle();
		
   $.validate({
	form : '#country_add'
	});
  });
  
 $(document).ready(function (){  
 $.validate({
	form : '#country_edit'
	});
   var table = $('#hotel-country-table').DataTable(
   {
	//  "searching": false,
	"autoWidth": false,
		"lengthMenu": [[10, 25, 50, -1], [" 10 Per Page"," 25 Per Page", " 50 Per Page", "All"]],
		language : {
        sLengthMenu: "View: _MENU_"
  },
  "columns": [
    { "width": "35%" },
	{ "width": "35%" },
    { "width": "30%" }
  ],
  
  "aoColumns": [
      null,
      null,
      { "bSortable": false }
    ],
	 "order": [[ 0, "asc" ], [ 1, 'asc' ]]
  }
   );
   
 // table.search( '' ) .columns().search( '' ) .draw();
 
  });
  
 
  function inlinesaveAction(rowid)
  {
	$('#new-title-'+rowid).validate(function(valid, elem) {
	if(valid)
	 { 
	$('#new-cval-'+rowid).validate(function(valid, elem) {
	if(valid)
	 {
	 var newval= $('#new-title-'+rowid).val();
	 var country_code= $('#new-cval-'+rowid).val();
	 $('#edit-block-'+rowid).hide();  
	 $('#label-block-'+rowid).show(); 
	 $('#edit-cval-block-'+rowid).hide();  
	 $('#label-cval-block-'+rowid).show(); 
	 $('#additional-action-blok-'+rowid).hide(); 
	 jQuery.ajax({
		type: "POST",
		url:"<?php echo $editAction?>",
		dataType: "json",
		data: {newname: newval,code: country_code,id: rowid},
		success: function(res) {
		if(res["error"]==false)
		{
		$('#new-title-'+rowid).val(res["country"]);	
		$('#label-block-'+rowid).html(res["country"]);
		$('#new-cval-'+rowid).val(res["country_code"]);	
		$('#label-cval-block-'+rowid).html(res["country_code"]);
		}
		else if(res["error"]==true){
			alert( res["msg"]);
		}
		}
		});
		}
	  });
	 }
	 });
  }
  function toggleEdit(rowid)
  {
	   $('#edit-block-'+rowid).toggle(); 
	   $('#edit-cval-block-'+rowid).toggle(); 
	   $('#label-block-'+rowid).toggle(); 	
	   $('#label-cval-block-'+rowid).toggle(); 	
	  $('#additional-action-blok-'+rowid).toggle(); 	   
  }
  
 function cancelsaveAction(rowid)
  {
	$('#edit-block-'+rowid).hide();  
	$('#label-block-'+rowid).show();  
	$('#edit-cval-block-'+rowid).hide();  
	$('#label-cval-block-'+rowid).show(); 
	$('#additional-action-blok-'+rowid).hide(); 	 
  }
  
</script>