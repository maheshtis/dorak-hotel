<h2><?php echo $city_name?>  Districts</h2>
<div class="search-box">
<div class="tg1">
<a class="add-hotel" href="javascript:void(0);" id="imageid">Add District</a>
<div id="toggle">
    <div class = pull-right id = "import_curid">
        <img src="<?php echo base_url(); ?>assets/themes/default/images/close-estimate.png" alt="add">
    </div>
<?php echo form_open_multipart($addAction,array('class' => 'district_add', 'id' => 'district_add'));?>
<?php echo form_input($add_input_name)?>
<input class="ok" type="submit" value="OK">
<?php   echo form_close();?>
</div>
</div>
<div class="tg2">
<a class="add-hotel" href="<?php echo base_url(); ?>settings/country/cities/<?php echo $country_id?>">Back to cities list</a>
</div>
</div>
<div class="setting-room-type">
<?php echo form_open($addAction,array('class' => 'district_edit', 'id' => 'district_edit'));?>

<div class="mian-head setting-section">
<table id="district-table" class="table settings-table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
<thead>
<tr>
<th>District</th>
<th class="action-col" style="width:158px;">Action</th>               
</tr>
</thead>
<tbody>
<?php if($listData && count($listData)>0){
	foreach($listData as $sData)
	{
	?>
	<tr>
	<td><label id="label-block-<?php echo $sData->id?>">
	<?php echo $sData->district_name;?></label><span class="edit-block" style="display:none;" id="edit-block-<?php echo $sData->id?>">
	<input type="text" id="new-title-<?php echo $sData->id?>" name="new_title_<?php echo $sData->id?>" data-validation-length="min3" maxlength = "50" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation="required,custom,length"  value="<?php echo $sData->district_name;?>">
	</span>
	<span class="action-span" id="additional-action-blok-<?php echo $sData->id?>" style="display:none;">
	<a class="save_button"  id="savelnk_cval-<?php echo $sData->id?>" onclick="inlinesaveAction(<?php echo $sData->id;?>)">save</a>
	<a class="cancel_button" id="cancellnk_<?php echo $sData->id?>" onclick="cancelsaveAction(<?php echo $sData->id;?>)">cancel</a>
	</span>
	</td>
	<td>
	<span class="edit-del-action">
	<a title="Edit" rel="<?php echo $sData->id?>" href="javascript:void(0);" onclick="toggleEdit(<?php echo $sData->id?>)"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-edit.jpg" alt="edit"></a> 
	<a title="Delete" href="<?php echo $delAction ?>/<?php echo $sData->id?>/<?php echo $sData->district_name?>" onclick="return confirm('Are you sure you want to remove <?php echo !empty($sData->district_name) ? $sData->district_name : '';?> district?');"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-remove.jpg" alt="remove"></a>
	
	</span></td></tr>
	<?php 
	} 
	} 
	?>
</tbody>
</table>
</div>
<?php   echo form_close();?>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
<script>
  $(document).on('click','#imageid',function(){
    var $this= $(this);
    $('#toggle').toggle();
		
   $.validate({
	form : '#district_add'
	});
  });
  
 $(document).ready(function (){  
 $.validate({
	form : '#district_edit'
	});
   var table = $('#district-table').DataTable(
   {
	//  "searching": false,
	"autoWidth": false,
		"lengthMenu": [[10, 25, 50, -1], [" 10 Per Page"," 25 Per Page", " 50 Per Page", "All"]],
		language : {
        sLengthMenu: "View: _MENU_"
  },
  "columns": [
    { "width": "80%" },
    { "width": "20%" }
  ],
  
  "aoColumns": [
      null,
      { "bSortable": false }
    ],
	 "order": [[ 0, "asc" ], [ 1, 'asc' ]]
  }
   );
   
  });
  
 
  function inlinesaveAction(rowid)
  {
	  
	$('#new-title-'+rowid).validate(function(valid, elem) {
	if(valid)
	 { 	 
	 var newval= $('#new-title-'+rowid).val();
	 $('#edit-block-'+rowid).hide();  
	 $('#label-block-'+rowid).show(); 
	 $('#additional-action-blok-'+rowid).hide(); 
	 jQuery.ajax({
		type: "POST",
		url:"<?php echo $editAction?>",
		dataType: "json",
		data: {district_name: newval,district_id: rowid},
		success: function(res) {
		if(res["error"]==false)
		{
		$('#new-title-'+rowid).val(res["district_name"]);	
		$('#label-block-'+rowid).html(res["district_name"]);
		}
		else if(res["error"]==true){
			alert( res["msg"]);
		}
		}
		});
	 }
	 });
  }
  function toggleEdit(rowid)
  {
	   $('#edit-block-'+rowid).toggle(); 
	   $('#label-block-'+rowid).toggle(); 	
	  $('#additional-action-blok-'+rowid).toggle(); 	   
  }
  
 function cancelsaveAction(rowid)
  {
	$('#edit-block-'+rowid).hide();  
	$('#label-block-'+rowid).show();  
	$('#additional-action-blok-'+rowid).hide(); 	 
  }
  
</script>