<h2><?php echo $country_name?>  Cities</h2>
<div class="search-box">
<div class="tg1">
<a class="add-hotel" href="javascript:void(0);" id="imageid">Add City</a>
<div id="toggle">
    <div class = pull-right id = "import_curid">
        <img src="<?php echo base_url(); ?>assets/themes/default/images/close-estimate.png" alt="add">
    </div>
<?php echo form_open_multipart($addAction,array('class' => 'city_add', 'id' => 'city_add'));?>
<?php echo form_input($add_input_name)?>
<?php echo form_input($add_input_fld2)?>
<input class="ok" type="submit" value="OK">
<?php   echo form_close();?>
</div>
</div>
<div class="tg2">
<a class="add-hotel" href="<?php echo base_url(); ?>settings/country">Back to countries list</a>
</div>
</div>
<div class="setting-room-type">
<?php echo form_open($addAction,array('class' => 'city_edit', 'id' => 'city_edit'));?>

<div class="mian-head setting-section">
<table id="cities-table" class="table settings-table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
<thead>
<tr>
<th>City</th>
<th>Code</th>
<th class="action-col" style="width:158px;">Action</th>               
</tr>
</thead>
<tbody>
<?php if($listData && count($listData)>0){
	foreach($listData as $sData)
	{
	?>
	<tr>
	<td><label id="label-block-<?php echo $sData->id?>">
	<?php echo $sData->city_name;?></label><span class="edit-block" style="display:none;" id="edit-block-<?php echo $sData->id?>">
	<input type="text" id="new-title-<?php echo $sData->id?>" name="new_title_<?php echo $sData->id?>" data-validation-length="min3" maxlength = "50" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation="required,custom,length"  value="<?php echo $sData->city_name;?>">
	</span></td>
	<td>
	<label id="label-cval-block-<?php echo $sData->id?>">
	<?php echo $sData->city_code;?></label><span class="edit-block" style="display:none;" id="edit-cval-block-<?php echo $sData->id?>">
	<input type="text" id="new-cval-<?php echo $sData->id?>" data-validation-length="min2" maxlength = "8" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation="required,custom,length" name="new_cval_<?php echo $sData->id?>" value="<?php echo $sData->city_code;?>">
	</td>
	
	<td>
	<span class="action-span" id="additional-action-blok-<?php echo $sData->id?>" style="display:none;">
	<a class="save_button"  id="savelnk_cval-<?php echo $sData->id?>" onclick="inlinesaveAction(<?php echo $sData->id;?>)">save</a>
	<a class="cancel_button" id="cancellnk_<?php echo $sData->id?>" onclick="cancelsaveAction(<?php echo $sData->id;?>)">cancel</a>
	</span>
	<span class="edit-del-action">
	<a title="Edit" rel="<?php echo $sData->id?>" href="javascript:void(0);" onclick="toggleEdit(<?php echo $sData->id?>)"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-edit.jpg" alt="edit"></a> 
	<a title="Delete" href="<?php echo $delAction ?>/<?php echo $sData->id?>/<?php echo $sData->city_name?>" onclick="return confirm('Are you sure you want to remove <?php echo !empty($sData->city_name) ? $sData->city_name : '';?> city?');"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-remove.jpg" alt="remove"></a>
	<a title="Districts"  href="<?php echo $addDistrictUrl?>/<?php echo $sData->id?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-city.jpg" alt="Districts"></a>
	<a title="Location"  href="<?php echo $addlocationUrl?>/<?php echo $sData->id?>"><img src="<?php echo base_url();?>/assets/themes/default/images/pin.png" alt="Location Point"></a>
	
	</span></td></tr>
	<?php 
	} 
	} 
	?>
</tbody>
</table>
</div>
<?php   echo form_close();?>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
<script>
  $(document).on('click','#imageid',function(){
    var $this= $(this);
    $('#toggle').toggle();
		
   $.validate({
	form : '#city_add'
	});
  });
  
 $(document).ready(function (){  
 $.validate({
	form : '#city_edit'
	});
   var table = $('#cities-table').DataTable(
   {
	//  "searching": false,
	"autoWidth": false,
		"lengthMenu": [[10, 25, 50, -1], [" 10 Per Page"," 25 Per Page", " 50 Per Page", "All"]],
		language : {
        sLengthMenu: "View: _MENU_"
  },
  "columns": [
    { "width": "40%" },
	{ "width": "35%" },
    { "width": "20%" }
  ],
  
  "aoColumns": [
      null,
      null,
      { "bSortable": false }
    ],
	 "order": [[ 0, "asc" ], [ 1, 'asc' ]]
  }
   );
   
 // table.search( '' ) .columns().search( '' ) .draw();
 
  });
  
 
  function inlinesaveAction(rowid)
  {
	  
	$('#new-title-'+rowid).validate(function(valid, elem) {
	if(valid)
	 { 
	$('#new-cval-'+rowid).validate(function(valid, elem) {
	if(valid)
	 {
		 
	 var newval= $('#new-title-'+rowid).val();
	 var city_code= $('#new-cval-'+rowid).val();
	 $('#edit-block-'+rowid).hide();  
	 $('#label-block-'+rowid).show(); 
	 $('#edit-cval-block-'+rowid).hide();  
	 $('#label-cval-block-'+rowid).show(); 
	 $('#additional-action-blok-'+rowid).hide(); 
	 jQuery.ajax({
		type: "POST",
		url:"<?php echo $editAction?>",
		dataType: "json",
		data: {cityname: newval,citycode: city_code,ctyid: rowid},
		success: function(res) {
		if(res["error"]==false)
		{
		$('#new-title-'+rowid).val(res["city_name"]);	
		$('#label-block-'+rowid).html(res["city_name"]);
		$('#new-cval-'+rowid).val(res["city_code"]);	
		$('#label-cval-block-'+rowid).html(res["city_code"]);
		}
		else if(res["error"]==true){
			alert( res["msg"]);
		}
		}
		});
		}
	  });
	 }
	 });
  }
  function toggleEdit(rowid)
  {
	   $('#edit-block-'+rowid).toggle(); 
	   $('#edit-cval-block-'+rowid).toggle(); 
	   $('#label-block-'+rowid).toggle(); 	
	   $('#label-cval-block-'+rowid).toggle(); 	
	  $('#additional-action-blok-'+rowid).toggle(); 	   
  }
  
 function cancelsaveAction(rowid)
  {
	$('#edit-block-'+rowid).hide();  
	$('#label-block-'+rowid).show();  
	$('#edit-cval-block-'+rowid).hide();  
	$('#label-cval-block-'+rowid).show(); 
	$('#additional-action-blok-'+rowid).hide(); 	 
  }
  
</script>