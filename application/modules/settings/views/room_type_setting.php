<h2>Room Type</h2>
<div class="search-box">
<a class="add-hotel" href="javascript:void(0);" id="imageid">Add Room Type</a>
<div id="toggle">
    <div class = pull-right id = "import_curid">
        <img src="<?php echo base_url(); ?>assets/themes/default/images/close-estimate.png" alt="add">
    </div>
<?php echo form_open_multipart($addAction,array('class' => 'hotel_roomtypeadd', 'id' => 'hotel_roomtypeadd'));?>
<?php echo form_input($add_input_name)?>
<input class="ok" type="submit" value="OK">
<?php   echo form_close();?>
</div>
</div>
<div class="setting-room-type">
<div class="mian-head setting-section">
<?php echo form_open($addAction,array('class' => 'section_edit', 'id' => 'section_edit'));?>

<table id="hotel-rtype-table" class="table settings-table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
<thead>
<tr>
<th>Type</th>
<th class="action-col">Action</th>               
</tr>
</thead>
<tbody>
<?php if($listData && count($listData)>0){
	foreach($listData as $sData)
	{
	?>
	<tr><td><label id="label-block-<?php echo $sData->id?>"><?php echo $sData->entity_title;?></label><span class="edit-block" style="display:none;" id="edit-block-<?php echo $sData->id?>"><input type="text" maxlength = "50" data-validation-length="min2" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation="required,custom,length"  id="new-title-<?php echo $sData->id?>" name="new_title_<?php echo $sData->id?>" value="<?php echo $sData->entity_title;?>">
	<a class="save_button" id="savelnk_<?php echo $sData->id?>" onclick="inlinesaveAction(<?php echo $sData->id;?>)">save</a>
	<a class="cancel_button" id="cancellnk_<?php echo $sData->id?>" onclick="cancelsaveAction(<?php echo $sData->id;?>)">cancel</a>
	
	</span></td>
	<td>
	<span class="edit-del-action">
	<a title="Edit" rel="<?php echo $sData->id?>" href="javascript:void(0);" onclick="toggleEdit(<?php echo $sData->id?>)"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-edit.jpg" alt="edit"></a> 
	<a title="Delete" href="<?php echo $delAction ?>/<?php echo $sData->id?>/<?php echo $sData->entity_title?>" onclick="return confirm('Are you sure you want to remove <?php echo !empty($sData->entity_title) ? $sData->entity_title : '';?> room type?');"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-remove.jpg" alt="remove"></a>
	</span></td></tr>
	<?php 
	} 
	} 
	?>
</tbody>
</table>
<?php   echo form_close();?>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
  <script>
  $(document).on('click','#imageid',function(){
    var $this= $(this);
    $('#toggle').toggle();   
   $.validate({
	form : '#hotel_roomtypeadd'
	});
  });
  $(document).ready(function (){
	  $.validate({
	form : '#section_edit'
	});  
	  
   var table = $('#hotel-rtype-table').DataTable(
   {
   "order": [[ 0, "asc" ]],
	  "searching": false,
		"lengthMenu": [[10, 25, 50, -1], [" 10 Per Page"," 25 Per Page", " 50 Per Page", "All"]],
		language : {
        sLengthMenu: "View: _MENU_"
  },
  "columns": [
    { "width": "90%" },
    { "width": "10%" }
  ],
  "aoColumns": [
      null,
      { "bSortable": false }
    ]
  }
   );
  });
  
  function inlinesaveAction(rowid)
  {
	$('#new-title-'+rowid).validate(function(valid, elem) {
	if(valid)
	 { 
	 var newval= $('#new-title-'+rowid).val();
	 $('#edit-block-'+rowid).hide();  
	 $('#label-block-'+rowid).show(); 
	 jQuery.ajax({
		type: "POST",
		url:"<?php echo $editAction?>",
		dataType: "json",
		data: {newname: newval,id: rowid},
		success: function(res) {

			if(res["error"]==false)
					{
					$('#new-title-'+rowid).val(res["newname"]);	
					$('#label-block-'+rowid).html(res["newname"]);
					}
				else{
					alert( res["error"]);
					}
		}
		});
		}
	  });
  }
  function toggleEdit(rowid)
  {
	   $('#edit-block-'+rowid).toggle(); 
	   $('#label-block-'+rowid).toggle(); 	   
  }
   function cancelsaveAction(rowid)
  {
	$('#edit-block-'+rowid).hide();  
	 $('#label-block-'+rowid).show();   
  }
  
</script>