<h2>Role</h2>
<div class="search-box">
<a class="add-hotel" href="javascript:void(0);" id="imageid">Add Role</a>
<div id="toggle">
    <div class = pull-right id = "import_curid">
        <img src="<?php echo base_url(); ?>assets/themes/default/images/close-estimate.png" alt="add">
    </div>
<?php echo form_open_multipart($addAction,array('class' => 'hotel_role_add', 'id' => 'hotel_role_add'));?>
<?php echo form_input($add_input_name)?>
<?php echo form_dropdown('access_level_id',$access_levels,'',' tabindex="2" id="access_level_id" class="selectpicker" ')?>   
<input class="ok" type="submit" value="OK">
<?php   echo form_close();?>
</div>
</div>
<div class="setting-room-type">
<div class="mian-head setting-section">
<?php echo form_open($addAction,array('class' => 'section_edit', 'id' => 'section_edit'));?>

<table id="role-section-edit-table" class="table settings-table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
<thead>
<tr>
<th>Role</th>
<th>Access Level</th>
<th class="action-col">Action</th>               
</tr>
</thead>
<tbody>
<?php if($listData && count($listData)>0){
	foreach($listData as $sData)
	{
	?>
	<tr><td><label id="label-block-<?php echo $sData->id?>"><?php echo $sData->title;?></label><span class="edit-block" style="display:none;" id="edit-block-<?php echo $sData->id?>"><input type="text" maxlength = "50" data-validation-length="min2" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation="required,custom,length"  id="new-title-<?php echo $sData->id?>" name="new_title_<?php echo $sData->id?>" value="<?php echo $sData->title;?>">
	</span></td>
	<td>
	<label id="label-cval-block-<?php echo $sData->id?>">
	<?php echo getAccessLevelTitle($sData->access_level_id);?></label>
	<span class="edit-block" style="display:none;" id="edit-cval-block-<?php echo $sData->id?>">
	<?php echo form_dropdown('access_level_id',$access_levels,$sData->access_level_id,' tabindex="2"  id="new-cval-'.$sData->id.'" class="selectpicker" ')?>   
	</span>
	</td>
	<td>
	<span class="action-span" id="additional-action-blok-<?php echo $sData->id?>" style="display:none;">
	<a class="save_button"  id="savelnk_cval-<?php echo $sData->id?>" onclick="inlinesaveAction(<?php echo $sData->id;?>)">save</a>
	<a class="cancel_button" id="cancellnk_<?php echo $sData->id?>" onclick="cancelsaveAction(<?php echo $sData->id;?>)">cancel</a>
	</span>
	<span class="edit-del-action">
	<a title="Edit" rel="<?php echo $sData->id?>" href="javascript:void(0);" onclick="toggleEdit(<?php echo $sData->id?>)"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-edit.jpg" alt="edit"></a> 
	<a title="Delete" href="<?php echo $delAction ?>/<?php echo $sData->id?>/<?php echo $sData->title?>" onclick="return confirm('Are you sure you want to remove <?php echo !empty($sData->title) ? $sData->title : '';?> role?');"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-remove.jpg" alt="remove"></a>
	</span></td></tr>
	<?php 
	} 
	} 
	?>
</tbody>
</table>
<?php   echo form_close();?>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
  <script>
  $(document).on('click','#imageid',function(){
    var $this= $(this);
    $('#toggle').toggle();   
   $.validate({
	form : '#hotel_role_add'
	});
  });
  $(document).ready(function (){
	  $.validate({
	form : '#section_edit'
	});  
	  
   var table = $('#role-section-edit-table').DataTable(
   {
   "order": [[ 0, "asc" ]],
	  "searching": false,
		"lengthMenu": [[10, 25, 50, -1], [" 10 Per Page"," 25 Per Page", " 50 Per Page", "All"]],
		language : {
        sLengthMenu: "View: _MENU_"
  },
  "columns": [
    { "width": "40%" },
	  { "width": "40%" },
    { "width": "10%" }
  ],
  "aoColumns": [
      null,
	  { "bSortable": false },
      { "bSortable": false }
    ]
  }
   );
  });
  
 
  function inlinesaveAction(rowid)
  {
	
	$('#new-title-'+rowid).validate(function(valid, elem) {
	if(valid)
	 { 
     var access_level_id= $('#new-cval-'+rowid).val();	
	 var newval= $('#new-title-'+rowid).val();
	 var access_level_id= $('#new-cval-'+rowid).val();
	 $('#edit-block-'+rowid).hide();  
	 $('#label-block-'+rowid).show(); 
	 $('#edit-cval-block-'+rowid).hide();  
	 $('#label-cval-block-'+rowid).show(); 
	 $('#additional-action-blok-'+rowid).hide(); 
	 jQuery.ajax({
		type: "POST",
		url:"<?php echo $editAction?>",
		dataType: "json",
		data: {newname: newval,access_id: access_level_id,id: rowid},
		success: function(res) {
		if(res["error"]==false)
		{
		$('#new-title-'+rowid).val(res["newname"]);	
		$('#label-block-'+rowid).html(res["newname"]);
		$('#new-cval-'+rowid).val(res["level_id"]);	
		$('#label-cval-block-'+rowid).html(res["level_id"]);
		}
		else if(res["error"]==true){
			alert( res["msg"]);
		}
		}
	  });
	 }
	 });
  }
   function toggleEdit(rowid)
  {
	   $('#edit-block-'+rowid).toggle(); 
	   $('#edit-cval-block-'+rowid).toggle(); 
	   $('#label-block-'+rowid).toggle(); 	
	   $('#label-cval-block-'+rowid).toggle(); 	
	  $('#additional-action-blok-'+rowid).toggle(); 	   
  }
  
 function cancelsaveAction(rowid)
  {
	$('#edit-block-'+rowid).hide();  
	$('#label-block-'+rowid).show();  
	$('#edit-cval-block-'+rowid).hide();  
	$('#label-cval-block-'+rowid).show(); 
	$('#additional-action-blok-'+rowid).hide(); 	 
  }
  
</script>