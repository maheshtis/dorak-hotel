/* 
 * Common js for setting module
 * To handle all type of java script event
 */

// common function for hide toggle div on close
$(document).on('click', '#import_curid', function() {
    var $this = $(this);
    $('#toggle').toggle();
});

var CONFIG = (function() {
     var private = {
         'HOTEL_CHAIN': 'Are you sure want to delete hotel chain',
         'HOTEL_FACILITY': 'Are you sure want to delete hotel facility',
     };

     return {
        get: function(name) { return private[name]; }
    };
})();
