<?php

/**
 * @author tis
 * @author Jeevan
 * fucntion description :-

  star_rating()        method used to perform functions like add star rating,update and delete .
  dorak_rating()       method used to perform fucntions such as add dorak rating,update and delete.
  status()             method used to perform functions such as add status,update and delete
  departments()        method used to perform add department,update and delete
  role()               method used to perform fucntions such as add role,update and delete.
  market()             method used to perform functions such as add market,update and delete.
  travel_type()        method used to perform  fucntions such as add travel,update and delete.
  market()             method used to perform functions such as add market,update and delete.
  travel_type()        method used to perform  fucntions such as add travel,update and delete.
  market()             method used to perform functions such as add market,update and delete.
  position()           method used to perform  functions such as add position,update and delete.
 * @description this controller is implemented for basic settings control in related fields such as edit,delete and update.
 * @method removeExtraspace($str): helper method removes extra space from passed string parameter ; 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Entity extends AdminController {

    function __construct() {
        parent::__construct();
        $this->_init();
    }

    private function _init() {
        $this->load->helper(array('form', 'html', 'form_field_values', 'acl'));
        $this->load->library('form_validation');
        $this->output->set_meta('description', 'Dorak Hotel Settings');
        $this->output->set_meta('title', 'Dorak Hotel Settings');
        $this->output->set_title('Dorak Hotel Settings');
        $this->output->set_template('settings');
        $this->load->model('settings/setting_model');
        $this->load->model('settings/country_model');
        $this->tmpFileDir = $this->config->item('upload_temp_dir');
    }

    public function index() {
        redirect('settings/entity/star_rating');
    }

    /**
     * @method star_rating() method used to perform functions like add star rating,update and delete .
     * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
      ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=hotels,foregin key value=$did,database table foregin key column name=star_rating,hotel id=hotel_id
     * @param  string $action hold form action path
     * @param  int $did star ratings id
     * @todo this method is used to perform  fucntions like add star rating,update and delete in entity_attributes table
     * @return json value is returned.
     */
    public function star_rating($action = 'list', $did = '', $name = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('hotels', $did, 'star_rating', 'hotel_id')) {//if not assigned then delete
                        if ($this->setting_model->delStarRating($did)) {
                            $sdata['message'] = 'Selected star rating deleted!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('star_rating'), $name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/star_rating');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getStarRating($new_title, $dbId)) {
                    if (!$this->setting_model->updateStarRating($dbId, $upData)) {
                        $return = array('error' => false, 'newname' => $new_title);
                        echo json_encode($return);
                    }
                } else {
                    $return = array('error' => 'Star rating: ' . $new_title . ' already exists', 'newname' => $new_title);
                    echo json_encode($return);
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('star_rating_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getStarRating($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_star_ratings'));
                        if ($this->setting_model->addStarRating($insData)) {
                            $sdata['message'] = 'Star rating ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Star rating ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('star_rating'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/star_rating');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/entity/star_rating/add'),
            'editAction' => site_url('settings/entity/star_rating/editajax'),
            'delAction' => site_url('settings/entity/star_rating/del'),
            'add_input_name' => array('id' => 'star_rating_name',
                'name' => 'star_rating_name',
                'data-validation' => 'required,number,length',
                'data-validation-allowing' => "range[1;7]",
                'data-validation-length' => "min1",
                'data-validation-error-msg' => "Star Rating 1 to 7",
                'maxlength' => '3',
                'placeholder' => 'Star Rating',
                'value' => $this->input->post('star_rating_name')));
        $data['listData'] = $this->setting_model->getStarRatingList();
        $this->load->view('star_rating_setting', $data);
    }

    /**
     * @method dorak_rating() method used to perform fucntions such as add dorak rating,update and delete.
     * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
      ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=hotels,foregin key value=$did,database table foregin key column name=dorak_rating,hotel id=hotel_id
     * @param  string $action hold form action path
     * @param  int $did dorak ratings id
     * @todo this method is used to perform fucntions such as add dorak rating,update and delete into entity_attributes table.
     * @return json value is returned.
     */
    public function dorak_rating($action = 'list', $did = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    if (!$this->setting_model->isAttributeTypeAssigned('hotels', $did, 'dorak_rating', 'hotel_id')) {//if not assigned then delete
                        if ($this->setting_model->delDorakRating($did)) {
                            $sdata['message'] = 'Selected dorak rating deleted!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('dorak_rating'), $did);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/dorak_rating');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getDorakRating($new_title, $dbId)) {
                    if (!$this->setting_model->updateDorakRating($dbId, $upData)) {
                        $return = array('error' => false, 'newname' => $new_title);
                        echo json_encode($return);
                    }
                } else {
                    $return = array('error' => 'Dorak rating: ' . $new_title . ' already exists', 'newname' => $new_title);
                    echo json_encode($return);
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('dorak_rating_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getDorakRating($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_dorak_ratings'));
                        if ($this->setting_model->addDorakRating($insData)) {
                            $sdata['message'] = 'Dorak rating ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Dorak rating ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error');
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('dorak_rating'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error');
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/dorak_rating');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/entity/dorak_rating/add'),
            'editAction' => site_url('settings/entity/dorak_rating/editajax'),
            'delAction' => site_url('settings/entity/dorak_rating/del'),
            'add_input_name' => array('id' => 'dorak_rating_name',
                'name' => 'dorak_rating_name',
                'data-validation' => 'required,number,length',
                'data-validation-allowing' => "range[1;7]",
                'data-validation-length' => "min1",
                'data-validation-error-msg' => "Dorak Rating 1 to 7",
                'maxlength' => '3',
                'placeholder' => 'Dorak Rating',
                'value' => $this->input->post('dorak_rating_name')));
        $data['listData'] = $this->setting_model->getDorakRatingList();
        $this->load->view('dorak_rating_setting', $data);
    }

    /**
     * @method status() method used to perform functions such as add status,update and delete 
     * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
      ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=hotels,foregin key value=$did,database table foregin key column name=status,hotel id=hotel_id
     * @param  string $action hold form action path
     * @param  int $did status id
     * @todo this method is used to perform funtions such as add status,update and delete into entity_attributes
     * @return json value is returned.
     */
    public function status($action = 'list', $did = '', $name = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('hotels', $did, 'status', 'hotel_id')) {//if not assigned then delete
                        if ($this->setting_model->delStatus($did)) {
                            $sdata['message'] = 'Selected status deleted!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess');
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('status'), $name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/status');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getStatus($new_title, $dbId)) {
                    if (!$this->setting_model->updateStatus($dbId, $upData)) {
                        $return = array('error' => false, 'newname' => $new_title);
                        echo json_encode($return);
                    }
                } else {
                    $return = array('error' => 'Status: ' . $new_title . ' already exists', 'newname' => $new_title);
                    echo json_encode($return);
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('status_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getStatus($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_hotel_status'));
                        if ($this->setting_model->addStatus($insData)) {
                            $sdata['message'] = 'Status ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess');
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Status ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error');
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('status'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error');
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/status');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/entity/status/add'),
            'editAction' => site_url('settings/entity/status/editajax'),
            'delAction' => site_url('settings/entity/status/del'),
            'add_input_name' => array('id' => 'status_name',
                'name' => 'status_name',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s ]*$",
                'data-validation-length' => "min3",
                'maxlength' => '50',
                'placeholder' => 'Status',
                'value' => $this->input->post('status_name')));
        $data['listData'] = $this->setting_model->getStatusList();
        $this->load->view('status_setting', $data);
    }

    /**
     * @method departments() method used to perform add department,update and delete 
     * @method isAttributeTypeAssigned() method used to check department assign in hotels or not('table name,rating id,selected field and hotel id')
     * @param  string $action hold form action path
     * @param  int $did department id
     * @todo this method is used to perform functions such as add department,update and delete in entity_attributes.
     * @return json value is returned.
     */
    public function departments($action = 'list', $did = '', $name = '') {

        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('dorak_users', $did, 'department', 'id')) {//if not assigned then delete
                        if ($this->setting_model->delDepartment($did)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('user_department'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess');
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('USER_ALREADY_ASSIGNED'), $this->config->item('user_department'), $name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/departments');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getDepartment($new_title, $dbId)) {
                    if (!$this->setting_model->updateDepartment($dbId, $upData)) {
                        $return = array('error' => false, 'newname' => $new_title);
                        echo json_encode($return);
                    }
                } else {
                    $return = array('error' => 'Department: ' . $new_title . ' already exists', 'newname' => $new_title);
                    echo json_encode($return);
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('department_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getDepartment($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_deppartment'));
                        if ($this->setting_model->addDepartment($insData)) {
                            $sdata['message'] = 'Department ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess');
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Department ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error');
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('user_department'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error');
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/departments');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/entity/departments/add'),
            'editAction' => site_url('settings/entity/departments/editajax'),
            'delAction' => site_url('settings/entity/departments/del'),
            'add_input_name' => array('id' => 'department_name',
                'name' => 'department_name',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s ]*$",
                'data-validation-length' => "min2",
                'maxlength' => '50',
                'placeholder' => 'Department',
                'value' => $this->input->post('department_name')));
        $data['listData'] = $this->setting_model->getDepartmentsList();
        $this->load->view('departments_setting', $data);
    }

    /**
     * @method role() method used to perform fucntions such as add role,update and delete.
     * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
      ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=dorak_users,foregin key value=$did,database table foregin key column name=company,hotel id=id
     * @param  string $action hold form action path
     * @param  int $did role id
     * @todo this method is used to perform functions such as add role,update and delete into role table.
     * @return json value is returned.
     */
    public function role($action = 'list', $did = '', $name = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('dorak_users', $did, 'company', 'id')) {//if not assigned then delete
                        if ($this->setting_model->delRole($did)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('role'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('USER_ALREADY_ASSIGNED'), $this->config->item('role'), $name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/role');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $access_level_id = removeExtraspace($this->input->post('access_id'));
                $dbId = $this->input->post('id');
                $upData = array('title' => $new_title, 'access_level_id' => $access_level_id);
                if ($new_title != "" && !$this->setting_model->getRole($new_title, $dbId)) {
                    if (!$this->setting_model->updateRole($dbId, $upData)) {
                        $return = array('error' => false, 'newname' => $new_title, 'level_id' => getAccessLevelTitle($access_level_id));
                        echo json_encode($return);
                    }
                } else {
                    $return = array('error' => 'Role: ' . $new_title . ' already exists', 'newname' => $new_title);
                    echo json_encode($return);
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('role_name'));
                $access_level_id = removeExtraspace($this->input->post('access_level_id'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getRole($newname)) {
                        $insData = array('title' => $newname, 'access_level_id' => $access_level_id);
                        if ($this->setting_model->addRole($insData)) {
                            $sdata['message'] = 'Role ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Role ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('role'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/role');
                break;
            default:
        }
        $accessLevels = getAccessLevelOptions(); /* using helper */
        $data = array(
            'addAction' => site_url('settings/entity/role/add'),
            'editAction' => site_url('settings/entity/role/editajax'),
            'delAction' => site_url('settings/entity/role/del'),
            'add_input_name' => array('id' => 'role_name',
                'name' => 'role_name',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s ]*$",
                'data-validation-length' => "min2",
                'maxlength' => '50',
                'placeholder' => 'Role',
                'value' => $this->input->post('role_name')),
            'access_levels' => $accessLevels
        );
        $data['listData'] = $this->setting_model->getRolesList();
        $this->load->view('role_setting', $data);
    }

    /**
     * @method market() method used to perform functions such as add market,update and delete.
     * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
      ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=hotel_rooms_pricing_details,foregin key value=$did,database table foregin key column name=market_id,hotel id=id
     * @param  string $action hold form action path
     * @param  int $did market id
     * @todo this method is used to perform functions such as add market,update and delete into entity_attributes table.
     * @return json value is returned.
     */
    public function market($action = 'list', $did = '', $name = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('hotel_rooms_pricing_details', $did, 'market_id', 'id')) {//if not assigned then delete
                        if ($this->setting_model->delMarket($did)) {
                            $sdata['message'] = 'Selected market deleted!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('market'), $name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/market');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getMarket($new_title, $dbId)) {
                    if (!$this->setting_model->updateMarket($dbId, $upData)) {
                        $return = array('error' => false, 'newname' => $new_title);
                        echo json_encode($return);
                    }
                } else {
                    $return = array('error' => 'Market : ' . $new_title . ' already exists', 'newname' => $new_title);
                    echo json_encode($return);
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('market_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getMarket($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_market'));
                        if ($this->setting_model->addMarket($insData)) {
                            $sdata['message'] = "Market " . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = "Market " . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('market'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/market');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/entity/market/add'),
            'editAction' => site_url('settings/entity/market/editajax'),
            'delAction' => site_url('settings/entity/market/del'),
            'add_input_name' => array('id' => 'market_name',
                'name' => 'market_name',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s .,;]*$",
                'data-validation-length' => "min2",
                'maxlength' => '200',
                'placeholder' => 'Market',
                'value' => $this->input->post('market_name'))
        );
        $data['listData'] = $this->setting_model->getMarketList();
        $this->load->view('market_setting', $data);
    }

    /**
     * @method travel_type() method used to perform  fucntions such as add travel,update and delete.
     * @param  string $action hold form action path
     * @param  int $did travel id
     * @todo this method is used to perform fucntions like add travel,update and delete into entity_attributes table in DB.
     * @return json value is returned.
     */
    public function travel_type($action = 'list', $did = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    if ($this->setting_model->delTravelType($did)) {
                        $sdata['message'] = 'Selected travel type deleted!';
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'sucess'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/travel_type');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getTravelType($new_title, $dbId)) {
                    if (!$this->setting_model->updateTravelType($dbId, $upData)) {
                        echo $new_title;
                    }
                } else {
                    echo 'exists';
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('travel_type_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getTravelType($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_travel_type'));
                        if ($this->setting_model->addTravelType($insData)) {
                            $sdata['message'] = 'Travel type ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = $newname . ' travel type not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('travel_type'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/travel_type');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/entity/travel_type/add'),
            'editAction' => site_url('settings/entity/travel_type/editajax'),
            'delAction' => site_url('settings/entity/travel_type/del'),
            'add_input_name' => array('id' => 'travel_type_name',
                'name' => 'travel_type_name',
                'class' => 'required',
                'data-validation' => 'required,alphanumeric,length',
                'data-validation-allowing' => '-_ ',
                'data-validation-length' => "min3",
                'maxlength' => '50',
                'placeholder' => 'Travel Type',
                'value' => $this->input->post('travel_type_name'))
        );
        $data['listData'] = $this->setting_model->getTravelTypeList();
        $this->load->view('travel_type_setting', $data);
    }

    public function room_type($action = 'list', $did = '', $name = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('hotel_rooms_pricing', $did, 'room_type', 'hotel_id')) {//if not assigned then delete
                        if ($this->setting_model->delRoomType($did)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('hotel_room_type'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('hotel_room_type'), $name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/room_type');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getRoomType($new_title, $dbId)) {
                    if (!$this->setting_model->updateRoomType($dbId, $upData)) {
                        $return = array('error' => false, 'newname' => $new_title);
                        echo json_encode($return);
                    }
                } else {
                    $return = array('error' => 'Room type : ' . $new_title . ' already exists', 'newname' => $new_title);
                    echo json_encode($return);
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('room_type_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getRoomType($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_room_types'));
                        if ($this->setting_model->addRoomType($insData)) {
                            $sdata['message'] = 'Room type ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Room type ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('hotel_room_type'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/room_type');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/entity/room_type/add'),
            'editAction' => site_url('settings/entity/room_type/editajax'),
            'delAction' => site_url('settings/entity/room_type/del'),
            'add_input_name' => array('id' => 'room_type_name',
                'name' => 'room_type_name',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s ]*$",
                'data-validation-length' => "min2",
                'maxlength' => '50',
                'placeholder' => 'Room Type',
                'value' => $this->input->post('room_type_name'))
        );
        $data['listData'] = $this->setting_model->getRoomTypesList();
        $this->load->view('room_type_setting', $data);
    }

    /**
     * @method position() method used to perform  functions such as add position,update and delete.
     * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
      ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=dorak_users,foregin key value=$did,database table foregin key column name=position,hotel id=id
     * @param  string $action hold form action path
     * @param  int $did position id
     * @todo this method is used to perform add position,update and delete into entity_attributes table.
     * @return json value is returned.
     */
    public function position($action = 'list', $did = '', $name = '') {

        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('hotel_contact_details', $did, 'position', 'hotel_id')) {//if not assigned then delete
                        if ($this->setting_model->delPosition($did)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('user_position'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('user_position'), $name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/position');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getPosition($new_title, $dbId)) {
                    if (!$this->setting_model->updatePosition($dbId, $upData)) {
                        $return = array('error' => false, 'newname' => $new_title);
                        echo json_encode($return);
                    }
                } else {
                    $return = array('error' => 'Position: ' . $new_title . ' already exists', 'newname' => $new_title);
                    echo json_encode($return);
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('position_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getPosition($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_hotel_positions'));
                        if ($this->setting_model->addPosition($insData)) {
                            $sdata['message'] = 'Position ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Position ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('user_position'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/entity/position');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/entity/position/add'),
            'editAction' => site_url('settings/entity/position/editajax'),
            'delAction' => site_url('settings/entity/position/del'),
            'add_input_name' => array('id' => 'position_name',
                'name' => 'position_name',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s ]*$",
                'data-validation-length' => "min2",
                'maxlength' => '50',
                'placeholder' => 'Position',
                'value' => $this->input->post('position_name')));
        $data['listData'] = $this->setting_model->getPositionsList();
        $this->load->view('position_setting', $data);
    }

}
