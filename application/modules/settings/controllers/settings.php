<?php

/**
 * @author tis
 * @author M
 * @description this module is implemented for performing actions such as add,edit and delete in hotel settings.
 * @method removeExtraspace($str): helper method remove extra space from passed string parameter ; 
 * function description :-
 
    chain()                      method used to perform fucntions such as add chain,update and delete in entity_attributes table.      
    isAttributeTypeAssigned()    method used to perform fucntions such as add chain,update and delete in entity_attributes table.
    purpose()                    method used to perform functions such as add purpose,update and delete in entity_attributes table.
    property_type()              used to perform functions suh as add property type,update and delete in entity_attributes table
    facility()                   method used to perform functions such as add facility,update and delete in entity_attributes table
    currency()                   method used to perform functions such as add currency,update and delete in entity_attributes table
    process_curency_rates()      method used to perform function import currency rate from excel file into database.
    meal_plan()                  method used to perform functions such as add meal plan,update and delete in entity_attributes table
    cancellation_duration()      method used to perform functions such as add cancellation duration,update and delete in entity_attributes table.
    payment_duration()           method used to perform functions such as add payment duration,update and delete in entity_attributes table
    age_group()                  method used to perform functions such as add age group,update and delete in entity_attributes table
    edit_company()               method used to edit company details.
 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends AdminController {

    function __construct() {
        parent::__construct();
        $this->_init();
    }

    private function _init() {
        $this->load->helper(array('form', 'html', 'form_field_values', 'acl'));
        $this->load->library('form_validation');
        $this->output->set_meta('description', 'Dorak Hotel Settings');
        $this->output->set_meta('title', 'Dorak Hotel Settings');
        $this->output->set_title('Dorak Hotel Settings');
        $this->output->set_template('settings');
        $this->load->model('settings/setting_model');
        $this->config->load('message_constant', TRUE);
        $this->tmpFileDir = $this->config->item('upload_temp_dir');
    }

    public function index() {
        redirect('settings/chain');
    }
  /**
     * @method chain() method used to perform add chain,update and delete 
	 * @param  string $action hold form action path
	 * @param  int $cid chain id
	 * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
	   ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=hotel,foregin key value=$cid,database table foregin key column name=chain_id,hotel id=hotel_id
	 * @todo method used to perform fucntions such as add chain,update and delete in entity_attributes table.
     */
    public function chain($action = 'list', $cid = '',$name = '') {
        switch ($action) {
            case 'del':
                if ($cid != "") {

                    $chid = (int)$cid;
                    $name = urldecode($name);
                    /**
                     * @method string isAttributeTypeAssigned
                     * @param tablename(string), $cid(int) selected coloum feild id(int) assigned table hotel_id.
                     * @todo  function is implemented to ensure 'hotel' assigned to the 'chain' is not deleted.
                     * @return will return output true if it exists else false will be returned.
                     */


                    if (!$this->setting_model->isAttributeTypeAssigned('hotels', $cid, 'chain_id', 'hotel_id')) {//if not assigned then delete
                        if ($this->setting_model->delHotelChain($chid)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('hotel_chain'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('hotel_chain'),$name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/chain');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $chain_id = trim($this->input->post('id'));
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getHotelChain($new_title, $chain_id)) {
                    if (!$this->setting_model->updateHotelChain($chain_id, $upData)) {
                        echo $new_title;
                    }
                } else {
                    echo 'exists';
                }
                exit();
                break;
            case 'add':
                $chname = removeExtraspace($this->input->post('chain_name'));
                if ($chname != "") {
                    //check if already in database
                    if (!$this->setting_model->getHotelChain($chname)) {
                        $insData = array('entity_title' => $chname, 'entity_type' => $this->config->item('attribute_hotel_chain'));
                        if ($this->setting_model->addHotelChain($insData)) {
                            $sdata['message'] = 'Chain ' . $chname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Chain ' . $chname . ' not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $chname, $this->config->item('hotel_chain'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/chain');
                break;
            default:
        }
        // generate  html  fields and action veriable for views
        $data = array(
            'addAction' => site_url('settings/chain/add'),
            'editAction' => site_url('settings/chain/editajax'),
            'delAction' => site_url('settings/chain/del'),
            'chain_name' => array('id' => 'chain_name',
                'name' => 'chain_name',
                'class' => 'required',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s]*$",
                'data-validation-length' => "3-100",
                'placeholder' => 'Chain name',
                'maxlength' => '100',
                'size' => '100',
                'value' => $this->input->post('chain_name'))
        );
        $data['hotel_chains'] = $this->setting_model->getHotelChainList();
        $this->load->view('chain_setting', $data);
    }

      /**
     * @method purpose() method used to perform add purpose,update and delete 
     * @method isAttributeTypeAssigned()  method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
	   ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=hotel,foregin key value=$did,database table foregin key column name=purpose,hotel id=hotel_id
	 * @param  string $action hold form action path
	 * @param  int $cid purpose id
	 * @todo method used to perform functions such as add purpose,update and delete in entity_attributes table.
     */
    public function purpose($action = 'list', $did = '',$name = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('hotels', $did, 'purpose', 'hotel_id')) {//if not assigned then delete
                        if ($this->setting_model->delHotelPurpose($did)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('purpose'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('purpose'),$name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/purpose');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getHotelPurpose($new_title, $dbId)) {
                    if (!$this->setting_model->updateHotelPurpose($dbId, $upData)) {
                        echo $new_title;
                    }
                } else {
                    echo 'exists';
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('purpose_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getHotelPurpose($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_hotel_purpose'));
                        if ($this->setting_model->addHotelPurpose($insData)) {
                            $sdata['message'] = 'Purpose ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Purpose ' . $newname . ' not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $chname, $this->config->item('purpose'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/purpose');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/purpose/add'),
            'editAction' => site_url('settings/purpose/editajax'),
            'delAction' => site_url('settings/purpose/del'),
            'purpose_name' => array('id' => 'purpose_name',
                'name' => 'purpose_name',
                'class' => 'required',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s]*$",
                'data-validation-length' => "3-100",
                'placeholder' => 'Purpose',
                'maxlength' => '100',
                'size' => '100',
                'value' => $this->input->post('purpose_name'))
        );
        $data['listData'] = $this->setting_model->getHotelPurposeList();
        $this->load->view('purpose_setting', $data);
    }

    /**
     * @method property_type() method used to perform add property type,update and delete 
     * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
	   ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=hotel_property_types,foregin key value=$did,database table foregin key column name=property_type_id,hotel id=id
	 * @param  string $action hold form action path
	 * @param  int $cid property type id
	 * @todo used to perform functions suh as add property type,update and delete in entity_attributes table
     */
    public function property_type($action = 'list', $did = '',$name= '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('hotel_property_types', $did, 'property_type_id', 'id')) {//if not assigned then delete
                        if ($this->setting_model->delHotelPropertyType($did)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('property_type'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('property_type'),$name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/property_type');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getHotelPropertyType($new_title, $dbId)) {
                    if (!$this->setting_model->updateHotelPropertyType($dbId, $upData)) {
                        echo $new_title;
                    }
                } else {
                    echo 'exists';
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('property_type_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getHotelPropertyType($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_property_types'));
                        if ($this->setting_model->addHotelPropertyType($insData)) {
                            $sdata['message'] = 'Property type ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Property type ' . $newname . ' not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('property_type'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/property_type');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/property_type/add'),
            'editAction' => site_url('settings/property_type/editajax'),
            'delAction' => site_url('settings/property_type/del'),
            'add_input_name' => array('id' => 'property_type_name',
                'name' => 'property_type_name',
                'class' => 'required',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z&\-\s]*$",
                'data-validation-length' => "3-100",
                'placeholder' => 'Property type',
                'maxlength' => '100',
                'size' => '100',
                'value' => $this->input->post('property_type_name'))
        );
        $data['listData'] = $this->setting_model->getHotelPropertyTypeList();
        $this->load->view('property_type_setting', $data);
    }

 /**
     * @method facility() method used to perform add facility,update and delete 
     * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
	   ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=hotel_complimentary_services,foregin key value=$did,database table foregin key column name=cmpl_service_id,hotel id=hotel_id
	 * @param  string $action hold form action path
	 * @param  int $cid facility id
	 * @todo  method used to perform functions such as add facility,update and delete in entity_attributes table
     */
    public function facility($action = 'list', $did = '',$name = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('hotel_complimentary_services', $did, 'cmpl_service_id', 'hotel_id')) {//if not assigned then delete
                        if ($this->setting_model->delRoomFacility($did)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('hotel_room_facility'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('hotel_room_facility'),$name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/facility');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getRoomFacility($new_title, $dbId)) {
                    if (!$this->setting_model->updateRoomFacility($dbId, $upData)) {
                        echo $new_title;
                    }
                } else {
                    echo 'exists';
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('rfacility_name'));
                if ($newname != "") {
//                            echo $newname;die;;
                    //check if already in database
                    if (!$this->setting_model->getRoomFacility($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_complementy_service'));
                        if ($this->setting_model->addRoomFacility($insData)) {
                            $sdata['message'] = 'Room Facility ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Room Facility ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('hotel_room_facility'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/facility');
                break;
            ## hotel label ##
            case 'delhotelfacility':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('hotel_facilities', $did, 'facility_id', 'hotel_id')) {//if not assigned then delete
                        if ($this->setting_model->delHotelFacility($did)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('hotel_facility'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('hotel_facility'),$name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/facility');
                break;
            case 'edithotelfacilityajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getHotelFacility($new_title, $dbId)) {
                    if (!$this->setting_model->updateHotelFacility($dbId, $upData)) {
                        echo $new_title;
                    }
                } else {
                    echo 'exists';
                }
                exit();
                break;
            case 'addhotelfacility':
                $newname = removeExtraspace($this->input->post('hfacility_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getHotelFacility($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_facilities'));
                        if ($this->setting_model->addHotelFacility($insData)) {
                            $sdata['message'] = 'Facility ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Facility ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('hotel_facility'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/facility');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/facility/add'),
            'editAction' => site_url('settings/facility/editajax'),
            'delAction' => site_url('settings/facility/del'),
            'add_input_name' => array('id' => 'rfacility_name',
                'name' => 'rfacility_name',
                'class' => 'required',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s]*$",
                'data-validation-length' => "min3",
                'placeholder' => 'Room Facility',
                'maxlength' => '100',
                'size' => '100',
                'value' => $this->input->post('rfacility_name')),
            'addHFAction' => site_url('settings/facility/addhotelfacility'),
            'editHFAction' => site_url('settings/facility/edithotelfacilityajax'),
            'delHFAction' => site_url('settings/facility/delhotelfacility'),
            'add_input_hfacility_name' => array('id' => 'hfacility_name',
                'name' => 'hfacility_name',
                'class' => 'required',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s]*$",
                'data-validation-length' => "min3",
                'placeholder' => 'Hotel Facility',
                'maxlength' => '100',
                'size' => '100',
                'value' => $this->input->post('hfacility_name'))
        );
        $data['listData'] = $this->setting_model->getRoomFacilityList();
        $data['listHFData'] = $this->setting_model->getHotelFacilityList();
        $this->load->view('facility_setting', $data);
    }

/**
     * @method currency() method used to perform add currency,update and delete 
     * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
	   ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=hotels,foregin key value=$did,database table foregin key column name=currency,hotel id=hotel_id
	 * @param  string $action hold form action path
	 * @param  int $cid currency id
	 * @todo  method used to perform functions such as add currency,update and delete in entity_attributes table
     */

    public function currency($action = 'list', $did = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
//		  $did=(int)$did;
                    if (!$this->setting_model->isAttributeTypeAssigned('hotels', $did, 'currency', 'hotel_id')) {//if not assigned then delete
                        if ($this->setting_model->delCurrency($did)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('hotel_currency'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('hotel_currency'),$did);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/currency');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $currency_usd_value = removeExtraspace($this->input->post('newusdval'));
                $dbId = $this->input->post('id');
                $upData = array('code' => $new_title, 'name' => $new_title, 'currency_to_tr' => $currency_usd_value,'tr_to_currency' => $currency_convert_value);
                if ($new_title != "" && $currency_usd_value != "" && $currency_convert_value != "") {

                    if (!$this->setting_model->getCurrency($new_title, $currency_usd_value, $dbId)) {
                        if (!$this->setting_model->updateCurrency($dbId, $upData)) {
                            $return = array('error' => false, 'curency' => $new_title, 'usdval' => $currency_usd_value);
                            echo json_encode($return);
                            // echo $new_title;
                        }
                    } else {
                        $return = array('error' => true, 'curency' => $new_title, 'usdval' => $currency_usd_value);
                        echo json_encode($return);
                    }
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('currency_name'));
                $usd_value = removeExtraspace($this->input->post('currency_usd_val'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getCurrency($newname)) {
                        $insData = array('code' => $newname, 'name' => $newname, 'currency_to_tr' => $usd_value,'tr_to_currency'=>$sell_value);

                        if ($this->setting_model->addCurrency($insData)) {
                            $sdata['message'] = 'Currency ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Currency ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('hotel_currency'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/currency');
                break;
            default:
        }
        $data = array(
            'importAction' => site_url('settings/process_curency_rates'),
            'addAction' => site_url('settings/currency/add'),
            'editAction' => site_url('settings/currency/editajax'),
            'delAction' => site_url('settings/currency/del'),
            'add_input_name' => array('id' => 'currency_name',
                'name' => 'currency_name',
                'class' => 'required',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s]*$",
                'data-validation-length' => "min2",
                'placeholder' => 'Currency',
                'maxlength' => '30',
                'size' => '50',
                'value' => $this->input->post('currency_name')),
            'add_input_value' => array('id' => 'currency_usd_val',
                'name' => 'currency_usd_val',
                'class' => 'required',
                'data-validation' => 'required,price',
                'data-validation-allowing' => 'float',
                'placeholder' => 'TR Value',
                'maxlength' => '10',
                'size' => '50',
                'value' => $this->input->post('sell_value')),
            'cell_input_value' => array('id' => 'sell_value',
                'name' => 'sell_value',
                'class' => 'required',
                'data-validation' => 'required,price',
                'data-validation-allowing' => 'float',
                'placeholder' => 'TR to Currency',
                'maxlength' => '10',
                'size' => '50',
                'value' => $this->input->post('sell_value')),
           'clear_old_data' => array(
                'name' => 'clear_old_data',
                'id' => 'clear_old_data',
                'value' => 'yclear',
                'data-label' => "Clear old data",
                'checked' => False,
            )
        );
        $data['listData'] = $this->setting_model->getCurrencyList();
        $this->load->view('currency_setting', $data);
    }

/**
     * @method process_curency_rates() method used to import currency rates
	 * @todo  method used to perform function import currency rate from excel file into database.
     */			

    public function process_curency_rates() {
//        echo "jeevan";die;
        if ($this->form_validation->run($this) != FALSE) {
            $sdata['message'] = 'Please select excel file to import';
            $flashdata = array(
                'flashdata' => $sdata['message'],
                'message_type' => 'notice'
            );
            $this->session->set_userdata($flashdata);
            $this->currency();
        } else {
            $arr_data = array();

            $clear_old_data = $this->input->post('clear_old_data');
            // config upload
            $config['upload_path'] = $this->tmpFileDir;
            $config['allowed_types'] = 'xlsx|csv|xls|xls';
            $config['max_size'] = '10000';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('curency_file')) {
                $sdata['message'] = $this->upload->display_errors();
                $flashdata = array(
                    'flashdata' => $sdata['message'],
                    'message_type' => 'notice'
                );
                $this->session->set_userdata($flashdata);
                $this->currency();
            } else {
                $upload_data = $this->upload->data();
                $curency_file = $upload_data['full_path'];
                chmod($curency_file, 0777);
                $this->load->library('excel');
                //read file from path
                $objPHPExcel = PHPExcel_IOFactory::load($curency_file);
                //get only the Cell Collection
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                //extract to a PHP readable array format
                $delArray = array(); // prepare curency code to delete
                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                    //header will/should be in row 1 only. of course this can be modified to suit your need.
                    if ($row == 1) {
                        $header[$row][$column] = $data_value;
                    } else {
                        $arr_data[$row][$header[1][$column]] = $data_value;
                        array_push($delArray, $arr_data[$row][$header[1]['A']]);
                    }
                }
                $dCurencyCode = array_unique($delArray);
                if (count($dCurencyCode) > 0) {
                    $this->setting_model->deleteCurencyCodesData($dCurencyCode);
                }
                if (in_array('code', $header[1]) && in_array('name', $header[1]) && in_array('currency_to_tr', $header[1]) && in_array('tr_to_currency', $header[1])) {
                    if ($clear_old_data && $clear_old_data == 'yclear')
                        $clearPrevData = true;
                    else
                        $clearPrevData = false;
                    $isimportedData = false;
                    if ($arr_data && count($arr_data) > 0) {
                        if ($clearPrevData) {
                            // empty the datble data and add new data
                            if ($this->setting_model->importCurencyData($arr_data)) {
                                $isimportedData = true;
                            }
                        } else {
                            // add new data to the  table with old data
                            if ($this->setting_model->addCurencyData($arr_data)) {
                                $isimportedData = true;
                            }
                        }
                        if ($isimportedData) {
                            $sdata['message'] = 'Currency details imported!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                            if (file_exists($curency_file)) {
                                unlink($curency_file);
                            }
                        } else {
                            $sdata['message'] = 'Currency details not imported!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                            if (file_exists($curency_file)) {
                                unlink($curency_file);
                            }
                        }
                    }
                } else {
                    $sdata['message'] = 'The file is not in proper format!Please check the sample file';
                    $flashdata = array(
                        'flashdata' => $sdata['message'],
                        'message_type' => 'error'
                    );
                    $this->session->set_userdata($flashdata);
                }
                if (file_exists($curency_file)) {
                    unlink($curency_file);
                }
            }
        }
        redirect('settings/currency');
    }

/**
     * @method meal_plan() method used to perform add meal plan,update and delete 
	 * @param  string $action hold form action path
	 * paaremter description : action values are list=display details,editajax=edit details,add=new meal plan,del=delete
	 * @param  int $did meal plan id
	 * @todo  method used to perform functions such as add meal plan,update and delete in entity_attributes table
     */

    public function meal_plan($action = 'list', $did = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    if ($this->setting_model->delMealPlan($did)) {
                        $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('meal_plan'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'sucess'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/meal_plan');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getMealPlan($new_title, $dbId)) {
                    if (!$this->setting_model->updateMealPlan($dbId, $upData)) {
                        $return = array('error' => false, 'newname' => $new_title);
                        echo json_encode($return);
                    }
                } else {
                    $return = array('error' => 'Meal plan: ' . $new_title . ' already exists', 'newname' => $new_title);
                    echo json_encode($return);
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('meal_plan_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getMealPlan($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_meal_plan'));
                        if ($this->setting_model->addMealPlan($insData)) {
                            $sdata['message'] = 'Meal plan ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Meal plan ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $newname);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/meal_plan');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/meal_plan/add'),
            'editAction' => site_url('settings/meal_plan/editajax'),
            'delAction' => site_url('settings/meal_plan/del'),
            'add_input_name' => array('id' => 'meal_plan_name',
                'name' => 'meal_plan_name',
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s ]*$",
                'data-validation-length' => "min2",
                'maxlength' => '10',
                'placeholder' => 'Meal Plan',
                'value' => $this->input->post('meal_plan_name')));
        $data['listData'] = $this->setting_model->getMealPlansList();
        $this->load->view('meal_plan_setting', $data);
    }

/**
     * @method cancellation_duration() method used to perform add ,update and delete cancellation duration for hotels
     * @method isAttributeTypeAssigned()  method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
	   ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=hotels,foregin key value=$did,database table foregin key column name=cancelled_before,hotel id=hotel_id
	 * @param  string $action hold form action path
	 * @param  int $did cancellation duration id
	 * @todo method used to perform functions such as add cancellation duration,update and delete in entity_attributes table.
     */	

    public function cancellation_duration($action = 'list', $did = '',$name = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('hotel_cancellation', $did, 'cancelled_before', 'hotel_id')) {//if not assigned then delete
                    if ($this->setting_model->delCancellationDuration($did)) {
                                $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('cancel_duration'));
                                $flashdata = array(
                                    'flashdata' => $sdata['message'],
                                    'message_type' => 'sucess');
                                $this->session->set_userdata($flashdata);
                        }} else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('cancel_duration'),$name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                  
                }
                redirect('settings/cancellation_duration');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getCancellationDuration($new_title, $dbId)) {
                    if (!$this->setting_model->updateCancellationDuration($dbId, $upData)) {
                        $return = array('error' => false, 'newname' => $new_title);
                        echo json_encode($return);
                    }
                } else {
                    $return = array('error' => 'Duration: ' . $new_title . ' already exists', 'newname' => $new_title);
                    echo json_encode($return);
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('duration_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getCancellationDuration($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_duration_cancellation'));
                        if ($this->setting_model->addCancellationDuration($insData)) {
                            $sdata['message'] = 'Duration ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess');
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Duration ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error');
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('cancel_duration'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error');
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/cancellation_duration');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/cancellation_duration/add'),
            'editAction' => site_url('settings/cancellation_duration/editajax'),
            'delAction' => site_url('settings/cancellation_duration/del'),
            'add_input_name' => array('id' => 'duration_name',
                'name' => 'duration_name',
                'data-validation' => 'required,alphanumeric,length',
                'data-validation-allowing' => "- ><.",
                'data-validation-length' => "min2",
                'maxlength' => '50',
                'placeholder' => 'Duration',
                'value' => $this->input->post('duration_name')));
        $data['listData'] = $this->setting_model->getCancellationDurationList();
        $this->load->view('cancellation_duration_setting', $data);
    }


/**
     * @method payment_duration() method used to perform add payment duration,update and delete 
     * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
	   ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=hotel_payment_shedules,foregin key value=$did,database table foregin key column name=payment_option_id,hotel id=hotel_id
	 * @param  string $action hold form action path
	 * @param  int $did payment duration id
	 * @todo  method used to perform functions such as add payment duration,update and delete in entity_attributes table
     */	

    public function payment_duration($action = 'list', $did = '',$name = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('hotel_payment_shedules', $did, 'payment_option_id', 'hotel_id')) {//if not assigned then delete
                        if ($this->setting_model->delPaymentDuration($did)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('hotel_payment_duration'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('hotel_payment_duration'),$name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/payment_duration');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getPaymentDuration($new_title, $dbId)) {
                    if (!$this->setting_model->updatePaymentDuration($dbId, $upData)) {
                        $return = array('error' => false, 'newname' => $new_title);
                        echo json_encode($return);
                    }
                } else {
                    $return = array('error' => 'Duration: ' . $new_title . ' already exists', 'newname' => $new_title);
                    echo json_encode($return);
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('duration_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getPaymentDuration($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_duration_payment'));
                        if ($this->setting_model->addPaymentDuration($insData)) {
                            $sdata['message'] = 'Duration ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess');
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Duration ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error');
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('hotel_payment_duration'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error');
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/payment_duration');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/payment_duration/add'),
            'editAction' => site_url('settings/payment_duration/editajax'),
            'delAction' => site_url('settings/payment_duration/del'),
            'add_input_name' => array('id' => 'duration_name',
                'name' => 'duration_name',
                'data-validation' => 'required,alphanumeric,length',
                'data-validation-allowing' => "- ><.",
                'data-validation-length' => "min2",
                'maxlength' => '50',
                'placeholder' => 'Duration',
                'value' => $this->input->post('duration_name'))
        );
        $data['listData'] = $this->setting_model->getPaymentDurationList();
        $this->load->view('payment_duration_setting', $data);
    }


/**
     * @method age_group() method used to perform add age group,update and delete 
     * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
	   ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=hotels,foregin key value=$did,database table foregin key column name=child_age_group_id,hotel id=hotel_id
	 * @param  string $action hold form action path
	 * @param  int $did age group id
	 * @todo  method used to perform functions such as add age group,update and delete in entity_attributes table
     */

    public function age_group($action = 'list', $did = '',$name = '') {
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    if (!$this->setting_model->isAttributeTypeAssigned('hotels', $did, 'child_age_group_id', 'hotel_id')) {//if not assigned then delete
                        if ($this->setting_model->delAgeGroup($did)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('child_age_group'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('SETTING_ASSIGNED_DELETE_FORMAT'), $this->config->item('child_age_group'),$name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/age_group');
                break;
            case 'editajax':
                $new_title = removeExtraspace($this->input->post('newname'));
                $dbId = $this->input->post('id');
                $upData = array('entity_title' => $new_title);
                if ($new_title != "" && !$this->setting_model->getAgeGroup($new_title, $dbId)) {
                    if (!$this->setting_model->updateAgeGroup($dbId, $upData)) {
                        $return = array('error' => false, 'newname' => $new_title);
                        echo json_encode($return);
                    }
                } else {
                    $return = array('error' => 'Age group: ' . $new_title . ' already exists', 'newname' => $new_title);
                    echo json_encode($return);
                }
                exit();
                break;
            case 'add':
                $newname = removeExtraspace($this->input->post('age_group_name'));
                if ($newname != "") {
                    //check if already in database
                    if (!$this->setting_model->getAgeGroup($newname)) {
                        $insData = array('entity_title' => $newname, 'entity_type' => $this->config->item('attribute_child_group'));
                        if ($this->setting_model->addAgeGroup($insData)) {
                            $sdata['message'] = 'Age group ' . $newname . ' added';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess');
                            $this->session->set_userdata($flashdata);
                        } else {
                            $sdata['message'] = 'Age group ' . $newname . '  not added!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error');
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('child_age_group'));
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error');
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/age_group');
                break;
            default:
        }
        $data = array(
            'addAction' => site_url('settings/age_group/add'),
            'editAction' => site_url('settings/age_group/editajax'),
            'delAction' => site_url('settings/age_group/del'),
            'add_input_name' => array('id' => 'age_group_name',
                'name' => 'age_group_name',
                'data-validation' => 'required,alphanumeric,length',
                'data-validation-allowing' => "- ><. +",
                'data-validation-length' => "min2",
                'maxlength' => '50',
                'placeholder' => 'Age group',
                'value' => $this->input->post('age_group_name')));
        $data['listData'] = $this->setting_model->getAgeGroupList();
        $this->load->view('age_group_setting', $data);
    }

/**
     * @method edit_company() method used to edit company details.
	 * @param  int $cid company id
	 * @todo method used to edit company.
     */	

    public function edit_company($cid = '') {
        $this->load->library('upload');
        $this->load->helper('path');
        $this->logoFileDir = $this->config->item('company_logo_dir');
        $getcmpMarkets = $this->setting_model->getCompanyMarkets($cid);
        $cmpMarkets = array();
        if ($getcmpMarkets) {
            foreach ($getcmpMarkets as $marketID) {
                $cmpMarkets[$marketID->market_id] = $marketID->market_id;
            }
        }
        if ($cid != "") {
            if (!empty($_POST)) {
                $sdata = array('message' => '');
                # validation rules 
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                $this->form_validation->set_rules('company_name', 'Company name', 'required|min_length[5]|max_length[100]');
                $this->form_validation->set_rules('company_address', 'Address', 'required|min_length[10]|max_length[150]');
                $this->form_validation->set_rules('city', 'City', 'required');
                $this->form_validation->set_rules('country_code', 'Country', 'required');
                if ($this->form_validation->run($this) != FALSE) {
                    $logo_name = '';
                    if (isset($_FILES['logo_file']) && is_uploaded_file($_FILES['logo_file']['tmp_name'])) { //get old logo and delete that one
                        if ($lgfile = $this->setting_model->getCompanyLogo($cid)) {
                            if ($lgfile != "") {
                                $logofile = set_realpath($this->logoFileDir) . $lgfile;
                                if (file_exists($logofile)) {
                                    unlink($logofile);
                                }
                            }
                        }
                        // upload new logo

                        $config['upload_path'] = $this->logoFileDir;
                        $config['allowed_types'] = 'gif|jpg|jpeg|png|svg';
                        $config['max_size'] = '0'; //2 mb , 0 for unlimited
                        $config['max_width'] = '0'; // unlimited
                        $config['max_height'] = '0';
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('logo_file')) {
                            $error = $this->upload->display_errors();
                            $sdata['message'] .= ', Notice.logo file not uploaded because ' . $error . ' , please upload it again!';
                        } else {
                            $uploadedFileDetail = $this->upload->data();
                            $logo_name = $uploadedFileDetail['file_name'];
                        }
                    }
                    if ($this->input->post('primary_company_id') != "" && $this->input->post('primary_company_id') > 0) {
                        $isPcompany = 0;
                    } else {
                        $isPcompany = 1;
                    }
                    $upData = array(
                        'company_name' => $this->input->post('company_name'),
                        'address' => $this->input->post('company_address'),
                        'city' => $this->input->post('city'),
                        'country_code' => $this->input->post('country_code'),
                        'head' => $this->input->post('company_head'),
                        //'market_id' => $this->input->post('market_id'),				
                        'is_primary_company' => $isPcompany,
                        'primary_company_id' => $this->input->post('primary_company_id'),
                        'status' => $this->input->post('company_status')
                    );
                    if ($logo_name != "")
                        $upData['company_logo'] = $logo_name;

                    if ($this->setting_model->updateCompany($cid, $upData)) {
                        /* update Company Markets start */
                        // check  if  markets are  changed then  update
                        $companyMarkets = $this->input->post('market_id');
                        $resultMatch = array_intersect($cmpMarkets, $companyMarkets); // return the matched values
                        if (count($cmpMarkets) != count($resultMatch) || count($cmpMarkets) < 1) {
                            // delete old  markets 
                            $this->setting_model->delCompanyMarkets($cid);
                            if ($companyMarkets != '' && count($companyMarkets) > 0) {
                                $addCompanyMarketsData = array();
                                foreach ($companyMarkets as $companyMarket) {
                                    if ($companyMarket != '') {
                                        $addCompanyMarketsData[] = array(
                                            'company_id' => $cid,
                                            'market_id' => $companyMarket
                                        );
                                    }
                                }
                                if (count($addCompanyMarketsData) > 0) {
                                    $this->setting_model->addCompanyMarkets($addCompanyMarketsData);
                                }
                            }
                        }
                        /* update Company Markets end */

                        $sdata['message'] .= 'Company: ' . $this->input->post('company_name') . ' details updated';
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'sucess'
                        );
                        $this->session->set_userdata($flashdata);
                    } else {
                        $sdata['message'] = 'Company:' . $this->input->post('company_name') . ' Detail not updated!';
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'error'
                        );
                        $this->session->set_userdata($flashdata);
                    }

                    redirect('settings/company');
                }
            }

            $countriesOptions = getCountriesOptions(); /* using helper */
            $primaryCompanies = getPrimaryCompanies();
            $primaryCompaniesOptions = array('0' => 'None');
            if ($primaryCompanies && count($primaryCompanies) > 0) {
                foreach ($primaryCompanies as $primaryCompany) {
                    $primaryCompaniesOptions[$primaryCompany->id] = $primaryCompany->company_name;
                }
            }

            $dc = $this->config->item('default_country_code');

            $pcdoce = $this->input->post('country_code');
            if ($pcdoce == "") {
                $pcdoce = $dc;
            }
            $citiesOptions = getCountyCitiesOptions($pcdoce); /* using helper */
            $marketsList = getMarkets();
            $marketOptions = array();
            if ($marketsList && count($marketsList) > 0) {
                foreach ($marketsList as $market) {
                    $marketOptions[$market->id] = $market->entity_title;
                }
            }
            $positions = getPositions();
            $positionOptions = array('' => 'Select');
            if ($positions && count($positions) > 0) {
                foreach ($positions as $position) {
                    $positionOptions[$position->entity_title] = $position->entity_title;
                }
            }
            $comp_det = $this->setting_model->getCompanyDetails($cid);
            $data = array(
                'logo_directory' => $this->logoFileDir,
                'editAction' => site_url('settings/edit_company/' . $cid),
                'edit_company_name' => array('id' => 'company_name',
                    'name' => 'company_name',
                    'autofocus' => 'autofocus',
                    'tabindex' => 1,
                    'data-validation' => 'required,custom,length',
                    'data-validation-regexp' => "^[a-zA-Z\-\s]*$",
                    'data-validation-length' => "min3",
                    'maxlength' => '100',
                    'placeholder' => 'Name',
                    'value' => $comp_det->company_name),
                'edit_company_address' => array('id' => 'company_address',
                    'tabindex' => 4,
                    'data-validation' => 'required,alphanumeric,length',
                    'data-validation-allowing' => "'\'-_@#:,./ ()\n\r",
                    'data-validation-length' => "15-150",
                    'name' => 'company_address',
                    'cols' => '43',
                    'tabindex' => 4,
                    'rows' => '2'),
                'country_id_options' => $countriesOptions,
                'city_options' => $citiesOptions,
                'markets_options' => $marketOptions,
                'edit_company_head' => array('id' => 'company_head',
                    'name' => 'company_head',
                    'tabindex' => 7,
                    'data-validation' => 'custom,length',
                    'data-validation-regexp' => "^[a-zA-Z\-\s]*$",
                    'data-validation-optional' => 'true',
                    'data-validation-length' => "min3",
                    'maxlength' => '50',
                    'placeholder' => 'Head',
                    'value' => $comp_det->head),
                'statusOptions' => array(
                    '1' => 'Enable',
                    '0' => 'Disable',
                ),
                'position_options' => $positionOptions,
                'primary_companies_options' => $primaryCompaniesOptions,
                'companyDetail' => $comp_det,
                'companyMarketIds' => $cmpMarkets,
            );

            $this->load->view('company_edit_setting', $data);
        } else {
            redirect('settings/company');
        }
    }

/**
     * @method company() method used to perform add company,update and delete 
     * @method isAttributeTypeAssigned() method used to check if chain is assigned in hotels or not, this function  need to pass  4 parameters 
	   ('database table name,foregin key value,database table foregin key column name , hotel id')
     * @parameter description : database table name=dorak_users,foregin key value=$did,database table foregin key column name=company,hotel id=id
	 * @param  string $action hold form action path
	 * @param  int $did company id
	 * @todo  method used to perform functions such as add company,update and delete
     */
    public function company($action = 'list', $did = '',$name = '') {
        $this->load->library('upload');
        $this->load->helper('path');
        $this->logoFileDir = $this->config->item('company_logo_dir');
        switch ($action) {
            case 'del':
                if ($did != "") {
                    $did = (int) $did;
                    $name = urldecode($name);
                    ##check if company have logi uploaded  then delete that file
                    if (!$this->setting_model->isAttributeTypeAssigned('dorak_users', $did, 'company', 'id')) {//if not assigned then delete
                        if ($lgfile = $this->setting_model->getCompanyLogo($did)) {
                            if ($lgfile != "") {
                                $logofile = set_realpath($this->logoFileDir) . $lgfile;
                                if (file_exists($logofile)) {
                                    unlink($logofile);
                                }
                            }
                        }
                        if ($this->setting_model->delCompany($did)) {
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'), $this->config->item('compony'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    } else {
                        $sdata['message'] = sprintf($this->config->item('USER_ALREADY_ASSIGNED'), $this->config->item('compony'),$name);
                        $flashdata = array(
                            'flashdata' => $sdata['message'],
                            'message_type' => 'notice'
                        );
                        $this->session->set_userdata($flashdata);
                    }
                }
                redirect('settings/company');
                break;

            case 'add':
                if (!empty($_POST)) {
                    $sdata = array('message' => '');
                    # validation rules 
                    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                    $this->form_validation->set_rules('company_name', 'Company name', 'required|min_length[5]|max_length[100]');
                    $this->form_validation->set_rules('company_address', 'Address', 'required|min_length[10]|max_length[150]');
                    $this->form_validation->set_rules('city', 'City', 'required');
                    $this->form_validation->set_rules('country_code', 'Country', 'required');
                    if ($this->form_validation->run($this) != FALSE) {
                        $newname = removeExtraspace($this->input->post('company_name'));
                        //check if already in database
                        if (!$this->setting_model->getCompany($newname)) {
                            $logo_name = '';
                            if (isset($_FILES['logo_file']) && is_uploaded_file($_FILES['logo_file']['tmp_name'])) {
                                $config['upload_path'] = $this->logoFileDir;
                                $config['allowed_types'] = 'gif|jpg|jpeg|png|svg';
                                $config['max_size'] = '0'; //2 mb , 0 for unlimited
                                $config['max_width'] = '0'; // unlimited
                                $config['max_height'] = '0';
                                $this->upload->initialize($config);
                                if (!$this->upload->do_upload('logo_file')) {
                                    $error = $this->upload->display_errors();
                                    $sdata['message'] .= ', Notice.logo file not uploaded because ' . $error . ' , please upload it again!';
                                } else {
                                    $uploadedFileDetail = $this->upload->data();
                                    $logo_name = $uploadedFileDetail['file_name'];
                                }
                            }
                            if ($this->input->post('primary_company_id') != "" && $this->input->post('primary_company_id') > 0) {
                                $isPcompany = 0;
                            } else {
                                $isPcompany = 1;
                            }
                            $insData = array(
                                'company_name' => removeExtraspace($this->input->post('company_name')),
                                'address' => $this->input->post('company_address'),
                                'city' => $this->input->post('city'),
                                'country_code' => $this->input->post('country_code'),
                                'head' => $this->input->post('company_head'),
                                'company_logo' => $logo_name,
                                'is_primary_company' => $isPcompany,
                                'primary_company_id' => $this->input->post('primary_company_id'),
                                'status' => $this->input->post('company_status')
                            );


                            if ($cmpID = $this->setting_model->addCompany($insData)) {
                                $sdata['message'] .= 'Company ' . $newname . ' added';
                                $flashdata = array(
                                    'flashdata' => $sdata['message'],
                                    'message_type' => 'sucess'
                                );
                                $this->session->set_userdata($flashdata);

                                /* Add Company Markets start */
                                $companyMarkets = $this->input->post('market_id');
                                if ($companyMarkets != '' && count($companyMarkets) > 0) {
                                    $addCompanyMarketsData = array();
                                    foreach ($companyMarkets as $companyMarket) {
                                        if ($companyMarket != '') {
                                            $addCompanyMarketsData[] = array(
                                                'company_id' => $cmpID,
                                                'market_id' => $companyMarket
                                            );
                                        }
                                    }
                                    if (count($addCompanyMarketsData) > 0) {
                                        $this->setting_model->addCompanyMarkets($addCompanyMarketsData);
                                    }
                                }

                                /* Add Company Markets end */
                            } else {
                                $sdata['message'] = 'Company ' . $newname . '  not added!';
                                $flashdata = array(
                                    'flashdata' => $sdata['message'],
                                    'message_type' => 'error'
                                );
                                $this->session->set_userdata($flashdata);
                            }
                        } else {
                            $sdata['message'] = sprintf($this->config->item('ALREADY_EXIST'), $newname, $this->config->item('compony'));
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                        redirect('settings/company');
                    }
                } else {
                    $sdata['message'] = 'Empty data';
                    $flashdata = array(
                        'flashdata' => $sdata['message'],
                        'message_type' => 'error'
                    );
                    $this->session->set_userdata($flashdata);
                }
                redirect('settings/company');

                break;
            default:
        }


        $countriesOptions = getCountriesOptions(); /* using helper */
        $primaryCompanies = getPrimaryCompanies();
        $primaryCompaniesOptions = array('0' => 'None');
        if ($primaryCompanies && count($primaryCompanies) > 0) {

            foreach ($primaryCompanies as $primaryCompany) {
                $primaryCompaniesOptions[$primaryCompany->id] = $primaryCompany->company_name;
            }
        }

        $dc = $this->config->item('default_country_code');

        $pcdoce = $this->input->post('country_code');
        if ($pcdoce == "") {
            $pcdoce = $dc;
        }
        $citiesOptions = getCountyCitiesOptions($pcdoce); /* using helper */
        $marketsList = getMarkets();
        $marketOptions = array();
        if ($marketsList && count($marketsList) > 0) {
            foreach ($marketsList as $market) {
                $marketOptions[$market->id] = $market->entity_title;
            }
        }

        $positions = getPositions();
        $positionOptions = array('' => 'Select');
        if ($positions && count($positions) > 0) {
            foreach ($positions as $position) {
                $positionOptions[$position->entity_title] = $position->entity_title;
            }
        }

        $data = array(
            'addAction' => site_url('settings/company/add'),
            'editAction' => site_url('settings/edit_company/'),
            'delAction' => site_url('settings/company/del'),
            'company_name' => array('id' => 'company_name',
                'name' => 'company_name',
                'tabindex' => 1,
                'data-validation' => 'required,custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s]*$",
                'data-validation-length' => "min3",
                'placeholder' => 'Name',
                'value' => $this->input->post('name')),
            'company_address' => array('id' => 'company_address',
                'tabindex' => 4,
                'data-validation' => 'required,alphanumeric,length',
                'data-validation-allowing' => "'\'-_@#:,./ ()\n\r",
                'data-validation-length' => "15-150",
                'name' => 'company_address',
                'cols' => '40',
                'tabindex' => 4,
                'rows' => '2'),
            'country_id_options' => $countriesOptions,
            'city_options' => $citiesOptions,
            'markets_options' => $marketOptions,
            'company_head' => array('id' => 'company_head',
                'name' => 'company_head',
                'tabindex' => 7,
                'data-validation' => 'custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s]*$",
                'data-validation-optional' => 'true',
                'data-validation-length' => "min3",
                'placeholder' => 'Head',
                'value' => $this->input->post('company_head')),
            'statusOptions' => array(
                '1' => 'Enable',
                '0' => 'Disable',
            ),
            'member_name' => array(
                'name' => 'member[1][name]',
                'id' => 'member_1_name',
                'tabindex' => 10,
                'data-validation' => 'custom,length',
                'data-validation-regexp' => "^[a-zA-Z\-\s]*$",
                'data-validation-length' => "min3",
                'data-validation-optional' => 'true',
                'placeholder' => 'Name',
                'value' => set_value('member[1][name]')),
            'member_email' => array(
                'name' => 'member[1][email]',
                'tabindex' => 11,
                'id' => 'member_1_email',
                'data-validation' => 'email',
                'data-validation-optional' => 'true',
                'placeholder' => 'Email',
                'value' => set_value('member[1][email]')),
            'position_options' => $positionOptions,
            'primary_companies_options' => $primaryCompaniesOptions
        );
        $defMarket = $this->config->item('default_market');
        $defCcode = array($dc);
        $data['posted_country_code'] = $this->input->post('country_code') != '' ? $this->input->post('country_code') : $defCcode;
        $data['posted_city'] = $this->input->post('city') != '' ? $this->input->post('city') : '';
        $data['posted_address'] = $this->input->post('company_address');
        $data['posted_market_id'] = $this->input->post('market_id') != '' ? $this->input->post('market_id]') : '';
        $data['posted_company_status'] = $this->input->post('company_status') != '' ? $this->input->post('company_status]') : '1';
        $data['posted_position'] = $this->input->post('member[1][position]') != '' ? $this->input->post('member[1][position]]') : '';
        $data['posted_primary_company_id'] = $this->input->post('primary_company_id') != '' ? $this->input->post('primary_company_id') : '0';
        $data['listData'] = $this->setting_model->getCompanyList();
        $this->load->view('company_setting', $data);
    }

## Company/sub company section end ####
}
