<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| ALL MESSAGE CONSTANT DEFINED HERE
|--------------------------------------------------------------------------
|
| This is file definde all types of message such as on deleted on update and any
| type of message which we want to modified.
|
*/
$config['SETTING_ASSIGNED_DELETE_FORMAT'] = 'Selected %s <span style = "color:red; font-weight: bold;">%s</span> is already associated with hotel/hotels. Please remove the association and try again.';
$config['USER_ALREADY_ASSIGNED'] = 'Selected %s <span style = "color:red; font-weight: bold;">%s</span> is already associated with user/users. Please remove the association and try again.';
$config['hotel_facility'] = 'Facility';
$config['hotel_room_facility'] = 'Room Facility';
$config['hotel_chain'] = 'Hotel Chain';
$config['purpose'] = 'Purpose';
$config['property_type'] = 'Property Type';
$config['hotel_currency'] = 'Currency';
$config['hotel_room_type'] = 'Room Type';
$config['user_department'] = 'Department';
$config['user_position'] = 'Position';
$config['child_age_group'] = 'Child Age Group';
$config['cancel_duration'] = 'Cancel Duration';
$config['hotel_payment_duration'] = 'Payment Duration';
$config['compony'] = 'Company';
$config['role'] = 'Role';
$config['meal_plan'] = 'Meal Plan';
$config['star_rating'] = 'Star Rating';
$config['dorak_rating'] = 'Dorak Rating';
$config['status'] = 'Status';
$config['market'] = 'Market';
$config['travel_type'] = 'Travel Type';
$config['country'] = 'Country';
$config['district'] = 'District';

$config['DELETE_FORMAT'] = '%s is successfully deleted!';
$config['ALREADY_EXIST'] = '%s %s already exists!';




