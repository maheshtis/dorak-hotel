<div class="main-part-container"> 
<div class="main-hotel">
<div class="hotel-serach-form">
<h3>Search</h3>
<?php echo form_open('hotels/index',array('class' => 'hotel_infosearch', 'id' => 'hotel_infosearch')); ?>
<div class="dd">
<div class="dt">
<label>Name</label>
 <?php echo form_input($hotel_name); ?>
</div>
<div class="dt">
<label>Code</label>
 <?php echo form_input($hotel_code); ?>
</div> 
<div class="dt">
<label>Chain</label>
 <?php  echo form_dropdown('hotel_chain_id',$chain_options,$serched_chain_id,'title="Select" class="selectpicker"')?>                                     
</div>
<div class="dt">
<label>Star Rating</label>
  <?php  echo form_dropdown('star_rating',$star_rating_options,$serched_star_rating,'title="Select" class="selectpicker"')?>                                     
</div>
<div class="dt">
<label>Purpose</label>
 <?php  echo form_dropdown('purpose',$purpose_options,$serched_purpose,'title="Select" class="selectpicker"')?>                                     
</div>
<div class="dt">
<label>Property Type</label>
<?php  echo form_dropdown('property_type',$property_type_options,$serched_property_type,'title="Select" id="property_type" class="selectpicker"')?>                                     
</div>
		<div class="dt">							
          <label>Country</label>
           <?php echo form_dropdown('country_code',$countriesOptions,$p_country_code,' tabindex="8"  data-live-search="true" id="country_code" class="selectpicker" onchange=getCountryCities(this)')?>   
            <?php echo form_error('country_code'); ?>
            </div>	
            <div class="dt">
            <label>City</label>
             <div id='city_container'>                                  
              <?php  echo form_dropdown('city',$citiesOptions,$posted_city,' tabindex="9"  id="city_name" data-live-search="true" class="selectpicker"  onchange=getCityDistricts(this)')?>   
              <?php echo form_error('city'); ?>
              </div>                                     
             </div>                                   
            <div class="dt disc">
             <label>District</label>
             <div id='district_container'>                                
              <?php  echo form_dropdown('district',$districtOptions,$posted_district,' tabindex="10"  id="district_name" data-live-search="true" class="selectpicker"')?> 
			  <?php echo form_error('district'); ?> 
              </div>                      
             </div>
			<div class="dt">
			<label>Status</label>
			<?php  echo form_dropdown('status_id',$status_options,$serched_status,'title="Select" class="selectpicker"')?>                                     
		</div>
		<div class="dt-go">
		<input class="go" type="submit" value="GO">
		</div>
	</div>
	<?php echo form_close(); ?>   
</div>
<div>
<div class="hotel-search-cont">
	<div class="add-delete">
	<?php if(checkAccess($this->accessLabelId,'hotels','add')){ ?>
	<div class="tgcnt">
	<div class="tg1">
	<!-- <a class="add-hotel" onclick ="customAlert(event)" href="<?php echo base_url('hotels/import/export_hotel'); ?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/add-rate-icon.png" alt="add">Export hotels</a>-->
	<a class="add-hotel" href="javascript:void(0);" id="import_curid">Import hotel</a>
	<div id="toggle" class="toggle-block">
	<div class = pull-right id = "import_curid">
    <img src="<?php echo base_url(); ?>assets/themes/default/images/close-estimate.png" alt="add">
    </div>
	<?php echo form_open_multipart($importAction,array('class' => 'frmhotels_file', 'id' => 'frmhotels_file'));?>
	<div class="input-group">
	<span class="input-group-btn">
	<span class="btn btn-primary btn-file">
	<img src="<?php echo base_url(); ?>assets/themes/default/images/upload-now.jpg" alt="upload"> Upload Excel File
	<?php echo form_upload('hotels_file','',' data-validation-max-size="1M" data-validation="required,extension"   data-validation-allowing="xls,xlsx"'); ?> 
	<?php echo form_error('hotels_file'); ?>
	<input type="text" class="form-control input-file-postion" readonly>
    </span>										
   </div>
	<input class="ok" type="submit" value="Go">
	<?php   echo form_close();?>
	<a class="himp-sample-download" href="<?php echo base_url('samples/import_hotels_sample.xlsx'); ?>" target="_blank">Download Sample File</a>
	</div>
	</div>
<div class="tg2">
<a class="add-hotel" href="javascript:void(0);" id="import_curid2">Import hotel pricing</a>
<div id="toggle-block" class="toggle-block">
	<div class = "pull-right" id = "import_curid2">
       <img src="<?php echo base_url(); ?>assets/themes/default/images/close-estimate.png" alt="add">
    </div>
<?php echo form_open_multipart('hotels/import/importPriceing',array('class' => 'frmhotels_file', 'id' => 'importPriceing'));?>
 <div class="input-group">
	<span class="input-group-btn">
	<span class="btn btn-primary btn-file">
	<img src="<?php echo base_url(); ?>assets/themes/default/images/upload-now.jpg" alt="upload"> Upload Excel File
	<?php echo form_upload('hotels_pricingfile','',' data-validation-max-size="1M" data-validation="required,extension,size"   data-validation-allowing="xls,xlsx"'); ?> 
	<?php echo form_error('hotels_pricingfile'); ?>
	<input type="text" class="form-control input-file-postion" readonly>
     </span>										
   </div>
	<input class="ok" type="submit" value="Go">
	<?php   echo form_close();?>
	<a class="himp-sample-download" href="<?php echo base_url('samples/hotels_pricing_sample.xlsx'); ?>" target="_blank">Download Sample File</a>
	</div>
	</div>
   </div>
	<a class="add-hotel" title="add hotel" href="<?php echo base_url('hotels/information'); ?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/add-rate-icon.png" alt="add">Add hotel</a>
	<?php }?>
	<?php if(checkAccess($this->accessLabelId,'hotels','delete')){ ?>
	<a href="javascript:void(0)" id="submit_listing_actions"><img src="<?php echo base_url(); ?>assets/themes/default/images/delete-icon-view.png" alt="delete" title="Delete selected hotel"></a>
	<?php }?>
	</div>
<?php echo form_open('hotels/massActions',array('class' => 'hotel_massactions', 'id' => 'hotel_massactions')); ?>
		<table id="hotel-listing-table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		<thead>
		<tr>
		<th width="4%">
		<?php if(checkAccess($this->accessLabelId,'hotels','delete')){ ?>
		<input name="select_all" value="1" id="hotel-listing-table-select-all" data-label="all" type="checkbox">
		<?php }?>
		</th>
		<th width="12%">Code</th>
		<th width="14%">Name</th>
		<th width="8%">Chain</th>
		<th width="11%">Star Rating</th>
		<th width="11%">Dorak Rating</th>
		<th width="6%">City</th>
		<th width="6%">Country</th>
		<th width="6%">Status</th>
		<th width="9%">Property Type</th>
		<th width="7%">Purpose</th>
		<th width="88">Action</th>               
		</tr>
	</thead>
	<tbody>
	   <!--  data generated data table server call!-->
	</tbody>
	</table>
	<?php echo form_close(); ?>  
	</div>
 </div>
 </div>
 </div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
<script>
function getCountryCities(ele){
getCityDistricts($("div#district_container select"));		
var baseUrl ="<?php echo base_url()?>";
var ccode = $(ele).val();
jQuery.ajax({
type: "POST",
url: baseUrl + "index.php/ajaxController/getCountryCities",
data: {country_code: ccode},
success: function(res) {
if (res)
{
jQuery("div#city_container").html(res);
$("div#city_container select").selectpicker();
}
}
});
}
 
function getCityDistricts(ele){	
	var baseUrl ="<?php echo base_url()?>";
	var cname = $(ele).val();
	jQuery.ajax({
	type: "POST",
	url: baseUrl + "index.php/ajaxController/getCityDistricts",
	data: {city_name: cname},
	success: function(res) {
	if (res)
	{
	jQuery("div#district_container").html(res);
	$("div#district_container select").selectpicker();
	}
	}
	});
}

  $(document).on('click','#import_curid',function(){
		var $this= $(this);
		$('#toggle').toggle();  
		$('#toggle-block').hide();	
		$.validate({
		form : '#frmhotels_file',
		modules : 'file'
		});	
	});
   $(document).on('click','#import_curid2',function(){
		var $this= $(this);
		$('#toggle-block').toggle();  
		$('#toggle').hide();	
	   $.validate({
		form : '#importPriceing',
		modules : 'file'
		});
  }); 
  // <![CDATA[
  $(document).ready(function (){
	 $.validate({
	form : '#frmhotels_file',
	modules : 'file'
	}); 
	 $.validate({
	form : '#importPriceing',
	modules : 'file'
	}); 
	
	<?php if($posted_city==""){?>
	var el=$("#country_code");
	if(el.length && el.val()!="")
	{
	getCountryCities(el);
	}
	var del=$("#city_name");
	if(del.length && del.val()!="")
	{
	getCityDistricts(del);
	}
	<?php }?>
   var table = $('#hotel-listing-table').DataTable({
      'ajax': {
         'url': '<?php echo base_url()?>hotels/hotelListingAjax' 
      },
	  "bAutoWidth": false,
	  "autoWidth": false,
      'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
             return '<input type="checkbox" name="id[]" data-label="id" value="' + $('<div/>').text(data).html() + '">';
		 }
      }],
	  "columns": [
					{ "width": "3%" },
					{ "width": "10%" },
					{ "width": "15%" },
					{ "width": "5%" },
					{ "width": "5%" },
					{ "width": "5%" },
					{ "width": "8%" },
					{ "width": "8%" },
					{ "width": "8%" },
					{ "width": "5%" },
					{ "width": "10%" },
					{ "width": "10%" },  
		],
	   "aoColumns": [
					  { "bSortable": false },
					  { "bSortable": true },
					  { "bSortable": true },
					  { "bSortable": true },
					  { "bSortable": true },
					  { "bSortable": true },
					  { "bSortable": true },
					  { "bSortable": true },
					  { "bSortable": true },
					  { "bSortable": false },
					  { "bSortable": true },
					  { "bSortable": false }
		],
      'order': [[1, 'asc']],
	  "searching": false,
		"lengthMenu": [[10, 25, 50, -1], [" 10 Per Page"," 25 Per Page", " 50 Per Page", "All"]],
		language : {
        sLengthMenu: "_MENU_"
		},
	     "fnDrawCallback": function (oSettings) {
          $('#hotel-listing-table input[type="checkbox"]').checkbox({
				checkedClass: 'icon-check',
				uncheckedClass: 'icon-check-empty'
			}); 
     }		
   });
   
    if($('#hotel-listing-table-select-all').is(':checked'))
	 {	
	  	$('.bootstrap-checkbox', rows).addClass("active");
	 }
	 else
	 {
	  var rows = table.rows({ 'search': 'applied' }).nodes();
		$('.bootstrap-checkbox', rows).removeClass("active");	 
	 }
   
   // Handle click on "Select all" control
   $('#hotel-listing-table-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
	 if($('#hotel-listing-table-select-all').is(':checked'))
	 {	
	  	$('.bootstrap-checkbox', rows).addClass("active");
	 }
	 else
	 {
	  var rows = table.rows({ 'search': 'applied' }).nodes();
		$('.bootstrap-checkbox', rows).removeClass("active");	 
	 }
	   
   });

   // Handle click on checkbox to set state of "Select all" control
   $('#hotel-listing-table tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#hotel-listing-table-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control 
            // as 'indeterminate'
            el.indeterminate = true;			  
         }		 
      }
   });

   // Handle form submission event
   $('#frm-hlisting').on('submit', function(e){
      var form = this;
      // Iterate over all checkboxes in the table
      table.$('input[type="checkbox"]').each(function(){
         // If checkbox doesn't exist in DOM
         if(!$.contains(document, this)){
            // If checkbox is checked
            if(this.checked){
               // Create a hidden element 
               $(form).append(
                  $('<input>')
                     .attr('type', 'hidden')
                     .attr('name', this.name)
                     .val(this.value)
               );			 
            }		
         } 
		 
      });
   });
   
  $('#submit_listing_actions').click(function () {
  
    var check = $('input[name="id[]"]').is(":checked");
    if (!check) {
         var r=confirm("Please select records  to delete!")
            if (r==true)
            {
               return true;
            }else{
                return false;
            }
      }
        
   var r=confirm("Are you sure to delete hotel/hotels?");
   if (r==true)
   {
      $('form#hotel_massactions').submit();
      return true;
   }
 
});

 $('.btn-file :file').on('fileselect', function(event, numFiles, label){
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });	 
   
});

function customAlert(event){
    event.preventDefault();
    var answer = confirm("Are you sure to delete hotel/hotels?");
        if (answer)
        {
           window.location = $(".add-hotel").attr('href');
        }
}

 $(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});
 // ]]></script>