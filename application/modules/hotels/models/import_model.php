<?php
/* 
 * @author :   Original Author TIS
 * @author :   Another Author <jeevan@jeevan@tisindiasupport.com>
 * @date   :   28-mar-2016
 * @functions : 
    addCountry() :                         Data record in the form of array inserted in DB                        
	getCountryCode()                        Country code is fetched from the DB. 
	getCountryId()                      this method used to return country id from countries table
	addCity()                        this method used to add country city in country_cities table
	getCityId()                        this method is used to fetch country city in country_cities table using city name and country code
	getDistrictId()                  this method is used to fetch district id using district name and city id from city_districts table
	addDistrict()                      this method is used to fetch district id using district name and city id from city_districts table
	getHotelDetailsBycode()            this method is used to fetch hotel id using hotel code from hotels table
	updateHotelDetails()              this method is used to update hotel details in hotels table
	getHotelDetails()                  this method is used to fetch hotel id using hotel code from hotels table
	insertDetails()                     this method used to add hotel record in hotels table
	addAgeGroup()                      this method used to add group records record in entity_attributes table
	getAgeGroup()                     this method used to fetch age group id record from entity_attributes table
	addPurpose()                      this method used insert purpose into entity_attributes table
	getPurpose()                       this method used fetch purpose from entity_attributes table
	getPropertyTypeId()                this method used fetch property type id from entity_attributes table
	addPropertyType()                    this method used insert property type into entity_attributes table
	getPaymentPlan()                   this method used fetch payment plan from entity_attributes table
	getRenovationName()                this method used fetch renovation id from renovation_types table
	addRenovationName()                 this method used insert renovation types into renovation_types table
	addPaymentPlan()                     this method used insert payment plan into entity_attributes table
	getCancelBefore()                    this method used fetch cancellation id into entity_attributes table
	addCancelBefore()                     this method used insert cancellation title into entity_attributes table
	deleteHotalPaymentShedules()          this method used delete hotel payment schedule from hotel_payment_shedules tabl
	importHotalPaymentShedules()          this method used import hotel payment schedule from hotel_payment_shedules table
	addHotelChain()                       this method used to insert hotel chain into entity_attributes table
	getHotelChain()                       this method method used to fetch hotel chain from entity_attributes table
	addPositions()                       this method method used to insert positions into entity_attributes table
	getPositions()                        this method method used to insert positions into entity_attributes table
	deleteHotalContactInfo()               this method method used to insert positions into entity_attributes table
	importHotalContactInfo()               this method method used to insert hotel contact info into hotel_contact_details table
	addComplimentaryService()              this method method used to insert hotel complimentary service into entity_attributes table
	getComplimentaryService()              this method method used to fetch hotel complimentary service from entity_attributes table
	deleteComplimentaryServices()           this method method used to remove complimentary service from hotel_complimentary_services table
	importComplimentaryServices()          this method method used to insert complimentary service into hotel_complimentary_services table
	importHotalContract()                  this method method used to insert hotel contract information into hotel_contract_info table
	deleteHotalComlimentaryRoom()          this method method used to delete hotel complimentary services from hotel_complimentary_room table
	importHotalComlimentaryRoom()          this method method used to insert hotel complimentary room into hotel_complimentary_room table
	importHotalComlimentaryRoomExcludedDate() this method method used to insert hotel complimentary room into hotel_cmplimntry_room_excluded_dates table
	addHotalPropertyTypes()                   this method method used to insert hotel property types into hotel_property_types table
	deleteHotalBankingInfo()                this method method used to delete hotel banking information from hotel_bank_accounts table
	importHotalBankingInfo()                this method method used to import hotel banking information into hotel_bank_accounts table
	deleteHotalCancellation()                 this method method used to delete hotel cancellation from hotel_cancellation table
	importHotalCancellation()               this method  used to import hotel cancellation into hotel_cancellation table
	deleteHotalRenovationSchedule()         this method  used to delete hotel renovation schedule from hotel_renovation_schedule table
	importHotalRenovationSchedule()         this method  used to insert hotel renovation schedule into hotel_renovation_schedule table
	deleteHotalDistanceFrom()               this method  used to delete hotel distance from hotel_distance table
	importHotalDistanceFrom()                this method  used to import hotel distance from hotel_distance table
	addFacility()                            this method  used to add hotel facility into entity_attributes table
	getFacility()                            this method  used to fetch hotel facility id from entity_attributes table
	deleteHotalFacilities()                    this method  used to delete hotel facility from hotel_facilities table
	importHotalFacilities()                    this method  used to delete hotel facility from hotel_facilities table
	getHotalRoomsPricingData()                 this method  used to fetch hotel pricing from hotel_rooms_pricing table
	editHotalRoomsPricingData()              this method  used to update hotel pricing from hotel_rooms_pricing table
	importHotalRoomsPricingData()            this method  used to import hotel room pricing data into hotel_rooms_pricing table
	deleteRoompricing()                      this method  used to delete room pricing data from hotel_rooms_pricing table
	addHotalRoomPricingComplimentary()        this method  used to insert hotel room pricing complimentary into hotel_rooms_pricing_complimentary table
	deleteRoomPricingComplimentary()          this method  used to delete hotel room pricing complimentary from hotel_rooms_pricing_complimentary table
	importHotelRroomsPricingDetails()          this method  used to import hotel room pricing details into hotel_rooms_pricing_details table
	updateHotelRroomsPricingDetails()          this method  used to update hotel room pricing details into hotel_rooms_pricing_details table
	addRoomType()                              this method  used to insert hotel room type into entity_attributes table
	getRoomTypeId()                            this method  used to insert hotel room type into entity_attributes table
	addMarket()                                this method  used to insert hotel market type into entity_attributes table
	getMarket()                                this method  used to insert hotel market into entity_attributes table
	get_room_types()                           this method  used to fetch hotel room type details from hotel_rooms_pricing table
	
 * @description : Imports all data into the database related with hotels attributes.
 */
class Import_model extends AdminModel {

    function __construct() {
        // Call the Parent Model constructor
        parent::__construct();
    }
    
 /*
  * @method string addCountry(),it updates country table with provided value.
  * @param array $data is inserted in the DB.
  * @todo Data record in the form of array inserted in DB
  * @return Returns last inserted id (numerical value) of added country.
  */
    function addCountry($data) {
        if ($this->db->insert('countries', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
    
 /*
  * @method string Fetches countrycode in string value.
  * @param Country code is fetched through int $c_code.
  * @todo  Country code is fetched from the DB. 
  * @return Provides Country code list from  the database.
  */

    function getCountryCode($c_code) {
        $this->db->select('country_code');
        $this->db->where("country_code", $c_code);
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->country_code;
        } else {
            return false;
        }
    }
/**
     * @method getCountryId() method used for get country id using country code
	 * @param int $c_code holds country code in the form of an array through which country id is fetched.
	 * @todo this method used to return country id from countries table
     * @return will return country id in the fom of an array OR return false
     */
    function getCountryId($c_code) {
        $this->db->select('id');
        $this->db->where("country_code", $c_code);
        $query = $this->db->get('countries');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addCity() method used to add city in country_cities table 
	 * @param int $data hold country city details in the form of an array.
	 * @todo this method used to add country city in country_cities table
     * @return id|false
     */
    function addCity($data) {
        if ($this->db->insert('country_cities', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getCityId() method used to get city in country_cities table using city name or country code
	 * @param string $cityName holds country city name in DB table.
	 * @param string $country_code holds country code for fetching city details.
	 * @todo this method is used to fetch country city in country_cities table using city name and country code
     * @return id|false
     */
    function getCityId($cityName, $country_code = '') {
        $this->db->select('id');
        $this->db->where("city_name", $cityName);
        if ($country_code != "")
            $this->db->where("country_code", $country_code);
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method getDistrictId() method used to fetch id from city_districts table
	 * @param string $district_name holds country city name details.
	 * @param int $cityId holds city id in DB table.
	 * @todo this method is used to fetch district id using district name and city id from city_districts table
     * @return will return id in the form of an array|false
     */
    function getDistrictId($district_name, $cityId) {
        $this->db->select('id');
        $this->db->where("city_id", $cityId);
        $this->db->where("district_name", $district_name);
        $query = $this->db->get('city_districts');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addDistrict() method used to add district from city_districts table
	 * @param array $data hold district name in the form of an array
	 * @todo this method is used to fetch district id using district name and city id from city_districts table
     * @return will return id |false
     */
    function addDistrict($data) {
        if ($this->db->insert('city_districts', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getHotelDetailsBycode() method used to fetch hotel id 
	 * @param string $hotelcode holds hotel code in DB.
	 * @todo this method is used to fetch hotel id using hotel code from hotels table
     * @return will return hotel_id |false
     */
    function getHotelDetailsBycode($hotelcode) {
        $this->db->select('hotel_id');
        $this->db->where('hotel_code', $hotelcode);
        $query = $this->db->get('hotels');
        if ($query->num_rows() > 0) {
            return $query->row()->hotel_id;
        } else {
            return false;
        }
    }

		/**
     * @method updateHotelDetails() method used to update hotel record using hotel id
	 * @param int $hid hold hotel id - hotel details updated using hotel id.
	 * @param array() $data hold  hotel record in the form of an array
	 * @todo this method is used to update hotel details in hotels table
     */
    function updateHotelDetails($hid, $data) {
        $this->db->where('hotel_id', $hid);
        $this->db->update('hotels', $data);
    }
/**
     * @method getHotelDetails() method used to fetch hotel id using hotel name,hotel city and post code.
	 * @param string $hname hold hotel name - contains hotel details in DB table.
	 * @param string $hcity hold hotel city - city name in DB.
	 * @param string $postcode hold hotel post code
	 * @todo this method used to fetch hotel id using hotel name,hotel city and post code from hotels table
	 * @return hotel id in the form of an array OR return false
     */
    function getHotelDetails($hname, $hcity, $postcode) {
        $this->db->select('hotel_id');
        $this->db->where('hotel_name', $hname);
        $this->db->where('city', $hcity);
        $this->db->where('post_code', $postcode);
        $query = $this->db->get('hotels');
        if ($query->num_rows() > 0) {
            return $query->row()->hotel_id;
        } else {
            return false;
        }
    }

/**
     * @method insertDetails() method used to add hotel details.
	 * @param array() $data holds hotel records in the form of an array
	 * @todo this method used to add hotel record in hotels table
	 * @return hotel id |return false
     */

    function insertDetails($data) {
        if ($this->db->insert('hotels', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method addAgeGroup() method used to add age group 
	 * @param array() $data holds age group records in the form of an array
	 * @todo this method used to add group records record in entity_attributes table
	 * @return hotel id | false
     */
    function addAgeGroup($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getAgeGroup() method used to fetch age group id
	 * @param string $title holds age group title in db table.
	 * @todo this method used to fetch age group id record from entity_attributes table
	 * @return hotel id | false
     */
    function getAgeGroup($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_child_group'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addPurpose() method used to insert purpose
	 * @param array() $data holds purpose in the form of an array in DB.
	 * @todo this method used insert purpose into entity_attributes table
	 * @return hotel id | false
     */
    function addPurpose($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getPurpose() method used to fetch purpose
	 * @param string $title holds purpose name in DB table.
	 * @todo this method used fetch purpose from entity_attributes table
	 * @return id | false
     */
    function getPurpose($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_hotel_purpose'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method getPropertyTypeId() method used to fetch property type id
	 * @param string $title holds property type name in DB table.
	 * @todo this method used fetch property type id from entity_attributes table
	 * @return id | false
     */
    function getPropertyTypeId($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_property_types'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addPropertyType() method used to insert property type detail
	 * @param string $title hold property type name in DB table.
	 * @todo this method used insert property type into entity_attributes table
	 * @return id | false
     */
    function addPropertyType($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getPaymentPlan() method used to fetch payment plan id
	 * @param string $title hold payment plan name in DB table.
	 * @todo this method used fetch payment plan from entity_attributes table
	 * @return id | false
     */
    function getPaymentPlan($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_duration_payment'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method getRenovationName() method used to fetch renovation id
	 * @param string $title hold renovation_type data in DB.
	 * @todo this method used fetch renovation id from renovation_types table
	 * @return id | false
     */
    function getRenovationName($title) {
        $this->db->select('id');
        $this->db->where("renovation_type", $title);
        $query = $this->db->get('renovation_types');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addRenovationName() method used to insert renovation types
	 * @param array() $data holds renovation_types in the form of an array in DB.
	 * @todo this method used insert renovation types into renovation_types table
	 * @return id | false
     */
    function addRenovationName($data) {
        if ($this->db->insert('renovation_types', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method addPaymentPlan() method used to insert payment plan
	 * @param array() $data holds payment plan in the form of an array.
	 * @todo this method used insert payment plan into entity_attributes table
	 * @return id | false
     */
    function addPaymentPlan($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getCancelBefore() method used to fetch cancellation before id
	 * @param string $data holds cancellation title data in DB.
	 * @todo this method used fetch cancellation id into entity_attributes table
	 * @return id | false
     */
    function getCancelBefore($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_duration_cancellation'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addCancelBefore() method used to insert cancellation before
	 * @param array() $data holds cancellation title in the form of an array
	 * @todo this method used insert cancellation title into entity_attributes table
	 * @return id | false
     */
    function addCancelBefore($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method deleteHotalPaymentShedules() method used to delete hotel payment schedule
	 * @param int $hid hold hotel id - holds payment schedule value.
	 * @todo this method used delete hotel payment schedule from hotel_payment_shedules table
	 * @return id | false
     */
    function deleteHotalPaymentShedules($hid) {
        if ($hid != "") {
            $this->db->where('hotel_id', $hid);
            $this->db->delete('hotel_payment_shedules');
        }
        return;
    }
/**
     * @method importHotalPaymentShedules() method used to import hotel payment schedule
	 * @param array() $data holds hotel payment schedule record in DB.
	 * @todo this method used import hotel payment schedule from hotel_payment_shedules table
	 * @return true | false
     */
    function importHotalPaymentShedules($data) {
        if ($this->db->insert_batch('hotel_payment_shedules', $data)) {
            return true;
        }
        return false;
    }
/**
     * @method addHotelChain() method used to insert hotel chain
	 * @param array() $data holds hotel chain records in the form of an array in DB.
	 * @todo this method used to insert hotel chain into entity_attributes table
	 * @return true | false
     */
    function addHotelChain($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getHotelChain() method used to fetch hotel chain
	 * @param string $title holds hotel chain title - name of hotel chain title.
	 * @todo this method method used to fetch hotel chain from entity_attributes table
	 * @return id | false
     */
    function getHotelChain($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_hotel_chain'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }
/**
     * @method addPositions() method used to insert positions
	 * @param array() $data holds positions in the form of an array
	 * @todo this method method used to insert positions into entity_attributes table
	 * @return id | false
     */
    function addPositions($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getPositions() method used to fetch positions id
	 * @param string $title hold position  - name of positions.
	 * @todo this method method used to insert positions into entity_attributes table
	 * @return will return id in the form of an array | false
     */
    function getPositions($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_hotel_positions'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return false;
        }
    }

/**
     * @method deleteHotalContactInfo() method used to delete hotel contact info
	 * @param int $hid hold hotel id - holds daa in array form in DB.
	 * @todo this method method used to insert positions into entity_attributes table
	 * @return will return id in the form of an array | false
     */

    function deleteHotalContactInfo($hid) {

            $this->db->where('hotel_id', $hid);
            $this->db->delete('hotel_contact_details');
        
    }
/**
     * @method importHotalContactInfo() method used to import hotel contact info
	 * @param int $hid hold hotel id
	 * @todo this method method used to insert hotel contact info into hotel_contact_details table
	 * @return true | false
     */
    function importHotalContactInfo($data) {
        if ($this->db->insert_batch('hotel_contact_details', $data)) {
            return true;
        }
        return false;
    }


/**
     * @method addComplimentaryService() method used to insert complimentary service
	 * @param array() $data holds hotel complimentary services in the form of an array
	 * @todo this method method used to insert hotel complimentary service into entity_attributes table
	 * @return id | false
     */
    function addComplimentaryService($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getComplimentaryService() method used to fetch complimentary service id
	 * @param string $title holds hotel complimentary service name records.
	 * @todo this method method used to fetch hotel complimentary service from entity_attributes table
	 * @return id | false
     */
    function getComplimentaryService($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_complementy_service'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return false;
        }
    }
/**
     * @method deleteComplimentaryServices() method used to remove complimentary service
	 * @param int $hid hold hotel id - comp services details in the table DB.
	 * @todo this method method used to remove complimentary service from hotel_complimentary_services table
	 * @return true | false
     */
    function deleteComplimentaryServices($hid) {
        if ($hid != "") {
            $this->db->where('hotel_id', $hid);
            $this->db->delete('hotel_complimentary_services');
        }
        return;
    }
/**
     * @method importComplimentaryServices() method used to insert complimentary service data 
	 * @param array() $data hold complimentary service in the form of an array
	 * @todo this method method used to insert complimentary service into hotel_complimentary_services table
	 * @return true | false
     */
    function importComplimentaryServices($data) {
        if ($this->db->insert_batch('hotel_complimentary_services', $data)) {
            return true;
        }
        return false;
    }

  /**
     * @method importHotalContract() method used to insert hotel contract information
	 * @param array() $data holds hotel contract information in the form of an array.
	 * @todo this method method used to insert hotel contract information into hotel_contract_info table
	 * @return true | false
     */

    function importHotalContract($data) {
        if ($this->db->insert_batch('hotel_contract_info', $data)) {
            return true;
        }
        return false;
    }

 /**
     * @method deleteHotalComlimentaryRoom() method used to delete hotel complimentary services
	 * @param int $hid hold hotel id - details in the form of array.
	 * @todo this method method used to delete hotel complimentary services from hotel_complimentary_room table
	 * @return true | false
     */

    function deleteHotalComlimentaryRoom($hid) {
        if ($hid != "") {
            $sql = 'DELETE hotel_complimentary_room,hotel_cmplimntry_room_excluded_dates
					FROM hotel_complimentary_room
					INNER JOIN hotel_cmplimntry_room_excluded_dates ON hotel_complimentary_room.cmpl_room_id = hotel_cmplimntry_room_excluded_dates.cmpl_room_id
					WHERE  hotel_complimentary_room.cmpl_room_id=hotel_cmplimntry_room_excluded_dates.cmpl_room_id  AND  hotel_complimentary_room.hotel_id =' . $hid;
            $this->db->query($sql);
        }
        return;
    }
 /**
     * @method importHotalComlimentaryRoom() method used to insert hotel complimentary room
	 * @param array() $data hold hotel complimentary room in the form of an array
	 * @todo this method method used to insert hotel complimentary room into hotel_complimentary_room table
	 * @return true | false
     */
    function importHotalComlimentaryRoom($data) {
        if ($this->db->insert('hotel_complimentary_room', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
 /**
     * @method importHotalComlimentaryRoomExcludedDate() method used to insert hotel complimentary room exclude date
	 * @param array() $data hold hotel complimentary room exclude date the form of an array
	 * @todo this method method used to insert hotel complimentary room into hotel_cmplimntry_room_excluded_dates table
	 * @return true | false
     */
    function importHotalComlimentaryRoomExcludedDate($data) {
        if ($this->db->insert_batch('hotel_cmplimntry_room_excluded_dates', $data)) {
            return true;
        }
        return false;
    }


/**
     * @method addHotalPropertyTypes() method used to insert hotel property types
	 * @param array() $data hold hotel property types in the form of an array
	 * @todo this method method used to insert hotel property types into hotel_property_types table
	 * @return true | false
     */

    function addHotalPropertyTypes($data) {
        if ($this->db->insert_batch('hotel_property_types', $data)) {
            return true;
        }
        return false;
    }

  /**
     * @method deleteHotalBankingInfo() method used to delete hotel banking information
	 * @param int $hid hold hotel id - banking info details fetch through hotel id.
	 * @todo this method method used to delete hotel banking information from hotel_bank_accounts table
	 * @return true | false
     */
    function deleteHotalBankingInfo($hid) {
        if ($hid != "") {
            $this->db->where('hotel_id', $hid);
            $this->db->delete('hotel_bank_accounts');
        }
        return;
    }
/**
     * @method importHotalBankingInfo() method used to import hotel banking information
	 * @param array() $data hold hotel bank accounts in the form of an array
	 * @todo this method method used to import hotel banking information into hotel_bank_accounts table
	 * @return true | false
     */
    function importHotalBankingInfo($data) {
        if ($this->db->insert_batch('hotel_bank_accounts', $data)) {
            return true;
        }
        return false;
    }

     /**
     * @method deleteHotalCancellation() method used to delete hotel cancellation
	 * @param int $hid holds hotel id details for hotel cancellation.
	 * @param string $seasion hold hotel seasion('low' or 'high')
	 * @todo this method method used to delete hotel cancellation from hotel_cancellation table
	 * @return true | false
     */

    function deleteHotalCancellation($hid, $seasion) {
        if ($hid != "") {
            $this->db->where('hotel_id', $hid);
            $this->db->where('seasion', $seasion);
            $this->db->delete('hotel_cancellation');
        }
        return;
    }
   /**
     * @method importHotalCancellation() method used to import hotel cancellation
	 * @param array() $data hold hotel cancellation in the form of an array
	 * @todo this method  used to import hotel cancellation into hotel_cancellation table
	 * @return true | false
     */
    function importHotalCancellation($data) {
        if ($this->db->insert_batch('hotel_cancellation', $data)) {
            return true;
        }
        return false;
    }


   /**
     * @method deleteHotalRenovationSchedule() method used to delete hotel renovation schedule
	 * @param int $hid holds hotel id - which contains schedule info.
	 * @todo this method  used to delete hotel renovation schedule from hotel_renovation_schedule table
	 * @return true | false
     */

    function deleteHotalRenovationSchedule($hid) {
        if ($hid != "") {
            $this->db->where('hotel_id', $hid);
            $this->db->delete('hotel_renovation_schedule');
        }
        return;
    }
   /**
     * @method importHotalRenovationSchedule() method used to insert hotel renovation schedule
	 * @param array() $data hold hotel renovation schedule in the form of an array
	 * @todo this method  used to insert hotel renovation schedule into hotel_renovation_schedule table
	 * @return true | false
     */
    function importHotalRenovationSchedule($data) {
        if ($this->db->insert_batch('hotel_renovation_schedule', $data)) {
            return true;
        }
        return false;
    }

 /**
     * @method deleteHotalDistanceFrom() method used to delete hotel distance
	 * @param int $hid hold hotel id - contains distance details which are to be deleted.
	 * @todo this method  used to delete hotel distance from hotel_distance table
	 * @return true | false
     */

    function deleteHotalDistanceFrom($hid) {
        if ($hid != "") {
            $this->db->where('hotel_id', $hid);
            $this->db->delete('hotel_distance');
        }
        return;
    }
/**
     * @method importHotalDistanceFrom() method used to import hotel distance from
	 * @param array() $data hold hotel distance from in the form of an array
	 * @todo this method  used to import hotel distance from hotel_distance table
	 * @return true | false
     */
    function importHotalDistanceFrom($data) {
        if ($this->db->insert_batch('hotel_distance', $data)) {
            return true;
        }
        return false;
    }

/**
     * @method addFacility() method used to add hotel facility
	 * @param array() $data hold hotel facility from in the form of an array
	 * @todo this method  used to add hotel facility into entity_attributes table
	 * @return id | false
     */
    function addFacility($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getFacility() method used to fetch hotel facility id
	 * @param string() $title holds hotel facility title or name.
	 * @todo this method  used to fetch hotel facility id from entity_attributes table
	 * @return id | false
     */
    function getFacility($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_facilities'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return false;
        }
    }
/**
     * @method deleteHotalFacilities() method used to delete hotel facility
	 * @param int $hid hold hotel id -  hotel facility detais using hotel id.
	 * @todo this method  used to delete hotel facility from hotel_facilities table
	 * @return true | false
     */
    function deleteHotalFacilities($hid) {
        if ($hid != "") {
            $this->db->where('hotel_id', $hid);
            $this->db->delete('hotel_facilities');
        }
        return;
    }
/**
     * @method importHotalFacilities() method used to import hotel facility
	 * @param array $data hold hotel facility in the form of an array
	 * @todo this method  used to delete hotel facility from hotel_facilities table
	 * @return true | false
     */
    function importHotalFacilities($data) {
        if ($this->db->insert_batch('hotel_facilities', $data)) {
            return true;
        }
        return false;
    }

 /**
     * @method getHotelRoomsPricingData() method used to fetch hotel pricing data.
	 * @param int $hotel_id holds hotel id
	 * @param int $room_type holds room type 
	 * @param int $currency holds currency
	 * @param string $period_from holds period from
	 * @param string $period_to holds period to
	 * @todo method used to fetch hotel pricing from hotel_rooms_pricing table.
	 * @return pricing_ids | false
     */


    function getHotalRoomsPricingData($hotel_id, $room_type, $currency, $period_from, $period_to) {
        $this->db->select('pricing_id');
        $this->db->where("hotel_id", $hotel_id);
        $this->db->where("room_type", $room_type);
        $this->db->where("curency_code", $currency);
        $this->db->where("period_from", $period_from);
        $this->db->where("period_to", $period_to);
        $query = $this->db->get('hotel_rooms_pricing');
        if ($query->num_rows() > 0) {
            return $query->row()->pricing_id;
        } else {
            return false;
        }
    }
  /**
     * @method editHotelRoomsPricingData() method used to update hotel pricing data
	 * @param int $hid holds hotel id -  contains hotel info tables.
	 * @param int $pricing_id holds pricing id - room pricing is edited through it.
	 * @param array() $data hold hotel room princing in the form of an array
	 * @todo this method  used to update hotel pricing from hotel_rooms_pricing table
     */
    function editHotalRoomsPricingData($hid, $pricing_id, $data) {
        $this->db->where('pricing_id', $pricing_id);
        $this->db->where("hotel_id", $hid);
        $this->db->update('hotel_rooms_pricing', $data);
    }

	/**
     * @method importHotalRoomsPricingData() method used to import hotel room pricing data
	 * @param array() $data hold hotel room pricing in the form of an array
	 * @todo this method  used to import hotel room pricing data into hotel_rooms_pricing table
     */
    function importHotalRoomsPricingData($data) {
        if ($this->db->insert('hotel_rooms_pricing', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method deleteRoompricing() method used to delete room pricing.
	 * @param int() $hid holds hotel id - room pricing deleted using hotel id.
	 * @param int() $room_type holds room type - contains room type details.
	 * @todo this method  used to delete room pricing data from hotel_rooms_pricing table
     */
	function deleteRoompricing($hid,$room_type) {
		$this->db->where('hotel_id', $hid);
		$this->db->where('room_type', $room_type);
        $this->db->delete('hotel_rooms_pricing');
    }
	/**
     * @method addHotalRoomPricingComplimentary() method used to insert hotel room pricing complimentary
	 * @param array() $data hold hotel room complimentary services in the form of an array
	 * @todo this method  used to insert hotel room pricing complimentary into hotel_rooms_pricing_complimentary table
	 * @return true|false
     */
    function addHotalRoomPricingComplimentary($data) {
        if ($this->db->insert_batch('hotel_rooms_pricing_complimentary', $data)) {
            return true;
        }
        return false;
    }
	/**
     * @method deleteRoomPricingComplimentary() method used to delete hotel room pricing complimentary services.
	 * @param int  $pricingId holds princing id containing priing details.
	 * @todo method  used to delete hotel room pricing complimentary from hotel_rooms_pricing_complimentary table
     */
    function deleteRoomPricingComplimentary($pricingId) {
      
            $this->db->where('pricing_id', $pricingId);
            $this->db->delete('hotel_rooms_pricing_complimentary');
      
    }
/**
     * @method importHotelRroomsPricingDetails() method used to import hotel room pricing details
	 * @param array()  $data holds hotel room pricing detail in the form of an array
	 * @todo this method  used to import hotel room pricing details into hotel_rooms_pricing_details table
	 * @return true|false
     */
    function importHotelRroomsPricingDetails($data) {
        if ($this->db->insert_batch('hotel_rooms_pricing_details', $data)) {
            return true;
        }
        return false;
    }
/**
     * @method updateHotelRroomsPricingDetails() method used to update hotel room pricing details
	 * @param array()  $data holds hotel room pricing detail in the form of an array
	 * @param int $pricing_id holds hotel room pricing id - primary key.
	 * @param int $market_id holds hotel room market id 
	 * @todo this method  used to update hotel room pricing details into hotel_rooms_pricing_details table
	 * @return id|false
     */
    function updateHotelRroomsPricingDetails($data, $pricing_id = '', $market_id = null) {
        if ($pricing_id != "") {
            $this->db->where('pricing_id', $pricing_id);
            $this->db->where('market_id', $market_id);
            $q = $this->db->get('hotel_rooms_pricing_details');
            if ($q->num_rows() > 0) {
                $this->db->where('pricing_id', $pricing_id);
                $this->db->where('market_id', $market_id);
                $this->db->update('hotel_rooms_pricing_details', $data);
                return $pricing_id;
            } else {
                $this->db->insert('hotel_rooms_pricing_details', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('hotel_rooms_pricing_details', $data);
            return $this->db->insert_id();
        }
        return;
    }

   /**
     * @method addRoomType() method used to insert hotel room type
	 * @param array()  $data holds hotel room type details in the form of an array
	 * @todo this method  used to insert hotel room type into entity_attributes table
	 * @return id|false
     */

    function addRoomType($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
/**
     * @method getRoomTypeId() method used to fetch hotel room type id
	 * @param string  $title contains hotel room type name.
	 * @todo this method  used to insert hotel room type into entity_attributes table
	 * @return id|false
     */
    function getRoomTypeId($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_room_types'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return false;
        }
    }
 /**
     * @method addMarket() method used to insert hotel market
	 * @param array()  $data holds hotel market in the form of an array in DB.
	 * @todo this method  used to insert hotel market type into entity_attributes table
	 * @return id|false
     */
    function addMarket($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
 /**
     * @method getMarket() method used to fetch hotel market id
	 * @param array()  $data hold hotel market in the form of an array
	 * @param string  $title hold hotel market title
	 * @todo this method  used to insert hotel market into entity_attributes table
	 * @return id|false
     */
    function getMarket($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_market'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return false;
        }
    }
/**
     * @method get_room_types() method used to fetch hotel room type details
	 * @param int  $hotel_id hold hotel id - data table for hotels.
	 * @todo method  used to fetch hotel room type details from hotel_rooms_pricing table
	 * @return array |false
     */
    function get_room_types($hotel_id = null) {
        if (!empty($hotel_id)) {
            $this->db->select('hotel_room_types.title, hotel_rooms_pricing.pricing_id,market_id,inclusions,curency_code,max_adult,max_child,inventory,DATE(period_from) as period_from,DATE(period_to) as period_to,double_price,triple_price,quad_price,breakfast_price,half_board_price,all_incusive_adult_price,extra_adult_price,extra_child_price,extra_bed_price,markets.title as market');
            $this->db->from('hotel_rooms_pricing');
            $this->db->join('hotel_rooms_pricing_details', 'hotel_rooms_pricing.pricing_id = hotel_rooms_pricing_details.pricing_id');
            $this->db->join('markets', 'hotel_rooms_pricing_details.market_id = markets.id');
            $this->db->join('hotel_room_types', 'hotel_rooms_pricing.room_type = hotel_room_types.type_id');
            $this->db->where("hotel_rooms_pricing.hotel_id", $hotel_id);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }
        return false;
    }
	
	/**
     * Common function to check already assigned attributes.
     * @param string $table 
     * @param int $hotel_id
     * @param string $where
     * @param string $selected_feild
     * @return true
     */
    function isDataExist($table = '',$hotel_id = null, $where = '',$select_feild = '') {
        if(!empty($select_feild) )
        $this->db->select($select_feild);
        
        if(!empty($where) && !empty($hotel_id))
        $this->db->where($where, $hotel_id);
        
        if(!empty($table))
        $query = $this->db->get($table);
        
//        echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

}
