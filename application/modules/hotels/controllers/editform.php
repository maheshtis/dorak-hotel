<?php

/**
 * @author tis
 * @author M
 * @functions:
	edit_information() method used to update hotel information using ajax request
 * @description this controller  is for  add/ edit delete hotel  related  information
 * @uses AdminController::Common base controller to perform all type of action.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Editform extends AdminController {
    
    /** @var string|null Should contain a user identification */

    public static $identityfier;
    function __construct() {
        parent::__construct();
        $this->_init();
    }
	
 /**
     * @method _init() method used initialise associative helpers('form','html','form_field_values','file','directory','array_column')and library('form_validation','datatables') and hotel_model
     */
    private function _init() {
        if (!$this->ion_auth->logged_in()) {
            // redirect non logged user to  login page
            redirect('auth/login', 'refresh');
        }
        $this->identityfier = $this->session->userdata('session_id') . '_' . time();
        $this->load->helper(array('form', 'html', 'form_field_values', 'file', 'directory'));
        $this->load->library('form_validation');
        $this->load->helper('array_column');
        $this->load->library('Datatables');
        $this->output->set_meta('description', 'Dorak Hotels');
        $this->output->set_meta('title', 'Dorak Hotels');
        $this->output->set_title('Dorak Hotels');
        $this->load->model('hotels/hotel_model');
        $this->accessDenidMessage = $this->config->item('access_not_allowed_message');
        $this->contractFileDir = $this->config->item('upload_contract_file_dir');
        $this->hotelFileDir = $this->config->item('hotel_gallery_dir');
        $this->hotel_model->clear_gallery_image_table();
        if (!checkAccess($this->accessLabelId, 'hotels', 'view')) {
            $sdata['message'] = $this->accessDenidMessage;
            $flashdata = array(
                'flashdata' => $sdata['message'],
                'message_type' => 'notice'
            );
            $this->session->set_userdata($flashdata);
            redirect('users', 'refresh');
        }
    }

     /**
     * @method edit_information() method used to update hotel information using ajax request
     * @param int $hid hotel id
	 * @todo this method used for update hotel information
     * @return json value|false
     */
    public function edit_information($hid = null) {
//        dump($_FILES);
        $data = array('message' => '');
        // this  function edit hotel information	
        if (!checkAccess($this->accessLabelId, 'hotels', 'edit')) {
            $flashdata = array( 'flashdata' => $this->accessDenidMessage, 'message_type' => 'notice');
            $this->session->set_userdata($flashdata);
            redirect('users', 'refresh');
        }
        if ($hid == "") {
            $data['message'] = 'Invalid  Access!';
            $flashdata = array( 'flashdata' => $data['message'], 'message_type' => 'notice');
            $this->session->set_userdata($flashdata);
            redirect("hotels"); /* redirect to hotel listing page */
        }
        $this->load->library('upload');
        $this->output->set_meta('description', 'Hotel Information');
        $this->output->set_meta('title', 'Hotel Information');
        $this->output->set_title('Hotel Information');
        $user = $this->ion_auth->user()->row();
        if ($this->session->userdata('stepsess') == '#tab-5') {
            $this->session->set_userdata('stepsess', '#tab-6');
        }
		
        if (!empty($_POST)) {
            $identity = $this->input->post('identity');
            $step = $this->input->post('step'); /* input for save data on tab basis */
            $this->session->set_userdata('stepsess', $step); /* session start for tabs */
            # validation rules 
           
                $contactData = array();
                $nearestDistanceData = array();  /* we'll store each distance row in this array */
                $hotelBankData = array();
                $purpose = $this->input->post('purpose');
                if ($purpose != '' && $purpose == 'Other') {
                    # insert new purpose
                    $purpose_options_oth = $this->input->post('purpose_options_oth');
                    if ($purpose_options_oth != '') {
                        # check if already in database if not then add
                        if ($pid = $this->hotel_model->getPurpose($purpose_options_oth)) {
                            $purposeId = $pid;
                        } else {
                            # add data in database 
                            $hotel_purpose = array('title' => $purpose_options_oth);
                            $purposeId = $this->hotel_model->addPurpose($hotel_purpose);
                        }
                    }
                } else {
                    $purposeId = $purpose;
                }
                # add new hotel chain if  user choose  other options the specified  value need to store int  hotel chain list table
                $hotelchain = $this->input->post('chain_id');
                if ($hotelchain != '' && $hotelchain == 'Other') {
                    # insert new purpose
                    $hotel_chain_oth = $this->input->post('hotel_chain_oth');
                    if ($hotel_chain_oth != '') {
                        /* check if already in database if not then add */
                        if ($hcid = $this->hotel_model->getHotelChain($hotel_chain_oth)) {
                            $chainId = $hcid;
                        } else {
                            /* add data in database  */
                            $hotel_chain = array('entity_title' => $hotel_chain_oth, 'entity_type' => $this->config->item('attribute_hotel_chain'));
                            $chainId = $this->hotel_model->addHotelChain($hotel_chain);
                        }
                    }
                } else {
                    $chainId = $hotelchain;
                }
                $hotel_detail = array( 'hotel_address' => $this->input->post('hotel_address'), 'post_code' => $this->input->post('post_code'),'star_rating' => $this->input->post('star_rating'),
                    'dorak_rating' => $this->input->post('dorak_rating'),'chain_id' => $chainId,'purpose' => $purposeId,'currency' => $this->input->post('currency'),'district' => $this->input->post('district'), 'status' => $this->input->post('status'),
                );
                $this->hotel_model->updateHotelInformation($hid, $hotel_detail);

                /* update contacts details  of the hotels */
                $nonDelContactids = array();
                $hotelContactDetails = $this->input->post('contact');
                if (count($hotelContactDetails) > 0) {
                    foreach ($hotelContactDetails as $hotelContact) {
                        if (isset($hotelContact['id']))
                            $hcontactid = $hotelContact['id'];
                        else
                            $hcontactid = '';
                        if ($hcontactid != "")
                            array_push($nonDelContactids, $hcontactid);
				$contactData = array('hotel_id' => $hid, 'position' => $hotelContact['position'],'name' => $hotelContact['name'], 'email' => $hotelContact['email'],'phone' => $hotelContact['phone'],
					'extension' => $hotelContact['extension']);
                        $updatecontactid = $this->hotel_model->updateHotalContact($contactData, $hcontactid);
                        array_push($nonDelContactids, $updatecontactid);
                    }
                    $this->hotel_model->deleteHotalContact($nonDelContactids, $hid);
                }
                # add hotel complementary  services
                $cmplServices = $this->input->post('complimentary');
                if (empty($cmplServices)) {
                    $this->hotel_model->deleteroomFacility($hid);
                }
                if ($cmplServices != "" && count($cmplServices) > 0) {
                    $hotelCmplServices = array();

                    foreach ($cmplServices as $cmplServiceId) {
                        if (isset($cmplServiceId) && $cmplServiceId != "") {
                            $hotelCmplServices[] = array('hotel_id' => $hid, 'cmpl_service_id' => $cmplServiceId );
                        }
                    }
                    if ($this->hotel_model->deleteroomFacility($hid)) {
                        $this->hotel_model->addHotalComplimentaryServices($hotelCmplServices);
                    }
                }
                # add hotel Facilities
                $facilities = $this->input->post('facilities');
                if (empty($facilities)) {
                    $this->hotel_model->deletehotelFacility($hid);
                }
                if ($facilities != "" && count($facilities) > 0) {
                    $hotelFacilities = array();
                    foreach ($facilities as $facility_id) {

                        if (isset($facility_id) && $facility_id != "") {
                            $hotelFacilities[] = array( 'hotel_id' => $hid, 'facility_id' => $facility_id
                            );
                        }
                    }
                    if ($this->hotel_model->deletehotelFacility($hid)) {
                        $this->hotel_model->addHotalFacilities($hotelFacilities);
                    }
                }
                $nonbnkIds = array();
                $bnk_details = $this->input->post('bank');
                if (!empty($bnk_details) && count($bnk_details) > 0) {
                    foreach ($bnk_details as $bnk) {
                        if (isset($bnk['id'])){ $bankid = $bnk['id'];}else{ $bankid = '';}
                        if ($bnk['account_number'] != "" && $bnk['account_name'] != "" && $bnk['bank_name'] != "" && $bnk['bank_address'] != "") {
                            $hotelBankData = array('hotel_id' => $hid, 'iban_code' => $bnk['iban_code'],
                                'account_number' => $bnk['account_number'],'account_name' => $bnk['account_name'], 'bank_name' => $bnk['bank_name'],'bank_address' => $bnk['bank_address'],'branch_name'=> $bnk['branch_name'],'branch_code' => $bnk['branch_code'],'swift_code' => $bnk['swift_code'], 'bank_ifsc_code' => $bnk['bank_ifsc_code'] );
                            $bankNid = $this->hotel_model->updateHotalbankaccounts($hotelBankData, $bankid);
                            array_push($nonbnkIds, $bankNid);
                        } else {
                            $this->hotel_model->deleteHotalbankaccount($bankid, $hid); // delete empty  account details
                        }
                    }
                    // delete non existing data
                    $this->hotel_model->deleteHotalbankaccounts($nonbnkIds, $hid);
                } else {
                    $this->hotel_model->deleteHotalbankaccounts(false, $hid);
                }
                /* distance by city name */
                $noncityIds = array();
                $distance_from_city_name = $this->input->post('distance_from_city');
                if (!empty($distance_from_city_name)) {
                    foreach ($distance_from_city_name as $cityname) {
                        if (isset($cityname['id'])){ $cityid = $cityname['id'];} else{$cityid = '';} 
						if ($cityid != "")
                            array_push($noncityIds, $cityid);
                        if (isset($cityname['name']) && $cityname['name'] != "" && isset($cityname['distance']) && $cityname['distance'] != "") {
                            $citynamedata = array('hotel_id' => $hid, 'distance_from' => $cityname['name'],         'distance_type' => '1','distance' => $cityname['distance'] );
                            $cityupdateid = $this->hotel_model->updateHotalcity($citynamedata, $cityid);
                            array_push($noncityIds, $cityupdateid);
                        }
                    }
                    $this->hotel_model->deleteHotalcity($noncityIds, $hid);
                }

                /* distance by city name close here */

                /* distance by airport name */
                $nonairIds = array();
                $distance_from_airport_name = $this->input->post('distance_from_airport');
                if (!empty($distance_from_airport_name)) {
                    foreach ($distance_from_airport_name as $airname) {
                        if (isset($airname['id'])){$airid = $airname['id'];}else{$airid = '';}
                        if ($airid != "")
                            array_push($nonairIds, $airid);
                        if (isset($airname['name']) && $airname['name'] != "" && isset($airname['distance']) && $airname['distance'] != "") {
                            $airnamedata = array('hotel_id' => $hid,'distance_from' => $airname['name'], 'distance_type' => '2','distance' => $airname['distance']);
                            $airupdateid = $this->hotel_model->updatedistanceForAirport($airnamedata, $airid);
                            array_push($nonairIds, $airupdateid);
                        }
                    }
                    $this->hotel_model->deletedistanceForAirport($nonairIds, $hid);
                }
                /* distance by airport name close here */
                /* add new hotel property type  if  user choose  other options */
                $hotelPtype = $this->input->post('property_type');
                if (empty($hotelPtype)) {
                    $this->hotel_model->deleteHotalpropertytype($hid);
                }
                if ($hotelPtype != '' && count($hotelPtype) > 0) {
                    $addHotalPropertyTypesData = array();
                    foreach ($hotelPtype as $HpType) {
                        if ($HpType != '') {
                            if ($HpType == 'Other') {
                                /* insert new purpose */
                                $property_type_oth = $this->input->post('property_type_oth');
                                if ($property_type_oth != '') {
                                    /* check if already in database if not then add */
                                    if ($hPid = $this->hotel_model->getHotelPropertyType($property_type_oth)) {
                                        $hotelPtypeId = $hPid;
                                    } else {
                                        /* add data in database */
                                        $hotelPtype = array('entity_title' => $property_type_oth, 'entity_type' => $this->config->item('attribute_property_types'));
                                        $hotelPtypeId = $this->hotel_model->addHotelPropertyType($hotelPtype);
                                    }
                                }
                            } else {
                                $hotelPtypeId = $HpType;
                            }

                            $addHotalPropertyTypesData[] = array('hotel_id' => $hid,'property_type_id' => $hotelPtypeId);
                        }
                    }
                    if (count($addHotalPropertyTypesData) > 0) {
                        if ($this->hotel_model->deleteHotalpropertytype($hid)) {
                            $this->hotel_model->addHotalPropertyTypes($addHotalPropertyTypesData);
                        }
                    }
                }
                /* add property close here */
                # add hotel renovation shedule	
                $nonDelRnvIds = array();
                $renovationSchedules = $this->input->post('rnv_shedule');
                if (count($renovationSchedules) > 0) {
                    foreach ($renovationSchedules as $renovationSchedule) {
                        if (isset($renovationSchedule['id']))
                            $scheduleId = $renovationSchedule['id'];
                        else
                            $scheduleId = '';
                        if ($scheduleId != "")
                            array_push($nonDelRnvIds, $scheduleId);
                        if ($renovationSchedule['date_from'] != "" && $renovationSchedule['date_to'] != "" && $renovationSchedule['renovation_type'] != "" && $renovationSchedule['area_effected'] != "") {
                            $hotelRenovationData = array( 'hotel_id' => $hid,'date_from' => convert_mysql_format_date($renovationSchedule['date_from']),'date_to' => convert_mysql_format_date($renovationSchedule['date_to']),'renovation_type' => $renovationSchedule['renovation_type'],'area_effected' => $renovationSchedule['area_effected']);
                            $idu = $this->hotel_model->updateHotalRenovationSchedule($hotelRenovationData, $scheduleId);
                            array_push($nonDelRnvIds, $idu);
                        }
                    }
                    // delete non existing data
                    $this->hotel_model->deleteHotalRenovationSchedule($nonDelRnvIds, $hid);
                }
                # add hotel complementary room
                $cmpli_room_night = $this->input->post('cmpli_room_night');
                $cmpli_room_id = $this->input->post('cmpli_room_id');
                $cmpli_date_from = convert_mysql_format_date($this->input->post('cmpli_date_from'));
                $cmpli_date_to = convert_mysql_format_date($this->input->post('cmpli_date_to'));
                $cmpli_upgrade = $this->input->post('cmpli_upgrade');
                $cmpli_exclude_dates = $this->input->post('cmpli_exclude_date');
                if ($cmpli_room_night != "" && $cmpli_date_from != "") {
                    $complimentary_room_data = array( 'hotel_id' => $hid,'room_night' => $cmpli_room_night,         'start_date' => $cmpli_date_from,'end_date' => $cmpli_date_to,'upgrade' => $cmpli_upgrade);

                    #update   details
                    if ($cmpli_room_id = $this->hotel_model->updateHotalComlimentaryRoom($complimentary_room_data, $hid, $cmpli_room_id)) {
                        $nonDelCmpExIds = array();
                        if (count($cmpli_exclude_dates) > 0) {
                            foreach ($cmpli_exclude_dates as $cmpli_exclude_date) {
                                if (isset($cmpli_exclude_date['id']))
                                    $cmExId = $cmpli_exclude_date['id'];
                                else
                                    $cmExId = '';
                                if ($cmExId != "")
                                    array_push($nonDelCmpExIds, $cmExId);
                                if ($cmpli_exclude_date['from'] != "" && $cmpli_exclude_date['to'] != "") {
                                    $cmpli_excluded_dates = array('cmpl_room_id' => $cmpli_room_id, 'exclude_date_from' => convert_mysql_format_date($cmpli_exclude_date['from']),   'excluded_date_to' => convert_mysql_format_date($cmpli_exclude_date['to']));
                                    $ncmExId = $this->hotel_model->updateHotalComlimentaryRoomExcludedDate($cmpli_excluded_dates, $cmExId);
                                    // function return new added id so we do not have to deltete that one
                                    array_push($nonDelCmpExIds, $ncmExId);
                                }
                            }
                            $this->hotel_model->deleteHotalComlimentaryRoomExcludedDate($nonDelCmpExIds, $cmpli_room_id);
                        }
                    }
                }

                # update hotel payment shedule  option				
                $paymentShedules = $this->input->post('payment_plan'); // multi dimensonal array posted from form
                if (!empty($paymentShedules) && count($paymentShedules) > 0) {
                    $nonDelPaymentIds = array();
                    foreach ($paymentShedules as $paymentShedule) {
                        if (isset($paymentShedule['id']))
                            $pmExId = $paymentShedule['id'];
                        else
                            $pmExId = '';
                        if ($pmExId != "")
                            array_push($nonDelPaymentIds, $pmExId);
                        if ($paymentShedule['payment_value'] != "" && $paymentShedule['payment_option_id'] != "") {
                            $hotelPaymentShedulesData = array( 'hotel_id' => $hid,'payment_option_id' => $paymentShedule['payment_option_id'], 'payment_value' => $paymentShedule['payment_value'],
                            );
                            $npmExId = $this->hotel_model->updateHotalPaymentShedules($hotelPaymentShedulesData, $pmExId);
                            // function return new added id so we do not have to deltete that one
                            array_push($nonDelPaymentIds, $npmExId);
                        }
                    }

                    $this->hotel_model->deleteHotalPaymentShedules($nonDelPaymentIds, $hid);
                }
                # add hotel payment shedule  option end 
                #add hotel cancellation information  
                $lowseason_cancelations = $this->input->post('lowseason_canceled'); // multi dimensonal array posted from form
                $highseason_cancelations = $this->input->post('highseason_canceled'); // multi dimensonal array posted from form
                $nonDelCancellationIds = array();
                if (!empty($lowseason_cancelations) && count($lowseason_cancelations) > 0) {
                    foreach ($lowseason_cancelations as $lowseason_cancel) {
                        if (isset($lowseason_cancel['id']))
                            $cancellationExId = $lowseason_cancel['id'];
                        else
                            $cancellationExId = '';
                        if ($cancellationExId != "")
                            array_push($nonDelCancellationIds, $cancellationExId);
                        if ($lowseason_cancel['before'] != "" && $lowseason_cancel['payment_request'] != "") {
                            $hotelCancellationData = array('hotel_id' => $hid,'cancelled_before' => $lowseason_cancel['before'],'payment_request' => $lowseason_cancel['payment_request'],'seasion' => 'low'
                            );
                            $nCanclExId = $this->hotel_model->updateHotalCancellation($hotelCancellationData, $cancellationExId);
                            // function return new added id so we do not have to deltete that one
                            array_push($nonDelCancellationIds, $nCanclExId);
                        }
                    }
                }
                if (!empty($highseason_cancelations) && count($highseason_cancelations) > 0) {
                    foreach ($highseason_cancelations as $highseason_cancel) {
                        if (isset($highseason_cancel['id']))
                            $cancellationExId = $highseason_cancel['id'];
                        else
                            $cancellationExId = '';
                        if ($cancellationExId != "")
                            array_push($nonDelCancellationIds, $cancellationExId);

                        if ($highseason_cancel['before'] != "" && $highseason_cancel['payment_request'] != "") {
                            $hotelCancellationData = array('hotel_id' => $hid,'cancelled_before' => $highseason_cancel['before'],'payment_request' => $highseason_cancel['payment_request'], 'seasion' => 'high');
                            $nCanclExId = $this->hotel_model->updateHotalCancellation($hotelCancellationData, $cancellationExId);
                            // function return new added id so we do not have to deltete that one
                            array_push($nonDelCancellationIds, $nCanclExId);
                        }
                    }
                }
                $this->hotel_model->deleteHotalCancellation($nonDelCancellationIds, $hid);
                #add hotel cancellation information end 
                $aplicable_child_age_group = $this->input->post('aplicable_child_age_group');
                if ($aplicable_child_age_group != "") {
                    $uchData = array('child_age_group_id' => $aplicable_child_age_group);
                    $this->hotel_model->updateHotelDetails($hid, $uchData);
                }
                #add hotel contract information
                $contract_start_date = $this->input->post('contract_start_date');
                $contract_end_date = $this->input->post('contract_end_date');
                $contract_signed_by = $this->input->post('contract_signed_by');
                $contract_file_name = '';
                $responseArray = array();
                if (isset($_FILES['contract_file']) && is_uploaded_file($_FILES['contract_file']['tmp_name']) && $contract_end_date == "" && $contract_signed_by == "") {
                    $data['message'] = '<li> Hotel Contract Details Not Added,Hotel Contract Details Fields are missing! </li>';
                } elseif ($contract_start_date != "" && $contract_end_date != "" && $contract_signed_by != "") {
                    if (isset($_FILES['contract_file']) && is_uploaded_file($_FILES['contract_file']['tmp_name'])) {
//                        echo "jeevan";
                        $config['upload_path'] = $this->contractFileDir;
                        $config['allowed_types'] = 'pdf|jpg|jpeg';
                        $config['max_size'] = '0'; //2 mb , 0 for unlimited
                        $config['max_width'] = '0'; // unlimited
                        $config['max_height'] = '0';
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('contract_file')) {
                            $error = $this->upload->display_errors();
                            $data['message'] = '<li>Notice.Contract file not uploaded because ' . $error . ' , please upload it again, by editing the hotel details!</li>';
                        } else {
                            $uploadedFileDetail = $this->upload->data();
                            $contract_file_name = $uploadedFileDetail['file_name'];
                        }
                    }
                    $hotelContractData = array('hotel_id' => $hid,'start_date' => convert_mysql_format_date($contract_start_date),'end_date' => convert_mysql_format_date($contract_end_date),'signed_by' => $contract_signed_by,'contract_file' => $contract_file_name);
                    if ($this->hotel_model->addHotalContract($hotelContractData)) {
                        if ($contract_file_name != "") {
                            $data['message'] = '<li>Hotel Contract Details Added</li>';
                        } else {
                            $data['message'] = '<li>Hotel Contract Details</li> ';
                        }
                    }
                    
                    $explode = explode('.',$contract_file_name);
                    $file_extention = !empty($explode[1]) ? $explode[1] : '';
                    $name_with_break = strlen($contract_file_name) > 16 ? substr($contract_file_name, 0, 20)."...".$file_extention : $contract_file_name;
                    
                    $responseArray['contract_file'] = $name_with_break;
                    $responseArray['contract_name'] = $contract_file_name;
                    $responseArray['start_date'] = $contract_start_date;
                    $responseArray['end_date'] = $contract_end_date;
                    $responseArray['signed_by'] = $contract_signed_by;
                    $responseArray['success'] = 'true';

                echo json_encode($responseArray);die;
                }

                # edit hotel pricing section					
                $hotelPricingRecords = $this->input->post('pricing'); // multi dimensonal array posted from form
                $hotelRoomsPricingData = array(); // for table hotel_rooms_pricing
                if (!empty($hotelPricingRecords) && count($hotelPricingRecords) > 0) {
                    $nonDelPricingRecordIds = array();
                    foreach ($hotelPricingRecords as $hotelPricingRecord) {
                        //echo $hotelPricingRecord['pricing_id']."<br>";
                        // prepare  records  which will not deleted other record whicg are not submitted will be deleted
                        if (isset($hotelPricingRecord['pricing_id']))
                            $prExId = $hotelPricingRecord['pricing_id'];
                        else
                            $prExId = '';

                        $hotelRroomsPricingDetailsData = array(); // for hotel_rooms_pricing_details table
                        $hotelRoomsPricingComplimentaryData = array(); // for table hotel_rooms_pricing_complimentary

                        if (isset($hotelPricingRecord['room_type']) && $hotelPricingRecord['room_type'] != "" && isset($hotelPricingRecord['invenory']) && $hotelPricingRecord['invenory'] != "") {
                            $hotelRoomsPricingData = array('hotel_id' => $hid,'room_type' => $hotelPricingRecord['room_type'],'inclusions' => $hotelPricingRecord['pricing_inclusions'],'curency_code' => $hotelPricingRecord['currency'],'max_adult' => $hotelPricingRecord['max_adult'],'max_child' => $hotelPricingRecord['max_child'],'inventory' => $hotelPricingRecord['invenory'],'period_from' => convert_mysql_format_date($hotelPricingRecord['period_from_date']),'period_to' => convert_mysql_format_date($hotelPricingRecord['period_to_date']));
                            $pricing_inserted_id = $this->hotel_model->updateHotalRoomsPricingData($hotelRoomsPricingData, $prExId);
                            if ($pricing_inserted_id) {
                                array_push($nonDelPricingRecordIds, $pricing_inserted_id); // add id to non delete list			
                                // if hotel pricing record added in database // add  room pricing facilities
                                if (isset($hotelPricingRecord['room_facilities'])) {
                                    $pricingRoomFacilities = $hotelPricingRecord['room_facilities'];
                                    if (!empty($pricingRoomFacilities) && count($pricingRoomFacilities) > 0) {
                                        foreach ($pricingRoomFacilities as $roomFacility) {
                                            if ($roomFacility != "") {
                                                $hotelRoomsPricingComplimentaryData[] = array('pricing_id' => $pricing_inserted_id,'cmpl_service_id' => $roomFacility);
                                            }
                                        }
                                        if (!empty($hotelRoomsPricingComplimentaryData) && count($hotelRoomsPricingComplimentaryData) > 0) {
                                            $this->hotel_model->addHotalRoomPricingComplimentary($hotelRoomsPricingComplimentaryData, $pricing_inserted_id);
                                        }
                                    }
                                }// add  room pricing facilities end
                                // add  room pricing details
                                if (isset($hotelPricingRecord['price_detail'])) {
                                    $roomPriceingDetails = $hotelPricingRecord['price_detail'];
                                    if ($pricing_inserted_id == '')
                                        $pricing_inserted_id = $prExId;
                                    if (!empty($roomPriceingDetails) && count($roomPriceingDetails) > 0) {
                                        $nonDelPricingDetailRecordIds = array();
                                        // store  ids which need to keep after update
                                        foreach ($roomPriceingDetails as $roomPriceingDetail) {
                                            if (isset($roomPriceingDetail['pricing_detId']))
                                                $prDetExId = $roomPriceingDetail['pricing_detId'];
                                            else
                                                $prDetExId = '';
                                            if ($prDetExId != "")
                                                array_push($nonDelPricingDetailRecordIds, $prDetExId);
                                            if ($roomPriceingDetail['market_id'] != "") {
                                                $hotelRroomsPricingDetailsData = array('pricing_id' => $pricing_inserted_id,'market_id' => $roomPriceingDetail['market_id'],'double_price' => $roomPriceingDetail['double_price'],'triple_price' => $roomPriceingDetail['triple_price'],'quad_price' => $roomPriceingDetail['quad_price'],'breakfast_price' => $roomPriceingDetail['breakfast_price'],'half_board_price' => $roomPriceingDetail['half_board_price'],'all_incusive_adult_price' => $roomPriceingDetail['all_incusive'],'extra_adult_price' => $roomPriceingDetail['extra_adult'],'extra_child_price' => $roomPriceingDetail['extra_child'],'extra_bed_price' => $roomPriceingDetail['extra_bed']);
                                                $npricingDetId = $this->hotel_model->updateHotelRroomsPricingDetails($hotelRroomsPricingDetailsData, $prDetExId, $pricing_inserted_id);
                                                array_push($nonDelPricingDetailRecordIds, $npricingDetId);
                                            }
                                        }
                                        // delete non  existing  pricing details
                                        if (count($nonDelPricingDetailRecordIds) > 0)
                                            $this->hotel_model->deleteHotalHotelRroomsPricingDetails($nonDelPricingDetailRecordIds, $pricing_inserted_id);
                                    }// end of pricing records if exists	
                                }
                            }
                            // add  room pricing details end
                        }// end if of if   pricing data added							
                        // delete other not existing procing  of the hotel
                    }// end of pricing records for	

                    if (count($nonDelPricingRecordIds) > 0)
                        $this->hotel_model->deleteHotalRoomsPricingData($nonDelPricingRecordIds, $hid);
                }// end of pricing records if exists	
                # add hotel pricing section end
                $identity = $this->input->post('identity');
                $dbidentity = $this->hotel_model->get_dbidentity($identity);
                if ($dbidentity && $identity == ($dbidentity->identifier)) {
                    $data_identity = array('hotel_id' => $hid);
                    $this->hotel_model->update_images($identity, $data_identity);
                }
				echo $return = 1; die;
				
                /*  zip folder code close   */
                $data['message'] = '<li>Hotel information updated</li>';
                $flashdata = array(
                    'flashdata' => $data['message'],
                    'message_type' => 'sucess'
                );
                $this->session->set_userdata($flashdata);
            
        }
       
    }
}

