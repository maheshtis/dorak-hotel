<div class="main-part-container"> 
<div class="main-hotel">
<div class="hotel-serach-form">
<h3>Edit role</h3>
<?php echo form_open(current_url(),array('class' => 'edit-access-role', 'id' => 'edit-access-role'));?>
<div class="dd">
<div class="dt">
<label> Access Level </label>
<?php echo form_input($group_name);?>
</div>
</div>
<div class="dd access-specifier-block">
<h4>Access</h4>
 <?php
if($acl_controllers)
{
?>
<table class="access-specifier" cellspacing="0" width="100%">
<thead>
	<tr>
		<th>Name</th>
		<th>View</th>
		<th>Add</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr>
</thead>
<tbody>
<?php
foreach($acl_controllers as $controller)
 {
	$checked = null;
	if(checkInAssignedACLResource($controller->id,$ACLResource))
			$checked= ' checked="checked"';
			$cdatalabel=$controller->title;
			if($cdatalabel=="")
			{
				$cdatalabel=str_replace('_',' ',$controller->class_name);
			}
			$cntrolerMethos=array();			
			?>
			<?php $conrollerACLMethods=acl_controller_methods($controller->id) ?>
			<?php if($conrollerACLMethods) {?>			
			<?php
		    $mchecked = null;
			$mcheckedall = null;
			foreach($conrollerACLMethods as $method)
			{				
			if(checkInAssignedACLResource($method->id,$ACLResource,'method'))
				$mchecked= ' checked="checked"';
			else
				$mchecked='';
				$datalabel=$method->title;
				if($datalabel=="")
				{
				$datalabel=str_replace('_',' ',$method->class_method_name);
				}
				if(strtolower($datalabel)==='view')
				{
				$cntrolerMethos[$controller->id]['view_method_id']=$method->id;
				$cntrolerMethos[$controller->id]['view_label']=$datalabel;
				$cntrolerMethos[$controller->id]['view_chk']=$mchecked;
				
				}
				elseif(strtolower($datalabel)==='add')
				{
				$cntrolerMethos[$controller->id]['add_method_id']=$method->id;
				$cntrolerMethos[$controller->id]['add_label']=$datalabel;
				$cntrolerMethos[$controller->id]['add_chk']=$mchecked;
				}
				elseif(strtolower($datalabel)==='edit')
				{
				$cntrolerMethos[$controller->id]['edit_method_id']=$method->id;
				$cntrolerMethos[$controller->id]['edit_label']=$datalabel;
				$cntrolerMethos[$controller->id]['edit_chk']=$mchecked;
				}
				elseif(strtolower($datalabel)==='delete')
				{
				$cntrolerMethos[$controller->id]['delete_method_id']=$method->id;
				$cntrolerMethos[$controller->id]['delete_label']=$datalabel;
				$cntrolerMethos[$controller->id]['del_chk']=$mchecked;
				}
				}
				if($cntrolerMethos[$controller->id]['view_chk']!="" && $cntrolerMethos[$controller->id]['del_chk']!="" && $cntrolerMethos[$controller->id]['edit_chk']!="" && $cntrolerMethos[$controller->id]['add_chk'] !="")
				{
				$mcheckedall= ' checked="checked"';
				}
				else{
					$mcheckedall="";
				}
				
				?>
			<tr>
			<td>
			<?php echo form_checkbox('acl_controllerAll['.$controller->id.']',$controller->id,'',$mcheckedall.' class="checkallrow" data-label="'.$cdatalabel.'" ');?>
			</td>
			<td>	
			<?php //echo form_checkbox('acl_controller[]',$controller->id,'',$checked.' class="achecked" data-label="'.$cdatalabel.'" ');?>
		
			<?php if(isset($cntrolerMethos[$controller->id]['view_method_id'])){?>
			<?php echo form_checkbox('acl_method['.$controller->id.'][]',$cntrolerMethos[$controller->id]['view_method_id'],'',$cntrolerMethos[$controller->id]['view_chk'].' class="achecked" data-label="'.$cntrolerMethos[$controller->id]['view_label'].'" ');?>
			<?php }?>
			</td>
			<td>
			<?php if(isset($cntrolerMethos[$controller->id]['add_method_id'])){?>
			<?php echo form_checkbox('acl_method['.$controller->id.'][]',$cntrolerMethos[$controller->id]['add_method_id'],'',$cntrolerMethos[$controller->id]['add_chk'].' class="achecked"  data-label="'.$cntrolerMethos[$controller->id]['add_label'].'" ');?>
			<?php }?>
			</td>
			<td>
			<?php if(isset($cntrolerMethos[$controller->id]['edit_method_id'])){?>
			<?php echo form_checkbox('acl_method['.$controller->id.'][]',$cntrolerMethos[$controller->id]['edit_method_id'],'',$cntrolerMethos[$controller->id]['edit_chk'].' class="achecked" data-label="'.$cntrolerMethos[$controller->id]['edit_label'].'" ');?>
			<?php }?>
			</td>
			
			<td>
			<?php if(isset($cntrolerMethos[$controller->id]['delete_method_id'])){?>
			<?php echo form_checkbox('acl_method['.$controller->id.'][]',$cntrolerMethos[$controller->id]['delete_method_id'],'',$cntrolerMethos[$controller->id]['del_chk'].' class="achecked" data-label="'.$cntrolerMethos[$controller->id]['delete_label'].'" ');?>
			<?php }?>
			</td>		   
		<?php
			}?>			
		<?php			
			}
		?>
	  
	  </td>
	  </tr>

	  <?php
	  }
?>
<?php //print_r($assignedACLResource);
?>
</tbody>
</table>
 </div>
<input type="submit" value="Save" class="submit-button1">
<div class="cancel_block">
<a class="submit-button1" href="<?php echo base_url(); ?>users/user_roles">Cancel </a>
</div> 
<?php echo form_close();?>
</div>
</div>
</div>
<script>

jQuery(document).ready(function () {
	$('.checkallrow').click(function() {
    var checkboxes = $(this).closest('tr').find(':checkbox');
	var lvl = $(this).closest('tr').find('label.achecked');
    if($(this).is(':checked')) {
        checkboxes.attr('checked', 'checked');
		lvl.addClass("active");
    } else {
        checkboxes.removeAttr('checked');
		lvl.removeClass("active");
    }
});
$('table').on('change', '.achecked', function (e) {
   var checkboxesall = $(this).closest('tr').find(".checkallrow");
	var lvl = $(this).closest('tr').find('label.checkallrow');
	checkboxesall.removeAttr('checked');
	lvl.removeClass("active");
});


    });

</script>