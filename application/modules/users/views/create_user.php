<h2>Create user</h2>
<div class="setting-room-type">
<div class="mian-head setting-section">
<?php echo form_open("users/create_user",array('class' => 'company_user', 'id' => 'company_user'));?>

<div class="dd">
<div class="dt">
<label>First Name <span class="vali-star">*</span></label>
<?php echo form_input($first_name);?>
<?php echo form_error('first_name');?>
</div>
<div class="dt">
<label>Last Name </label>
<?php echo form_input($last_name);?>
</div>

<div class="dt">
<label>Company<span class="vali-star">*</span></label>
 <?php echo form_dropdown('company',$companies,$posted_company,' tabindex="4" data-validation="required" data-live-search="true" class="selectpicker"')?>   
<?php echo form_error('company');?>
 </div>
</div>
<div class="dd">
<div class="dt">
<label>Designation <span class="vali-star">*</span></label>
<?php echo form_dropdown('position',$positions,$posted_position,' data-validation="required" tabindex="5" class="selectpicker"')?>   
</div>
<div class="dt">
<label>Department <span class="vali-star">*</span></label>                           
<?php echo form_dropdown('department',$departments,$posted_department,' data-validation="required" tabindex="6"  class="selectpicker" ')?>                                   
</div>
<div class="dt">
<label>Role <span class="vali-star">*</span></label>
 <?php echo form_dropdown('role',$roles,$posted_role,' data-validation="required"  tabindex="7" id="access_level_id" class="selectpicker" ')?>   
</div>
</div>
<div class="dd">
<div class="dt">
<label>Email<span class="vali-star">*</span></label>
<?php echo form_input($email);?>                                       
</div>
<div class="dt">
<label>Phone <span class="vali-star">*</span></label>
 <?php echo form_input($phone);?>
</div>
<div class="dt">

  <label>Password <span class="vali-star">*</span></label>
   <?php echo form_input($password);?>
   <?php echo form_error('password_confirmation');?>
   </div>
</div>   
<div class="dd last">

<div class="dt">
   <label>Confirm Password <span class="vali-star">*</span></label>
   <?php echo form_input($password_confirm);?> 
<?php echo form_error('password');?>   
</div>
 <?php
     if($identity_column!=='email') {
          echo '<div class="dt">';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</div>';
      }
      ?>
</div>  
<input type="submit" value="Save" class="submit-button1">
<div class="cancel_block">
<a class="submit-button1" href="<?php echo base_url(); ?>users">Cancel </a>
</div> 
<?php echo form_close();?>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/security.js"></script>
<script>

// Add custom validation rule
$.formUtils.addValidator({
  name : 'phone_number',
  validatorFunction : function(value, $el, config, language, $form) {
    //var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/; 
  //[0-9\-\(\)\s]+.
  //([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})

	var filter =/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})?([ .-]?)\2?([ .-]?)([0-9]{4})/;
   if (filter.test(value)) {
        return true;
    }
    else {
        return false;
    }   
  },
  errorMessage : 'Enter valid phone number',
  errorMessageKey: 'badPhoneNumber'
});


  $(document).ready(function (){	  
	$.validate({
	form : '#company_user',
	modules : 'security',
	  onModulesLoaded : function() {
    var optionalConfig = {
      fontSize: '12pt',
      padding: '4px',
      bad : 'Very bad',
      weak : 'Weak',
      good : 'Good',
      strong : 'Strong'
    };
    $('input[name="password_confirmation"]').displayPasswordStrength(optionalConfig);
  }
	});   
  });
</script>