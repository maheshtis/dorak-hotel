<div class="main-part-container"> 
<div class="main-hotel">
<div class="hotel-serach-form rols-pg">
<h3>Access levels</h3>

<table id="user-role-table" class="table settings-table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
<thead>
	<tr>
		<th width="90%">Level</th>		
		<th width="10%">Action</th>
	</tr>
	</thead>
<tbody>
	<?php foreach ($user_roles as $role):?>
		<tr>
            <td><label><?php echo $role->name;?></label></td>
			<td><?php if($role->id!=1){?><?php echo anchor("users/edit_role/".$role->id, 'Edit',' class="edit_rec"') ;?><?php }?></td>
		</tr>
	<?php endforeach;?>
 </tbody>
</table>
</div>
</div>
</div>
<script>
  // <![CDATA[
$(document).ready(function (){
   var table = $('#user-role-table').DataTable(
   {
	"bPaginate": false,
	"aoColumns": [
    { "bSortable": true },	 
	{ "bSortable": false }
    ],
	 "columns": [  
    { "width": "90%" },
    { "width": "10%" }  
  ],
      'order': [[0, 'asc']],
	  "searching": false,
		"lengthMenu": [[10, 25, 50, -1], [" 10 Per Page"," 25 Per Page", " 50 Per Page", "All"]],
		language : {
        sLengthMenu: "View: _MENU_"
		}
			
   });
  
});
  // ]]></script>