<?php
class Users_model extends AdminModel {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
  function getHotelChain($title,$chain_id='')
    {
		$this->db->select('chain_id');
		$this->db->where("title", $title);
		if($chain_id!="")	
		$this->db->where("chain_id !=", $chain_id);			
        $query = $this->db->get('hotel_chain_list');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->chain_id;
		}
		else{
			return false;
		}
	}
	
function getHotelChainList()
    {
		$this->db->select('chain_id,title'); 
		$this->db->order_by('title');
        $query = $this->db->get('hotel_chain_list');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	
/*  hotel pricings  section  end*/
}

}