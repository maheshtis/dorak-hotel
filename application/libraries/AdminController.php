<?php
class AdminController extends MX_Controller {
  	protected $paginateConfig = array();
  	protected $languages = array();
  	protected $breadcrum;
	protected $userId;
	protected $lang_id;
	public $curentControler;
	public $controlerMethod;
	public $accessDenidMessage;
	public $accessLabelId;
    public function __construct()
    {
		$this->load->helper('date_helper');
		$lastActivity = $this->session->userdata('last_activity');
		$curr_time = now();
		$diff = $curr_time - $lastActivity;
		$interval = 10;
		if($diff > $interval*60 ){
			$this->session->sess_destroy();
			 redirect('auth/login', 'refresh');
		}else{
			$this->session->set_userdata('last_activity',time());
		}
		$this->load->library(array('ion_auth','user_agent'));
        $this->load->helper(array('url','acl_helper'));
		$this->accessDenidMessage=$this->config->item('access_not_allowed_message');
        parent::__construct();
		$this->output->set_meta('description','Dorak information');
		$this->output->set_meta('title','Dorak information');
		$this->output->set_title('Dorak');
		$this->output->set_template('default');
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		
      // if not logged in - go to home page
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        } 
	$user_info = $this->ion_auth->user()->row();
	$user_name = $user_info->first_name.' '.$user_info->last_name;
	$this->output->set_user($user_name);
	if ($this->ion_auth->logged_in()){
		$this->curentControler=$this->router->fetch_class(); // current class
		$this->controlerMethod=$this->router->fetch_method(); // curent class method
		$user = $this->ion_auth->user()->row();	
		$this->accessLabelId = $this->ion_auth->get_users_groups($user->id)->row()->id;	
		if(!$this->ion_auth->is_admin() && ($this->curentControler!='users' && $this->controlerMethod!='index'))// check user access for class/methods 
		{
		$ac=checkAccess($this->accessLabelId,$this->curentControler,'view');// if user have permession to see
		if(!$ac)
		{
					$sdata['message'] ='Permession required to access this page';					
					$flashdata = array(
							'flashdata'  => $sdata['message'],
							'message_type'     => 'notice'
							);				
					$this->session->set_userdata($flashdata);
			if ($this->agent->is_referral() && $this->agent->is_referral()!=site_url('auth/login'))	
			{
			redirect($this->agent->referrer());
			}
			else
			{
			 redirect('users', 'refresh');	
			}
		}
		}
		}
    }

}
