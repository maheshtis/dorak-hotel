<?php 
function removeExtraspace($str)
{
	$trimstr=trim($str);
	return $new_str = preg_replace('/\s+/',' ', $trimstr);
}

function getCountriesOptions($includeSel='')
{
	$countriesOptions =array();	
	if($includeSel!="")	
	{
	$countriesOptions['']="Select"	;
	}
	$CI =& get_instance();	
	$CI->db->select('country_code,country_name');
	$CI->db->order_by('country_name','asc'); 
    $query = $CI->db->get('countries');
        if ($query->num_rows() > 0)
		{
		$countriesList=$query->result(); 
		foreach($countriesList as $country)
			{
		$countriesOptions[$country->country_code]=$country->country_name;
		}
		
		}
	return $countriesOptions;	
}

function getCountyCities($country_code="TR")
{
	if($country_code=="")
	{
		$country_code="TR";
	}
	$CI =& get_instance();	
	if($country_code!="")
	{
	$CI->db->select('city_name'); 
	$CI->db->where('country_code',$country_code);
	$CI->db->order_by('city_name','asc'); 
    $query = $CI->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	return false;	
	
}

function getCityDistricts($city_name=null)
{
	$CI =& get_instance();	
	if($city_name!="")
	{
	$CI->db->select('district_name'); 
	$CI->db->where('city_name',$city_name);
	$CI->db->order_by('district_name','asc'); 
    $query = $CI->db->get('city_districts');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
	}
	return false;	
	
}


function getCityDistrictOptions($city_name=false,$includeSel="")
{
$districtOptions =array();
if($includeSel!="")	
{
$districtOptions['']="Select"	;
}	
	$CI =& get_instance();	
	if($city_name!="")
	{
	$CI->db->select('district_name'); 
	$CI->db->where('city_name',$city_name);
	$CI->db->order_by('district_name','asc'); 
    $query = $CI->db->get('city_districts');
        if ($query->num_rows() > 0)
		{
        $distList=$query->result(); 
		foreach($distList as $dist)
		{
			$districtOptions[$dist->district_name]=$dist->district_name;
		}
		}
	}
return $districtOptions;
}

function getCountyCitiesOptions($country_code="TR",$includeSel="")
{
$citiesOptions =array();
if($includeSel!="")	
{
$citiesOptions['']="Select"	;
}	
	if($country_code=="")
	{
		$country_code="TR";
	}
	$CI =& get_instance();	
	if($country_code!="")
	{
	$CI->db->select('city_name,id');
	$CI->db->where('country_code',$country_code);
	$CI->db->order_by('city_name','asc'); 
    $query = $CI->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
        $citiesList=$query->result(); 
		foreach($citiesList as $city)
		{
			$citiesOptions[$city->city_name]=$city->city_name;
		}
		}
	}
return $citiesOptions;
}

function getCityCode($city_name)
{
	if(!$city_name=="")
	{
	$CI =& get_instance();	
	$CI->db->select('city_code'); 
	$CI->db->where('city_name',$city_name);
    $query = $CI->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
         $row=$query->row(); 
		 return $row->city_code;
		}
		else{
			return false;
		}
	
	}
	return false;	
}


function getHotelStatus()
{
	$hotelStatusOptions=array();
	$CI =& get_instance();	
	$CI->db->select('id,entity_title');
	$CI->db->order_by('entity_title','asc'); 
	$CI->db->where('entity_type',$CI->config->item('attribute_hotel_status')); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        $hotelStatus=$query->result(); 
		foreach($hotelStatus as $hStatus)
		{
			$hotelStatusOptions[$hStatus->id]=$hStatus->entity_title;

		}
		}	
		return $hotelStatusOptions;
			
}


function getStarRatings()
{
	$starRatingOptions=array();	
	$starRatingOptions[]="Select";	
	$CI =& get_instance();	
	$CI->db->select('id,entity_title');
	$CI->db->where('entity_type',$CI->config->item('attribute_star_ratings')); 
	$CI->db->order_by('entity_title','asc'); 
    $query = $CI->db->get('entity_attributes');
      if ($query->num_rows() > 0)
		{
        $starRatings=$query->result(); 		
		
		foreach($starRatings as $sRating)
		{
			if($sRating->entity_title==1)
			{
			$post_fix=' Star';	
			}
			elseif($sRating->entity_title>1){
				$post_fix=' Stars';	
			}
			$starRatingOptions[$sRating->entity_title]=$sRating->entity_title.$post_fix;
		}
		
		}
		$starRatingOptions['0']="Unrated";
		return $starRatingOptions;	
}

function getDorakRatings()
{
	$dorakRatingOptions=array();
	$dorakRatingOptions[]="Select";	
	$CI =& get_instance();	
	$CI->db->select('id,entity_title');
	$CI->db->where('entity_type',$CI->config->item('attribute_dorak_ratings')); 
	$CI->db->order_by('entity_title','asc'); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        $dorakRatings=$query->result(); 
		
		foreach($dorakRatings as $dRating)
		{
			if($dRating->entity_title==1)
			{
			$post_fix=' Star';	
			}
			elseif($dRating->entity_title>1){
				$post_fix=' Stars';	
			}
			$dorakRatingOptions[$dRating->entity_title]=$dRating->entity_title.$post_fix;

		}
		
		}
		$dorakRatingOptions['0']="Unrated";
	return $dorakRatingOptions;	
		
}


function getTodayDate()
{
	return date('d/m/Y');
}

function getMarkets()
{
	$CI =& get_instance();	
	$CI->db->select('id,entity_title');
	$CI->db->where('entity_type',$CI->config->item('attribute_market'));
	$CI->db->order_by('entity_title','asc'); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
		
}

function getCurrencies()
{
	$CurrenciesOptions =array();
	$CI =& get_instance();	
	$CI->db->select('code'); 
	$CI->db->order_by('code','asc'); 
    $query = $CI->db->get('currencies');
        if ($query->num_rows() > 0)
		{
        $currencies=$query->result(); 
		foreach($currencies as $currency)
		{
		$CurrenciesOptions[$currency->code]=$currency->code;
		}
		}
		return $CurrenciesOptions;	
}

function getChildAgeOptions()
{
	$ageGroupsOption =array();
	$CI =& get_instance();	
	$CI->db->select('id,entity_title'); 
	$CI->db->order_by('entity_title','asc'); 
	$CI->db->where('entity_type',$CI->config->item('attribute_child_group')); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        $childAgeGroups= $query->result(); 
		foreach($childAgeGroups as $childAgeGroup)
		{
		$ageGroupsOption[$childAgeGroup->id]=$childAgeGroup->entity_title;
		}
		}
		
	return $ageGroupsOption;
		
}


function getAvailabeComplementaryServices()
{
	$CI =& get_instance();	
	$CI->db->select('id,entity_title'); 
	$CI->db->order_by('entity_title','asc'); 
	$CI->db->where('entity_type',$CI->config->item('attribute_complementy_service')); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
		
}

function getAvailabeFacilities()
{
	$CI =& get_instance();	
	$CI->db->select('id,entity_title'); 
	$CI->db->order_by('entity_title','asc'); 
	$CI->db->where('entity_type',$CI->config->item('attribute_facilities')); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}	
}

function getRoomTypes()
{
	$CI =& get_instance();	
	$CI->db->select('id,entity_title'); 
		$CI->db->order_by('entity_title','asc'); 
		$CI->db->where('entity_type',$CI->config->item('attribute_room_types')); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
}


function getPurposeOptions()
{
	$hotelPurposeOptions =array(''  => 'Select');	
	$CI =& get_instance();	
	$CI->db->select('id,entity_title'); 
	$CI->db->order_by('entity_title','asc'); 
	$CI->db->where('entity_type',$CI->config->item('attribute_hotel_purpose')); 
    $query = $CI->db->get('entity_attributes');
       if ($query->num_rows() > 0)
		{
        $hotelPurposes=$query->result(); 
		foreach($hotelPurposes as $hotelPurpose)
		{
			$hotelPurposeOptions[$hotelPurpose->id]=$hotelPurpose->entity_title;
		}
		}
	return $hotelPurposeOptions;
}

function getPropertyTypeOptions($needsel='')
{
	$hotelPropertyTypeOptions =array();
	if($needsel=="")
	{
	$hotelPropertyTypeOptions['']='Select';	
	}
	$CI =& get_instance();	
	$CI->db->select('id,entity_title'); 
	$CI->db->order_by('entity_title','asc'); 
	$CI->db->where('entity_type',$CI->config->item('attribute_property_types')); 
    $query = $CI->db->get('entity_attributes');
       if ($query->num_rows() > 0)
		{
        $hotelPropertyTypes=$query->result(); 
		foreach($hotelPropertyTypes as $hotelPropertyType)
		{
			$hotelPropertyTypeOptions[$hotelPropertyType->id]=$hotelPropertyType->entity_title;
		}
		}
	return $hotelPropertyTypeOptions;
}




function getHotelChainsOptions()
{
	$hotel_chain_options =array(''  => 'Select');	
	$CI =& get_instance();	
	$CI->db->select('id,entity_title'); 
	$CI->db->order_by('entity_title','asc'); 
	$CI->db->where('entity_type',$CI->config->item('attribute_hotel_chain')); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        $chainlist=$query->result(); 
			foreach($chainlist as $chain)
			{
			$hotel_chain_options[$chain->id]=$chain->entity_title;
			}
		}
	return $hotel_chain_options;
}

function getPositions()
{
	$CI =& get_instance();	
	$CI->db->select('id,entity_title'); 
		$CI->db->order_by('entity_title','asc'); 
		$CI->db->where('entity_type',$CI->config->item('attribute_hotel_positions')); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
		
}

function getCompanies()
{
	$CI =& get_instance();	
	$CI->db->select('id,company_name'); 
		$CI->db->order_by('company_name','asc'); 
    $query = $CI->db->get('companies');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
		
}
function getRoleAccessLevelId($roleId)
{
	$CI =& get_instance();		
	$CI->db->select('access_level_id'); 
	$CI->db->where('id',$roleId); 
    $query = $CI->db->get('role'); 
	if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->access_level_id;
		}
	return false;       
	
}

function getDepartements()
{
	$CI =& get_instance();	
	$CI->db->select('id,entity_title'); 
		$CI->db->order_by('entity_title','asc'); 
		$CI->db->where('entity_type',$CI->config->item('attribute_deppartment')); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
		
}


function getRoles()
{
	$CI =& get_instance();	
	$CI->db->select('id,title'); 
		$CI->db->order_by('title','asc'); 
    $query = $CI->db->get('role');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
		
}

function getRoleTitle($id)
{
	$CI =& get_instance();	
	$CI->db->select('title'); 
	$CI->db->where('id',$id); 
    $query = $CI->db->get('role'); 
	if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->title;
		}
	return false;       
	
}

function getCancelationPeriods()
{
	$CI =& get_instance();	
	$CI->db->select('id,entity_title'); 
	$CI->db->order_by('entity_title','asc'); 
	$CI->db->where('entity_type',$CI->config->item('attribute_duration_cancellation')); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
		
}

function getPaymentOptions()
{
	$CI =& get_instance();	
	$CI->db->select('id,entity_title'); 
	$CI->db->order_by('entity_title','asc'); 
	$CI->db->where('entity_type',$CI->config->item('attribute_duration_payment')); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
		
}

function getRenovationTypes()
{
	$CI =& get_instance();	
	$CI->db->select('id,renovation_type'); 
	$CI->db->order_by('renovation_type','asc'); 
    $query = $CI->db->get('renovation_types');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
		
}





function get_assigned_property_types($prms=5)
{
	$html="";
	$CI =& get_instance();
	$parameters=explode(',',$prms);
	$hid=$parameters['0'];
	$CI->db->select('property_types.title,property_types.id');
	$CI->db->from('property_types');
	$CI->db->join('hotel_property_types', 'property_types.id = hotel_property_types.property_type_id');
	$CI->db->where('property_types.id=hotel_property_types.property_type_id');
	$CI->db->where('hotel_property_types.hotel_id',$hid);
	$query2 = $CI->db->get();
	$tot= $query2->num_rows();
	if($tot > 0)
	{
      $ptypes=$query2->result();
	$lst='<ul>';
	foreach($ptypes as $ptype)
	{
	$lst.='<li>'.$ptype->title.'</li>';	
	}
	$lst.='</ul>';
	$html ='<a href="#"onmouseover="tooltip.pop(this,\'#property_type'.$ptype->id.'\')"><img src="'.base_url('assets/themes/default/images/property-type-icon.png').'" alt="estaimte" ></a>';
	$html .='<div style="display:none;"><div id="property_type'.$ptype->id.'"><div class = "pty-pop-tool">'.$lst.'</div></div></div>';
	} 
  return $html;

}

function getPrimaryCompanies()
{
	$CI =& get_instance();	
	$CI->db->select('id,company_name'); 
	$CI->db->order_by('company_name','asc');
	$CI->db->where('is_primary_company','1');
	$CI->db->where('status','1');	
    $query = $CI->db->get('companies');
        if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
			return false;
		}
		
}


function getHotelRroomsPricingDetails($pricing_id)
	{
	$CI =& get_instance();	
	$CI->db->select('id,market_id,double_price,
	triple_price,quad_price,breakfast_price,half_board_price,
	all_incusive_adult_price,extra_adult_price,extra_child_price,extra_bed_price'); 
	$CI->db->where("pricing_id", $pricing_id);
	$query = $CI->db->get('hotel_rooms_pricing_details');
     if ($query->num_rows() > 0)
		{
        return $query->result(); 
		}
		else{
		return false;
		}	
	}
	
function getHotalRoomPricingComplimentary($pricing_id)
	{
	$CI =& get_instance();
	$CI->db->select('cmpl_service_id'); 
	$CI->db->where("pricing_id", $pricing_id);
	$query = $CI->db->get('hotel_rooms_pricing_complimentary');
     if ($query->num_rows() > 0)
		{
        $pricingComplimentaries= $query->result(); 
		$roomPricingComplimentary =array();
		foreach($pricingComplimentaries as $pricingComplimentary)
		{
			$roomPricingComplimentary[$pricingComplimentary->cmpl_service_id]=$pricingComplimentary->cmpl_service_id;
		}
		return $roomPricingComplimentary;
		}
		else{
		return false;
		}		
	}	
	

function convert_mysql_format_date($dateval)
{
	$repl  =array('/','\\',':');
	$replby = array('-');
	$rdateval=str_replace($repl, $replby, $dateval);
	if($rdateval!="")
	{
	$date = new DateTime($rdateval);
	return $date->format('Y-m-d H:i:s');
	}
	return false;
}

function format_date_tolocal($dateval)
{
	if(!empty($dateval))
	{
	$date = new DateTime($dateval);
	return $date->format('d-m-Y');
	}
	return '';
}

function getLocation_point($country_code = '',$city_id = '')
{
        // Get country id and city id based on country code
        $country_id = '';
        $CI =& get_instance();	
	$query = $CI->db->query("select * from country_cities where country_code = '".$country_code."'");
        if ($query->num_rows() > 0) {
        $row = $query->row();
        $country_id = $row->country_id;
    }
    $hotelLoctionOption =array();	
	$CI =& get_instance();	
	$CI->db->select('hotel_location_points.id,point_name','country_cities.id');
        $CI->db->join('country_cities', 'hotel_location_points.city_id = country_cities.id');
        $CI->db->where('hotel_location_points.country_id',$country_id);
        $CI->db->where('city_name',$city_id);
		$CI->db->order_by('point_name','asc'); 
        $query = $CI->db->get('hotel_location_points');
       if ($query->num_rows() > 0)
		{
                $hotelLoction=$query->result(); 
                    foreach($hotelLoction as $hotelLoctions)
                    {
                            if($hotelLoctions->point_name){
                                $hotelLoctionOption[$hotelLoctions->id]=$hotelLoctions->point_name;
                            }
                    }
		}
	return $hotelLoctionOption;
}

