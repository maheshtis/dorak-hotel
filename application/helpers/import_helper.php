<?php 
function removeExtraspace($str)
{
	$trimstr=trim($str);
	return $new_str = preg_replace('/\s+/',' ', $trimstr);
}

function getHotelStatusId($title)
{	
	$CI =& get_instance();	
	$CI->db->select('id');
	$CI->db->where('entity_title',$title); 
	$CI->db->where('entity_type',$CI->config->item('attribute_hotel_status')); 
    $query = $CI->db->get('entity_attributes');
        if ($query->num_rows() > 0)
		{
        $hotelStatus=$query->result(); 
		
		 $row = $query->row(); 
         return $row->id;
		}	
		return 'Enable';// disabled
			
}

function getCityCode($city_name)
{
	if(!$city_name=="")
	{
	$CI =& get_instance();	
	$CI->db->select('city_code'); 
	$CI->db->where('city_name',$city_name);
    $query = $CI->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
         $row=$query->row(); 
		 return $row->city_code;
		}
		else{
			return false;
		}
	
	}
	return false;	
}

 function getHotelDetails($hname,$hcity,$postcode)
    {
		$CI =& get_instance();	
        $CI->db->select('hotel_id');
        $CI->db->where('hotel_name', $hname);
		$CI->db->where('city', $hcity);
		$CI->db->where('post_code', $postcode);
        $query = $CI->db->get('hotels');        
        if ($query->num_rows() > 0)
		{
		return $query->row()->hotel_id; 
		}
		else{
			return false;
		}
    }
	
	function formatDateToMysql($dateval)
	{		
		if($dateval!='')
		{
		return date('Y-m-d H:i:s',strtotime($dateval));	
		}
	   return '';
	}
