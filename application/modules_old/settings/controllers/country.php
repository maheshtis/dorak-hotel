<?php 
/**
  * @author tis
  * @author M
  * functions :- 
	  cities()       method here is used to perform following fucntions:-  add city,update and delete.
	  district()     method used here performs add district,update and delete functions.
	  countryList()  method used here performs functions such as :-  add country,update and delete.
	  
  
  * @description this controller is implemented in settings module for various functions such as edit,delete and update in hotel settings.
  * @method removeExtraspace($str): helper method removes extra space from passed string parameter ; 
  */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Country extends AdminController {
	function __construct()
	{
		parent::__construct();
		$this->_init();
	}
private function _init()
	{
            $this->load->helper(array('form','html','form_field_values','acl'));
            $this->load->library('form_validation');	
            $this->output->set_meta('description','Dorak Hotel Settings');
            $this->output->set_meta('title','Dorak Hotel Settings');
            $this->output->set_title('Dorak Hotel Settings');
            $this->output->set_template('settings');
            $this->load->model('settings/setting_model');
            $this->load->model('settings/country_model');
            $this->tmpFileDir=$this->config->item('upload_temp_dir');
	}
public function index(){
       redirect('settings/country/countryList');
    }
/**
     * @method cities() method here is used to perform following fucntions:-  add city,update and delete.
	 * @param  string $action hold form action path
	 * @param  int $cid city id - primary key.
	 * @param  int $cuntryId country id - foreign key in city table.
	 * @todo this method  is used to perform add city,update and delete functions.
	 * @return json value is returned as output
     */
public function cities($cuntryId='',$action='list',$cid='',$name = '')
{       
	if($cuntryId!=""){
	switch ($action) {
    case 'del':
		if($cid!=""){
		 $did=(int)$cid;
                 $name = urldecode($name);
		 // check if  cities have districts
		if(!$this->setting_model->getCityDistricts($did) && !$this->setting_model->isAttributeTypeAssigned('hotels', $name, 'city', 'hotel_id')){
		if($this->setting_model->delCity($did)){
		  $sdata['message'] = 'Selected city deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }	
		}
		else{$sdata['message'] = sprintf('Selected city <span style = "color:red; font-weight: bold;">%s</span> is already associated with hotel/hotels. Please remove the association and try again.',$name);
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'notice'
						);				
			$this->session->set_userdata($flashdata);
			
		}		
		}
		redirect('settings/country/cities/'.$cuntryId);		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('cityname'));
		$city_code=removeExtraspace($this->input->post('citycode'));
		$dbId=$this->input->post('ctyid');
		$upData = array('city_name'=>$new_title,'city_code'=>$city_code);
		if($new_title!="" || $city_code!=""){
		if($this->setting_model->checkCityExists($new_title,$dbId,$cuntryId)){
		$return=array('error'=>true,'city_name'=>$new_title,'city_code'=>$city_code,'msg'=>'City Name Already Exists');
		echo  json_encode($return);
		exit();
		}
		if($city_code!="" && $this->setting_model->checkCityCodeExists($city_code,$dbId,$cuntryId)){	
			$return=array('error'=>true,'city_name'=>$new_title,'city_code'=>$city_code,'msg'=>'City Code Already Exists');
			echo  json_encode($return);
			exit();
		}
		if(!$this->setting_model->updateCity($dbId,$upData)){
				$return=array('error'=>false,'city_name'=>$new_title,'city_code'=>$city_code);
				echo  json_encode($return);
			}
		}		
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('city_name'));
		$city_code=removeExtraspace($this->input->post('city_code'));
		$country_code=$this->setting_model->getCountryCode($cuntryId);
		if($newname!=""){
		//check if already in database		
		$isCityExists=false;
		if($this->setting_model->checkCityExists($newname,'',$cuntryId)){
			$isCityExists=true;
			$sdata['message'] = "City name ". $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);			
		}	
		
		if($this->setting_model->checkCityCodeExists($city_code,'',$cuntryId)){
			$isCityExists=true;
			$sdata['message'] = "City code ". $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);	
			
		}	
	if(!$isCityExists){
		$insData=array('country_id'=>$cuntryId,'country_code'=>$country_code,'city_name'=>$newname,'city_code'=>$city_code);
		if($this->setting_model->addCity($insData)){
		  $sdata['message'] = "City ".$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = "City ".$newname.'  not ndded!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}	
		}
		redirect('settings/country/cities/'.$cuntryId);	
        break;
	default:  
	}
	$country_name=$this->setting_model->getCountryName($cuntryId);
        $data = array(	
			'country_name' => $country_name,		
            'addAction' => site_url('settings/country/cities/'.$cuntryId.'/add'), 
			'editAction' => site_url('settings/country/cities/'.$cuntryId.'/editajax'), 
			'delAction' => site_url('settings/country/cities/'.$cuntryId.'/del'),
			'addDistrictUrl' => site_url('settings/country/district/'.$cuntryId),
			'addlocationUrl' => site_url('settings/country/location_point/'.$cuntryId),
            'add_input_name'=>array('id' => 'city_name', 														
								'name' => 'city_name',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"min3",											
								'placeholder'=>'City Name',				
								'maxlength'     => '30',
								'size'          => '50',
								'value'=>$this->input->post('city_name')),
			'add_input_fld2'=>array('id' => 'city_code', 														
								'name' => 'city_code',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"min2",
								'placeholder'=>'City Code',				
								'maxlength'     => '8',
								'size'          => '10',
								'value'=>$this->input->post('city_code')),			
																
					
			);			
		$data['listData']=$this->setting_model->getCitiesList($cuntryId);		
        $this->load->view('country_cities_setting', $data);
	}
	else{
		redirect('settings/country');	
	}
	
}
/**
     * @method district() method used here performs add district,update and delete functions.
	 * @param  string $action hold form action path
	 * @param  int $cityId city id - associated to district database table.
	 * @param  int $cuntryId country id - district related info updated through country id.
	 * @param  int $did district id - primary key.
	 * @todo this method is used to perform functions such as add city,update and delete.
	 * @return json value is returned as output.
     */

    public function district($cuntryId='',$cityId='',$action='list',$did='',$name = ''){
	if($cuntryId!="" && $cityId!=""){
	$city_name=$this->setting_model->getCityName($cityId);
	switch ($action) {
    case 'del':
		if($did!=""){
		 $cdid=(int)$did;
                 $name = urldecode($name);
                 if (!$this->setting_model->isAttributeTypeAssigned('hotels', $name, 'district', 'hotel_id')) {//if not assigned then delete
                    if($this->setting_model->delDistrict($cdid)){
                            $sdata['message'] = sprintf($this->config->item('DELETE_FORMAT'),$this->config->item('country'));
                            $flashdata = array('flashdata'  => $sdata['message'],'message_type'     => 'sucess');				
                            $this->session->set_userdata($flashdata);
                    }
                 } else {
                            $sdata['message'] = sprintf('Selected district <span style = "color:red; font-weight: bold;">%s</span> is already associated with hotel/hotels. Please remove the association and try again.',$name);
                            $flashdata = array('flashdata' => $sdata['message'],'message_type' => 'notice');
                            $this->session->set_userdata($flashdata);
                    }
		}
		redirect('settings/country/district/'.$cuntryId.'/'.$cityId);		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('district_name'));
		$dbId=$this->input->post('district_id');
		$upData = array('district_name'=>$new_title);
		if($new_title!=""){
		if($this->setting_model->checkDistrictExists($new_title,$cuntryId,$cityId,$dbId)){
		$return=array('error'=>true,'district_name'=>$new_title,'msg'=>'District Name Already Exists');
		echo  json_encode($return);
		exit();
		}
		if(!$this->setting_model->updateDistrict($dbId,$upData)){
				$return=array('error'=>false,'district_name'=>$new_title);
				echo  json_encode($return);      
			}
		
		}		
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('district_name'));
//                echo $newname;die;
		if($newname!=""){
		//check if already in database
		$isDistrictExists=false;
		if($this->setting_model->checkDistrictExists($newname,$cuntryId,$cityId,'')){
			$isDistrictExists=true;
			$sdata['message'] = "District name ". $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);	
		}	
	if(!$isDistrictExists){
		$insData=array('country_id'=>$cuntryId,'city_id'=>$cityId,'city_name'=>$city_name,'district_name'=>$newname);
		if($this->setting_model->addDistrict($insData)){
		  $sdata['message'] = "District ".$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = "District ".$newname.'  not ndded!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}	
		}
		redirect('settings/country/district/'.$cuntryId.'/'.$cityId);		
        break;
	default:  
	}
        $data = array(	
			'city_name' => $city_name,	
			'country_id' =>$cuntryId,			
                        'addAction' => site_url('settings/country/district/'.$cuntryId.'/'.$cityId.'/add'), 
			'editAction' => site_url('settings/country/district/'.$cuntryId.'/'.$cityId.'/editajax'), 
			'delAction' => site_url('settings/country/district/'.$cuntryId.'/'.$cityId.'/del'),
			'add_input_name'=>array('id' => 'district_name', 														
                                                'name' => 'district_name',
                                                'class'=>'required',
                                                'data-validation'=>'required,custom,length',
                                                'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
                                                'data-validation-length'=>"min3",											
                                                'placeholder'=>'District Name',				
                                                'maxlength'     => '50',
                                                'size'          => '50',
                                                'value'=>$this->input->post('district_name')),
				);
		$data['listData']=$this->setting_model->getDistrictList($cuntryId,$cityId);		
                $this->load->view('city_district_setting', $data);
	}
	else{
		redirect('settings/country');	
	}
}
/**
     * @method countryList() method used here performs functions such as :-  add country,update and delete.
	 * @param  string $action hold form action path
	 * @param  int $did district id - contains district details for fetching values in CoutryList().
	 * @todo this method is used to perform fucntions such as add country,update and delete.
	 * @return json value is returned as output
     */
public function countryList($action='list',$did='',$code = '',$name = ''){
	switch ($action) {
    case 'del':
		if($did!=""){
		  $did=(int)$did;
                  $code = urldecode($code);
                  $name = urldecode($name);
		  #check if country having  cities , if not then delete
	          if(!$this->setting_model->getCitiesList($did) && !$this->setting_model->isAttributeTypeAssigned('hotels', $code, 'country_code', 'hotel_id')){
		  if($this->setting_model->delCountry($did)){
		  $sdata['message'] = 'Selected country deleted!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }	
		}
		else{
			 $sdata['message'] = sprintf('Selected country <span style = "color:red; font-weight: bold;">%s</span> is already associated with hotels/cities. Please remove the association and try again.',$name);
			 $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'notice'
						);				
			$this->session->set_userdata($flashdata);
		}		
		}
		redirect('settings/country');		
        break;
    case 'editajax':
		$new_title=removeExtraspace($this->input->post('newname'));
		$country_code=removeExtraspace($this->input->post('code'));
		$dbId=$this->input->post('id');
		$upData = array('country_name'=>$new_title,'country_code'=>$country_code);
		if($new_title!="" && $country_code!=""){
		if(!$this->setting_model->checkCountryExists($dbId,$new_title)){
			if(!$this->setting_model->checkCountryCodeExists($dbId,$country_code)){	
				if(!$this->setting_model->updateCountry($dbId,$upData)){
					$return=array('error'=>false,'country'=>$new_title,'country_code'=>$country_code);
					echo  json_encode($return);
				}
			}
			else
			{
				$return=array('error'=>true,'country'=>$new_title,'country_code'=>$country_code,'msg'=>'Country Code Already Exists');
				echo  json_encode($return);
			}
		}
		else
		{
			$return=array('error'=>true,'country'=>$new_title,'country_code'=>$country_code,'msg'=>'Country Name Already Exists');
		   echo  json_encode($return);
		}
		}
		exit();
        break;
    case 'add':    
		$newname=removeExtraspace($this->input->post('country_name'));
		$cuntry_code=removeExtraspace($this->input->post('country_code'));
		if($newname!=""){
		//check if already in database
		$isexists=false;
		if($this->setting_model->checkCountryExists('',$newname)){
			$isexists=true;
			$sdata['message'] = "Country name ". $newname.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);	
		}	
		if($this->setting_model->checkCountryCodeExists('',$cuntry_code)){
			$isexists=true;
			$sdata['message'] = "Country Code ". $cuntry_code.' already exists';
			$flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);	
		}	
		if(!$isexists){
		$insData=array('country_name'=>$newname,'country_code'=>$cuntry_code);
		if($this->setting_model->addCountry($insData)){
		  $sdata['message'] = "Country ".$newname.' added';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'sucess'
						);				
			$this->session->set_userdata($flashdata);
		 }
		else{
		$sdata['message'] = "Country ".$newname.'  not ndded!';
		  $flashdata = array(
						'flashdata'  => $sdata['message'],
						'message_type'     => 'error'
						);				
			$this->session->set_userdata($flashdata);
			}		 
		}
		}
		redirect('settings/country');
        break;
	default:  
	}
        $data = array(		
            'addAction' => site_url('settings/country/countryList/add'), 
			'editAction' => site_url('settings/country/countryList/editajax'), 
			'delAction' => site_url('settings/country/countryList/del'), 
			'addCitiesUrl' => site_url('settings/country/cities'),			
            'add_input_name'=>array('id' => 'country_name', 														
								'name' => 'country_name',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"min2",											
								'placeholder'=>'Country Name',				
								'maxlength'     => '30',
								'size'          => '50',
								'value'=>$this->input->post('country_name')),
			'add_input_fld2'=>array('id' => 'country_code', 														
								'name' => 'country_code',
								'class'=>'required',
								'data-validation'=>'required,custom,length',
								'data-validation-regexp'=>"^[a-zA-Z\-\s]*$",											
								'data-validation-length'=>"min2",
								'placeholder'=>'Country Code',				
								'maxlength'     => '8',
								'size'          => '10',
								'value'=>$this->input->post('country_code')),			
																
					
			);			
		$data['listData']=$this->country_model->getCountriesList();		
        $this->load->view('country_setting', $data);
    }

	
/**
  *@author Jeevan singh 
  *@todo  to control all Hotel location points related functionality.
  *@return string(json) is returned value.
  */
    public function location_point($cuntryId = '', $cityId = '', $action = 'list', $did = '') {
        if ($cuntryId != "" && $cityId != "") {
            $city_name = $this->country_model->getCityName($cityId);
            switch ($action) {
                case 'del':
                    if ($did != "") {
                        $cdid = (int) $did;
                        //echo $cdid;die;
                        if ($this->setting_model->delLoction_point($cdid)) {
                            $sdata['message'] = 'Selected Location deleted!';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'sucess'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                    }
                    redirect('settings/country/location_point/' . $cuntryId . '/' . $cityId);
                    break;
                case 'editajax':
                    $new_title = removeExtraspace($this->input->post('point_name'));
                    $dbId = $this->input->post('point_id');
                    //echo $dbId;die;
                    $upData = array('point_name' => $new_title);
                    // dump($upData);die;
                    if ($new_title != "") {
                        if ($this->setting_model->checkLoctionExists($new_title, $cuntryId, $cityId, $dbId)) {
                            $return = array('error' => true, 'point_name' => $new_title, 'msg' => 'Location Point Already Exists');
                            echo json_encode($return);
                            exit();
                        }
                        if (!$this->setting_model->updateLoction($dbId, $upData)) {
                            $return = array('error' => false, 'point_name' => $new_title);
                            echo json_encode($return);
                        }
                    }
                    exit();
                    break;
                case 'add':
                    $newname = removeExtraspace($this->input->post('point_name'));
                    if ($newname != "") {
                        //check if already in database
                        $is_loction_exists = false;
                        if ($this->setting_model->checkLoctionExists($newname, $cuntryId, $cityId, '')) {
                            $is_loction_exists = true;
                            $sdata['message'] = "Location name " . $newname . ' already exists';
                            $flashdata = array(
                                'flashdata' => $sdata['message'],
                                'message_type' => 'error'
                            );
                            $this->session->set_userdata($flashdata);
                        }
                        if (!$is_loction_exists) {
                            $insData = array('country_id' => $cuntryId, 'city_id' => $cityId, 'point_name' => $newname);
                    
						  if ($this->setting_model->addLoctionPoint($insData)) {
                                $sdata['message'] = "Loction Point " . $newname . ' added';
                                $flashdata = array(
                                    'flashdata' => $sdata['message'],
                                    'message_type' => 'sucess'
                                );
                                $this->session->set_userdata($flashdata);
                            } else {
                                $sdata['message'] = "Location Point " . $newname . '  not added!';
                                $flashdata = array(
                                    'flashdata' => $sdata['message'],
                                    'message_type' => 'error'
                                );
                                $this->session->set_userdata($flashdata);
                            }
                        }
                    }
                    redirect('settings/country/location_point/' . $cuntryId . '/' . $cityId);
                    break;
                default:
            }
            $data = array(
                'city_name' => $city_name,
                'country_id' => $cuntryId,
                'addAction' => site_url('settings/country/location_point/' . $cuntryId . '/' . $cityId . '/add'),
                'editAction' => site_url('settings/country/location_point/' . $cuntryId . '/' . $cityId . '/editajax'),
                'delAction' => site_url('settings/country/location_point/' . $cuntryId . '/' . $cityId . '/del'),
                'add_input_name' => array('id' => 'point_name',
                    'name' => 'point_name',
                    'class' => 'required',
                    'data-validation' => 'required,custom,length',
                    'data-validation-regexp' => "^[a-zA-Z\-\s]*$",
                    'data-validation-length' => "min3",
                    'placeholder' => 'Location Name',
                    'maxlength' => '50',
                    'size' => '50',
                    'value' => $this->input->post('point_name')),
            );
            $data['listData'] = $this->country_model->get_location_point($cuntryId, $cityId);
            $this->load->view('hotel_location_points', $data);
        } else {
            redirect('settings/country');
        }
    }
##  Loction Point section end##
}