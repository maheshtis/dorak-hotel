<?php
/*
 * @data 15-mar-2016
 * @author tis
 * @description This model  is for  add/ edit delete hotels setting related stuff.
 */

class Setting_model extends AdminModel {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
     /**
     * @method getHotelChain() method get the chain
     * @param varchar $title The chain title
     * @param int $chain_id The chain id
	 * @todo this method used to get hotel chain name from database 
     * @return id|false
     */
    function getHotelChain($title, $chain_id = '') {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where('entity_type', $this->config->item('attribute_hotel_chain'));
        if ($chain_id != "")
            $this->db->where("id !=", $chain_id);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }

    /**
     * @method addHotelChain() method insert chain
	 * @param int $chain_id The chain id
	 * @todo this method used to add hotel chain name into database 
     * @return id|false
     */
    function addHotelChain($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

   /**
     * @method getHotelChainList() method list the chain
	 * @todo this method used to to return hotel chain listing from database 
     * @return chain list form of an array
     */
    function getHotelChainList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where('entity_type', $this->config->item('attribute_hotel_chain'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

     /**
     * @method delHotelChain() method delete the chain
     * @param int $chainId The chain id
	 * @todo this method used to to delete hotel chain from database 
     * @return true
     */
    function delHotelChain($chainId) {
        $this->db->where('id', $chainId);
        $this->db->where('entity_type', $this->config->item('attribute_hotel_chain'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * @method updateHotelChain() method update the chain
     * @param int $chainId The chain id
     * @param varchar $data The chain record
	 * @todo this method used to to update hotel chain in database 
     * @return true
     */
    function updateHotelChain($chainId, $data) {
        $this->db->where('id', $chainId);
        $this->db->where('entity_type', $this->config->item('attribute_hotel_chain'));
        $this->db->update('entity_attributes', $data);
    }
    /**
     * @method addHotelPurpose() method insert purpose
     * @param varchar $data The purpose title
	 * @todo this method used to add hotel purpose name into entity_attributes table 
     * @return id|false
     */
    function addHotelPurpose($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getHotelPurpose() method get the purpose
     * @param varchar $title The purpose title
     * @param int $eid The purpose id
	 * @todo this method used to get hotel purpose name from entity_attributes table 
     * @return id|false
     */
    function getHotelPurpose($title, $eid = '') {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_hotel_purpose'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getHotelPurposeList() method list the purpose
	 * @todo this method used to get hotel purpose list name from entity_attributes table 
     * @return purpose list in the form of an array OR false
     */
    function getHotelPurposeList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where('entity_type', $this->config->item('attribute_hotel_purpose'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method delHotelPurpose() method delete the purpose
     * @param int $chainId The purpose id
	 * @todo this method used to delete hotel purpose name from entity_attributes table 
     * @return true
     */
    function delHotelPurpose($chainId) {
        $this->db->where('id', $chainId);
        $this->db->where('entity_type', $this->config->item('attribute_hotel_purpose'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * @method updateHotelPurpose() method update the chain
     * @param int $dbId The purpose id
     * @param varchar $newdata The purpose record
	 * @todo this method used to update hotel purpose name into entity_attributes table 
     * @return true
     */
    function updateHotelPurpose($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where('entity_type', $this->config->item('attribute_hotel_purpose'));
        $this->db->update('entity_attributes', $newdata);
    }

/**
     * @method addHotelPropertyType() method insert hotel property type
	 * @param array() $data The chain id
	 * @todo this method used to add hotel property name into entity_attributes table 
     * @return id|false
     */

    function addHotelPropertyType($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    
     /**
     * Common function for check already assigned attributes
     * @param string $table 
     * @param int $checkid
     * @param string $field
     * @param string $selected_feild
	 * @todo this method used to check entity already assign any hotel or not in database table 
     * @return true|false
     */
    function isAttributeTypeAssigned($table = '',$check_id = null, $field = '',$select_feild = '') {
        if(!empty($select_feild) )
        $this->db->select($select_feild);
        
        if(!empty($field) && !empty($check_id))
        $this->db->where($field, $check_id);
        
        if(!empty($table))
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    /*
     * @method getHotelPropertyType() method get the property list
     * @param varchar $title The property title.
     * @param int $eid The property id.
	 * @todo this method used to get hotel property name from entity_attributes table 
     * @return id|false
     */
    function getHotelPropertyType($title, $eid = '') {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_property_types'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getHotelPropertyTypeList() method list the property
	 * @todo this method used to to return hotel property list  from entity_attributes table 
     * @return property list form in of an array OR false
     */
    function getHotelPropertyTypeList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_property_types'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * @method delHotelPropertyType() method delete the property
     * @param int $dId The property id
	 * @todo this method used to delete hotel property list  from entity_attributes table 
     * @return true|false
     */
    function delHotelPropertyType($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_property_types'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * @updateHotelPropertyType() method update the property
     * @param int $dbId The property id
     * @param varchar $newdata The property record
	 * @todo this method used to update hotel property from entity_attributes table
     * @return true|false
     */
    function updateHotelPropertyType($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_property_types'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * @method addHotelFacility() method insert facility
     * @param array() $data hold facility title in the form of an array
	 * @todo this method used to update hotel property from entity_attributes table
     * @return id|false
     */
    function addHotelFacility($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * @method getHotelFacility() method used to get  the hotel facility title from entity_attributes table
     * @param array() $title hold The facility title in the form of an array
     * @param int $eid The facility id
	 * @todo this method used to get hotel facility from entity_attributes table
     * @return id|false
     */
    function getHotelFacility($title, $eid = '') {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_facilities'));
        if ($eid != "")
            $this->db->where("id !=", $eid);
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }

    /**
     * @method getHotelFacilityList() method used to get hotel facility from entity_attributes
	 * @todo this method used to get hotel facility from entity_attributes table
     * @return array(), return hotel facility in the form of an array().
     */
    function getHotelFacilityList() {
        $this->db->select('id,entity_title');
        $this->db->order_by('entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_facilities'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * delHotelFacility() method delete the facility
     * @param int $dId The facility id
	 * @todo this method used to delet hotel facility from entity_attributes table
     * @return true|false
     */
    function delHotelFacility($dId) {
        $this->db->where('id', $dId);
        $this->db->where("entity_type", $this->config->item('attribute_facilities'));
        return $this->db->delete('entity_attributes');
    }

    /**
     * @method updateHotelFacility() used to  update hotel facility inside entity_attributes table
     * @param int $dbId The facility id
     * @param varchar $newdata The facility record
	 * @todo this method used used to  update hotel facility inside entity_attributes table
     * @return true
     */
    function updateHotelFacility($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->where("entity_type", $this->config->item('attribute_facilities'));
        $this->db->update('entity_attributes', $newdata);
    }

    /**
     * addRoomFacility() method insert room facility
     * @param array() $data hold facility data in the form of an array
	 * @todo this method used used to add room facility in to entity_attributes table
     * @return id|false
     */

        function addRoomFacility($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }
        /**
         * @method getRoomFacility() used to get the room facility
         * @param array()  $title hold The room facility title in the form of an array
         * @param int $eid The room facility id
         * @return id|false
         */
        function getRoomFacility($title, $eid = '') {
            $this->db->select('id');
            $this->db->where("entity_title", $title);
            $this->db->where("entity_type", $this->config->item('attribute_complementy_service'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * getRoomFacilityList() method list the room facility
		 * @todo this method used used to  list hotel room facility from entity_attributes table
         * @return room facility in the form of an array OR false
         */
        function getRoomFacilityList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where("entity_type", $this->config->item('attribute_complementy_service'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * delRoomFacility() method delete the room facility
         * @param int $dId The room facility id
		 * @todo this method used to  delete hotel room facility from entity_attributes table
         * @return true|false
         */
        function delRoomFacility($dId) {
            $this->db->where('id', $dId);
            $this->db->where("entity_type", $this->config->item('attribute_complementy_service'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * @method updateRoomFacility() method update the room facility
         * @param int $dbId The room facility id
         * @param array() $newdata hold room facility title in the form of an array
		 * @todo this method used used to  update hotel room facility inside entity_attributes table
         * @return true|false
         */
        function updateRoomFacility($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->where("entity_type", $this->config->item('attribute_complementy_service'));
            $this->db->update('entity_attributes', $newdata);
        }
	/**
     * @method addCurrency() method insert currency
	 * @param array() $data hold the currency in the form of an array
	 * @todo this method used to add currency name into currencies table 
     * @return id|false
     */
        function addCurrency($data) {
            if ($this->db->insert('currencies', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }
	/**
     * @method getCurrency() method get the currency
     * @param varchar $code hold The currency code
     * @param int $usval hold The currency value
     * @param int $eid hold The currency id
	 * @todo this method used to get currency name from currencies table
     * @return id|false
     */
        function getCurrency($code, $usval = '', $eid = '') {
            $this->db->select('id');
            $this->db->where("code", $code);
            if ($usval != '')
                $this->db->where("currency_usd_value", $usval);

            if ($eid != "")
                $this->db->where("id !=", $eid);

            $query = $this->db->get('currencies');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }
	/**
     * @method getCurrencyList() used to list the currency code, id and currency value
	 * @todo this method used to return list the currency code, id and currency value from currencies table
     * @return currency list in  form of an array OR false
     */
        function getCurrencyList() {
            $this->db->select('id,code,currency_usd_value');
            $this->db->order_by('code');
            $query = $this->db->get('currencies');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }
	/**
     * @method delCurrency() method delete the currency
     * @param int $dId hold the currency code
	 * @todo this method used to delete currency  from currencies table 
     * @return true|false
     */
        function delCurrency($dId) {
            $this->db->where('code', $dId);
            return $this->db->delete('currencies');
        }
	/**
     * @method deleteCurencyCodesData() method delete the currencies using currency code
     * @param array $delCodesArray hold the currency code in the form of an array().
	 * @todo this method used to delete currencies records  from currencies table 
     * @return true|false
     */
        function deleteCurencyCodesData($delCodesArray) {
            $this->db->where_in('code', $delCodesArray);
            return $this->db->delete('currencies');
        }
	/**
     * @method updateCurrency() method update the currency 
     * @param int $dbId The currency id
     * @param array() $newdata hold currency record
	 * @todo this method used to update currency inside currencies table 
     */
        function updateCurrency($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->update('currencies', $newdata);
        }
	/**
     * @method importCurencyData() method used to import currencies
	 * @param array $data hold the currency record in the form of an array
	 * @todo this method used to import currencies into currencies table
     * @return true|false
     */
        function importCurencyData($data) {
            // clear old data
            $this->db->truncate('currencies');
            // import new data
            return $this->db->insert_batch('currencies', $data);
        }
	/**
     * @method addCurencyData() method insert currencies
	 * @param array $data hold the currency record in the form of an array
	 * @todo this method used to add currency into currencies table
     * @return true|false
     */
        function addCurencyData($data) {
            return $this->db->insert_batch('currencies', $data);
        }
	/**
     * @method addCountry() method insert country
	 * @param array $data hold the country record in the form of an array
	 * @todo this method used to add country  into countries table
     * @return true|false
     */
        function addCountry($data) {
            if ($this->db->insert('countries', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }
	/**
     * @method getCountry() method get country id 
     * @param varchar $cname The country title
	 * @todo this method used to get country name from country  table
     * @return id|false
     */
        function getCountry($cname) {
            $this->db->select('id');
            $this->db->where("country_name", $cname);
            $query = $this->db->get('countries');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }
	/**
     * @method checkCountryExists() method check the country exist or not 
     * @param int $Id The country id
     * @param varchar $name The country name
	 * @todo this method used to check country exit or not using country id and name in country  table
     * @return id|false
     */
        function checkCountryExists($Id = '', $name) {
            $this->db->select('id');
            if ($Id != '')
                $this->db->where("id !=", $Id);
            $this->db->where("country_name", $name);
            $query = $this->db->get('countries');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }
	/**
     * @method checkCountryCodeExists() method check the country code exist or not 
     * @param int $Id The country id
     * @param varchar $ccode The country code
	 * @todo this method used to check country exit or not using country code and id inside country  table
     * @return id|false
     */
        function checkCountryCodeExists($Id = '', $ccode) {
            $this->db->select('id');
            if ($Id != '')
                $this->db->where("id !=", $Id);
            $this->db->where("country_code", $ccode);
            $query = $this->db->get('countries');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }
	/**
     * @method getCountryCode() method get the country code
     * @param int $cid hold the country  id
	 * @todo this method used to get country code name from database 
     * @return country_code|false
     */
        function getCountryCode($cid) {
            $this->db->select('country_code');
            $this->db->where("id", $cid);
            $query = $this->db->get('countries');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->country_code;
            } else {
                return false;
            }
            return false;
        }
/**
     * @method getCountryName() method get the country name
     * @param int $cid hold the country  id
	 * @todo this method used to get country  name from countries table 
     * @return country name|false
     */
        function getCountryName($cid) {
            $this->db->select('country_name');
            $this->db->where("id", $cid);
            $query = $this->db->get('countries');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->country_name;
            } else {
                return false;
            }
            return false;
        }
/**
     * @method getCountriesList() method get the country listing
	 * @todo this method used to get country list name from countries table 
     * @return array() country name in the form of an array OR false
     */
        function getCountriesList() {
            $this->db->select('id,country_code as code,country_name');
            $this->db->order_by('country_name', 'ASC');
            $query = $this->db->get('countries');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }
/**
     * delCountry() method delete the countries form countries table
     * @param int $dId The country  id
	 * @todo this method used to delete countries from countries table 
     * @return true|false
     */
        function delCountry($dId) {
            $this->delCities($dId); // delete cities of the selected cuntry
            $this->db->where('id', $dId);
            return $this->db->delete('countries');
        }
 /**
     * updateCountry() method update the countries
     * @param int $dbId hold country id 
     * @param array $newdata hold the country record in the form of an array()
	 * @todo this method used to update country record in countries table 
     */
        function updateCountry($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->update('countries', $newdata);
        }
/**
     * @method addCity() method insert country city
	 * @param array  $data hold country city in the form of an array.
	 * @todo this method used to add country city name into country_cities table 
     * @return id|false
     */
        function addCity($data) {
            if ($this->db->insert('country_cities', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }
/**
     * @method getCity() method get the city name 
     * @param varchar $cname The city name
	 * @todo this method used to get city name from country_cities table 
     * @return id|false
     */
        function getCity($cname) {
            $this->db->select('id');
            $this->db->where("city_name", $cname);
            $query = $this->db->get('country_cities');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }
/**
     * @method checkCityExists() method check city exist or not 
     * @param varchar $name The city name
     * @param int $cid The city id
	 * @todo this method used to check city exist or not in country_cities table 
     * @return id|false
     */
        function checkCityExists($name, $cid = '', $cuntryId = '') {
            $this->db->select('id');
            $this->db->where("city_name", $name);
            if ($cid != '')
                $this->db->where("id !=", $cid);
            if ($cuntryId != '')
                $this->db->where("country_id", $cuntryId);
            $query = $this->db->get('country_cities');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }
/**
     * @method checkCityCodeExists() method check city exist or not 
     * @param varchar $ccode The city code
     * @param int $cid The city id
     * @param int $cuntryId The country id
	 * @todo this method used to check city exist or not using city code,city id and country id in country_cities table 
     * @return id|false
     */
        function checkCityCodeExists($ccode, $cid = '', $cuntryId = '') {
            $this->db->select('id');
            if ($cid != '')
                $this->db->where("id !=", $cid);

            if ($cuntryId != '')
                $this->db->where("country_id", $cuntryId);

            $this->db->where("city_code", $ccode);
            $query = $this->db->get('country_cities');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }
	/**
     * @method getCityByCode() method return  city id
     * @param varchar $ccode The city code
	 * @todo this method used to return city id using city code in country_cities table 
     * @return id|false
     */
        function getCityByCode($ccode) {
            $this->db->select('id');
            $this->db->where("city_code", $ccode);
            $query = $this->db->get('country_cities');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }
/**
     * @method getCitiesList() method used to get id,city name, and city code
     * @param id $cuntryId The country id
	 * @todo this method used to return id,city name, and city code using country id in country_cities table 
     * @return cities in the form of an array OR false
     */
        function getCitiesList($cuntryId) {
            $this->db->select('id,city_name,city_code');
            $this->db->order_by('city_name', 'ASC');
            $this->db->where("country_id", $cuntryId);
            $query = $this->db->get('country_cities');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }
	/**
     * delCity() method delete district 
     * @param int $dId The district id
	 * @todo this method used to delete district from country_cities table 
     * @return true|false
     */
        function delCity($dId) {
            $this->delCityDistricts($dId);
            $this->db->where('id', $dId);
            return $this->db->delete('country_cities');
        }
	/**
     * delCities() method delete country city 
     * @param int $cuntryId country id
	 * @todo this method used to delete city from country_cities table using country id
     * @return true|false
     */
        function delCities($cuntryId) {//  by passing country id
            $this->delCountryCityDistricts($cuntryId);
            $this->db->where('country_id', $cuntryId);
            return $this->db->delete('country_cities');
        }
/**
     * updateCity() method update country city
     * @param int $dbId the city id
     * @param array $newdata hold the city name in the form of an array.
	 * @todo this method used to update country city form country_cities table using city id
     */
        function updateCity($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->update('country_cities', $newdata);
        }
/**
     * @method getCityName() method get the city name
     * @param int $cId The city id
	 * @todo this method used to get city name from country_cities table 
     * @return city name|false
     */
        function getCityName($cId) {
            $this->db->select('city_name');
            $this->db->where("id", $cId);
            $query = $this->db->get('country_cities');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->city_name;
            } else {
                return false;
            }
            return false;
        }

/**
     * @method addDistrict() method insert district
	 * @param array $data hold the districts record in the form of an array()
	 * @todo this method used to add districts into table  
     * @return id|false
     */
        function addDistrict($data) {
            if ($this->db->insert('city_districts', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }
	/**
     * @method getDistrict() method get the district name
     * @param int $cid The district id
     * @param varchar $dname The district name
	 * @todo this method used to get district name from city_districts table 
     * @return id|false
     */
        function getDistrict($dname, $cid = '') {
            $this->db->select('id');
            $this->db->where("district_name", $dname);
            if ($cid != '')
                $this->db->where("city_id", $cid);
            $query = $this->db->get('city_districts');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }
/**
     * @method checkDistrictExists() method used to check district exist or not
     * @param int $cuntryId The country id
     * @param int $cid The city id
     * @param int $did The district id
     * @param varchar $name The district name
	 * @todo this method used to get district id from city_districts table 
     * @return id|false
     */
        function checkDistrictExists($name, $cuntryId = '', $cid = '', $did = '') {
            $this->db->select('id');
            $this->db->where("district_name", $name);
            if ($cid != '')
                $this->db->where("city_id", $cid);
            if ($did != '')
                $this->db->where("id !=", $did);
            if ($cuntryId != '')
                $this->db->where("country_id", $cuntryId);
            $query = $this->db->get('city_districts');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }
/**
     * @method getCityDistricts() method select district name and id
	 * @todo this method used for district list from city_districts table
     * @return district list in the form of an array
     */
        function getCityDistricts($cityId) {
            $this->db->select('id,district_name');
            $this->db->where("city_id", $cityId);
            $this->db->order_by('district_name', 'ASC');
            $query = $this->db->get('city_districts');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }
/**
     * @method getDistrictList() method select district name
	 * @todo this method used for district list from city_districts table using country id and city id
     * @return district list in the form of an array
     */
        function getDistrictList($cuntryId, $cityId) {
            $this->db->select('id,district_name');
            $this->db->where("country_id", $cuntryId);
            $this->db->where("city_id", $cityId);
            $this->db->order_by('district_name', 'ASC');
            $query = $this->db->get('city_districts');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }
 /**
     * @method delDistrict() method delete the district
     * @param int $dId district id
	 * @todo this method used to  delete district chain from city_districts table
     * @return true|false
     */
        function delDistrict($dId) {
            $this->db->where('id', $dId);
            return $this->db->delete('city_districts');
        }
/**
     * @method delCityDistricts() method delete the city
     * @param int $city_id hold city id
	 * @todo this method used to  delete city from city_districts table
     * @return true|false
     */
        function delCityDistricts($city_id) {//  by passing country id
            $this->db->where('city_id', $city_id);
            return $this->db->delete('city_districts');
        }
/**
     * @method delCountryCityDistricts() method district using country code
     * @param int $country_id hold country id
	 * @todo this method used to  delete country from city_districts table
     * @return true|false
     */
        function delCountryCityDistricts($country_id) {
            $this->db->where('country_id', $country_id);
            return $this->db->delete('city_districts');
        }
/**
     * @method updateDistrict() method update the district name
     * @param int $dbId The district id
     * @param array() $newdata hold district name in the form of an array
	 * @todo this method used to to update district name inside city_districts table 
     */
        function updateDistrict($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->update('city_districts', $newdata);
        }

        /**
         * @method addMarket() method insert market
         * @param array() $data hold market title in the form of an array
		 * @todo this method used to add market name in entity_attributes table 
         * @return id|false
         */

        function addMarket($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * @method getMarket() method get the market
         * @param varchar $cname The market title
         * @param int $eid The market id
		 * @todo this method used to get market name from entity_attributes table 
         * @return id|false
         */
        function getMarket($cname, $eid = '') {
            $this->db->select('id');
            $this->db->where("entity_title", $cname);
            $this->db->where("entity_type", $this->config->item('attribute_market'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * getMarketList() method used to get market list
		  * @todo this method used to get market list from entity_attributes table 
         * @return markets in the form of an array OR false
         */
        function getMarketList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where("entity_type", $this->config->item('attribute_market'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * delMarket() method delete the market
         * @param int $dId The market id
		  * @todo this method used to delete market from entity_attributes table 
         * @return true|false
         */
        function delMarket($dId) {
            $this->db->where('id', $dId);
            $this->db->where("entity_type", $this->config->item('attribute_market'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * updateMarket() method update the market
         * @param int $dbId The market id
         * @param array() $newdata hold markets in the form of an array
		  * @todo this method used to update market inside entity_attributes table 
         */
        function updateMarket($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->where("entity_type", $this->config->item('attribute_market'));
            $this->db->update('entity_attributes', $newdata);
        }

        /**
         * addTravelType() method insert travel
         * @param array() $data hold travel title
		  * @todo this method used to add market into entity_attributes table 
         * @return id|false
         */
        function addTravelType($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * getTravelType() method get the travel
         * @param varchar $cname The travel title
         * @param int $eid The travel id
         * @return id
         */
        function getTravelType($cname, $eid = "") {
            $this->db->select('id');
            $this->db->where("entity_title", $cname);
            $this->db->where("entity_type", $this->config->item('attribute_travel_type'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * getTravelTypeList() method list the travel
		 * @todo this method used to get travel from entity_attributes table 
         * @return an array() which hold travel title OR return false.
         */
        function getTravelTypeList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where('entity_type', $this->config->item('attribute_travel_type'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * delTravelType() method delete the travel type
         * @param int $dId The travel type id
		  * @todo this method used to delete travel from entity_attributes table
         * @return true|false
         */
        function delTravelType($dId) {
            $this->db->where('id', $dId);
            $this->db->where('entity_type', $this->config->item('attribute_travel_type'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * updateTravelType() method update the travel
         * @param int $dbId The travel id
         * @param array() $newdata hold travel  record
		 * @todo this method used to update travel name inside entity_attributes table
         */
        function updateTravelType($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->where('entity_type', $this->config->item('attribute_travel_type'));
            $this->db->update('entity_attributes', $newdata);
        }
		
        /**
         * addRoomType() method insert room type
         * @param varchar $data The room type title
		  * @todo this method used to add room type title into entity_attributes table
         * @return id|false
         */

        function addRoomType($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * getRoomType() method get the room type
         * @param varchar $cname The room type title
         * @param int $eid The room type id
		 * @todo this method used to get room type title from entity_attributes table
         * @return id|false
         */
        function getRoomType($cname, $eid = "") {
            $this->db->select('id');
            $this->db->where("entity_title", $cname);
            $this->db->where("entity_type", $this->config->item('attribute_room_types'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * getRoomTypesList() method list the room type
		  * @todo this method used to get room type title list from entity_attributes table
         * @return room types title in the form of an array() OR return false.
         */
        function getRoomTypesList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where("entity_type", $this->config->item('attribute_room_types'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * delRoomType() method delete the room type
         * @param int $dId The room type id
		 * @todo this method used to delete room type from entity_attributes table
         * @return true|false
         */
        function delRoomType($dId) {
            $this->db->where('id', $dId);
            $this->db->where("entity_type", $this->config->item('attribute_room_types'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * @method updateRoomType() method update the room type
         * @param int $dbId The room type id
         * @param array() $newdata hold room type record in the form of an array().
		 * @todo this method used to update room type inside entity_attributes table
         */
        function updateRoomType($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->where("entity_type", $this->config->item('attribute_room_types'));
            $this->db->update('entity_attributes', $newdata);
        }

        /**
         * @method addMealPlan() method insert meal
         * @param array() $data meal title in the form of an array()
		 * @todo this method used to add meal plan into entity_attributes table
         * @return id|false
         */
        function addMealPlan($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * @method getMealPlan() method get the meal
         * @param varchar $cname The meal title
         * @param int $eid The meal id
		 * @todo this method used to get meal plan from entity_attributes table
         * @return id|false
         */
        function getMealPlan($cname, $eid = "") {
            $this->db->select('id');
            $this->db->where("entity_title", $cname);
            $this->db->where("entity_type", $this->config->item('attribute_meal_plan'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * getMealPlansList() method list the meal
		 * @todo this method used to add meal plan into entity_attributes table
         * @return meal plan list in the form of an array() OR false
         */
        function getMealPlansList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where("entity_type", $this->config->item('attribute_meal_plan'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * @method delMealPlan() method delete the meal
         * @param int $dId The meal id
		  * @todo this method used to delete meal plan from entity_attributes table
         * @return true|false
         */
        function delMealPlan($dId) {
            $this->db->where('id', $dId);
            $this->db->where("entity_type", $this->config->item('attribute_meal_plan'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * updateMealPlan() method update the meal plan
         * @param int $dbId The meal id
         * @param array $newdata hold meal plan in the form of an array()
		  * @todo this method used to update meal plan inside entity_attributes table

         */
        function updateMealPlan($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->where("entity_type", $this->config->item('attribute_meal_plan'));
            $this->db->update('entity_attributes', $newdata);
        }

        /**
         * @method addRole() method insert role
         * @param array() $data hold role title in the form of an array
		  * @todo this method used to add role into role table
         * @return id|false
         */
        function addRole($data) {
            if ($this->db->insert('role', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * @method getRole() method get the role
         * @param $cname[] The role title
         * @param int $eid The role id
		 * @todo this method used to get role into role table
         * @return id|false
         */
        function getRole($cname, $eid = "") {
            $this->db->select('id');
            $this->db->where("title", $cname);
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('role');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * getRolesList() method list the role
		 * @todo this method used to get role list from role table
         * @return roles in the form of an array OR return false
         */
        function getRolesList() {
            $this->db->select('id,title,access_level_id');
            $this->db->order_by('title');
            $query = $this->db->get('role');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * @method delRole() method delete the role
         * @param int $dId The role id
		 * @todo this method used to delete role from role table
         * @return true|false
         */
        function delRole($dId) {
            $this->db->where('id', $dId);
            return $this->db->delete('role');
        }

        /**
         * updateRole() method update the role
         * @param int $dbId The role id
         * @param array $newdata hold roles in the form of an array.
		 * @todo this method used to update role inside role table
         */
        function updateRole($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->update('role', $newdata);
        }

        /**
         * addPosition() method insert position
         * @param varchar $data The position title
         * @return id
         */
        function addPosition($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * getPosition() method get the position
         * @param array $cname[] holds The position title
         * @param int $eid The position id
		 * @todo this method used to get position from role entity_attributes
         * @return id|false
         */
        function getPosition($cname, $eid = "") {
            $this->db->select('id');
            $this->db->where("entity_title", $cname);
            $this->db->where("entity_type", $this->config->item('attribute_hotel_positions'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * @method getPositionsList() method list the postion
		  * @todo this method used to get positions from entity_attributes
         * @return positions in the form of an array OR false
         */
        function getPositionsList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where("entity_type", $this->config->item('attribute_hotel_positions'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * delPosition() method delete the position
         * @param int $dId The position id
		  * @todo this method used to delete position from entity_attributes
         * @return true
         */
        function delPosition($dId) {
            $this->db->where('id', $dId);
            $this->db->where("entity_type", $this->config->item('attribute_hotel_positions'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * updatePosition() method update the position
         * @param int $dbId The position id
         * @param array() $newdata hold position record in the form of an array
		   * @todo this method used to update position from entity_attributes
         */
        function updatePosition($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->where("entity_type", $this->config->item('attribute_hotel_positions'));
            $this->db->update('entity_attributes', $newdata);
        }

        /**
         * addStarRating() method insert star ratings
         * @param array() $data hold star ratings in the form of an array
		  * @todo this method used to add star rating into entity_attributes
         * @return id|false
         */
        function addStarRating($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * getStarRating() method get the star ratings
         * @param varchar $cname The star ratings title
         * @param int $eid The star ratings id
		  * @todo this method used to get star rating into entity_attributes
         * @return id|false
         */
        function getStarRating($cname, $eid = "") {
            $this->db->select('id');
            $this->db->where("entity_title", $cname);
            $this->db->where("entity_type", $this->config->item('attribute_star_ratings'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * getStarRatingList() method list the star ratings
         * @return star ratings
		 * @todo this method used to get star rating from entity_attributes
		 * @return star rating in the form of an array OR return false
         */
        function getStarRatingList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where("entity_type", $this->config->item('attribute_star_ratings'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * @method delStarRating() method delete the star ratings
         * @param int $dId The star rating id
		 * @todo this method used to del star rating from entity_attributes
         * @return true|false
         */
        function delStarRating($dId) {
            $this->db->where('entity_title', $dId);
            $this->db->where("entity_type", $this->config->item('attribute_star_ratings'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * updateStarRating() method update the star rating
         * @param int $dbId The star rating id
         * @param varchar $data The star rating record
		  * @todo this method used to update star rating inside entity_attributes
         */
        function updateStarRating($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->where("entity_type", $this->config->item('attribute_star_ratings'));
            $this->db->update('entity_attributes', $newdata);
        }

        /**
         * addDorakRating() method insert dorak rating
         * @param varchar $data The dorak rating title
		  * @todo this method used to add dorak rating into entity_attributes
         * @return id|false
         */
        function addDorakRating($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * getDorakRating() method get the dorak rating
         * @param varchar $cname The dorak rating title
         * @param int $eid The dorak rating id
		  * @todo this method used to get dorak rating from entity_attributes
         * @return id|false
         */
        function getDorakRating($cname, $eid = "") {
            $this->db->select('id');
            $this->db->where("entity_title", $cname);
            $this->db->where("entity_type", $this->config->item('attribute_dorak_ratings'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * getDorakRatingList() method list the dorak rating
		  * @todo this method used to get dorak rating from entity_attributes
         * @return dorak ratings in the form of an array OR return false
         */
        function getDorakRatingList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where("entity_type", $this->config->item('attribute_dorak_ratings'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * @method delDorakRating() method delete the dorak rating
         * @param int $dId The dorak rating id
		  * @todo this method used to del dorak rating from entity_attributes
         * @return true|false
         */
        function delDorakRating($dId) {
            $this->db->where('entity_title', $dId);
            $this->db->where("entity_type", $this->config->item('attribute_dorak_ratings'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * updateDorakRating() method update the dorak rating
         * @param int $dbId The dorak rating id
         * @param varchar $newdata The dorak rating record
		  * @todo this method used to update dorak rating from entity_attributes
         */
        function updateDorakRating($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->where("entity_type", $this->config->item('attribute_dorak_ratings'));
            $this->db->update('entity_attributes', $newdata);
        }

        /**
         * @method addStatus() method insert status
         * @param varchar $data The status title
		  * @todo this method used to add status into entity_attributes
         * @return id|false
         */
        function addStatus($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * getStatus() method get the status
         * @param varchar $cname The status title
         * @param int $eid The status id
		  * @todo this method used to get status from entity_attributes
         * @return id|false
         */
        function getStatus($cname, $eid = "") {
            $this->db->select('id');
            $this->db->where("entity_title", $cname);
            $this->db->where("entity_type", $this->config->item('attribute_hotel_status'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * @method getStatusList() method list the status
		  * @todo this method used to get status from entity_attributes
         * @return status list in the form of an array OR false
         */
        function getStatusList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where("entity_type", $this->config->item('attribute_hotel_status'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * delStatus() method delete the status
         * @param int $dId The status id
		  * @todo this method used to del status from entity_attributes
         * @return true|false
         */
        function delStatus($dId) {
            $this->db->where('id', $dId);
            $this->db->where("entity_type", $this->config->item('attribute_hotel_status'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * updateStatus() method update the status
         * @param int $dbId The status id
         * @param array() $newdata hold status record in the form of an array
         */
        function updateStatus($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->update('entity_attributes', $newdata);
        }

        /**
         * addDepartment() method insert department
         * @param array() $data hold department title  in the form of an array
		  * @todo this method used to add department into entity_attributes
         * @return id|false
         */
        function addDepartment($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * getDepartment() method get the department
         * @param varchar $cname The department title
         * @param int $eid The department id
		  * @todo this method used to get department from entity_attributes
         * @return id|false
         */
        function getDepartment($cname, $eid = "") {
            $this->db->select('id');
            $this->db->where("entity_title", $cname);
            $this->db->where("entity_type", $this->config->item('attribute_deppartment'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * @method getDepartmentsList() method list the department
		  * @todo this method used to get department from entity_attributes
         * @return departments in the form of an arry OR return false
         */
        function getDepartmentsList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where("entity_type", $this->config->item('attribute_deppartment'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * delDepartment() method delete the department
         * @param int $dId The department id
		  * @todo this method used to del department from entity_attributes
         * @return true|false
         */
        function delDepartment($dId) {
            $this->db->where('id', $dId);
            $this->db->where("entity_type", $this->config->item('attribute_deppartment'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * updateDepartment() method update the department
         * @param int $dbId The department id
         * @param varchar $newdata The department record
		  * @todo this method used to update department inside entity_attributes table
         */
        function updateDepartment($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->where("entity_type", $this->config->item('attribute_deppartment'));
            $this->db->update('entity_attributes', $newdata);
        }

        /**
         * addCancellationDuration() method insert cancellation
         * @param array() $data hold cancellation title 
		  * @todo this method used to add cancellation into entity_attributes table
         * @return id|false
         */
        function addCancellationDuration($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * getCancellationDuration() method get the cancellation
         * @param array() $cname hold cancellation title
         * @param int $eid The cancellation id
		  * @todo this method used to get cancellation from entity_attributes table
         * @return id|false
         */
        function getCancellationDuration($cname, $eid = "") {
            $this->db->select('id');
            $this->db->where("entity_title", $cname);
            $this->db->where("entity_type", $this->config->item('attribute_duration_cancellation'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * @method getCancellationDurationList() method list the cancellation
		  * @todo this method used to get cancellation list from entity_attributes table
         * @return cancellations record in the form of array OR return false
         */
        function getCancellationDurationList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where('entity_type', $this->config->item('attribute_duration_cancellation'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * delCancellationDuration() method delete the cancellation
         * @param int $dId The cancellation id
		  * @todo this method used to delete cancellation from entity_attributes table
         * @return true|false
         */
        function delCancellationDuration($dId) {
            $this->db->where('id', $dId);
            $this->db->where('entity_type', $this->config->item('attribute_duration_cancellation'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * updateCancellationDuration() method update the cancellation
         * @param int $dbId The cancellation id
         * @param array() $newdata hold cancellation record in the form of array
		 * @todo this method used to update cancellation record from entity_attributes table
         */
        function updateCancellationDuration($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->where('entity_type', $this->config->item('attribute_duration_cancellation'));
            $this->db->update('entity_attributes', $newdata);
        }

        /**
         * addPaymentDuration() method insert payment
         * @param array() $data hold payment title in the form of an array.
		  * @todo this method used to add payment into entity_attributes table
         * @return id|false
         */
        function addPaymentDuration($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * @method getPaymentDuration() method get the payment
         * @param array() $cname hold  payment duration title in the form of an array
         * @param int $eid The payment id
		  * @todo this method used to get payment from entity_attributes table
         * @return id|false
         */
        function getPaymentDuration($cname, $eid = "") {
            $this->db->select('id');
            $this->db->where("entity_title", $cname);
            $this->db->where("entity_type", $this->config->item('attribute_duration_payment'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * getPaymentDurationList() method list the payment options
		  * @todo this method used to get payment duration from entity_attributes table
         * @return payment options in the form of an array OR return false
         */
        function getPaymentDurationList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where("entity_type", $this->config->item('attribute_duration_payment'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * delPaymentDuration() method delete the paymtent options
         * @param int $dId The payment id
		  * @todo this method used to delete payment duration from entity_attributes table
         * @return true|false
         */
        function delPaymentDuration($dId) {
            $this->db->where('id', $dId);
            $this->db->where("entity_type", $this->config->item('attribute_duration_payment'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * @method updatePaymentDuration() method update the payment
         * @param int $dbId The payment id
         * @param array() $newdata hold payment record in the form of an array
		  * @todo this method used to update payment duration into entity_attributes table

         */
        function updatePaymentDuration($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->where("entity_type", $this->config->item('attribute_duration_payment'));
            $this->db->update('entity_attributes', $newdata);
        }

        /**
         * @method addAgeGroup() method insert age group options
         * @param array() $data hold  age title in the form of an array.
		  * @todo this method used to add age group into entity_attributes table
         * @return id|false
         */
        function addAgeGroup($data) {
            if ($this->db->insert('entity_attributes', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }

        /**
         * getAgeGroup() method get the age group options
         * @param array() $cname hold age group options title in the form of an array()
         * @param int $eid The age group option id
		  * @todo this method used to get age group from entity_attributes table
         * @return id|false
         */
        function getAgeGroup($cname, $eid = "") {
            $this->db->select('id');
            $this->db->where("entity_title", $cname);
            $this->db->where("entity_type", $this->config->item('attribute_child_group'));
            if ($eid != "")
                $this->db->where("id !=", $eid);
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

        /**
         * getAgeGroupList() method list the age group options
		  * @todo this method used to get age group list from entity_attributes table
         * @return age group title in the form of an array OR return false.
         */
        function getAgeGroupList() {
            $this->db->select('id,entity_title');
            $this->db->order_by('entity_title');
            $this->db->where("entity_type", $this->config->item('attribute_child_group'));
            $query = $this->db->get('entity_attributes');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

        /**
         * @method delAgeGroup() method delete the age group options
         * @param int $dId The age group options id
		  * @todo this method used to delete age group title from  entity_attributes table
         * @return true|false
         */
        function delAgeGroup($dId) {
            $this->db->where('id', $dId);
            $this->db->where("entity_type", $this->config->item('attribute_child_group'));
            return $this->db->delete('entity_attributes');
        }

        /**
         * updateAgeGroup() method update the age group options
         * @param int $dbId The age group options id
         * @param array() $newdata hold age group title records in the form of an array.
		  * @todo this method used to update age group title from entity_attributes table
         */
        function updateAgeGroup($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->where("entity_type", $this->config->item('attribute_child_group'));
            $this->db->update('entity_attributes', $newdata);
        }
/**
     * @method addCompany() method insert company
	 * @param array() $data hold company name in the form of an array
	 * @todo this method used to insert company name into companies table 
     * @return id|false
     */
        function addCompany($data) {
            if ($this->db->insert('companies', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }
/**
     * @method addCompanyMarkets() method insert company market
	 * @param array() $data hold company market in the form of an array
	 * @todo this method used to insert company market into company_markets table 
     * @return id|false
     */
        function addCompanyMarkets($data) {
            if ($this->db->insert_batch('company_markets', $data)) {
                return true;
            }
            return false;
        }
/**
     * @method getCompanyMarkets() method get the market id
     * @param varchar $title The chain title
     * @param int $cId hold company id
	 * @todo this method used to get company market id from company_markets table 
     * @return id|false
     */
        function getCompanyMarkets($cId) {
            $this->db->select('market_id');
            $this->db->where("company_id", $cId);
            $query = $this->db->get('company_markets');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }
/**
     * @method getCompany() method get the company name
     * @param varchar $cname company name
	 * @todo this method used to get company name  from companies table 
     * @return id|false
     */
        function getCompany($cname) {
            $this->db->select('id');
            $this->db->where("company_name", $cname);
            $query = $this->db->get('companies');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }

/**
     * @method getCompanyDetails() method used to get id, company name, address, country code, city, head, company  logo primary company id and status
     * @param varchar $cname company name
	 * @todo this method used to get company details  from companies table 
     * @return id|false
     */
        function getCompanyDetails($cid) {
            $this->db->select('id,company_name,address,country_code,city,head,company_logo,primary_company_id,status');
            $this->db->where("id", $cid);
            $query = $this->db->get('companies');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row;
            } else {
                return false;
            }
            return false;
        }
	/**
		 * @method getCompanyLogo() method get the company logo
		 * @param int $cid company id
		 * @todo this method used to get company logo  from companies table 
		 * @return string company logo|false
		 */
        function getCompanyLogo($cid) {
            $this->db->select('company_logo');
            $this->db->where("id", $cid);
            $query = $this->db->get('companies');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->company_logo;
            } else {
                return false;
            }
            return false;
        }
 /**
     * getCompanyList() method used to get id and company name
	 * @todo this method used to get company name and id from database 
     * @return array()|false
     */
        function getCompanyList() {
            $this->db->select('id,company_name as title');
            $this->db->order_by('company_name');
            $query = $this->db->get('companies');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }
 /**
     * delCompany() method delete the company
     * @param int $did hold company id
	 * @todo this method used to  delete company company from database 
     * @return true|false
     */
        function delCompany($dId) {
            $this->delCompanyMarkets($dId);
            $this->db->where('id', $dId);
            return $this->db->delete('companies');
        }
/**
     * delCompanyMarkets() method delete the company market
     * @param int $cmpId hold company id
	 * @todo this method used to  delete company market from company_markets table 
     * @return true|false
     */
        function delCompanyMarkets($cmpId) {
            $this->db->where('company_id', $cmpId);
            return $this->db->delete('company_markets');
        }

        function updateCompany($dbId, $newdata) {
            $this->db->where('id', $dbId);
            return $this->db->update('companies', $newdata);
        }
/**
     * delLoction_point() method delete the location
     * @param int $dId hold location id
	 * @todo this method used to  delete location  from hotel_location_points table 
     * @return true|false
     */
        function delLoction_point($dId) {
            $this->db->where('id', $dId);
            return $this->db->delete('hotel_location_points');
        }
 /**
     * updateLoction() method update the location
     * @param int $dbId location  id
     * @param array() $newdata hold location in the form of an array
	 * @todo this method used to to update hotel location point in hotel_location_points  table
     */
        function updateLoction($dbId, $newdata) {
            $this->db->where('id', $dbId);
            $this->db->update('hotel_location_points', $newdata);
        }
	/**
     * checkLoctionExists() method check hotel location exist or not
     * @param int $cuntryId country id
     * @param int $cid city id
     * @param int $did district id
     * @param string $name hold location name
	 * @todo this method used to to check hotel location point exist or not in hotel_location_points  table
	 * @return true|false
     */
        function checkLoctionExists($name, $cuntryId = '', $cid = '', $did = '') {
            $this->db->select('id');
            $this->db->where("point_name", $name);
            if ($cid != '')
                $this->db->where("city_id", $cid);
            if ($did != '')
                $this->db->where("id !=", $did);
            if ($cuntryId != '')
                $this->db->where("country_id", $cuntryId);
            $query = $this->db->get('hotel_location_points');
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->id;
            } else {
                return false;
            }
            return false;
        }
/**
     * @method addLoctionPoint() method insert location point
	 * @param array() $data hold the data in the form of an array
	 * @todo this method used to add location point hotel_location_points table
     * @return id|false
     */
        function addLoctionPoint($data) {
            if ($this->db->insert('hotel_location_points', $data)) {
                return $this->db->insert_id();
            }
            return false;
        }
/**
     * @method get_location_point() method used to get hotel location
	 * @param int $countryid country id
	 * @param int $cityId city id
	 * @todo this method used to get hotel location using country code and city code.
	 * @return hotel location in the form of an array OR return false.
     */
        function get_location_point($cuntryId, $cityId) {
            $this->db->select('id,point_name');
            $this->db->where("country_id", $cuntryId);
            $this->db->where("city_id", $cityId);
            $this->db->order_by('point_name', 'ASC');
            $query = $this->db->get('hotel_location_points');
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return false;
            }
        }

}
