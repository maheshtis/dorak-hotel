<?php
/*
 * @data 15-mar-2016
 * @author tis
 * @functions:
	addCurrency()
	getCurrency()
	getCurrencyList()
	delCurrency()
	deleteCurencyCodesData()
	updateCurrency()
	importCurencyData()
	addCurencyData()
	addCountry()
	getCountry()
	checkCountryExists()
	checkCountryCodeExists()
	getCountryCode()
	getCountryName()
	getCountriesList()
	delCountry()
	updateCountry()
	addCity()
	getCity()
	checkCityExists()
	checkCityCodeExists()
	getCityByCode()
	getCitiesList()
	delCity()
	delCities()
	updateCity()
	getCityName()
	addDistrict()
	getDistrict()
	checkDistrictExists()
	getCityDistricts()
	getDistrictList()
	delDistrict()
	delCityDistricts()
	delCountryCityDistricts()
	updateDistrict()
	addRole()
	getRole()
	getRolesList()
	delRole()
	updateRole()
	addCompany()
	addCompanyMarkets()
	getCompanyMarkets()
	getCompany()
	getCompanyDetails()
	getCompanyLogo()
	getCompanyList()
	delCompany()
	delCompanyMarkets()
	updateCompany()
	delLoction_point()
	updateLoction()
	checkLoctionExists()
	addLoctionPoint()
	get_location_point()

 * @description :
 */

class Country_model extends AdminModel {
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }     
	/**
     * @method addCurrency() method is used to insert currency.
	 * @param array() $data holds currency in the form of an array.
	 * @todo this method is used to insert currency in currency table.
     * @return output is last inserted value for true else returns false.
     */
	function addCurrency($data){
        if($this->db->insert('currencies', $data)) {
			return $this->db->insert_id();
		}
		return false;
    }
    /**
     * @method getCurrency() method used to fetch currency from currency table using code, and currency usd value.
     * @param string $code currency code.
     * @param string $usval currency usd value
     * @param int $eid currency id
	 * @todo this method is used to fetch currency from currencies table. 
     * @return output is last inserted value for true else returns false.
     */
	function getCurrency($code,$usval='',$eid='') {
		$this->db->select('id');
		$this->db->where("code",$code); 
		if($usval!='')
		$this->db->where("currency_usd_value",$usval); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('currencies');
        if ($query->num_rows() > 0) {
			$row = $query->row(); 
			return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	/**
     * @method getCurrencyList() method is used to fetch currency list from DB.
	 * @todo method is used to get all currecny list from DB.
     * @return will return currency in the form of an array for true condition else will return false.
     */
	function getCurrencyList() {
		$this->db->select('id,code,currency_usd_value'); 
		$this->db->order_by('code');
        $query = $this->db->get('currencies');
        if ($query->num_rows() > 0)	{
			return $query->result(); 
		}else{
			return false;
		}
	}
	/**
     * @method delCurrency() method used to delete currency using currency id
	 * @param string $dId currency id
	 * @todo method used to delete currency from DB.
     * @return will delete the value for true condition.
     */
	function delCurrency($dId) {
		$this->db->where('id',$dId);
		return $this->db->delete('currencies');	
	}
/**
     * @method delCurrency() method used to delete currency using currency id
	 * @todo method used for delete currency
     * @return will return true or false
     */
	function deleteCurencyCodesData($delCodesArray)	{
		$this->db->where_in('code', $delCodesArray);
		return $this->db->delete('currencies');		
	}
/**
     * @method updateCurrency() method used to update currency using currency id
     * @param int $dbId  currency id
     * @param array() $newdata hold currency in the form of an array
	 * @todo this method is used to update currency in currency table
	 * @return will update currency value for true condition.
     */
	function updateCurrency($dbId,$newdata)	{
		$this->db->where('id', $dbId);
		$this->db->update('currencies', $newdata); 
	}	
	/**
     * @method importCurencyData() method is used to import currency data.
	 * @param array() $data hold currency in the form of an array.
	 * @todo this method is used to delete old currency and insert new currency in  currency table
     * @return for true condition data will be imported else will return false.
     */
	function importCurencyData($data) {
		 // clear old data
		 $this->db->truncate('currencies'); 
		 // import new data
		return $this->db->insert_batch('currencies',$data);
	}	
	/**
     * @method addCurencyData() method is used to add currency
	 * @param array() $data hold currency in the form of an array.
	 * @todo this method is used to insert currency in  currency table
     * @return for true currency data will be added.
     */
	function addCurencyData($data) {
      return $this->db->insert_batch('currencies',$data);
	}	
/**
     * @method addCountry() method used to add country
	 * @param array() $data hold country in the form of an array.
	 * @todo this method is used to insert countries in  countries table
     * @return will insert country for true condition.
     */
	function addCountry($data) {
		if($this->db->insert('countries', $data)) {
			return $this->db->insert_id();
		}
		return false;
	}
    /**
     * @method getCountry() method used to fetch country from country table using country name
     * @param string $cname country name.
	 * @todo this method used to get country from country table. 
     * @return last inserted value is the output for true condition
     */
	function getCountry($cname)	{
		$this->db->select('id');
		$this->db->where("country_name",$cname); 
		$query = $this->db->get('countries');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		}else{
			return false;
		}
		return false;
	}
	
	/**
     * @method checkCountryExists() method used to verify if country exists or not using country name and country id
     * @param string $name country name.
	 * @todo this method is used to verify if country exists or not using country name and country id in countries table. 
     * @return returns last inserted value for true else will return false.
     */
	function checkCountryExists($Id='',$name) {
		$this->db->select('id');
		if($Id!='')
		$this->db->where("id !=",$Id); 
		$this->db->where("country_name",$name);	
		$query = $this->db->get('countries');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		}else{
			return false;
		}
		return false;
	}	
	/**
     * @method checkCountryCodeExists() method used to validate if country exists or not using country code and country id
     * @param string $ccode country code.
     * @param int $Id country id.
	 * @todo this method is used to validate if country exists or not using country code and country id in countries table. 
     * @return returns last inserted value for true condition.
     */
	function checkCountryCodeExists($Id='',$ccode) {
		$this->db->select('id');
		if($Id!='')
		$this->db->where("id !=",$Id); 
		$this->db->where("country_code",$ccode);
		$query = $this->db->get('countries');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		}else{
			return false;
		}
		return false;
	}	
	 /**
     * @method getCountryCode() method used to fetch country code from country table using country id
     * @param int $cid country id.
	 * @todo this method is used to fetch country code from country table using country id from countries table. 
     * @return will return country code for true.
     */
	function getCountryCode($cid) {
		$this->db->select('country_code');
		$this->db->where("id",$cid); 
		$query = $this->db->get('countries');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->country_code;
		}else{
			return false;
		}
		return false;
	}
	/**
     * @method getCountryName() method used to fetch country name from country table using country id
     * @param int $cid country id.
	 * @todo this method is used to fetch country name from country table using country id from countries table. 
     * @return returns country name for true condition.
     */
	function getCountryName($cid) {
		$this->db->select('country_name');
		$this->db->where("id",$cid); 
		$query = $this->db->get('countries');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->country_name;
		} else{
			return false;
		}
		return false;
	}
	/**
     * @method getCountriesList() method used to fetch country list from country table
	 * @todo this method is used to fetch country list from country table 
     * @return country name and country code in the form of an array for true condition else will return false.
     */
	function getCountriesList()	{
			$this->db->select('id,country_code as code,country_name'); 
			$this->db->order_by('country_name','ASC');
			$query = $this->db->get('countries');
			if ($query->num_rows() > 0)	{
				return $query->result(); 
			}else{
				return false;
			}
	}
	/**
     * @method delCountry() method used to delete country from country table
	 * @param int $dId country id.
	 * @todo this method is used to delete country from country table 
     * @return deletes the value for true condition.
     */
	function delCountry($dId) {
		$this->delCities($dId);// delete cities of the selected cuntry
		$this->db->where('id',$dId);
		return $this->db->delete('countries');	
	}
	/**
     * @method updateCountry() method is used to update country in country table
	 * @param int $dbId country id.
	 * @param array() $newdata hold country in the form of an array.
	 * @todo this method is used to update country in country table 
	 * @return updates the value for true condition.
     */
	function updateCountry($dbId,$newdata) {
		$this->db->where('id', $dbId);
		$this->db->update('countries', $newdata); 
	}	
/**
     * @method addCity() method used to add city
	 * @param array() $data hold city in the form of an array.
	 * @todo this method is used to insert country city in  country table
     * @return returns last inserted vaue for true condition.
     */
	function addCity($data)	{
		if($this->db->insert('country_cities', $data))	{
			return $this->db->insert_id();
		}
		return false;
	}
    /**
     * @method getCity() method used to fetch country city name from country_cities table using city name
     * @param string $cname city name.
	 * @todo this method used to fetch city id from country_cities table using city name. 
     * @return returns ;ast inserted value for true condition.
     */
	function getCity($cname)   {
		$this->db->select('id');
		$this->db->where("city_name",$cname); 
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		}else{
			return false;
		}
		return false;
	}
	
	/**
     * @method checkCityExists() method used to verify if city exists or not using city name, city id and country id.
     * @param string $name city name.
     * @param int $cid city id.
     * @param int $cuntryId country id.
	 * @todo this method is used to check if city exists or not using city name, city id and country id. 
     * @return returns with existing city list for true condition.
     */
	function checkCityExists($name,$cid='',$cuntryId='') {
		$this->db->select('id');
		$this->db->where("city_name",$name); 
		if($cid!='')
		$this->db->where("id !=",$cid); 
		if($cuntryId!='')
		$this->db->where("country_id",$cuntryId);	
		$query = $this->db->get('country_cities');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		}
		else{
			return false;
		}
		return false;
	}	
	/**
     * @method checkCityCodeExists() method used to validate if city exists or not using city code, city id and country id.
     * @param string $ccode city code.
     * @param int $cid city id.
     * @param int $cuntryId country id.
	 * @todo this method used to validate if city exists or not using city code, city id and country id from country_cities. 
     * @return last inserted value for true condition else returns false.
     */
	function checkCityCodeExists($ccode,$cid='',$cuntryId='')  {
		$this->db->select('id');
		if($cid!='')
		$this->db->where("id !=",$cid); 
	
		if($cuntryId!='')
		$this->db->where("country_id",$cuntryId); 
	
		$this->db->where("city_code",$ccode); 
        $query = $this->db->get('country_cities');
        if ($query->num_rows() > 0)
		{
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}	
	/**
     * @method getCityByCode() method used to fetch city id using city code.
     * @param string $ccode city code.
	 * @todo this method is used to fetch city id from country_cities. 
     * @return output gives last inserted value for true condition else returns false.
     */
	function getCityByCode($ccode){
		$this->db->select('id');
		$this->db->where("city_code",$ccode); 
		$query = $this->db->get('country_cities');
		if ($query->num_rows() > 0)
		{
		$row = $query->row(); 
		return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	
	/**
     * @method getCitiesList() method used to fetch city id, city name and city code using country id
     * @param int $cuntryId country id.
	 * @todo this method is used to fetch city id, city name and city code using country id from country_cities. 
     * @return city name and city code in the form of an array is returned fo true condition.
     */
	function getCitiesList($cuntryId) {
		$this->db->select('id,city_name,city_code'); 
		$this->db->order_by('city_name','ASC');
		$this->db->where("country_id",$cuntryId); 
		$query = $this->db->get('country_cities');
		if ($query->num_rows() > 0)
		{
		return $query->result(); 
		}
		else{
			return false;
		}
	}
	/**
     * @method delCity() method used to delete country city using id
     * @param int $dId city id.
	 * @todo this method is used to delete city using city id from country_cities. 
     * @return will delete the value fr true else returns false.
     */
	function delCity($dId) {
		$this->delCityDistricts($dId);// delCityDistricts method used to delete district
		$this->db->where('id',$dId);
		return $this->db->delete('country_cities');	
	}
	/**
     * @method delCities() method used to delete cities using country id.
     * @param int $cuntryId country id.
	 * @todo this method is used to delete cities using country id from country_cities. 
     * @return will delete value for true condition else will return false.
     */
	function delCities($cuntryId) {
		$this->delCountryCityDistricts($cuntryId);
		$this->db->where('country_id',$cuntryId);
		return $this->db->delete('country_cities');	
	}
/**
     * @method updateCity() method used to update city name using city id
     * @param int $dbId country id.
     * @param array $newdata hold  country id.
	 * @todo this method is used to update city name using city id from country_cities. 
     * @return will update value for true condition else will return false.
     */
	function updateCity($dbId,$newdata) {
		$this->db->where('id', $dbId);
		$this->db->update('country_cities', $newdata); 
	}

	/**
     * @method getCityName() method used to fetch city name using city id
     * @param int $cId city id.
	 * @todo this method used to fetch city name using city id from country_cities. 
     * @return city name in the form of an array is returned for true condition.
     */	
	function getCityName($cId) {
		$this->db->select('city_name');
		$this->db->where("id",$cId); 
		$query = $this->db->get('country_cities');
		if ($query->num_rows() > 0)	{
		$row = $query->row(); 
		return $row->city_name;
		} else{
			return false;
		}
		return false;
	}
/**
     * @method addDistrict() method used to add district name.
     * @param array $data hold city name in the form of an array.
	 * @todo this method is used to add district name using city id in city_districts. 
     * @return will return last inserted value for true condition.
     */	
	function addDistrict($data)	{
		if($this->db->insert('city_districts', $data))	{
			return $this->db->insert_id();
		}
		return false;
	}
    /**
     * @method getDistrict() method used to fetch district id
     * @param string $dname hold district name
     * @param int $cid hold city id
	 * @todo this method used to fetch district id from city_districts table. 
     * @return returns last inserted value for true condition.
     */
	function getDistrict($dname,$cid='') {
		$this->db->select('id');
		$this->db->where("district_name",$dname); 
		if($cid!='')
		$this->db->where("city_id",$cid); 
		$query = $this->db->get('city_districts');
		if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		} else{
			return false;
		}
		return false;
	}
	
	/**
     * @method checkDistrictExists() method used to vaidate if  district name exists or not
     * @param string $name hold district name
     * @param int $cid hold city id
     * @param int $did hold district id
	 * @todo this method is used to validate if  district name exists or not in city_districts table. 
     * @return returns last inserted value for true condition else will return false.
     */
	function checkDistrictExists($name,$cuntryId='',$cid='',$did='') {
		$this->db->select('id');
		$this->db->where("district_name",$name);
		if($cid!='')
		$this->db->where("city_id",$cid); 
		if($did!='')
		$this->db->where("id !=",$did); 
		if($cuntryId!='')
		$this->db->where("country_id",$cuntryId);	
		$query = $this->db->get('city_districts');
		if ($query->num_rows() > 0)
		{
		$row = $query->row(); 
		return $row->id;
		}
		else{
			return false;
		}
		return false;
	}	
	/**
     * @method getCityDistricts() method used to fetch city district name.
     * @param int $cityId hold city id
     * @param int $did hold district id
	 * @todo this method is used to fetch city district name city_districts table.
     * @return will return id and district name in the form of an array for true condition else will return false.
     */
	function getCityDistricts($cityId){
			$this->db->select('id,district_name');		
			$this->db->where("city_id",$cityId); 
			$this->db->order_by('district_name','ASC');
			$query = $this->db->get('city_districts');
			if ($query->num_rows() > 0){
			return $query->result(); 
			}
			else{
				return false;
			}
	}
/**
     * @method getDistrictList() method used to fetch district list using country id and city id.
     * @param int $cuntryId hold country id
     * @param int $cityId hold city id
	 * @todo this method is used to fetch district list from city_districts table. 
     * @return will return id and district name in the form of an array for true condition else will return false.
     */	
	function getDistrictList($cuntryId,$cityId){
		$this->db->select('id,district_name');		
		$this->db->where("country_id",$cuntryId); 
		$this->db->where("city_id",$cityId); 
		$this->db->order_by('district_name','ASC');
		$query = $this->db->get('city_districts');
		if ($query->num_rows() > 0){
		return $query->result(); 
		}
		else{
			return false;
		}
	}
/**
     * @method delDistrict() method used to delete district using district id
     * @param int $dId hold district id
	 * @todo this method is used to delete district using district id from city_districts table. 
     * @return will delete the value for true condition else will return false.
     */	
	function delDistrict($dId){
		$this->db->where('id',$dId);
		return $this->db->delete('city_districts');	
	}
/**
     * @method delCityDistricts() method used to delete district using city id.
     * @param int $city_id hold city id
	 * @todo this method is used to delete district using city id from city_districts table. 
     * @return will delete district value for true condition else will return false.
     */	
	function delCityDistricts($city_id)	{
		$this->db->where('city_id',$city_id);
		return $this->db->delete('city_districts');	
	}
	/**
     * @method delCountryCityDistricts() method is used to delete district using country id.
     * @param int $country_id hold country id
	 * @todo this method is used to delete district using country id from city_districts table. 
     * @return will delete the value for true condition else will return false.
     */	
	function delCountryCityDistricts($country_id){
		$this->db->where('country_id',$country_id);
		return $this->db->delete('city_districts');	
	}
/**
     * @method updateDistrict() method used to update district name using district id
     * @param int $dbId hold district id
     * @param array() $newdata hold district name in the form of an array.
	 * @todo this method is used to update district name using district id from city_districts table. 
	   @return will update the value for true condition.
     */	
	function updateDistrict($dbId,$newdata){
		$this->db->where('id', $dbId);
		$this->db->update('city_districts', $newdata); 
	}	
	/**
     * @method addRole() method used to add role in database.
     * @param array() $data hold role name in the form of an array.
	 * @todo this method is used to insert role into role table. 
	 * @return will add roe for true condition else will return false.
     */	
	function addRole($data)	{
		if($this->db->insert('role', $data)) {
			return $this->db->insert_id();
		}
		return false;
	}
	/**
     * @method getRole() method used to fetch role from database
     * @param string $cname hold role name .
	 * @todo this method is used to fetch role from role table. 
	 * @return will return last inserted value for true condition.
     */		
	function getRole($cname,$eid=""){
		$this->db->select('id');
		$this->db->where("title",$cname); 
		if($eid!="")	
		$this->db->where("id !=", $eid);
        $query = $this->db->get('role');
        if ($query->num_rows() > 0){
        $row = $query->row(); 
        return $row->id;
		}
		else{
			return false;
		}
		return false;
	}
	/**
     * @method getRolesList() method used to fetch role title,id and access level id from database
	 * @todo this  method used to fetch role title,id and access level id from role table. 
	 * @return title and access level id in the form of an array is returned as output for true condition.
     */	
	function getRolesList(){
		$this->db->select('id,title,access_level_id'); 
		$this->db->order_by('title');
        $query = $this->db->get('role');
        if ($query->num_rows() > 0){
        return $query->result(); 
		}
		else{
			return false;
		}
	}
/**
     * @method delRole() method used to delete role
	 * @todo this  method is used to delete role from role table. 
	 * @return will del role for true condition else will return false.
     */
	function delRole($dId){
		$this->db->where('id',$dId);
		return $this->db->delete('role');	
	}
	/**
     * @method updateRole() method is used to update role
	 * @param int $dbId hold role id .
	 * @param array() $newdata hold role in the form of an array.
	 * @todo this  method is used to update role inside role table in DB.
	 * @return will update role for true condition else will return false.
     */
	function updateRole($dbId,$newdata){
		$this->db->where('id', $dbId);
		$this->db->update('role', $newdata); 
	}		
	/**
     * @method addCompany() method used to insert company
	 * @param array() $data hold company name in the form of an array.
	 * @todo this  method is used to insert company in company table. 
	 * @return will return last inserted value for true condition.
     */
	function addCompany($data){
        if($this->db->insert('companies', $data)){
			return $this->db->insert_id();
		}
		return false;
    }
	/**
     * @method addCompanyMarkets() method used to insert company_markets table
	 * @param array() $data hold company market name in the form of an array.
	 * @todo this  method is used to insert company market name into company_markets table. 
	 * @return will add the data for true condition else will return false.
     */
	function addCompanyMarkets($data) {
       if($this->db->insert_batch('company_markets', $data)){
		return true;
	  }
		return false;
    }
/**
     * @method getCompanyMarkets() method used to fetch market id company_markets table
	 * @param int $cId hold company id
	 * @todo this  method is used to fetch company market id from company_markets table. 
	 * @return market ids in the form of an array is returned for true condition else will return false.
     */	
	function getCompanyMarkets($cId) {
		$this->db->select('market_id');
		$this->db->where("company_id",$cId); 
        $query = $this->db->get('company_markets');
        if ($query->num_rows() > 0){
			return $query->result(); 
		} else{
			return false;
		}
	}
    /**
     * @method getCompany() method used to fetch id from company table
	 * @param string $cname hold company name
	 * @todo this  method used to fetch company name from company table. 
	 * @return company ids in the form of an array is returned as output for true condition.
     */
	function getCompany($cname)  {
		$this->db->select('id');
		$this->db->where("company_name",$cname); 
        $query = $this->db->get('companies');
        if ($query->num_rows() > 0)	{
			$row = $query->row(); 
			return $row->id;
		}else{
			return false;
		}
		return false;
	}
	 /**
     * @method getCompanyDetails() method used to fetch company details from company table.
	 * @param string $cid hold company id
	 * @todo this  method is used to fetch company id from company table. 
	 * @return company details in the form of an array is returned for true condition else will return false.
     */
	function getCompanyDetails($cid) {
		$this->db->select('id,company_name,address,country_code,city,head,company_logo,primary_company_id,status');
		$this->db->where("id",$cid); 
		$query = $this->db->get('companies');
		if ($query->num_rows() > 0){
		$row = $query->row(); 
		return $row;
		}
		else{
			return false;
		}
		return false;
	}
	/**
     * @method getCompanyLogo() method used to fetch company logo from company table
	 * @param string $cid hold company id
	 * @todo this  method is used to fetch company logo from company table. 
	 * @return company logo in the form of an array is returned for true condition.
     */	
	function getCompanyLogo($cid){
		$this->db->select('company_logo');
		$this->db->where("id",$cid); 
		$query = $this->db->get('companies');
		if ($query->num_rows() > 0){
		$row = $query->row(); 
		return $row->company_logo;
		}
		else{
			return false;
		}
		return false;
	}
	/**
     * @method getCompanyList() method used to fetch company name from company table
	 * @todo this  method is used to fetch company name from company table. 
	 * @return company logo in the form of an array is returned as output for true condition.
     */
	function getCompanyList() {
		$this->db->select('id,company_name as title'); 
		$this->db->order_by('company_name');
		$query = $this->db->get('companies');
		if ($query->num_rows() > 0)
		{
		return $query->result(); 
		}
		else{
			return false;
		}
	}
	/**
     * @method delCompany() method used to delete company from company table
	 * @param $did company
	 * @todo this  method used to delete company from company table. 
	 * @return will delete the value for true condition else will return false.
     */
	function delCompany($dId){
		$this->delCompanyMarkets($dId);
		$this->db->where('id',$dId);
		return $this->db->delete('companies');	
	}
	/**
     * @method delCompanyMarkets() method used to delete company market from company_markets table
	 * @param $cmpId company id
	 * @todo this  method is used to delete company market from company_markets table
	 * @return will delete company market data for true condition else will return false.
     */
	function delCompanyMarkets($cmpId){
		$this->db->where('company_id',$cmpId);
		return $this->db->delete('company_markets');	
	}
	/**
     * @method updateCompany() method used to update company in company table
	 * @param $dbId company id
	 * @param array $newdata hold company name in the form of an array
	 * @todo this  method used to update company market in company_markets table in DB.
	 * @return will update company for true condition else will return false.
     */
	function updateCompany($dbId,$newdata){
		$this->db->where('id', $dbId);
		return $this->db->update('companies', $newdata); 
	}	
  	/**
     * @method delLoction_point() method used to delete hotel location from hotel_location_points table.
	 * @param $dId company id
	 * @todo this method is used to delete hotel location from hotel_location_points table  from DB.
	 * @return will delete location point for true condition else will return false.
     */
    function delLoction_point($dId) {
        $this->db->where('id', $dId);
        return $this->db->delete('hotel_location_points');
    }
	/**
     * @method updateLoction() method used to update hotel location in hotel_location_points table.
	 * @param $dbId company id
	 * @param array() $newdata hold hotel location in the form of an array
	 * @todo this method used to update hotel location from hotel_location_points table.
	 * @return will delete location point for true condition else will return false.
     */
    function updateLoction($dbId, $newdata) {
        $this->db->where('id', $dbId);
        $this->db->update('hotel_location_points', $newdata);
    }
	/**
     * @method checkLoctionExists() method used to validate if hotel location exists or not in hotel_location_points table.
	 * @param  string $name location name
	 * @param  int $cuntryId country id
	 * @param  int $cid city id
	 * @param  int $did district id
	 * @todo this method is used to validate if hotel location exists or not in hotel_location_points table.
	 * @return will check the list for true condition else will return false.
     */
    function checkLoctionExists($name, $cuntryId = '', $cid = '', $did = '') {
        $this->db->select('id');
        $this->db->where("point_name", $name);
        if ($cid != '')
            $this->db->where("city_id", $cid);
        if ($did != '')
            $this->db->where("id !=", $did);
        if ($cuntryId != '')
            $this->db->where("country_id", $cuntryId);
        $query = $this->db->get('hotel_location_points');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
        return false;
    }
	/**
     * @method addLoctionPoint() method used to insert location point in hotel_location_points table.
	 * @param  array() $data hold location name in the form of an array.
	 * @todo this  method is used to insert location point in hotel_location_points table
	 * @return  will add location point for true condition else will return false.
     */
    function addLoctionPoint($data) {
        if ($this->db->insert('hotel_location_points', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }
	/**
     * @method get_location_point() method used to fetch location  point in hotel_location_points table.
	 * @param  int $cuntryId  hold country id
	 * @param  int $cityId  hold city id
	 * @todo this method used to fetch location  point in hotel_location_points table in DB.
	 * @return result is in the form of an array for true condition else will return false.
     */
    function get_location_point($cuntryId, $cityId) {
        $this->db->select('id,point_name');
        $this->db->where("country_id", $cuntryId);
        $this->db->where("city_id", $cityId);
        $this->db->order_by('point_name', 'ASC');
        $query = $this->db->get('hotel_location_points');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
}