<h2>Currency </h2>
<div class="search-box">
<div class="tg1">
<a class="add-hotel" href="javascript:void(0);" id="imageid">Add Currency</a>
<div id="toggle">
    <div class = pull-right id = "import_curid">
        <img src="<?php echo base_url(); ?>assets/themes/default/images/close-estimate.png" alt="add">
    </div>
<?php echo form_open_multipart($addAction,array('class' => 'curency_add', 'id' => 'curency_add'));?>
<?php echo form_input($add_input_name)?>
<?php echo form_input($add_input_value)?>
<input class="ok" type="submit" value="OK">
<?php   echo form_close();?>
</div>
</div>
<div class="tg2">
<a class="add-hotel" href="javascript:void(0);" id="import_curid2">Upload Conversion Rate</a>
<div id="toggle-block" class="toggle-block">
    <div class = pull-right id = "import_curid2">
        <img src="<?php echo base_url(); ?>assets/themes/default/images/close-estimate.png" alt="add">
    </div>
<?php echo form_open_multipart($importAction,array('class' => 'import_currate', 'id' => 'import_currate'));?>
<?php //echo form_upload('curency_file');?>

 <div class="input-group">
	<span class="input-group-btn">
	<span class="btn btn-primary btn-file">
	<img src="<?php echo base_url(); ?>assets/themes/default/images/upload-now.jpg" alt="upload"> Upload Now 
	<?php echo form_upload('curency_file','',' data-validation="required,extension,size" data-validation-max-size="1M"   data-validation-allowing="xls,xlsx"'); ?> 
	<?php echo form_error('curency_file'); ?>
	<input type="text" class="form-control input-file-postion" readonly>
     </span>										
   </div>
<?php echo form_checkbox($clear_old_data);?>
<input class="ok" type="submit" value="OK">
<?php   echo form_close();?>

<a class="sample-download" href="<?php echo base_url('samples/conversion_sample.xlsx'); ?>" target="_blank">Download Sample file</a>

</div>
</div>
</div>
<div class="setting-room-type">
<?php echo form_open($addAction,array('class' => 'curency_edit', 'id' => 'curency_edit'));?>

<div class="mian-head setting-section">
<table id="hotel-curency-table" class="table settings-table table-striped  dt-responsive nowrap" cellspacing="0" width="100%">
<thead>
<tr>
<th>Currency</th>
<th>Amount</th>
<th class="action-col">Action</th>               
</tr>
</thead>
<tbody>
<?php if($listData && count($listData)>0){
	foreach($listData as $sData)
	{
	?>
	<tr>
	<td><label id="label-block-<?php echo $sData->id?>">
	<?php echo $sData->code;?></label><span class="edit-block" style="display:none;" id="edit-block-<?php echo $sData->id?>">
	<input type="text" id="new-title-<?php echo $sData->id?>" data-validation-length="min2" maxlength = "30" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation="required,custom,length" name="new_title_<?php echo $sData->id?>" value="<?php echo $sData->code;?>">
	</span></td>
	<td>
	<label id="label-cval-block-<?php echo $sData->id?>">
	<?php echo $sData->currency_usd_value;?></label><span class="edit-block" style="display:none;" id="edit-cval-block-<?php echo $sData->id?>">
	<input type="text" maxlength = "10" id="new-cval-<?php echo $sData->id?>" data-validation-allowing="float" data-validation="required,price" name="new_cval_<?php echo $sData->id?>" value="<?php echo $sData->currency_usd_value;?>">
	</td>
	
	<td>
	<span class="action-span" id="additional-action-blok-<?php echo $sData->id?>" style="display:none;">
	<a class="save_button"  id="savelnk_cval-<?php echo $sData->id?>" onclick="inlinesaveAction(<?php echo $sData->id;?>)">save</a>
	<a class="cancel_button" id="cancellnk_<?php echo $sData->id?>" onclick="cancelsaveAction(<?php echo $sData->id;?>)">cancel</a>
	</span>
	<span class="edit-del-action">
	<a title="Edit" rel="<?php echo $sData->id?>" href="javascript:void(0);" onclick="toggleEdit(<?php echo $sData->id?>)"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-edit.jpg" alt="edit"></a> 
	<a title="Delete" href="<?php echo $delAction ?>/<?php echo $sData->code?>" onclick="return confirm('Are you sure you want to remove <?php echo !empty($sData->code) ? $sData->code : '';?> currency?');"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting-remove.jpg" alt="remove"></a>
	</span></td></tr>
	<?php 
	} 
	} 
	?>
</tbody>
</table>
</div>
<?php   echo form_close();?>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
  <script>
  
  
$.formUtils.addValidator({
  name : 'price',
  validatorFunction : function(value, $el, config, language, $form) {
    if(value.indexOf('.')!=-1){ 		
		var filter = /^[0-9,]+\.\d{1,3}$/;
			if (!filter.test(value)) {
			return false;
		}		
		var dval=value.split(".")[0];
		 if(value.split(".")[0].length < 1){                
               return false;
            } 
          if( dval < 0 )
            {
				 return false;
			}		     
          if(value.split(".")[1].length > 2){                
               return false;
            }         
         else
         {
			    return true;
		 } 
		} 
		else
		 {
			 if(value > 0)
			 {
				return true;
			 }
			 else{
				 return false;
			 }
		}    
  },
  errorMessage : 'Enter valid price value',
  errorMessageKey: 'badminfloat'
});
  
  $(document).on('click','#imageid',function(){
    var $this= $(this);
    $('#toggle').toggle();
	$('#toggle-block').hide();  	
   $.validate({
	form : '#curency_add'
	});
  });
  
  $(document).on('click','#import_curid2',function(){
    var $this= $(this);
    $('#toggle-block').toggle();  
	$('#toggle').hide();	
   $.validate({
	form : '#import_currate',
	modules : 'file'
	});
	
	
  });
 
 $(document).ready(function (){
	  
	$.validate({
		form : '#curency_edit'
	});
	
  var table = $('#hotel-curency-table').DataTable(
   {
	  "searching": false,
		"lengthMenu": [[10, 25, 50, -1], [" 10 Per Page"," 25 Per Page", " 50 Per Page", "All"]],
		language : {
        sLengthMenu: "View: _MENU_"
  },
  "columns": [
    { "width": "33%" },
	{ "width": "33%" },
    { "width": "33%" }
  ],
  
  "aoColumns": [
      null,
      null,
      { "bSortable": false }
    ],
	 "order": [[ 0, "asc" ], [ 1, 'asc' ]]
  
  }
   );
   

   $('.btn-file :file').on('fileselect', function(event, numFiles, label){
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });	
   
  });
  
  $(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});
  
  function inlinesaveAction(rowid)
  {
	$('#new-title-'+rowid).validate(function(valid, elem) {
	if(valid)
	{
	$('#new-cval-'+rowid).validate(function(valid, elem) {
	if(valid)
	 {
	 var newval= $('#new-title-'+rowid).val();
	 var usdval= $('#new-cval-'+rowid).val();
	 $('#edit-block-'+rowid).hide();  
	 $('#label-block-'+rowid).show(); 
	 $('#edit-cval-block-'+rowid).hide();  
	 $('#label-cval-block-'+rowid).show(); 
	 $('#additional-action-blok-'+rowid).hide(); 
	 jQuery.ajax({
		type: "POST",
		url:"<?php echo $editAction?>",
		dataType: "json",
		data: {newname: newval,newusdval: usdval,id: rowid},
		success: function(res) {			
		if(res["error"]==false)
		{
		$('#new-title-'+rowid).val(res["curency"]);	
		$('#label-block-'+rowid).html(res["curency"]);
		$('#new-cval-'+rowid).val(res["usdval"]);	
		$('#label-cval-block-'+rowid).html(res["usdval"]);
		}
		else{
			alert('Currency : ' + res["curency"] + ' with amount '+ res["usdval"] + ' already exists');
		}
		}
		});
		}
	  });
	  }
	  });
  }
  function toggleEdit(rowid)
  {
	   $('#edit-block-'+rowid).toggle(); 
	   $('#edit-cval-block-'+rowid).toggle(); 
	   $('#label-block-'+rowid).toggle(); 	
	   $('#label-cval-block-'+rowid).toggle(); 	
	  $('#additional-action-blok-'+rowid).toggle(); 	   
  }
  
 function cancelsaveAction(rowid)
  {
	$('#edit-block-'+rowid).hide();  
	$('#label-block-'+rowid).show();  
	$('#edit-cval-block-'+rowid).hide();  
	$('#label-cval-block-'+rowid).show(); 
	$('#additional-action-blok-'+rowid).hide(); 	 
  }
  
</script>