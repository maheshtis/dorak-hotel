<?php

/*
 * @data 15-mar-2016
 * @author tis
 * @description This controller  is perform all action like add update sevral checks which is related to hotel.
 */

class Hotel_model extends AdminModel {

    function __construct() {
        // Call the Model constructor.(it call the base model to hmvc to construct the instanse of a model)
        parent::__construct();
    }

    /**
     * @meathod : string getHotelsDetails(), get full details of a hotels based on a hotel id.
     * @param : int $hotelId
     * @todo : Fetch all hotel information with all related attributes like country,chain of associative hotel ID. 
     * @return :array(), hotel details of hotel in the form of array.
     */
    function getHotelsDetails($hotelId) {
        $this->db->select('hotels.hotel_id,
							hotels.hotel_name,
							hotels.hotel_code,
							hotels.country_code,
							hotels.city,
							hotels.hotel_address,
							hotels.post_code,
							hotels.district,
							hotels.dorak_rating,
							hotels.star_rating,
							hotels.purpose,
							hotels.currency,
							hotels.chain_id,							
							hotels.commisioned,
							hotels.child_age_group_id,
							hotels.status');
        $this->db->from('hotels');
        $this->db->where('hotels.hotel_id', $hotelId);
        $query = $this->db->get();
        return $query->row();
    }

     /**
     * @meathod : string getHotelsList(), get list of hotel.
     * @param : $search_term(array), $limit(int),$offset(int).
     * @todo : Fetch all hotel information based on parameter of searching a hotel and default limit is 10 hotels. 
     * @return :array(), hotel details of hotel in the form of array.
     */
    function getHotelsList($search_term = null, $limit = 10, $offset = 0) {
        $this->db->select('hotels.hotel_id,
							hotels.hotel_name,
							hotels.hotel_code,
							countries.country_name,
							hotels.city,
							hotels.dorak_rating,
							hotels.star_rating,
							hotel_purpose.title as purpose,
							hotels.last_updated,
							cn.title as chain,
							hs.title as status,
							pt.title as property_types');
        $this->db->from('hotels');
        $this->db->join('hotel_status hs', 'hotels.status = hs.id');
        $this->db->join('countries', 'hotels.country_code = countries.country_code');
        $this->db->join('hotel_chain_list cn', 'cn.chain_id = hotels.chain_id', 'left');
        $this->db->join('hotel_purpose', 'hotel_purpose.id = hotels.purpose', 'left');
        $this->db->join('hotel_property_types  hpt', 'hotels.hotel_id = hpt.hotel_id', 'left');
        $this->db->join('property_types  pt', 'pt.id = hpt.property_type_id', 'left');
        if ($search_term != "" and count($search_term) > 0) {

            if ($search_term['searchByHotelName'] != "")
                $this->db->like('hotels.hotel_name', $search_term['searchByHotelName']);
            if ($search_term['searchByHotelCode'] != "")
                $this->db->like('hotels.hotel_code', $search_term['searchByHotelCode']);
            if ($search_term['searchByPurpose'] != "")
                $this->db->where('hotels.purpose', $search_term['searchByPurpose']);
            if ($search_term['searchByStarRating'] != "")
                $this->db->where('hotels.star_rating', $search_term['searchByStarRating']);
            if ($search_term['searchByStatus'] != "")
                $this->db->where('hotels.status', $search_term['searchByStatus']);
            if ($search_term['searchByChainId'] != "")
                $this->db->where('cn.chain_id', $search_term['searchByChainId']);
            if ($search_term['searchByPropertyType'] != "")
                $this->db->where('pt.id', $search_term['searchByPropertyType']);
        }
        $this->db->order_by("hotels.hotel_name", "asc");
        $this->db->group_by('hotels.hotel_id');
        $this->db->limit($limit, $offset);

        $query = $this->db->get();
        return $query->result();
    }

    /**
     * string deleteHotels() :-(select all hotel) used to delete a hotel with all related hotel entity.
     * @param array $hids The hotel id
     * @todo it will delete hotel list(array format) with related all entities like property types,facilities,roomtypes etc.
     * @return true
     */
    function deleteHotels($hids) {
        $bank_del = $this->deleteHotelBankDetails($hids); // delete bank details first due to reference fo forien key relationship
        if ($bank_del) { // if bank details deletd successfully then it will deleted all hotels attributes.

            $this->deleteHotelPropertyTypes($hids);
            $this->deleteHotelFacilities($hids);
            $this->deleteHotelDistance($hids);
            $this->deleteHotelContactDetails($hids);
            $this->deleteHotelContractInfo($hids);
            $this->deleteHotelRenovationSchedule($hids);
            $this->deleteHotelComplimentaryRoomExcludedDates($hids);
            $this->deleteHotelComplimentaryRoom($hids);
            $this->deleteHotelComplimentaryServices($hids);
            $this->deleteHotelRoomsPricingComplimentary($hids);
            $this->deleteHotelRoomsPricing($hids);
            $this->db->where_in('hotel_id', $hids);
            $this->db->delete('hotels');
        }
        return true;
    }

    /**
     * string deleteHotel() method delete the hotel one by one
     * @param int $hid The hotel id
     * @todo delete hotels one by one based on hotel ids.
     * @return true
     */
    function deleteHotel($hid) {
        $hotelId = (int) $hid;
        $bankrecord = $this->deleteHotelBankDetails($hotelId);
        if ($bankrecord) {
            $this->deleteHotelBankDetails($hotelId);
            $this->deleteHotelPropertyTypes($hotelId);
            $this->deleteHotelFacilities($hotelId);
            $this->deleteHotelDistance($hotelId);
            $this->deleteHotelContactDetails($hotelId);
            $this->deleteHotelContractInfo($hotelId);
            $this->deleteHotelRenovationSchedule($hotelId);
            $this->deleteHotelComplimentaryRoomExcludedDates($hotelId);
            $this->deleteHotelComplimentaryRoom($hotelId);
            $this->deleteHotelComplimentaryServices($hotelId);
            $this->deleteHotelRoomsPricingComplimentary($hotelId);
            $this->deleteHotelRoomsPricing($hotelId);
            $this->db->where('hotel_id', $hotelId);
            $this->db->delete('hotels');
        }
        return true;
    }

    /**
     * string deleteHotelBankDetails() method delete the bank detail
     * @param array|int $hids The hotel id
     * @todo it delete hotel bank details associated to a hotel ids.
     * @return true
     */
    function deleteHotelBankDetails($hids) {
        if (is_array($hids)) {
            $this->db->where_in('hotel_id', $hids);
        } else {
            $this->db->where('hotel_id', (int) $hids);
        }
        return $this->db->delete('hotel_bank_accounts');
    }

    /**
     * string deleteHotelFacilities() method delete the hotel facilities.
     * @param array|int $hids The hotel id
     * @todo it delete hotel facilities associated to a hotel ids.
     * @return true
     */
    function deleteHotelFacilities($hids) {
        if (is_array($hids)) {
            $this->db->where_in('hotel_id', $hids);
        } else {
            $this->db->where('hotel_id', (int) $hids);
        }
        return $this->db->delete('hotel_facilities');
    }

     /**
     * string deleteHotelPropertyTypes() method delete the hotel property type
     * @param array|int $hids The hotel id
     * @todo it delete hotel property associated to a hotel ids.
     * @return true
     */
    function deleteHotelPropertyTypes($hids) {
        if (is_array($hids)) {
            $this->db->where_in('hotel_id', $hids);
        } else {
            $this->db->where('hotel_id', (int) $hids);
        }
        return $this->db->delete('hotel_property_types');
    }

    /**
     * string deleteHotelDistance() method delete hotel distance
     * @param array|int $hids The hotel id
     * @todo it delete hotel distance associated to a hotel ids.
     * @return true
     */
    function deleteHotelDistance($hids) {
        if (is_array($hids)) {
            $this->db->where_in('hotel_id', $hids);
        } else {
            $this->db->where('hotel_id', (int) $hids);
        }
        return $this->db->delete('hotel_distance');
    }

    /**
     * string deleteHotelContactDetails() method delete hotel contact details.
     * @param array|int $hids The hotel id
     * @todo it delete hotel contact details associated to a hotel ids.
     * @return true
     */
    function deleteHotelContactDetails($hids) {
        if (is_array($hids)) {
            $this->db->where_in('hotel_id', $hids);
        } else {
            $this->db->where('hotel_id', (int) $hids);
        }
        return $this->db->delete('hotel_contact_details');
    }

     /**
     * string deleteHotelContractInfo() method delete hotel contract information.
     * @param array|int $hids The hotel id
     * @todo it delete hotel contact details associated to a hotel ids.
     * @return true
     */
    function deleteHotelContractInfo($hids) {
        if (is_array($hids)) {
            $this->db->where_in('hotel_id', $hids);
        } else {
            $this->db->where('hotel_id', (int) $hids);
        }
        return $this->db->delete('hotel_contract_info');
    }

     /**
     * string deleteHotelRenovationSchedule() method delete hotel renovation shedule.
     * @param array|int $hids The hotel id
     * @todo it delete hotel renovation shedules associated to a hotel ids.
     * @return true
     */
    function deleteHotelRenovationSchedule($hids) {
        if (is_array($hids)) {
            $this->db->where_in('hotel_id', $hids);
        } else {
            $this->db->where('hotel_id', (int) $hids);
        }
        return $this->db->delete('hotel_renovation_schedule');
    }

    /**
     * string deleteHotelComplimentaryServices() method delete hotel complementry service.
     * @param array|int $hids The hotel id
     * @todo it delete hotel complementry service associated to a hotel ids.
     * @return true
     */
    function deleteHotelComplimentaryServices($hids) {
        if (is_array($hids)) {
            $this->db->where_in('hotel_id', $hids);
        } else {
            $this->db->where('hotel_id', (int) $hids);
        }
        return $this->db->delete('hotel_complimentary_services');
    }

    /**
     * string deleteHotelComplimentaryRoom() method delete hotel complementry rooms.
     * @param array|int $hids The hotel id
     * @todo it delete hotel complementry rooms associated to a hotel ids.
     * @return true
     */
    function deleteHotelComplimentaryRoom($hids) {
        if (is_array($hids)) {
            $this->db->where_in('hotel_id', $hids);
        } else {
            $this->db->where('hotel_id', (int) $hids);
        }
        return $this->db->delete('hotel_complimentary_room');
    }

    /**
     * string deleteHotelComplimentaryRoomExcludedDates() method delete hotel complementry exclude dates rooms.
     * @param array|int $hids The hotel id
     * @todo it delete hotel complementry rooms exclude dates associated to a hotel ids.
     * @return true
     */
    function deleteHotelComplimentaryRoomExcludedDates($hids) {
        if (is_array($hids)) {
            $hids = implode($hids, ',');
        }
        $sql = 'DELETE hotel_cmplimntry_room_excluded_dates
		FROM hotel_cmplimntry_room_excluded_dates 
		INNER JOIN hotel_complimentary_room ON hotel_complimentary_room.cmpl_room_id = hotel_cmplimntry_room_excluded_dates.cmpl_room_id
		';
        $sql.=' WHERE hotel_complimentary_room.cmpl_room_id = hotel_cmplimntry_room_excluded_dates.cmpl_room_id  AND hotel_complimentary_room.hotel_id IN (' . $hids . ')';
        $this->db->query($sql);
        return true;
    }

    /**
     * string deleteHotelRoomsPricingComplimentary() method delete hotel pricings rooms(complimentry).
     * @param array|int $hids The hotel id
     * @todo it delete hotel pricings rooms(complimentry) associated to a hotel ids.
     * @return true
     */
    function deleteHotelRoomsPricingComplimentary($hids) {
        if (is_array($hids)) {
            $hidsD = implode($hids, ',');
        } else {
            $hidsD = $hids;
        }
        $sql = 'DELETE hotel_rooms_pricing_complimentary
			FROM hotel_rooms_pricing_complimentary
			INNER JOIN hotel_rooms_pricing ON hotel_rooms_pricing.pricing_id = hotel_rooms_pricing_complimentary.pricing_id
			';
        $sql.='WHERE  hotel_rooms_pricing_complimentary.pricing_id=hotel_rooms_pricing.pricing_id  AND   hotel_rooms_pricing.hotel_id IN (' . $hidsD . ')';
        $this->db->query($sql);
        return true;
    }

    /**
     * string deleteHotelRoomsPricing() method delete hotel pricings rooms.
     * @param array|int $hids The hotel id
     * @todo it delete hotel pricings rooms associated to a hotel ids.
     * @return true
     */
    function deleteHotelRoomsPricing($hids) {
        if (is_array($hids)) {
            $hidsD = implode($hids, ',');
        } else {
            $hidsD = $hids;
        }
        $sql = 'DELETE hotel_rooms_pricing,hotel_rooms_pricing_details
		FROM hotel_rooms_pricing
		INNER JOIN hotel_rooms_pricing_details ON hotel_rooms_pricing.pricing_id = hotel_rooms_pricing_details.pricing_id
		';
        $sql.='WHERE  hotel_rooms_pricing_details.pricing_id=hotel_rooms_pricing.pricing_id  AND  hotel_rooms_pricing.hotel_id IN (' . $hidsD . ')';
        $this->db->query($sql);
        return true;
    }

    /**
     * string updateHotelDetails() method to update the hotel detail
     * @param (int)$hid, array($data) hotel id used for which hotel will deleted and data stands for hotel feilds.
     * @todo it will update the hotel feilds based on hotel id.
     * @return true
     */
    function updateHotelDetails($hid, $data) {
        $this->db->where('hotel_id', $hid);
        $this->db->update('hotels', $data);
    }

    /**
     * string getHotelDetails() method get the hotels
     * @param (varchar)(text) $hname The hotel title(hotel name)
     * @param (varchar)(text) $hcity The city
     * @param (int)$postcode The post code
     * @todo it fetch the list of hotels based on hotel name,hotel city and postcode.
     * @return output hotel list array()
     */
    function getHotelDetails($hname, $hcity, $postcode) {
        $this->db->select('hotel_id');
        $this->db->where('hotel_name', $hname);
        $this->db->where('city', $hcity);
        $this->db->where('post_code', $postcode);
        $query = $this->db->get('hotels');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string insertDetails() method insert hotels
     * @param varchar $data The hotel record
     * @todo insert the hotel data into the hotel tables.
     * @return last inserted hotel id
     */
    function insertDetails($data) {
        if ($this->db->insert('hotels', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * string getHotelPropertyType() method get hotels property
     * @param varchar $title The property title
     * @todo it will fetch all property types based on property title.
     * @return id
     */
    function getHotelPropertyType($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_property_types'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }

    /**
     * string addHotelPropertyType() method add property
     * @param varchar $data The property title.
     * @todo add hotel property type into the database.
     * @return lastinserted id
     */
    function addHotelPropertyType($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * string addHotalPaymentShedules() method add payment schedule
     * @param varchar $data The payment schedule
     * @todo  add hotel payment shedules
     * @return true
     */
    function addHotalPaymentShedules($data) {
        if ($this->db->insert_batch('hotel_payment_shedules', $data)) {
            return true;
        }
        return false;
    }

   /**
     * string getHotalPaymentShedules() method used for listing to hotel payment shedules.
     * @param int $hotel_id 
     * @todo  it fetch all hotel payment shedules.
     * @return true
     */
    function getHotalPaymentShedules($hotel_id) {
        $this->db->select('id,payment_option_id,payment_value');
        $this->db->where("hotel_id", $hotel_id);
        $this->db->order_by("payment_value", "desc");
        $query = $this->db->get('hotel_payment_shedules');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string deleteHotalPaymentShedules() method delete the payment schedule
     * @param int $hotelId (array)$arrayIds The array ids
     * @todo  int delete hotel payments data associated with hotel id
     * @return true
     */
    function deleteHotalPaymentShedules($arrayIds, $hotelId) {
        if (!empty($arrayIds) && count($arrayIds) > 0) {
            $this->db->where('hotel_id', $hotelId);
            $this->db->where_not_in('id', $arrayIds);
            $this->db->delete('hotel_payment_shedules');
        }
        return true;
    }

    /**
     * updateHotalPaymentShedules() method update the payment
     * @param int $id The payment schedule id
     * @param varchar $data The payment schedule record
     * @todo it update the hotel payment shedule informations based on hotel id.
     * @return true
     */
    function updateHotalPaymentShedules($data, $id = '') {
        if ($id != "") {
            $this->db->where('id', $id);
            $q = $this->db->get('hotel_payment_shedules');
            if ($q->num_rows() > 0) {
                $this->db->where('id', $id);
                $this->db->update('hotel_payment_shedules', $data);
                return $id;
            }
        } else {
            $this->db->insert('hotel_payment_shedules', $data);
            return $this->db->insert_id();
        }
        return;
    }

    /**
     * string addHotelChain() method insert chain
     * @param varchar $data The chain title
     * @todo it add the hotel chain into DB.
     * @return id
     */
    function addHotelChain($data) {
        if ($this->db->insert('entity_attributes', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * string getHotelChain() method get the chain
     * @param varchar $title The chain title
     * @todo  it fetch the list of hotel chain based on hotel id.
     * @return id
     */
    function getHotelChain($title) {
        $this->db->select('id');
        $this->db->where("entity_title", $title);
        $this->db->where("entity_type", $this->config->item('attribute_hotel_chain'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return false;
        }
    }

    /**
     * string getHotelChainList() method list the chain
     * @todo  it fetch hotel chain list for dropdown(hotel chain)
     * @return chains array()
     */
    function getHotelChainList() {
        $this->db->select('id,entity_title');
        $this->db->where("entity_type", $this->config->item('attribute_hotel_chain'));
        $query = $this->db->get('entity_attributes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addHotalContactInfo() method insert contract
     * @param varchar $data The contract title
     * @todo it add the hotel contact info into the DB.
     * @return id
     */
    function addHotalContactInfo($data) {
        if ($this->db->insert_batch('hotel_contact_details', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getHotelChain() method get the chain
     * @param int $hotel_id The contact information
     * @todo  it fetch the hotel contact data from the database based on hotel id.
     * @return output hotel contact info form of array()
     */
    function getHotalContactInfo($hotel_id) {
        $this->db->select('contact_id,position,name,email,phone,extension');
        $this->db->where("hotel_id", $hotel_id);
        $query = $this->db->get('hotel_contact_details');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addHotalComplimentaryServices() method insert complimentary services
     * @param varchar $data The complimentary services title
     * @todo  it will add hotel complimentry service into the DB.
     * @return true
     */
    function addHotalComplimentaryServices($data) {
        if ($this->db->insert_batch('hotel_complimentary_services', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getHotalComplimentaryServices() method list the complimentary services
     * @param int $hotel_id hotel id
     * @todo it will fetch all list of hotel complimentry service based on hotel id.
     * @return complimentary services
     */
    function getHotalComplimentaryServices($hotel_id) {
        $this->db->select('complimentary_services.cmpl_service_id,complimentary_services.service_name');
        $this->db->from('complimentary_services');
        $this->db->join('hotel_complimentary_services', 'complimentary_services.cmpl_service_id = hotel_complimentary_services.cmpl_service_id');
        $this->db->where("hotel_complimentary_services.hotel_id", $hotel_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addHotalContract() method insert contract
     * @param (array)$data The contact information
     * @todo  it add hotel contract into the DB.
     * @return id
     */
    function addHotalContract($data) {
        if (!empty($data)) {
            if ($this->db->insert('hotel_contract_info', $data)) {
                return $this->db->insert_id();
            }
        }
        return false;
    }

    /**
     * string getHotalContract() method get the hotel contract information
     * @param int $hotel_id , id of hotel.
     * @todo it will fetch all details of hotel contract based on hotel id.ss
     * @return output list of hotel contract in the form array()
     */
    function getHotalContract($hotel_id) {
        $this->db->select('id,start_date,end_date,signed_by,contract_file');
        $this->db->where("hotel_id", $hotel_id);
        $this->db->order_by("creared_on", "desc");
        $query = $this->db->get('hotel_contract_info');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addHotalComlimentaryRoom() method used to insert hotel complimentary room
     * @param varchar $data The complimentary record
     * @todo  it add the hotel complimentry data in the form of array into the DB.
     * @return it return the last inserted id of the hotel complimentry.
     */
    function addHotalComlimentaryRoom($data) {
        if ($this->db->insert('hotel_complimentary_room', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * string getHotalComlimentaryRoom() method insert hotel complimentary room
     * @param id $hotel_id(hotel id).
     * @todo it fetched the hotel complimentry data based on hotel id.
     * @return output list of hotel complimentry room in the form array()
     */
    function getHotalComlimentaryRoom($hotel_id) {
        $this->db->select('cmpl_room_id,room_night,room_type,start_date,end_date,upgrade');
        $this->db->where("hotel_id", $hotel_id);
        $query = $this->db->get('hotel_complimentary_room');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    /**
     * string updateHotalComlimentaryRoom().
     * @param $data(array),(int)$hotel_id,$cmpl_room_id;
     * @todo it update the hotels complimentry room data into the database based on perticular hotel id and complimentry id
     * @return id
     */
    function updateHotalComlimentaryRoom($data, $hotel_id, $cmpl_room_id = '') {
        if ($cmpl_room_id != "") {
            $this->db->where("hotel_id", $hotel_id);
            $this->db->where('cmpl_room_id', $cmpl_room_id);
            $q = $this->db->get('hotel_complimentary_room');
            if ($q->num_rows() > 0) {
                $this->db->where('cmpl_room_id', $cmpl_room_id);
                $this->db->update('hotel_complimentary_room', $data);
                return $cmpl_room_id;
            } else {
                $this->db->insert('hotel_complimentary_room', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('hotel_complimentary_room', $data);
            return $this->db->insert_id();
        }
        return;
    }

    /**
     * string addHotalComlimentaryRoomExcludedDate() method insert Hotal Comlimentary Room Excluded Date
     * @param (array)$data The Hotal Comlimentary RoomExcluded Date title
     * @todo add hotel complimentry excludes dates into the database.
     * @return id
     */
    function addHotalComlimentaryRoomExcludedDate($data) {
        if ($this->db->insert_batch('hotel_cmplimntry_room_excluded_dates', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getHotalComlimentaryRoomExcludedDate() method get Comlimentary Room
     * @param int $hotel_cmpl_room_id The hotel complimentary id
     * @todo it fetch the list of hotel complimentry exclude date based on hotel complimentry id.
     * @return array()
     */
    function getHotalComlimentaryRoomExcludedDate($hotel_cmpl_room_id) {
        $this->db->select('id,exclude_date_from,excluded_date_to');
        $this->db->where("cmpl_room_id", $hotel_cmpl_room_id);
        $query = $this->db->get('hotel_cmplimntry_room_excluded_dates');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string getHotalComlimentaryRoomExcludedDateByHotel() method get Comlimentary Room exclude date by hotel id
     * @param int $hotel_id The hotel  id
     * @param int $hotel_id The hotel  id
     * @return array()
     */
    function getHotalComlimentaryRoomExcludedDateByHotel($hotel_id = null) {
        $this->db->select('id,exclude_date_from,excluded_date_to');
        $this->db->from('hotel_cmplimntry_room_excluded_dates');
        $this->db->join('hotel_complimentary_room', 'hotel_complimentary_room.cmpl_room_id = hotel_cmplimntry_room_excluded_dates.cmpl_room_id');
        $this->db->where("hotel_complimentary_room.hotel_id", $hotel_id);
        $this->db->order_by("hotel_cmplimntry_room_excluded_dates.exclude_date_from", "desc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string deleteHotalComlimentaryRoomExcludedDate() method delete the hotel complimentary room exclude date
     * @param array $arrayIds The array ids
     * @param int $cmpl_room_id The complimentary room id
     * @todo it delete the hotel complimentry room exclude dates in the form of array.
     * @return true
     */
    function deleteHotalComlimentaryRoomExcludedDate($arrayIds, $cmpl_room_id) {
        if (!empty($arrayIds) && count($arrayIds) > 0) {
            $this->db->where('cmpl_room_id', $cmpl_room_id);
            $this->db->where_not_in('id', $arrayIds);
            $this->db->delete('hotel_cmplimntry_room_excluded_dates');
        }
        return true;
    }

    /**
     * string updateHotalComlimentaryRoomExcludedDate() method update hotel complimentary room exclude date
     * @param int $id The complimentary room exclude date id
     * @param string $data hotel complimentary room exclude date record
     * @todo it update the hotel complimentry room exclude datae based on id.
     * @return true
     */
    function updateHotalComlimentaryRoomExcludedDate($data, $id = '') {
        if ($id != "") {
            $this->db->where('id', $id);
            $q = $this->db->get('hotel_cmplimntry_room_excluded_dates');
            if ($q->num_rows() > 0) {
                $this->db->where('id', $id);
                $this->db->update('hotel_cmplimntry_room_excluded_dates', $data);
                return $id;
            }
        } else {
            $this->db->insert('hotel_cmplimntry_room_excluded_dates', $data);
            return $this->db->insert_id();
        }
        return;
    }

    /**
     * string addHotalPropertyTypes() method insert hotel property types
     * @param int $id The complimentary room exclude date id
     * @todo it add hotel property
     * @return true
     */
    function addHotalPropertyTypes($data) {
        if ($this->db->insert_batch('hotel_property_types', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string addHotalBankingInfo() method insert bank information
     * @param varchar() $data bank accounts array
     * @todo  it will add hotel bank information into the DB.
     * @return true
     */
    function addHotalBankingInfo($data) {
        if ($this->db->insert_batch('hotel_bank_accounts', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getHotalBankInfo() method insert bank information
     * @param int $hotel_id hotel id
     * @todo it fetched hotel bank information from the database based on hotel id.
     * @return list of hotel banking information in the form of array()
     */
    function getHotalBankInfo($hotel_id) {
        $this->db->select('id,account_number,account_name,bank_name,bank_address,bank_ifsc_code');
        $this->db->where("hotel_id", $hotel_id);
        $query = $this->db->get('hotel_bank_accounts');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addHotalConfrence() method insert hotel conference
     * @param varchar() $data hotel conference array
     * @todo  add hotel confrence detail into the DB.
     * @return true
     */
    function addHotalConfrence($data) {
        if ($this->db->insert_batch('hotel_confrences', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getHotalConfrence() method get hotel conference
     * @param int $hotel_id hotel conference array
     * @todo it fetched the list of hotel confrence using hotel id.
     * @return list of hotel hotel confrence in the form of array()
     */
    function getHotalConfrence($hotel_id) {
        $this->db->select('id,confrence_name,area,celling_height,classroom,theatre,banquet,reception,conference,ushape,hshape');
        $this->db->where("hotel_id", $hotel_id);
        $query = $this->db->get('hotel_confrences');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addHotalCancellation() method insert hotel cancellation
     * @param varchar() $data hotel conference array
     * @todo add hotel cancellation data into the database.
     * @return true
     */
    function addHotalCancellation($data) {
        if ($this->db->insert_batch('hotel_cancellation', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getHotalCancellation() method get hotel cancellation
     * @param id $hotel_id hotel id
     * @todo it fetched the list of hotel cancellation using hotel id.
     * @return list of hotel hotel cancellation in the form of array()
     */
    function getHotalCancellation($hotel_id) {
        $this->db->select('id,seasion,cancelled_before,payment_request');
        $this->db->where("hotel_id", $hotel_id);
        $this->db->order_by("payment_request", "desc");
        $query = $this->db->get('hotel_cancellation');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string deleteHotalCancellation() method delete the hotel cancellation
     * @param int $arrayIds The array ids
     * @param int $hotelId The hotel id
     * @todo it delete the hotel cancellation data from the DB.
     * @return true
     */
    function deleteHotalCancellation($arrayIds, $hotelId) {
        if (!empty($arrayIds) && count($arrayIds) > 0) {
            $this->db->where('hotel_id', $hotelId);
            $this->db->where_not_in('id', $arrayIds);
            $this->db->delete('hotel_cancellation');
        }
        return true;
    }

    /**
     * string updateHotalCancellation() method update hotel cancellation
     * @param int $id The hotel cancellation id.
     * @param varchar $data The chain record.
     * @todo it update the hotel cancellation data based on id.
     * @return id
     */
    function updateHotalCancellation($data, $id = '') {
        if ($id != "") {
            $this->db->where('id', $id);
            $q = $this->db->get('hotel_cancellation');
            if ($q->num_rows() > 0) {
                $this->db->where('id', $id);
                $this->db->update('hotel_cancellation', $data);
                return $id;
            }
        } else {
            $this->db->insert('hotel_cancellation', $data);
            return $this->db->insert_id();
        }
        return;
    }

    /**
     * string addHotalRenovationSchedule() method insert renovation schedule
     * @param varchar $data The renovation array
     * @todo  it add the hotel renovation schedule data into the database.
     * @return id
     */
    function addHotalRenovationSchedule($data) {
        if ($this->db->insert_batch('hotel_renovation_schedule', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getHotalRenovationSchedule() method insert renovation schedule
     * @param int $hotel_id hotel id
     * @todo it will fetched the all list of hotel renovation schedule based on hotel id.
     * @return array
     */
    function getHotalRenovationSchedule($hotel_id) {
        $this->db->select('rnv_id,date_from,date_to,renovation_type,area_effected');
        $this->db->where("hotel_id", $hotel_id);
        $this->db->order_by("date_from", "desc");
        $query = $this->db->get('hotel_renovation_schedule');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string deleteHotalRenovationSchedule() method delete the renovation id
     * @param int $arrayIds The renovation ids
     * @param it delete the hotel renovation schedule data from the DB.
     * @return true
     */
    function deleteHotalRenovationSchedule($arrayIds, $hid) {
        if (!empty($arrayIds) && count($arrayIds) > 0) {
            $this->db->where('hotel_id', $hid);
            $this->db->where_not_in('rnv_id', $arrayIds);
            $this->db->delete('hotel_renovation_schedule');
        }
        return true;
    }

    /**
     * strign updateHotalRenovationSchedule() method update renovation schedule
     * @param int $id The hotel renovation id
     * @param varchar $data The renovation record
     * @todo it update the hotel renovation schedule data from the database using id.
     * @return id
     */
    function updateHotalRenovationSchedule($data, $id = '') {
        if ($id != "") {
            $this->db->where('rnv_id', $id);
            $q = $this->db->get('hotel_renovation_schedule');
            if ($q->num_rows() > 0) {
                $this->db->where('rnv_id', $id);
                $this->db->update('hotel_renovation_schedule', $data);
                return $id;
            } else {
                $this->db->insert('hotel_renovation_schedule', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('hotel_renovation_schedule', $data);
            return $this->db->insert_id();
        }
        return;
    }

    /**
     * string addHotalDistanceFrom() method insert distance
     * @param varchar $data The distance record
     * @todo it add the hotel distance from into the DB.
     * @return id
     */
    function addHotalDistanceFrom($data) {
        if ($this->db->insert_batch('hotel_distance', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string getHotalDistanceFrom() method get the distance
     * @param varchar $dtype The distance type
     * @param int $hotel_id The hotel id
     * @todo it fetched the list of hotel distance from the database using hotel id and its type.
     * @return id
     */
    function getHotalDistanceFrom($hotel_id, $dtype = '') {
        $this->db->select('id,distance_from,distance,distance_type');
        $this->db->where("hotel_id", $hotel_id);
        if ($dtype != '') {
            $this->db->where("distance_type", $dtype); /* dtype 1 for  normal, and 2 for airport */
        }
        $query = $this->db->get('hotel_distance');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string addHotalFacilities() method insert facilities
     * @param varchar $data facility array
     * @todo it add the data of hotel facility in batch(multiple) form.
     * @return ture
     */
    function addHotalFacilities($data) {
        if ($this->db->insert_batch('hotel_facilities', $data)) {
            return true;
        }
        return false;
    }


    /**
     * string addHotalRoomsPricingData() method insert hotel pricing record
     * @param varchar $data The hotel pricing record
     * @todo it add hotel room pricing data into the DB.
     * @return id|false
     */
    function addHotalRoomsPricingData($data) {
        if ($this->db->insert('hotel_rooms_pricing', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * string addHotalRoomPricingComplimentary() 
     * @param varchar $data The hotel pricing record
     * @param id $pricing_id The hotel pricing id
     * @todo method insert hotel room pricing complimentary record
     * @return true
     */
    function addHotalRoomPricingComplimentary($data, $pricing_id = null) {
        if ($pricing_id != "") {
            $this->db->where('pricing_id', $pricing_id);
            $this->db->delete('hotel_rooms_pricing_complimentary');
        }
        if ($this->db->insert_batch('hotel_rooms_pricing_complimentary', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string updateHotalRoomsPricingData() method update the hotel room pricing record
     * @param int $pricing_id room pricing id
     * @param varchar $data The room pricing record
     * @todo it update the hotel room pricing data into the db using pricing id.
     * @return id
     */
    function updateHotalRoomsPricingData($data, $pricing_id) {
        if ($pricing_id != "") {
            $this->db->where('pricing_id', $pricing_id);
            $q = $this->db->get('hotel_rooms_pricing');
            if ($q->num_rows() > 0) {
                $this->db->where('pricing_id', $pricing_id);
                $this->db->update('hotel_rooms_pricing', $data);
                return $pricing_id;
            } else {
                $this->db->insert('hotel_rooms_pricing', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('hotel_rooms_pricing', $data);
            return $this->db->insert_id();
        }
        return;
    }

    /**
     * string deleteHotalRoomsPricingData() method delete room price
     * @param array $arrayIds room price id
     * @param int $hid hotel id
     * @todo int $hid hotel id
     * @return true
     */
    function deleteHotalRoomsPricingData($arrayIds, $hid) {
        if (!empty($arrayIds) && count($arrayIds) > 0) {
            $this->db->where('hotel_id', $hid);
            $this->db->where_not_in('pricing_id', $arrayIds);
            $this->db->delete('hotel_rooms_pricing');
        }
        return true;
    }

    /**
     * string addHotalRoomsPricingData() method insert hotel pricing record
     * @param varchar $data The hotel pricing record
     * @todo it add hotel room pricing data into the DB.
     * @return id|false
     */
    function addHotelRroomsPricingDetails($data) {
        if ($this->db->insert_batch('hotel_rooms_pricing_details', $data)) {
            return true;
        }
        return false;
    }

    /**
     * string updateHotelRroomsPricingDetails() 
     * @param varchar $data,(int)$pricingDetId,(int)$pricing_id
     * @todo it update the hotel room pricing data based on pricing id and its id.
     * @return id|false
     */
    function updateHotelRroomsPricingDetails($data, $pricingDetId, $pricing_id) {
        if ($pricing_id != "" && $pricingDetId != '') {
            $this->db->where('pricing_id', $pricing_id);
            $this->db->where('id', $pricingDetId);
            $q = $this->db->get('hotel_rooms_pricing_details');
            if ($q->num_rows() > 0) {
                $this->db->where('pricing_id', $pricing_id);
                $this->db->where('id', $pricingDetId);
                $this->db->update('hotel_rooms_pricing_details', $data);
                return $pricing_id;
            } else {
                $this->db->insert('hotel_rooms_pricing_details', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('hotel_rooms_pricing_details', $data);
            return $this->db->insert_id();
        }
        return;
    }
    /**
     * string deleteHotalHotelRroomsPricingDetails()
     * @param array $arrayIds,(int) $pricing_id The hotel pricing record
     * @todo method to delete hotel room pricing data from the DB.
     * @return id|false
     */
    function deleteHotalHotelRroomsPricingDetails($arrayIds, $pricing_id) {
        if (!empty($arrayIds) && count($arrayIds) > 0) {
            $this->db->where('pricing_id', $pricing_id);
            $this->db->where_not_in('id', $arrayIds);
            $this->db->delete('hotel_rooms_pricing_details');
        }
        return true;
    }
    /**
     * string getHotalRoomsPricingData()
     * @param varchar $data The hotel pricing record
     * @todo  method to fetched the data hotel room pricing from the DB.
     * @return id|false
     */
    function getHotalRoomsPricingData($hotel_id) {
        $this->db->select('pricing_id,room_type,inclusions,
		curency_code,max_adult,max_child,inventory,period_from,period_to');
        $this->db->where("hotel_id", $hotel_id);
        $query = $this->db->get('hotel_rooms_pricing');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    /**
     * string getHotelRroomsPricingDetails()
     * @param (int)$pricing_id hotel pricing id.
     * @todo  it fetched the list form the database of hotel room pricing using pricing id.
     * @return id|false
     */
    function getHotelRroomsPricingDetails($pricing_id) {
        $this->db->select('id,market_id,double_price,
		triple_price,quad_price,breakfast_price,half_board_price,
		all_incusive_adult_price,extra_adult_price,extra_child_price,extra_bed_price');
        $this->db->where("pricing_id", $pricing_id);
        $query = $this->db->get('hotel_rooms_pricing_details');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    /**
     * string getHotalRoomPricingComplimentary() 
     * @param varchar $data The hotel pricing record
     * @todo it fetched the list form the database of hotel room pricing complimentry using pricing id.
     * @return id|false
     */
    function getHotalRoomPricingComplimentary($pricing_id) {
        $this->db->select('cmpl_service_id');
        $this->db->where("pricing_id", $pricing_id);
        $query = $this->db->get('hotel_rooms_pricing_complimentary');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * string get_gallary() 
     * @todo it fetched the list form the database of hotel gallery using pricing id.
     * @return id|false
     */

    function get_gallary() {
        $this->db->select('id,entity_title');
        $this->db->order_by('id', 'ASC');
        $this->db->where('entity_type', $this->config->item('attribute_hotel_gallery'));
        $query = $this->db->get('entity_attributes');
        return $result = $query->result();
    }
     /**
     * string insert_images()
     * @param array $data 
     * @todo it add hotel gallery images data into the DB.
     * @return id|false
     */
    function insert_images($data) {
        $id = $this->db->insert('gallery_images', $data);
        return $id;
    }
    /**
     * string update_images()
     * @param array $data (int) $identity
     * @todo it update hotel gallery images data into the DB using perticular identity id.
     * @return id|false
     */
    function update_images($identity, $data) {
        $this->db->where('identifier', $identity);
        $this->db->update('gallery_images', $data);
    }
    /**
     * string get_dbidentity() 
     * @param (int)$identity
     * @todo it fetched the list form the database of hotel room gallery images  using identity.
     * @return true|false
     */
    function get_dbidentity($identity) {
        $this->db->where('identifier', $identity);
        $query = $this->db->get('gallery_images');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    /**
     * string clear_gallery_image_table() 
     * @todo it clear all images whose id is null and removed images 1 day befor form the upload dates.
     * @return true|false
     */
    function clear_gallery_image_table() {

        $this->db->query("DELETE FROM `gallery_images` where `upload_date` < DATE_SUB(NOW() , INTERVAL 24 HOUR) and hotel_id IS NULL");
    }
    /**
     * string delete_images() 
     * @param (int)$galid,string($imgname)
     * @todo it delete gallery images from the db using galler id or image name.
     * @return true|false
     */
    function delete_images($imgname, $galid) {

        $this->db->query("delete from gallery_images where image_name = '" . $imgname . "' and gallery_id = '" . $galid . "'");
    }
    /**
     * string gallery_detail() 
     * @todo it fetched the list form the database of hotel gallery.
     * @return true|false
     */
    function gallery_detail() {
        $this->db->select('id,entity_title');
        $this->db->from('entity_attributes');
        $this->db->where("entity_type", $this->config->item('attribute_hotel_gallery'));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    /**
     * string get_gallaryimg() 
     * @param int($hid) 
     * @todo it fetched the list form the database of gallery imagges using hotel id.
     * @return id|false
     */
    function get_gallaryimg($hid) {
        $this->db->select('gi.gallery_id, gi.id, gi.image_name');
        $this->db->from('gallery_images gi');
        $this->db->where("gi.hotel_id", $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    /**
     * string deletehotelgalleryimg() 
     * @param int($imgid),int($hotelgalid),int($hotelid)
     * @todo it deleted hotel gallery images from the database from the DB.
     * @return id|false
     */
    function deletehotelgalleryimg($imgid, $hotelgalid, $hotelid) {
        $this->db->where('gallery_id', $hotelgalid);
        $this->db->where('id', $imgid);
        $this->db->where('hotel_id', $hotelid);
        $this->db->delete('gallery_images');
        return $imgid;
    }
    /**
     * string getHotelbyid() 
     * @param $hid(int)
     * @todo it fetched the list of hotel by its id.
     * @return id|false
     */
    function getHotelbyid($hid) {
        $this->db->select('hotels.hotel_id,
							hotels.hotel_name,
							hotels.hotel_address,
							hotels.hotel_code,
							countries.country_name,
							hotels.city,
							hotels.district,
							hotels.currency,
							hotels.post_code,
							hotels.dorak_rating,
							hotels.star_rating,
							hotels.child_age_group_id,
							hotels.purpose,
							hotels.last_updated,
							hotels.country_code,
							hotels.chain_id as chain,
							hotels.status,
							
							hpt.property_type_id as property_types');
        $this->db->from('hotels');
        $this->db->join('countries', 'hotels.country_code = countries.country_code');
        $this->db->join('hotel_property_types  hpt', 'hotels.hotel_id = hpt.hotel_id', 'left');
        $this->db->where('hotels.hotel_id', $hid);
        $query = $this->db->get();
        $results = $query->row();
        if ($results) {
            return $results;
        } return false;
    }
    /**
     * string deleteHotalpropertytype()
     * @param int $hid
     * @todo method to delete hotel room property type data from the DB.
     * @return id|false
     */
    function deleteHotalpropertytype($hid) {
        $this->db->where('hotel_id', $hid);
        $this->db->delete('hotel_property_types');
        return true;
    }
    /**
     * string getpropertyid() 
     * @param int $hid
     * @todo it fetched the list form the database of property using pricing id.
     * @return id|false
     */
    function getpropertyid($hid) {

        $this->db->select('hpt.property_type_id');
        $this->db->from('hotel_property_types hpt');
        $this->db->where('hpt.hotel_id', $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $re) {
                return $ids = array_column($result, 'property_type_id');
            }
        } else {
            return false;
        }
    }

    /**
     * string getcontactinfobyid() 
     * @param int $hid hotel id
     * @todo it fetched the list form the database of hotel contact information using pricing id.
     * @return id|false
     */
    function getcontactinfobyid($hid) {
        $this->db->select('hcd.position as designation, hcd.name, hcd.email, hcd.phone, hcd.extension, hcd.contact_id');
        $this->db->from('hotel_contact_details hcd');
        //$this->db->join('entity_attributes as p','p.id = hcd.position AND entity_type="ahpo"');
        $this->db->where('hcd.hotel_id', $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $result = $query->result();
        } else {
            return false;
        }
    }
    /**
     * string updateHotalContact()
     * @param $data array format
     * @todo it update hotel contact data from the database.
     * @return id|false
     */
    function updateHotalContact($data, $id = '') {
        if ($id != "") {
            $this->db->where('contact_id', $id);
            $q = $this->db->get('hotel_contact_details');
            if ($q->num_rows() > 0) {
                $this->db->where('contact_id', $id);
                $this->db->update('hotel_contact_details', $data);
                return $id;
            } else {
                $this->db->insert('hotel_contact_details', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('hotel_contact_details', $data);
            return $this->db->insert_id();
        }
        return;
    }
    /**
     * string deleteHotalContact()
     * @param $data(array),$hid(int)
     * @todo it delete hotel contact data from the database.
     * @return id|false
     */
    function deleteHotalContact($arrayIds, $hid) {
        $this->db->where('hotel_id', $hid);
        $this->db->where_not_in('contact_id', $arrayIds);
        $this->db->delete('hotel_contact_details');
        return true;
    }
    /**
     * string getdistanceForCity()
     * @param $hid(int)
     * @todo it fetched the list from the database of get distance for city using hotel id.
     * @return id|false
     */
    function getdistanceForCity($hid) {

        $this->db->select('hd.distance_from as city_name_normal, hd.distance as dis_city, hd.id');
        $this->db->from('hotel_distance hd');
        $this->db->where('hd.distance_type', 1);
        $this->db->where('hd.hotel_id', $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $result = $query->result();
        } else {
            return false;
        }
    }
    /**
     * string updateHotalcity()
     * @param $hid(int),array($data)
     * @todo it update the hotel city into the database using id 
     * @return id|false
     */
    function updateHotalcity($data, $id = '') {
        if ($id != "") {
            $this->db->where('id', $id);
            $q = $this->db->get('hotel_distance');
            if ($q->num_rows() > 0) {
                $this->db->where('id', $id);
                $this->db->where('distance_type', 1);
                $this->db->update('hotel_distance', $data);
                return $id;
            } else {
                $this->db->insert('hotel_distance', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('hotel_distance', $data);
            return $this->db->insert_id();
        }
        return;
    }
    /**
     * string deleteHotalcity()
     * @param $hid(int),array($arrayIds)
     * @todo it delete hotel city using hotel id
     * @return id|false
     */
    function deleteHotalcity($arrayIds, $hid) {
        $this->db->where('hotel_id', $hid);
        $this->db->where('distance_type', '1');
        $this->db->where_not_in('id', $arrayIds);
        $this->db->delete('hotel_distance');
        return true;
    }
     /**
     * string getdistanceForAirport()
     * @param $hid(int)
     * @todo it fetched the list of distance for airport from the DB.
     * @return associative data row.
     */
    function getdistanceForAirport($hid) {

        $this->db->select('hd.distance_from as city_name_airport, hd.distance as dis_airport, hd.id');
        $this->db->from('hotel_distance hd');
        $this->db->where('hd.distance_type', 2);
        $this->db->where('hd.hotel_id', $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $result = $query->result();
        } else {
            return false;
        }
    }
    /**
     * string updatedistanceForAirport() method insert hotel pricing record
     * @param array $data,int $id.
     * @todo it update hotel distance into the DB.
     * @return last inserted id
     */
    function updatedistanceForAirport($data, $id = '') {
        if ($id != "") {
            $this->db->where('id', $id);
            $q = $this->db->get('hotel_distance');
            if ($q->num_rows() > 0) {
                $this->db->where('id', $id);
                $this->db->where('distance_type', '2');
                $this->db->update('hotel_distance', $data);
                return $id;
            } else {
                $this->db->insert('hotel_distance', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('hotel_distance', $data);
            return $this->db->insert_id();
        }
        return;
    }
    /**
     * string deletedistanceForAirport()
     * @param array $arrayIds,int $hid.
     * @todo it delete hotel distance for airport from the DB.
     * @return true or false
     */
    function deletedistanceForAirport($arrayIds, $hid) {
        $this->db->where('hotel_id', $hid);
        $this->db->where('distance_type', '2');
        $this->db->where_not_in('id', $arrayIds);
        $this->db->delete('hotel_distance');
        return true;
    }
    /**
     * string hotelFacility()
     * @param int $hid.
     * @todo it fetched the list form the database of hotel room facility using pricing id.
     * @return list of hotel facility.
     */
    function hotelFacility($hid) {
        $this->db->select('*');
        $this->db->from('hotel_facilities');
        $this->db->where('hotel_id', $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            //echo $this->db->last_query(); die;
            foreach ($result as $re) {
                return $ids = array_column($result, 'facility_id');
            }
        } else {
            return false;
        }
    }
    /**
     * string deletehotelFacility()
     * @param int $hid.
     * @todo it deleted hotel facility into the DB based on hotel id.
     * @return true or false.
     */
    function deletehotelFacility($hid) {
        $this->db->where('hotel_id', $hid);
        $this->db->delete('hotel_facilities');
        return true;
    }

    /**
     * string roomFacility()
     * @param int $hid.
     * @todo it deleted hotel facility into the DB based on hotel id.
     * @return true or false.
     */
    function roomFacility($hid) {
        $this->db->select('hcs.cmpl_service_id');
        $this->db->from('hotel_complimentary_services hcs');
        $this->db->where('hcs.hotel_id', $hid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $re) {
                return $ids = array_column($result, 'cmpl_service_id');
            }
        } else {
            return false;
        }
    }
    /**
     * string deleteroomFacility()
     * @param int $hid.
     * @todo it deleted hotel room facility into the DB based on hotel id.
     * @return true or false.
     */
    function deleteroomFacility($hid) {
        $this->db->where('hotel_id', $hid);
        $this->db->delete('hotel_complimentary_services');
        return true;
    }
    /**
     * string hotelBankAccount()
     * @param int $hid.
     * @todo it fetched hotel bank accounts into the DB based on hotel id.
     * @return true or false.
     */
    function hotelBankAccount($hid) {
        $this->db->select('*');
        $this->db->from('hotel_bank_accounts hba');
        $this->db->where('hba.hotel_id', $hid);
        $query = $this->db->get();
        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            return false;
        }
    }
    /**
     * string updateHotalbankaccounts()
     * @param int $hid.
     * @todo it update hotel bank accounts into the DB based on hotel id.
     * @return true or false.
     */
    function updateHotalbankaccounts($data, $id = '') {

        if ($id != "") {
            $this->db->where('id', $id);
            $q = $this->db->get('hotel_bank_accounts');
            if ($q->num_rows() > 0) {
                $this->db->where('id', $id);
                $this->db->update('hotel_bank_accounts', $data);
                return $id;
            } else {
                $this->db->insert('hotel_bank_accounts', $data);
                return $this->db->insert_id();
            }
        } else {
            $this->db->insert('hotel_bank_accounts', $data);
            return $this->db->insert_id();
        }
        return;
    }
    /**
     * string deleteHotalbankaccounts()
     * @param int $hid.
     * @todo it delete(multiple) hotel bank accounts into the DB based on hotel id.
     * @return true or false.
     */
    function deleteHotalbankaccounts($arrayIds = false, $hid) {
        $this->db->where('hotel_id', $hid);
        if ($arrayIds)
            $this->db->where_not_in('id', $arrayIds);
        $this->db->delete('hotel_bank_accounts');
        return true;
    }
    /**
     * string deleteHotalbankaccount()
     * @param int $hid.
     * @todo it delete hotel bank accounts into the DB based on hotel id.
     * @return true or false.
     */
    function deleteHotalbankaccount($acId, $hid) {
        $this->db->where('hotel_id', $hid);
        if ($acId != "")
            $this->db->where('id', $acId);
        $this->db->delete('hotel_bank_accounts');
        return true;
    }
    /**
     * string updateHotelInformation()
     * @param int $hid.
     * @todo it update the hotel all informatin based on hotel id to associative data.
     * @return true or false.
     */
    function updateHotelInformation($hid, $hotel_detail) {
        $this->db->where('hotel_id', $hid);
        $this->db->update('hotels', $hotel_detail);
    }
}
