<?php

/**
 * @author tis
 * @author M
 * @description this controller  is for  add/ edit delete hotel  related  information
 * @uses AdminController::Common base controller to perform all type of action.
 * functions : 
  
  _init()            this method is used to load all libraries and model.
  index()            this method is used to list hotel listing.
  hotelListingAjax()        this method is used to fetch hotel list from via ajax.
  delete_hotel()            this method is used to delete hotel from database.
  massActions()  	    this method is used to delete hotel in bulk from the database .
  minDist()          this is callback meathod to validate minimum distance.
  maxDist()         this is callback meathod to validate minimum distance.
  information()       this method is used to add hotel information with all attributes like facility banking price etc.
  upload ()          this method is used to upload gallery image.
  deleteimg()       this method is used to delete gallery image.
  edit_information()     this method is used to update hotel information based on perticular hotel id.
  deletehotelimg()     this method is used to delete hotel gallery image in the bulk format.
  uploadGaleryImage()  this method is used to upload gallery image zip or normat browse.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hotels extends AdminController {
    
    /** @var string|null Should contain a user identification */

    public static $identityfier;

    function __construct() {
        parent::__construct();
        $this->_init();
    }

     /*
     * @method string init()
     * @param N/A
     * @todo Loads all required models(database tables),helpers(views),libraries(third party eg:phpexcel)
     * @return nothing 
     */
    private function _init() {
        if (!$this->ion_auth->logged_in()) {
            // redirect non logged user to  login page
            redirect('auth/login', 'refresh');
        }
        $this->identityfier = $this->session->userdata('session_id') . '_' . time();
        $this->load->helper(array('form', 'html', 'form_field_values', 'file', 'directory'));
        $this->load->library('form_validation');
        $this->load->helper('array_column');
        $this->load->library('Datatables');
        $this->output->set_meta('description', 'Dorak Hotels');
        $this->output->set_meta('title', 'Dorak Hotels');
        $this->output->set_title('Dorak Hotels');
        $this->load->model('hotels/hotel_model');
        $this->accessDenidMessage = $this->config->item('access_not_allowed_message');
        $this->contractFileDir = $this->config->item('upload_contract_file_dir');
        $this->hotelFileDir = $this->config->item('hotel_gallery_dir');
        $this->hotel_model->clear_gallery_image_table();
        if (!checkAccess($this->accessLabelId, 'hotels', 'view')) {
            $sdata['message'] = $this->accessDenidMessage;
            $flashdata = array('flashdata' => $sdata['message'],'message_type' => 'notice');
            $this->session->set_userdata($flashdata);
            redirect('users', 'refresh');
        }
    }

     /**
     * string index() used to fetch hotel listing and search hotel sepecific condition.
     * @todo it will load all the hotels listing and search hotel by condition.
     * @return hotel listing.
     */
    
    public function index() {
        $this->session->unset_userdata('stepsess');
        $this->output->set_meta('title', 'Hotel Listing');
        $this->output->set_title('Hotel Listing');
        $search_terms = array();

        if (!empty($_POST)) {
            // store  posted data in session for ajax pagination  and data tables  search results listing
            $hname = removeExtraspace($this->input->post('hotel_name'));
            $hcode = removeExtraspace($this->input->post('hotel_code'));
            $this->session->set_userdata('searchByHotelName', $hname);
            $this->session->set_userdata('searchByHotelCode', $hcode);
            $this->session->set_userdata('searchByStarRating', $this->input->post('star_rating'));
            $this->session->set_userdata('searchByPropertyType', $this->input->post('property_type'));
            $this->session->set_userdata('searchByStatus', $this->input->post('status_id'));
            $this->session->set_userdata('searchByPurpose', $this->input->post('purpose'));
            $this->session->set_userdata('searchByChainId', $this->input->post('hotel_chain_id'));
            $this->session->set_userdata('searchByCountry', $this->input->post('country_code'));
            $this->session->set_userdata('searchByCity', $this->input->post('city'));
            $this->session->set_userdata('searchByDistrict', $this->input->post('district'));
            $search_terms['searchByHotelName'] = $hname;$search_terms['searchByHotelCode'] = $hcode;$search_terms['searchByStarRating'] = $this->input->post('star_rating');$search_terms['searchByPropertyType'] = $this->input->post('property_type');$search_terms['searchByStatus'] = $this->input->post('status_id');$search_terms['searchByPurpose'] = $this->input->post('purpose');$search_terms['searchByChainId'] = $this->input->post('hotel_chain_id');
            $search_terms['searchByCountry'] = $this->input->post('country_code');$search_terms['searchByCity'] = $this->input->post('city');$search_terms['searchByDistrict'] = $this->input->post('district');
        } else {
            $this->session->unset_userdata('searchByHotelName');$this->session->unset_userdata('searchByHotelCode');$this->session->unset_userdata('searchByStarRating');
            $this->session->unset_userdata('searchByPropertyType');$this->session->unset_userdata('searchByStatus');
            $this->session->unset_userdata('searchByPurpose');$this->session->unset_userdata('searchByChainId');$this->session->unset_userdata('searchByCountry');
            $this->session->unset_userdata('searchByCity');$this->session->unset_userdata('searchByDistrict');
        }
        $countriesOptions = getCountriesOptions('sel'); /* using helper with select option */
        $dc = $this->config->item('default_country_code');
        $ccode = $this->input->post('country_code') != '' ? $this->input->post('country_code') : '';
        if ($ccode != "") {
            $citiesOptions = getCountyCitiesOptions($ccode, 'sel');
        } else {
            $citiesOptions = array('' => 'None');
        }
        $scity = $this->input->post('city') != '' ? $this->input->post('city') : '';
        if ($scity != "") {
            $districtOptions = getCityDistrictOptions($scity, 'sel');
        } else {
            $districtOptions = array('' => 'Select');
        }
        $this->data = array(
        'importAction' => site_url('hotels/import/process'),
        'hotel_name' => array('id' => 'hotel_name','name' => 'hotel_name','maxlength' => '100','size' => '100','value' => $this->input->post('hotel_name')),
        'hotel_code' => array('id' => 'hotel_code','name' => 'hotel_code','maxlength' => '100','size' => '100','value' => $this->input->post('hotel_code')),
        'district' => array('id' => 'district','tabindex' => 12,'data-validation' => 'custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "3-80",'data-validation-optional' => 'true','name' => 'district','maxlength' => '100','size' => '100',
        'value' => set_value('district')),'star_rating_options' => getStarRatings(),'status_options' => getHotelStatus(),'purpose_options' => getPurposeOptions(),'chain_options' => getHotelChainsOptions(),'countriesOptions' => $countriesOptions,'citiesOptions' => $citiesOptions,'districtOptions' => $districtOptions,'property_type_options' => getPropertyTypeOptions(),);
        $this->data['p_country_code'] = $this->input->post('country_code') != '' ? $this->input->post('country_code') : '';
        $this->data['posted_city'] = $this->input->post('city') != '' ? $this->input->post('city') : '';
        $this->data['posted_district'] = $this->input->post('district') != '' ? $this->input->post('district') : '';
        $this->data['serched_chain_id'] = $this->input->post('hotel_chain_id') != '' ? $this->input->post('hotel_chain_id') : '';
        $this->data['serched_star_rating'] = $this->input->post('star_rating') != '' ? $this->input->post('star_rating') : '';
        $this->data['serched_property_type'] = $this->input->post('property_type') != '' ? $this->input->post('property_type') : '';
        $this->data['serched_status'] = $this->input->post('status_id') != '' ? $this->input->post('status_id') : '';
        $this->data['serched_purpose'] = $this->input->post('purpose') != '' ? $this->input->post('purpose') : '';
        $limit = 10;
        $offset = 0;
        $this->data['hotels'] = $this->hotel_model->getHotelsList($search_terms, $limit, $offset);
        $this->load->view('hotel_listing', $this->data);
    }

    /**
     * @method string hotelListingAjax()
     * @method use datatable to view the list of hotels called by ajax	
     * @return datatable library generated list of hotels.
     */
    function hotelListingAjax() {
        $actionlinks = '';
        if (checkAccess($this->accessLabelId, 'hotels', 'edit'))
            $actionlinks.='<a class="edit_rec" title="Edit" href="hotels/edit_information/$1">Edit</a> ';
        if (checkAccess($this->accessLabelId, 'hotels', 'delete'))
            $actionlinks.='<a title="Delete" class="delete_rec" href="' . base_url() . 'hotels/delete_hotel/$1" onclick="return confirm(\'Are you sure to delete this hotel?\')">Delete</a>';
        if (trim($actionlinks) == "") {
            $actionlinks = 'No action permitted';
        }
        $this->datatables->select('
                                            hotels.hotel_id,
                                            hotels.hotel_code,
                                            hotels.hotel_name,	
                                            cn.entity_title as chain,
                                            hotels.star_rating,	
                                            hotels.dorak_rating,
                                            hotels.city,							
                                            countries.country_name,
                                            ea.entity_title as status,
                                            eap.entity_title as property_types,	
                                            eaps.entity_title as purpose
							', False)
                ->add_column('Actions', $actionlinks, 'hotels.hotel_id')
                ->edit_column('hotels.star_rating', '<span class="starRating"><span id="srating$1">$1</span></span>', 'hotels.star_rating')
                ->edit_column('hotels.dorak_rating', '<span class="dRating"><span id="drating$1">$1</span></span>', 'hotels.dorak_rating')
                ->edit_column('property_types', '$1', 'get_assigned_property_types(hotels.hotel_id)')
                ->join('entity_attributes ea', 'hotels.status = ea.id AND ea.entity_type="ahs"')
                ->join('countries', 'hotels.country_code = countries.country_code')
                ->join('entity_attributes cn', 'cn.id = hotels.chain_id AND cn.entity_type="ahc"', 'left')
                ->join('entity_attributes eaps', 'eaps.id = hotels.purpose', 'left')
                ->join('hotel_property_types  hpt', 'hotels.hotel_id = hpt.hotel_id', 'left')
                ->join('entity_attributes  eap', 'eap.id = hpt.property_type_id AND eap.entity_type="apt"', 'left')
                ->group_by('hotels.hotel_id');
        if ($this->session->userdata('searchByHotelName') && $this->session->userdata('searchByHotelName') != "") {
            $this->datatables->like('hotels.hotel_name', $this->session->userdata('searchByHotelName'), 'both');
        }
        if ($this->session->userdata('searchByHotelCode') && $this->session->userdata('searchByHotelCode') != "") {
            $this->datatables->like('hotels.hotel_code', $this->session->userdata('searchByHotelCode'), 'both');
        }
        if ($this->session->userdata('searchByCountry') && $this->session->userdata('searchByCountry') != "") {
            $this->datatables->where('hotels.country_code', $this->session->userdata('searchByCountry'));
        }
        if ($this->session->userdata('searchByCity') && $this->session->userdata('searchByCity') != "") {
            $this->datatables->where('hotels.city', $this->session->userdata('searchByCity'));
        }
        if ($this->session->userdata('searchByDistrict') && $this->session->userdata('searchByDistrict') != "") {
            $this->datatables->where('hotels.district', $this->session->userdata('searchByDistrict'));
        }
        if ($this->session->userdata('searchByPurpose') && $this->session->userdata('searchByPurpose') != "") {
            $this->datatables->where('hotels.purpose', $this->session->userdata('searchByPurpose'));
        }
        if ($this->session->userdata('searchByStarRating') != "") {
            $this->datatables->where('hotels.star_rating', $this->session->userdata('searchByStarRating'));
        }
        if ($this->session->userdata('searchByStatus') && $this->session->userdata('searchByStatus') != "") {
            $this->datatables->where('hotels.status', $this->session->userdata('searchByStatus'));
        }
        if ($this->session->userdata('searchByChainId') && $this->session->userdata('searchByChainId') != "") {
            $this->datatables->where('hotels.chain_id', $this->session->userdata('searchByChainId'));
        }
        if ($this->session->userdata('searchByPropertyType') && $this->session->userdata('searchByPropertyType') != "") {
            $this->datatables->where('eap.id', $this->session->userdata('searchByPropertyType'));
        }
        $this->datatables->from('hotels');
        echo $this->datatables->generate();
        exit();
    }

   /**
     * @method string delete_hotel()
     * @method use datatable to view the list of hotels called by ajax
    *  @param hotelId $hid - hotel id(primary key) used to delete hotel.
     * @return datatable library generated list of hotels.
     */
    function delete_hotel($hid = null) {
        if ($hid != "") {
            try {
                $this->hotel_model->deleteHotel($hid);
            } catch (Exception $e) {
                $sdata['message'] = 'Selected hotel not deleted!' . $e->getMessage();
                $flashdata = array('flashdata' => $sdata['message'],'message_type' => 'error');
                $this->session->set_userdata($flashdata);
                redirect('/hotels/', 'refresh');
            }
            $sdata['message'] = 'Selected hotel deleted!';
            $flashdata = array('flashdata' => $sdata['message'],'message_type' => 'sucess');
            $this->session->set_userdata($flashdata);
            redirect('/hotels/', 'refresh');
        }
        $this->index();
    }

    /**
     * @method string massActions()
     * @description delete hotels multiple hotels by ids
     * @return true|false
     */
    function massActions() {
        if (!empty($_POST)) {
            $sdata = array();
            $hotelIdsToDelete = $this->input->post('id');
            if ($hotelIdsToDelete && count($hotelIdsToDelete) > 0) {
                // delete all the  hotels  
                try {
                    $this->hotel_model->deleteHotels($hotelIdsToDelete);
                } catch (Exception $e) {
                    $sdata['message'] = 'Selected hotels not deleted!' . $e->getMessage();
                    $flashdata = array(
                        'flashdata' => $sdata['message'],
                        'message_type' => 'error'
                    );
                    $this->session->set_userdata($flashdata);
                    redirect('/hotels/', 'refresh');
                }
            } else {
                $sdata['message'] = 'Please select records  to delete!';
                $flashdata = array(
                    'flashdata' => $sdata['message'],
                    'message_type' => 'notice'
                );
                $this->session->set_userdata($flashdata);
            }
        }
        redirect('/hotels');
    }

    /**
     * @method string minDist()
     * @param $input(float) - distance data via post to validate.
     * @method validate minimum distance.
     * @return true|false
     */
    public function minDist($input) {
        # this function validate   decimal  number  for distance min limit
        if ($input != '') {
            $pieces = explode(".", $input);
            if (isset($pieces[0]) && $pieces[0] == '') {
                $this->form_validation->set_message('min_dist', 'The %s field should have at least one digit before decimal');
                return FALSE;
            }
            if (isset($pieces[1]) && $pieces[1] > 99) {
                $this->form_validation->set_message('min_dist', 'The %s field can not accept more then 2 digit after decimial');
                return FALSE;
            }

            if ($input < 0) {
                $this->form_validation->set_message('min_dist', 'The %s field invalid value');
                return FALSE;
            }
        }
        return TRUE;
    }
    /**
     * @method string maxDist()
     * @param $input(float) - distance data via post to validate.
     * @method validate maximum distance.
     * @return true|false
     */
    public function maxDist($input = '') {
        # this function validate   decimanl  number  for distance max limit
        if ($input != '') {
            $pieces = explode(".", $input);
            if (isset($pieces[0]) && $pieces[0] > 999) {
                $this->form_validation->set_message('max_dist', 'The %s field exceed the max limit');
                return FALSE;
            }
            if (isset($pieces[1]) && $pieces[1] > 99) {
                $this->form_validation->set_message('max_dist', 'The %s field can not accept more then 2 digit after decimial');
                return FALSE;
            }
            if ($input < 0) {
                $this->form_validation->set_message('max_dist', 'The %s field invalid value');
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * @method: string information()
     * @todo: Add hotel details  in database for all tabs like info,facilities,banking,contract.
     * @return : NULL
     */
    public function information() {
        if (!checkAccess($this->accessLabelId, 'hotels', 'add')) { // check if user have access to this section
            $sdata['message'] = $this->accessDenidMessage;
            $flashdata = array(
                'flashdata' => $sdata['message'],
                'message_type' => 'notice'
            );
            $this->session->set_userdata($flashdata);
            redirect('users', 'refresh');
        }

        $data = array('message' => '');
        $this->load->library('upload');
        $this->output->set_meta('description', 'Hotel Information');
        $this->output->set_meta('title', 'Hotel Information');
        $this->output->set_title('Hotel Information');
        $user = $this->ion_auth->user()->row();
        if ($this->session->userdata('stepsess') == '#tab-5') {
            $this->session->set_userdata('stepsess', '#tab-6');
        }
        if (!empty($_POST)) {
            $identity = $this->input->post('identity');

            $step = $this->input->post('step'); /* input for save data on tab basis */
            $this->session->set_userdata('stepsess',  $step); /* session start for tabs */

            ######################### server sides validation rules ################################ 
            
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('hotel_name', 'Hotel name', 'required|min_length[10]|max_length[100]');
            $this->form_validation->set_rules('hotel_address', 'Address', 'required|min_length[15]|max_length[150]');
            $this->form_validation->set_rules('post_code', 'Zip Code', 'required|alpha_numeric|min_length[4]|max_length[8]');
            $this->form_validation->set_rules('currency', 'Currency', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('country_code', 'Country', 'required');
            $this->form_validation->set_rules('distance_from_city[1][distance]', 'Distance From City', 'callback_min_dist|callback_max_dist');
            $this->form_validation->set_rules('distance_from_airport[1][distance]', 'Distance From Airport', 'callback_minDist|callback_maxDist');
            $this->form_validation->set_rules('property_type_oth', 'property_type_oth', 'min_length[3]');
            $this->form_validation->set_rules('purpose_options_oth', 'purpose_options_oth', 'min_length[3]');
            $this->form_validation->set_rules('hotel_chain_oth', 'hotel_chain_oth', 'min_length[5]');
            $this->form_validation->set_rules('contact[1][position]', 'Contact position', 'required');
            $this->form_validation->set_rules('contact[1][name]', 'Contact Name', 'required');
            $this->form_validation->set_rules('contact[1][email]', 'Contact Email', 'required|valid_email');
            $this->form_validation->set_rules('contact[1][phone]', 'Contact Phone', 'required');
            
            ###################################End validation rules###########################################
            
            /**********************************If all validation has successed****************/
            if ($this->form_validation->run($this) != FALSE) {
                $purpose = $this->input->post('purpose');
                if ($purpose != '' && $purpose == 'Other') {
                    // insert new purpose
                    $purpose_options_oth = $this->input->post('purpose_options_oth');
                    if ($purpose_options_oth != '') {
                        // check if already in database if not then add
                        if ($pid = $this->hotel_model->getPurpose($purpose_options_oth)) {
                            $purposeId = $pid;
                        } else { // add data in database 
                            $hotel_purpose = array('title' => $purpose_options_oth);
                            $purposeId = $this->hotel_model->addPurpose($hotel_purpose);
                        }
                    }
                } else {
                    $purposeId = $purpose;
                }
                // add new hotel chain if  user choose  other options the specified  value need to store int  hotel chain list table
                $hotelchain = $this->input->post('chain_id');
                if ($hotelchain != '' && $hotelchain == 'Other') {
                    // insert new purpose
                    $hotel_chain_oth = $this->input->post('hotel_chain_oth');
                    if ($hotel_chain_oth != '') {
                        /* check if already in database if not then add */
                        if ($hcid = $this->hotel_model->getHotelChain($hotel_chain_oth)) {
                            $chainId = $hcid;
                        } else {
                            /* add data in database  */
                            $hotel_chain = array('entity_title' => $hotel_chain_oth, 'entity_type' => $this->config->item('attribute_hotel_chain'));
                            $chainId = $this->hotel_model->addHotelChain($hotel_chain);
                        }
                    }
                } else {
                    $chainId = $hotelchain;
                }
                $contactData = array();  /* # we'll store each contact row in this array */
                $nearestDistanceData = array();  /* we'll store each distance row in this array */
                $hotelBankData = array();  /* we'll store each bank details row in this array */
                $hotelRenovationData = array();  /* we'll store each bank renovation row in this array */
                /* Setup a counter for looping through post arrays */
                $bnk_details = $this->input->post('bank');
                $hotelContactDetails = $this->input->post('contact');
                $aplicable_child_age_group = $this->input->post('aplicable_child_age_group');
                /* Setting values for  hotel tabel fields */
                $hotel_data = array('hotel_name' => $this->input->post('hotel_name'),'hotel_address' => $this->input->post('hotel_address'),'chain_id' => $chainId,'purpose' => $purposeId,'city' => $this->input->post('city'),'country_code' => $this->input->post('country_code'),'district' => $this->input->post('district'),'post_code' => $this->input->post('post_code'),'currency' => $this->input->post('currency'),'star_rating' => $this->input->post('star_rating'),'dorak_rating' => $this->input->post('dorak_rating'),'commisioned' => $this->input->post('commisioned'),'status' => $this->input->post('status'),'child_age_group_id' => $aplicable_child_age_group,'created_byuser' => $user->id);
                /* check if hotel details already added on the basis of name city and postcode */
                if ($this->hotel_model->getHotelDetails($hotel_data['hotel_name'], $hotel_data['city'], $hotel_data['post_code'])) {
                    $data['message'] = 'Hotel is already exists!';
                    $flashdata = array('flashdata' => $data['message'],'message_type' => 'notice');
                    $this->session->set_userdata($flashdata);
                } else {
                    /* if hotel details are not already added before then  add  new hotel in database,Transfering data to Model */
                    if ($inserted_hotel_id = $this->hotel_model->insertDetails($hotel_data)) {
                        $data['message'] = '<li>Hotel Information</li>';
                        // sets hotel code  as per inserted  id and prefic  defiened in config
                        //Format of the Hotel Code: DRK-_<Auto generated sequence number>-<First Four Characters of the hotel name>-<3 character City Code>-<3 character Country Code>
                        $hCodePrefix = $this->config->item('hotel_code_prefix');
                        $first4CharOfName = substr($hotel_data['hotel_name'], 0, 4);
                        $first3CharOfCity = substr($hotel_data['city'], 0, 3);
                        $hotelCountryCode = $hotel_data['country_code'];
                        $hotelCityCode = getCityCode($hotel_data['city']); // using helper				
                        $hcode = $hCodePrefix . $inserted_hotel_id . "-" . $first4CharOfName;
                        if ($hotelCityCode && $hotelCityCode != "") {
                            $hcode.="-" . $hotelCityCode;
                        } else {
                            $hcode.="-" . $first3CharOfCity;
                        }
                        if ($hotelCountryCode && $hotelCountryCode != "")
                            $hcode.="-" . $hotelCountryCode;
                        $uhData = array('hotel_code' => $hcode);
                        $this->hotel_model->updateHotelDetails($inserted_hotel_id, $uhData);
                        /* add new hotel property type  if  user choose  other options */
                        $hotelPtype = $this->input->post('property_type');
                        if ($hotelPtype != '' && count($hotelPtype) > 0) {
                            $addHotalPropertyTypesData = array();
                            foreach ($hotelPtype as $HpType) {
                                if ($HpType != '') {
                                    if ($HpType == 'Other') {
                                        /* insert new purpose */
                                        $property_type_oth = $this->input->post('property_type_oth');
                                        if ($property_type_oth != '') {
                                            /* check if already in database if not then add */
                                            if ($hPid = $this->hotel_model->getHotelPropertyType($property_type_oth)) {
                                                $hotelPtypeId = $hPid;
                                            } else {
                                                /* add data in database */
                                                $hotelPtype = array('entity_title' => $property_type_oth, 'entity_type' => $this->config->item('attribute_property_types'));
                                                $hotelPtypeId = $this->hotel_model->addHotelPropertyType($hotelPtype);
                                            }
                                        }
                                    } else {
                                        $hotelPtypeId = $HpType;
                                    }
                                    $addHotalPropertyTypesData[] = array('hotel_id' => $inserted_hotel_id,'property_type_id' => $hotelPtypeId);
                                }
                            }
                            if (count($addHotalPropertyTypesData) > 0) {
                                $this->hotel_model->addHotalPropertyTypes($addHotalPropertyTypesData);
                            }
                        }
                        /* if Hotal Property Types Specified */
                        /* if hotel added, insert contacts details  of the last added hotels */
                        if (count($hotelContactDetails) > 0) {
                            foreach ($hotelContactDetails as $hotelContact) {
                                $contactData[] = array('hotel_id' => $inserted_hotel_id,'position' => $hotelContact['position'],'name' => $hotelContact['name'],'email' => $hotelContact['email'],'phone' => $hotelContact['phone'],'extension' => $hotelContact['extension']);
                            }
							
                            $this->hotel_model->addHotalContactInfo($contactData);
                        }
                        /* insert distances	from details  of the inserted hotels */
                        $dst_distance_from_city = $this->input->post('distance_from_city');
                        $dst_distance_from_airport = $this->input->post('distance_from_airport');
                        if (!empty($dst_distance_from_airport) && count($dst_distance_from_airport) > 0) {
                            foreach ($dst_distance_from_airport as $dst_distance_airport) {
                                if ($dst_distance_airport['name'] != "" && $dst_distance_airport['distance'] != "") {
                                    $nearestDistanceData[] = array('hotel_id' => $inserted_hotel_id,'distance_from' => $dst_distance_airport['name'],'distance' => $dst_distance_airport['distance'],'distance_type' => 2);
                                }
                            }
                        }
                        if (!empty($dst_distance_from_city) && count($dst_distance_from_city) > 0) {
                            foreach ($dst_distance_from_city as $dst_distance_city) {
                                if ($dst_distance_city['name'] != "" && $dst_distance_city['distance'] != "") {
                                    $nearestDistanceData[] = array('hotel_id' => $inserted_hotel_id,'distance_from' => $dst_distance_city['name'],'distance' => $dst_distance_city['distance'],'distance_type' => 1);
                                }
                            }
                        }
                        if (!empty($nearestDistanceData) && count($nearestDistanceData) > 0) {
                            # add hotel nearest  distance 				
                            $this->hotel_model->addHotalDistanceFrom($nearestDistanceData);
                        }
                        # add hotel banking details if posted , posted data are in multy dimensanol array field name
                        if (!empty($bnk_details) && count($bnk_details) > 0) {
                            foreach ($bnk_details as $bnk) {
                                if ($bnk['account_number'] != "" && $bnk['account_name'] != "" && $bnk['bank_name'] != "" && $bnk['bank_address'] != "") {
                                    $hotelBankData[] = array('hotel_id' => $inserted_hotel_id,'iban_code' => $bnk['iban_code'],'account_number' => $bnk['account_number'],'account_name' => $bnk['account_name'],'bank_name' => $bnk['bank_name'],'bank_address' => $bnk['bank_address'],'branch_name' => $bnk['branch_name'],'branch_code' => $bnk['branch_code'],'swift_code' => $bnk['swift_code'],'bank_ifsc_code' => $bnk['bank_ifsc_code']);
                                }
                            }

                            if (!empty($hotelBankData) && count($hotelBankData) > 0) {
                                # add hotel banking details 				
                                if ($this->hotel_model->addHotalBankingInfo($hotelBankData)) {
                                    $data['message'] = '<li> Hotel Banking Details</li>';
                                }
                            }
                        }
                        # add hotel renovation shedule				
                        $RenovationSchedules = $this->input->post('rnv_shedule');
                        if (count($RenovationSchedules) > 0) {
                            foreach ($RenovationSchedules as $RenovationSchedule) {
                                if ($RenovationSchedule['date_from'] != "" && $RenovationSchedule['date_to'] != "" && $RenovationSchedule['renovation_type'] != "" && $RenovationSchedule['area_effected'] != "") {
                                    $hotelRenovationData[] = array('hotel_id' => $inserted_hotel_id,'date_from' => convert_mysql_format_date($RenovationSchedule['date_from']),'date_to' => convert_mysql_format_date($RenovationSchedule['date_to']),'renovation_type' => $RenovationSchedule['renovation_type'],'area_effected' => $RenovationSchedule['area_effected']);
                                }
                            }

                            if (!empty($hotelRenovationData) && count($hotelRenovationData) > 0) {
                                # add hotel banking details 				
                                if ($this->hotel_model->addHotalRenovationSchedule($hotelRenovationData)) {
                                    $data['message'] = '<li> Hotel Renovation Details</li>';
                                }
                            }
                        }
                        # add hotel complementary  services
                        $cmplServices = $this->input->post('complimentary');
                        if ($cmplServices != "" && count($cmplServices) > 0) {
                            $hotelCmplServices = array();
                            foreach ($cmplServices as $cmplServiceId) {
                                $hotelCmplServices[] = array('hotel_id' => $inserted_hotel_id,'cmpl_service_id' => $cmplServiceId);
                            }
                            if ($this->hotel_model->addHotalComplimentaryServices($hotelCmplServices)) {
                                $data['message'] = '<li>Hotel Complimentary Details</li>';
                            }
                        }
                        # add hotel Facilities
                        $facilities = $this->input->post('facilities');
                        if ($facilities != "" && count($facilities) > 0) {
                            $hotelFacilities = array();
                            foreach ($facilities as $facility_id) {
                                $hotelFacilities[] = array('hotel_id' => $inserted_hotel_id,'facility_id' => $facility_id);
                            }
                            if ($this->hotel_model->addHotalFacilities($hotelFacilities)) {
                                $data['message'] = '<li>Hotel Facilities Details</li>';
                            }
                        }
                        # add hotel complementary room
                        $cmpli_room_night = $this->input->post('cmpli_room_night');
                        $cmpli_date_from = convert_mysql_format_date($this->input->post('cmpli_date_from'));
                        $cmpli_date_to = convert_mysql_format_date($this->input->post('cmpli_date_to'));
                        $cmpli_upgrade = $this->input->post('cmpli_upgrade');
                        $cmpli_exclude_dates = $this->input->post('cmpli_exclude_date');
                        if ($cmpli_room_night != "" && $cmpli_date_from != "" && strtotime($cmpli_date_from) > strtotime("now")) {
                            $complimentary_room_data = array('hotel_id' => $inserted_hotel_id,'hotel_id' => $inserted_hotel_id,'room_night' => $cmpli_room_night,'start_date' => $cmpli_date_from,'end_date' => $cmpli_date_to,'upgrade' => $cmpli_upgrade,);
                            ##################insert and get hotel complementary room id  to add the  multiple Excluded date#####################
                            if ($new_cmpl_room_id = $this->hotel_model->addHotalComlimentaryRoom($complimentary_room_data)) {
                                $data['message'] = 'Hotel Complimentary Room Details';
                                $cmpli_excluded_dates = array();
                                if (count($cmpli_exclude_dates) > 0) {
                                    foreach ($cmpli_exclude_dates as $cmpli_exclude_date) {
                                        if ($cmpli_exclude_date['from'] != "" && $cmpli_exclude_date['to'] != "") {
                                            $cmpli_excluded_dates[] = array('cmpl_room_id' => $new_cmpl_room_id,'exclude_date_from' => convert_mysql_format_date($cmpli_exclude_date['from']),'excluded_date_to' => convert_mysql_format_date($cmpli_exclude_date['to']));
                                        }
                                    }
                                    if (!empty($cmpli_excluded_dates) && count($cmpli_excluded_dates) > 0) {
                                        # add complimentary  room excluded dates				
                                        $this->hotel_model->addHotalComlimentaryRoomExcludedDate($cmpli_excluded_dates);
                                    }
                                }
                            }
                        }
                        ########################################add hotel complementary room end#######################################	
                        $confrences = $this->input->post('confrence');
                        if (!empty($confrences) && count($confrences) > 0) {
                            $hotelConfrenceData = array();
                            foreach ($confrences as $confrence) {
                                if ($confrence['area'] != "" && $confrence['celling_height'] != "" && $confrence['name'] != "") {
                                    $hotelConfrenceData[] = array('hotel_id' => $inserted_hotel_id,'confrence_name' => $confrence['name'],'area' => $confrence['area'],'celling_height' => $confrence['celling_height'],'classroom' => $confrence['classroom'],'theatre' => $confrence['theatre'],'banquet' => $confrence['banquet'],'reception' => $confrence['reception'],'conference' => $confrence['conference'],'ushape' => $confrence['ushape'],'hshape' => $confrence['hshape']);
                                }
                            }
                            if (!empty($hotelConfrenceData) && count($hotelConfrenceData) > 0) {
                                # add hotel confrence details 				
                                $this->hotel_model->addHotalConfrence($hotelConfrenceData);
                            }
                        }
                        ###########################################add hotel contract information###################################
                        $contract_start_date = $this->input->post('contract_start_date');
                        $contract_end_date = $this->input->post('contract_end_date');
                        $contract_signed_by = $this->input->post('contract_signed_by');
                        $contract_file_name = '';
                        if ($contract_start_date != "" && $contract_end_date != "") {
                            if (isset($_FILES['contract_file']) && is_uploaded_file($_FILES['contract_file']['tmp_name'])) {
                                $config['upload_path'] = $this->contractFileDir;
                                $config['allowed_types'] = 'pdf|jpg|jpeg';
                                $config['max_size'] = '0'; //2 mb , 0 for unlimited
                                $config['max_width'] = '0'; // unlimited
                                $config['max_height'] = '0';
                                $this->upload->initialize($config);
                                if (!$this->upload->do_upload('contract_file')) {
                                    $error = $this->upload->display_errors();
                                    $data['message'] = '<li>Notice.Contract file not uploaded because ' . $error . ' , please upload it again, by editing the hotel details!</li>';
                                } else {
                                    $uploadedFileDetail = $this->upload->data();$contract_file_name = $uploadedFileDetail['file_name'];}
                            }
                            $hotelContractData = array('hotel_id' => $inserted_hotel_id,'start_date' => convert_mysql_format_date($contract_start_date),'end_date' => convert_mysql_format_date($contract_end_date),'signed_by' => $contract_signed_by,'contract_file' => $contract_file_name);
                            if ($this->hotel_model->addHotalContract($hotelContractData)) {
                                if ($contract_file_name != "") {
                                    $data['message'] = '<li>Hotel Contract Details with file ' . $contract_file_name . '</li>';
                                } else {$data['message'] = '<li>Hotel Contract Details</li>';}
                            }
                        }
                        ###############################add hotel contract information end########################################
                        
                        ###############################add hotel cancellation information  start#################################
                        
                        $lowseason_cancelations = $this->input->post('lowseason_canceled'); // multi dimensonal array posted from form
                        $highseason_cancelations = $this->input->post('highseason_canceled'); // multi dimensonal array posted from form
                        $hotelCancellationData = array();
                        if (!empty($lowseason_cancelations) && count($lowseason_cancelations) > 0) {
                            foreach ($lowseason_cancelations as $lowseason_cancel) {
                                if ($lowseason_cancel['before'] != "" && $lowseason_cancel['payment_request'] != "") {
                                    $hotelCancellationData[] = array('hotel_id' => $inserted_hotel_id,'cancelled_before' => $lowseason_cancel['before'],'payment_request' => $lowseason_cancel['payment_request'],'seasion' => 'low');
                                }
                            }
                        }
                        if (!empty($highseason_cancelations) && count($highseason_cancelations) > 0) {
                            foreach ($highseason_cancelations as $highseason_cancel) {
                                if ($highseason_cancel['before'] != "" && $highseason_cancel['payment_request'] != "") {
                                    $hotelCancellationData[] = array('hotel_id' => $inserted_hotel_id,'cancelled_before' => $highseason_cancel['before'],'payment_request' => $highseason_cancel['payment_request'],'seasion' => 'high');
                                }
                            }
                        }
                        if (!empty($hotelCancellationData) && count($hotelCancellationData) > 0) {
                            if ($this->hotel_model->addHotalCancellation($hotelCancellationData)) {
                                $data['message'] = '<li>Hotel Cancellation Details</li>';
                            }
                        }
                        ######################################add hotel cancellation information end###################################
                         								
                        ######################################add hotel payment shedule  option block##################################				
                        $paymentShedules = $this->input->post('payment_plan'); // multi dimensonal array posted from form
                        $hotelPaymentShedulesData = array();
                        if (!empty($paymentShedules) && count($paymentShedules) > 0) {
                            foreach ($paymentShedules as $paymentShedule) {
                                if ($paymentShedule['payment_value'] != "" && $paymentShedule['payment_option_id'] != "") {
                                    $hotelPaymentShedulesData[] = array('hotel_id' => $inserted_hotel_id,'payment_option_id' => $paymentShedule['payment_option_id'],'payment_value' => $paymentShedule['payment_value'],);
                                }
                            }
                            if (!empty($hotelPaymentShedulesData) && count($hotelPaymentShedulesData) > 0) {
                                if ($this->hotel_model->addHotalPaymentShedules($hotelPaymentShedulesData)) {
                                    $data['message'] = '<li>Hotel Payment Details</li>';
                                }
                            }
                        }
                        ####################################add hotel payment shedule  option end########################################
                         				
                        ####################################add hotel pricing section####################################################
                        
                        $hotelPricingRecords = $this->input->post('pricing'); // multi dimensonal array posted from form
                        $hotelRoomsPricingData = array(); // for table hotel_rooms_pricing
                        if (!empty($hotelPricingRecords) && count($hotelPricingRecords) > 0) {
                            foreach ($hotelPricingRecords as $hotelPricingRecord) {
                                $hotelRroomsPricingDetailsData = array(); // for hotel_rooms_pricing_details table
                                $hotelRoomsPricingComplimentaryData = array(); // for table hotel_rooms_pricing_complimentary
                                if ($hotelPricingRecord['room_type'] != "" && $hotelPricingRecord['invenory'] != "") {
                                    $hotelRoomsPricingData = array('hotel_id' => $inserted_hotel_id,'room_type' => $hotelPricingRecord['room_type'],'inclusions' => $hotelPricingRecord['pricing_inclusions'],'curency_code' => $hotelPricingRecord['currency'],'max_adult' => $hotelPricingRecord['max_adult'],'max_child' => $hotelPricingRecord['max_child'],'inventory' => $hotelPricingRecord['invenory'],'period_from' => convert_mysql_format_date($hotelPricingRecord['period_from_date']),'period_to' => convert_mysql_format_date($hotelPricingRecord['period_to_date']),);
                                    $pricingRoomFacilities = $hotelPricingRecord['room_facilities'];
                                    $roomPriceingDetails = $hotelPricingRecord['price_detail'];
                                    if (!empty($hotelRoomsPricingData) && count($hotelRoomsPricingData) > 0) {
                                        $pricing_inserted_id = $this->hotel_model->addHotalRoomsPricingData($hotelRoomsPricingData); // insert data and get inserted id
                                        if ($pricing_inserted_id) {// if hotel pricing record added in database
                                            // add  room pricing facilities
                                            if (!empty($pricingRoomFacilities) && count($pricingRoomFacilities) > 0) {
                                                foreach ($pricingRoomFacilities as $roomFacility) {
                                                    if ($roomFacility != "") {
                                                        $hotelRoomsPricingComplimentaryData[] = array('pricing_id' => $pricing_inserted_id,'cmpl_service_id' => $roomFacility);
                                                    }
                                                }
                                                if (!empty($hotelRoomsPricingComplimentaryData) && count($hotelRoomsPricingComplimentaryData) > 0) {
                                                    $this->hotel_model->addHotalRoomPricingComplimentary($hotelRoomsPricingComplimentaryData);
                                                }
                                            }
                                            ##########################room pricing facilities section end.################################
                                            // add  room pricing details
                                            if (!empty($roomPriceingDetails) && count($roomPriceingDetails) > 0) {
                                                foreach ($roomPriceingDetails as $roomPriceingDetail) {
                                                    if ($roomPriceingDetail['market_id'] != "") {
                                                        $hotelRroomsPricingDetailsData[] = array('pricing_id' => $pricing_inserted_id,'market_id' => $roomPriceingDetail['market_id'],'double_price' => $roomPriceingDetail['double_price'],'triple_price' => $roomPriceingDetail['triple_price'],'quad_price' => $roomPriceingDetail['quad_price'],'breakfast_price' => $roomPriceingDetail['breakfast_price'],'half_board_price' => $roomPriceingDetail['half_board_price'],'all_incusive_adult_price' => $roomPriceingDetail['all_incusive'],'extra_adult_price' => $roomPriceingDetail['extra_adult'],'extra_child_price' => $roomPriceingDetail['extra_child'],'extra_bed_price' => $roomPriceingDetail['extra_bed'],);
                                                    }
                                                }
                                                if (!empty($hotelRroomsPricingDetailsData) && count($hotelRroomsPricingDetailsData) > 0) {
                                                    $this->hotel_model->addHotelRroomsPricingDetails($hotelRroomsPricingDetailsData);
                                                    $data['message'] = 'Hotel Pricing Details ';
                                                }
                                            }
                                            // add  room pricing details end							
                                        }// endif  pricing data added
                                    }
                                }// end of pricing records if exists
                            }// end for each  price records for loop													
                        }
                        
                        #########################################add hotel pricing section end#####################################
                        $identity = $this->input->post('identity');
                        $dbidentity = $this->hotel_model->get_dbidentity($identity);
                        if ($dbidentity && $identity == ($dbidentity->identifier)) {
                            $identity_data = array('hotel_id' => $inserted_hotel_id);
                            $this->hotel_model->update_images($identity, $identity_data);
                        }
                        if (isset($_FILES['myfile']) && is_uploaded_file($_FILES['myfile']['tmp_name'])) {
                            /*  zip folder code start   */
                            $config['upload_path'] = $this->hotelFileDir;
                            $config['allowed_types'] = 'zip';
                            $config['max_size'] = '0';
                            $this->upload->initialize($config);
                            $this->upload->do_upload('myfile');
                            #Get the uploaded zip file
                            $categories = $this->hotel_model->gallery_detail();
                            if ($categories && count($categories > 0)) {
                                foreach ($categories as $v) {
                                    $category[$v['id']] = $v['entity_title'];
                                }
                                $zip_array = array('upload_data' => $this->upload->data());
                                //print_r($zip_array); die;
                                $zip_file = $zip_array['upload_data']['full_path'];
                                $pathpart = pathinfo($zip_file);
                                $file_name_with_underscore = $pathpart['filename'];
                                $folder_name = str_replace('_', ' ', $file_name_with_underscore); // folder name contain _ replace it by space
                                $zip = new ZipArchive;
                                #Open the Zip File, and extract it's contents.
                                if ($zip->open($zip_file) === TRUE) {
                                    $zip->extractTo($this->hotelFileDir);
                                    foreach ($category as $key => $v) {
                                        if ($folder_name == $v) {
                                            $images_main = directory_map($this->hotelFileDir . $folder_name);
                                            $counter_main = count($images_main);
                                            if ($counter_main != 1) {
                                                foreach ($images_main as $img_main) {
                                                    $pathpart = pathinfo($img_main);
                                                    $ext = $pathpart['extension'];
                                                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                                                        $dir = $this->hotelFileDir . trim($folder_name) . '/' . $img_main;
                                                        $galimage_data = array('hotel_id' => $hid, 'gallery_id' => $key, 'image_name' => $img_main);
                                                        $this->hotel_model->insert_images($galimage_data);
                                                        rename("$dir", "$this->hotelFileDir$img_main"); // move file to respective directory
                                                    }
                                                }
                                            }
                                        }
                                        $images = directory_map($this->hotelFileDir . $folder_name . '/' . trim($v) . '/');
                                        $counter = count($images);
                                        if ($counter != 1) {
                                            foreach ($images as $img) {
                                                $pathpart = pathinfo($img);
                                                $ext = $pathpart['extension'];
                                                if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                                                    $dir = $this->hotelFileDir . $folder_name . '/' . trim($v) . '/' . $img;
                                                    $data_image_array = array('hotel_id' => $inserted_hotel_id, 'gallery_id' => $key, 'image_name' => $img);
                                                    $this->hotel_model->insert_images($data_image_array);
                                                    rename("$dir", "$this->hotelFileDir$img");
                                                }
                                            }
                                        }
                                    }
                                    $zip->close();
                                    if (file_exists($zip_file)) {
                                        try {
                                            unlink($zip_file);
                                        } catch (Exception $e) {
                                            log_message('gallery zip folder not deleted', $e->getMessage());
                                        }
                                    }
                                    unlink($this->hotelFileDir . $file_name_with_underscore);
                                }
                            }
                        }
                        /*  zip folder code close   */
                        /* clear all post data */
                        //unset($_POST);
                        //unset($_REQUEST);
						$submitcheck = $this->input->post('submit_call');
						 if ($submitcheck != "") {
						/*if((isset($_POST['submit_call'])=='save') && ($this->session->userdata('stepsess')=='#tab-1')){*/
							
							$data['message'] = 'Hotel Details Added Successfully Hotel Code:- "'.$hcode.'"';
							$flashdata = array('flashdata' => $data['message'],'message_type' => 'sucess');
							$this->session->set_userdata($flashdata);
							redirect('hotels','refresh');
						}else{
					
							redirect("hotels/edit_information/" . $inserted_hotel_id); /* redirect to hotel listing page  after sucessfull insertion */
						}
                       
						
                    } else {
                        $data['message'] = 'Data Not Inserted,Please try again';
                        $flashdata = array('flashdata' => $data['message'],'message_type' => 'error');
                        $this->session->set_userdata($flashdata);
                    }
                }
            } else {
                $data['message'] = 'Data Not valid,Please try again';
                $flashdata = array('flashdata' => $data['message'],'message_type' => 'error');
                $this->session->set_userdata($flashdata);
            }
        }
        //get the list of hotel chain from the form field value helper
        $chain_options = getHotelChainsOptions();
        $chain_options['Other'] = 'Other'; // add extra option 
        $propertyTypeOptions = getPropertyTypeOptions('nosel');
        $propertyTypeOptions['Other'] = 'Other';
        $countriesOptions = getCountriesOptions(); /* using helper */
        $pcdoce = $this->input->post('country_code');
        $citiesOptions = getCountyCitiesOptions($pcdoce); /* using helper */
        $roomTypes = getRoomTypes(); /* using helper */
        $roomTypesOptions = array('' => 'Select');
        if ($roomTypes && count($roomTypes) > 0) {
            foreach ($roomTypes as $roomType) {
                $roomTypesOptions[$roomType->id] = $roomType->entity_title;
            }
        }
        //get the list of hotel position from the form field value helper
        $positions = getPositions();
        $positionOptions = array('' => 'Select');
        if ($positions && count($positions) > 0) {
            foreach ($positions as $position) {
                $positionOptions[$position->id] = $position->entity_title;
            }
        }
        //get the list of hotel currencies from the form field value helper
        $CurrenciesOptions = getCurrencies(); /* using helper */
        $renovationTypes = getRenovationTypes();
        $renovationTypesOptions = array();
        if ($renovationTypes && count($renovationTypes) > 0) {
            foreach ($renovationTypes as $renovationType) {
                $renovationTypesOptions[$renovationType->id] = $renovationType->renovation_type;
            }
        }
        //get the list of age group for child from the form field value helper
        $ageGroupsOption = getChildAgeOptions(); /* using helper */
        $cancelationPeriods = getCancelationPeriods();
        $cancelationPeriodsOption = array();
        if ($cancelationPeriods && count($cancelationPeriods) > 0) {
            foreach ($cancelationPeriods as $cancelationPeriod) {
                $cancelationPeriodsOption[$cancelationPeriod->id] = $cancelationPeriod->entity_title;
            }
        }
        //get the payment list from the form field value helper
        $applicablePaymentOptions = getPaymentOptions();
        $paymentOptions = array();
        if ($applicablePaymentOptions && count($applicablePaymentOptions) > 0) {
            foreach ($applicablePaymentOptions as $applicablePaymentOption) {
                $paymentOptions[$applicablePaymentOption->id] = $applicablePaymentOption->entity_title;
            }
        }
        //get the list of availabe complementary from the form field value helper
        $roomCmplServices = getAvailabeComplementaryServices();
        $roomCmplServicesOptions = array();
        if ($roomCmplServices && count($roomCmplServices) > 0) {
            foreach ($roomCmplServices as $cmplService) {
                $roomCmplServicesOptions[$cmplService->id] = $cmplService->entity_title;
            }
        }
        //get the list of markets from the form field value helper
        $marketsList = getMarkets();
        $marketOptions = array();
        if ($marketsList && count($marketsList) > 0) {
            foreach ($marketsList as $market) {
                $marketOptions[$market->id] = $market->entity_title;
            }
        }
        $scity = $this->input->post('city') != '' ? $this->input->post('city') : '';
        if ($scity != "") {
            $districtOptions = getCityDistrictOptions($scity, 'sel');
        } else {
            $districtOptions = array('' => 'Select');
        }
          /**************************generate form field for hotel entry form ***************************************/
            $this->data = array('hotel_name' => array('id' => 'hotel_name','class' => 'required','tabindex' => 1,'data-validation' => 'required,custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "10-100",'data-validation-help' => "Should  be minimum  10  characters ",'name' => 'hotel_name','maxlength' => '100','size' => '100','value' => set_value('hotel_name')),
            'star_rating_options' => getStarRatings(),
            'dorak_rating_options' => getDorakRatings(),
            'status_options' => getHotelStatus(),
            'position_options' => $positionOptions,
            'currency_options' => $CurrenciesOptions,
            'commisioned_options' => array('' => 'Select','1' => 'Yes','0' => 'No'),
            'purpose_options' => getPurposeOptions(),
            'purpose_options_oth' => array('id' => 'purpose_options_oth','class' => 'purpose_options_oth','placeholder' => 'Specify other','data-validation' => 'required,length','data-validation-length' => "4-50",'name' => 'purpose_options_oth','value' => set_value('purpose_options_oth')),
            
            'chain_options' => $chain_options,
            'hotel_chain_oth' => array('id' => 'hotel_chain_oth','class' => 'hotel_chain_oth','maxlength' => "50",'placeholder' => 'Specify other','data-validation' => 'required,custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",                'data-validation-length' => "3-50",'name' => 'hotel_chain_oth','value' => set_value('hotel_chain_oth')),
            
            'property_type_options' => $propertyTypeOptions,
            'property_type_oth' => array('id' => 'property_type_oth','class' => 'property_type_oth','placeholder' => 'Specify other','data-validation' => 'required,custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "4-50",'name' => 'property_type_oth','value' => set_value('property_type_oth')),'country_id_options' => $countriesOptions,'city_options' => $citiesOptions,'hotel_address' => array('id' => 'hotel_address','tabindex' => 14,'data-validation' => 'required,alphanumeric,length','data-validation-allowing' => "'\'-_@#:,./() ",'data-validation-length' => "15-150",'data-validation-help' => "Should  be minimum  15  characters ",'name' => 'hotel_address','maxlength' => '150','size' => '150','value' => set_value('hotel_address')),
            'districtOptions' => $districtOptions,
            
            'post_code' => array('id' => 'post_code','class' => 'required','tabindex' => 18,'data-validation' => 'required,length,alphanumeric','data-validation-length' => "4-8",'name' => 'post_code','maxlength' => '10','size' => '10','value' => set_value('post_code')),
            
            'distance_from_city' => array('name' => 'distance_from_city[1][distance]','data-validation' => 'distance','tabindex' => 20,'data-validation-optional' => 'true','data-validation-help' => "Only numeric or decimal ie. (3,2) value accepted ",'placeholder' => 'Distance','value' => set_value('distance_from_city[1][distance]')),
            
            'distance_from_city_name' => array('name' => 'distance_from_city[1][name]','class' => 'g-autofill','tabindex' => 19,'onfocus' => "searchLocation(this)",'id' => "distanc-city",'data-validation' => 'alphanumeric','data-validation-allowing' => "-_@#:,./\() ",'data-validation-optional' => 'true','placeholder' => 'Enter City / Locality Name','value' => set_value('distance_from_city[1][name]')),
            
            'distance_from_airport' => array('name' => 'distance_from_airport[1][distance]','data-validation' => 'distance','tabindex' => 22,'data-validation-optional' => 'true','placeholder' => 'Distance','data-validation-help' => "Only numeric or decimal ie. (3,2) value accepted ",'value' => set_value('distance_from_airport[1][distance]')),
            
            'distance_from_airport_name' => array('name' => 'distance_from_airport[1][name]','tabindex' => 21,'data-validation' => 'alphanumeric','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-allowing' => "-_@#:,./\() ",'onfocus' => "searchLocation(this)",'class' => 'g-autofill','data-validation-optional' => 'true','placeholder' => 'Enter Airport Name','value' => set_value('distance_from_airport[1][name]')),
            
            'contact_name' => array('name' => 'contact[1][name]','id' => 'contact_1_name','tabindex' => 10,'class' => 'required','data-validation' => 'custom,required,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "min3",'placeholder' => 'Name','value' => set_value('contact[1][name]')),
            
            'contact_phone' => array('name' => 'contact[1][phone]','tabindex' => 12,'id' => 'contact_1_phone','data-validation' => 'required,phone_number',
                //'data-validation-length'=>"5-20",
                'data-validation-length' => "min10",'maxlength' => '20','placeholder' => 'Mobile','class' => 'required','value' => set_value('contact[1][phone]')),
            
            'contact_email' => array('name' => 'contact[1][email]','tabindex' => 11,'id' => 'contact_1_email','data-validation' => 'required,email','class' => 'required','placeholder' => 'Email','value' => set_value('contact[1][email]')),
            'contact_extension' => array('tabindex' => 13,'name' => 'contact[1][extension]','data-validation' => 'length,number','data-validation-optional' => 'true','data-validation-length' => "2-6",'maxlength' => '6','size' => '6','class' => 'required','placeholder' => 'Ext.','value' => set_value('contact[1][extension]')),
            
            /*********************************form fields for  bank information**************************************/
            'account_number' => array('class' => 'required','data-validation' => 'number,length','data-validation-length' => "5-16",'data-validation-help' => "Minimum  5 digit",'data-validation-optional' => 'true','name' => 'bank[1][account_number]','maxlength' => '16','size' => '20','value' => set_value('bank[1][account_number]')),'account_name' => array('data-validation' => 'custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "3-100",'data-validation-help' => "Minimum  3  characters ",'data-validation-optional' => 'true','name' => 'bank[1][account_name]','maxlength' => '100','size' => '100','value' => set_value('bank[1][account_name]')),'bank_name' => array('data-validation' => 'length','data-validation-length' => "3-100",'data-validation-help' => "Minimum  3  characters ",'data-validation-optional' => 'true','name' => 'bank[1][bank_name]','maxlength' => '100','size' => '100','value' => set_value('bank[1][bank_name]')),'bank_address' => array('data-validation' => 'alphanumeric,length','data-validation-allowing' => "'\'-_@#:,./() ",'data-validation-length' => "10-150",'data-validation-optional' => 'true','name' => 'bank[1][bank_address]','data-validation-help' => "Minimum  length 10  characters ",'maxlength' => '100','size' => '100','value' => set_value('bank[1][bank_address]')),'branch_name' => array('data-validation' => 'length','data-validation-length' => "3-100",'data-validation-help' => "Minimum  3  characters ",'data-validation-optional' => 'true','name' => 'bank[1][branch_name]','maxlength' => '100',               'size' => '100','value' => set_value('bank[1][branch_name]')),'bank_ifsc_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",                'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][bank_ifsc_code]','maxlength' => '30','size' => '30','value' => set_value('bank[1][bank_ifsc_code]')),
            'iban_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][iban_code]','maxlength' => '30','size' => '30','value' => set_value('bank[1][iban_code]')),
            'branch_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][branch_code]','maxlength' => '30','size' => '30','value' => set_value('bank[1][branch_code]')),
            'swift_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][swift_code]','maxlength' => '30','size' => '30','value' => set_value('bank[1][swift_code]')),
            'roomTypesOptions' => $roomTypesOptions,
            'upgradeOptions' => array('0' => 'No','1' => 'Yes',),
            'ageGroupsOption' => $ageGroupsOption,
            'cmpli_room_night' => array('data-validation' => 'number','data-validation-optional' => 'true','name' => 'cmpli_room_night','value' => set_value('cmpli_room_night')),
            'confrenceUshapeOptions' => array('' => 'Select','1' => '1','2' => '2','3' => '3','4' => '4'),
            'confrenceHsquareOptions' => array('' => 'Select','1' => '1','2' => '2','3' => '3','4' => '4'),
            'cancellation_options' => $cancelationPeriodsOption,
            'payment_options' => $paymentOptions,
            'rnv_shedule_area_effected' => array('name' => 'rnv_shedule[1][area_effected]','id' => 'rnv_shedule[1][area_effected]','value' => set_value('rnv_shedule[1][area_effected]'),'rows' => '1','cols' => '10',),
            'renovation_options' => $renovationTypesOptions,
            'room_cmpl_options' => $roomCmplServicesOptions,
            'pricing_inclusions' => array('name' => 'pricing[1][pricing_inclusions]','id' => 'pricing[1][pricing_inclusions]','value' => set_value('pricing[1][pricing_inclusions]'),'rows' => '5','cols' => '10',),
            'markets_options' => $marketOptions,
        );
            $defMarket = $this->config->item('default_market');
            $this->data['posted_position'] = $this->input->post('position');
            $this->data['posted_star_rating'] = $this->input->post('star_rating');
            $this->data['posted_dorak_rating'] = $this->input->post('dorak_rating');
            $this->data['posted_chain_id'] = $this->input->post('chain_id');
            $this->data['posted_property_type'] = $this->input->post('property_type');
            $this->data['posted_purpose'] = $this->input->post('purpose');
            $this->data['posted_commisioned'] = $this->input->post('commisioned') != '' ? $this->input->post('commisioned') : '';
            $this->data['posted_currency'] = $this->input->post('currency') != '' ? $this->input->post('currency') : 'USD';
            $this->data['posted_status'] = $this->input->post('status') != '' ? $this->input->post('status') : 'Enabled';
            $dc = $this->config->item('default_country_code');
            $defCcode = array($dc);
            $this->data['posted_country_code'] = $this->input->post('country_code') != '' ? $this->input->post('country_code') : $defCcode;
            $this->data['posted_city'] = $this->input->post('city') != '' ? $this->input->post('city') : '';
            $this->data['posted_district'] = $this->input->post('district') != '' ? $this->input->post('district') : '';
            $this->data['posted_room_type'] = $this->input->post('room_type') != '' ? $this->input->post('room_type') : '';
            $this->data['posted_cmpli_room_type'] = $this->input->post('cmpli_room_type') != '' ? $this->input->post('cmpli_room_type') : '';
            $this->data['posted_cmpli_upgrade'] = $this->input->post('cmpli_upgrade') != '' ? $this->input->post('cmpli_upgrade') : '0';
            $this->data['posted_cmpli_room_night'] = $this->input->post('cmpli_room_night') != '' ? $this->input->post('cmpli_room_night') : '';
            $this->data['confrence_conference'] = $this->input->post('confrence_conference');
            $this->data['confrence_reception'] = $this->input->post('confrence_reception');
            $this->data['confrence_banquet'] = $this->input->post('confrence_banquet');
            $this->data['confrence_theatre'] = $this->input->post('confrence_theatre');
            $this->data['confrence_ushape'] = $this->input->post('confrence_ushape') != '' ? $this->input->post('confrence_ushape') : '';
            $this->data['confrence_hsquare'] = $this->input->post('confrence_hsquare') != '' ? $this->input->post('confrence_hsquare') : '';
            $this->data['posted_renovation_type'] = $this->input->post('rnv_shedule[1][renovation_type]') != '' ? $this->input->post('rnv_shedule[1][renovation_type]') : '';
            $this->data['posted_market_id'] = $this->input->post('pricing[1][price_detail][1][market_id]') != '' ? $this->input->post('pricing[1][price_detail][1][market_id]') : $defMarket;
            $this->data['galleries'] = $this->hotel_model->get_gallary();
            $this->data['identity'] = $this->identityfier;
            $this->load->view('informations_view', $this->data);
    }

    /**
     * @method string upload().
     * @todo upload  gallery image using ajax.  
     * @return success if file successfully uploaded.  
     */
    function upload() {
        if (!empty($_FILES)) {
            $gallary_id = $this->input->post('galeryid');
            $gallerysessid = $this->input->post('gallerysessid');
            $hotelids = $this->input->post('hotelid');
            if (!empty($hotelids)) {
                $hotelid = $hotelids;
            } else {
                $hotelid = null;
            }
            $ds = DIRECTORY_SEPARATOR;
            foreach ($_FILES["file"]["error"] as $key => $error) {
                if ($error == UPLOAD_ERR_OK) {
                    $tempFile = $_FILES['file']['tmp_name'][$key];
                    $name = $_FILES["file"]["name"][$key];
                    $file_extension = end((explode(".", $name))); # extra () to prevent notice
                    $targetPath = FCPATH . $this->hotelFileDir . $ds;  //4
                    $targetFile = $targetPath . $name;
                    move_uploaded_file($tempFile, $targetFile);
                    $data_galery_det = array('hotel_id' => $hotelid, 'gallery_id' => $gallary_id, 'image_name' => $name, 'identifier' => $gallerysessid);
                    $status = $this->hotel_model->insert_images($data_galery_det);
                }
            }
        }
    }

    /**
     * @method string deleteimg().
     * @todo delete  gallery image using ajax.  
     * @return NULL
     */
    function deleteimg() {
        $imgname = $this->input->post('id');
        $galid = $this->input->post('galid');
        $ds = DIRECTORY_SEPARATOR;
        $targetPath = FCPATH . $this->hotelFileDir . $ds;  //4
        $targetFile = $targetPath . $imgname;
        try {
            unlink($targetFile);
        } catch (Exception $e) {
            log_message('gallery image deletion ', $e->getMessage());
        }
        $this->hotel_model->delete_images($imgname, $galid);
    }

    /**
     * @method string edit_information()
     * @todo Update hotel information(all tabs related data) based on perticular hotel id. 
     * @param $hid(int) hotel id - it is the primary key of an hotel.
     * @return success it all data successfully updated else false.
     */
    public function edit_information($hid = null) {
//        ob_start();
//        
//         dump($_FILES);
        $data = array('message' => '');
        // Check acess the user access	
        if (!checkAccess($this->accessLabelId, 'hotels', 'edit')) {
            $flashdata = array('flashdata' => $this->accessDenidMessage,'message_type' => 'notice');
            $this->session->set_userdata($flashdata);
            redirect('users', 'refresh');
        }
        if ($hid == "") {
            $data['message'] = 'Invalid  Access!';
            $flashdata = array('flashdata' => $data['message'],'message_type' => 'notice');
            $this->session->set_userdata($flashdata);
            redirect("hotels"); /* redirect to hotel listing page */
        }
        $this->load->library('upload');
        $this->output->set_meta('description', 'Hotel Information');
        $this->output->set_meta('title', 'Hotel Information');
        $this->output->set_title('Hotel Information');
        $user = $this->ion_auth->user()->row();
        if ($this->session->userdata('stepsess') == '#tab-5') {
            $this->session->set_userdata('stepsess', '#tab-6');
        }

        if (!empty($_POST)) {
		 //echo ($_POST['submit_call']); die;
			//echo $this->input->get_post('submit_call'); die;
           //dump($_POST);die;
            $identity = $this->input->post('identity');
            $step = $this->input->post('step'); /* input for save data on tab basis */
            $this->session->set_userdata('stepsess', $step); /* session start for tabs */
            # validation rules 
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('hotel_address', 'Address', 'required|min_length[15]|max_length[150]');
            $this->form_validation->set_rules('post_code', 'Zip Code', 'required|alpha_numeric|min_length[4]|max_length[8]');
            $this->form_validation->set_rules('currency', 'Currency', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('property_type_oth', 'property_type_oth', 'min_length[3]');
            $this->form_validation->set_rules('purpose_options_oth', 'purpose_options_oth', 'min_length[3]');
            $this->form_validation->set_rules('hotel_chain_oth', 'hotel_chain_oth', 'min_length[5]');
            if ($this->form_validation->run($this) != FALSE) {
                $contactData = array();
                $nearestDistanceData = array();  /* we'll store each distance row in this array */
                $hotelBankData = array();
                $purpose = $this->input->post('purpose');
                if ($purpose != '' && $purpose == 'Other') {
                    # insert new purpose
                    $purpose_options_oth = $this->input->post('purpose_options_oth');
                    if ($purpose_options_oth != '') {
                        # check if already in database if not then add
                        if ($pid = $this->hotel_model->getPurpose($purpose_options_oth)) {
                            $purposeId = $pid;
                        } else {
                            # add data in database 
                            $hotel_purpose = array('title' => $purpose_options_oth);
                            $purposeId = $this->hotel_model->addPurpose($hotel_purpose);
                        }
                    }
                } else {
                    $purposeId = $purpose;
                }
                # add new hotel chain if  user choose  other options the specified  value need to store int  hotel chain list table
                $hotelchain = $this->input->post('chain_id');
                if ($hotelchain != '' && $hotelchain == 'Other') {
                    # insert new purpose
                    $hotel_chain_oth = $this->input->post('hotel_chain_oth');
                    if ($hotel_chain_oth != '') {
                        /* check if already in database if not then add */
                        if ($hcid = $this->hotel_model->getHotelChain($hotel_chain_oth)) {
                            $chainId = $hcid;
                        } else {
                            /* add data in database  */
                            $hotel_chain = array('entity_title' => $hotel_chain_oth, 'entity_type' => $this->config->item('attribute_hotel_chain'));
                            $chainId = $this->hotel_model->addHotelChain($hotel_chain);
                        }
                    }
                } else {
                    $chainId = $hotelchain;
                }
                $hotel_detail = array('hotel_address' => $this->input->post('hotel_address'),'post_code' => $this->input->post('post_code'),'star_rating' => $this->input->post('star_rating'),'dorak_rating' => $this->input->post('dorak_rating'),'chain_id' => $chainId,'purpose' => $purposeId,'currency' => $this->input->post('currency'),'district' => $this->input->post('district'),'status' => $this->input->post('status'),);
                $this->hotel_model->updateHotelInformation($hid, $hotel_detail);

                /* update contacts details  of the hotels */
                $nonDelContactids = array();
                $hotelContactDetails = $this->input->post('contact');
                if (count($hotelContactDetails) > 0) {
                    foreach ($hotelContactDetails as $hotelContact) {
                        if (isset($hotelContact['id']))
                            $hcontactid = $hotelContact['id'];
                        else
                            $hcontactid = '';
                        if ($hcontactid != "")
                            array_push($nonDelContactids, $hcontactid);
                        $contactData = array('hotel_id' => $hid,'position' => $hotelContact['position'],'name' => $hotelContact['name'],'email' => $hotelContact['email'],'phone' => $hotelContact['phone'],'extension' => $hotelContact['extension'],);

                        $updatecontactid = $this->hotel_model->updateHotalContact($contactData, $hcontactid);
                        array_push($nonDelContactids, $updatecontactid);
                    }
                    $this->hotel_model->deleteHotalContact($nonDelContactids, $hid);
                }
                # add hotel complementary  services
                $cmplServices = $this->input->post('complimentary');
                if (empty($cmplServices)) {
                    $this->hotel_model->deleteroomFacility($hid);
                }
                if ($cmplServices != "" && count($cmplServices) > 0) {
                    $hotelCmplServices = array();

                    foreach ($cmplServices as $cmplServiceId) {
                        if (isset($cmplServiceId) && $cmplServiceId != "") {
                            $hotelCmplServices[] = array('hotel_id' => $hid,'cmpl_service_id' => $cmplServiceId);
                        }
                    }
                    if ($this->hotel_model->deleteroomFacility($hid)) {
                        $this->hotel_model->addHotalComplimentaryServices($hotelCmplServices);
                    }
                }
                # add hotel Facilities
                $facilities = $this->input->post('facilities');
                if (empty($facilities)) {
                    $this->hotel_model->deletehotelFacility($hid);
                }
                if ($facilities != "" && count($facilities) > 0) {
                    $hotelFacilities = array();
                    foreach ($facilities as $facility_id) {

                        if (isset($facility_id) && $facility_id != "") {
                            $hotelFacilities[] = array('hotel_id' => $hid,'facility_id' => $facility_id);
                        }
                    }
                    if ($this->hotel_model->deletehotelFacility($hid)) {
                        $this->hotel_model->addHotalFacilities($hotelFacilities);
                    }
                }
                $nonbnkIds = array();
                $bnk_details = $this->input->post('bank');
                if (!empty($bnk_details) && count($bnk_details) > 0) {
                    foreach ($bnk_details as $bnk) {
                        if (isset($bnk['id']))
                            $bankid = $bnk['id'];
                        else
                            $bankid = '';
                        //if($bankid!="")			
                        //array_push($nonbnkIds,$bankid);
                        if ($bnk['account_number'] != "" && $bnk['account_name'] != "" && $bnk['bank_name'] != "" && $bnk['bank_address'] != "") {
                            $hotelBankData = array('hotel_id' => $hid,'iban_code' => $bnk['iban_code'],'account_number' => $bnk['account_number'],'account_name' => $bnk['account_name'],'bank_name' => $bnk['bank_name'],'bank_address' => $bnk['bank_address'],'branch_name' => $bnk['branch_name'],'branch_code' => $bnk['branch_code'],'swift_code' => $bnk['swift_code'],'bank_ifsc_code' => $bnk['bank_ifsc_code']);
                            $bankNid = $this->hotel_model->updateHotalbankaccounts($hotelBankData, $bankid);
                            array_push($nonbnkIds, $bankNid);
                        } else {
                            $this->hotel_model->deleteHotalbankaccount($bankid, $hid); // delete empty  account details
                        }
                    }
                    // delete non existing data
                    $this->hotel_model->deleteHotalbankaccounts($nonbnkIds, $hid);
                } else {
                    $this->hotel_model->deleteHotalbankaccounts(false, $hid);
                }
                /* distance by city name */
                $noncityIds = array();
                $distance_from_city_name = $this->input->post('distance_from_city');
                if (!empty($distance_from_city_name)) {
                    foreach ($distance_from_city_name as $cityname) {
                        if (isset($cityname['id']))
                            $cityid = $cityname['id'];
                        else
                            $cityid = '';
                        if ($cityid != "")
                            array_push($noncityIds, $cityid);
                        if (isset($cityname['name']) && $cityname['name'] != "" && isset($cityname['distance']) && $cityname['distance'] != "") {
                            $citynamedata = array('hotel_id' => $hid,'distance_from' => $cityname['name'],'distance_type' => '1','distance' => $cityname['distance']);
                            $cityupdateid = $this->hotel_model->updateHotalcity($citynamedata, $cityid);
                            array_push($noncityIds, $cityupdateid);
                        }
                    }
                    $this->hotel_model->deleteHotalcity($noncityIds, $hid);
                }

                /* distance by city name close here */

                /* distance by airport name */
                $nonairIds = array();
                $distance_from_airport_name = $this->input->post('distance_from_airport');
                if (!empty($distance_from_airport_name)) {
                    foreach ($distance_from_airport_name as $airname) {
                        if (isset($airname['id']))
                            $airid = $airname['id'];
                        else
                            $airid = '';
                        if ($airid != "")
                            array_push($nonairIds, $airid);
                        if (isset($airname['name']) && $airname['name'] != "" && isset($airname['distance']) && $airname['distance'] != "") {
                            $airnamedata = array('hotel_id' => $hid,'distance_from' => $airname['name'],'distance_type' => '2','distance' => $airname['distance']);
                            $airupdateid = $this->hotel_model->updatedistanceForAirport($airnamedata, $airid);
                            array_push($nonairIds, $airupdateid);
                        }
                    }
                    $this->hotel_model->deletedistanceForAirport($nonairIds, $hid);
                }
                /* distance by airport name close here */
                /* add new hotel property type  if  user choose  other options */
                $hotelPtype = $this->input->post('property_type');
                if (empty($hotelPtype)) {
                    $this->hotel_model->deleteHotalpropertytype($hid);
                }
                if ($hotelPtype != '' && count($hotelPtype) > 0) {
                    $addHotalPropertyTypesData = array();
                    foreach ($hotelPtype as $HpType) {
                        if ($HpType != '') {
                            if ($HpType == 'Other') {
                                /* insert new purpose */
                                $property_type_oth = $this->input->post('property_type_oth');
                                if ($property_type_oth != '') {
                                    /* check if already in database if not then add */
                                    if ($hPid = $this->hotel_model->getHotelPropertyType($property_type_oth)) {
                                        $hotelPtypeId = $hPid;
                                    } else {
                                        /* add data in database */
                                        $hotelPtype = array('entity_title' => $property_type_oth, 'entity_type' => $this->config->item('attribute_property_types'));
                                        $hotelPtypeId = $this->hotel_model->addHotelPropertyType($hotelPtype);
                                    }
                                }
                            } else {
                                $hotelPtypeId = $HpType;
                            }
                            $addHotalPropertyTypesData[] = array('hotel_id' => $hid,'property_type_id' => $hotelPtypeId);
                        }
                    }
                    if (count($addHotalPropertyTypesData) > 0) {
                        if ($this->hotel_model->deleteHotalpropertytype($hid)) {
                            $this->hotel_model->addHotalPropertyTypes($addHotalPropertyTypesData);
                        }
                    }
                }
                /* add property close here */
                # add hotel renovation shedule	
                $nonDelRnvIds = array();
                $renovationSchedules = $this->input->post('rnv_shedule');
                if (count($renovationSchedules) > 0) {
                    foreach ($renovationSchedules as $renovationSchedule) {
                        if (isset($renovationSchedule['id']))
                            $scheduleId = $renovationSchedule['id'];
                        else
                            $scheduleId = '';
                        if ($scheduleId != "")
                            array_push($nonDelRnvIds, $scheduleId);
                        if ($renovationSchedule['date_from'] != "" && $renovationSchedule['date_to'] != "" && $renovationSchedule['renovation_type'] != "" && $renovationSchedule['area_effected'] != "") {
                            $hotelRenovationData = array('hotel_id' => $hid,'date_from' => convert_mysql_format_date($renovationSchedule['date_from']),'date_to' => convert_mysql_format_date($renovationSchedule['date_to']),'renovation_type' => $renovationSchedule['renovation_type'],'area_effected' => $renovationSchedule['area_effected']);
                            $idu = $this->hotel_model->updateHotalRenovationSchedule($hotelRenovationData, $scheduleId);
                            array_push($nonDelRnvIds, $idu);
                        }
                    }
                    // delete non existing data
                    $this->hotel_model->deleteHotalRenovationSchedule($nonDelRnvIds, $hid);
                }
                # add hotel complementary room
                $cmpli_room_night = $this->input->post('cmpli_room_night');
                $cmpli_room_id = $this->input->post('cmpli_room_id');
                $cmpli_date_from = convert_mysql_format_date($this->input->post('cmpli_date_from'));
                $cmpli_date_to = convert_mysql_format_date($this->input->post('cmpli_date_to'));
                $cmpli_upgrade = $this->input->post('cmpli_upgrade');
                $cmpli_exclude_dates = $this->input->post('cmpli_exclude_date');
                if ($cmpli_room_night != "" && $cmpli_date_from != "") {
                    $complimentary_room_data = array('hotel_id' => $hid,'room_night' => $cmpli_room_night,'start_date' => $cmpli_date_from,'end_date' => $cmpli_date_to,'upgrade' => $cmpli_upgrade,
                    );

                    #update   details
                    if ($cmpli_room_id = $this->hotel_model->updateHotalComlimentaryRoom($complimentary_room_data, $hid, $cmpli_room_id)) {
                        $nonDelCmpExIds = array();
                        if (count($cmpli_exclude_dates) > 0) {
                            foreach ($cmpli_exclude_dates as $cmpli_exclude_date) {
                                if (isset($cmpli_exclude_date['id']))
                                    $cmExId = $cmpli_exclude_date['id'];
                                else
                                    $cmExId = '';
                                if ($cmExId != "")
                                    array_push($nonDelCmpExIds, $cmExId);
                                if ($cmpli_exclude_date['from'] != "" && $cmpli_exclude_date['to'] != "") {
                                    $cmpli_excluded_dates = array('cmpl_room_id' => $cmpli_room_id,'exclude_date_from' => convert_mysql_format_date($cmpli_exclude_date['from']),'excluded_date_to' => convert_mysql_format_date($cmpli_exclude_date['to'])
                                    );
                                    $ncmExId = $this->hotel_model->updateHotalComlimentaryRoomExcludedDate($cmpli_excluded_dates, $cmExId);
                                    // function return new added id so we do not have to deltete that one
                                    array_push($nonDelCmpExIds, $ncmExId);
                                }
                            }
                            $this->hotel_model->deleteHotalComlimentaryRoomExcludedDate($nonDelCmpExIds, $cmpli_room_id);
                        }
                    }
                }

                # update hotel payment shedule  option				
                $paymentShedules = $this->input->post('payment_plan'); // multi dimensonal array posted from form
                if (!empty($paymentShedules) && count($paymentShedules) > 0) {
                    $nonDelPaymentIds = array();
                    foreach ($paymentShedules as $paymentShedule) {
                        if (isset($paymentShedule['id']))
                            $pmExId = $paymentShedule['id'];
                        else
                            $pmExId = '';
                        if ($pmExId != "")
                            array_push($nonDelPaymentIds, $pmExId);
                        if ($paymentShedule['payment_value'] != "" && $paymentShedule['payment_option_id'] != "") {
                            $hotelPaymentShedulesData = array('hotel_id' => $hid,'payment_option_id' => $paymentShedule['payment_option_id'],'payment_value' => $paymentShedule['payment_value'],
                            );
                            $npmExId = $this->hotel_model->updateHotalPaymentShedules($hotelPaymentShedulesData, $pmExId);
                            // function return new added id so we do not have to deltete that one
                            array_push($nonDelPaymentIds, $npmExId);
                        }
                    }
                    $this->hotel_model->deleteHotalPaymentShedules($nonDelPaymentIds, $hid);
                }
                # add hotel payment shedule  option end 
                #add hotel cancellation information  
                $lowseason_cancelations = $this->input->post('lowseason_canceled'); // multi dimensonal array posted from form
                $highseason_cancelations = $this->input->post('highseason_canceled'); // multi dimensonal array posted from form
                $nonDelCancellationIds = array();
                if (!empty($lowseason_cancelations) && count($lowseason_cancelations) > 0) {
                    foreach ($lowseason_cancelations as $lowseason_cancel) {
                        if (isset($lowseason_cancel['id']))
                            $cancellationExId = $lowseason_cancel['id'];
                        else
                            $cancellationExId = '';
                        if ($cancellationExId != "")
                            array_push($nonDelCancellationIds, $cancellationExId);
                        if ($lowseason_cancel['before'] != "" && $lowseason_cancel['payment_request'] != "") {
                            $hotelCancellationData = array('hotel_id' => $hid,'cancelled_before' => $lowseason_cancel['before'],'payment_request' => $lowseason_cancel['payment_request'],'seasion' => 'low'
                            );
                            $nCanclExId = $this->hotel_model->updateHotalCancellation($hotelCancellationData, $cancellationExId);
                            // function return new added id so we do not have to deltete that one
                            array_push($nonDelCancellationIds, $nCanclExId);
                        }
                    }
                }
                if (!empty($highseason_cancelations) && count($highseason_cancelations) > 0) {
                    foreach ($highseason_cancelations as $highseason_cancel) {
                        if (isset($highseason_cancel['id']))
                            $cancellationExId = $highseason_cancel['id'];
                        else
                            $cancellationExId = '';
                        if ($cancellationExId != "")
                            array_push($nonDelCancellationIds, $cancellationExId);

                        if ($highseason_cancel['before'] != "" && $highseason_cancel['payment_request'] != "") {
                            $hotelCancellationData = array('hotel_id' => $hid,'cancelled_before' => $highseason_cancel['before'],'payment_request' => $highseason_cancel['payment_request'],'seasion' => 'high'
                            );
                            $nCanclExId = $this->hotel_model->updateHotalCancellation($hotelCancellationData, $cancellationExId);
                            // function return new added id so we do not have to deltete that one
                            array_push($nonDelCancellationIds, $nCanclExId);
                        }
                    }
                }
                $this->hotel_model->deleteHotalCancellation($nonDelCancellationIds, $hid);
                #add hotel cancellation information end 
                $aplicable_child_age_group = $this->input->post('aplicable_child_age_group');
                if ($aplicable_child_age_group != "") {
                    $uchData = array('child_age_group_id' => $aplicable_child_age_group);
                    $this->hotel_model->updateHotelDetails($hid, $uchData);
                }
                #add hotel contract information
                $contract_start_date = $this->input->post('contract_start_date');
                $contract_end_date = $this->input->post('contract_end_date');
                $contract_signed_by = $this->input->post('contract_signed_by');
                $contract_file_name = '';
                if (isset($_FILES['contract_file']) && is_uploaded_file($_FILES['contract_file']['tmp_name']) && $contract_end_date == "" && $contract_signed_by == "") {
                    $data['message'] = '<li> Hotel Contract Details Not Added,Hotel Contract Details Fields are missing! </li>';
                } elseif ($contract_start_date != "" && $contract_end_date != "" && $contract_signed_by != "") {
                    if (isset($_FILES['contract_file']) && is_uploaded_file($_FILES['contract_file']['tmp_name'])) {
                        $config['upload_path'] = $this->contractFileDir;
                        $config['allowed_types'] = 'pdf|jpg|jpeg';
                        $config['max_size'] = '0'; //2 mb , 0 for unlimited
                        $config['max_width'] = '0'; // unlimited
                        $config['max_height'] = '0';
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('contract_file')) {
                            $error = $this->upload->display_errors();
                            $data['message'] = '<li>Notice.Contract file not uploaded because ' . $error . ' , please upload it again, by editing the hotel details!</li>';
                        } else {
                            $uploadedFileDetail = $this->upload->data();
                            $contract_file_name = $uploadedFileDetail['file_name'];
                        }
                    }
                    $hotelContractData = array('hotel_id' => $hid,'start_date' => convert_mysql_format_date($contract_start_date),'end_date' => convert_mysql_format_date($contract_end_date),'signed_by' => $contract_signed_by,'contract_file' => $contract_file_name
                    );
                    if ($this->hotel_model->addHotalContract($hotelContractData)) {
                        if ($contract_file_name != "") {
                            $data['message'] = '<li>Hotel Contract Details Added</li>';
                        } else {
                            $data['message'] = '<li>Hotel Contract Details</li> ';
                        }
                    }
                }

                # edit hotel pricing section					
                $hotelPricingRecords = $this->input->post('pricing'); // multi dimensonal array posted from form
                $hotelRoomsPricingData = array(); // for table hotel_rooms_pricing
                if (!empty($hotelPricingRecords) && count($hotelPricingRecords) > 0) {
                    $nonDelPricingRecordIds = array();
                    foreach ($hotelPricingRecords as $hotelPricingRecord) {
                        //echo $hotelPricingRecord['pricing_id']."<br>";
                        // prepare  records  which will not deleted other record whicg are not submitted will be deleted
                        if (isset($hotelPricingRecord['pricing_id']))
                            $prExId = $hotelPricingRecord['pricing_id'];
                        else
                            $prExId = '';

                        $hotelRroomsPricingDetailsData = array(); // for hotel_rooms_pricing_details table
                        $hotelRoomsPricingComplimentaryData = array(); // for table hotel_rooms_pricing_complimentary

                        if (isset($hotelPricingRecord['room_type']) && $hotelPricingRecord['room_type'] != "" && isset($hotelPricingRecord['invenory']) && $hotelPricingRecord['invenory'] != "") {
                            $hotelRoomsPricingData = array('hotel_id' => $hid,'room_type' => $hotelPricingRecord['room_type'],'inclusions' => $hotelPricingRecord['pricing_inclusions'],'curency_code' => $hotelPricingRecord['currency'],'max_adult' => $hotelPricingRecord['max_adult'],'max_child' => $hotelPricingRecord['max_child'],'inventory' => $hotelPricingRecord['invenory'],'period_from' => convert_mysql_format_date($hotelPricingRecord['period_from_date']),'period_to' => convert_mysql_format_date($hotelPricingRecord['period_to_date']),
                            );
                            $pricing_inserted_id = $this->hotel_model->updateHotalRoomsPricingData($hotelRoomsPricingData, $prExId);
                            if ($pricing_inserted_id) {
                                array_push($nonDelPricingRecordIds, $pricing_inserted_id); // add id to non delete list			
                                // if hotel pricing record added in database // add  room pricing facilities
                                if (isset($hotelPricingRecord['room_facilities'])) {
                                    $pricingRoomFacilities = $hotelPricingRecord['room_facilities'];
                                    if (!empty($pricingRoomFacilities) && count($pricingRoomFacilities) > 0) {
                                        foreach ($pricingRoomFacilities as $roomFacility) {
                                            if ($roomFacility != "") {
                                                $hotelRoomsPricingComplimentaryData[] = array('pricing_id' => $pricing_inserted_id,'cmpl_service_id' => $roomFacility
                                                );
                                            }
                                        }
                                        if (!empty($hotelRoomsPricingComplimentaryData) && count($hotelRoomsPricingComplimentaryData) > 0) {
                                            $this->hotel_model->addHotalRoomPricingComplimentary($hotelRoomsPricingComplimentaryData, $pricing_inserted_id);
                                        }
                                    }
                                }// add  room pricing facilities end
                                // add  room pricing details
                                if (isset($hotelPricingRecord['price_detail'])) {
                                    $roomPriceingDetails = $hotelPricingRecord['price_detail'];
                                    if ($pricing_inserted_id == '')
                                        $pricing_inserted_id = $prExId;
                                    if (!empty($roomPriceingDetails) && count($roomPriceingDetails) > 0) {
                                        $nonDelPricingDetailRecordIds = array();
                                        // store  ids which need to keep after update
                                        foreach ($roomPriceingDetails as $roomPriceingDetail) {
                                            if (isset($roomPriceingDetail['pricing_detId']))
                                                $prDetExId = $roomPriceingDetail['pricing_detId'];
                                            else
                                                $prDetExId = '';
                                            if ($prDetExId != "")
                                                array_push($nonDelPricingDetailRecordIds, $prDetExId);
                                            if ($roomPriceingDetail['market_id'] != "") {
                                                $hotelRroomsPricingDetailsData = array('pricing_id' => $pricing_inserted_id,'market_id' => $roomPriceingDetail['market_id'],'double_price' => $roomPriceingDetail['double_price'],'triple_price' => $roomPriceingDetail['triple_price'],'quad_price' => $roomPriceingDetail['quad_price'],'breakfast_price' => $roomPriceingDetail['breakfast_price'],'half_board_price' => $roomPriceingDetail['half_board_price'],'all_incusive_adult_price' => $roomPriceingDetail['all_incusive'],'extra_adult_price' => $roomPriceingDetail['extra_adult'],'extra_child_price' => $roomPriceingDetail['extra_child'],'extra_bed_price' => $roomPriceingDetail['extra_bed'],
                                                );
                                                $npricingDetId = $this->hotel_model->updateHotelRroomsPricingDetails($hotelRroomsPricingDetailsData, $prDetExId, $pricing_inserted_id);
                                                array_push($nonDelPricingDetailRecordIds, $npricingDetId);
                                            }
                                        }
                                        // delete non  existing  pricing details
                                        if (count($nonDelPricingDetailRecordIds) > 0)
                                            $this->hotel_model->deleteHotalHotelRroomsPricingDetails($nonDelPricingDetailRecordIds, $pricing_inserted_id);
                                    }// end of pricing records if exists	
                                }
                            }
                            // add  room pricing details end
                        }// end if of if   pricing data added							
                        // delete other not existing procing  of the hotel
                    }// end of pricing records for	

                    if (count($nonDelPricingRecordIds) > 0)
                        $this->hotel_model->deleteHotalRoomsPricingData($nonDelPricingRecordIds, $hid);
                }// end of pricing records if exists	
                # add hotel pricing section end
                $identity = $this->input->post('identity');
                $dbidentity = $this->hotel_model->get_dbidentity($identity);
                if ($dbidentity && $identity == ($dbidentity->identifier)) {
                    $data_identity = array('hotel_id' => $hid);
                    $this->hotel_model->update_images($identity, $data_identity);
                }
                if (isset($_FILES['myfile']) && is_uploaded_file($_FILES['myfile']['tmp_name'])) {
				$this->uploadGaleryImage($hid);
				}
                /*  zip folder code close   */
				 $data['hotel_code']= $this->hotel_model->getHotelbyid($hid);
				 $hotelcode = $data['hotel_code']->hotel_code;
			if(isset($_POST['submit_call'])=='save'){
                $data['message'] = '<li>Hotel information updated for  Hotel Code:-"'.$hotelcode.'"</li>';
                $flashdata = array(
                    'flashdata' => $data['message'],'message_type' => 'sucess'
                );
                $this->session->set_userdata($flashdata);
				
					redirect('hotels','refresh');
				}
            } else {
                $data['message'] = 'Data Not valid,Please try again';
                $flashdata = array('flashdata' => $data['message'],'message_type' => 'error'
                );
                $this->session->set_userdata($flashdata);
            }
        }
        $chain_options = getHotelChainsOptions();
        $chain_options['Other'] = 'Other'; // add extra option 
        $propertyTypeOptions = getPropertyTypeOptions('nosel');
        $propertyTypeOptions['Other'] = 'Other';
        $countriesOptions = getCountriesOptions();  /* using helper to get available Countries Options for select dropdown */
        $pcdoce = $this->input->post('country_code');
        $citiesOptions = getCountyCitiesOptions($pcdoce); /* using helper */
        $roomTypes = getRoomTypes(); /* using helper to get available Room Types for select dropdown */
        $roomTypesOptions = array('' => 'Select');
        if ($roomTypes && count($roomTypes) > 0) {
            foreach ($roomTypes as $roomType) {
                $roomTypesOptions[$roomType->id] = $roomType->entity_title;
            }
        }
        $positions = getPositions();
        $positionOptions = array('' => 'Select');
        if ($positions && count($positions) > 0) {
            foreach ($positions as $position) {
                $positionOptions[$position->id] = $position->entity_title;
            }
        }
        $CurrenciesOptions = getCurrencies(); /* using helper to get available Currencies for select dropdown */
        $renovationTypes = getRenovationTypes(); /* using helper to get available Renovation Types for select dropdown */
        $renovationTypesOptions = array();
        if ($renovationTypes && count($renovationTypes) > 0) {
            foreach ($renovationTypes as $renovationType) {
                $renovationTypesOptions[$renovationType->id] = $renovationType->renovation_type;
            }
        }
        $ageGroupsOption = getChildAgeOptions(); /* using helper */
        $cancelationPeriods = getCancelationPeriods();
        $cancelationPeriodsOption = array();
        if ($cancelationPeriods && count($cancelationPeriods) > 0) {
            foreach ($cancelationPeriods as $cancelationPeriod) {
                $cancelationPeriodsOption[$cancelationPeriod->id] = $cancelationPeriod->entity_title;
            }
        }
        $applicablePaymentOptions = getPaymentOptions();
        $paymentOptions = array();
        if ($applicablePaymentOptions && count($applicablePaymentOptions) > 0) {
            foreach ($applicablePaymentOptions as $applicablePaymentOption) {
                $paymentOptions[$applicablePaymentOption->id] = $applicablePaymentOption->entity_title;
            }
        }
        $roomCmplServices = getAvailabeComplementaryServices();
        $roomCmplServicesOptions = array();
        if ($roomCmplServices && count($roomCmplServices) > 0) {
            foreach ($roomCmplServices as $cmplService) {
                $roomCmplServicesOptions[$cmplService->id] = $cmplService->entity_title;
            }
        }
        $marketsList = getMarkets();
        $marketOptions = array();
        if ($marketsList && count($marketsList) > 0) {
            foreach ($marketsList as $market) {
                $marketOptions[$market->id] = $market->entity_title;
            }
        }
        $scity = $this->input->post('city') != '' ? $this->input->post('city') : '';
        if ($scity != "") {
            $districtOptions = getCityDistrictOptions($scity, 'sel');
        } else {
            $districtOptions = array('' => 'Select');
        }
        $hotelContactInfo = $this->hotel_model->getHotalContactInfo($hid);
        $hotelContractInfo = $this->hotel_model->getHotalContract($hid);
//        dump($hotelContractInfo);
        $hotalComlimentaryRoom = $this->hotel_model->getHotalComlimentaryRoom($hid);
        $hotalComlimentaryRoomExcludedDate = $this->hotel_model->getHotalComlimentaryRoomExcludedDateByHotel($hid);
        $hotalRenovationSchedule = $this->hotel_model->getHotalRenovationSchedule($hid);
        $hotalPaymentPlans = $this->hotel_model->getHotalPaymentShedules($hid);
        $hotalCancellation = $this->hotel_model->getHotalCancellation($hid);
        $lowseason = 0;
        $high_session = 0;
        if(!empty($hotalCancellation)){
        foreach($hotalCancellation as $hotel_session){
            if($hotel_session->seasion == 'low'){
                $lowseason++;
            }elseif($hotel_session->seasion == 'high'){
                $high_session++;
            }
        }
        }
//        echo $high_session;die;
        $cmpli_room_night = ($hotalComlimentaryRoom) ? $hotalComlimentaryRoom->room_night : '';
        /* generate form field for hotel entry form */
        $this->data = array('hotel_name' => array('id' => 'hotel_name2','readonly' => 'readonly','tabindex' => 1,'name' => 'hotel_name2','maxlength' => '100','size' => '100'),
            'status_options' => getHotelStatus(),
            'position_options' => $positionOptions,
            'currency_options' => $CurrenciesOptions,
            'commisioned_options' => array('' => 'Select','1' => 'Yes','0' => 'No'),
            'purpose_options' => getPurposeOptions(),
            'purpose_options_oth' => array('id' => 'purpose_options_oth','class' => 'purpose_options_oth','placeholder' => 'Specify other','data-validation' => 'required,length','data-validation-length' => "4-50",'name' => 'purpose_options_oth'),
            
            'chain_options' => $chain_options,
            'hotel_chain_oth' => array('id' => 'hotel_chain_oth','class' => 'hotel_chain_oth','maxlength' => "50",'placeholder' => 'Specify other','data-validation' => 'required,custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "3-50",'name' => 'hotel_chain_oth'),'property_type_options' => $propertyTypeOptions,'property_type_oth' => array('id' => 'property_type_oth','class' => 'property_type_oth','placeholder' => 'Specify other','data-validation' => 'required,custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "4-50",'name' => 'property_type_oth'),
            
            'star_rating_options' => getStarRatings(),
            'dorak_rating_options' => getDorakRatings(),
            'country_id_options' => $countriesOptions,
            'hotel_address' => array('id' => 'hotel_address','tabindex' => 14,'data-validation' => 'required,alphanumeric,length','data-validation-allowing' => "'\'-_@#:,./() ",'data-validation-length' => "15-150",'data-validation-help' => "Should  be minimum  15  characters ",'name' => 'hotel_address','maxlength' => '150','size' => '150'),
            
            'districtOptions' => $districtOptions,
            'post_code' => array('id' => 'post_code','class' => 'required','tabindex' => 18,'data-validation' => 'required,length,alphanumeric','data-validation-length' => "4-8",'name' => 'post_code','maxlength' => '10','size' => '10'),
            'distance_from_city' => array('name' => 'distance_from_city[1][distance]','data-validation' => 'distance','tabindex' => 20,'data-validation-optional' => 'true','data-validation-help' => "Only numeric or decimal ie. (3,2) value accepted ",'placeholder' => 'Distance'),
            'distance_from_city_name' => array('name' => 'distance_from_city[1][name]','class' => 'g-autofill','tabindex' => 19,'onfocus' => "searchLocation(this)",'id' => "distanc-city",'data-validation' => 'alphanumeric','data-validation-allowing' => "-_@#:,./\() ",'data-validation-optional' => 'true','placeholder' => 'Enter City / Locality Name'),
            'distance_from_airport' => array('name' => 'distance_from_airport[1][distance]','data-validation' => 'distance','tabindex' => 22,'data-validation-optional' => 'true','placeholder' => 'Distance','data-validation-help' => "Only numeric or decimal ie. (3,2) value accepted "),
            'distance_from_airport_name' => array('name' => 'distance_from_airport[1][name]','tabindex' => 21,'data-validation' => 'alphanumeric','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-allowing' => "-_@#:,./\() ",'onfocus' => "searchLocation(this)",'class' => 'g-autofill','data-validation-optional' => 'true','placeholder' => 'Enter Airport Name'),
            'contact_name' => array('name' => 'contact[1][name]','id' => 'contact_1_name','tabindex' => 10,'class' => 'required','data-validation' => 'custom,required,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "min3",'placeholder' => 'Name',
            ),
            'contact_phone' => array('name' => 'contact[1][phone]','tabindex' => 12,'id' => 'contact_1_phone','data-validation' => 'required,phone_number,length','data-validation-length' => "min10",'maxlength' => '20','placeholder' => 'Mobile','class' => 'required',),
            'contact_email' => array('name' => 'contact[1][email]','tabindex' => 11,'id' => 'contact_1_email','data-validation' => 'required,email','class' => 'required','placeholder' => 'Email'),
            'contact_extension' => array('tabindex' => 13,'name' => 'contact[1][extension]','data-validation' => 'length,number','data-validation-optional' => 'true','data-validation-length' => "2-6",'maxlength' => '6','size' => '6','class' => 'required','placeholder' => 'Ext.'),
            'account_number' => array('class' => 'required','data-validation' => 'number,length','data-validation-length' => "5-16",'data-validation-help' => "Minimum  5 digit",'data-validation-optional' => 'true','name' => 'bank[1][account_number]','maxlength' => '16','size' => '20'),
            'account_name' => array('data-validation' => 'custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "3-100",'data-validation-help' => "Minimum  3  characters ",'data-validation-optional' => 'true','name' => 'bank[1][account_name]','maxlength' => '100','size' => '100',),
            'bank_name' => array('data-validation' => 'length','data-validation-length' => "3-100",'data-validation-help' => "Minimum  3  characters ",'data-validation-optional' => 'true','name' => 'bank[1][bank_name]','maxlength' => '100','size' => '100',),
            'bank_address' => array('data-validation' => 'alphanumeric,length','data-validation-allowing' => "'\'-_@#:,./() ",'data-validation-length' => "10-150",'data-validation-optional' => 'true','name' => 'bank[1][bank_address]','data-validation-help' => "Minimum  length 10  characters ",'maxlength' => '100','size' => '100'),
            'branch_name' => array('data-validation' => 'required,custom,length','data-validation-regexp' => "^[a-zA-Z\-\s]*$",'data-validation-length' => "3-100",'data-validation-help' => "Minimum  3  characters",'data-validation-optional' => 'true','name' => 'bank[1][branch_name]','maxlength' => '100','size' => '100'),
            'bank_ifsc_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][bank_ifsc_code]','maxlength' => '30','size' => '30'),
            'iban_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][iban_code]','maxlength' => '30','size' => '30'),
            'branch_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][branch_code]','maxlength' => '30','size' => '30'),
            'swift_code' => array('data-validation' => 'alphanumeric,length','data-validation-length' => "3-20",'data-validation-help' => "Minimum  length 3 ",'data-validation-optional' => 'true','name' => 'bank[1][swift_code]','maxlength' => '30','size' => '30'),
            'roomTypesOptions' => $roomTypesOptions,
            'upgradeOptions' => array('0' => 'No', '1' => 'Yes'),
            'ageGroupsOption' => $ageGroupsOption,
            'cmpli_room_night' => array('data-validation' => 'number','data-validation-optional' => 'true','name' => 'cmpli_room_night','value' => $cmpli_room_night),
            'confrenceUshapeOptions' => array('' => 'Select','1' => '1','2' => '2','3' => '3','4' => '4'),
            'confrenceHsquareOptions' => array('' => 'Select','1' => '1','2' => '2','3' => '3','4' => '4'),
            'cancellation_options' => $cancelationPeriodsOption,
            'payment_options' => $paymentOptions,
            'rnv_shedule_area_effected' => array('name' => 'rnv_shedule[1][area_effected]','id' => 'rnv_shedule[1][area_effected]','value' => set_value('rnv_shedule[1][area_effected]'),'rows' => '1','cols' => '10'),
            'renovation_options' => $renovationTypesOptions,
            'room_cmpl_options' => $roomCmplServicesOptions,
            'pricing_inclusions' => array('name' => 'pricing[1][pricing_inclusions]','id' => 'pricing[1][pricing_inclusions]','value' => set_value('pricing[1][pricing_inclusions]'),'rows' => '5','cols' => '10'),
            'markets_options' => $marketOptions);
        $this->data['galleries'] = $this->hotel_model->get_gallary();
        $this->data['hotalCancellation'] = $hotalCancellation;$this->data['lowsession_count'] = $lowseason;$this->data['highsession_count'] = $high_session;$this->data['hotalPaymentPlans'] = $hotalPaymentPlans;$this->data['comlimentaryRoom'] = $hotalComlimentaryRoom;$this->data['comlimentaryRoomExcludedDate'] = $hotalComlimentaryRoomExcludedDate;$this->data['hotalRenovationSchedule'] = $hotalRenovationSchedule;$this->data['hotelPricing'] = $this->hotel_model->getHotalRoomsPricingData($hid);
        $this->data['hotelContactInfo'] = $hotelContactInfo;$this->data['hotelContractInfo'] = $hotelContractInfo;
        $this->data['identity'] = $this->identityfier;$this->data['hotelinfo'] = $this->hotel_model->getHotelbyid($hid);
        $this->data['propertyids'] = $this->hotel_model->getpropertyid($hid);$this->data['contactinfo'] = $this->hotel_model->getcontactinfobyid($hid);
        $this->data['citydis'] = $this->hotel_model->getdistanceForCity($hid);$this->data['airportdis'] = $this->hotel_model->getdistanceForAirport($hid);
        $this->data['hotelfacilities'] = $this->hotel_model->hotelFacility($hid);$this->data['roomfacility'] = $this->hotel_model->roomFacility($hid);
        $this->data['hotelbankaccounts'] = $this->hotel_model->hotelBankAccount($hid);$this->data['galimages'] = $this->hotel_model->get_gallaryimg($hid);
        $this->data['hotelid'] = $hid;
        $cityi = $this->data['hotelinfo']->city;
        $districtOptions = getCityDistrictOptions($cityi, 'sel');
        $this->data['districtOptions'] = $districtOptions;
        $this->load->view('edit_information_view', $this->data);
    }
    /**
     * @method string deletehotelimg()
     * @todo delete hotel gallery image via ajax.
     * @return : it will return deleted id from the database.
     */
    function deletehotelimg() {
        $imgid = $this->input->post('imgid');
        $hotelgalid = $this->input->post('galid');
        $hotelid = $this->input->post('hid');
        $imageid = $this->hotel_model->deletehotelgalleryimg($imgid, $hotelgalid, $hotelid);
        echo $imageid;
    }
    
    /**
     * @method string uploadGaleryImage()
     * @param $hid(int) : - it is the primary key of hotel table.
     * @todo upload gallery images based on hotels.
     * @return : it will return true if data successfully deleted else false.
     */
	function uploadGaleryImage($hid)
	{
		 if (isset($_FILES['myfile']) && is_uploaded_file($_FILES['myfile']['tmp_name'])) {
                    /*  zip folder code start   */
                    $config['upload_path'] = $this->hotelFileDir;
                    $config['allowed_types'] = 'zip';
                    $config['max_size'] = '0';
                    $this->upload->initialize($config);
                    $this->upload->do_upload('myfile');
                    #Get the uploaded zip file
                    $categories = $this->hotel_model->gallery_detail();
                    if ($categories && count($categories > 0)) {
                        foreach ($categories as $v) {
                            $category[$v['id']] = $v['entity_title'];
                        }
                        $zip_array = array('upload_data' => $this->upload->data());
                        $zip_file = $zip_array['upload_data']['full_path'];
                        $pathpart = pathinfo($zip_file);
                        $file_name_with_underscore = $pathpart['filename'];
                        $folder_name = str_replace('_', ' ', $file_name_with_underscore);
                        $zip = new ZipArchive;
                        #Open the Zip File, and extract it's contents.
                        if ($zip->open($zip_file) === TRUE) {
                            $zip->extractTo($this->hotelFileDir);
                            foreach ($category as $key => $v) {
                                if ($folder_name == $v) {
                                    $images_main = directory_map($this->hotelFileDir . $folder_name);
                                    $counter_main = count($images_main);
                                    if ($counter_main != 1) {
                                        foreach ($images_main as $img_main) {
                                            $pathpart = pathinfo($img_main);
                                            $ext = $pathpart['extension'];
                                            if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                                                $dir = $this->hotelFileDir . trim($folder_name) . '/' . $img_main;
                                                $data_gallery_images = array('hotel_id' => $hid, 'gallery_id' => $key, 'image_name' => $img_main);
                                                $this->hotel_model->insert_images($data_gallery_images);
                                                rename("$dir", "$this->hotelFileDir$img_main");
                                            }
                                        }
                                    }
                                }
                                $images = directory_map($this->hotelFileDir . $folder_name . '/' . trim($v) . '/');
                                $counter = count($images);
                                if ($counter != 1) {
                                    foreach ($images as $img) {
                                        $pathpart = pathinfo($img);
                                        $ext = $pathpart['extension'];
                                        if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                                            $dir = $this->hotelFileDir . $folder_name . '/' . trim($v) . '/' . $img;
                                            $data_galery_image = array('hotel_id' => $hid, 'gallery_id' => $key, 'image_name' => $img);
                                            $this->hotel_model->insert_images($data_galery_image);
                                            rename("$dir", "$this->hotelFileDir$img");
                                        }
                                    }
                                }
                            }
                            $zip->close();
                            if (file_exists($zip_file)) {
                                unlink($zip_file);
                            }
                        }
                    }
                }
				return true;
	}
}