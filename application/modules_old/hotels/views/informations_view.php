<?php   echo $this->session->flashdata('flush_sucess');?>
<?php 
/* get upload max file size from server */
	$upload_size = ini_get('upload_max_filesize');
	$usize = str_replace('M','',$upload_size);
/* get upload max file size from server */
?>
<?php $stepsess= $this->session->userdata('stepsess');?>
<div class="tab-main-hotel cf">
<?php echo form_open_multipart('hotels/information',array('class' => 'hotel_infoadd', 'id' => 'hotel_infoadd')); ?>
<div class="nav3 hotel-info-nav cf">
   <ul class="cf">
     <li id='default-tab' class="current"><a href="#tab-1">Hotel Info</a></li>
     <li><a href="#tab-2">Facilities</a></li>
     <li><a href="#tab-3">Contract</a></li>
     <li><a href="#tab-4">Banking Detail</a></li>
     <li><a href="#tab-5">Pricing</a></li>
     <li><a href="#tab-6">Gallery </a></li>
   </ul>
</div>
 <div class="tab-content2"  id="tab-1">
   <div class="main-part main-partt">
	<div class="main-part-container">
          		<div class="main-hotel">
                	<div class="main-hotel-left">
                    	<div class="main-hotel-box  contact-hotel-box">
                        	<h2>Hotel info</h2>
                            <div class="effect-toogle  effect-toogle-hotel-info cf">
                                	<div class="dd">
                                    <div class="dt-hotel">
                                    <label>Hotel Name<span class="vali-star">*</span></label>
                                    <?php echo form_input($hotel_name); ?>	
                                    </div>
                                    <div class="dt">
                                    <label>Star Rating   </label>
                                     <?php  echo form_dropdown('star_rating',$star_rating_options,$posted_star_rating,'title="Select" tabindex="2" class="selectpicker"')?>     
                                    </div>
                                    </div>
                                    <div class="dt-main">                                    
                                    <div class="dt">
                                    <label>Dorak Rating </label>
                                   <?php  echo form_dropdown('dorak_rating',$dorak_rating_options,$posted_dorak_rating,'class="selectpicker" tabindex="3" title="Select"')?>  
                                    </div>
                                    <div class="dt">
                                    <label>Hotel Chain </label>
                                    <?php  echo form_dropdown('chain_id',$chain_options,$posted_chain_id,'id="hotel_chain"  tabindex="4" class="selectpicker"')?>  
                                   <?php  echo form_input($hotel_chain_oth)?>  
                                    <?php echo form_error('hotel_chain_oth');?>
                                    </div>
                                    <div class="dt">
                                    <label>Property Type  </label>
                                     <?php  echo form_multiselect('property_type[]',$property_type_options,$posted_property_type,'id="property_type" tabindex="5" class="datacheckoptions" multiple title="Select" data-selected-text-format="count > 2"')?>   
                                     <?php  echo form_input($property_type_oth)?> 
                                      <?php echo form_error('property_type_oth'); ?>
                                    </div> 
                                    </div>
                                    <div class="dt-main">                                    
                                    <div class="dt">
                                    <label>Purpose </label>
                                     <?php  echo form_dropdown('purpose',$purpose_options,$posted_purpose,' title="Select" tabindex="6" id="purpose"  class="selectpicker"')?>   
                                     <?php  echo form_input($purpose_options_oth)?> 
                                      <?php echo form_error('purpose_options_oth'); ?>
                                    </div>
                                    <div class="dt">
                                    <label>Currency<span class="vali-star">*</span></label>
                                    <?php  echo form_dropdown('currency',$currency_options,$posted_currency,' title="Select" tabindex="7" data-validation="required" id="currency"  class="selectpicker"')?>   
                                    <?php echo form_error('currency'); ?>
                                    </div>
                                    <div class="dt">
                                    <label>Status<span class="vali-star">*</span> </label>
                                     <?php  //echo form_dropdown('status',$status_options,$posted_status,'  tabindex="8" data-validation="required" id="status" class="selectpicker"')?>

								<select name="status"   tabindex="8" class="selectpicker">                              
									<?php foreach($status_options as $key=>$val):?>                                              
										<?php if(is_array('Enable')){$selected = in_array($val,'Enable') ? " selected " : null;}
										else{$selected="";}
										?><option value="<?php echo removeExtraspace($key);?>"<?=$selected?> ><?php echo removeExtraspace($val);?></option>                              
									<?php endforeach?>
									</select>									 
                                    <?php echo form_error('status'); ?>
                                    </div>
                                    </div>
                                </div>
                        </div>
                            <div class="main-hotel-box  contact-hotel-box">
                                <h2>Contact information</h2>
                                <div class="effect-toogle">                            	
                                    <div class="right-dd contact-inf-detail">
                                        <ul>
                                            <li>Designation<span class="vali-star">*</span></li>
                                            <li>Name<span class="vali-star">*</span></li> 
                                            <li>Email<span class="vali-star">*</span></li> 
                                            <li>Phone<span class="vali-star">*</span></li>    
                                            <li>Ext.</li>
                                        </ul>    
                                        <div class="main-div-inf-detail">
                                            <div class="contact_block">
                                                <div class="contactfield_wrapper">
                                                    <div class="dt-main">
                                                        <div class="dt">
                                                            <?php echo form_dropdown('contact[1][position]', $position_options, $posted_position, ' tabindex="9" data-validation="required" id="contact_1_position"  class="selectpicker"') ?>   
                                                            <?php echo form_error('contact[1][position]'); ?>
                                                        </div>
                                                        <div class="dt"> <?php echo form_input($contact_name); ?> 
                                                            <?php echo form_error('contact[1][name]'); ?>
                                                        </div>
                                                        <div class="dt">
                                                            <?php echo form_input($contact_email); ?> 
                                                            <?php echo form_error('contact[1][email]'); ?>
                                                        </div>
                                                        <div class="dt">
                                                            <?php echo form_input($contact_phone); ?> 
                                                            <?php echo form_error('contact[1][phone]'); ?>
                                                        </div>
                                                        <div class="dt dt_extension"><?php echo form_input($contact_extension); ?> 
                                                            <?php echo form_error('contact[1][extension]'); ?> 								   
                                                        </div>
                                                        <a href="javascript:void(0)" class='add_more_contact'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
                                                        <a href="javascript:void(0)" class="remove_button" style="display: none;"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png" alt="plus"></a>
                                                    </div>															  
                                                </div>
                                                <!--								<div class="add_contact">
                                                                                    <a href="javascript:void(0)" class='add_more_contact'><img src="<?php //echo base_url();  ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
                                                                                   </div>-->
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                       </div>
                    <div class="main-hotel-right">
                    <div class="main-hotel-box  contact-hotel-box">
                        	<h2>Hotel Address</h2>
                            <div class="effect-toogle">
                              	<div class="dd add">
                                    <label>Address <span class="vali-star">*</span></label>
                                   <?php echo form_input($hotel_address); ?> 
                                    <?php echo form_error('hotel_address'); ?>  
                                    </div>
                                    <div class="dt-main bank-add">										
									 <div class="dt">							
                                    <label>Country <span class="vali-star">*</span></label>
                                    <?php echo form_dropdown('country_code',$country_id_options,$posted_country_code,'tabindex="15" data-validation="required" data-live-search="true" id="country_code" class="selectpicker" onchange=getCountryCities(this)')?>   
                                   <?php echo form_error('country_code'); ?>
                                    </div>	
                                    <div class="dt">
                                    <label>City <span class="vali-star">*</span></label>
                                    <div id='city_container'>                                  
                                    <?php  echo form_dropdown('city',$city_options,$posted_city,' tabindex="16" data-validation="required" id="city_name" data-live-search="true" class="selectpicker"  onchange=getCityDistricts(this)')?>   
                                   <?php echo form_error('city'); ?>
                                   </div>                                     
                                    </div>                                   
                                    <div class="dt disc">
                                    <label>District</label>
                                    <div id='district_container'>                                
										<?php  echo form_dropdown('district',$districtOptions,$posted_district,' tabindex="17"  id="district_name" data-live-search="true" class="selectpicker"')?> 
										<?php echo form_error('district'); ?> 
                                   </div>                      
                                    </div>                                    
                                    <div class="dt disc">
                                    <label>Zip Code <span class="vali-star">*</span></label>
                                     <?php echo form_input($post_code); ?> 
									<?php echo form_error('post_code'); ?> 	
                                    </div>
                                    </div>
                                </div>
                        </div>
                        <div class="main-hotel-box  contact-hotel-box">
                        <h2>Distance</h2>
                        <div class="effect-toogle">
                        <div class="dt-main">
                              <div class="dd distance">
                                  <div class="dt_city_wraper">
                                      <div class="dt_row">							 
                                          <div class="dt aiport">											 
                                              <?php echo form_input($distance_from_city_name); ?> 
                                              <?php echo form_error('distance_from_city_name[1][name]'); ?>       	
                                          </div>
                                          <div class="right-dd-box-bank-ifsc">
                                              <?php echo form_input($distance_from_city); ?> 
                                              <?php echo form_error('distance_from_city[1][distance]'); ?>                                         
                                          </div>
                                          <a href="javascript:void(0)" class='add_ctydistance_row'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="add"></a>
                                          <a href="javascript:void(0)" class='remove_link' style="display:none;"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png" alt="add"></a>
                                      </div>
                                  </div>
<!--                                  <div class="add_lnk distance_cty">
                                      <a href="javascript:void(0)" class='add_ctydistance_row'><img src="<?php //echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="add"></a>
                                  </div>-->
                                </div>
                                    <div class="dd distance">
                                          <h2>Airport</h2>
                                          <div class="dt_air_wraper">  
                                              <div class="dt_row">
                                                  <div class="dt aiport">
                                                      <?php echo form_input($distance_from_airport_name); ?> 
                                                      <?php echo form_error('distance_from_airport_name[1][name]'); ?>                                     
                                                  </div>
                                                  <div class="right-dd-box-bank-ifsc">
                                                      <?php echo form_input($distance_from_airport); ?> 
                                                      <?php echo form_error('distance_from_airport[1][distance]'); ?>
                                                  </div>
                                                  <a href="javascript:void(0)" class='add_airdistance_row'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
                                                  <a href="javascript:void(0)" class='remove_link' style="display:none;"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png" alt="add"></a>
                                              </div>
                                              
                                          </div>
                                          </div>
<!--                                    <div class="add_lnk distance_air">
                                    <a href="javascript:void(0)" class='add_airdistance_row'><img src="<?php //echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
                                    </div>-->
                                     </div>
                                  </div>
                           </div>
                        </div>
                    </div>                  
                </div>
    </div>
</div>
   </div>             
 <div class="tab-content2" style="display:none;"  id="tab-2">
    <div class="main-part main-partt">
    	<div class="main-part-container">
          		<div class="main-hotel">
                <div class="main-hotel-left">
                	<div class="main-hotel-box contact-hotel-box">
                        	<h2>Hotel Facilities</h2>
                            <div class="effect-toogle">
                               <div class="facility-box cf">
                                <ul>									
									<?php									 
									$facilities=getAvailabeFacilities();
									$tf=count($facilities);
									if($tf>0)
									{
									$i=1;
									foreach($facilities as $facility)
									{									
									?>
									 <li>  									 
									<input type="checkbox"  data-label="<?php echo $facility->entity_title?>" name="facilities[]" value="<?php echo $facility->id?>" <?php echo set_checkbox('facilities[]', $facility->id); ?> />
								  </li>
									 <?php 
									 if(($i % 3 === 0) && $i > 1 && $tf > $i)
									 {									
									 ?>
									</ul><ul>
								    <?php										
									 }
									 $i++;
									 }
									 }?>
                                    </ul>
                                </div>
                               </div>
                        </div>
                 </div>
                    <div class="main-hotel-right">
                    <div class="main-hotel-box contact-hotel-box contact-hotel-box-scroll">
                     <h2>Room Facilities </h2>						
                    <div class="effect-toogle">
                    		<div class="facility-box facility-box1 cf">
                            	<ul>
								<?php									 
									$cmplServices=getAvailabeComplementaryServices();
									$cmpln=count($cmplServices);
									if($cmpln>0)									
									{
									$k=0;	
									foreach($cmplServices as $cmplService)
									{
									$k++;
									?>
									<li>           
									<input type="checkbox" name="complimentary[]" data-label="<?php echo $cmplService->entity_title?>" value="" <?php echo set_checkbox('complimentary[]', $cmplService->id); ?> />
									
									</li>
									 <?php 
									 if(($k % 2 == 0) && $k > 1 && $cmpln > $k)
									 {									
									 ?>
									</ul><ul>
								    <?php										
									 }																		 
									 }	
									 }?>	
                                   </ul>                                   
                            </div>
                         </div>
                        </div>
                    </div>
                </div>
    </div>
    </div>
   </div>
   <div class="tab-content2" style="display:none;"  id="tab-3">
   <div class="main-part main-partt">
	<div class="main-part-container">
          		<div class="main-hotel">
                	<div class="main-hotel-left">
                    	<div class="main-hotel-box contact-hotel-box ">
                        	<h2>Contract info</h2>
                            <div class="effect-toogle">
                                <div class="contact-info dd">
                                    <div class="ci">                                    	
                                        <label>Start Date</label>
                                            <div class="form-group">
                                            <div class='input-group date pickdate' id='contract_startdate'>											
											<?php echo form_input('contract_start_date',getTodayDate(),' data-validation="date" id="contract_start_date" placeholder="From" data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
											<?php echo form_error('contract_start_date'); ?>                    
                                                 <span class="input-group-addon">
                                                 <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>                                        
                                        
                                    </div>	
                                    <div class="ci">
                                    	<label>  End Date  </label>
                                    	<div class="form-group">
                                            <div class='input-group date' id='contract_enddate'>
                                                 <?php echo form_input('contract_end_date','',' id="contract_end_date" data-validation="date" placeholder="To"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
													<?php echo form_error('contract_end_date'); ?>                               
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ci signby">
                                    	<label>Signed by</label>
                                    	 <?php echo form_input('contract_signed_by'); ?> 
										<?php echo form_error('contract_signed_by'); ?>
                                    </div>	
                                    <div class="upload-now-continfo">
                                    <label> Upload Contract  </label>
									
									 <div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
											 <img src="<?php echo base_url(); ?>assets/themes/default/images/upload-now.jpg" alt="upload"> Upload Now 
										<?php echo form_upload('contract_file','',' data-validation="extension,upload_contract"  data-validation-optional="true" data-validation-allowing="pdf,jpeg,jpg"'); ?> 
										<?php echo form_error('contract_file'); ?>
										<input type="text" class="form-control input-file-postion hotel-con" readonly>
                                   												
										</span>										
                                      </div>
								 </div>
                               </div>
                               </div>
                        </div>
                        <div class="main-hotel-box contact-hotel-box contact-hotel-box-change">
                        	<h2>Complimentary Room</h2>
                            <div class="effect-toogle">
                            	<div class="dd contact-info">
                                	<div class="cr">
                                    <label>Room Night</label>
                                    	<?php  echo form_input($cmpli_room_night)?>   
										 <?php echo form_error('cmpli_room_night'); ?>
                                    </div>
                                   
                                    <div class="cr drange">
                                    	<label>Period</label>
                                        <div class="cr1">
                                    		<div class="form-group">
                                            <div class='input-group date' id='cmpli_date_from_input'>
                                                <?php echo form_input('cmpli_date_from','',' data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" placeholder="From"'); ?> 
												<?php echo form_error('cmpli_date_from'); ?>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="cr1">
                                        <div class="form-group">
                                            <div class='input-group date' id='cmpli_date_to_input'>
                                              <?php echo form_input('cmpli_date_to','',' data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" placeholder="To"'); ?> 
											<?php echo form_error('cmpli_date_to'); ?>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
										<div class="cr custom-upgrade">
                                            <label>Upgradable</label>
                                            <?php echo form_dropdown('cmpli_upgrade', $upgradeOptions, $posted_cmpli_upgrade, ' class="selectpicker"') ?>   
                                            <?php echo form_error('cmpli_upgrade'); ?>
                                        </div>
                                </div>
                                <div class="excluded-date">
                                	<h3>Excluded Date</h3>
									<div class="excluded-date-wraper">
                                    <div class="exdd cf">
                                    	<div class="exdd-box">
                                        
                                        <div class="form-group">
                                            <div class='input-group date' id='cmpli_exclude_date_frm_1'>
                                               <?php echo form_input('cmpli_exclude_date[1][from]','',' data-validation="date" placeholder="From" data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" class="datetimepicker"'); ?> 
													<?php echo form_error('cmpli_exclude_date[1][from]'); ?> 
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        
                                        </div>
                                        <div class="exdd-box">
                                        	 <div class="form-group">
                                            <div class='input-group date' id='cmpli_exclude_date_to_1'>
                                               <?php echo form_input('cmpli_exclude_date[1][to]','',' data-validation="date" placeholder="To" data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
													<?php echo form_error('cmpli_exclude_date[1][to]'); ?>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        </div>                                    
                                       
										</div>
                                    </div>
									<div class="exdd-box add-icon">
                                       <a href="javascript:void(0)" class='add-excluded-date-row' ><img alt="add more" src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png"></a>
                                     </div>                               
                                    
                                </div>
                                
                                </div>
                        </div>
                    <div class="main-hotel-box contact-hotel-box contact-hotel-box-scroll">
                        	<h2>Renovation Schedule</h2>
                            <div class="effect-toogle">
                             <div class="bank-box">
                          	<div class="renovation_block">
							<div class="scrollbars">
                            	<div class="rs-con">
                                    	<ul class="rs-con1 cf">
                                        	<li>Period</li>
                                            <li>Renovation Type</li>
                                            <li>Areas Effected</li>
                                        </ul>										
										<div class="renovation_wraper">                                        										
										<div class="exdd cf">										
                                    	<div class="exdd-box">
                                        	<div class='input-group date' id='renovation_shedule_from_1'>
                                                <?php echo form_input('rnv_shedule[1][date_from]','',' placeholder="From" data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
												<?php echo form_error('rnv_shedule[1][date_from]'); ?> 
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>           
										</div>
                                        <div class="exdd-box">
											<div class='input-group date' id='renovation_shedule_to_1'>
                                        	 <?php echo form_input('rnv_shedule[1][date_to]','',' data-validation="date" placeholder="To" data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
												<?php echo form_error('rnv_shedule[1][date_to]'); ?> 
												<span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>   
										   </div>
                                        <div class="exdd-box exdd-box2">
                                        	<?php  echo form_dropdown('rnv_shedule[1][renovation_type]',$renovation_options,$posted_renovation_type,'title="Select" class="selectpicker"')?>   
										   <?php echo form_error('rnv_shedule[1][renovation_type]'); ?>
                                        </div>
                                          <div class="exdd-box exdd-box2 exdd-box3">
                                        	<?php echo form_textarea($rnv_shedule_area_effected); ?> 
											<?php echo form_error('rnv_shedule[1][area_effected]'); ?>
                                           </div> 
										   </div>									  								  
                                    </div>
									<a href="javascript:void(0)" class='add_renovation_row' ><img alt="add more" src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png"></a>           
                                </div>
                                </div>
								 </div>
                                </div>
                                </div>
                        </div>
					</div>
                    <div class="main-hotel-right">
					
					  <div class="main-hotel-box contact-hotel-box contact-hotel-box-scroll">
                        	<h2>Child</h2>
                            <div class="effect-toogle">
							   <div class="lscandd">									 
                                    	<div class="lscan">
                                        	 <?php  echo form_dropdown('aplicable_child_age_group',$ageGroupsOption,'',' title="Select age group" class="selectpicker"')?>   
										    <?php echo form_error('aplicable_child_age_group'); ?>  
                                     </div>
							</div>
                            </div>
                        </div>
						<div class="main-hotel-box contact-hotel-box contact-hotel-box-scroll">
                       <h2>Payment Plan</h2>
                        <div class="effect-toogle">
						<div class="contact-hotel-box-scrollt">
									<div class="contact-cancellation">										
										<div class="lscandd cf">
										<div class="payment_plan_wraper">	
										  <div class="dt-row">									
											<div class="lscan">
											<?php  echo form_dropdown('payment_plan[1][payment_option_id]',$payment_options,'','title="Select" class="selectpicker"')?>   
											   <?php echo form_error('payment_plan[1][payment_option_id]'); ?>											 
											</div>
											<div class="lscan ">
												<?php echo form_input('payment_plan[1][payment_value]','',' data-validation="number" data-validation-allowing="range[0.005;100],float" data-validation-allowing="float" data-validation-optional="true"  placeholder="0%"'); ?> 
												 <?php echo form_error('payment_plan[1][payment_value]'); ?>
											  </div>											
											</div>	
											</div>	
											 <div class="lscan add-icon">
												<a href="javascript:void(0)" class='add_payment_row'><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png"></a>
											</div>
										</div>								  
									</div>
						  </div>
						 </div>                        
                     </div>
                    <div class="main-hotel-box contact-hotel-box contact-hotel-box-scroll">
                     <h2>Cancellation </h2>
                    <div class="effect-toogle">
                    <div class="contact-hotel-box-scrollt">  
					<div class="cancellation_block">
                            	<div class="contact-cancellation">
                                <h3>Low Season</h3>
                                	<ul class="cf contact-cancellation-title">
                                    	<li>Cancelled Before  </li>
                                        <li> Payment Request  </li>
                                    </ul>
                                    <div class="lscandd cf">
									<div class="lowsession_wraper">	
									  <div class="dt-row">									
                                    	<div class="lscan">
                                        <?php  echo form_dropdown('lowseason_canceled[1][before]',$cancellation_options,'','title="Select" class="selectpicker"')?>   
										  <?php echo form_error('lowseason_canceled[1][before]'); ?>
                                        </div>
                                        <div class="lscan ">
                                        <?php echo form_input('lowseason_canceled[1][payment_request]','',' data-validation="number" data-validation-allowing="range[0.005;100],float" data-validation-allowing="float" data-validation-optional="true"  placeholder="0%"'); ?> 
										 <?php echo form_error('lowseason_canceled[1][payment_request]'); ?>
										 </div>		  
										</div>	
										</div>	
										 <div class="lscan add-icon">
                                        <a href="javascript:void(0)" class='add_lowsession_row'><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="contact-cancellation">
                                <h3>High Season</h3>
                                	<ul class="cf contact-cancellation-title">
                                    	<li>Cancelled Before  </li>
                                        <li> Payment Request  </li>
                                    </ul>
                                    <div class="lscandd cf">
									<div class="highsession_wraper">
									<div class="dt-row">
                                    	<div class="lscan">
                                        	 <?php  echo form_dropdown('highseason_canceled[1][before]',$cancellation_options,'','title="Select" class="selectpicker"')?>   
										    <?php echo form_error('highseason_canceled[1][before]'); ?>   
                                        </div>
                                        <div class="lscan ">
										<?php echo form_input('highseason_canceled[1][payment_request]','',' data-validation="number" data-validation-allowing="range[0.005;100],float" data-validation-allowing="float" data-validation-optional="true"  placeholder="0%"'); ?> 
											  <?php echo form_error('highseason_canceled[1][payment_request]'); ?>	
                                        </div>     
										</div>	
										</div>	
										<div class="lscan add-icon">
                                        	<a href="javascript:void(0)" class='add_highsession_row' ><img alt="add more" src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png"></a>
                                        </div>	 
                                    </div>                                    
                                </div>
                       </div>
                         </div>
                         </div>
                        </div> 
                    </div>         
                </div>
    </div>
</div>
   </div>
    
<div class="tab-content2" style="display:none;"  id="tab-4">
   		 <div class="main-part main-partt">
         <div class="main-part-container">
    		<div class="main-hotel-box main-hotel-box1 main-hotel-box1-room contact-hotel-box-change">
                        	<h2>Hotel Banking Details</h2>
                            	<div class="bank-details-main">
								<div class="bnkfield_wrapper">
                                	<div class="bank-details-main-box cf">
									<div class="n-row">
									<div class="bank-details-main-input-box">
                                        	<label>Iban Code  </label>
                                           <?php echo form_input($iban_code); ?> 
											<?php echo form_error('bank[1][iban_code]'); ?> 
											</div>
                                    	<div class="bank-details-main-input-box">
                                        	<label>Account Number     </label>
                                           <?php echo form_input($account_number); ?> 
											<?php echo form_error('bank[1][account_number]'); ?> 
											</div>
                                            <div class="bank-details-main-input-box">
                                            <label>Account Name</label>
                                           <?php echo form_input($account_name); ?> 
											<?php echo form_error('bank[1][account_name]'); ?> 
                                            </div>
                                            <div class="bank-details-main-input-box">
                                            <label>Bank Name   </label>
                                              <?php echo form_input($bank_name); ?> 
											<?php echo form_error('bank[1][bank_name]'); ?>  
											</div>
                                            <div class="bank-details-main-input-box">
                                            <label>Bank Address </label>
                                              <?php echo form_input($bank_address); ?> 
												<?php echo form_error('bank[1][bank_address]'); ?>  
											</div>                                         
                                    </div> 
									<div class="n-row">
									 <div class="bank-details-main-input-box">
                                            <label> Branch Name</label>
                                             <?php echo form_input($branch_name); ?> 
											<?php echo form_error('bank[1][branch_name]'); ?> 
											</div>
                                    	<div class="bank-details-main-input-box">
                                        	<label>Bank Branch Code    </label>
                                           <?php echo form_input($branch_code); ?> 
											<?php echo form_error('bank[1][branch_code]'); ?> 
											</div>
                                            <div class="bank-details-main-input-box">
                                            <label>IFSC  Code</label>
                                             <?php echo form_input($bank_ifsc_code); ?> 
											<?php echo form_error('bank[1][bank_ifsc_code]'); ?> 
											</div>
                                            <div class="bank-details-main-input-box">
                                            <label>SWIFT Code   </label>
                                              <?php echo form_input($swift_code); ?> 
											<?php echo form_error('bank[1][swift_code]'); ?>  
											</div>                                           
                                      </div> 
									  <a class="remove_button" style="display:none;" href="javascript:void(0);"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
									 </div>										 
                                    	
                                  </div>
                                 <div class="plus-red">
                                   <a href="javascript:void(0);" class='bnkadd_button'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="Add more"></a>
                                  </div>
                        </div>
     </div>
   </div>
     </div>
   </div>	
    <div class="tab-content2" style="display:none;"  id="tab-5">
   			<div class="main-part main-partt">
         <div class="main-part-container">
    		<div class="main-hotel-box main-hotel-box1 main-hotel-box1-room contact-hotel-box-change">
                        	<div class="room_type_details_title"><h2>Room Type Detail</h2>
							<div class="add_room_type_block"><a href="javascript:void(0)">Add Room Type</a></div>
                            </div>
							<div class="room_type_block_wraper">
								<div class="price-rate-main-page cf cf-row">
                            	<div class="price-room-main-page">
                                	<div class="prive-room-box-left">
                                    	<div class="dd">
                                        	<div class="dt rp-dt">
                                            	<label>Room Type</label>
                                            		<?php  echo form_dropdown('pricing[1][room_type]',$roomTypesOptions,'','title="Select" class="selectpicker"')?>   
													<?php echo form_error('pricing[1][room_type]'); ?>
											</div>
                                           
                                            <div class="dt rp-dt">
                                            	<label>Currency  </label>
                                            	 <?php  echo form_dropdown('pricing[1][currency]',$currency_options,'',' title="Select" class="selectpicker"')?>   
												<?php echo form_error('pricing[1][currency]'); ?>
                                            </div>
                                            <div class="dt inventory-ty">
                                            <label>Inventory  </label>
                                                 <?php echo form_input('pricing[1][invenory]','','data-validation-optional="true" data-validation="number"'); ?> 
												<?php echo form_error('pricing[1][invenory]'); ?> 	
                                            </div>
                                            <div class="dt rp-dt1">
                                            	<label>Complimentary</label>												
												<?php  echo form_multiselect('pricing[1][room_facilities][]',$room_cmpl_options,'',' class="datacheckroom" multiple title="Select" ')?> 
                        						<?php echo form_error('pricing[1][room_facilities][]'); ?>
                                            </div>
                                        </div>
                                        <div class="dd">
                                        	<div class="dt inventory-ty">
                                            	<label>Max Adult   </label>
                                                  <?php echo form_input('pricing[1][max_adult]','','data-validation-optional="true" data-validation="number"'); ?> 
												 <?php echo form_error('pricing[1][max_adult]'); ?>                                        
                                            </div>
                                            <div class="dt inventory-ty">
                                            	<label>Max Child   </label>
                                                 <?php echo form_input('pricing[1][max_child]','','data-validation-optional="true" data-validation="number"'); ?> 
												 <?php echo form_error('pricing[1][max_child]'); ?> 
                                            </div>
                                            
                                            <div class="price-dta-ran">
                                            <label>Period</label>
                                            <div class="dt pdtrn">
                                            <div class="form-group">
                                            <div class='input-group date' id='period_from_date_1'>
                                                  <?php echo form_input('pricing[1][period_from_date]','',' data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" placeholder="From"'); ?> 
												<?php echo form_error('pricing[1][period_from_date]'); ?> 
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                             </div>
                                              <div class="dt pdtrn">
                                             <div class="form-group">
                                            <div class='input-group date' id='period_to_date_1'>
                                                <?php echo form_input('pricing[1][period_to_date]','',' data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" placeholder="To"'); ?> 
												<?php echo form_error('pricing[1][period_to_date]'); ?>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="prive-room-box-right">  
                                    <div class="dd">
                                    <label>Inclusions</label>
                                    	<?php echo form_textarea($pricing_inclusions); ?> 
										<?php echo form_error('pricing[1][pricing_inclusions]'); ?>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="price-rate-main-page1">
                                	<table id="price_rate_table_1" width="100%" border="0" cellspacing="1" cellpadding="5" align="center">
                                    <thead>
                                      <tr>
                                        <th width="13%">Market </th>
                                        <th width="9%">Double</th>
                                        <th width="9%">Triple </th>
                                        <th width="9%">Quad </th>
                                        <th width="9%"> Breakfast </th>
                                        <th width="9%">Half Board </th>
                                        <th width="9%">All Inclusive </th>
                                        <th width="9%">Extra Adult</th>
                                        <th width="9%">Extra Child </th>
                                        <th width="9%">Extra Bed</th>
										<th width="6%"><a class="tg-bdy dwn" onclick="tgbdybind(this);">&nbsp;</a></th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                        <td width="13%">
                                            <div class="counter-prive-table-select">
                                            <?php  echo form_dropdown('pricing[1][price_detail][1][market_id]',$markets_options,$posted_market_id,' class="selectpicker"  title="Select" ')?>   
											<?php echo form_error('pricing[1][price_detail][1][market_id]'); ?>
                                            </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
										 <?php echo form_input('pricing[1][price_detail][1][double_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing[1][price_detail][1][double_price]'); ?>                                       
                                        </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing[1][price_detail][1][triple_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
											<?php echo form_error('pricing[1][price_detail][1][triple_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
                                           <?php echo form_input('pricing[1][price_detail][1][quad_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing[1][price_detail][1][quad_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing[1][price_detail][1][breakfast_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing[1][price_detail][1][breakfast_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing[1][price_detail][1][half_board_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing[1][price_detail][1][half_board_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                           <?php echo form_input('pricing[1][price_detail][1][all_incusive]','','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing[1][price_detail][1][all_incusive]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing[1][price_detail][1][extra_adult]','','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing[1][price_detail][1][extra_adult]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing[1][price_detail][1][extra_child]','','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing[1][price_detail][1][extra_child]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int counter-prive-table-int1">
                                             <?php echo form_input('pricing[1][price_detail][1][extra_bed]','','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
											<?php echo form_error('pricing[1][price_detail][1][extra_bed]'); ?> 
                                             </div>                                            
                                        </td>
										 <td width="6%">
                                            <div class="counter-lnk">                                            
                                           <a href="javascript:void(0);" rel='1' onclick="addnerow(this,'1');" class='add-price-row'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="Add more"></a>
											</div>
                                        </td>
                                      </tr>                                      
                                      </tbody>
                                    </table>
                                </div>
                             </div> 
							 </div>
                        </div>
                 
                        </div>
     </div>
   </div> 
<div class="tab-content2" style="display:none;" id="tab-6">
   <div class="main-part main-partt">
	<div class="main-part-container">
	<div class="main-hotel-left">
    <div class="gallery-img-section">
    <div class="main-hotel-box main-hotel-box-gellery contact-hotel-box-change">
                        	<h2>Hotel Gallery</h2>
                            	<div class="right-dd">
								<?php if(!empty($galleries)){?>
								<?php foreach($galleries as $gallery): ?>
                                <div class="upload-sec">								
                                <span><?php echo $gallery->entity_title;?></span>
                                <a href="#myModa<?php echo $gallery->id;?>" data-toggle="modal" data-target="#myModa<?php echo $gallery->id;?>" data-gallery_id = "<?php echo $gallery->id; ?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/upload.jpg" alt="upload" class="modalink">Upload</a>
                                </div>
								<?php endforeach; ?>
								<? } ?>      
                             </div>
                        </div>
			</div>
	</div>
		<div class="main-hotel-right">
			<div class="uploadzip">
					<h2>Upload Zip File </h2>
		<div class="input-group">
			<span class="input-group-btn">
					<span class="btn btn-primary btn-file">
					<img src="<?php echo base_url(); ?>assets/themes/default/images/upload-now.jpg" alt="upload"> Upload Now 
					<?php echo form_upload('myfile','','data-validation="extension,size"  data-validation-optional="true" data-validation-allowing="zip"  data-validation-max-size="2M"'); ?>
					<?php echo form_error();?>
					<input type="text" class="form-control input-file-postion" readonly>
				</span>	
			</span>		                   	
		</div>
		</div>
		</div>
     </div>
  </div>
 </div>
	 <div class="submit-area-bottom">
        <input class="submit-button1" id="hotel-info-submit" type="submit" value="Save"  tabindex="50" onclick="return goToNextTab();">   
    </div>
	<div class="submit-area-bottom" id="nxtcnt">
        <button class="submit-button1"  type="submit" onclick="goToNextTab();" id="nextcntnar"> Next Facilities</button>  
    </div>

	<?php echo form_hidden('identity', $identity); ?>
	<input type="hidden" id="step" name="step">
	<input type="hidden" name="submit_call" id="submit_call">
	<?php 
	/* @stepsess user for save data on tab basis */
	if($stepsess==''){
		$stepsess = '#tab-1';
	}
	?>
	<input type="hidden" id="step1" name="step1" value="<?php echo $stepsess; ?>">
      <?php echo form_close(); ?> 
 </div>   
<?php if(!empty($galleries)){?>
<?php foreach($galleries as $gallery): ?>
<div class="modal fade" id="myModa<?php echo $gallery->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog" role="document">
		<div class="modal-content upload-gallery-main-box">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
			</button>
			<h4 class="modal-title" id="myModalLabel"><?php echo $gallery->entity_title;?></h4>
			<h6>Image file should not be more than <?php echo $usize;?> MB</h6>
		  </div>
		  <div class="modal-body">
			<div class="gallery-upload-img">
				<div class="img-upload-area">
					<?php echo form_open_multipart("hotels/upload/$gallery->id",array('class' => 'dropzone', 'id' => "uploadform$gallery->id")); ?>
						<input type="hidden" id="galeryid" name="galeryid" value="<?php echo $gallery->id; ?>" />
						<input type="hidden" id="gallerysessid" name="gallerysessid" value="<?php echo $identity; ?>" />
						<input type="hidden" id="previewid" name="previewid" value="<?php echo $gallery->id; ?>" />
						<div class="upload-now-continfo">
						</div>
						<?php echo form_close(); ?> 
						<div id="loader-image"></div>
				</div>
				<div class="view-upload-img cf">
				<div id="dropzonePreview<?php echo $gallery->id; ?>"></div>
				
				</div>
			</div>
		  </div>
		</div>
	  </div>
</div>
<?php endforeach; ?>
<?php } ?>

 <!-- http://www.formvalidator.net/#default-validators_help -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/dropzone.js"></script>
<script type="text/javascript">
function isValidform()
{
	var errors;
	 if( !$('#hotel_infoadd').isValid() ) {
    return false;
   } else {
   // The form is valid
  return true;
   }
}

function goToNextTab(){
	var tab1 = $('.nav3 li.current a').attr("href");
	var tab = $('#step1').val();
	 $('#step').val(tab1);//@tab1 used for set value in hidden field for save data on tab basis ur
}


	jQuery(document).ready(function(){
            
	$("#hotel_chain").change(function(){
	$(this).parents().removeClass('has-error');
	});
        
//        $("#contact_1_position").change(function(){
//            var chek = isValidform();
//            
//            if(chek){
//                alert('jdfd');
//                $(this).parent().parent().parent().addClass('jeevan');
//                $$(this).parent().parent().parent().removeClass('has-error');
//            }else{
//                alert('sdfsdf');
//                $(this).parent().addClass('has-error');
//                $(this).parent().removeClass('has-success');
//            }
//	});
        
         $('.hotel-con').hide();
         $('input[name=contract_file]').change(function(){
            $(this).parent().parent().parent().addClass('remove-space');
        });
        
        // set value on session to store information on tab on click event close here
		
		jQuery('.upload-sec a').click(function(){
		   var gallaryid = $(this).data('id');
			jQuery("#previewid").val(gallaryid);
	  });
	  
	  var flag = false;
	 jQuery('#hotel-info-submit').click(function(){
		flag = true;
		jQuery("#submit_call").val('submitclick');
		var subval = $('#submit_call').val();
		});
	  if(flag==false){
		 jQuery("#submit_call").val(''); 
	  }
	  
<?php if(!empty($galleries)){?>
<?php foreach($galleries as $gallery): ?>
	Dropzone.autoDiscover = false; // keep this line if you have multiple dropzones in the same page
		$("#uploadform<?php echo $gallery->id;?>").dropzone({	
		//acceptedFiles: "image/jpeg",
		url: '<?php echo base_url();?>hotels/upload',
		uploadMultiple: true,
		acceptedFiles: ".jpeg,.jpg,.png,.gif",
		init: function(){
            this.on("error", function(file){if (!file.accepted) this.removeFile(file);
			if(!file.type.match('image.*')) {
			alert("You can upload only image file");
			return false;
			}
			});
			
			this.on("success", function(file, responseText) {
        });
		
		this.on("uploadprogress", function(file, progress) {
			jQuery('.close').hide();
			jQuery('#loader-image').html('<img src="<?php echo base_url(); ?>assets/themes/default/images/loading_bar.gif" alt="Uploading...">');
		});
		
		 this.on("complete", function(file, complete) {		 
			//alert("Upload complete.");
			jQuery('.close').show();			
			jQuery('#loader-image').html('');
			//jQuery('#progressdiv').hide();
			
		 
		}); 
			
        },
		maxFilesize: <?php echo $usize ?>,
		maxThumbnailFilesize: 500,
		dictDefaultMessage: '<img src="<?php echo base_url(); ?>assets/themes/default/images/img-upoad-top-icone.jpg"><h3>Drag & Drop Picture here</h3><img src="<?php echo base_url(); ?>assets/themes/default/images/or-icone.jpg" alt="or" class="or-upload"><span><img alt="upload" src="<?php echo base_url(); ?>assets/themes/default/images/arrow-upload-img.jpg"><input type="button" class="btn btn-default btn-file"> Upload Now</button></span>',
		thumbnailWidth: 47,
		thumbnailHeight: 47,
		maxfilesexceeded: function(file) {
			alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
		},
		
	 clickable: true,
		/* success: function (response) {
			//alert(response.xhr.responseText);
			//var x = JSON.parse(response.xhr.responseText);
			//alert(response);
		}, */
		previewsContainer: '#dropzonePreview<?php echo $gallery->id;?>',
		previewTemplate:'<ul class="cf">' +
            '<li class="dz-details cf" >' +
            '<h5 dz-filename><img style="width: 47;height: 47" data-dz-thumbnail /> <span data-dz-name></span></h5>' +
            '<h6><span data-dz-size></span> <a data-dz-remove href="javascript:undefined;"title="Delete Image"><img src="<?php echo base_url(); ?>assets/themes/default/images/upload-gellery-img-delete.jpg" alt="delete"></span> </a></h6>' +
			 '</li>'+
			 '</ul>',				 
		addRemoveLinks: false,
		removedfile: function(file) {
		var name = file.name; 
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url();?>hotels/deleteimg',
			data: "id="+name+"&galid="+<?php echo $gallery->id;?>
		});
			var _ref;
				return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
		 }	
		});

<?php endforeach;  } ?> 

	$('select.datacheckoptions').multiselect({ 
        numberDisplayed: 1, 
		nonSelectedText: 'Select Property Type'		
    });
	$('select.datacheckroom').multiselect({ 
        numberDisplayed: 1, 
		nonSelectedText: 'Select Complimentry'		
    });
	// form validation
	$.validate({
	form : '#hotel_infoadd',
	modules : 'file',
    decimalSeparator : '.',
	onValidate : function(form) { 
	return customeChecking();	// custome validation added for foerb tab data
    }
	});
	$(document).on('submit','form#hotel_infoadd',function(){
		$('#hotel-info-submit').prop('disabled', 'disabled');// disable the form submit once submitted
		});
	var baseUrl ="<?php echo base_url()?>";
    var x = 2; //Initial field counter
    $('.bnkadd_button').click(function(e){ //Once add button is clicked
			e.preventDefault();   
            x++; //Increment field counter
           var fieldHTML= getNextBankFld(x,baseUrl);
            $('.bnkfield_wrapper').append(fieldHTML); // Add field html
			callbackValidator();
			$('.bnkfield_wrapper > .bank-details-main-box  >.remove_button').show();	
   
    });
    
    $('.bnkfield_wrapper').on('click', '.remove_button', function(e){
		 //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
		
		if(x==2)
		 {
			$('.bnkfield_wrapper > .bank-details-main-box  >.remove_button').hide();	
		 }	
    });
    
    var ccn = 2; //Initial field counter
     $('.contactfield_wrapper').on('click', '.add_more_contact', function(e) { //Once add more contact button is clicked            
		e.preventDefault(); 
                $('.contactfield_wrapper > .dt-main  >.add_more_contact').hide();
                $('.contactfield_wrapper > .dt-main  >.remove_button').show();
            ccn++; //Increment field counter
            getNextContactRow(ccn,baseUrl); // add using ajax  
    });
    
     $('.contactfield_wrapper').on('click', '.remove_button', function(e) {
                //Once remove button is clicked
                e.preventDefault();
                // alert(ccn);
                if (ccn == 3) {
                    $('.contactfield_wrapper > .dt-main  >.remove_button').hide();
                }

                var ParentDiv = $(this).parent('div').parent('div');
                var lastDiv = $(this).parent('div');
                lastDiv.remove();
                $(ParentDiv).find(".add_more_contact").show();
                $(ParentDiv).find(".add_more_contact").not(":last").hide();
                ccn--; //Decrement field counter
            });
    
	
//	 var arw = 2; //Initial field counter 
//     $('.add_airdistance_row').click(function(e){ //Once add more link is clicked      
//            arw++; //Increment field counter
//			e.preventDefault();
//           var fieldHTML= getNextAirRow(arw,baseUrl);
//            $('.dt_air_wraper').append(fieldHTML);  
//			callbackValidator();			
//    });
    
    var arw = 2; //Initial field counter 
    $('.dt_air_wraper').on('click', '.add_airdistance_row', function(e) { //Once add more contact button is clicked
            e.preventDefault();
            $('.dt_air_wraper > .dt_row  >.add_airdistance_row').hide();
            $('.dt_air_wraper > .dt_row  >.remove_link').show();
	    arw++; //Increment field counter
             
           var fieldHTML= getNextAirRow(arw,baseUrl);
            $('.dt_air_wraper').append(fieldHTML);  
	    callbackValidator();			
    });
    
    $('.dt_air_wraper').on('click', '.remove_link', function(e) {
                //Once remove button is clicked
                e.preventDefault();
                if (arw == 2) {
                    $('.dt_air_wraper > .dt_row  >.remove_link').hide();
                }

                var ParentDiv = $(this).parent('div').parent('div');
                var lastDiv = $(this).parent('div');
                lastDiv.remove();
                $(ParentDiv).find(".add_airdistance_row").show();
                $(ParentDiv).find(".add_airdistance_row").not(":last").hide();
                arw--; //Decrement field counter
            });
//    $('.dt_air_wraper').on('click', '.remove_lnk', function(e){
//		 //Once remove button is clicked
//        e.preventDefault();
//        $(this).parent('div').remove(); //Remove field html
//        arw--; //Decrement field counter
//    });
	 var arw = 1; //Initial field counter is 2
     
     $('.dt_city_wraper').on('click', '.add_ctydistance_row', function(e) { //Once add more contact button is clicked
            e.preventDefault();
            $('.dt_city_wraper > .dt_row  >.add_ctydistance_row').hide();
            $('.dt_city_wraper > .dt_row  >.remove_link').show();
	    arw++; //Increment field counter
             
           var fieldHTML= getNextCityRow(arw,baseUrl);
            $('.dt_city_wraper').append(fieldHTML);  
		callbackValidator();			
    });
    
    $('.dt_city_wraper').on('click', '.remove_link', function(e) {
                //Once remove button is clicked
                e.preventDefault();
//                 alert(arw);
                if (arw == 2) {
                    $('.dt_city_wraper > .dt_row  >.remove_link').hide();
                }

                var ParentDiv = $(this).parent('div').parent('div');
                var lastDiv = $(this).parent('div');
                lastDiv.remove();
                $(ParentDiv).find(".add_ctydistance_row").show();
                $(ParentDiv).find(".add_ctydistance_row").not(":last").hide();
                arw--; //Decrement field counter
            });
    
	var lows = 1; //Initial field counter is 2
     $('.add_lowsession_row').click(function(e){ //Once add more link is clicked      
			e.preventDefault();           
		   lows++; //Increment field counter
		  setNextSessionRow(lows,baseUrl,'low','lowsession_wraper'); 	
    });
    $('.lowsession_wraper').on('click', '.remove_lnk', function(e){
		 //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        lows--; //Decrement field counter
    });
	 var hsession = 1; //Initial field counter is 2
     $('.add_highsession_row').click(function(e){ //Once add more link is clicked      
		e.preventDefault();           
		 hsession++; //Increment field counter
		setNextSessionRow(hsession,baseUrl,'high','highsession_wraper'); 
    });
    $('.highsession_wraper').on('click', '.remove_lnk', function(e){
		 //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        hsession--; //Decrement field counter
    });
	 var cr = 1; //Initial field counter is 2
     $('.add_renovation_row').click(function(e){ //Once add more link is clicked      
			e.preventDefault();           
		    cr++; //Increment field counter
			setNextRenovationRow(cr,baseUrl);  
    });
    $('.renovation_wraper').on('click', '.remove_lnk', function(e){
		 //Once remove link is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        cr--; //Decrement field counter
    });
	 var exdr = 1; //Initial field counter is 2
     $('.add-excluded-date-row').click(function(e){ //Once add more link is clicked      
			e.preventDefault();           
		    exdr++; //Increment field counter			 
			setExcludedDateRow(exdr,baseUrl,'<?php echo getTodayDate() ?>'); 
   
		var elIdFrom="#cmpli_exclude_date_frm_"+exdr;
		var elIdTo="#cmpli_exclude_date_to_"+exdr;
		datetimepicker_from_to(elIdFrom,elIdTo);


   });
    $('.excluded-date-wraper').on('click', '.remove_lnk', function(e){
		 //Once remove link is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        exdr--; //Decrement field counter
    });
	var pr = 1; //Initial field counter
     $('.add_payment_row').click(function(e){ //Once add more link is clicked      
			e.preventDefault();           
		   pr++; //Increment field counter
		  setNextPaymentPlanRow(pr,baseUrl); 	
    });
    $('.payment_plan_wraper').on('click', '.remove_lnk', function(e){
		 //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        pr--; //Decrement field counter
    })
	//add room type block
	var nextBlockId = 1; //Initial field counter is 1
     $('.add_room_type_block a').click(function(e){ 
	 //Once add more link is clicked      
		e.preventDefault();           
		 nextBlockId++; //Increment field counter
		 setNextRoomTypeBlock(nextBlockId,baseUrl);	
		$('select.datacheckroom').multiselect({ 
        numberDisplayed: 1, 
		nonSelectedText: 'Select Complimentry'		
		});
    });
    $('.room_type_block_wraper').on('click', '.price-block-remove-lnk', function(e){
		 //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        nextBlockId--; //Decrement field counter
    })
	// pricing section new  price  row end
     $( ".main-hotel-box h2" ).click(function() {
		if($(this).parent('.main-hotel-box').find('.effect-toogle').is(":visible")){
			$(this).addClass('arrow-down');
		}else{
			$(this).removeClass('arrow-down');
		}
		$(this).parent('.main-hotel-box').find('.effect-toogle').toggle('blind', 10);
    });
	$('#contract_startdate').datetimepicker({
                format: 'DD/MM/YYYY',
				allowInputToggle : true,
				//useCurrent:false,
							
            });
	$('#contract_enddate').datetimepicker({
               format: 'DD/MM/YYYY',
			   allowInputToggle : true,
			   useCurrent:false,
        });
		 $('#contract_enddate').data("DateTimePicker").minDate($("#contract_enddate").date);
		$("#contract_startdate").on("dp.change", function (e) {
			   $('#contract_enddate').data("DateTimePicker").minDate(e.date);
		 });
        $("#contract_enddate").on("dp.change", function (e) {
            $('#contract_startdate').data("DateTimePicker").maxDate(e.date);
        });
	$('#renovation_shedule_from_1').datetimepicker({
               format: 'DD/MM/YYYY',
			   allowInputToggle : true,
			   useCurrent:false, 
        });
	$('#renovation_shedule_to_1').datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });	
		
	$("#renovation_shedule_from_1").on("dp.change", function (e) {
          $('#renovation_shedule_to_1').data("DateTimePicker").minDate(e.date);
     });
	$('#renovation_shedule_from_1').data("DateTimePicker").minDate($('#renovation_shedule_to_1').date);
	$('#renovation_shedule_to_1').data("DateTimePicker").minDate($("#renovation_shedule_from_1").date);
	$("#renovation_shedule_to_1").on("dp.change", function (e) {
    $('#renovation_shedule_from_1').data("DateTimePicker").maxDate(e.date);
    });	
	$('#cmpli_date_from_input').datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });	
		$('#cmpli_date_to_input').datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });	
		$("#cmpli_date_from_input").on("dp.change", function (e) {
           $('#cmpli_date_to_input').data("DateTimePicker").minDate(e.date);
		});
		$('#cmpli_date_from_input').data("DateTimePicker").minDate($('#cmpli_date_to_input').date);
		$('#cmpli_date_to_input').data("DateTimePicker").minDate($("#cmpli_date_from_input").date);
		
		$("#cmpli_date_to_input").on("dp.change", function (e) {
            $('#cmpli_date_from_input').data("DateTimePicker").maxDate(e.date);
        });
		$('#cmpli_exclude_date_frm_1').datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });	
		$('#cmpli_exclude_date_to_1').datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });	
	$("#cmpli_exclude_date_frm_1").on("dp.change", function (e) {
           $('#cmpli_exclude_date_to_1').data("DateTimePicker").minDate(e.date);
     });
	$('#cmpli_exclude_date_frm_1').data("DateTimePicker").minDate($('#cmpli_exclude_date_to_1').date);
	$('#cmpli_exclude_date_to_1').data("DateTimePicker").minDate($("#cmpli_exclude_date_frm_1").date);
	$("#cmpli_exclude_date_to_1").on("dp.change", function (e) {
    $('#cmpli_exclude_date_frm_1').data("DateTimePicker").maxDate(e.date);
    });
	 $('#period_from_date_1').datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });	
	$('#period_to_date_1').datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });		
	 $("#period_from_date_1").on("dp.change", function (e) {
           $('#period_to_date_1').data("DateTimePicker").minDate(e.date);
     });
	 $('#period_from_date_1').data("DateTimePicker").minDate($('#period_to_date_1').date);
	$('#period_to_date_1').data("DateTimePicker").minDate($("#period_from_date_1").date);
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
     var input = $(this).parents('.input-group').find(':text'),
       log = numFiles > 1 ? numFiles + ' files selected' : label;
        if( input.length ) {
            input.val(log);
        } 
    });	

 $( ".btn-group .multiselect-container li" ).on('change', 'input[type="checkbox"]', function(){
  var chval=($(this).attr('value'));
  if(chval=='Other')
  {
  $('.btn-group').removeClass('open');
  }
  });				
});

 $(".nav3 li a").click(function(event) {
	 	var chek = isValidform();
	if(chek){
        event.preventDefault();
	    $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content2").not(tab).css("display", "none");
        $(tab).fadeIn();
		var tab1 = tab.replace("#tab-1", "#tab-6");
		$('#step').val(tab1);
		if(tab1=='#tab-6'){	return false;}
		
		
		if(tab1=='#tab-6')
		{
		$( "#nextcntnar" ).html('Next Facilities');	
		//$('#step').val(tab);
		$('#hotel_infoadd').submit();
		
		}
		var tab1 = tab.replace("#tab-2", "#tab-1");
		$('#step').val(tab1);
		if(tab=='#tab-2')
		{
			$( "#nextcntnar" ).html('Next Contract');
			//$('#step').val(tab);
			$('#hotel_infoadd').submit();
			
		}
		var tab1 = tab.replace("#tab-3", "#tab-2");
		$('#step').val(tab1);
		if(tab=='#tab-3')
		{
			$( "#nextcntnar" ).html('Next Banking');
			//$('#step').val(tab);
			$('#hotel_infoadd').submit();
		}
		var tab1 = tab.replace("#tab-4", "#tab-3");
		$('#step').val(tab1);
		if(tab=='#tab-4')
		{
			$( "#nextcntnar" ).html('Next Pricing');
			//$('#step').val(tab);
			$('#hotel_infoadd').submit();
			
		}
		var tab1 = tab.replace("#tab-5", "#tab-4");
		$('#step').val(tab1);
		if(tab=='#tab-5')
		{
			$( "#nextcntnar" ).html('Next Gallery');
			//$('#step').val(tab);
			$('#hotel_infoadd').submit();
			
		}
		 var tab1 = tab.replace("#tab-6", "#tab-5");
		$('#step').val(tab1);		
		if(tab=='#tab-5')
		{
			$( "#nextcntnar" ).html('Hotel Info');
			//$('#step').val(tab);
			$('#hotel_infoadd').submit();
			
		} 
 }		
    });

$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

function getCountryCities(ele){
getCityDistricts($("div#district_container select"));	
var baseUrl ="<?php echo base_url()?>";
var ccode = $(ele).val();
jQuery.ajax({
type: "POST",
url: baseUrl + "index.php/ajaxController/getCountryCities",
data: {country_code: ccode},
success: function(res) {
if (res)
{
jQuery("div#city_container").html(res);
$("div#city_container select").selectpicker();
}
}
});
}

function getCityDistricts(ele){
var baseUrl ="<?php echo base_url()?>";
var cname = $(ele).val();
jQuery.ajax({
type: "POST",
url: baseUrl + "index.php/ajaxController/getCityDistricts",
data: {city_name: cname},
success: function(res) {
if (res)
{
jQuery("div#district_container").html(res);
$("div#district_container select").selectpicker();
}
}
});
}

// Add custom validation rule
$.formUtils.addValidator({

name : 'phone_number',
validatorFunction : function(value, $el, config, language, $form) {
//var filter =/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})?([ .-]?)\2?([ .-]?)([0-9]{4})/;
var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
   if (filter.test(value)) {
	   
        return true;
    }
    else {
        return false;
    }   
  },
  errorMessage : 'Enter valid phone number',
  errorMessageKey: 'badPhoneNumber'
});

$.formUtils.addValidator({
  name : 'distance',
  validatorFunction : function(value, $el, config, language, $form) {
    if(value.indexOf('.')!=-1){ 		
		var filter = /^[0-9,]+\.\d{1,3}$/;
			if (!filter.test(value)) {
			return false;
		}		
		var dval=value.split(".")[0];
		 if(value.split(".")[0].length < 1){                
               return false;
            } 
          if( dval < 0 || dval > 999)
            {
				 return false;
			}		     
          if(value.split(".")[1].length > 2){                
               return false;
            }         
         else
         {
			    return true;
		 } 
		} 
		else
		 {
			 if(value < 1000 && value > 0)
			 {
				return true;
			 }
			 else{
				 return false;
			 }
		}    
  },
  errorMessage : 'Enter valid distance value',
  errorMessageKey: 'badminfloat'
});

$.formUtils.addValidator({
                    name: 'upload_contract',
                    validatorFunction: function(value, $el, config, language, $form) {
                        
                        var signed_value = $("input[name=contract_signed_by]").val();
                        var contract_end_date = $("input[name=contract_end_date]").val();
                        if ((signed_value!='') && (contract_end_date!='')) {
                            return true;
                        }
                        else
                        {
                          return false;  
                            
                        }
                    },
                    errorMessage: 'Contract end date and Signed value is must for upload contract',
                    errorMessageKey: 'badminfloat'
                });

$.formUtils.addValidator({
  name : 'price',
  validatorFunction : function(value, $el, config, language, $form) {
    if(value.indexOf('.')!=-1){ 		
		var filter = /^[0-9,]+\.\d{1,3}$/;
			if (!filter.test(value)) {
			return false;
		}		
		var dval=value.split(".")[0];
		 if(value.split(".")[0].length < 1){                
               return false;
            } 
          if( dval < 0 )
            {
				 return false;
			}		     
          if(value.split(".")[1].length > 2){                
               return false;
            }         
         else
         {
			    return true;
		 } 
		} 
		else
		 {
			 if(value > 0)
			 {
				return true;
			 }
			 else{
				 return false;
			 }
		}    
  },
  errorMessage : 'Enter valid price value',
  errorMessageKey: 'badminfloat'
});

function setNextRoomTypeBlock(nextBlockId,baseUrl)
{
	/* get new price block  row using ajax */		
		jQuery.ajax({
		type: "POST",
		url: baseUrl + "index.php/ajaxController/nextPriceBlock",
		data: {block_num: nextBlockId},
		success: function(res) {
		if (res)
		{			
		$('.room_type_block_wraper').append(res); 
		$('.room_type_block_wraper select.datacheckroom').multiselect({ 
			numberDisplayed: 1,
			nonSelectedText: 'Select Complimentry'			
			});
		$('.room_type_block_wraper input[type="checkbox"]').checkbox({
		checkedClass: 'icon-check',
		uncheckedClass: 'icon-check-empty'
		});
		$('.room_type_block_wraper .selectpicker').selectpicker();
	
		$('#period_from_date_'+nextBlockId).datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });
		$('#period_to_date_'+nextBlockId).datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });
		
		var elIdFrom="#period_from_date_"+nextBlockId;
			var elIdTo="#period_to_date_"+nextBlockId;
			datetimepicker_from_to(elIdFrom,elIdTo)	
			callbackValidator();// for validation
		
		}
		else{
			return '';
			}
		}
	});
	
}

function addNewPriceRow(n,blockId,baseUrl){
	var tableId ='price_rate_table_'+blockId;
	/* get new low price row using ajax */		
		jQuery.ajax({
		type: "POST",
		url: baseUrl + "index.php/ajaxController/getNewPriceLine",
		data: {row_num: n,block_num: blockId},
		success: function(res) {
		if (res)
		{			
		$("#"+tableId+" tbody").append(res); 
		$("#"+tableId+" tbody select").selectpicker();
		callbackValidator();
		}
		else{
			return '';
			}
		}
	});
}; 

function addnerow(e,nrc){
	var baseUrl ="<?php echo base_url()?>";
	// pricing section new  price  row
	 nrc++; //Increment field counter
	 var blockId=$(e).attr( 'rel' );
		addNewPriceRow(nrc,blockId,baseUrl);
	 var pt = $(e).parents("table:first");
	 $(pt).find(".add-price-row").hide();  
}
function removerow(e,nrc){
	var pt = $(e).parents("table:first");
	var par = $(e).parents("tr:first");
	par.remove();
    //nrc--; //Decrement field counter
	$(pt).find(".add-price-row").show();
	$(pt).find(".add-price-row").not(":last").hide();
}

function gsearchLocation(e)
	{
	var baseUrl ="<?php echo base_url()?>";
	$(e).autocomplete({	  
      source: baseUrl + "index.php/ajaxController/googleSearch"
    });	
	}
	
	function tgbdybind(el)
	{
		$(el).closest('table').children('tbody').toggle(); 
		 if($(el).hasClass('dwn')){
				$(el).removeClass('dwn');
				$(el).addClass('upa');
				 
			}
			else{
				$(el).removeClass('upa');
				$(el).addClass('dwn')
			}
	}

 function searchLocation(e) {
                        var ccode = $('#country_code').val();
                        var cid = $('#city_name').val();
//                        alert(cid);
                        var baseUrl = "<?php echo base_url() ?>";
                        jQuery.ajax({
                            type: "POST",
                            url: baseUrl + "index.php/ajaxController/get_location_point",
                            data: {country_code: ccode,city_id: cid},
                            dataType: 'json',
                            success: function(response) {
                                if (response)
                                {   
                                    var location_data = JSON.stringify(response);
                                    var locArray = [];
                                    $.each( $.parseJSON(location_data), function( key, value ) {
                                        locArray.push(value)
                                    });
                                    
                                    $(e).autocomplete({
                                    source: locArray,
                                    minLength: 0,
                                    scroll: true,
                                    response: function(event, locArray) {
                                    // ui.content is the array that's about to be sent to the response callback.
                                    if (locArray.content.length === 0) {
                                       gsearchLocation(e);                                       
                                    } else {
                                        searchLocation(e);
                                    }
                                }
                                }).focus(function() {
                                   $(e).autocomplete("search", "");
                                });
                             }else{
                                  gsearchLocation(e);
                                }
                            }
                        });
                    }
					function autoSave(){
					var baseUrl ="<?php echo base_url()?>";
				  var formData = $("#hotel_infoadd").submit();
			}
					
</script>
 <?php //}?>
                                                               