<div class="main-part-container"> 
<div class="main-hotel">
<?php echo form_open_multipart($importAction,array('class' => 'frmhotels_file', 'id' => 'frmhotels_file'));?>
 <div class="input-group">
	<span class="input-group-btn">
	<span class="btn btn-primary btn-file">
	<img src="<?php echo base_url(); ?>assets/themes/default/images/upload-now.jpg" alt="upload"> Upload excel file
	<?php echo form_upload('hotels_file','',' data-validation-max-size="1M" data-validation="required,extension"   data-validation-allowing="xls,xlsx"'); ?> 
	<?php echo form_error('hotels_file'); ?>
	<input type="text" class="form-control input-file-postion" readonly>
     </span>										
   </div>
<input class="ok" type="submit" value="Start Import">
<?php   echo form_close();?>
  </div>
 </div>
 
 <script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
  <script>
 $(document).ready(function (){
	$.validate({
	form : '#frmhotels_file',
	modules : 'file'
	});

   $('.btn-file :file').on('fileselect', function(event, numFiles, label){
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });	
   
  });
  
  $(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});
  

</script>