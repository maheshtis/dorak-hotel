<?php  echo $this->session->flashdata('flush_sucess');?>
<?php 
/* get upload max file size from server */

	$upload_size = ini_get('upload_max_filesize');
	$usize = str_replace('M','',$upload_size);
	//print_r($hotelContractInfo);die;
	
/* get upload max file size from server */
?>
<?php $stepsess= $this->session->userdata('stepsess');?>
<div class="tab-main-hotel cf">
 <div id="loader"></div>
<?php echo form_open_multipart('hotels/edit_information/'.$hotelid, array('class' => 'hotel_infoadd', 'id' => 'hotel_infoadd')); ?>
<div class="nav3 hotel-info-nav cf">
   <ul class="cf">
     <li id='default-tab'><a href="#tab-1">Hotel Info</a></li>
     <li><a href="#tab-2">Facilities</a></li>
     <li><a href="#tab-3">Contract</a></li>
     <li><a href="#tab-4">Banking Detail</a></li>
     <li><a href="#tab-5">Pricing</a></li>
     <li><a href="#tab-6">Gallery </a></li>
   </ul>
</div>

 <div class="tab-content2"  id="tab-1">
   <div class="main-part main-partt">
  
	<div class="main-part-container">
        	<?php //echo '<pre>'; print_r($star_rating_options);?>
          		<div class="main-hotel">
                	<div class="main-hotel-left">
                    	<div class="main-hotel-box  contact-hotel-box">
                        	<h2>Hotel info</h2>
                            <div class="effect-toogle  effect-toogle-hotel-info cf">
                                	<div class="dd">
                                    <div class="dt-hotel">
                                    <label>Hotel Name<span class="vali-star"></span></label>
                                    <?php echo form_input($hotel_name, ($hotelinfo) ? $hotelinfo->hotel_name :null )?>	
                                    </div>
                                    <div class="dt">
                                    <label>Star Rating   </label>
                                     <?php  echo form_dropdown('star_rating',$star_rating_options,($hotelinfo) ? $hotelinfo->star_rating :null,'title="Select" tabindex="2" class="selectpicker"')?>     
                                    </div>
                                    </div>
                                    <div class="dt-main">                                    
                                    <div class="dt">
                                    <label>Dorak Rating </label>
                                   <?php  echo form_dropdown('dorak_rating',$dorak_rating_options,($hotelinfo) ? $hotelinfo->dorak_rating :null,'class="selectpicker" tabindex="3" title="Select"')?>  
                                    </div>
                                    <div class="dt">
                                    <label>Hotel Chain </label>
                                    <?php  echo form_dropdown('chain_id',$chain_options,($hotelinfo)? $hotelinfo->chain:null,'id="hotel_chain"  tabindex="4" class="selectpicker"')?>  
                                   <?php  echo form_input($hotel_chain_oth)?>  
                                    <?php echo form_error('hotel_chain_oth');?>
                                    </div>
                                    <div class="dt">
                                    <label>Property Type  </label>
									<select name="property_type[]" multiple id="property_type" tabindex="5" class="datacheckoptions" multiple title="Select" data-selected-text-format="count > 2">                              
									<?php foreach($property_type_options as $key=>$val):?>                                              
										<?php if(is_array($propertyids)){$selected = in_array($key,$propertyids) ? " selected " : null;}
										else{$selected="";}
										?><option value="<?php echo removeExtraspace($key);?>"<?=$selected?> ><?php echo removeExtraspace($val);?></option>                              
									<?php endforeach?>
									</select>
                                     <?php // echo form_multiselect('property_type[]',$property_type_options,$hotelinfo->property_types,'id="property_type" tabindex="5" class="datacheckoptions" multiple title="Select" data-selected-text-format="count > 2"')?>   
                                     <?php  echo form_input($property_type_oth)?> 
                                      <?php echo form_error('property_type_oth'); ?>
                                    </div> 
                                    </div>
                                    <div class="dt-main">                                    
                                    <div class="dt">
                                    <label>Purpose </label>
                                     <?php  echo form_dropdown('purpose',$purpose_options,($hotelinfo)? $hotelinfo->purpose:null,' title="Select" tabindex="6" id="purpose"  class="selectpicker"')?>   
                                     <?php  echo form_input($purpose_options_oth)?> 
                                      <?php echo form_error('purpose_options_oth'); ?>
                                    </div>
                                    <div class="dt">
                                    <label>Currency<span class="vali-star">*</span></label>
                                    <?php  echo form_dropdown('currency',$currency_options,($hotelinfo)? $hotelinfo->currency : null,' title="Select" tabindex="7" data-validation="required" id="currency"  class="selectpicker"')?>   
                                    <?php echo form_error('currency'); ?>
                                    </div>
                                    <div class="dt">
                                    <label>Status<span class="vali-star">*</span> </label>
									<select name="status"   tabindex="8" class="selectpicker">  
               
									<?php foreach($status_options as $key=>$val):?>                                              
										<?php if(is_array('Active')){$selected = in_array($val,'Active') ? " selected " : null;}
										else{$selected="";}
										?><option value="<?php echo removeExtraspace($key);?>"<?=$selected?> ><?php echo removeExtraspace($val);?></option>                              
									<?php endforeach?>
									</select>								 
                                    <?php echo form_error('status'); ?>
                                    </div>
                                    </div>
                                </div>
                        </div>
                        <div class="main-hotel-box  contact-hotel-box">
                        	<h2>Contact information</h2>
                            <div class="effect-toogle">                            	
                            	<div class="right-dd contact-inf-detail">
								<ul>
								<li>Designation<span class="vali-star">*</span></li>
								<li>Name<span class="vali-star">*</span></li> 
								<li>Email<span class="vali-star">*</span></li> 
								<li>Phone<span class="vali-star">*</span></li>    
								<li>Ext.</li>
                                </ul>
						 <div class="main-div-inf-detail">
							 <div class="contact_block">
							 <div class="contactfield_wrapper">
							  <?php
								$contactNO=count($contactinfo);
								$ct=1;
								if($contactinfo && $contactNO>0)
								{
								foreach($contactinfo as $contact): 
//                                                                    echo $ct;
							  ?>
								 <div class="dt-main">
								 <?php echo form_hidden('contact['.$ct.'][id]',$contact->contact_id); ?>
									  <div class="dt">
										 <?php  echo form_dropdown('contact['.$ct.'][position]',$position_options, $contact->designation,' tabindex="9" data-validation="required" id="contact_1_position"  class="selectpicker"')?>   
										 <?php echo form_error('contact['.$ct.'][position]'); ?>
									</div>
									<div class="dt"> <?php echo form_input('contact['.$ct.'][name]',$contact->name,'tabindex="10" data-validation="custom,required,length" id="contact_'.$ct.'_name"  class="required" data-validation-length="min3" data-validation-regexp="^[a-zA-Z\-\s]*$" placeholder="Name"' ); ?> 
										<?php echo form_error('contact['.$ct.'][name]'); ?>
									</div>
								   <div class="dt">
									 <?php echo form_input('contact['.$ct.'][email]',$contact->email, 'tabindex="11" data-validation="required,email" id="contact_'.$ct.'_email"  class="required"  placeholder="Email"'); ?> 
									<?php echo form_error('contact['.$ct.'][email]'); ?>
									</div>
									 <div class="dt">
										<?php echo form_input('contact['.$ct.'][phone]',$contact->phone, 'tabindex="12" data-validation="required,phone_number,length" id="contact_'.$ct.'_phone" maxlength = "20"  class="required"  placeholder="Mobile" data-validation-length="min10"'); ?> 
										<?php echo form_error('contact['.$ct.'][phone]'); ?>
									 </div>
									 <div class="dt dt_extension"><?php echo form_input('contact['.$ct.'][extension]',$contact->extension, 'tabindex="13" data-validation="length,number" data-validation-optional="true" data-validation-length = "2-6" maxlength="6" size="6" class="required" placeholder="Ext"'); ?> 
									<?php echo form_error('contact['.$ct.'][extension]'); ?> 								   
									</div>
									<a class="remove_button" href="javascript:void(0);" style="display :<?php echo ($contactNO == 1) ? 'none' : 'block';?>"><img src="<?php echo base_url();?>assets/themes/default/images/small-red-cross.png" alt="close">
										</a>
                                                                     <a href="javascript:void(0)" class='add_more_contact' style="display :<?php echo $ct == $contactNO ? 'block' : 'none';?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
									</div>															  
								 <?php 
								 $ct++;
								 endforeach; 
								}
								 ?>
								 <?php if($ct==1){?>
								  <div class="dt-main">
									  <div class="dt">
										  <?php  echo form_dropdown('contact['.$ct.'][position]',$position_options, '',' tabindex="9" data-validation="required" id="contact_'.$ct.'_position"  class="selectpicker"')?>   
										 <?php echo form_error('contact['.$ct.'][position]'); ?>
									</div>
									<div class="dt"> <?php echo form_input('contact['.$ct.'][name]','','tabindex="10" data-validation="custom,required,length" id="contact_'.$ct.'_name"  class="required" data-validation-length="min3" data-validation-regexp="^[a-zA-Z\-\s]*$" placeholder="Name"'); ?> 
										<?php echo form_error('contact['.$ct.'][name]'); ?>
									</div>
								   <div class="dt">
									 <?php echo form_input('contact['.$ct.'][email]','','tabindex="11" data-validation="required,email" id="contact_'.$ct.'_email"  class="required"  placeholder="Email"'); ?> 
									<?php echo form_error('contact['.$ct.'][email]'); ?>
									</div>
									 <div class="dt">
										<?php echo form_input('contact['.$ct.'][phone]','','tabindex="12" data-validation="required,phone_number,length" id="contact_'.$ct.'_phone" maxlength = "20"  class="required"  placeholder="Mobile" data-validation-length="min10"'); ?> 
										<?php echo form_error('contact['.$ct.'][phone]'); ?>
									 </div>
									 <div class="dt dt_extension"><?php echo form_input('contact['.$ct.'][extension]','','tabindex="13" data-validation="length,number" data-validation-optional="true" data-validation-length = "2-6" maxlength="6" size="6" class="required" placeholder="Ext"'); ?> 
									<?php echo form_error('contact['.$ct.'][extension]'); ?> 								   
									</div>
									<a class="remove_button" href="javascript:void(0);"><img src="<?php echo base_url();?>assets/themes/default/images/small-red-cross.png" alt="close">
										</a>
                                                                      <a href="javascript:void(0)" class='add_more_contact'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
									</div>
								 <?php } ?>
								 </div>
<!--								<div class="add_contact">
                                                                <a href="javascript:void(0)" class='add_more_contact'><img src="<?php //echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
                                                                </div>-->
							</div>
							</div>
                            </div>
						</div>	
                        </div>
                        </div>
                    <div class="main-hotel-right">
                    <div class="main-hotel-box  contact-hotel-box">
                        	<h2>Hotel Address</h2>
                            <div class="effect-toogle">
                                	<div class="dd add">
                                    <label>Address <span class="vali-star">*</span></label>
                                   <?php echo form_input($hotel_address,($hotelinfo)?$hotelinfo->hotel_address:null); ?> 
                                    <?php echo form_error('hotel_address'); ?>  
                                    </div>
                                    <div class="dt-main bank-add">										
									 <div class="dt">							
									<label>Country <span class="vali-star"></span></label>
									<input type="hidden" id="country_code" value="<?php echo $hotelinfo->country_code;?>" tabindex="15">

									<?php echo form_input(array( 'name' => 'country', 'tabindex' => '15',	'value' => ($hotelinfo) ? $hotelinfo->country_name: null,'readonly' => 'readonly'));?>
                                     </div>	
                                    <div class="dt">
                                    <label>City <span class="vali-star"></span></label>                                    
                                    <div id='city_container'>
							<?php echo form_input(array( 'name' => 'city', 'tabindex' => '16', 'id' =>'city_name','value' => ($hotelinfo)? $hotelinfo->city:null,'readonly' => 'readonly'));?>
                                   </div>                                     
                                    </div>                                   
                                    <div class="dt disc">
                                    <label>District</label>
                                    <div id='district_container'>                    
										<?php  echo form_dropdown('district',$districtOptions,($hotelinfo) ? $hotelinfo->district :null,' tabindex="17"  id="district_name" data-live-search="true" class="selectpicker"')?> 
										<?php echo form_error('district'); ?> 
                                   </div>                      
                                    </div>                                    
                                    <div class="dt disc">
                                    <label>Zip Code <span class="vali-star">*</span></label>
                                     <?php echo form_input($post_code,($hotelinfo)? $hotelinfo->post_code : null); ?> 
									<?php echo form_error('post_code'); ?>
                                    </div>
                                    </div>
                                </div>
                        </div>
						<div class="main-hotel-box  contact-hotel-box">
                        <h2>Distance</h2>
                        <div class="effect-toogle">
                        <div class="dt-main">
                              <div class="dd distance">
                                  <div class="dt_city_wraper">
                                      <?php
                                      $totalcd = count($citydis);
                                      $c = 1;
                                      if ($citydis && $totalcd > 0) {
                                          foreach ($citydis as $cityd):
                                              ?>
                                              <div class="dt_row">							 
                                                  <div class="dt aiport">											 
                                                      <?php echo form_hidden('distance_from_city[' . $c . '][id]', $cityd->id); ?>
                                                      <?php echo form_input('distance_from_city[' . $c . '][name]', $cityd->city_name_normal, 'tabindex="18" data-validation="alphanumeric" data-validation-allowing="-_@#:,./() " data-validation-optional = "true" placeholder="Enter City / Locality Name" onfocus="searchLocation(this)" class="g-autofill"'); ?> 
                                                      <?php echo form_error('distance_from_city[' . $c . '][name]'); ?>     
                                                  </div>
                                                  <div class="right-dd-box-bank-ifsc">
                                                      <?php echo form_input('distance_from_city[' . $c . '][distance]', $cityd->dis_city, 'tabindex="19" data-validation="distance" data-validation-optional="true" data-validation-help = "Only numeric or decimal ie. (3,2) value accepted " placeholder="Distance"'); ?> 
                                                      <?php echo form_error('distance_from_city[' . $c . '][distance]'); ?>                                         
                                                  </div>
                                                  <a  href="javascript:void(0)" class="remove_link" style="display :<?php echo ($totalcd == 1) ? 'none' : 'block';?>"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
                                                  <a href="javascript:void(0)" class='add_ctydistance_row' style="display :<?php echo $c == $totalcd ? 'block' : 'none';?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
                                              </div>
                                              <?php
                                              $c++;
                                          endforeach;
                                          ?>
                                      <?php } ?>
                                      <?php if ($c == 1) { ?>
                                          <div class="dt_row">							 
                                              <div class="dt aiport">											  
                                                  <?php echo form_input('distance_from_city[' . $c . '][name]', '', 'tabindex="18" data-validation="alphanumeric" data-validation-allowing="-_@#:,./() " data-validation-optional = "true" placeholder="Enter City / Locality Name" onfocus="searchLocation(this)" class="g-autofill"'); ?> 
                                                  <?php echo form_error('distance_from_city[' . $c . '][name]'); ?>
                                              </div>
                                              <div class="right-dd-box-bank-ifsc">
                                                  <?php echo form_input('distance_from_city[' . $c . '][distance]', '', 'tabindex="19" data-validation="distance" data-validation-optional="true" data-validation-help = "Only numeric or decimal ie. (3,2) value accepted " placeholder="Distance"'); ?> 
                                                  <?php echo form_error('distance_from_city[' . $c . '][distance]'); ?>                                         
                                              </div>
                                              <a  href="javascript:void(0)" class="remove_link" style="display :<?php echo ($totalcd == 1) ? 'none' : 'block';?>"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
                                              <a href="javascript:void(0)" class='add_ctydistance_row'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="add"></a>
                                          </div>
                                      <?php } ?>								
                                  </div>
<!--								<div class="add_lnk distance_cty">
                                  <a href="javascript:void(0)" class='add_ctydistance_row'><img src="<?php //echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="add"></a>
								 </div>-->
                                </div>
                            <div class="dd distance">
                                <h2>Airport</h2>
                                <div class="dt_air_wraper">
                                    <?php
                                    $totalad = count($airportdis);
                                    $a = 1;
                                    if ($airportdis && $totalad > 0) {
                                        ?>
                                        <?php
                                        foreach ($airportdis as $airportd):
                                            ?>
                                            <div class="dt_row">
                                                <div class="dt aiport">
                                                    <?php echo form_hidden('distance_from_airport[' . $a . '][id]', $airportd->id); ?>
                                                    <?php echo form_input('distance_from_airport[' . $a . '][name]', $airportd->city_name_airport, 'tabindex="20" data-validation="alphanumeric"  data-validation-allowing="-_@#:,./() " data-validation-optional = "true" placeholder="Enter Airport Name" onfocus="searchLocation(this)" class="g-autofill"'); ?> 
                                                    <?php echo form_error('distance_from_airport[' . $a . '][name]'); ?>                                     
                                                </div>
                                                <div class="right-dd-box-bank-ifsc">
                                                    <?php echo form_input('distance_from_airport[' . $a . '][distance]', $airportd->dis_airport, 'tabindex="21" data-validation="distance" data-validation-optional="true" data-validation-help = "Only numeric or decimal ie. (3,2) value accepted " placeholder="Distance"'); ?> 
                                                    <?php echo form_error('distance_from_airport[' . $a . '][distance]'); ?>
                                                </div>
                                                <a  href="javascript:void(0)" class="remove_link" style="display :<?php echo ($totalad == 1) ? 'none' : 'block';?>"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
                                                <a href="javascript:void(0)" class='add_airdistance_row' style="display :<?php echo ($a == $totalad) ? 'block' : 'none';?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
                                            </div>
                                            <?php
                                            $a++;
                                        endforeach;
                                        ?>
                                    <?php } ?>
                                    <?php if ($a == 1) {?>
                                        <div class="dt_row">
                                            <div class="dt aiport">
                                                <?php echo form_input('distance_from_airport[' . $a . '][name]', '', 'tabindex="20" data-validation="alphanumeric"  data-validation-allowing="-_@#:,./() " data-validation-optional = "true" placeholder="Enter Airport Name" onfocus="searchLocation(this)" class="g-autofill"'); ?> 
                                                <?php echo form_error('distance_from_airport[' . $a . '][name]'); ?>                                     
                                            </div>
                                            <div class="right-dd-box-bank-ifsc">
                                                <?php echo form_input('distance_from_airport[' . $a . '][distance]', '', 'tabindex="21" data-validation="distance" data-validation-optional="true" data-validation-help = "Only numeric or decimal ie. (3,2) value accepted " placeholder="Distance"'); ?> 
                                                <?php echo form_error('distance_from_airport[' . $a . '][distance]'); ?>
                                            </div>
                                            <a href="javascript:void(0)" class="remove_link" style="display :<?php echo ($totalad == 1) ? 'none' : 'block';?>"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
                                            <a href="javascript:void(0)" class='add_airdistance_row'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
                                        </div>
                                    <?php } ?>
                                </div>
<!--                                <div class="add_lnk distance_air">
                                    <a href="javascript:void(0)" class='add_airdistance_row'><img src="<?php //echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
                                </div>-->
                            </div>
                                     </div>
                           </div>
                        </div>       
                    </div>                   
                </div>
    </div>
</div>
   </div>              
 <div class="tab-content2" style="display:none;"  id="tab-2">
    <div class="main-part main-partt">
    	<div class="main-part-container">
          		<div class="main-hotel">
                <div class="main-hotel-left">
                	<div class="main-hotel-box contact-hotel-box">
                        	<h2>Hotel Facilities</h2>
							
                            <div class="effect-toogle">
                                <div class="facility-box cf">
                                	<ul>									
									<?php 
									$facilities=getAvailabeFacilities();
									$tf=count($facilities);
									if($tf>0)
									{
									$i=1;
									foreach($facilities as $facility)
									{
										if(!empty($hotelfacilities)){

										 if(in_array($facility->id,$hotelfacilities) ){ $checked ='checked'; }else { $checked =''; } 	
										}else{ $checked =''; }
										
									?>
									<?php //echo $facility->facility_id.'<br>';?>
									 <li>
									
									<input type="checkbox"  data-label="<?php echo $facility->entity_title?>" name="facilities[]" value="<?php echo $facility->id?>" <?php echo $checked;?> />
								  </li>
									 <?php 
									 if(($i % 3 === 0) && $i > 1 && $tf > $i)
									 {									
									 ?>
									</ul><ul>
								    <?php										
									 }
									 $i++;
									 }
									 }?>
                                    </ul>
                                </div>
                               </div>
                        </div>
                 </div>
                    <div class="main-hotel-right">
                    <div class="main-hotel-box contact-hotel-box contact-hotel-box-scroll">
                     <h2>Room Facilities </h2>						
                    <div class="effect-toogle">
                    		<div class="facility-box facility-box1 cf">
                            	<ul>
								<?php
									$cmplServices=getAvailabeComplementaryServices();
									$cmpln=count($cmplServices);
									if($cmpln>0)									
									{
									$k=0;	
									foreach($cmplServices as $cmplService)
									{
									$k++;
									if(!empty($roomfacility)){
									if(in_array($cmplService->id,$roomfacility) ){ $checked ='checked'; }else { $checked =''; }
									}
									?>
									<li>           
									<input type="checkbox" name="complimentary[]" data-label="<?php echo $cmplService->entity_title?>" value="<?php echo $cmplService->id?>" <?php echo $checked;?> />
									</li>
									 <?php 
									 if(($k % 2 == 0) && $k > 1 && $cmpln > $k)
									 {									
									 ?>
									</ul><ul>
								    <?php										
									 }																		 
									 }	
									 }
									 ?>	
                                   </ul>                                   
                            </div>
                         </div>
                        </div>
                    </div>
                </div>
    </div>
    </div>
   </div>
   <div class="tab-content2" style="display:none;"  id="tab-3">
   <div class="main-part main-partt">
	<div class="main-part-container">
          		<div class="main-hotel">
                	<div class="main-hotel-left">
                    	<div class="main-hotel-box contact-hotel-box ">
                        	<h2>Contract info</h2>
                            <div class="effect-toogle">
                                <div class="contact-info dd">
                                    <div class="ci">                             	
                                        <label>Start Date</label>
                                            <div class="form-group">
                                            <div class='input-group date pickdate' id='contract_startdate'>											
											<?php echo form_input('contract_start_date',getTodayDate(),' data-validation="date" id="contract_start_date" placeholder="From" data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
											<?php echo form_error('contract_start_date'); ?>                    
                                                 <span class="input-group-addon">
                                                 <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>                                        
                                    </div>	
                                    <div class="ci">
                                    	<label>  End Date  </label>
                                    	<div class="form-group">
                                            <div class='input-group date' id='contract_enddate'>
                                                 <?php echo form_input('contract_end_date','',' id="contract_end_date" data-validation="date" placeholder="To"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
													<?php echo form_error('contract_end_date'); ?>                               
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
									
                                    <div class="ci signby">
                                    	<label>Signed by</label>

										
                                    	 <?php echo form_input('contract_signed_by','','data-validation="required,custom,length" id = "contract_signed_by" data-validation-optional="true" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation-length="3-50" maxlength="50" size="50"'); ?> 

										<?php echo form_error('contract_signed_by'); ?>
                                    </div>	
                                    <div class="upload-now-continfo">
                                    <label> Upload Contract  </label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                <img src="<?php echo base_url(); ?>assets/themes/default/images/upload-now.jpg" alt="upload"> Upload Now 
                                                <?php echo form_upload('contract_file', '', 'data-validation-max-size="1M" data-validation="upload_contract,extension,size" id = "contract_file" data-validation-allowing="pdf,jpeg,jpg" data-validation-optional="true"'); ?> 
                                                <?php echo form_error('contract_file'); ?>
                                                <input type="text" id="contract_file-name" class="form-control input-file-postion hotel-con" readonly>

                                            </span>	
                                        </span>


                                    </div>
								 </div>
                               </div>
                                    
                                <?php if ($hotelContractInfo) {
                                   
                                    ?>
                                    <div class="contract-file-list">

                                        <div class="t-row">
                                            <div class="t-row-head">
                                                <div class="ci th-td">Start Date</div>
                                                <div class="ci th-td">End Date</div>
                                                <div class="ci th-td">Signed By</div>
                                                <div class="ci th-td td-last-col">Contract</div>
                                            </div>
                                        </div>
                                        <span id="custom-contract" style="visibility:hidden; width: 0px;height: 0px;"></span>
                                        <?php
                                        foreach ($hotelContractInfo as $contractInfo) {
                                            $explode = explode('.',$contractInfo->contract_file);
                                            $file_extention = !empty($explode[1]) ? $explode[1] : '';
                                            ?>
                                            <div class="t-row">
                                                <div class="contact-info dd">
                                                    <div class="ci">
                                                        <div class='input-group'>											
                                                            <?php echo format_date_tolocal($contractInfo->start_date) ?>                    
                                                        </div> 
                                                    </div>	
                                                    <div class="ci">
                                                        <div class='input-group'>
                                                            <?php echo format_date_tolocal($contractInfo->end_date); ?>         
                                                        </div>
                                                    </div>
                                                    <div class="ci signby">
                                                        <?php echo $contractInfo->signed_by ?>
                                                    </div>	
                                                    <div class="upload-continfo">
                                                        <a title="Click to view file : <?php echo $contractInfo->contract_file ?>" href="<?php echo base_url() . $this->config->item('upload_contract_file_dir') . $contractInfo->contract_file ?>" target="_blank"><?php echo substr($contractInfo->contract_file, 0, 20) ?><?php echo $dd = (strlen($contractInfo->contract_file) > 20) ? "..".$file_extention : ""; ?></a>
                                                    </div>
                                                </div>	
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
													
                               </div>
                        </div>
                        <div class="main-hotel-box contact-hotel-box contact-hotel-box-change">
                        	<h2>Complimentary Room</h2>
                            <div class="effect-toogle">
                            	<div class="dd contact-info1">
                                	<div class="cr">
									 <?php if($comlimentaryRoom){echo form_hidden('cmpli_room_id', $comlimentaryRoom->cmpl_room_id);}?>											
										
                                    <label>Room Night</label>
                                    	<?php  echo form_input($cmpli_room_night)?>   
										 <?php echo form_error('cmpli_room_night'); ?>
                                    </div>
                                   
                                    <div class="cr drange">
                                    	<label>Period</label>
                                        <div class="cr1">
                                    		<div class="form-group">
                                            <div class='input-group date' id='cmpli_date_from_input'>
                                                <?php												
												$cmpli_date_from=($comlimentaryRoom)?format_date_tolocal($comlimentaryRoom->start_date):'';	
												echo form_input('cmpli_date_from',$cmpli_date_from,' data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" placeholder="From"'); ?> 
												<?php echo form_error('cmpli_date_from'); ?>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="cr1">
                                        <div class="form-group">
                                            <div class='input-group date' id='cmpli_date_to_input'>
                                              <?php 
											  	$cmpli_date_to=($comlimentaryRoom)?format_date_tolocal($comlimentaryRoom->end_date):'';	
											  echo form_input('cmpli_date_to',$cmpli_date_to,' data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" placeholder="To"'); ?> 
											<?php echo form_error('cmpli_date_to'); ?>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
	
									<div class="cr custom-upgrade">
                                    <label>Upgradable</label>
                                    	 <?php  
										 	$comlimentaryRoomupgrade=($comlimentaryRoom)?$comlimentaryRoom->upgrade:'';	
											
										 echo form_dropdown('cmpli_upgrade',$upgradeOptions,$comlimentaryRoomupgrade,' class="selectpicker"')?>   
										   <?php echo form_error('cmpli_upgrade'); ?>
                                    </div>
										
                                </div>
                                <div class="excluded-date">
                                	<h3>Excluded Date</h3>
									<div class="excluded-date-wraper">									
									<?php 
								    $totexd=count($comlimentaryRoomExcludedDate);
									$exi=1;
									if($comlimentaryRoomExcludedDate && $totexd>0)
									{
									foreach($comlimentaryRoomExcludedDate as $comlimentaryExcludedDate)
										{											
										?>										
										<div class="exdd cf">
                                    	<div class="exdd-box">                                        
                                        <div class="form-group">
										  <?php echo form_hidden('cmpli_exclude_date['.$exi.'][id]', $comlimentaryExcludedDate->id);?>											
											
                                            <div class='input-group date' id='cmpli_exclude_date_frm_<?php echo $exi?>'>
                                              <?php echo form_input('cmpli_exclude_date['.$exi.'][from]',format_date_tolocal($comlimentaryExcludedDate->exclude_date_from),' data-validation="date" placeholder="From" data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" class="datetimepicker"'); ?> 
													<?php echo form_error('cmpli_exclude_date['.$exi.'][from]'); ?> 
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>                                        
                                        </div>
                                        <div class="exdd-box">
                                        	 <div class="form-group">
                                            <div class='input-group date' id='cmpli_exclude_date_to_<?php echo $exi?>'>
                                               <?php echo form_input('cmpli_exclude_date['.$exi.'][to]',format_date_tolocal($comlimentaryExcludedDate->excluded_date_to),' data-validation="date" placeholder="To" data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
													<?php echo form_error('cmpli_exclude_date['.$exi.'][to]'); ?>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        </div>                                    
                                       <a href="javascript:void(0)" class="remove_lnk"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
										</div>	
									<?php 
										$exi++;
										}
										}
										if($exi==1)
										{
										?>
                                    <div class="exdd cf">
                                    	<div class="exdd-box">                                        
                                        <div class="form-group">
                                            <div class='input-group date' id='cmpli_exclude_date_frm_<?php echo $exi?>'>                    
						
											  <?php echo form_input('cmpli_exclude_date['.$exi.'][from]','',' data-validation="date" placeholder="From" data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" class="datetimepicker"'); ?> 
													<?php echo form_error('cmpli_exclude_date['.$exi.'][from]'); ?> 
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        
                                        </div>
                                        <div class="exdd-box">
                                        	 <div class="form-group">
                                            <div class='input-group date' id='cmpli_exclude_date_to_<?php echo $exi?>'>
                                               <?php echo form_input('cmpli_exclude_date['.$exi.'][to]','',' data-validation="date" placeholder="To" data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
													<?php echo form_error('cmpli_exclude_date['.$exi.'][to]'); ?>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        </div> 
										<a  href="javascript:void(0)" class="remove_lnk"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
																		
										</div>	
									<?php 
										}										
										?>										
                                    </div>
									<div class="exdd-box add-icon">
                                       <a href="javascript:void(0)" class='add-excluded-date-row' ><img alt="add more" src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png"></a>
                                     </div>
                                </div>
                                
                                </div>
                        </div>
                    <div class="main-hotel-box contact-hotel-box contact-hotel-box-scroll">
                        	<h2>Renovation Schedule</h2>
                            <div class="effect-toogle">
                             <div class="bank-box">
                          	<div class="renovation_block">
							<div class="scrollbars">
                            	<div class="rs-con">
                                    	<ul class="rs-con1 cf">
                                        	<li>Period</li>
                                            <li>Renovation Type</li>
                                            <li>Areas Effected</li>
                                        </ul>										
										<div class="renovation_wraper"> 
									<?php 						
								    $totRen=count($hotalRenovationSchedule);
									$reni=1;
									if($hotalRenovationSchedule && $totRen>0)
									{
									foreach($hotalRenovationSchedule as $renovationSchedule)
										{											
										?>										
										<div class="exdd cf">										
                                    	<div class="exdd-box">
										  <?php echo form_hidden('rnv_shedule['.$reni.'][id]', $renovationSchedule->rnv_id);?>
											
                                        	<div class='input-group date' id='renovation_shedule_from_<?php echo $reni?>'>
                                              	<?php echo form_input('rnv_shedule['.$reni.'][date_from]',format_date_tolocal($renovationSchedule->date_from),' placeholder="From" data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
												<?php echo form_error('rnv_shedule['.$reni.'][date_from]'); ?> 
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>           
										</div>
                                        <div class="exdd-box">
											<div class='input-group date' id='renovation_shedule_to_<?php echo $reni?>'>
                                        	 <?php echo form_input('rnv_shedule['.$reni.'][date_to]',format_date_tolocal($renovationSchedule->date_to),' data-validation="date" placeholder="To" data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
												<?php echo form_error('rnv_shedule['.$reni.'][date_to]'); ?> 
												<span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>   
										   </div>
                                        <div class="exdd-box exdd-box2">
                                        	<?php  echo form_dropdown('rnv_shedule['.$reni.'][renovation_type]',$renovation_options,$renovationSchedule->renovation_type,'title="Select" class="selectpicker"')?>   
										   <?php echo form_error('rnv_shedule['.$reni.'][renovation_type]'); ?>
                                        </div>
                                        <div class="exdd-box exdd-box2 exdd-box3">
                                        	<?php echo form_textarea('rnv_shedule['.$reni.'][area_effected]',$renovationSchedule->area_effected); ?> 
											<?php echo form_error('rnv_shedule['.$reni.'][area_effected]'); ?>
                                         </div> 
										<a href="javascript:void(0)" class="remove_lnk"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
										</div>										  
										<?php
										 $reni++;
										} 								 
									    }
										if($reni==1)
										{										
										?>
										<div class="exdd cf">										
                                    	<div class="exdd-box">
                                        	<div class='input-group date' id='renovation_shedule_from_<?php echo $reni?>'>
                                                <?php echo form_input('rnv_shedule['.$reni.'][date_from]','',' placeholder="From" data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
												<?php echo form_error('rnv_shedule['.$reni.'][date_from]'); ?> 
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>           
										</div>
                                        <div class="exdd-box">
											<div class='input-group date' id='renovation_shedule_to_<?php echo $reni?>'>
                                        	 <?php echo form_input('rnv_shedule['.$reni.'][date_to]','',' data-validation="date" placeholder="To" data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy"'); ?> 
												<?php echo form_error('rnv_shedule['.$reni.'][date_to]'); ?> 
												<span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>   
										   </div>
                                        <div class="exdd-box exdd-box2">
                                        	<?php  echo form_dropdown('rnv_shedule[1][renovation_type]',$renovation_options,'','title="Select" class="selectpicker"')?>   
										   <?php echo form_error('rnv_shedule[1][renovation_type]'); ?>
                                        </div>
                                          <div class="exdd-box exdd-box2 exdd-box3">
                                        	<?php echo form_textarea($rnv_shedule_area_effected); ?> 
											<?php echo form_error('rnv_shedule[1][area_effected]'); ?>
                                           </div> 
										   <a href="javascript:void(0)" class="remove_lnk"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
										
										   </div>
										<?php 
										}										
										?>										   
                                    </div>
									<a href="javascript:void(0)" class='add_renovation_row' ><img alt="add more" src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png"></a>           
                                </div>
                                </div>
								 </div>
                                </div>
                                </div>
                        </div>                      
					
					</div>
                    <div class="main-hotel-right">
					
					  <div class="main-hotel-box contact-hotel-box contact-hotel-box-scroll">
                        	<h2>Child</h2>
                            <div class="effect-toogle">
							   <div class="lscandd">									 
									
                                    	<div class="lscan">
                                        	 <?php  echo form_dropdown('aplicable_child_age_group',$ageGroupsOption,($hotelinfo) ? $hotelinfo->child_age_group_id : null,' title="Select age group" class="selectpicker"')?>   
										    <?php echo form_error('aplicable_child_age_group'); ?>
										   
                                     </div>
								
							</div>
                            </div>
                        </div>
						
						<div class="main-hotel-box contact-hotel-box contact-hotel-box-scroll">
                       <h2>Payment Plan</h2>
                        <div class="effect-toogle">
						<div class="contact-hotel-box-scrollt">					
						
                                                    <div class="contact-cancellation">										
                                                        <div class="lscandd cf">
                                                            <div class="payment_plan_wraper">										
                                                                <?php
                                                                $totPp = count($hotalPaymentPlans);
                                                                $ppc = 1;
                                                                if ($hotalPaymentPlans && $totRen > 0) {
                                                                    foreach ($hotalPaymentPlans as $hotalPaymentPlan) {
                                                                        ?>									

                                                                        <div class="dt-row">									
                                                                            <div class="lscan">
        <?php echo form_hidden('payment_plan[' . $ppc . '][id]', $hotalPaymentPlan->id); ?>
                                                                                <?php echo form_dropdown('payment_plan[' . $ppc . '][payment_option_id]', $payment_options, $hotalPaymentPlan->payment_option_id, 'title="Select" class="selectpicker"') ?>   
                                                                                <?php echo form_error('payment_plan[' . $ppc . '][payment_option_id]'); ?>											 
                                                                            </div>
                                                                            <div class="lscan ">
        <?php echo form_input('payment_plan[' . $ppc . '][payment_value]', $hotalPaymentPlan->payment_value, ' data-validation="number" data-validation-allowing="range[0.005;100],float" data-validation-allowing="float" data-validation-optional="true"  placeholder="0%"'); ?> 
                                                                                <?php echo form_error('payment_plan[' . $ppc . '][payment_value]'); ?>
                                                                            </div>
                                                                            <a href="javascript:void(0)" class="remove_lnk" style="display :<?php echo ($totPp == 1) ? 'none' : 'block';?>"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
                                                                            <a href="javascript:void(0)" class='add_payment_row' style="display :<?php echo $ppc == $totPp ? 'block' : 'none';?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
                                                                        </div>	
        <?php
        $ppc++;
    }
}
if ($ppc == 1) {
    ?>																			
                                                                    <div class="dt-row">									
                                                                        <div class="lscan">
                                                                            <?php echo form_dropdown('payment_plan[' . $ppc . '][payment_option_id]', $payment_options, '', 'title="Select" class="selectpicker"') ?>   
                                                                            <?php echo form_error('payment_plan[' . $ppc . '][payment_option_id]'); ?>											 
                                                                        </div>
                                                                        <div class="lscan ">
                                                                            <?php echo form_input('payment_plan[' . $ppc . '][payment_value]', '', ' data-validation="number" data-validation-allowing="range[0.005;100],float" data-validation-allowing="float" data-validation-optional="true"  placeholder="0%"'); ?> 
                                                                            <?php echo form_error('payment_plan[' . $ppc . '][payment_value]'); ?>
                                                                        </div>
                                                                        <a href="javascript:void(0)" class="remove_lnk"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
                                                                        <a href="javascript:void(0)" class='add_payment_row' style="display :<?php echo $ppc == $totPp ? 'block' : 'none';?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </div>	
<!--                                                            <div class="lscan add-icon">
                                                                <a href="javascript:void(0)" class='add_payment_row'><img alt="add" src="<?php //echo base_url(); ?>assets/themes/default/images/small-green-plus.png"></a>
                                                            </div>-->
                                                        </div>								  
                                                    </div>									
						 
						   </div>
						   </div>                        
                     </div>
                    <div class="main-hotel-box contact-hotel-box contact-hotel-box-scroll">
                     <h2>Cancellation </h2>
                    <div class="effect-toogle">
                    <div class="contact-hotel-box-scrollt">  
					<div class="cancellation_block">                   
                            	<div class="contact-cancellation">
                                <h3>Low Season</h3>
                                	<ul class="cf contact-cancellation-title">
                                    	<li>Cancelled Before  </li>
                                        <li> Payment Request  </li>
                                    </ul>
                                    <div class="lscandd cf">
									<div class="lowsession_wraper">	
								<?php	
								$lowctr=1;
//                                                                echo $totalCanc = count($hotalCancellation);
//                                                                dump($hotalCancellation);
								if($hotalCancellation)
								{
								foreach($hotalCancellation as $cancellation)
								{
                                                                    
								if($cancellation->seasion=='low')
								{
								?>
								  <div class="dt-row">									
                                    	<div class="lscan">
										<?php echo form_hidden('lowseason_canceled['.$lowctr.'][id]', $cancellation->id);?>
											
                                        <?php echo form_dropdown('lowseason_canceled['.$lowctr.'][before]',$cancellation_options,$cancellation->cancelled_before,'title="Select" class="selectpicker"')?>   
										   <?php echo form_error('lowseason_canceled['.$lowctr.'][before]'); ?>
										 
                                        </div>
                                        <div class="lscan ">
                                        	<?php echo form_input('lowseason_canceled['.$lowctr.'][payment_request]',$cancellation->payment_request,' data-validation="number" data-validation-allowing="range[0.005;100],float" data-validation-allowing="float" data-validation-optional="true"  placeholder="0%"'); ?> 
											 <?php echo form_error('lowseason_canceled['.$lowctr.'][payment_request]'); ?>
										  </div>		  
                                        <a href="javascript:void(0)" class="remove_lnk" style="display :<?php echo ($lowsession_count == 1) ? 'none' : 'block';?>"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
                                        <a href="javascript:void(0)" class='add_lowsession_row' style="display :<?php echo $lowctr == $lowsession_count ? 'block' : 'none';?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
										
									</div>	
								<?php 
									$lowctr++; }
								}
								}
							if($lowctr==1)	
								{						
								?>									
									  <div class="dt-row">									
                                    	<div class="lscan">
                                        <?php  echo form_dropdown('lowseason_canceled['.$lowctr.'][before]',$cancellation_options,'','title="Select" class="selectpicker"')?>   
										   <?php echo form_error('lowseason_canceled['.$lowctr.'][before]'); ?>
										 
                                        </div>
                                        <div class="lscan ">
                                        	<?php echo form_input('lowseason_canceled['.$lowctr.'][payment_request]','',' data-validation="number" data-validation-allowing="range[0.005;100],float" data-validation-allowing="float" data-validation-optional="true"  placeholder="0%"'); ?> 
											 <?php echo form_error('lowseason_canceled['.$lowctr.'][payment_request]'); ?>
										  </div>
											<a href="javascript:void(0)" class="remove_lnk" style="display :<?php echo ($lowsession_count == 1) ? 'none' : 'block';?>"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
											<a href="javascript:void(0)" class='add_lowsession_row' style="display :block;"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>								  
										</div>
										<?php } ?>																	
										</div>	
<!--										 <div class="lscan add-icon">
                                        	<a href="javascript:void(0)" class='add_lowsession_row'><img alt="close" src="<?php //echo base_url(); ?>assets/themes/default/images/small-green-plus.png"></a>
                                        </div>-->
                                    </div>
                              
                                </div>
                                <div class="contact-cancellation">
                                <h3>High Season</h3>
                                	<ul class="cf contact-cancellation-title">
                                    	<li>Cancelled Before  </li>
                                        <li> Payment Request  </li>
                                    </ul>
                                    <div class="lscandd cf">
									 
								<div class="highsession_wraper">									
								<?php	
								$highctr=1;
//                                                                $totalHigh = count($hotalCancellation);
								if($hotalCancellation)
								{
								foreach($hotalCancellation as $cancellation)
								{
								if($cancellation->seasion=='high')
								{
								?>
								  <div class="dt-row">									
                                    	<div class="lscan">
										<?php echo form_hidden('highseason_canceled['.$highctr.'][id]', $cancellation->id);?>
									
                                        <?php  echo form_dropdown('highseason_canceled['.$highctr.'][before]',$cancellation_options,$cancellation->cancelled_before,'title="Select" class="selectpicker"')?>   
										   <?php echo form_error('highseason_canceled['.$highctr.'][before]'); ?>
										 
                                        </div>
                                        <div class="lscan ">
                                        	<?php echo form_input('highseason_canceled['.$highctr.'][payment_request]',$cancellation->payment_request,' data-validation="number" data-validation-allowing="range[0.005;100],float" data-validation-allowing="float" data-validation-optional="true"  placeholder="0%"'); ?> 
											 <?php echo form_error('highseason_canceled['.$highctr.'][payment_request]'); ?>
										  </div>		  
                                        <a href="javascript:void(0)" class="remove_lnk" style="display :<?php echo ($highsession_count == 1) ? 'none' : 'block';?>"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
                                        <a href="javascript:void(0)" class='add_highsession_row' style="display :<?php echo $highctr == $highsession_count ? 'block' : 'none';?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
										
									</div>	
									<?php 
										$highctr++; }
									}
									}
									if($highctr==1)
									{										
									?>								
									
									<div class="dt-row">
                                    	<div class="lscan">
                                        	 <?php  echo form_dropdown('highseason_canceled['.$highctr.'][before]',$cancellation_options,'','title="Select" class="selectpicker"')?>   
										    <?php echo form_error('highseason_canceled['.$highctr.'][before]'); ?>
										   
                                        </div>
                                        <div class="lscan ">
										<?php echo form_input('highseason_canceled['.$highctr.'][payment_request]','',' data-validation="number" data-validation-allowing="range[0.005;100],float" data-validation-allowing="float" data-validation-optional="true"  placeholder="0%"'); ?> 
											  <?php echo form_error('highseason_canceled['.$highctr.'][payment_request]'); ?>
									
                                        </div>
                                        <a href="javascript:void(0)" class="remove_lnk" style="display :<?php echo ($highctr == 1) ? 'none' : 'block';?>"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
                                        <a href="javascript:void(0)" class='add_highsession_row' style="display :block;"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="plus"></a>
									
										</div>	
										<?php } ?>	
										</div>	
<!--                                        <div class="lscan add-icon">
                                            <a href="javascript:void(0)" class='add_highsession_row' ><img alt="add more" src="<?php //echo base_url(); ?>assets/themes/default/images/small-green-plus.png"></a>
                                        </div>-->
									 
                                    </div>                                 
                                    
                                </div>
						
                       </div>
                         </div>
                         </div>
                        </div>                 	 
                 
                 
                    </div>         
                </div>
    </div>
</div>
   </div>
    
<div class="tab-content2" style="display:none;"  id="tab-4">
   		 <div class="main-part main-partt">
         <div class="main-part-container">
    		<div class="main-hotel-box main-hotel-box1 main-hotel-box1-room contact-hotel-box-change">
                        	<h2>Hotel Banking Details</h2>
							
                            	<div class="bank-details-main">
								
								<div class="bnkfield_wrapper">
								<?php 
								$totbnkac=count($hotelbankaccounts);
								$bankctr=1;
								if($hotelbankaccounts && $totbnkac>0)
									{?>
								<?php 
								
								foreach($hotelbankaccounts as $hba): 
								
								?>
                                	<div class="bank-details-main-box cf">
									<div class="n-row">
									<div class="bank-details-main-input-box">
									<?php //echo'<pre>'; var_dump($iban_code); ?>
                                        	<label>Iban Code  </label>
											 <?php echo form_hidden('bank['.$bankctr.'][id]', $hba->id); ?>
                                           <?php echo form_input('bank['.$bankctr.'][iban_code]', $hba->iban_code, 'data-validation="alphanumeric,length" data-validation-length="3-20" data-validation-help="Minimum  length 3 " data-validation-optional="true" maxlength="30" size="30"'); ?> 
											<?php echo form_error('bank['.$bankctr.'][iban_code]'); ?> 
											</div>
											
                                    	<div class="bank-details-main-input-box">
                                        	<label>Account Number     </label>
                                           <?php echo form_input('bank['.$bankctr.'][account_number]', $hba->account_number,'class="required" data-validation="number,length" data-validation-length="5-16" data-validation-help="Minimum  5 digit" data-validation-optional="true" maxlength="16" size="20"'); ?> 
											<?php echo form_error('bank[1][account_number]'); ?> 
											</div>
                                            <div class="bank-details-main-input-box">
                                            <label>Account Name</label>
                                           <?php echo form_input('bank['.$bankctr.'][account_name]', $hba->account_name, 'data-validation="custom,length" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation-length="3-100" data-validation-help="Minimum  3  characters" data-validation-optional="true" maxlength="100" size="100"'); ?> 
											<?php echo form_error('bank['.$bankctr.'][account_name]'); ?> 
                                            </div>
                                            <div class="bank-details-main-input-box">
                                            <label>Bank Name   </label>
                                              <?php echo form_input('bank['.$bankctr.'][bank_name]', $hba->bank_name,'data-validation="length" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation-length="3-100" data-validation-help="Minimum  3  characters" data-validation-optional="true" maxlength="100" size="100"'); ?> 
											<?php echo form_error('bank[1][bank_name]'); ?>  
											</div>
                                            <div class="bank-details-main-input-box">
                                            <label>Bank Address </label>
                                              <?php echo form_input('bank['.$bankctr.'][bank_address]', $hba->bank_address,'data-validation="alphanumeric,length" data-validation-allowing="\ -_@#:,./() " data-validation-length="10-150" data-validation-help="Minimum  length 10  characters" data-validation-optional="true" maxlength="100" size="100"'); ?> 
												<?php echo form_error('bank['.$bankctr.'][bank_address]'); ?>  
											</div>                                         
                                     </div>
									<div class="n-row">
									 <div class="bank-details-main-input-box">
                                            <label> Branch Name</label>
                                        
                                             <?php echo form_input('bank['.$bankctr.'][branch_name]', $hba->branch_name,'data-validation="required,custom,length"  data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation-length = "3-150" data-validation-help="Minimum  3  characters" data-validation-optional="true" maxlength="150" size="150"'); ?> 
											<?php echo form_error('bank['.$bankctr.'][branch_name]'); ?> 
											</div>
                                    	<div class="bank-details-main-input-box">
										
                                        	<label>Bank Branch Code    </label>
                                           <?php echo form_input('bank['.$bankctr.'][branch_code]', $hba->branch_code,'data-validation="alphanumeric,length"  data-validation-length="3-20" data-validation-help="Minimum  length 3" data-validation-optional="true" maxlength="30" size="30"'); ?> 
											<?php echo form_error('bank['.$bankctr.'][branch_code]'); ?> 
											</div>
                                            <div class="bank-details-main-input-box">
                                            <label>IFSC  Code</label>
                                             <?php echo form_input('bank['.$bankctr.'][bank_ifsc_code]', $hba->bank_ifsc_code,'data-validation="alphanumeric,length"  data-validation-length="3-20" data-validation-help="Minimum  length 3" data-validation-optional="true" maxlength="30" size="30"'); ?> 
											<?php echo form_error('bank['.$bankctr.'][bank_ifsc_code]'); ?> 
											</div>
                                            <div class="bank-details-main-input-box">
                                            <label>SWIFT Code   </label>
                                              <?php echo form_input('bank['.$bankctr.'][swift_code]', $hba->swift_code,'data-validation="alphanumeric,length"  data-validation-length="3-20" data-validation-help="Minimum  length 3" data-validation-optional="true" maxlength="30" size="30"'); ?> 
											<?php echo form_error('bank['.$bankctr.'][swift_code]'); ?>  
											</div>                                           
                                           
                              </div>
                                    
										<a class="remove_button" href="javascript:void(0);"><img src="<?php echo base_url();?>assets/themes/default/images/small-red-cross.png" alt="close">
										</a>								
									 </div>									 
									<?php 
									$bankctr++;
									
									endforeach; ?>									 
                                     <?php }
										if($bankctr==1){
										?>

										<div class="bank-details-main-box cf">
									<div class="n-row">
									<div class="bank-details-main-input-box">
									<?php //echo'<pre>'; var_dump($iban_code); ?>
                                        	<label>Iban Code  </label>
											 
                                           <?php echo form_input($iban_code); ?> 
											<?php echo form_error('bank['.$bankctr.'][iban_code]'); ?> 
											</div>
											
                                    	<div class="bank-details-main-input-box">
                                        	<label>Account Number     </label>
                                           <?php echo form_input($account_number); ?> 
											<?php echo form_error('bank['.$bankctr.'][account_number]'); ?> 
											</div>
                                            <div class="bank-details-main-input-box">
                                            <label>Account Name</label>
                                           <?php echo form_input($account_name); ?> 
											<?php echo form_error('bank['.$bankctr.'][account_name]'); ?> 
                                            </div>
                                            <div class="bank-details-main-input-box">
                                            <label>Bank Name   </label>
                                              <?php echo form_input($bank_name); ?> 
											<?php echo form_error('bank['.$bankctr.'][bank_name]'); ?>  
											</div>
                                            <div class="bank-details-main-input-box">
                                            <label>Bank Address </label>
                                              <?php echo form_input($bank_address); ?> 
												<?php echo form_error('bank['.$bankctr.'][bank_address]'); ?>  
											</div>                                         
										</div>                                         
                                   
									<div class="n-row">
									 <div class="bank-details-main-input-box">
                                            <label> Branch Name</label>
                                             <?php echo form_input($branch_name); ?> 
											<?php echo form_error('bank['.$bankctr.'][branch_name]'); ?> 
											</div>
                                    	<div class="bank-details-main-input-box">
                                        	<label>Bank Branch Code    </label>
                                           <?php echo form_input($branch_code); ?> 
											<?php echo form_error('bank['.$bankctr.'][branch_code]'); ?> 
											</div>
                                            <div class="bank-details-main-input-box">
                                            <label>IFSC  Code</label>
                                             <?php echo form_input($bank_ifsc_code); ?> 
											<?php echo form_error('bank['.$bankctr.'][bank_ifsc_code]'); ?> 
											</div>
                                            <div class="bank-details-main-input-box">
                                            <label>SWIFT Code   </label>
                                              <?php echo form_input($swift_code); ?> 
											<?php echo form_error('bank['.$bankctr.'][swift_code]'); ?>  
											</div>                                           
									</div>                                           
                                           
                                 <!--  <a href="javascript:void(0);" class="remove_button"><img src="<?php echo base_url(); ?>assets/themes/default/images/close.jpg" alt="close"></a> -->
                                   
										<a class="remove_button" href="javascript:void(0);"><img src="<?php echo base_url();?>assets/themes/default/images/small-red-cross.png" alt="close">
										</a>								
									 </div>

										<?php } ?>
                                  </div>
								 
                                 <div class="plus-red">
                                   <a href="javascript:void(0);" class='bnkadd_button'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="Add more"></a>
                                  </div>
                        </div>
						
     </div>
   </div>
     </div>
   </div>  
    <div class="tab-content2" style="display:none;"  id="tab-5">
   			<div class="main-part main-partt">
         <div class="main-part-container">
    		<div class="main-hotel-box main-hotel-box1 main-hotel-box1-room contact-hotel-box-change">
                        	<div class="room_type_details_title"><h2>Room Type Detail</h2>
							<div class="add_room_type_block"><a href="javascript:void(0)">Add Room Type</a></div>
                            </div>
							<div class="room_type_block_wraper">							
							<?php 
							$prc=1;
							$pdrc=1;
							//print_r($hotelPricing);
							if($hotelPricing)
							{
							foreach($hotelPricing as $pricing)
							{							
							?>							
							<?php echo form_hidden('pricing['.$prc.'][pricing_id]', $pricing->pricing_id);?>
						<div class="price-rate-main-page cf cf-row">
							<a class="price-block-remove-lnk" href="javascript:void(0);"><img alt="close" src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png"></a>
                            	<div class="price-room-main-page">
                                	<div class="prive-room-box-left">
                                    	<div class="dd">
                                        	<div class="dt rp-dt">
                                            	<label>Room Type</label>
                                            		<?php  echo form_dropdown('pricing['.$prc.'][room_type]',$roomTypesOptions,$pricing->room_type,'title="Select" class="selectpicker"')?>   
													<?php echo form_error('pricing['.$prc.'][room_type]'); ?>
											</div>                                           
                                            <div class="dt rp-dt">
                                            	<label>Currency</label>
                                            	 <?php  echo form_dropdown('pricing['.$prc.'][currency]',$currency_options,$pricing->curency_code,' title="Select" class="selectpicker"')?>   
												<?php echo form_error('pricing['.$prc.'][currency]'); ?>
                                            </div>
                                            <div class="dt inventory-ty">
                                            <label>Inventory  </label>
                                                 <?php echo form_input('pricing['.$prc.'][invenory]',$pricing->inventory,'data-validation-optional="true" data-validation="number"'); ?> 
												<?php echo form_error('pricing['.$prc.'][invenory]'); ?> 	
                                            </div>
                                            <div class="dt rp-dt1">
                                            	<label>Complimentary</label>												
												<?php  
												$room_cmplArry=getHotalRoomPricingComplimentary($pricing->pricing_id);
												echo form_multiselect('pricing['.$prc.'][room_facilities][]',$room_cmpl_options,$room_cmplArry,' class="datacheckroom" multiple title="Select" ')?>   
												<?php echo form_error('pricing['.$prc.'][room_facilities][]'); ?>                                            
                                            </div>
                                        </div>
                                        <div class="dd">
                                        	<div class="dt inventory-ty">
                                            	<label>Max Adult   </label>
                                                  <?php echo form_input('pricing['.$prc.'][max_adult]',$pricing->max_adult,'data-validation-optional="true" data-validation="number"'); ?> 
												 <?php echo form_error('pricing['.$prc.'][max_adult]'); ?>                                        
                                            </div>
                                            <div class="dt inventory-ty">
                                            	<label>Max Child   </label>
                                                 <?php echo form_input('pricing['.$prc.'][max_child]',$pricing->max_child,'data-validation-optional="true" data-validation="number"'); ?> 
												 <?php echo form_error('pricing['.$prc.'][max_child]'); ?> 
                                            </div>                                            
                                            <div class="price-dta-ran">
                                            <label>Period</label>
                                            <div class="dt pdtrn">
                                            <div class="form-group">
                                            <div class='input-group date' id='period_from_date_<?php echo $prc ?>'>
                                                  <?php echo form_input('pricing['.$prc.'][period_from_date]',format_date_tolocal($pricing->period_from),' data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" placeholder="From"'); ?> 
												<?php echo form_error('pricing['.$prc.'][period_from_date]'); ?> 
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                             </div>
                                              <div class="dt pdtrn">
                                             <div class="form-group">
                                            <div class='input-group date' id='period_to_date_<?php echo $prc ?>'>
                                                <?php echo form_input('pricing['.$prc.'][period_to_date]',format_date_tolocal($pricing->period_to),' data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" placeholder="To"'); ?> 
												<?php echo form_error('pricing['.$prc.'][period_to_date]'); ?>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                         </div>
                                         </div>
                                        </div>
                                    </div>
                                    <div class="prive-room-box-right">  
                                    <div class="dd">
                                    <label>Inclusions</label>
                                    	<?php 
										$pricing_inclusions= array(
											  'name'        => 'pricing['.$prc.'][pricing_inclusions]',
											  'id'          => 'pricing['.$prc.'][pricing_inclusions]',											 
											  'value'       => $pricing->inclusions,
											  'rows'        => '5',
											  'cols'        => '10',
											);										
										 echo form_textarea($pricing_inclusions); ?> 
										<?php echo form_error('pricing['.$prc.'][pricing_inclusions]'); ?>
                                        </div>
                                    </div>
                                </div>                               
                                <div class="price-rate-main-page1">
                                	<table id="price_rate_table_<?php echo $prc ?>" width="100%" border="0" cellspacing="1" cellpadding="5" align="center">
                                    <thead>
                                      <tr>
                                        <th width="13%">Market </th>
                                        <th width="9%">Double</th>
                                        <th width="9%">Triple </th>
                                        <th width="9%">Quad </th>
                                        <th width="9%"> Breakfast </th>
                                        <th width="9%">Half Board </th>
                                        <th width="9%">All Inclusive </th>
                                        <th width="9%">Extra Adult</th>
                                        <th width="9%">Extra Child </th>
                                        <th width="9%">Extra Bed</th>
										<th width="6%"><span class="tg-bdy dwn">&nbsp;</span></th>
                                      </tr>
                                      </thead>
                                      <tbody>									  
									  <?php 
									  $pdrc=1;
									  $pricingDetails=getHotelRroomsPricingDetails($pricing->pricing_id); 
									  if($pricingDetails)
									  {
									 $totpdet=count($pricingDetails);
									  foreach($pricingDetails as $pricingDetail)
										 {
										?>
										 <tr>
                                        <td width="13%">
										
										<?php echo form_hidden('pricing['.$prc.'][price_detail]['.$pdrc.'][pricing_detId]', $pricingDetail->id);?>
						
                                            <div class="counter-prive-table-select">
											
                                            <?php  echo form_dropdown('pricing['.$prc.'][price_detail]['.$pdrc.'][market_id]',$markets_options,$pricingDetail->market_id,' class="selectpicker"  title="Select" ')?>   
											<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][market_id]'); ?>
                                            </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
										 <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][double_price]',($pricingDetail->double_price>0)?$pricingDetail->double_price:'',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][double_price]'); ?>                                       
                                        </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][triple_price]',($pricingDetail->triple_price>0)?$pricingDetail->triple_price:'',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
											<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][triple_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
                                           <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][quad_price]',($pricingDetail->quad_price>0)?$pricingDetail->quad_price:'',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][quad_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][breakfast_price]',($pricingDetail->breakfast_price>0)?$pricingDetail->breakfast_price:'',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][breakfast_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][half_board_price]',($pricingDetail->half_board_price>0)?$pricingDetail->half_board_price:'',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][half_board_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                           <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][all_incusive]',($pricingDetail->all_incusive_adult_price>0)?$pricingDetail->all_incusive_adult_price:'','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][all_incusive]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_adult]',($pricingDetail->extra_adult_price>0)?$pricingDetail->extra_adult_price:'','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_adult]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_child]',($pricingDetail->extra_child_price>0)?$pricingDetail->extra_child_price:'','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_child]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int counter-prive-table-int1">
                                             <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_bed]',($pricingDetail->extra_bed_price>0)?$pricingDetail->extra_bed_price:'','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
											<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_bed]'); ?> 
                                             </div>                                            
                                        </td>
										 <td width="6%">
                                            <div class="counter-lnk">
											<?php $showdel=''; if($totpdet<2){$showdel='style="display:none;"';}?>											
											<a <?php echo $showdel?> onclick="removerow(this,'<?php echo $pdrc?>');" href="javascript:void(0);" rel='<?php echo $prc?>' class="price-remove-lnk"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png" alt="close"></a>											 											
                                           <a href="javascript:void(0);" rel='<?php echo $prc?>' onclick="addnerow(this,'<?php echo $pdrc?>');" class='add-price-row'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="Add more"></a>
											
											</div>
                                        </td>
                                      </tr>  
										<?php	
										$pdrc++;											
										}										  
									  }
									  else{
									  ?>
                                      <tr>
                                        <td width="13%">
                                            <div class="counter-prive-table-select">
                                            <?php  echo form_dropdown('pricing['.$prc.'][price_detail]['.$pdrc.'][market_id]',$markets_options,'',' class="selectpicker"  title="Select" ')?>   
											<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][market_id]'); ?>
                                            </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
										 <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][double_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][double_price]'); ?>                                       
                                        </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][triple_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
											<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][triple_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
                                           <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][quad_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][quad_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][breakfast_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][breakfast_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][half_board_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][half_board_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                           <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][all_incusive]','','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][all_incusive]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_adult]','','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_adult]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_child]','','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_child]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int counter-prive-table-int1">
                                             <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_bed]','','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
											<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_bed]'); ?> 
                                             </div>                                            
                                        </td>
										 <td width="6%">
                                            <div class="counter-lnk"> 
										  <a style="display:none;" onclick="removerow(this,'<?php echo $pdrc?>');" href="javascript:void(0);" rel='<?php echo $prc?>' class="price-remove-lnk"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png" alt="close"></a>											 
                                      				
                                           <a href="javascript:void(0);" rel='<?php echo $pdrc?>' onclick="addnerow(this,'<?php echo $pdrc?>');" class='add-price-row'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="Add more"></a>
											</div>
                                        </td>
                                      </tr> 
									<?php } ?>									  
                                      </tbody>
                                    </table>
                                </div>
                             </div>							
							<?php 
							$prc++;
							}							
							}
							else
							{								
							?>							
								<div class="price-rate-main-page cf cf-row">
                            	<div class="price-room-main-page">
                                	<div class="prive-room-box-left">
                                    	<div class="dd">
                                        	<div class="dt rp-dt">
                                            	<label>Room Type</label>
                                            		<?php  echo form_dropdown('pricing['.$prc.'][room_type]',$roomTypesOptions,'','title="Select" class="selectpicker"')?>   
													<?php echo form_error('pricing['.$prc.'][room_type]'); ?>
											</div>
                                           
                                            <div class="dt rp-dt">
                                            	<label>Currency  </label>
                                            	 <?php  echo form_dropdown('pricing['.$prc.'][currency]',$currency_options,'USD',' title="Select" class="selectpicker"')?>   
												<?php echo form_error('pricing['.$prc.'][currency]'); ?>
                                            </div>
                                            <div class="dt inventory-ty">
                                            <label>Inventory  </label>
                                                 <?php echo form_input('pricing['.$prc.'][invenory]','','data-validation-optional="true" data-validation="number"'); ?> 
												<?php echo form_error('pricing['.$prc.'][invenory]'); ?> 	
                                            </div>
                                            <div class="dt rp-dt1">
                                            	<label>Complimentary</label>												
												<?php  echo form_multiselect('pricing['.$prc.'][room_facilities][]',$room_cmpl_options,'',' class="datacheckroom" multiple title="Select" ')?>   
												<?php echo form_error('pricing['.$prc.'][room_facilities][]'); ?>
                                            
                                            </div>
                                        </div>
                                        <div class="dd">
                                        	<div class="dt inventory-ty">
                                            	<label>Max Adult   </label>
                                                  <?php echo form_input('pricing['.$prc.'][max_adult]','','data-validation-optional="true" data-validation="number"'); ?> 
												 <?php echo form_error('pricing['.$prc.'][max_adult]'); ?>                                        
                                            </div>
                                            <div class="dt inventory-ty">
                                            	<label>Max Child   </label>
                                                 <?php echo form_input('pricing['.$prc.'][max_child]','','data-validation-optional="true" data-validation="number"'); ?> 
												 <?php echo form_error('pricing['.$prc.'][max_child]'); ?> 
                                            </div>
                                            
                                            <div class="price-dta-ran">
                                            <label>Period</label>
                                            <div class="dt pdtrn">
                                            <div class="form-group">
                                            <div class='input-group date' id='period_from_date_<?php echo $prc ?>'>
                                                  <?php echo form_input('pricing['.$prc.'][period_from_date]','',' data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" placeholder="From"'); ?> 
												<?php echo form_error('pricing['.$prc.'][period_from_date]'); ?> 
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                             </div>
                                              <div class="dt pdtrn">
                                             <div class="form-group">
                                            <div class='input-group date' id='period_to_date_<?php echo $prc ?>'>
                                                <?php echo form_input('pricing['.$prc.'][period_to_date]','',' data-validation="date"  data-validation-optional=true data-validation-format="dd/mm/yyyy" data-validation-help="dd/mm/yyyy" placeholder="To"'); ?> 
												<?php echo form_error('pricing['.$prc.'][period_to_date]'); ?>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                         </div>
                                         </div>
                                        </div>
                                    </div>
                                    <div class="prive-room-box-right">  
                                    <div class="dd">
                                    <label>Inclusions</label>
                                    	<?php echo form_textarea($pricing_inclusions); ?> 
										<?php echo form_error('pricing['.$prc.'][pricing_inclusions]'); ?>
                                        </div>
                                    </div>
                                </div>                               
                                <div class="price-rate-main-page1">
                                	<table id="price_rate_table_<?php echo $prc ?>" width="100%" border="0" cellspacing="1" cellpadding="5" align="center">
                                    <thead>
                                      <tr>
                                        <th width="13%">Market </th>
                                        <th width="9%">Double</th>
                                        <th width="9%">Triple </th>
                                        <th width="9%">Quad </th>
                                        <th width="9%"> Breakfast </th>
                                        <th width="9%">Half Board </th>
                                        <th width="9%">All Inclusive </th>
                                        <th width="9%">Extra Adult</th>
                                        <th width="9%">Extra Child </th>
                                        <th width="9%">Extra Bed</th>
										<th width="6%"><!--<a class="tg-bdy dwn" onclick="tgbdybind(this);">&nbsp;</a>--><span class="tg-bdy dwn">&nbsp;</span></th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                        <td width="13%">
                                            <div class="counter-prive-table-select">
                                            <?php  echo form_dropdown('pricing['.$prc.'][price_detail]['.$pdrc.'][market_id]',$markets_options,'',' class="selectpicker"  title="Select" ')?>   
											<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][market_id]'); ?>
                                            </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
										 <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][double_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][double_price]'); ?>                                       
                                        </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][triple_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
											<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][triple_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
                                           <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][quad_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][quad_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                        <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][breakfast_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][breakfast_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][half_board_price]','',' placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][half_board_price]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                           <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][all_incusive]','','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][all_incusive]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                            <div class="counter-prive-table-int">
                                            <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_adult]','','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_adult]'); ?> 
                                            </div>
                                        </td>
                                        <td width="9%">
                                           <div class="counter-prive-table-int">
                                          <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_child]','','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
										<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_child]'); ?> 
                                           </div>
                                        </td>
                                        <td width="9%">
                                          <div class="counter-prive-table-int counter-prive-table-int1">
                                             <?php echo form_input('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_bed]','','placeholder="0.00" data-validation="price" data-validation-allowing="float"  data-validation-optional=true '); ?> 
											<?php echo form_error('pricing['.$prc.'][price_detail]['.$pdrc.'][extra_bed]'); ?> 
                                          </div>                                            
                                        </td>
										 <td width="6%">
										     <div class="counter-lnk">  
											  <a style="display:none;" onclick="removerow(this,'<?php echo $pdrc?>');" href="javascript:void(0);" rel='<?php echo $prc?>' class="price-remove-lnk"><img src="<?php echo base_url(); ?>assets/themes/default/images/small-red-cross.png" alt="close"></a>
										     <a href="javascript:void(0);" rel='<?php echo $prc?>' onclick="addnerow(this,'<?php echo $pdrc?>');" class='add-price-row'><img src="<?php echo base_url(); ?>assets/themes/default/images/small-green-plus.png" alt="Add more"></a>
											</div>
                                        </td>
                                      </tr>                                      
                                      </tbody>
                                    </table>
                                </div>
                             </div> 
							<?php 
							}
							?>
						</div>
                  </div>
	</div>
    </div>
   </div> 
<div class="tab-content2" style="display:none;" id="tab-6">
   <div class="main-part main-partt">
	<div class="main-part-container">
	<div class="main-hotel-left">
    <div class="gallery-img-section">
    <div class="main-hotel-box main-hotel-box-gellery contact-hotel-box-change">
                        	<h2>Hotel Gallery</h2>
                            	<div class="right-dd">
								<?php if(!empty($galleries)){?>
								<?php foreach($galleries as $gallery): ?>
                                <div class="upload-sec">								
                                <span><?php echo $gallery->entity_title;?></span>
                                <a href="#myModa<?php echo $gallery->id;?>" data-toggle="modal" data-target="#myModa<?php echo $gallery->id;?>" data-id = "<?php echo $gallery->id; ?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/upload.jpg" alt="upload" class="modalink">Upload</a>
								<?php if(!empty($galimages)): foreach($galimages as $img){?>
								<?php if($img->gallery_id==$gallery->id):?>								
							
								<div id="img<?php echo $img->id;?>">
									<img src="<?php echo base_url(); ?>hotel_gallery/<?php echo $img->image_name;?>" alt="<?php echo $img->image_name;?>" height="100" width="100"><a  onclick='return deleteimg(<?php echo $img->id;?>,<?php echo $img->gallery_id;?>,<?php echo $hotelid;?>)'><img alt="delete" src="<?php echo base_url();?>assets/themes/default/images/upload-gellery-img-delete.jpg"> </a>
								</div>						
								<?php endif; ?>
								<?php } endif; ?>
                                </div>
								<?php endforeach; ?>
								<? } ?>                                       
                             </div>
                        </div>
						</div>
	</div>
		<div class="main-hotel-right">
		<div class="uploadzip">
		<h2>Upload Zip File </h2>
		<div class="input-group">
			<span class="input-group-btn">
					<span class="btn btn-primary btn-file">
					<img src="<?php echo base_url(); ?>assets/themes/default/images/upload-now.jpg" alt="upload"> Upload Now 
					<?php echo form_upload('myfile','','data-validation="extension,size"  data-validation-optional="true" data-validation-allowing="zip"  data-validation-max-size="2M"'); ?>
					<?php echo form_error();?>
					<input type="text" class="form-control input-file-postion" readonly>
				</span>	
			</span>		                   	
		</div>	
				</div>
		</div>
     </div>
  </div>
 </div>
	 <div class="submit-area-bottom">
	 
        <input class="submit-button1" id="hotel-info-submit" type="submit" value="Save"  tabindex="50" onclick="return goToNextTab();">   
    </div>
	<div class="submit-area-bottom" id="nxtcnt">
        <button class="submit-button1"  type="button" onclick="ajaxCall();" id="nextcntnar"> Next Facilities</button>  
    </div>	
	<?php echo form_hidden('identity', $identity); ?>
	<input type="hidden" id="step" name="step">
	<input type="hidden" name="submit_call" id="submit_call">
	<?php 
	/* @stepsess user for save data on tab basis */
	if($stepsess==''){
		$stepsess = '#tab-6';
	}
	?>
	<input type="hidden" id="step1" name="step1" value="<?php echo $stepsess; ?>">
      <?php echo form_close(); ?>	  

 </div>
<?php if(!empty($galleries)){?>
<?php foreach($galleries as $gallery): ?>
<div class="modal fade" id="myModa<?php echo $gallery->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog" role="document">
		<div class="modal-content upload-gallery-main-box">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span>
			</button>
			<h4 class="modal-title" id="myModalLabel"><?php echo $gallery->entity_title;?></h4>
			<h6>Image file should not be more than <?php echo $usize;?> MB</h6>
		  </div>
		  <div class="modal-body">
			<div class="gallery-upload-img">
				<div class="img-upload-area">
					<?php echo form_open_multipart("hotels/upload/$gallery->id",array('class' => 'dropzone', 'id' => "uploadform$gallery->id")); ?>
						<input type="hidden" id="galeryid" name="galeryid" value="<?php echo $gallery->id; ?>" />
						<input type="hidden" id="gallerysessid" name="gallerysessid" value="<?php echo $identity; ?>" />
						<input type="hidden" id="previewid" name="previewid" value="<?php echo $gallery->id; ?>" />
						<input type="hidden" id="hotelid" name="hotelid" value="<?php echo $hotelid; ?>" />
						<div class="upload-now-continfo">
						</div>					
						<?php echo form_close(); ?> 
						<div id="loader-image"></div>
				</div>
				
				<div class="view-upload-img cf">
				<div id="dropzonePreview<?php echo $gallery->id; ?>"></div>
				
				</div>
			</div>
		  </div>
		  
		</div>
	  </div>
</div>
<?php endforeach; ?>
<?php } ?>


  <!--  http://www.formvalidator.net/#default-validators_help -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/dropzone.js"></script>
<script type="text/javascript">

function isValidform()
{
	var errors;
	 if( !$('#hotel_infoadd').isValid() ) {
    return false;
   } else {
   // The form is valid
  return true;
   }
}

function goToNextTab(){
	var tab1 = $('.nav3 li.current a').attr("href");
	var tab = $('#step1').val();
	 $('#step').val(tab1);//@tab1 used for set value in hidden field for save data on tab basis ur
}

function ajaxCall(){
	var chek = isValidform();
	if(chek){
	var tab = $('.nav3 li.current a').attr("href");
	$(".tab-content2").css("display", "none");
		$('ul.cf li').removeClass("current");
		
		if(tab=='#tab-1')
		{
			
			$('#tab-2').css("display", "block");
			$(tab).parents("li:first").addClass('selected');
			$('.nav3 li').each(function() {
			var theInput = $(this).children('a').attr("href");   
			if(theInput=='#tab-2')
			{
				$(this).addClass("current");
				$( "#nextcntnar" ).html('Next Contract');
				autoSave();
				
			}
			});
		}
		
		if(tab=='#tab-2')
		{
			$('#tab-3').css("display", "block");
			$(tab).parents("li:first").addClass('selected');
			$('.nav3 li').each(function() {
			var theInput = $(this).children('a').attr("href");   
			if(theInput=='#tab-3')
			{
				$(this).addClass("current");
				$( "#nextcntnar" ).html('Next Banking');
					autoSave();
			}
			});
		}
		if(tab=='#tab-3')
		{
			
			$('#tab-4').css("display", "block");
			$(tab).parents("li:first").addClass('selected');
			
			$('.nav3 li').each(function() {
			var theInput = $(this).children('a').attr("href");   
			if(theInput=='#tab-4')
			{
				$(this).addClass("current");
				$( "#nextcntnar" ).html('Next Pricing');
				autoSave();
			}
			});
		}
		if(tab=='#tab-4')
		{
			$('#tab-5').css("display", "block");
			$(tab).parents("li:first").addClass('selected');
			$('.nav3 li').each(function() {
			var theInput = $(this).children('a').attr("href");   
			if(theInput=='#tab-5')
			{
				$(this).addClass("current");
				$( "#nextcntnar" ).html('Next Gallery');
				autoSave();
			}
			});
		}
		if(tab=='#tab-5')
		{
			$('#tab-6').css("display", "block");
			$(tab).parents("li:first").addClass('selected');
			$('.nav3 li').each(function() {
			var theInput = $(this).children('a').attr("href");   
			if(theInput=='#tab-6')
			{
				$(this).addClass("current");
				$( "#nextcntnar" ).html('Hotel Info');
				autoSave();
			}
			});
		}
		if(tab=='#tab-6')
		{
			$('#tab-1').css("display", "block");
			$(tab).parents("li:first").addClass('selected');
			$('.nav3 li').each(function() {
			var theInput = $(this).children('a').attr("href");   
			if(theInput=='#tab-1')
			{
				$(this).addClass("current");
				$( "#nextcntnar" ).html('Next Facilities');
				autoSave();
			}
			});
		}
	}
}
 function searchLocation(e) {
                        var ccode = $('#country_code').val();
                        var cid = $('#city_name').val();
                        var baseUrl = "<?php echo base_url() ?>";
                        jQuery.ajax({
                            type: "POST",
                            url: baseUrl + "index.php/ajaxController/get_location_point",
                            data: {country_code: ccode,city_id: cid},
                            dataType: 'json',
                            success: function(response) {
								console.log('jevfadf');
                                if (response)
                                {   
                                    var location_data = JSON.stringify(response);
                                    var locArray = [];
                                    $.each( $.parseJSON(location_data), function( key, value ) {
                                        locArray.push(value)
                                    });
                                    
                                    $(e).autocomplete({
                                    source: locArray,
                                    minLength: 0,
                                    scroll: true,
                                    response: function(event, locArray) {
                                    // ui.content is the array that's about to be sent to the response callback.
                                    if (locArray.content.length === 0) {
                                       gsearchLocation(e);                                       
                                    } else {
                                        searchLocation(e);
                                    }
                                }
                                }).focus(function() {
                                   $(e).autocomplete("search", "");
                                });
                                    }
                            }
                        });
                    }
					



function deleteimg(imgid,galid,hid)
{
var baseUrl ="<?php echo base_url()?>";
jQuery.ajax({
type: "POST",
url: '<?php echo base_url();?>hotels/deletehotelimg',
data: {imgid: imgid, galid:galid, hid:hid},
success: function(res) {
if (res)
{
jQuery('#img'+imgid).hide();
}
}
}); 
}
jQuery(document).ready(function(){
   $("#hotel_chain").change(function(){
	$(this).parents().removeClass('has-error');
    });
    
    $('.hotel-con').hide();
    $('input[name=contract_file]').change(function(){
        $(this).parent().parent().parent().addClass('remove-space');
        $('#contract_file-name').removeClass('hotel-con-upload');
    });
    /**********************************Custom validation on contract upload to hide and show error messages********************************/
    $('#contract_signed_by').keypress(function() {
        $('#contract_file').parent().addClass('has-success');
        $('#contract_file').parent().removeClass('has-error');
    });
    
    $('#contract_end_date').focus(function() {
        $('#contract_file').parent().addClass('has-success');
        $('#contract_file').parent().removeClass('has-error');
    });
    /***********************************************end section*******************************************************************************/
    
	
	 var tab = $('#step1').val();
	$(".tab-content2").css("display", "none");
		$('ul.cf li').removeClass("current");
		if(tab=='#tab-1')
		{
			$('#tab-2').css("display", "block");
			$(tab).parents("li:first").addClass('selected');
			$('.nav3 li').each(function() {
			var theInput = $(this).children('a').attr("href");   
			if(theInput=='#tab-2')
			{
				$(this).addClass("current");
				$( "#nextcntnar" ).html('Next Contract');
				
			}
			});
			
		}
		
		if(tab=='#tab-2')
		{
			$('#tab-3').css("display", "block");
			$(tab).parents("li:first").addClass('selected');
			$('.nav3 li').each(function() {
			var theInput = $(this).children('a').attr("href");   
			if(theInput=='#tab-3')
			{
				$(this).addClass("current");
				$( "#nextcntnar" ).html('Next Banking');
				
			}
			});
		}
		if(tab=='#tab-3')
		{
			$('#tab-4').css("display", "block");
			$(tab).parents("li:first").addClass('selected');
			$('.nav3 li').each(function() {
			var theInput = $(this).children('a').attr("href");   
			if(theInput=='#tab-4')
			{
				$(this).addClass("current");
				$( "#nextcntnar" ).html('Next Pricing');
				
			}
			});
		}
		
		if(tab=='#tab-4')
		{
			$('#tab-5').css("display", "block");
			$(tab).parents("li:first").addClass('selected');
			$('.nav3 li').each(function() {
			var theInput = $(this).children('a').attr("href");   
			if(theInput=='#tab-5')
			{
				$(this).addClass("current");
				$( "#nextcntnar" ).html('Next Gallery');
				
			}
			});
		}
		if(tab=='#tab-5')
		{
			$('#tab-6').css("display", "block");
			$(tab).parents("li:first").addClass('selected');
			$('.nav3 li').each(function() {
			var theInput = $(this).children('a').attr("href");   
			if(theInput=='#tab-6')
			{
				$(this).addClass("current");
				$( "#nextcntnar" ).html('Hotel Info');
				
			}
			});
			
		}
		
		if(tab=='#tab-6')
		{
			$('#tab-1').css("display", "block");
			$(tab).parents("li:first").addClass('selected');
			$('.nav3 li').each(function() {
			var theInput = $(this).children('a').attr("href");   
			if(theInput=='#tab-1')
			{
				$(this).addClass("current");
				$( "#nextcntnar" ).html('Next Facilities');
				
			}
			});
		} 
		
	

	
   jQuery('.upload-sec a').click(function(){
       var gallaryid = $(this).data('id');
	    jQuery("#previewid").val(gallaryid);
  });
  
  	$(document).on('submit','form#hotel_infoadd',function(){
		var subval = $('#hotel-info-submit').val();
		jQuery("#submit_call").val(subval);
		});
  
	
<?php if(!empty($galleries)){?>
<?php foreach($galleries as $gallery): ?>
	Dropzone.autoDiscover = false; // keep this line if you have multiple dropzones in the same page
		$("#uploadform<?php echo $gallery->id;?>").dropzone({	
		//acceptedFiles: "image/jpeg",
		url: '<?php echo base_url();?>hotels/upload',
		uploadMultiple: true,
		acceptedFiles: ".jpeg,.jpg,.png,.gif",
		init: function(){
            this.on("error", function(file){if (!file.accepted) this.removeFile(file);
			if(!file.type.match('image.*')) {
			alert("You can upload only image file");
			return false;
			}
			});
			
			this.on("success", function(file, responseText) {
        });
		
		this.on("uploadprogress", function(file, progress) {
			jQuery('.close').hide();
			jQuery('#loader-image').html('<img src="<?php echo base_url(); ?>assets/themes/default/images/loading_bar.gif" alt="Uploading...">');
		});
		
		 this.on("complete", function(file, complete) {		 
			//alert("Upload complete.");
			jQuery('.close').show();			
			jQuery('#loader-image').html('');
			//jQuery('#progressdiv').hide();
			
		 
		}); 
			
        },
		maxFilesize: <?php echo $usize ?>,
		maxThumbnailFilesize: 500,
		dictDefaultMessage: '<img src="<?php echo base_url(); ?>assets/themes/default/images/img-upoad-top-icone.jpg"><h3>Drag & Drop Picture here</h3><img src="<?php echo base_url(); ?>assets/themes/default/images/or-icone.jpg" alt="or" class="or-upload"><span><img alt="upload" src="<?php echo base_url(); ?>assets/themes/default/images/arrow-upload-img.jpg"><input type="button" class="btn btn-default btn-file"> Upload Now</button></span>',
		thumbnailWidth: 47,
		thumbnailHeight: 47,
		maxfilesexceeded: function(file) {
			alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
		},
		
	 clickable: true,
		/* success: function (response) {
			//alert(response.xhr.responseText);
			//var x = JSON.parse(response.xhr.responseText);
			//alert(response);
		}, */
		previewsContainer: '#dropzonePreview<?php echo $gallery->id;?>',
		previewTemplate:'<ul class="cf">' +
            '<li class="dz-details cf" >' +
            '<h5 dz-filename><img style="width: 47;height: 47" data-dz-thumbnail /> <span data-dz-name></span></h5>' +
            '<h6><span data-dz-size></span> <a data-dz-remove href="javascript:undefined;"title="Delete Image"><img src="<?php echo base_url(); ?>assets/themes/default/images/upload-gellery-img-delete.jpg" alt="delete"></span> </a></h6>' +
			 '</li>'+
			 '</ul>',
		dictFileTooBig: "File is too big ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.",				 
		addRemoveLinks: false,
		removedfile: function(file) {
		var name = file.name; 
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url();?>hotels/deleteimg',
			data: "id="+name+"&galid="+<?php echo $gallery->id;?>
			
		});
			var _ref;
				return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
		 }	
		});

	<?php endforeach; } ?> 
	$('select.datacheckoptions').multiselect({ 
        numberDisplayed: 1, 
		nonSelectedText: 'Select Property Type'		
    });

	$('select.datacheckroom').multiselect({ 
                numberDisplayed: 1, 
		nonSelectedText: 'Select Complimentry'		
    });
	// form validation
	
	 $.validate({
	form : '#hotel_infoadd',
	modules : 'file'
	}); 
	

	$(document).on('submit','form#hotel_infoadd',function(){
		$('#hotel-info-submit').prop('disabled', 'disabled');// disable the form submit once submitted
		});
	var baseUrl ="<?php echo base_url()?>";
    var x = <?php echo ($bankctr==1)?'2':$bankctr?>; //Initial field 
	var xex = <?php echo ($bankctr==1)?'2':$bankctr?>; //Initial field 
	  if(xex==2)
		 {
			$('.bnkfield_wrapper > .bank-details-main-box  >.remove_button').hide();	
		 }	
     $('.bnkadd_button').click(function(e){ //Once add button is clicked
			e.preventDefault();   
            x++; //Increment field counter
			xex++;
          var fieldHTML= getNextBankFld(x,baseUrl);
            $('.bnkfield_wrapper').append(fieldHTML); // Add field html
			callbackValidator();
			$('.bnkfield_wrapper > .bank-details-main-box  >.remove_button').show();	
    });
	
    $('.bnkfield_wrapper').on('click', '.remove_button', function(e){
		 //Once remove button is clicked
        e.preventDefault();
		//if(xex == 2)
		//{
        $(this).parent('div').remove(); //Remove field html
        xex--; //Decrement field counter
		//}
		if(xex==2)
		 {
			$('.bnkfield_wrapper > .bank-details-main-box  >.remove_button').hide();	
		 }	
		
    });
	
    var ccn = <?php echo $ct;?> //Initial field counter
    var ccni = <?php echo $ct;?> //Initial field counter
	if(ccni==2)
	 {
		$('.contactfield_wrapper > .dt-main >.remove_button').hide();	
	 }
         
    $('.contactfield_wrapper').on('click', '.add_more_contact', function(e) { //Once add more contact button is clicked       
		e.preventDefault();   
            ccn++; //Increment field counter
            ccni++; //Increment field counter
            $('.contactfield_wrapper > .dt-main  >.add_more_contact').hide();
            $('.contactfield_wrapper > .dt-main >.remove_button').show();
            getNextContactRow(ccn,baseUrl); // add using ajax 
			callbackValidator();
    });
    
      $('.contactfield_wrapper').on('click', '.remove_button', function(e) {
                    //Once remove button is clicked
                    e.preventDefault();
                    // alert(ccn);
                    if (ccn == 3) {
                        $('.contactfield_wrapper > .dt-main  >.remove_button').hide();
                    }

                    var ParentDiv = $(this).parent('div').parent('div');
                    var lastDiv = $(this).parent('div');
                    lastDiv.remove();
                    $(ParentDiv).find(".add_more_contact").show();
                    $(ParentDiv).find(".add_more_contact").not(":last").hide();
                    ccn--; //Decrement field counter
                });
    
	
	 var arw = <?php echo ($a==1)?'2':$a?>; //Initial field counter
	 var arwa = <?php echo ($a==1)?'2':$a?>; //Initial field counter
	 if(arw==2)
	 {
		$('.dt_air_wraper > .dt_row >.remove_lnk').hide();	
	 }

 $('.dt_air_wraper').on('click', '.add_airdistance_row', function(e) { //Once add more contact button is clicked
            e.preventDefault();
            $('.dt_air_wraper > .dt_row  >.add_airdistance_row').hide();
            $('.dt_air_wraper > .dt_row  >.remove_link').show();
	    arw++; //Increment field counter
             
           var fieldHTML= getNextAirRow(arw,baseUrl);
            $('.dt_air_wraper').append(fieldHTML);  
	    callbackValidator();			
    });
    
     $('.dt_air_wraper').on('click', '.remove_link', function(e) {
                //Once remove button is clicked
                e.preventDefault();
                if (arw == 3) {
                    $('.dt_air_wraper > .dt_row  >.remove_link').hide();
                }

                var ParentDiv = $(this).parent('div').parent('div');
                var lastDiv = $(this).parent('div');
                lastDiv.remove();
                $(ParentDiv).find(".add_airdistance_row").show();
                $(ParentDiv).find(".add_airdistance_row").not(":last").hide();
                arw--; //Decrement field counter
            });
   
	
	var arwc = <?php echo ($c==1)?'2':$c; ?> //Initial field counter 
	 var arwcc = <?php echo ($c==1)?'2':$c; ?> //Initial field counter 
	 if(arwc==2)
	 {
		$('.dt_city_wraper > .dt_row >.remove_lnk').hide();	
	 }
         

$('.dt_city_wraper').on('click', '.add_ctydistance_row', function(e) { //Once add more contact button is clicked
            e.preventDefault();
            $('.dt_city_wraper > .dt_row  >.add_ctydistance_row').hide();
            $('.dt_city_wraper > .dt_row  >.remove_link').show();
	    arw++; //Increment field counter
             
           var fieldHTML= getNextCityRow(arw,baseUrl);
            $('.dt_city_wraper').append(fieldHTML);  
		callbackValidator();			
    });
    
    $('.dt_city_wraper').on('click', '.remove_link', function(e) {
                //Once remove button is clicked
                e.preventDefault();
//                 alert(arw);
                if (arw == 1) {
                    $('.dt_city_wraper > .dt_row  >.remove_link').hide();
                }

                var ParentDiv = $(this).parent('div').parent('div');
                var lastDiv = $(this).parent('div');
                lastDiv.remove();
                $(ParentDiv).find(".add_ctydistance_row").show();
                $(ParentDiv).find(".add_ctydistance_row").not(":last").hide();
                arw--; //Decrement field counter
            });
   
//    $('.dt_city_wraper').on('click', '.remove_lnk', function(e){
//		 //Once remove button is clicked
//        e.preventDefault();
//		if(arwcc>2)
//		{
//        $(this).parent('div').remove(); //Remove field html
//        arwcc--; //Decrement field counter
//		}
//	if(arwcc==2)
//	 {
//		$('.dt_city_wraper > .dt_row >.remove_lnk').hide();	
//	 }	
//    });
	var lows = <?php echo ($lowctr==1)?'2':$lowctr; ?> //Initial field counter 
	var lowsex = <?php echo ($lowctr==1)?'2':$lowctr; ?> //Initial field counter 
	if(lows==2)
	 {
		$('.lowsession_wraper > .dt-row >.remove_lnk').hide();	
	 }
//         
//     $('.add_lowsession_row').click(function(e){ //Once add more link is clicked      
//			e.preventDefault();           
//		   lows++; //Increment field counter
//		   lowsex++;
//		  setNextSessionRow(lows,baseUrl,'low','lowsession_wraper'); 
//		 $('.lowsession_wraper > .dt-row >.remove_lnk').show();	
//    });

$('.lowsession_wraper').on('click', '.add_lowsession_row', function(e) { //Once add more contact button is clicked            
		e.preventDefault(); 
                $('.lowsession_wraper > .dt-row  >.add_lowsession_row').hide();
                $('.lowsession_wraper > .dt-row  >.remove_lnk').show();
             lows++; //Increment field counter
             lowsex++; //Increment field counter
            setNextSessionRow(lows,baseUrl,'low','lowsession_wraper'); // add using ajax  
    });

$('.lowsession_wraper').on('click', '.remove_lnk', function(e) {
                //Once remove button is clicked
                e.preventDefault();
//               alert(lowsex);
                if (lowsex == 3) {
                    $('.lowsession_wraper > .dt-row  >.remove_lnk').hide();
                }

                var ParentDiv = $(this).parent('div').parent('div');
                var lastDiv = $(this).parent('div');
                lastDiv.remove();
                $(ParentDiv).find(".add_lowsession_row").show();
                $(ParentDiv).find(".add_lowsession_row").not(":last").hide();
                lowsex--; //Decrement field counter
            });
			
var hsession = <?php echo ($highctr==1)?'2':$highctr; ?> //Initial field counter 
var hsessionex = <?php echo ($highctr==1)?'2':$highctr; ?> //Initial field counter 
	if(hsession==2)
	 {
		$('.highsession_wraper > .dt-row > .remove_lnk').hide();	 
	 }
         $('.highsession_wraper').on('click', '.add_highsession_row', function(e) { //Once add more contact button is clicked            
		e.preventDefault(); 
                $('.highsession_wraper > .dt-row  >.add_highsession_row').hide();
                $('.highsession_wraper > .dt-row  >.remove_lnk').show();
             hsession++; //Increment field counter
             hsessionex++; //Increment field counter
            setNextSessionRow(hsession,baseUrl,'high','highsession_wraper');   
    });
    
    $('.highsession_wraper').on('click', '.remove_lnk', function(e) {
                //Once remove button is clicked
                e.preventDefault();
//               alert(lowsex);
                if (hsessionex == 3) {
                    $('.highsession_wraper > .dt-row  >.remove_lnk').hide();
                }

                var ParentDiv = $(this).parent('div').parent('div');
                var lastDiv = $(this).parent('div');
                lastDiv.remove();
                $(ParentDiv).find(".add_highsession_row").show();
                $(ParentDiv).find(".add_highsession_row").not(":last").hide();
                hsessionex--; //Decrement field counter
            });
         

	 var cr = <?php echo ($reni==1)?'2':$reni; ?>  /*Initial field counter */
	 var crex = <?php echo ($reni==1)?'2':$reni; ?> 
	 if(cr==2)
	 {
		$('.renovation_wraper > .exdd  > .remove_lnk').hide();	 
	 }
     $('.add_renovation_row').click(function(e){ //Once add more link is clicked      
			e.preventDefault();           
		    cr++; /*Increment field counter */
			 crex++;
			setNextRenovationRow(cr,baseUrl);	
			$('.renovation_wraper > .exdd > .remove_lnk').show();			
		});
  
    $('.renovation_wraper').on('click', '.remove_lnk', function(e){
		 /*Once remove link is clicked*/
        e.preventDefault();
		if(crex>2)
		{
        $(this).parent('div').remove(); //Remove field html
        crex--; /*Decrement field counter*/
		}
		if(crex==2)
		 {
			$('.renovation_wraper > .exdd > .remove_lnk').hide();	
		 }
    });
	
 var exdr = <?php echo ($exi==1)?'2':$exi; ?> //Initial field counter 
var exdrex = <?php echo ($exi==1)?'2':$exi; ?> //Initial field counter 
     $('.add-excluded-date-row').click(function(e){ //Once add more link is clicked      
			e.preventDefault();           
		    exdr++; //Increment field counter
			exdrex++;			
			setExcludedDateRow(exdr,baseUrl,'<?php echo getTodayDate() ?>'); 
 
		var elIdFrom="#cmpli_exclude_date_frm_"+exdr;
		var elIdTo="#cmpli_exclude_date_to_"+exdr;
		datetimepicker_from_to(elIdFrom,elIdTo);
		$('.excluded-date-wraper > .exdd > .remove_lnk').show();	 
   });
  
    $('.excluded-date-wraper').on('click', '.remove_lnk', function(e){
		 //Once remove link is clicked
        e.preventDefault();
		if(exdrex>2)
		{
        $(this).parent('div').remove(); //Remove field html
        exdrex--; //Decrement field counter
		}
	if(exdrex==2)
	 {
		$('.excluded-date-wraper > .exdd > .remove_lnk').hide();	 
	 }
    });
	 if(exdrex==2)
	 {
		$('.excluded-date-wraper > .exdd > .remove_lnk').hide();	 
	 }
	
	 var pr = <?php echo ($ppc==1)?'2':$ppc; ?> //Initial field counter 
	var prex = <?php echo ($ppc==1)?'2':$ppc; ?> //Initial field counter 
	if(prex==2)
		{
		$('.payment_plan_wraper > .dt-row > .remove_lnk').hide();
		}
                

 $('.payment_plan_wraper').on('click', '.add_payment_row', function(e) { //Once add more contact button is clicked            
		e.preventDefault(); 
                $('.payment_plan_wraper > .dt-row  >.add_payment_row').hide();
                $('.payment_plan_wraper > .dt-row  >.remove_lnk').show();
             pr++; //Increment field counter
             prex++; //Increment field counter
            setNextPaymentPlanRow(pr,baseUrl); // add using ajax  
    });
    
    $('.payment_plan_wraper').on('click', '.remove_lnk', function(e) {
                //Once remove button is clicked
                e.preventDefault();
                // alert(ccn);
                if (prex == 3) {
                    $('.payment_plan_wraper > .dt-row  >.remove_lnk').hide();
                }

                var ParentDiv = $(this).parent('div').parent('div');
                var lastDiv = $(this).parent('div');
                lastDiv.remove();
                $(ParentDiv).find(".add_payment_row").show();
                $(ParentDiv).find(".add_payment_row").not(":last").hide();
                prex--; //Decrement field counter
            });
   
	
	//add room type block
	//var nextBlockId = <?php echo $prc;?>; //Initial field 
	// var exnextBlockId = <?php echo $prc?>;
	 
	 var nextBlockId = <?php echo ($prc==1)?'2':$prc; ?> //Initial field counter 
	var exnextBlockId = <?php echo ($prc==1)?'2':$prc; ?> //Initial field counter 

     $('.add_room_type_block a').click(function(e){ 
	 //Once add more link is clicked      
		e.preventDefault();           
		 nextBlockId++; //Increment field counter
		 exnextBlockId++;
		 setNextRoomTypeBlock(nextBlockId,baseUrl);			
		$('.room_type_block_wraper > .price-rate-main-page >.price-block-remove-lnk').show();	
		 $('select.datacheckroom').multiselect({ 
			numberDisplayed: 1, 
			nonSelectedText: 'Select Complimentry'		
			});
    });
   
    $('.room_type_block_wraper').on('click', '.price-block-remove-lnk', function(e){
		 //Once remove button is clicked
        e.preventDefault();
		if(exnextBlockId>2)
		{
        $(this).parent('div').remove(); //Remove field html
        exnextBlockId--; //Decrement field counter
		}
	if(exnextBlockId==2)
	 {
		$('.room_type_block_wraper > .price-rate-main-page >.price-block-remove-lnk').hide();	
	 }
    })

	if(exnextBlockId==2)
	 {
		$('.room_type_block_wraper > .price-rate-main-page >.price-block-remove-lnk').hide();	
	 }
	// pricing section new  price  row end
	
     $( ".main-hotel-box h2" ).click(function() {
		if($(this).parent('.main-hotel-box').find('.effect-toogle').is(":visible")){
			$(this).addClass('arrow-down');
		}else{
			$(this).removeClass('arrow-down');
		}
		$(this).parent('.main-hotel-box').find('.effect-toogle').toggle('blind', 10);
    });
	

	$('#contract_startdate').datetimepicker({
                format: 'DD/MM/YYYY',
				allowInputToggle : true,
				//useCurrent:false,
							
            });
	$('#contract_enddate').datetimepicker({
               format: 'DD/MM/YYYY',
			   allowInputToggle : true,
			   useCurrent:false,
			
        });
		 $('#contract_enddate').data("DateTimePicker").minDate($("#contract_enddate").date);
		
		$("#contract_startdate").on("dp.change", function (e) {
			   $('#contract_enddate').data("DateTimePicker").minDate(e.date);
		 });
	 
        $("#contract_enddate").on("dp.change", function (e) {
            $('#contract_startdate').data("DateTimePicker").maxDate(e.date);
        });	
		<?php
		$xRen=1;
		if($reni==1)	
		{
		$reni=$reni+1;	
		}	
		while($xRen < $reni)	
		{	
		?>
		var elIdFrom="#renovation_shedule_from_<?php echo $xRen?>";
		var elIdTo="#renovation_shedule_to_<?php echo $xRen?>";
		datetimepicker_from_to(elIdFrom,elIdTo)
		<?php $xRen++;
		} ?>			
	$('#cmpli_date_from_input').datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });	
		
		$('#cmpli_date_to_input').datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });	
		
		$("#cmpli_date_from_input").on("dp.change", function (e) {
           $('#cmpli_date_to_input').data("DateTimePicker").minDate(e.date);

		});
		$('#cmpli_date_from_input').data("DateTimePicker").minDate($('#cmpli_date_to_input').date);
		$('#cmpli_date_to_input').data("DateTimePicker").minDate($("#cmpli_date_from_input").date);
		
		$("#cmpli_date_to_input").on("dp.change", function (e) {
            $('#cmpli_date_from_input').data("DateTimePicker").maxDate(e.date);
        });
		
		<?php
		$xdt=1;	
		if($exi==1)
		{
		$exi=$exi+1;	
		}	
		while($xdt < $exi)	
		{	
		?>
		var elIdFrom="#cmpli_exclude_date_frm_<?php echo $xdt?>";
		var elIdTo="#cmpli_exclude_date_to_<?php echo $xdt?>";
		datetimepicker_from_to(elIdFrom,elIdTo)
		<?php $xdt++;
		} ?>
	<?php
	$xprc=1;	
	if($prc==1)
	{
	$prc=$prc+1;	
	}	
	while($xprc < $prc)	
	{	
	?>
var elIdFrom="#period_from_date_<?php echo $xprc?>";
var elIdTo="#period_to_date_<?php echo $xprc?>";
datetimepicker_from_to(elIdFrom,elIdTo)
<?php $xprc++;
} ?>	 
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        var input = $(this).parents('.input-group').find(':text'),
          log = numFiles > 1 ? numFiles + ' files selected' : label;
        if( input.length ) {
            input.val(log);
        } 
   
    });	

 $( ".btn-group .multiselect-container li" ).on('change', 'input[type="checkbox"]', function(){
  var chval=($(this).attr('value'));
  if(chval=='Other')
  {
  $('.btn-group').removeClass('open');
  }
  });
					
});


 $(".nav3 li a").click(function(event) {
        event.preventDefault();
		var chek = isValidform();
		if(chek){
	    $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");

        var tab = $(this).attr("href");
        $(".tab-content2").not(tab).css("display", "none");
        $(tab).fadeIn();
		$('#step').val('#tab-6');
		
		if(tab=='#tab-1')
		{
			
				autoSave();
				$('#step').val('#tab-6');
				$( "#nextcntnar" ).html('Next Facilities');	
			
		}
		
		$('#step').val('#tab-1');
		if(tab=='#tab-2')
		{
			
				autoSave();
				$( "#nextcntnar" ).html('Next Contract');
			
		}
		
		$('#step').val('#tab-2');
		if(tab=='#tab-3')
		{
			
				autoSave();

				$( "#nextcntnar" ).html('Next Banking');
			
		}
		
		$('#step').val('#tab-3');
		if(tab=='#tab-4')
		{
			autoSave();

			$( "#nextcntnar" ).html('Next Pricing');
		}
		
		$('#step').val('#tab-4');
		if(tab=='#tab-5')
		{
			autoSave();

			$( "#nextcntnar" ).html('Next Gallery');
		}
			
		 if(tab=='#tab-6')
		{
			autoSave();

			$( "#nextcntnar" ).html('Hotel Info');
		} 			
		}		  
    }); 	

$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$('.tg-bdy').bind('click', function(){
     $(this).closest('table').children('tbody').toggle();	 
	 if($(this).hasClass('dwn')){
			$(this).removeClass('dwn');
			$(this).addClass('upa');
		}else{
			$(this).removeClass('upa');
			$(this).addClass('dwn')
		}
  });

function getCountryCities(ele){
getCityDistricts($("div#district_container select"));	
var baseUrl ="<?php echo base_url()?>";
var ccode = $(ele).val();
jQuery.ajax({
type: "POST",
url: baseUrl + "index.php/ajaxController/getCountryCities",
data: {country_code: ccode},
success: function(res) {
if (res)
{
jQuery("div#city_container").html(res);
$("div#city_container select").selectpicker();
}
}
});
}

function getCityDistricts(ele)
{
var baseUrl ="<?php echo base_url()?>";
var cname = $(ele).val();
jQuery.ajax({
type: "POST",
url: baseUrl + "index.php/ajaxController/getCityDistricts",
data: {city_name: cname},
success: function(res) {
if (res)
{
jQuery("div#district_container").html(res);
$("div#district_container select").selectpicker();
}
}
});
}

// Add custom validation rule
$.formUtils.addValidator({
  name : 'phone_number',
  validatorFunction : function(value, $el, config, language, $form) {
   	//var filter =/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})?([ .-]?)\2?([ .-]?)([0-9]{4})/;
	var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
   if (filter.test(value)) {
        return true;
    }
    else {
        return false;
    }   
  },
  errorMessage : 'Enter valid phone number',
  errorMessageKey: 'badPhoneNumber'
});

$.formUtils.addValidator({
  name : 'distance',
  validatorFunction : function(value, $el, config, language, $form) {
    if(value.indexOf('.')!=-1){ 		
		var filter = /^[0-9,]+\.\d{1,3}$/;
			if (!filter.test(value)) {
			return false;
		}		
		var dval=value.split(".")[0];
		 if(value.split(".")[0].length < 1){                
               return false;
            } 
          if( dval < 0 || dval > 999)
            {
				 return false;
			}		     
          if(value.split(".")[1].length > 2){                
               return false;
            }         
         else
         {
			    return true;
		 } 
		} 
		else
		 {
			 if(value < 1000 && value > 0)
			 {
				return true;
			 }
			 else{
				 return false;
			 }
		}    
  },
  errorMessage : 'Enter valid distance value',
  errorMessageKey: 'badminfloat'
});

        $.formUtils.addValidator({
                        name: 'upload_contract',
                        validatorFunction: function(value, $el, config, language, $form) {                        
                            var signed_value = $("input[name=contract_signed_by]").val();
                            var contract_end_date = $("input[name=contract_end_date]").val();
                            if ((signed_value!='') && (contract_end_date!='')) {
                                return true; 
                            }
                            else
                            {
                              return false;  
                                
                            }
                        },
                        errorMessage: 'Contract end date and Signed value is must for upload contract',
                        errorMessageKey: 'badminfloat'
                    });

$.formUtils.addValidator({
  name : 'price',
  validatorFunction : function(value, $el, config, language, $form) {
    if(value.indexOf('.')!=-1){ 		
		var filter = /^[0-9,]+\.\d{1,3}$/;
			if (!filter.test(value)) {
			return false;
		}		
		var dval=value.split(".")[0];
		 if(value.split(".")[0].length < 1){                
               return false;
            } 
          if( dval < 0 )
            {
				 return false;
			}		     
          if(value.split(".")[1].length > 2){                
               return false;
            }         
         else
         {
			    return true;
		 } 
		} 
		else
		 {
			 if(value > 0)
			 {
				return true;
			 }
			 else{
				 return false;
			 }
		}    
  },
  errorMessage : 'Enter valid price value',
  errorMessageKey: 'badminfloat'
});

function setNextRoomTypeBlock(nextBlockId,baseUrl)
{
	/* get new price block  row using ajax */		
		jQuery.ajax({
		type: "POST",
		url: baseUrl + "index.php/ajaxController/nextPriceBlock",
		data: {block_num: nextBlockId},
		success: function(res) {
		if (res)
		{			
		$('.room_type_block_wraper').append(res); 
		$('.room_type_block_wraper select.datacheckroom').multiselect({ 
			numberDisplayed: 1,
			nonSelectedText: 'Check an option!'			
			});
			
		$('.room_type_block_wraper input[type="checkbox"]').checkbox({
		checkedClass: 'icon-check',
		uncheckedClass: 'icon-check-empty'
		});
		$('.room_type_block_wraper .selectpicker').selectpicker();
	
		$('#period_from_date_'+nextBlockId).datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });
		$('#period_to_date_'+nextBlockId).datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });
		
		var elIdFrom="#period_from_date_"+nextBlockId;
			var elIdTo="#period_to_date_"+nextBlockId;
			datetimepicker_from_to(elIdFrom,elIdTo)		
		callbackValidator();
		}
		else{
			return '';
			}
		}
	});
	
}

function addNewPriceRow(n,blockId,baseUrl){
	var tableId ='price_rate_table_'+blockId;
	/* get new low price row using ajax */		
		jQuery.ajax({
		type: "POST",
		url: baseUrl + "index.php/ajaxController/getNewPriceLine",
		data: {row_num: n,block_num: blockId},
		success: function(res) {
		if (res)
		{			
		$("#"+tableId+" tbody").append(res); 
		$("#"+tableId+" tbody select").selectpicker();
		callbackValidator();
		}
		else{
			return '';
			}
		}
	});
}; 

function addnerow(e,nrc)
{
	var baseUrl ="<?php echo base_url()?>";
	// pricing section new  price  row
	 nrc++; //Increment field counter
	 var blockId=$(e).attr( 'rel' );
		addNewPriceRow(nrc,blockId,baseUrl);
	 var pt = $(e).parents("table:first");
	 $(pt).find(".add-price-row").hide();
		$(pt).find(".price-remove-lnk").show();
    
}
	
function removerow(e,nrc)
{
	var pt = $(e).parents("table:first");
	var totrows = $(pt).find("tr").length;
	var par = $(e).parents("tr:first");
	if(totrows >2)
	{
	par.remove();
    totrows--; //Decrement field counter
	}
	if(totrows <= 2)
	{
		$(pt).find(".price-remove-lnk").hide();
	}
	else{
		$(pt).find(".price-remove-lnk").show();
	}

	$(pt).find(".add-price-row").show();
	$(pt).find(".add-price-row").not(":last").hide();
}
	function tgbdybind(el)
	{
		
		$(el).closest('table').children('tbody').toggle(); 
		 if($(el).hasClass('dwn')){
				$(el).removeClass('dwn');
				$(el).addClass('upa');
				 
			}
			else{
				$(el).removeClass('upa');
				$(el).addClass('dwn')
			}
	}

function gsearchLocation(e)
	{
	var baseUrl ="<?php echo base_url()?>";
	$(e).autocomplete({	  
      source: baseUrl + "index.php/ajaxController/googleSearch"
    });	
	}
	
	function autoSave(){
                var baseUrl = "<?php echo base_url(); ?>";
                var uploadPath = "<?php echo base_url().'contract_files' ?>";
                var formData = new FormData($('#hotel_infoadd')[0]);
              $.ajax({
                url: baseUrl + "hotels/editform/edit_information/<?php echo $hotelid; ?>",
                type:"POST",
                async: false,
                data:formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){
                                var result = jQuery.parseJSON(response);
                                var responseContract = getContractHtml(result.start_date,result.end_date,result.signed_by,result.contract_file,result.contract_name,uploadPath);
                                var html = dataRow(result.start_date,result.end_date,result.signed_by,result.contract_file,result.contract_name,uploadPath);
            			if(result.success=='true'){
                                    if ( $('.contract-file-list').length ) {
                                          console.log(html);
                                          $("#custom-contract").after(html);
                                          $('#contract_file-name').addClass('hotel-con-upload');
                                          $('#contract_file').val('');
                                          $('#contract_signed_by').val('');
                                          $('#contract_end_date').val('');
                                   }else{
//                                       alert('not');
                                       console.log(responseContract);
                                       $('.contact-info').after(responseContract);
                                    $('#contract_file').val('');
                                    $('#contract_file-name').addClass('hotel-con-upload');
                                    $('#contract_signed_by').val('');
                                    $('#contract_end_date').val('');
                                   }
                                    
            			}
                    },
                
            });
          }
          
          function getContractHtml(start_date,end_date,signed_by,file_name,name,uploadPath){
                var contractHtml = '<div class="contract-file-list"><div class="t-row"><div class="t-row-head"><div class="ci th-td">Start Date</div><div class="ci th-td">End Date</div><div class="ci th-td">Signed By</div><div class="ci th-td td-last-col">Contract</div></div><span id="custom-contract" style="visibility:none; width: 0px;height: 0px;"></span><div class="t-row"><div class="contact-info dd"><div class="ci"><div class="input-group">'+start_date+'</div></div><div class="ci"><div class="input-group">'+end_date+'</div></div><div class="ci signby"><div class="input-group">'+signed_by+'</div></div><div class="upload-continfo"><a title="Click to view file : '+file_name+'" href='+uploadPath+'/'+name+' target="_blank">'+file_name+'</a></div></div></div></div>';
                return contractHtml;
          }
           function dataRow(start_date,end_date,signed_by,file_name,name,uploadPath){
                var dataHtml = '<div class="t-row"><div class = "contact-info dd"><div class="ci"><div class="input-group">'+start_date+'</div></div><div class="ci"><div class="input-group">'+end_date+'</div></div><div class="ci signby"><div class="input-group">'+signed_by+'</div></div><div class="upload-continfo"><a title="Click to view file : '+file_name+'" href='+uploadPath+'/'+name+' target="_blank">'+file_name+'</a></div></div></div>';
                return dataHtml;
          }
</script> 