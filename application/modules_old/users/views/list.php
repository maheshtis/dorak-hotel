<style>
    #result{ position: relative;
        font-size: 12px;
        float: left; padding: 5px 0; text-align: center;
        margin-top: 10px;
        font-weight: bold; margin-top: 10px;}
    .good{
        width: 100%;
        border: 1px dashed green;
        color: green; float: left;
    }
    .weak{
        width: 100%;
        border: 1px dashed red;
        color: red; float: left;
    }
    .strong{
        width: 100%;
        border: 1px dashed green;
        color: green;  float: left;
    }
    .custom-disable{
        opacity: 0.3;
    }
    .short{    
        width: 100%;
        border: 1px dashed red;
        color: red; float: left;
    }
</style>

<div class="main-part-container"> 
    <div class="main-hotel">
        <div class="hotel-serach-form">
            <table id="user-listing-table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Company</th>
                        <th>Department</th>
                        <th>Designation</th>
                        <th>Role</th>
                        <th>Email</th>
                        <th>Phone</th>		
                        <th>Status</th>
                        <th>Access Level</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user): ?>
                        <tr>
                            <td><?php echo htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8'); ?></td>
                            <td><?php echo htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8'); ?></td>
                            <td><?php echo htmlspecialchars($user->company, ENT_QUOTES, 'UTF-8'); ?></td>
                            <td><?php echo htmlspecialchars($user->department, ENT_QUOTES, 'UTF-8'); ?></td>

                            <td><?php echo htmlspecialchars($user->position, ENT_QUOTES, 'UTF-8'); ?></td>
                            <td><?php echo getRoleTitle($user->role); ?></td>
                            <td><?php echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?></td>
                            <td>
                                <?php foreach ($user->groups as $group): ?>
                                    <?php echo htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8'); ?><br />
                                <?php endforeach ?>
                            </td>
                            <td>
                                <?php if (checkAccess($this->accessLabelId, 'users', 'edit')) { ?>
                                    <?php echo ($user->active) ? anchor("users/deactivate/" . $user->id, lang('index_active_link')) : anchor("users/activate/" . $user->id, lang('index_inactive_link')); ?>
                                <?php } else {
                                    ?>
                                    <?php echo ($user->active) ? anchor("javascript:void(0);", lang('index_active_link')) : anchor("javascript:void(0);", lang('index_inactive_link')); ?>

                                    <?php }
                                ?>
                            </td>
                            <td>
                                <?php if (checkAccess($this->accessLabelId, 'users', 'edit')) { ?>
                                    <?php echo anchor("users/edit_user/" . $user->id, 'Edit'); ?>  
                                <?php } ?>
                                <?php if (checkAccess($this->accessLabelId, 'users', 'delete')) { ?>
                                    <?php if ($user->id != 1) {
                                        echo anchor("users/delete_user/" . $user->id, 'Delete');
                                    } ?>
                        <?php } ?>
                            </td>
                        </tr>
            <?php endforeach; ?>
                </tbody>
            </table>
<?php if (checkAccess($this->accessLabelId, 'users', 'add')) { ?>
                <div class="user_action_links">
                    <ul>
                        <li>
                            <a data-toggle="modal" data-target="#add-user-bock" class="add-hotel img-pop" href="javascript:void(0);">Add new user</a>
                        </li>
                        <li>
    <?php echo anchor('users/user_roles', "View Access Levels") ?>
                        </li>
                    </ul>
                </div>
<?php } ?>
        </div>
    </div>
</div>
<div class="modal fade newcoustomer" id="add-user-bock" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Create User</h3>
            </div>
            <div class="modal-body cf">
<?php echo form_open("users/create_user", array('class' => 'company_user', 'id' => 'company_user')); ?>
                <span class="abc" style="display: none;">bed password</span>
                <div class="dd">
                    <div class="dt">
                        <label>First Name <span class="vali-star">*</span></label>
<?php echo form_input($first_name); ?>
<?php echo form_error('first_name'); ?>
                    </div>
                    <div class="dt">
                        <label>Last Name </label>
<?php echo form_input($last_name); ?>
                    </div>
                </div>
                <div class="dd">
                    <div class="dt">
                        <label>Email<span class="vali-star">*</span></label>
<?php echo form_input($email); ?>                                       
                    </div>
                    <div class="dt">
                        <label>Phone <span class="vali-star">*</span></label>
<?php echo form_input($phone); ?>
                    </div>

                </div>  

                <div class="dd">
                    <div class="dt">
                        <label>Company<span class="vali-star">*</span></label>
<?php echo form_dropdown('company', $companies, $posted_company, ' tabindex="4" data-validation="required" data-live-search="true" class="selectpicker"') ?>   
<?php echo form_error('company'); ?>
                    </div>
                    <div class="dt">
                        <label>Designation <span class="vali-star">*</span></label>
<?php echo form_dropdown('position', $positions, $posted_position, ' data-validation="required" tabindex="5" class="selectpicker"') ?>   
                    </div>

                </div>
                <div class="dd">
                    <div class="dt">
                        <label>Department <span class="vali-star">*</span></label>                           
<?php echo form_dropdown('department', $departments, $posted_department, ' data-validation="required" tabindex="6"  class="selectpicker" ') ?>                                   
                    </div>
                    <div class="dt">
                        <label>Role <span class="vali-star">*</span></label>
<?php echo form_dropdown('role', $roles, $posted_role, ' data-validation="required" tabindex="7" id="access_level_id" class="selectpicker" ') ?>   
                    </div>
                </div>

                <div class="dd last">
                    <div class="dt">

                        <label>Password <span class="vali-star">*</span></label>
                        <?php echo form_input($password); ?>
                        <span id="result"></span>
<?php echo form_error('password_confirmation'); ?>
                    </div>

                    <div class="dt">
                        <label>Confirm Password <span class="vali-star">*</span></label>
                    <?php echo form_input($password_confirm); ?> 
                    <?php echo form_error('password'); ?>   
                    </div>
                    <?php
                    if ($identity_column !== 'email') {
                        echo '<div class="dt">';
                        echo lang('create_user_identity_label', 'identity');
                        echo '<br />';
                        echo form_error('identity');
                        echo form_input($identity);
                        echo '</div>';
                    }
                    ?>
                </div>  
                <input type="submit" value="Save" class="submit-button1" id="saveid">
<?php echo form_close(); ?>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/security.js"></script>
<script>
    // <![CDATA[

// Add custom validation rule
    $.formUtils.addValidator({
        name: 'phone_number',
        validatorFunction: function(value, $el, config, language, $form) {
            //var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/; 
            //[0-9\-\(\)\s]+.
            //([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})

            var filter = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})?([ .-]?)\2?([ .-]?)([0-9]{4})/;
            if (filter.test(value)) {
                return true;
            }
            else {
                return false;
            }
        },
        errorMessage: 'Enter valid phone number',
        errorMessageKey: 'badPhoneNumber'
    });
    
	/*
		assigning keyup event to password field
		so everytime user type code will execute
	*/

	$('#password_confirmation').keyup(function()
	{
		$('#result').html(checkStrength($('#password_confirmation').val()))
	})	
	
	/*
		checkStrength is function which will do the 
		main password strength checking for us
	*/
	
	function checkStrength(password)
	{
		//initial strength
		var strength = 0
		
		//if the password length is less than 6, return message.
		if (password.length < 6) { 
			$('#result').removeClass()
			$('#result').addClass('short')
                        $("#saveid").addClass('custom-disable')
			return 'Too short' 
		}
		
		//length is ok, lets continue.
		
		//if length is 8 characters or more, increase strength value
		if (password.length > 7) strength += 1
		
		//if password contains both lower and uppercase characters, increase strength value
		if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1
		
		//if it has numbers and characters, increase strength value
		if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1 
		
		//if it has one special character, increase strength value
		if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1
		
		//if it has two special characters, increase strength value
		if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
		
		//now we have calculated strength value, we can return messages
		
		//if value is less than 2
		if (strength < 2 )
		{
			$('#result').removeClass()
			$('#result').addClass('weak')
                        $("#saveid").addClass('custom-disable')
			return 'Weak'			
		}
		else if (strength == 2 )
		{
			$('#result').removeClass()
			$('#result').addClass('good')
                        $("#saveid").removeClass('custom-disable')
			return 'Good'		
		}
		else
		{
			$('#result').removeClass()
			$('#result').addClass('strong')
                        $("#saveid").removeClass('custom-disable')
			return 'Strong'
		}
	}
    


    $(document).ready(function() {
        
        var table = $('#user-listing-table').DataTable({
            'ajax': {
                'url': '<?php echo base_url() ?>users/userListingAjax'
            },
            "bAutoWidth": false,
            "autoWidth": false,
            "columns": [
                {"width": "10%"},
                {"width": "10%"},
                {"width": "10%"},
                {"width": "5%"},
                {"width": "5%"},
                {"width": "8%"},
                {"width": "8%"},
                {"width": "8%"},
                {"width": "5%"},
                {"width": "10%"},
                {"width": "24%"},
            ],
            "aoColumns": [
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": true},
                {"bSortable": false}
            ],
            'order': [[1, 'asc']],
            // "searching": false,
            "lengthMenu": [[10, 25, 50, -1], [" 10 Per Page", " 25 Per Page", " 50 Per Page", "All"]],
            language: {
                sLengthMenu: "View: _MENU_"
            },
        });

        $.validate({
            form: '#company_user',
//            modules: 'security',
//            onModulesLoaded: function() {
//                var optionalConfig = {
//                    fontSize: '12pt',
//                    padding: '4px',
//                    bad: 'Very bad',
//                    weak: 'Weak',
//                    good: 'Good',
//                    strong: 'Strong'
//                };
//                $('input[name="password_confirmation"]').displayPasswordStrength(optionalConfig);
//
//            }
        });
    });
    // ]]></script>