<html lang="en">
<head>
<title><?php echo $title; ?></title>
<meta name="resource-type" content="document" />
<meta http-equiv="expires" content="0">
<meta name="robots" content="all, index, follow"/>
<meta name="googlebot" content="all, index, follow" />
<!-- Le styles -->
<link href="<?php echo base_url(); ?>assets/themes/default/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap-checkbox.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap-select.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/calendar.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>assets/themes/default/css/star-rating.min.css" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,300,900' rel='stylesheet' type='text/css'>
<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/themes/default/images/favicon.png" type="image/x-icon"/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.js"></script>

<!-- Latest compiled and minified JavaScript -->

</head>
<body id="outer-container">
    <?php
    echo load_module_script('settings', array('setting.js'));
    ?>
<div class="wrapper-custom cf">
<div class = "nav-custom" style="position: absolute; height: 100%; width: 100%; z-index: 1;display: none;">        
 </div>
<div class="tab-wrapper" style="height:100%;width:100%"></div>
<div class="header">
<div class="logo">
<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/logo.png" alt="logo"></a>
</div>
<div class="main-setting">
 <div class="logged-user">
    	<h5>Welcome,<strong><?php echo $user; ?></strong>(<a href="<?php echo base_url(); ?>auth/logout">Logout</a>)</h5>
    	
    </div>
<div class="settings">
<a href="<?php echo base_url('settings'); ?>"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting1.png" alt="Setting"></a>
</div>
<a id="right-menu" href="#sidr"><img src="<?php echo base_url(); ?>assets/themes/default/images/menu-buttn.jpg" alt="menu"></a>
</div>
</div>
<div class="main-part main-part1">
<div id="setting-layout">
<div id="setting-layout-left-col">
<?php echo $this->load->section('sidebar', 'sections/settingmenu'); ?>
</div>
<div id="setting-layout-right-col">
<div class="setting-layout-right-col-inner">
<?php 
$uriseg=$this->uri->segment_array();
/* folowing are the custome code  to display alert message  after different  action
 * this added because the HMVC  module not support the  session flashdata of codinighter
 * */
if($this->session->userdata('message_type')=='sucess' && $this->session->userdata('flashdata')!=''){
	?>
<div id="alertmssage" class="flash_msg"><?php echo $this->session->userdata('flashdata');?> </div>
<?php }
elseif($this->session->userdata('message_type')=='notice' && $this->session->userdata('flashdata')!=''){
	?>
<div id="alertnotice" class="flash_msg"><?php echo $this->session->userdata('flashdata');?></div>
<?php }
elseif($this->session->userdata('message_type')=='error' && $this->session->userdata('flashdata')!=''){
	?>
<div id="alerterror" class="flash_msg"><?php echo $this->session->userdata('flashdata');?></div>
<?php 
}
if($this->session->userdata('message_type')!='')
{
$this->session->unset_userdata('message_type');
}
if($this->session->userdata('flashdata')!='')
{
$this->session->unset_userdata('flashdata');
}
$this->session->set_userdata('flashdata','');
$this->session->set_userdata('message_type','');
/* alert messahe section end */
?>
<?php echo $output;?>
</div>
</div>
</div>
<div id="sidr-right">
<!-- Your content -->
<?php echo $this->load->section('sidebar', 'sections/sidebar'); ?>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.pulse.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap-checkbox.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.sidr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/custome.js"></script>


</div>
</body></html>