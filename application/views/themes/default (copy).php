<html lang="en">
	<head>
		<title><?php echo $title; ?></title>
		<meta name="resource-type" content="document" />
		<meta name="robots" content="all, index, follow"/>
		<meta name="googlebot" content="all, index, follow" />
	<?php
	/** -- Copy from here -- */
	if(!empty($meta))
	foreach($meta as $name=>$content){
		echo "\n\t\t";
		?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
			 }
	echo "\n";

	if(!empty($canonical))
	{
		echo "\n\t\t";
		?><link rel="canonical" href="<?php echo $canonical?>" /><?php

	}
	echo "\n\t";

	foreach($css as $file){
	 	echo "\n\t\t";
		?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
	} echo "\n\t";

	foreach($js as $file){
			echo "\n\t\t";
			?><script src="<?php echo $file; ?>"></script><?php
	} echo "\n\t";

	/** -- to here -- */
?>

<!-- Le styles -->
<link href="<?php echo base_url(); ?>assets/themes/default/css/style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap.css" rel="stylesheet">  
<!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap-multiselect.css" type="text/css">-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap-select.min.css">
<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/themes/default/images/favicon.png" type="image/x-icon"/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.sidr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.pulse.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/custome.js"></script>


<!-- Latest compiled and minified JavaScript -->



</head>
<body>
<div class="tab-wrapper" style="height:100%;width:100%"></div>

<div id="sidr-right">
<!-- Your content -->
<?php echo $this->load->section('sidebar', 'sections/sidebar'); ?>
</div>
<div class="header">
<div class="logo">
<a href="#"><img src="<?php echo base_url(); ?>assets/themes/default/images/logo.png" alt="logo"></a>
</div>
<div class="main-setting">
<div class="main-message-box">
<div class="message-box">
<a href="#"><img src="<?php echo base_url(); ?>assets/themes/default/images/msg.png" alt="message"></a>
</div>
<div class="message-content">
<a href="#"><img src="<?php echo base_url(); ?>assets/themes/default/images/msg-box.png" alt="msg-box"></a>
<h3>01</h3>
</div>
</div>
<div class="settings">
<a href="#"><img src="<?php echo base_url(); ?>assets/themes/default/images/setting.png" alt="Setting"></a>
</div>
<a id="right-menu" href="#sidr"><img src="<?php echo base_url(); ?>assets/themes/default/images/menu-buttn.jpg" alt="menu"></a>
</div>
</div>
<div class="main-part">

<?php 
$uriseg=$this->uri->segment_array();
/* folowing are the custome code  to display alert message  after different  action
 * this added because the HMVC  module not support the  session flashdata of codinighter
 * */
if($this->session->userdata('message_type')=='sucess' && $this->session->userdata('flashdata')!=''){
	?>
<div id="alertmssage" class=""><?php echo $this->session->userdata('flashdata');?> </div>
<?php }
elseif($this->session->userdata('message_type')=='notice' && $this->session->userdata('flashdata')!=''){
	?>
<div id="alertnotice" class=""><?php echo $this->session->userdata('flashdata');?></div>
<?php }
elseif($this->session->userdata('message_type')=='error' && $this->session->userdata('flashdata')!=''){
	?>
<div id="alerterror" class=""><?php echo $this->session->userdata('flashdata');?></div>
<?php 
}
if($this->session->userdata('message_type')!='')
{
$this->session->unset_userdata('message_type');
}
if($this->session->userdata('flashdata')!='')
{
$this->session->unset_userdata('flashdata');
}

/* alert messahe section end */
?>


 <?php echo $output;?>


</div>

</body></html>
