<?php $objad=new AdminController;?>
<ul>
<li><a href="<?php echo base_url(); ?>users">Dashboard</a></li>
<?php if(checkAccess($objad->accessLabelId,'users','view')){ ?>
<li class="active"><a href="<?php echo base_url('users/users_list'); ?>">Users List</a></li>
<?php }?>
<?php if(checkAccess($objad->accessLabelId,'hotels','view')){ ?>
<li><a href="<?php echo base_url('hotels'); ?>">Hotels</a></li>
<?php }?>
<?php if(checkAccess($objad->accessLabelId,'hotels','add')){ ?>
<li><a href="<?php echo base_url('hotels/information'); ?>">Add Hotel</a></li>
<?php }?>
<li><a href="<?php echo base_url('auth/logout'); ?>">Logout</a></li>
</ul>
