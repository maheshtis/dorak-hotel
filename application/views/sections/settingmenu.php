<?php $actFunction=$this->router->fetch_method();?>
<ul>
<li class="<?php echo ($actFunction=='chain')?'active':''?>"><a href="<?php echo base_url(); ?>settings">Hotel Chain </a></li>
<li class="<?php echo ($actFunction=='purpose')?'active':''?>"><a href="<?php echo base_url(); ?>settings/purpose" >Purpose </a></li>
<li class="<?php echo ($actFunction=='property_type')?'active':''?>"><a href="<?php echo base_url(); ?>settings/property_type">Property Type</a></li>
<li class="<?php echo ($actFunction=='facility')?'active':''?>"><a href="<?php echo base_url(); ?>settings/facility">Facility</a></li>
<li class="<?php echo ($actFunction=='currency')?'active':''?>"><a href="<?php echo base_url(); ?>settings/currency">Currency</a></li>
<li class="<?php echo ($actFunction=='country')?'active':''?>"><a href="<?php echo base_url(); ?>settings/country">Country</a></li>
<li class="<?php echo ($actFunction=='market')?'active':''?>"><a href="<?php echo base_url(); ?>settings/entity/market">Market</a></li>
<li class="<?php echo ($actFunction=='travel_type')?'active':''?>"><a href="<?php echo base_url(); ?>settings/entity/travel_type">Travel Type </a></li>
<li class="<?php echo ($actFunction=='room_type')?'active':''?>"><a href="<?php echo base_url(); ?>settings/entity/room_type">Room Type</a></li>


<li class="<?php echo ($actFunction=='meal_plan')?'active':''?>"><a href="<?php echo base_url(); ?>settings/meal_plan">Meal Plan</a></li>

<li class="<?php echo ($actFunction=='role')?'active':''?>"><a href="<?php echo base_url(); ?>settings/entity/role">Role</a></li>
<li class="<?php echo ($actFunction=='position')?'active':''?>"><a href="<?php echo base_url(); ?>settings/entity/position">Designation</a></li>

<li class="<?php echo ($actFunction=='star_rating')?'active':''?>"><a href="<?php echo base_url(); ?>settings/entity/star_rating">Star Rating</a></li>
<li class="<?php echo ($actFunction=='dorak_rating')?'active':''?>"><a href="<?php echo base_url(); ?>settings/entity/dorak_rating">Dorak Rating</a></li>
<li class="<?php echo ($actFunction=='status')?'active':''?>"><a href="<?php echo base_url(); ?>settings/entity/status">Status</a></li>
<li class="<?php echo ($actFunction=='cancellation_duration')?'active':''?>"><a href="<?php echo base_url(); ?>settings/cancellation_duration">Duration for cancellation</a></li>
<li class="<?php echo ($actFunction=='payment_duration')?'active':''?>"><a href="<?php echo base_url(); ?>settings/payment_duration">Duration for payment</a></li>
<li class="<?php echo ($actFunction=='age_group')?'active':''?>"><a href="<?php echo base_url(); ?>settings/age_group">Child Age group</a></li>
<li class="<?php echo ($actFunction=='company' || $actFunction=='edit_company')?'active':''?>"><a href="<?php echo base_url(); ?>settings/company">Company/Sub Company</a></li>
<li class="<?php echo ($actFunction=='departments')?'active':''?>"><a href="<?php echo base_url(); ?>settings/entity/departments">Departments</a></li>
</ul>