<div class="lofin-part">
    	<div class="logo-login">
        <a href=""><img src="<?php echo base_url(); ?>assets/themes/default/images/login-logo.png" alt="logo"></a>
       <div class="frm_contener login">
	   <div id="infoMessage"><?php echo $message;?></div>
               <?php echo form_open("auth/login",'class="loginform"');?>
                <div class="login-section">
                <label>User Name:</label>
        		<?php echo form_input($identity,'','placeholder="Enter Your User Name"');?>
				</div>
                <div class="login-section">
                <label>Password:</label><a href="forgot_password"><?php echo lang('login_forgot_password');?></a>
        		<?php echo form_input($password,'','placeholder="Enter Your Password"');?>               
			   </div>
                <div class="login-section login-section">
        		<input type="submit" value="Login">
                </div>
        <?php echo form_close();?>
     </div>
 </div>
</div>