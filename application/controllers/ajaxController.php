<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
  * @author tis
  * @author M
  * @description this controller  is for  handling ajax  calls
  */
class ajaxController extends AdminController {
	function __construct()
	{
	$this->load->helper(array('form_field_values'));
		parent::__construct();
	}
	
function getCountryCities()
{
	$country_code=$this->input->post('country_code');
	$CountrycitiesList=getCountyCities($country_code);
	$cityDropdwn='<select name="city" id="city_name" class="selectpicker" data-live-search="true" onchange=getCityDistricts(this)>';
	//$CountryCitiesOptions =array(''  => 'Select');	
	if($CountrycitiesList && count($CountrycitiesList)>0)
	{
	foreach($CountrycitiesList as $city)
	{
		$cityDropdwn.='<option value="'.$city->city_name.'">'.$city->city_name.'</option>';
	}
	}
	$cityDropdwn.='</select>';
	echo $cityDropdwn;
	exit();
}

function getCityDistricts()
{
	//$country_code=$this->input->post('country_code');
	$city_name=$this->input->post('city_name');
	$cityDistrictsList=getCityDistricts($city_name);
	$districtDropdwn='<select name="district" data-live-search="true" class="selectpicker">';
	if($cityDistrictsList && count($cityDistrictsList)>0)
	{
	foreach($cityDistrictsList as $district)
	{
		$districtDropdwn.='<option value="'.$district->district_name.'">'.$district->district_name.'</option>';
	}
	}
	$districtDropdwn.='</select>';
	echo $districtDropdwn;
	exit();
}

function getContactLine()
{
$row_num=$this->input->post('row_num');
$positions=getPositions();	
$selpos='<select class="selectpicker" name="contact['.$row_num.'][position]" data-validation="required" ><option value="">Select</option>';
	if(count($positions)>0)
	{
	foreach($positions as $position)
	{
		$selpos.='<option value="'.$position->id.'">'.$position->entity_title.'</option>';
	}
	}
	$selpos.='</select>';
	$fldrow='';
	$fldrow.='<div class="dt-main"><div class="dt">'.$selpos.'</div>';
	$fldrow.='<div class="dt"> <input type="text" name="contact['.$row_num.'][name]" value="" class="required" data-validation="custom,required,length" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation-length="min3" placeholder="Name"> </div><div class="dt"> <input type="text" name="contact['.$row_num.'][email]" value="" data-validation="required,email" class="required" placeholder="Email"></div><div class="dt"><input type="text" name="contact['.$row_num.'][phone]" value="" data-validation="required,phone_number" maxlength="20" data-validation-length="min10" placeholder="Mobile" class="required"></div> <div class="dt dt_extension"><input type="text" name="contact['.$row_num.'][extension]" value="" data-validation="length,number" data-validation-optional="true" data-validation-length="2-6" maxlength="6" size="6" class="required cnt_extension" placeholder="Ext."></div><a href="javascript:void(0)" class="remove_button"><img alt="close" src="'.base_url().'assets/themes/default/images/small-red-cross.png"></a><a href="javascript:void(0)" class="add_more_contact"><img alt="close" src="'.base_url().'assets/themes/default/images/small-green-plus.png"></a></div>';
	echo $fldrow;
	exit();
	}
	
function  getNextPaymentPlanRow()
		{					
			$row_num=$this->input->post('row_num');			
			$applicablePaymentOptions=getPaymentOptions();			
			$selpos='<select name="payment_plan['.$row_num.'][payment_option_id]" class="selectpicker" title="Select"><option value="">Select</option>';
			
				if(count($applicablePaymentOptions)>0)
				{
				foreach($applicablePaymentOptions as $applicablePaymentOption)
				{
					$selpos.='<option value="'.$applicablePaymentOption->id.'">'.$applicablePaymentOption->entity_title.'</option>';
				}
				}
				$selpos.='</select>';
				$fldrow='';
				$fldrow.='<div class="dt-row"><div class="lscan">'.$selpos.'</div>';
				$fldrow.='<div class="lscan"> <input type="text" name="payment_plan['.$row_num.'][payment_value]" value="" placeholder="0%"  data-validation-allowing="range[0.005;100],float" data-validation-optional="true" data-validation-allowing="float" data-validation="number" ></div><a href="javascript:void(0)" class="remove_lnk"><img alt="close" src="'.base_url().'assets/themes/default/images/small-red-cross.png"></a><a href="javascript:void(0);" class="add_payment_row" title="Remove fields row"><img src="'.base_url().'assets/themes/default/images/small-green-plus.png" alt="close"></a></div>';
				echo $fldrow;
				exit();		
		}
function getNextSessionRow()
{
$row_num=$this->input->post('row_num');
$session_type=$this->input->post('session_type');
$CancelationPeriods=getCancelationPeriods();
if($session_type=='low')	/* session type  are : low and high */
{
$selpos='<select name="lowseason_canceled['.$row_num.'][before]" class="selectpicker" title="Select"><option value="">Select</option>';
}
else
{
$selpos='<select name="highseason_canceled['.$row_num.'][before]" class="selectpicker" title="Select"><option value="">Select</option>';
}
	if(count($CancelationPeriods)>0)
	{
	foreach($CancelationPeriods as $CancelationPeriod)
	{
		$selpos.='<option value="'.$CancelationPeriod->entity_title.'">'.$CancelationPeriod->entity_title.'</option>';
	}
	}
	$selpos.='</select>';
	$fldrow='';
	$fldrow.='<div class="dt-row"><div class="lscan">'.$selpos.'</div>';
	if($session_type=='low')	/* session type  are : low and high */
	{
	$fldrow.='<div class="lscan"> <input type="text" name="lowseason_canceled['.$row_num.'][payment_request]" value="" placeholder="0%"  data-validation-allowing="range[0.005;100],float" data-validation-optional="true" data-validation-allowing="float" data-validation="number"></div><a href="javascript:void(0)" class="remove_lnk"><img alt="close" src="'.base_url().'assets/themes/default/images/small-red-cross.png"></a><a href="javascript:void(0);" class="add_lowsession_row" title="Remove fields row"><img src="'.base_url().'assets/themes/default/images/small-green-plus.png" alt="close"></a></div>';
	}
	else{
	  $fldrow.='<div class="lscan"> <input type="text" name="highseason_canceled['.$row_num.'][payment_request]" value="" placeholder="0%"  data-validation-allowing="range[0.005;100],float" data-validation-optional="true" data-validation-allowing="float" data-validation="number" ></div><a href="javascript:void(0)" class="remove_lnk"><img alt="close" src="'.base_url().'assets/themes/default/images/small-red-cross.png"></a><a href="javascript:void(0);" class="add_highsession_row" title="Remove fields row"><img src="'.base_url().'assets/themes/default/images/small-green-plus.png" alt="close"></a></div>';
	}
	echo $fldrow;
	exit();
	}

function getRenovationLine()	
{		
$row_num=$this->input->post('row_num');
$renovationOptions=getRenovationTypes();	
$selRenovation='<select class="selectpicker" name="rnv_shedule['.$row_num.'][renovation_type]" class="selectpicker" title="Select">';
	if($renovationOptions && count($renovationOptions)>0)
	{
	foreach($renovationOptions as $renovationOption)
	{
		$selRenovation.='<option value="'.$renovationOption->id.'">'.$renovationOption->renovation_type.'</option>';
	}
	}
	$selRenovation.='</select>';
	$fldrow='<div class="exdd cf">';
	$fldrow.='<div class="exdd-box"><div id="renovation_shedule_from_'.$row_num.'" class="input-group date"><input type="text" data-validation-help="dd/mm/yyyy" data-validation-format="dd/mm/yyyy" data-validation-optional="true" data-validation="date" class="has-help-txt" placeholder="From" value="" name="rnv_shedule['.$row_num.'][date_from]"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></div></div>';
	$fldrow.='<div class="exdd-box"><div id="renovation_shedule_to_'.$row_num.'" class="input-group date"><input type="text" data-validation-help="dd/mm/yyyy" data-validation-format="dd/mm/yyyy" data-validation-optional="true" placeholder="To" data-validation="date" class="has-help-txt" value="" name="rnv_shedule['.$row_num.'][date_to]"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></div></div>';
    $fldrow.='<div class="exdd-box exdd-box2">'.$selRenovation.'</div>';
	$fldrow.='<div class="exdd-box exdd-box2 exdd-box3">
	<textarea id="rnv_shedule['.$row_num.'][area_effected]" rows="1" cols="10" name="rnv_shedule['.$row_num.'][area_effected]"></textarea></div>';
	$fldrow.='<a href="javascript:void(0)" class="remove_lnk"><img alt="close" src="'.base_url().'assets/themes/default/images/small-red-cross.png"></a></div>';
	echo $fldrow;
	exit();	
}

private function getMarketOption($block_num,$row_num)
{
$defMarket=$this->config->item('default_market');
$markets=getMarkets(); /* using helper*/
$marketSelect='<select name="pricing['.$block_num.'][price_detail]['.$row_num.'][market_id]" class="selectpicker"  title="Select">';
	if($markets && count($markets)>0)
	{
	foreach($markets as $market)
	{
	$selected='';
	if($market->id==$defMarket)
	{
	$selected='selected';	
	}
	$marketSelect.='<option value="'.$market->id.'" '.$selected.'>'.$market->entity_title.'</option>';
	}
	}	
	$marketSelect.='</select>';
	return $marketSelect;
}

private function getCurrenciesOptions($block_num='')
{
$defCurencyCode=$this->config->item('default_curency_code');
$currencies=getCurrencies();		
$CurrenciesSelect='<select name="pricing['.$block_num.'][currency]" class="selectpicker"  title="Select">';
if($currencies && count($currencies)>0)
	{
	foreach($currencies as $cCode=>$currency)
	{
	$selected='';
	if($cCode==$defCurencyCode)
	{
	$selected='selected';	
	}
		$CurrenciesSelect.='<option value="'.$cCode.'" '.$selected.'>'.$cCode.'</option>';
	}
	}
	$CurrenciesSelect.='</select>';
	return $CurrenciesSelect;
}

private function getRoomTypesOptions($block_num='')
{
$roomTypes=getRoomTypes(); /* using helper*/
$roomTypesSelect='<select name="pricing['.$block_num.'][room_type]"  class="selectpicker" title="Select">';
	if($roomTypes && count($roomTypes)>0)
	{
	foreach($roomTypes as $roomType)
	{
		$roomTypesSelect.='<option value="'.$roomType->id.'">'.$roomType->entity_title.'</option>';
	}
	}
	$roomTypesSelect.='</select>';
	return $roomTypesSelect;
	
}

private function getRoomCmplServicesOptions($block_num='')
{
$roomCmplServices=getAvailabeComplementaryServices();
$RoomCmplServicesSelect='<select name="pricing['.$block_num.'][room_facilities][]" data-selected-text-format="count > 2" multiple class="datacheckroom"  title="Select">';
	if($roomCmplServices && count($roomCmplServices)>0)
	{
	foreach($roomCmplServices as $roomCmplService)
	{
		$RoomCmplServicesSelect.='<option value="'.$roomCmplService->id.'">'.$roomCmplService->entity_title.'</option>';
	}
	}
	$RoomCmplServicesSelect.='</select>';
	return $RoomCmplServicesSelect;
}

function getNewPriceLine()	
{		
$row_num=$this->input->post('row_num');
$block_num=$this->input->post('block_num');
$placeholder='0.00';
$marketSelect= $this->getMarketOption($block_num,$row_num);
$fldrow='<tr><td width="13%"><div class="counter-prive-table-select">'.$marketSelect.'</div></td><td width="9%"><div class="counter-prive-table-int"> 
<input type="text" placeholder="'.$placeholder.'" data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][double_price]"></div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'" data-validation-optional="true" data-validation-allowing="float"data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][triple_price]"> </div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'" data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][quad_price]"></div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'"  data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][breakfast_price]"> </div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'"  data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][half_board_price]"></div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'"  data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][all_incusive]"> </div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'"  data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][extra_adult]"> </div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'"  data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][extra_child]"> </div></td><td width="9%"><div class="counter-prive-table-int counter-prive-table-int1"><input type="text" placeholder="'.$placeholder.'"  data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][extra_bed]"></div></td><td width="6%"><div class="counter-lnk"> <a class="price-remove-lnk" rel="'.$row_num.'" href="javascript:void(0);" onclick="removerow(this,'.$row_num.');"><img alt="close" src="'.base_url().'assets/themes/default/images/small-red-cross.png"></a><a class="add-price-row" href="javascript:void(0);" onClick="addnerow(this,'.$row_num.')" rel="'.$block_num.'"><img alt="Add more" src="'.base_url().'assets/themes/default/images/small-green-plus.png"></a></div></td></tr>';
	echo $fldrow;
	exit();	
}

function nextPriceBlock()
{
$block_num=$this->input->post('block_num');
$row_num=1;
$placeholder='0.00';
$marketSelect= $this->getMarketOption($block_num,1);
$curencySelect=$this->getCurrenciesOptions($block_num);
$ServicesSelect=$this->getRoomCmplServicesOptions($block_num);
$RoomTypesSelect=$this->getRoomTypesOptions($block_num);
$fldrow='<div class="price-rate-main-page cf cf-row"><a href="javascript:void(0);"  class="price-block-remove-lnk"><img src="'.base_url().'assets/themes/default/images/small-red-cross.png" alt="close"></a> <div class="price-room-main-page"><div class="prive-room-box-left"><div class="dd"><div class="dt rp-dt"><label>Room Type</label>'.$RoomTypesSelect.'</div> <div class="dt rp-dt"><label>Currency  </label>'.$curencySelect.'</div><div class="dt inventory-ty"><label>Inventory  </label><input type="text" data-validation="number" data-validation-optional="true" value="" name="pricing['.$block_num.'][invenory]"> </div><div class="dt rp-dt1"><label>Complimentary</label>'.$ServicesSelect.'</div></div><div class="dd"><div class="dt inventory-ty"><label>Max Adult </label><input type="text" data-validation="number" data-validation-optional="true" value="" name="pricing['.$block_num.'][max_adult]"></div><div class="dt inventory-ty"><label>Max Child</label><input type="text" data-validation="number" data-validation-optional="true" value="" name="pricing['.$block_num.'][max_child]"></div><div class="price-dta-ran"><label>Period</label><div class="dt pdtrn"><div class="form-group"><div id="period_from_date_'.$block_num.'" class="input-group date"><input type="text" placeholder="From" data-validation-help="dd/mm/yyyy" data-validation-format="dd/mm/yyyy"data-validation-optional="true" data-validation="date" value="" name="pricing['.$block_num.'][period_from_date]"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></div></div></div><div class="dt pdtrn"><div class="form-group"><div id="period_to_date_'.$block_num.'" class="input-group date"><input type="text" placeholder="To" data-validation-help="dd/mm/yyyy" data-validation-format="dd/mm/yyyy" data-validation-optional="true" data-validation="date" value="" name="pricing['.$block_num.'][period_to_date]"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></div></div></div></div></div></div><div class="prive-room-box-right"><div class="dd"><label>Inclusions</label><textarea id="pricing['.$block_num.'][pricing_inclusions]" rows="5" cols="10" name="pricing['.$block_num.'][pricing_inclusions]"></textarea></div></div></div>';
$fldrow.=' <div class="price-rate-main-page1"><table width="100%" cellspacing="1" cellpadding="5" border="0" align="center" id="price_rate_table_'.$block_num.'"><thead><tr><th width="13%">Market </th><th width="9%">Double</th><th width="9%">Triple </th><th width="9%">Quad </th><th width="9%"> Breakfast </th><th width="9%">Half Board </th><th width="9%">All Inclusive </th><th width="9%">Extra Adult</th><th width="9%">Extra Child </th><th width="9%">Extra Bed</th><th width="6%"><a class="tg-bdy dwn" onclick="tgbdybind(this);">&nbsp;</a></th></tr></thead><tbody><tr><td width="13%"><div class="counter-prive-table-select">'.$marketSelect.'</div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'" data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][double_price]"> </div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'" data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][triple_price]"> </div> </td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'" data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][quad_price]"> </div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'" data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][breakfast_price]"> </div></td><td width="9%"> <div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'" data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][half_board_price]"> </div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'" data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][all_incusive]"> </div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'" data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][extra_adult]"> </div></td><td width="9%"><div class="counter-prive-table-int"><input type="text" placeholder="'.$placeholder.'" data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][extra_child]"> </div></td><td width="9%"><div class="counter-prive-table-int counter-prive-table-int1"><input type="text" placeholder="'.$placeholder.'" data-validation-optional="true" data-validation-allowing="float" data-validation="price" value="" name="pricing['.$block_num.'][price_detail]['.$row_num.'][extra_bed]"></div></td><td width="6%"><div class="counter-lnk"><a class="add-price-row" rel="'.$block_num.'" onclick="addnerow(this,'.$row_num.');"><img alt="Add more" src="'.base_url().'assets/themes/default/images/small-green-plus.png"></a></div></td></tr></tbody></table></div>';
echo $fldrow;
exit();
}


function getNextMemberRow()
{
$row_num=$this->input->post('row_num');
$positions=getPositions();	
$selpos='<select class="selectpicker" name="member['.$row_num.'][position]"><option value="">Select</option>';
	if(count($positions)>0)
	{
	foreach($positions as $position)
	{
		$selpos.='<option value="'.$position->entity_title.'">'.$position->entity_title.'</option>';
	}
	}
	$selpos.='</select>';
	$fldrow='<div class="mcnt"><div class="dt"> <input type="text" name="member['.$row_num.'][name]" value=""  data-validation="custom,length" data-validation-optional="true" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation-length="min3" placeholder="Name"> </div>';
	$fldrow.='<div class="dt">'.$selpos.'</div>';
	$fldrow.='<div class="dt"> <input type="text" name="member['.$row_num.'][email]" value="" data-validation=",email" data-validation-optional="true" placeholder="Email"></div><a href="javascript:void(0)" class="remove_lnk"><img alt="close" src="'.base_url().'assets/themes/default/images/small-red-cross.png"></a></div>';
	echo $fldrow;
	exit();
	}


function googleSearch()
 {	$searchTerm=$_GET['term'];
	//if input value greater the 2 then search
	$googleApi=$this->config->item('googleApi'); 
	if(strlen($searchTerm)>2)
	{  try{
			 // curl initilaize here 
			 $ch = curl_init(); 
			 // set curl url with input value
			 curl_setopt($ch, CURLOPT_URL,"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=".urlencode($searchTerm)."&language=en&key=".$googleApi);
			 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			 curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			 // curl execute here 
			 $server_output = curl_exec ($ch);
			 // curl close 
			 curl_close ($ch);
			 // check return value ture or false
			if($server_output)
			{
					// return value is json encoded so here decode the value
					$json = json_decode($server_output,true); 
					// check response status of google place api if OK indicates that no errors occurred and at least one result was returned.
					if($json['status']=='OK')
					{
						$data= $json['predictions'] ;
						if($data)
						{	
							foreach($data as $val)
							{
								$value[]=$val['description'];
							}
							echo json_encode($value);
							exit();
						}
					}
				 	// if status  ZERO_RESULTS indicates that the search was successful but returned no results. This may occur if the search was passed a bounds in a remote location.
					if($json['status']=='ZERO_RESULTS')
					{
						
					}
					// if status OVER_QUERY_LIMIT indicates that you are over your quota.
					if($json['status']=='OVER_QUERY_LIMIT')
					{
						
					}
					//if status REQUEST_DENIED indicates that your request was denied, generally because of lack of an invalid key
					if($json['status']=='REQUEST_DENIED')
					{
						
					}
					//if status INVALID_REQUEST generally indicates that the input parameter is missing.
					if($json['status']=='INVALID_REQUEST')
					{	
					}
			}
		}catch(Exception $e)
		  {
		  echo 'Message: ' .$e->getMessage();
		  }	
	}
}
function get_location_point()
{
    $LoctionPoint = NULL;
	$country_code = $this->input->post('country_code');
	$city_id = $this->input->post('city_id');
    $LoctionPoint = getLocation_point($country_code,$city_id);
	
        if(!empty($LoctionPoint)){
           $json = json_encode($LoctionPoint, true);
            print_r($json);die;
        }else{
            $json = json_encode($LoctionPoint, true);
            print_r($json);die;
        }
}
}