--
-- Triggers `hotel_contact_details`
--
DROP TRIGGER IF EXISTS `contact_delete_log`;
DELIMITER //
CREATE TRIGGER `contact_delete_log` AFTER DELETE ON `hotel_contact_details`
 FOR EACH ROW BEGIN
	
	DECLARE designation varchar(255);
	
	-- Find position of contact info the INSERT into table
   SELECT title from positions where id = OLD.position INTO designation;
   
   -- Insert record into audit table
   INSERT INTO hotel_contact_log
   ( hotel_id,
	 position,
	 name,
	 email,
	 phone,
	 extension,
	 last_updated)
   VALUES
   ( OLD.hotel_id,
     designation,	
	 OLD.name,
     OLD.email,
	 OLD.phone,
	 OLD.extension,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `contact_log_insert`;
DELIMITER //
CREATE TRIGGER `contact_log_insert` AFTER INSERT ON `hotel_contact_details`
 FOR EACH ROW BEGIN
	
	   
   -- Insert record into audit table
   INSERT INTO hotel_contact_log
   ( hotel_id,
	 position,
	 name,
	 email,
	 phone,
	 extension,
	 last_updated)
   VALUES
   ( NEW.hotel_id,
     NEW.position,	
	 NEW.name,
	 NEW.email,
     NEW.phone,
	 NEW.extension,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `hotel_contact_log`;
DELIMITER //
CREATE TRIGGER `hotel_contact_log` AFTER UPDATE ON `hotel_contact_details`
 FOR EACH ROW BEGIN
	
	   
   -- Insert record into audit table
   INSERT INTO hotel_contact_log
   ( hotel_id,
	 position,
	 name,
	 email,
	 phone,
	 extension,
	 last_updated)
   VALUES
   ( NEW.hotel_id,
     NEW.position,	
	 NEW.name,
	 NEW.email,
     NEW.phone,
	 NEW.extension,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;