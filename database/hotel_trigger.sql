--
-- Triggers Hotel
--


DROP TRIGGER IF EXISTS `hotel_audit`;
DELIMITER //
CREATE TRIGGER `hotel_audit` AFTER UPDATE ON `hotels`
 FOR EACH ROW BEGIN

	DECLARE vUser varchar(50);

	DECLARE childAge varchar(50);

	DECLARE hotelPurpose varchar(255);

	DECLARE hotelChain varchar(255);

   -- Find username of person performing the INSERT into table
   SELECT first_name from dorak_users where id = NEW.created_byuser INTO vUser;
	
   SELECT age_range from child_age_ranges where id = NEW.child_age_group_id INTO childAge;

	-- Find hotel purpose of hotel performing the INSERT into table
   SELECT title from  hotel_purpose where id = NEW.purpose INTO hotelPurpose;

	-- Find hotel chain_title of chainlist performing the INSERT into table
   SELECT title from  hotel_chain_list where chain_id = NEW.chain_id INTO hotelChain;
	
 IF NEW.last_updated <> OLD.last_updated
	THEN
   -- Insert record into audit table
   INSERT INTO hotels_log
   ( hotel_id,
     log_operation,
	 hotel_code,
	 chain_id,
	 purpose,
	 hotel_name,
	 hotel_address,
	 city,
	 country_code,
	 district,
	 post_code,
	 hotel_image,
	 currency,
	 star_rating,
	 dorak_rating,
	 commisioned,
	 child_age_group_id,
	 status,
	 last_updated,
     created_byuser)
   VALUES
   ( NEW.hotel_id,
     'update',
     NEW.hotel_code,
     hotelChain,
     hotelPurpose,
     NEW.hotel_name,
     NEW.hotel_address,
     NEW.city,
     NEW.country_code,
     NEW.district,
     NEW.post_code,
     NEW.hotel_image,
     NEW.currency,
     NEW.star_rating,
     NEW.dorak_rating,
     NEW.commisioned,
     childAge,
     NEW.status,
     CURRENT_TIMESTAMP,
     vUser );
END IF;

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `hotel_audit_delete`;
DELIMITER //
CREATE TRIGGER `hotel_audit_delete` AFTER DELETE ON `hotels`
 FOR EACH ROW BEGIN

	DECLARE vUser varchar(50);

	DECLARE childAge varchar(50);

	DECLARE hotelPurpose varchar(255);

	DECLARE hotelChain varchar(255);

   -- Find username of person performing the INSERT into table
   SELECT first_name from dorak_users where id = OLD.created_byuser INTO vUser;
	
   SELECT age_range from child_age_ranges where id = OLD.child_age_group_id INTO childAge;

	-- Find hotel purpose of hotel performing the INSERT into table
   SELECT title from  hotel_purpose where id = OLD.purpose INTO hotelPurpose;

	-- Find hotel chain_title of chainlist performing the INSERT into table
   SELECT title from  hotel_chain_list where chain_id = OLD.chain_id INTO hotelChain;
	
   -- Insert record into audit table
   INSERT INTO hotels_log
   ( hotel_id,
     log_operation,
	 hotel_code,
	 chain_id,
	 purpose,
	 hotel_name,
	 hotel_address,
	 city,
	 country_code,
	 district,
	 post_code,
	 hotel_image,
	 currency,
	 star_rating,
	 dorak_rating,
	 commisioned,
	 child_age_group_id,
	 status,
	 last_updated,
     created_byuser)
   VALUES
   ( OLD.hotel_id,
     'update',
     OLD.hotel_code,
     hotelChain,
     hotelPurpose,
     OLD.hotel_name,
     OLD.hotel_address,
     OLD.city,
     OLD.country_code,
     OLD.district,
     OLD.post_code,
     OLD.hotel_image,
     OLD.currency,
     OLD.star_rating,
     OLD.dorak_rating,
     OLD.commisioned,
     childAge,
     OLD.status,
     CURRENT_TIMESTAMP,
     vUser );

END
//
DELIMITER ;





