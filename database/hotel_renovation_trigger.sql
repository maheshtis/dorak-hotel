--
-- Triggers `hotel_renovation_schedule`
--
DROP TRIGGER IF EXISTS `renovation-after-delete`;
DELIMITER //
CREATE TRIGGER `renovation-after-delete` AFTER DELETE ON `hotel_renovation_schedule`
 FOR EACH ROW BEGIN

   -- Insert record into audit table
   INSERT INTO renovation_log
   ( hotel_id,
	 date_from,
	 date_to,
	 renovation_type,
	 area_effected,
	 last_updated)
   VALUES
   ( OLD.hotel_id,
     OLD.date_from,	
	 OLD.date_to,
	 OLD.renovation_type,
	 OLD.area_effected,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `renovation-after-insert`;
DELIMITER //
CREATE TRIGGER `renovation-after-insert` AFTER INSERT ON `hotel_renovation_schedule`
 FOR EACH ROW BEGIN

   -- Insert record into audit table
   INSERT INTO renovation_log
   ( hotel_id,
	 date_from,
	 date_to,
	 renovation_type,
	 area_effected,
	 last_updated)
   VALUES
   ( NEW.hotel_id,
     NEW.date_from,	
	 NEW.date_to,
	 NEW.renovation_type,
	 NEW.area_effected,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `renovation_log`;
DELIMITER //
CREATE TRIGGER `renovation_log` AFTER UPDATE ON `hotel_renovation_schedule`
 FOR EACH ROW BEGIN

IF NEW.last_updated <> OLD.last_updated
THEN
   -- Insert record into audit table
   INSERT INTO renovation_log
   ( hotel_id,
	 date_from,
	 date_to,
	 renovation_type,
	 area_effected,
	 last_updated)
   VALUES
   ( NEW.hotel_id,
     NEW.date_from,	
	 NEW.date_to,
	 NEW.renovation_type,
	 NEW.area_effected,
     CURRENT_TIMESTAMP);

END IF;

END
//
DELIMITER ;