--
-- Table structure for table `hotel_contract_info`
--

CREATE TABLE IF NOT EXISTS `hotel_contract_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `signed_by` varchar(500) NOT NULL,
  `contract_file` varchar(500) DEFAULT NULL,
  `creared_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=125 ;

--
-- Dumping data for table `hotel_contract_info`
--

INSERT INTO `hotel_contract_info` (`id`, `hotel_id`, `start_date`, `end_date`, `signed_by`, `contract_file`, `creared_on`, `status`) VALUES
(113, 141, '2016-03-24 00:00:00', '2016-03-31 00:00:00', 'tis india', 'Dorak_database_schema_after_sprint_23.pdf', '2016-03-22 04:11:09', 1),
(114, 102, '2016-03-31 00:00:00', '2016-04-01 00:00:00', 'fdsfsf', '', '2016-03-31 15:17:08', 1),
(115, 102, '2016-03-31 00:00:00', '2016-04-08 00:00:00', 'jeevan', '', '2016-03-31 15:18:56', 1),
(116, 102, '2016-03-31 00:00:00', '2016-04-02 00:00:00', 'jeevan', '', '2016-03-31 15:22:27', 1),
(117, 102, '2016-03-31 00:00:00', '2016-04-08 00:00:00', 'sdfsdf', '', '2016-03-31 15:23:23', 1),
(118, 102, '2016-03-31 00:00:00', '2016-04-08 00:00:00', 'sdfsdf', '', '2016-03-31 15:32:33', 1),
(120, 102, '2016-03-31 00:00:00', '2016-04-01 00:00:00', 'dsdasd', '', '2016-03-31 15:33:26', 1),
(121, 102, '2016-03-31 00:00:00', '2016-04-09 00:00:00', 'dad', '', '2016-03-31 15:41:55', 1),
(122, 102, '2016-03-31 00:00:00', '2016-04-09 00:00:00', 'fdsfsf', 'Dorak_database_schema_after_sprint_235.pdf', '2016-03-31 15:56:13', 1),
(123, 114, '2016-04-06 00:00:00', '2016-04-14 00:00:00', 'jeevan', '', '2016-04-06 06:02:21', 1),
(124, 133, '2016-04-06 00:00:00', '2016-04-07 00:00:00', 'fdsfsf', '', '2016-04-06 07:51:12', 1);

--
-- Triggers `hotel_contract_info`
--
DROP TRIGGER IF EXISTS `contact_after_insert`;
DELIMITER //
CREATE TRIGGER `contact_after_insert` AFTER INSERT ON `hotel_contract_info`
 FOR EACH ROW BEGIN
	
	   
   -- Insert record into audit table
   INSERT INTO hotel_contract_info_log
   ( hotel_id,
	 start_date,
	 end_date,
	 signed_by,
	 contract_file,
	 creared_on)
   VALUES
   ( NEW.hotel_id,
     NEW.start_date,	
	 NEW.end_date,
	 NEW.signed_by,
	 NEW.contract_file,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `hotele_contract_delete`;
DELIMITER //
CREATE TRIGGER `hotele_contract_delete` AFTER DELETE ON `hotel_contract_info`
 FOR EACH ROW BEGIN
	
	   
   -- Insert record into audit table
   INSERT INTO hotel_contract_info_log
   ( hotel_id,
	 start_date,
	 end_date,
	 signed_by,
	 contract_file,
	 creared_on)
   VALUES
   ( OLD.hotel_id,
     OLD.start_date,	
	 OLD.end_date,
	 OLD.signed_by,
	 OLD.contract_file,
     CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `hotele_contract_log`;
DELIMITER //
CREATE TRIGGER `hotele_contract_log` AFTER UPDATE ON `hotel_contract_info`
 FOR EACH ROW BEGIN
	
	IF NEW.creared_on <> OLD.creared_on
    THEN
   -- Insert record into audit table
   INSERT INTO hotel_contract_info_log
   ( hotel_id,
	 start_date,
	 end_date,
	 signed_by,
	 contract_file,
	 creared_on)
   VALUES
   ( NEW.hotel_id,
     NEW.start_date,	
	 NEW.end_date,
	 NEW.signed_by,
	 NEW.contract_file,
     CURRENT_TIMESTAMP);
END IF;

END
//
DELIMITER ;