--
-- Triggers `hotel_cancellation`
--
DROP TRIGGER IF EXISTS `cancel_after_insert`;
DELIMITER //
CREATE TRIGGER `cancel_after_insert` AFTER INSERT ON `hotel_cancellation`
 FOR EACH ROW BEGIN

   -- Insert record into audit table
   INSERT INTO hotel_cancellation_log
   ( hotel_id,
	 seasion,
	 cancelled_before,
	 payment_request,
	 last_updated)
   VALUES
   ( NEW.hotel_id,
     NEW.seasion,	
	 NEW.cancelled_before,
	 NEW.payment_request,
	 CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `cancel_aftre_delete`;
DELIMITER //
CREATE TRIGGER `cancel_aftre_delete` AFTER DELETE ON `hotel_cancellation`
 FOR EACH ROW BEGIN

   -- Insert record into audit table
   INSERT INTO hotel_cancellation_log
   ( hotel_id,
	 seasion,
	 cancelled_before,
	 payment_request,
	 last_updated)
   VALUES
   ( OLD.hotel_id,
     OLD.seasion,	
	 OLD.cancelled_before,
	 OLD.payment_request,
	 CURRENT_TIMESTAMP);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `hotel_after_delete`;
DELIMITER //
CREATE TRIGGER `hotel_after_delete` AFTER UPDATE ON `hotel_cancellation`
 FOR EACH ROW BEGIN

IF NEW.last_updated <> OLD.last_updated
THEN
   -- Insert record into audit table
   INSERT INTO hotel_cancellation_log
   ( hotel_id,
	 seasion,
	 cancelled_before,
	 payment_request,
	 last_updated)
   VALUES
   ( NEW.hotel_id,
     NEW.seasion,	
	 NEW.cancelled_before,
	 NEW.payment_request,
	 CURRENT_TIMESTAMP);

END IF;

END
//
DELIMITER ;