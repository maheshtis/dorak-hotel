--
-- Triggers `hotel_rooms_pricing_complimentary`
--
DROP TRIGGER IF EXISTS `pricing_complimentry_after_delete`;
DELIMITER //
CREATE TRIGGER `pricing_complimentry_after_delete` AFTER DELETE ON `hotel_rooms_pricing_complimentary`
 FOR EACH ROW BEGIN

   INSERT INTO hotel_rooms_pricing_details_log
   ( pricing_id,
     cmpl_service_id)
   VALUES
   ( OLD.pricing_id,
     OLD.cmpl_service_id);


END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `pricing_complimentry_after_insert`;
DELIMITER //
CREATE TRIGGER `pricing_complimentry_after_insert` AFTER INSERT ON `hotel_rooms_pricing_complimentary`
 FOR EACH ROW BEGIN

DECLARE serviceName varchar(255);
	
	-- Find service_name from complimentary_services table for tracking
   SELECT service_name from complimentary_services where cmpl_service_id = NEW.cmpl_service_id INTO serviceName;

   -- Insert record into audit table
   INSERT INTO hotel_rooms_pricing_details_log
   ( pricing_id,
     cmpl_service_id)
   VALUES
   ( NEW.pricing_id,
     serviceName);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `pricing_complimentry_after_update`;
DELIMITER //
CREATE TRIGGER `pricing_complimentry_after_update` AFTER UPDATE ON `hotel_rooms_pricing_complimentary`
 FOR EACH ROW BEGIN

DECLARE serviceName varchar(255);
	
	-- Find service_name from complimentary_services table for tracking
   SELECT service_name from complimentary_services where cmpl_service_id = OLD.cmpl_service_id INTO serviceName;

IF NEW.pricing_id <> OLD.pricing_id
THEN
   -- Insert record into audit table
   INSERT INTO hotel_rooms_pricing_details_log
   ( pricing_id,
     cmpl_service_id)
   VALUES
   ( NEW.pricing_id,
     serviceName);

END IF;

END
//
DELIMITER ;