--
-- Triggers `hotel_complimentary_room`
--
DROP TRIGGER IF EXISTS `complimenry_after_insert`;
DELIMITER //
CREATE TRIGGER `complimenry_after_insert` AFTER INSERT ON `hotel_complimentary_room`
 FOR EACH ROW BEGIN
	
	DECLARE roomType varchar(255);
	
	-- Find room type from room type table the INSERT into table
   SELECT title from hotel_room_types where type_id = NEW.room_type INTO roomType;
   
   -- Insert record into audit table
   INSERT INTO hotel_complimentary_room_log
   ( hotel_id,
	 room_night,
	 room_type,
	 start_date,
	 end_date,
	 upgrade)
   VALUES
   ( NEW.hotel_id,
     NEW.room_night,
	 roomType,
	 NEW.start_date,
	 NEW.end_date,
     NEW.upgrade);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `complimenry_room_delete_log`;
DELIMITER //
CREATE TRIGGER `complimenry_room_delete_log` AFTER DELETE ON `hotel_complimentary_room`
 FOR EACH ROW BEGIN
	
	DECLARE roomType varchar(255);
	
	-- Find room type from room type table the INSERT into table
   SELECT title from hotel_room_types where type_id = OLD.room_type INTO roomType;
   
   -- Insert record into audit table
   INSERT INTO hotel_complimentary_room_log
   ( hotel_id,
	 room_night,
	 room_type,
	 start_date,
	 end_date,
	 upgrade)
   VALUES
   ( OLD.hotel_id,
     OLD.room_night,
	 roomType,
	 OLD.start_date,
	 OLD.end_date,
     OLD.upgrade);

END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `complimenry_room_log`;
DELIMITER //
CREATE TRIGGER `complimenry_room_log` AFTER UPDATE ON `hotel_complimentary_room`
 FOR EACH ROW BEGIN
	
	DECLARE roomType varchar(255);
	
	-- Find room type from room type table the INSERT into table
   SELECT title from hotel_room_types where type_id = NEW.room_type INTO roomType;
   
   IF NEW.room_night <> OLD.room_night or NEW.room_type <> OLD.room_type or 		NEW.start_date <> OLD.start_date or NEW.end_date <> OLD.end_date or 		NEW.upgrade <> OLD.upgrade
THEN
   -- Insert record into audit table
   INSERT INTO hotel_complimentary_room_log
   ( hotel_id,
	 room_night,
	 room_type,
	 start_date,
	 end_date,
	 upgrade)
   VALUES
   ( NEW.hotel_id,
     NEW.room_night,
	 roomType,
	 NEW.start_date,
	 NEW.end_date,
     NEW.upgrade);
 
END IF;

END
//
DELIMITER ;