
function  customeChecking()
{
		var error_with=false;
		var hname= $('#hotel_name').val();
		var hcurrency= $('#currency').val();
		var hstatus= $('#status').val();
		var haddress= $('#hotel_address').val();
		var hpostcode= $('#post_code').val();
		var hcityname= $('#city_name').val();
		var hcountrycode= $('#country_code').val();
		var hcname= $('#contact_1_name').val();
		var hcphone= $('#contact_1_phone').val();
		var hcemail= $('#contact_1_email').val();
		var hcposition= $('#contact_1_position').val();
		
		if(hname=='')
		{
		error_with='#hotel_name';	
		}
		else if(hcurrency=='')
		{
		error_with='#currency';	
		}
		else if(hstatus=='')
		{
		error_with='#status';	
		}
		else if(haddress=='')
		{
		error_with='#hotel_address';	
		}
		else if(hpostcode=='')
		{
		error_with='#post_code';	
		}
		else if(hcityname=='')
		{
		error_with='#city_name';	
		}
		else if(hcountrycode=='')
		{
		error_with='#country_code';	
		}
		else if(hcname=='')
		{
		error_with='#contact_1_name';	
		}
		else if(hcphone=='')
		{
		error_with='#contact_1_phone';	
		}
		else if(hcemail=='')
		{
		error_with='#contact_1_email';	
		}
		else if(hcposition=='')
		{
		error_with='#contact_1_position';	
		}
		else{
			error_with=false;
		}
		if(error_with && error_with!="")
		{
		$(".tab-content2").css("display", "none");
		$('ul.cf li').removeClass("current");
		$('#default-tab').addClass("current"); 
        $('#tab-1').css("display", "block");		
		  return {			  
			element : $(error_with),
			message : 'This is a required field'
		  }
		}
}	



function callbackValidator()
{
	$.validate({
	form : '#hotel_infoadd',
    decimalSeparator : '.',
	onValidate : function(form) {
	return customeChecking();	// custome validation added for foerb tab data
    }
	});
}

function getNextBankFld(n,baseUrl)
	{	
 	var fieldHTML = '<div class="bank-details-main-box cf"><div class="n-row"><div class="bank-details-main-input-box"><label> Iban  Code</label><input type="text" size="40" maxlength="40" data-validation-optional="true" data-validation-length="3-40" data-validation="alphanumeric,length" data-validation-allowing=" " data-validation-help="Minimum  length 3 " value="" name="bank['+ n +'][iban_code]"> </div><div class="bank-details-main-input-box"><label>Account Number </label><input type="text" size="20" maxlength="16" data-validation-optional="true" data-validation-length="3-16" data-validation="number,length" data-validation-help="Minimum  3 digit" class="required" value="" name="bank['+ n +'][account_number]"> </div><div class="bank-details-main-input-box"><label>Account Name</label><input type="text" size="100" maxlength="100" data-validation-optional="true" data-validation-length="3-100" data-validation-help="Minimum  3 characters" data-validation-regexp="^[a-zA-Z\-\s]*$" data-validation="custom,length" value="" name="bank['+ n +'][account_name]"> </div><div class="bank-details-main-input-box"><label>Bank Name  </label><input type="text" size="100" maxlength="100" data-validation-optional="true" data-validation-length="3-100" data-validation-help="Minimum  3 characters " data-validation="custom,length" data-validation-regexp="^[a-zA-Z\-\s]*$" value="" name="bank['+ n +'][bank_name]"></div><div class="bank-details-main-input-box"><label>Bank Address </label><input type="text" size="100" maxlength="100" data-validation-optional="true" data-validation-length="10-150" data-validation-help="Minimum  10 characters" data-validation-allowing="-_@#:, " data-validation="alphanumeric,length" value="" name="bank['+ n +'][bank_address]"></div></div><div class="n-row"><div class="bank-details-main-input-box"><label>Branch Name  </label><input type="text" size="100" maxlength="100" data-validation-optional="true" data-validation-length="3-100" data-validation-help="Minimum  3 characters "data-validation="custom,length" data-validation-regexp="^[a-zA-Z\-\s]*$"  value="" name="bank['+ n +'][branch_name]"></div><div class="bank-details-main-input-box"><label> Branch Code</label><input type="text" size="30" maxlength="30" data-validation-allowing=" " data-validation-optional="true" data-validation-length="3-20" data-validation="alphanumeric,length" data-validation-help="Minimum  length 3 " value="" name="bank['+ n +'][branch_code]"> </div><div class="bank-details-main-input-box"><label> IFSC  Code</label><input type="text" size="30" maxlength="30" data-validation-optional="true" data-validation-length="3-20" data-validation="alphanumeric,length" data-validation-help="Minimum  length 3 " data-validation-allowing=" " value="" name="bank['+ n +'][bank_ifsc_code]"> </div><div class="bank-details-main-input-box"><label> SWIFT  Code</label><input type="text" size="30" maxlength="30" data-validation-optional="true" data-validation-allowing=" " data-validation-length="3-20" data-validation="alphanumeric,length" data-validation-help="Minimum  length 3 " value="" name="bank['+ n +'][swift_code]"> </div></div><a class="remove_button" href="javascript:void(0);"><img alt="close" src="'+ baseUrl +'assets/themes/default/images/small-red-cross.png"></a></div>';
	return fieldHTML;
	}
	
function getNextAirRow(n,baseUrl)
{
var fieldHTML = '<div class="dt_row"><div class="dt aiport"><input type="text" onfocus="searchLocation(this)" placeholder="Enter Airport Name" class="g-autofill" data-validation-optional="true" data-validation="alphanumeric" data-validation-allowing="-_@#:,./\() " value="" name="distance_from_airport['+n+'][name]"></div><div class="right-dd-box-bank-ifsc"><input type="text" placeholder="Distance" data-validation-help="Please enter in decimal eg 1.99 " data-validation-optional="true" data-validation="distance" value="" name="distance_from_airport['+n+'][distance]" class="has-help-txt"></div><a href="javascript:void(0);" class="remove_link" title="Remove fields row"><img src="'+ baseUrl +'assets/themes/default/images/small-red-cross.png" alt="close"></a><a href="javascript:void(0);" class="add_airdistance_row" title="Remove fields row"><img src="'+ baseUrl +'assets/themes/default/images/small-green-plus.png" alt="close"></div>';	
return fieldHTML;	
}	

function getNextCityRow(n,baseUrl)
{
var fieldHTML = '<div class="dt_row"><div class="dt aiport"><input type="text" onfocus="searchLocation(this)" class="g-autofill" placeholder="Enter City / Locality Name" data-validation-optional="true" data-validation="alphanumeric" data-validation-allowing="-_@#:,./\() " value="" name="distance_from_city['+n+'][name]"></div><div class="right-dd-box-bank-ifsc"><input type="text" placeholder="Distance" data-validation-help="Please enter in decimal eg 1.99 " data-validation-optional="true" data-validation="distance" value="" name="distance_from_city['+n+'][distance]" class="has-help-txt"></div><a href="javascript:void(0);" class="remove_link" title="Remove fields row"><img src="'+ baseUrl +'assets/themes/default/images/small-red-cross.png" alt="close"></a><a href="javascript:void(0);" class="add_ctydistance_row" title="Remove fields row"><img src="'+ baseUrl +'assets/themes/default/images/small-green-plus.png" alt="close"></a></div>';	
return fieldHTML;	
}								
function getNextContactRow(n,baseUrl)
	{
		/* get new contact row using ajax */		
		jQuery.ajax({
		type: "POST",
		url: baseUrl + "index.php/ajaxController/getContactLine",
		data: {row_num: n},
		success: function(res) {
		if (res)
		{
		$('.contactfield_wrapper').append(res); 
		$('.contactfield_wrapper select').selectpicker();
		callbackValidator();
		}
		else{
			return '';
			}
		}
	});
}

function setNextSessionRow(n,baseUrl,session,wraper)
{
	/* get new low sssion row using ajax */		
		jQuery.ajax({
		type: "POST",
		url: baseUrl + "index.php/ajaxController/getNextSessionRow",
		data: {row_num: n,session_type: session,},
		success: function(res) {
		if (res)
		{
		$('.'+wraper).append(res); 
		$('.'+wraper+' select').selectpicker();
		callbackValidator();
		}
		else{
			return '';
			}
		}
	});
	
}

function setNextPaymentPlanRow(n,baseUrl)
{
	
	/* get new low sssion row using ajax */		
		jQuery.ajax({
		type: "POST",
		url: baseUrl + "index.php/ajaxController/getNextPaymentPlanRow",
		data: {row_num: n},
		success: function(res) {
		if (res)
		{
		$('.payment_plan_wraper').append(res); 
		$('.payment_plan_wraper select').selectpicker();
		callbackValidator();
		}
		else{
			return '';
			}
		}
	});
	
}

function setNextRenovationRow(n,baseUrl)
	{
		/* get new contact row using ajax */		
		jQuery.ajax({
		type: "POST",
		url: baseUrl + "index.php/ajaxController/getRenovationLine",
		data: {row_num: n},
		success: function(res) {
		if (res)
		{
		$('.renovation_wraper').append(res); 
		$('.renovation_wraper select').selectpicker();		
		// date field add calander
		$('#renovation_shedule_from_'+n).datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });
	   $('#renovation_shedule_to_'+n).datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });
		
		var elIdFrom="#renovation_shedule_from_"+n;
		var elIdTo="#renovation_shedule_to_"+n;
		datetimepicker_from_to(elIdFrom,elIdTo)	;
		
		//$('#renovation_shedule_from_'+n).on("dp.change", function (e) {
       //  $('#renovation_shedule_to_'+n).data("DateTimePicker").minDate(e.date);
		//});
		callbackValidator();
		}
		else{
			return '';
			}
		}
	});
}

function setExcludedDateRow(n,baseUrl,today)
{
	var htmlFilds='<div class="exdd cf"><div class="exdd-box"><div class="form-group has-success"><div id="cmpli_exclude_date_frm_'+n+'" class="input-group date"><input type="text" placeholder="From" data-validation-help="dd/mm/yyyy" data-validation-format="dd/mm/yyyy" data-validation-optional="true" data-validation="date" value="" name="cmpli_exclude_date['+n+'][from]" class="has-help-txt"><span class="input-group-addon"><span class="fa fa-calendar"></span></span></div></div></div><div class="exdd-box"><div class="form-group"><div id="cmpli_exclude_date_to_'+n+'" class="input-group date"><input type="text" placeholder="To" data-validation-help="dd/mm/yyyy" data-validation-format="dd/mm/yyyy" data-validation-optional="true" data-validation="date" value="" name="cmpli_exclude_date['+n+'][to]" class="has-help-txt"> <span class="input-group-addon"><span class="fa fa-calendar"></span></span></div></div></div><a href="javascript:void(0)" class="remove_lnk"><img alt="close" src="'+baseUrl+'assets/themes/default/images/small-red-cross.png"></a></div>';
											
	$('.excluded-date-wraper').append(htmlFilds); 
		
		$('#cmpli_exclude_date_frm_'+n).datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });
	   $('#cmpli_exclude_date_to_'+n).datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });
		
		$('#cmpli_exclude_date_frm_'+n).on("dp.change", function (e) {
           $('#cmpli_exclude_date_to_'+n).data("DateTimePicker").minDate(e.date);
		});
}


$(document).ready(function() {
 if ( $( "#right-menu" ).length ) {
    $('#right-menu').sidr({
    name: 'sidr-right',
    side: 'right',
    onOpen : function(){
        $('.nav-custom').show();
    },
    onClose  : function(){
        $('.nav-custom').hide();
    },
    });
    }
    $('.nav-custom').click(function(){
       $.sidr('close', 'sidr-right');
    });

$('input[type="checkbox"]').checkbox({
    checkedClass: 'icon-check',
    uncheckedClass: 'icon-check-empty'
});

var slider_initialized = true;

    /* $(".nav3 li a").click(function(event) {
        event.preventDefault();
	    $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content2").not(tab).css("display", "none");
        $(tab).fadeIn();
		
		if(tab=='#tab-1')
		{
		$( "#nextcntnar" ).html('Next Facilities');	
		}
		
		if(tab=='#tab-2')
		{
			$( "#nextcntnar" ).html('Next Contract');
		}
		if(tab=='#tab-3')
		{
			$( "#nextcntnar" ).html('Next Banking');
		}
		if(tab=='#tab-4')
		{
			$( "#nextcntnar" ).html('Next Pricing');
		}
		if(tab=='#tab-5')
		{
			$( "#nextcntnar" ).html('Next Gallery');
		}
			
if(tab=='#tab-6')
		{
			$( "#nextcntnar" ).html('Hotel Info');
		}			
			  
    }); */
	

/*
flesh messages 
*/
       
var el = $('#alertmssage');

el.pulse(
  {
    backgroundColor : 'purple',
    color           : 'white'
  },
  {
    returnDelay : 1,
    interval    : 100,
    pulses      : 0
  }

); 

//el.toggle(); 

var eln = $('#alertnotice');
eln.pulse(
  {
    backgroundColor : 'red',
    color           : 'blue'
  },
  {
    returnDelay : 1,
    interval    : 100,
    pulses      : 0
  }
);   

var ele = $('#alerterror');
ele.pulse(
  {
    backgroundColor : 'red',
    color           : 'black'
  },
  {
    returnDelay : 1,
    interval    : 100,
    pulses      : 0
  }
); 

$('[data-toggle="tooltip"]').tooltip(); 
$('#hotel_chain_oth').hide();
$('#purpose_options_oth').hide();
$('#property_type_oth').hide();


if ( $( "#hotel_chain" ).length ) {
		 
		if($('#hotel_chain').val()=='Other')
		{
			$('#hotel_chain_oth').show();
		}

		$('#hotel_chain').change(function() {
			$('#hotel_chain_oth').next('.form-error').hide();
			var selected = $(this).val();
			if(selected == 'Other'){
			  $('#hotel_chain_oth').show();
			}
			else{
			  $('#hotel_chain_oth').hide();
			}
		});
		 
}

if ( $( "#purpose" ).length ) {
	
	if($('#purpose').val()=='Other')
		{
			$('#purpose_options_oth').show();
		}
		
$('#purpose').change(function() {
	$('#purpose_options_oth').next('.form-error').hide();
    var selected = $(this).val();
    if(selected == 'Other'){
      $('#purpose_options_oth').show();
    }
    else{
      $('#purpose_options_oth').hide();
    }
});
}

if ( $( "#property_type" ).length ) {
	
	if($('#property_type').val()=='Other')
		{
			$('#property_type_oth').show();	
		}
		
		$('#property_type').change(function() {
			$('#property_type_oth').next('.form-error').hide();
			var selectedOth=false;
			//var selected = $(this).find("option:selected").val();    
			$("#property_type :selected").map(function(i, el) {
			var selected=$(el).val();
				if(selected == 'Other'){
				selectedOth=true;
				}
				});
			if(selectedOth){
			  $('#property_type_oth').show();
			}
			else{
			  $('#property_type_oth').hide();
			}
			});
	}
	
$('.selectpicker').selectpicker();
$('.dataTables_length select').selectpicker();

});

function datetimepicker_from_to(elIdFrom,elIdTo)
{
		$(elIdFrom).datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });	
		$(elIdTo).datetimepicker({
               format: 'DD/MM/YYYY',
			   useCurrent:false,
			   allowInputToggle : true
        });	
	$(elIdFrom).on("dp.change", function (e) {
           $(elIdTo).data("DateTimePicker").minDate(e.date);
     });
	$(elIdFrom).data("DateTimePicker").minDate($(elIdTo).date);
	$(elIdTo).data("DateTimePicker").minDate($(elIdFrom).date);
	$(elIdTo).on("dp.change", function (e) {
    $(elIdFrom).data("DateTimePicker").maxDate(e.date);
     });
	
}
