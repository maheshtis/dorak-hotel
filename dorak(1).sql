-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 06, 2016 at 12:41 PM
-- Server version: 5.6.27-0ubuntu0.14.04.1
-- PHP Version: 5.6.16-2+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dorak`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE IF NOT EXISTS `access_levels` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `name`, `description`) VALUES
(1, 'high', 'High Level'),
(2, 'medium', 'medium'),
(3, 'low', 'low'),
(4, 'guest', 'guest');

-- --------------------------------------------------------

--
-- Table structure for table `access_levels_acl`
--

CREATE TABLE IF NOT EXISTS `access_levels_acl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_level_id` int(11) NOT NULL,
  `acl_sys_controller_id` int(11) NOT NULL,
  `acl_sys_method_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `access_level_id` (`access_level_id`),
  KEY `acl_sys_controller_id` (`acl_sys_controller_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=678 ;

--
-- Dumping data for table `access_levels_acl`
--

INSERT INTO `access_levels_acl` (`id`, `access_level_id`, `acl_sys_controller_id`, `acl_sys_method_id`) VALUES
(402, 2, 0, 11),
(403, 2, 0, 4),
(404, 2, 0, 13),
(405, 2, 0, 16),
(406, 2, 0, 17),
(407, 2, 0, 18),
(408, 2, 0, 10),
(409, 2, 0, 19),
(410, 2, 0, 3),
(411, 2, 0, 9),
(668, 3, 5, 12),
(669, 3, 5, 11),
(670, 3, 5, 4),
(671, 3, 5, 13),
(672, 4, 4, 16),
(673, 4, 4, 17),
(674, 4, 4, 18),
(675, 4, 3, 19),
(676, 4, 3, 3),
(677, 4, 3, 9);

-- --------------------------------------------------------

--
-- Table structure for table `acl_system_controllers`
--

CREATE TABLE IF NOT EXISTS `acl_system_controllers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(200) NOT NULL,
  `title` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `class_name` (`class_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `acl_system_controllers`
--

INSERT INTO `acl_system_controllers` (`id`, `class_name`, `title`) VALUES
(3, 'users', 'User'),
(4, 'settings', 'Settings'),
(5, 'hotels', 'Hotel');

-- --------------------------------------------------------

--
-- Table structure for table `acl_system_controller_methods`
--

CREATE TABLE IF NOT EXISTS `acl_system_controller_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acl_controller_id` int(11) NOT NULL,
  `class_method_name` varchar(300) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acl_controller_id` (`acl_controller_id`),
  KEY `class_method_name` (`class_method_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `acl_system_controller_methods`
--

INSERT INTO `acl_system_controller_methods` (`id`, `acl_controller_id`, `class_method_name`, `title`, `description`) VALUES
(3, 3, 'edit_user', 'edit', 'Edit user details'),
(4, 5, 'edit', 'edit', 'Edit hotels'),
(9, 3, 'delete_user', 'delete', 'delete user'),
(10, 3, 'users_list', 'view', 'View all user list'),
(11, 5, 'information', 'add', 'Hotel'),
(12, 5, 'index', 'view', 'hotel'),
(13, 5, 'delete_hotel', 'delete', 'Hotel'),
(15, 4, 'index', 'view', 'View all settings'),
(16, 4, 'index', 'add', NULL),
(17, 4, 'index', 'edit', 'Edit'),
(18, 4, 'index', 'delete', NULL),
(19, 3, 'create_user', 'add', 'Add user');

-- --------------------------------------------------------

--
-- Table structure for table `booking_info`
--

CREATE TABLE IF NOT EXISTS `booking_info` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `quotation_quote_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `booked_by_user_id` int(11) NOT NULL,
  `total_amount` decimal(7,2) NOT NULL,
  `booked_date` datetime NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by_user_id` int(11) NOT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `booking_info_details`
--

CREATE TABLE IF NOT EXISTS `booking_info_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `checkin_date` datetime NOT NULL,
  `checkout_date` datetime NOT NULL,
  `adult` int(11) NOT NULL,
  `children` int(11) NOT NULL,
  `city` varchar(200) NOT NULL,
  `total_amount` decimal(7,2) NOT NULL,
  `booking_date` datetime NOT NULL,
  `booked_by_user_id` int(11) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cancel_time_option`
--

CREATE TABLE IF NOT EXISTS `cancel_time_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_period` varchar(300) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `cancel_time_option`
--

INSERT INTO `cancel_time_option` (`id`, `time_period`) VALUES
(1, '1 month6365fgdfhgdh'),
(2, '2 months'),
(3, '3 months'),
(4, '4 months'),
(5, '5 months'),
(6, '6 months'),
(7, '>  6 months.'),
(8, '1 month');

-- --------------------------------------------------------

--
-- Table structure for table `child_age_ranges`
--

CREATE TABLE IF NOT EXISTS `child_age_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age_range` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `child_age_ranges`
--

INSERT INTO `child_age_ranges` (`id`, `age_range`) VALUES
(1, '2  to  11  years'),
(2, '2 to  12 years'),
(4, 'sdssd'),
(5, '2 to 11 years'),
(6, '23+ 25 years'),
(7, 'asd asd'),
(8, '2 to 15 years');

-- --------------------------------------------------------

--
-- Table structure for table `city_districts`
--

CREATE TABLE IF NOT EXISTS `city_districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `city_name` varchar(300) NOT NULL,
  `district_name` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `city_districts`
--

INSERT INTO `city_districts` (`id`, `country_id`, `city_id`, `city_name`, `district_name`) VALUES
(4, 115, 700, 'Abohar', 'district a'),
(5, 115, 700, 'Abohar', 'District b'),
(10, 115, 306, 'Ahmadabad', 'alman'),
(11, 115, 306, 'Ahmadabad', 'zvsdvsdv'),
(12, 283, 4014, 'Delhi', 'Delhi'),
(13, 241, 4001, 'Istanbul', 'Abc'),
(14, 241, 4001, 'Istanbul', 'bcd'),
(15, 5, 4013, 'sdfsf', 'ALAERI-DIS');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(500) NOT NULL,
  `address` text NOT NULL,
  `country_code` varchar(10) NOT NULL,
  `city` varchar(200) NOT NULL,
  `head` varchar(500) NOT NULL,
  `company_logo` varchar(200) DEFAULT NULL,
  `is_primary_company` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 for no 1 for yes',
  `primary_company_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 for disable 1 for enable',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `address`, `country_code`, `city`, `head`, `company_logo`, `is_primary_company`, `primary_company_id`, `status`, `last_updated`) VALUES
(1, 'tis india', 'c 1c sec 8', 'IND', 'DE', 'FF', NULL, 1, 0, 1, '2016-02-24 06:30:24'),
(3, 'Tisco sd', 'fdsa fadsfas fasfa', 'TR', 'Ankara', 'fasa', '', 0, 0, 0, '2016-02-24 06:53:33'),
(4, 'asfsfggsg', 'dgas sdgasg gsdgg', 'TR', 'Ankara', 'fsdgs', '', 1, 0, 1, '2016-02-24 06:57:35'),
(5, 'hgfjfgjh', 'jdgf hgffjfg jhgrfjfj', 'aaayy', 'Ankara', 'djg', '', 0, 4, 1, '2016-02-24 07:03:07'),
(7, 'fafasfasf fgagg gasgagag', ' gfdasgsg agag afgag', 'TR', 'Ankara', '', '', 1, 0, 1, '2016-02-24 07:12:24'),
(8, 'afa fasfa ', ' afvaf gsadg gsdgs sgdsgsg', 'TR', 'Bursa', 'sfdffg', 'images_(1).png', 0, 4, 1, '2016-02-25 12:14:41'),
(9, 'Tisco asdf', 'c81c sec 7 noida', 'IN', 'Ankara', '', 'plan2_3d.jpg', 0, 1, 1, '2016-02-24 09:43:55'),
(10, 'asdefgh dsd', 'dfs fasda fasfa', 'IN', 'Ankara', 'afa', '', 1, 0, 1, '2016-02-26 07:05:38'),
(11, 'afa fasfa sdfaf', 'fasdaf dafa faf fafaf', 'TR', 'Ankara', 'asfa', '', 0, 10, 1, '2016-02-26 08:08:39');

-- --------------------------------------------------------

--
-- Table structure for table `company_markets`
--

CREATE TABLE IF NOT EXISTS `company_markets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `market_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `company_markets`
--

INSERT INTO `company_markets` (`id`, `company_id`, `market_id`) VALUES
(12, 10, 7),
(13, 10, 6),
(14, 8, 7),
(15, 8, 6),
(16, 8, 8),
(17, 11, 7);

-- --------------------------------------------------------

--
-- Table structure for table `complimentary_services`
--

CREATE TABLE IF NOT EXISTS `complimentary_services` (
  `cmpl_service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(500) NOT NULL,
  PRIMARY KEY (`cmpl_service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `complimentary_services`
--

INSERT INTO `complimentary_services` (`cmpl_service_id`, `service_name`) VALUES
(1, 'Club Living Room'),
(2, 'Luggage storage'),
(3, 'Coffee and tea'),
(4, 'Fitness Room'),
(5, 'Complimentary use of iPads'),
(6, 'Universal charging station'),
(7, 'jj ja'),
(8, 'maa ta'),
(11, 'tamta'),
(12, 'kghhg');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(5) NOT NULL,
  `country_name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_name` (`country_name`),
  KEY `country_code` (`country_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=284 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_code`, `country_name`) VALUES
(1, 'AW', 'Aruba'),
(2, 'AG', 'Antigua and Barbuda'),
(5, 'DZ', 'Algeria'),
(6, 'AZ', 'Azerbaijan'),
(8, 'AM', 'Armenia'),
(10, 'AO', 'Angola'),
(12, 'AR', 'Argentina'),
(13, 'AU', 'Australia'),
(14, 'Ashmo', 'Ashmore and Cartier Islands'),
(15, 'AT', 'Austria'),
(16, 'AI', 'Anguilla'),
(17, 'AX', 'Åland Islands'),
(18, 'AQ', 'Antarctica'),
(19, 'BH', 'Bahrain'),
(20, 'BB', 'Barbados'),
(21, 'BW', 'Botswana'),
(22, 'BM', 'Bermuda'),
(23, 'BE', 'Belgium'),
(24, 'BS', 'Bahamas, The'),
(25, 'BD', 'Bangladesh'),
(26, 'BZ', 'Belize'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BO', 'Bolivia'),
(29, 'MM', 'Myanmar'),
(30, 'BJ', 'Benin'),
(31, 'BY', 'Belarus'),
(32, 'SB', 'Solomon Islands'),
(33, 'Navas', 'Navassa Island'),
(34, 'BR', 'Brazil'),
(35, 'sda', 'Bassas da India'),
(36, 'BT', 'Bhutan'),
(37, 'BG', 'Bulgaria'),
(38, 'BV', 'Bouvet Island'),
(39, 'BN', 'Brunei'),
(40, 'BI', 'Burundi'),
(41, 'CA', 'Canada'),
(42, 'KH', 'Cambodia'),
(43, 'TD', 'Chad'),
(44, 'LK', 'Sri Lanka'),
(45, 'CG', 'Congo, Republic of the'),
(46, 'CD', 'Congo, Democratic Republic of the'),
(47, 'CN', 'China'),
(48, 'CL', 'Chile'),
(49, 'KY', 'Cayman Islands'),
(50, 'CC', 'Cocos (Keeling) Islands'),
(51, 'CM', 'Cameroon'),
(52, 'KM', 'Comoros'),
(53, 'CO', 'Colombia'),
(54, 'MP', 'Northern Mariana Islands'),
(55, 'Coral', 'Coral Sea Islands'),
(56, 'CR', 'Costa Rica'),
(57, 'CF', 'Central African Republic'),
(58, 'CU', 'Cuba'),
(59, 'CV', 'Cape Verde'),
(60, 'CK', 'Cook Islands'),
(61, 'CY', 'Cyprus'),
(62, 'DK', 'Denmark'),
(63, 'DJ', 'Djibouti'),
(64, 'DM', 'Dominica'),
(65, 'UM', 'Jarvis Island'),
(66, 'DO', 'Dominican Republic'),
(67, 'Dheke', 'Dhekelia Sovereign Base Area'),
(68, 'EC', 'Ecuador'),
(69, 'EG', 'Egypt'),
(70, 'IE', 'Ireland'),
(71, 'GQ', 'Equatorial Guinea'),
(72, 'EE', 'Estonia'),
(73, 'ER', 'Eritrea'),
(74, 'SV', 'El Salvador'),
(75, 'ET', 'Ethiopia'),
(76, 'Europ', 'Europa Island'),
(77, 'CZ', 'Czech Republic'),
(78, 'GF', 'French Guiana'),
(79, 'FI', 'Finland'),
(80, 'FJ', 'Fiji'),
(81, 'FK', 'Falkland Islands (Islas Malvinas)'),
(82, 'FM', 'Micronesia, Federated States of'),
(83, 'FO', 'Faroe Islands'),
(84, 'PF', 'French Polynesia'),
(85, 'UM', 'Baker Island'),
(86, 'FR', 'France'),
(87, 'TF', 'French Southern and Antarctic Lands'),
(88, 'GM', 'Gambia, The'),
(89, 'GA', 'Gabon'),
(90, 'GE', 'Georgia'),
(91, 'GH', 'Ghana'),
(92, 'GI', 'Gibraltar'),
(93, 'GD', 'Grenada'),
(94, 'GU', 'Guernsey'),
(95, 'GL', 'Greenland'),
(96, 'DE', 'Germany'),
(97, 'Glori', 'Glorioso Islands'),
(98, 'GP', 'Guadeloupe'),
(99, 'GU', 'Guam'),
(100, 'GR', 'Greece'),
(101, 'GT', 'Guatemala'),
(102, 'GN', 'Guinea'),
(103, 'GY', 'Guyana'),
(105, 'HT', 'Haiti'),
(106, 'HK', 'Hong Kong'),
(107, 'HM', 'Heard Island and McDonald Islands'),
(108, 'HN', 'Honduras'),
(109, 'UM', 'Howland Island'),
(110, 'HR', 'Croatia'),
(111, 'HU', 'Hungary'),
(112, 'IS', 'Iceland'),
(113, 'ID', 'Indonesia'),
(115, 'IN', 'India'),
(116, 'IO', 'British Indian Ocean Territory'),
(118, 'IR', 'Iran'),
(119, 'IL', 'Israel'),
(120, 'IT', 'Italy'),
(121, 'CI', 'Cote d''Ivoire'),
(122, 'IQ', 'Iraq'),
(123, 'JP', 'Japan'),
(124, 'JE', 'Jersey'),
(125, 'JM', 'Jamaica'),
(126, 'SJ', 'Jan Mayen'),
(127, 'JO', 'Jordan'),
(128, 'UM', 'Johnston Atoll'),
(130, 'KE', 'Kenya'),
(131, 'KG', 'Kyrgyzstan'),
(132, 'KP', 'Korea, North'),
(133, 'UM', 'Kingman Reef'),
(134, 'KI', 'Kiribati'),
(135, 'KR', 'Korea, South'),
(136, 'CX', 'Christmas Island'),
(137, 'KW', 'Kuwait'),
(138, 'KV', 'Kosovo'),
(139, 'KZ', 'Kazakhstan'),
(140, 'LA', 'Laos'),
(141, 'LB', 'Lebanon'),
(142, 'LV', 'Latvia'),
(143, 'LT', 'Lithuania'),
(144, 'LR', 'Liberia'),
(145, 'SK', 'Slovakia'),
(146, 'UM', 'Palmyra Atoll'),
(147, 'LI', 'Liechtenstein'),
(148, 'LS', 'Lesotho'),
(149, 'LU', 'Luxembourg'),
(150, 'LY', 'Libyan Arab'),
(151, 'MG', 'Madagascar'),
(152, 'MQ', 'Martinique'),
(153, 'MO', 'Macau'),
(154, 'MD', 'Moldova, Republic of'),
(155, 'YT', 'Mayotte'),
(156, 'MN', 'Mongolia'),
(157, 'MS', 'Montserrat'),
(158, 'MW', 'Malawi'),
(159, 'ME', 'Montenegro'),
(161, 'ML', 'Mali'),
(162, 'MC', 'Monaco'),
(163, 'MA', 'Morocco'),
(164, 'MU', 'Mauritius'),
(165, 'UM', 'Midway Islands'),
(166, 'MR', 'Mauritania'),
(167, 'MT', 'Malta'),
(168, 'OM', 'Oman'),
(169, 'MV', 'Maldives'),
(170, 'MX', 'Mexico'),
(171, 'MY', 'Malaysia'),
(172, 'MZ', 'Mozambique'),
(173, 'NC', 'New Caledonia'),
(174, 'NU', 'Niue'),
(175, 'NF', 'Norfolk Island'),
(176, 'NE', 'Niger'),
(177, 'VU', 'Vanuatu'),
(178, 'NG', 'Nigeria'),
(179, 'NL', 'Netherlands'),
(181, 'NO', 'Norway'),
(182, 'NP', 'Nepal'),
(183, 'NR', 'Nauru'),
(184, 'SR', 'Suriname'),
(185, 'AN', 'Netherlands Antilles'),
(186, 'NI', 'Nicaragua'),
(187, 'NZ', 'New Zealand'),
(188, 'PY', 'Paraguay'),
(189, 'PN', 'Pitcairn Islands'),
(190, 'PE', 'Peru'),
(193, 'PK', 'Pakistan'),
(194, 'PL', 'Poland'),
(195, 'PA', 'Panama'),
(196, 'PT', 'Portugal'),
(197, 'PG', 'Papua New Guinea'),
(198, 'PW', 'Palau'),
(199, 'GW', 'Guinea-Bissau'),
(200, 'QA', 'Qatar'),
(201, 'RE', 'Reunion'),
(202, 'RS', 'Serbia'),
(203, 'MH', 'Marshall Islands'),
(204, 'MF', 'Saint Martin'),
(205, 'RO', 'Romania'),
(206, 'PH', 'Philippines'),
(207, 'PR', 'Puerto Rico'),
(208, 'RU', 'Russia'),
(209, 'RW', 'Rwanda'),
(210, 'SA', 'Saudi Arabia'),
(211, 'PM', 'Saint Pierre and Miquelon'),
(212, 'KN', 'Saint Kitts and Nevis'),
(213, 'SC', 'Seychelles'),
(214, 'ZA', 'South Africa'),
(215, 'SN', 'Senegal'),
(216, 'SH', 'Saint Helena'),
(217, 'SI', 'Slovenia'),
(218, 'SL', 'Sierra Leone'),
(219, 'SM', 'San Marino'),
(220, 'SG', 'Singapore'),
(221, 'SO', 'Somalia'),
(222, 'ES', 'Spain'),
(223, 'LC', 'Saint Lucia'),
(224, 'SD', 'Sudan'),
(225, 'SJ', 'Svalbard'),
(226, 'SE', 'Sweden'),
(227, 'GS', 'South Georgia and the Islands'),
(228, 'SY', 'Syrian Arab Republic'),
(229, 'CH', 'Switzerland'),
(230, 'TT', 'Trinidad and Tobago'),
(232, 'TH', 'Thailand'),
(233, 'TJ', 'Tajikistan'),
(234, 'TC', 'Turks and Caicos Islands'),
(235, 'TK', 'Tokelau'),
(236, 'TO', 'Tonga'),
(237, 'TG', 'Togo'),
(238, 'ST', 'Sao Tome and Principe'),
(239, 'TN', 'Tunisia'),
(240, 'TL', 'East Timor'),
(241, 'TR', 'Turkey'),
(242, 'TV', 'Tuvalu'),
(243, 'TW', 'Taiwan'),
(244, 'TM', 'Turkmenistan'),
(245, 'TZ', 'Tanzania, United Republic of'),
(246, 'UG', 'Uganda'),
(247, 'GB', 'United Kingdom'),
(248, 'UA', 'Ukraine'),
(249, 'US', 'United States'),
(250, 'BF', 'Burkina Faso'),
(251, 'UY', 'Uruguay'),
(252, 'UZ', 'Uzbekistan'),
(253, 'VC', 'Saint Vincent and the Grenadines'),
(254, 'VE', 'Venezuela'),
(255, 'VG', 'British Virgin Islands'),
(256, 'VN', 'Vietnam'),
(257, 'VI', 'Virgin Islands (US)'),
(258, 'VA', 'Holy See (Vatican City)'),
(259, 'NA', 'Namibia'),
(261, 'WF', 'Wallis and Futuna'),
(262, 'EH', 'Western Sahara'),
(263, 'UM', 'Wake Island'),
(264, 'WS', 'Samoa'),
(265, 'SZ', 'Swaziland'),
(266, 'CS', 'Serbia and Montenegro'),
(267, 'YE', 'Yemen'),
(268, 'ZM', 'Zambia'),
(269, 'ZW', 'Zimbabwe'),
(270, 'AD', 'Andorra'),
(283, 'IND', 'IND');

-- --------------------------------------------------------

--
-- Table structure for table `country_cities`
--

CREATE TABLE IF NOT EXISTS `country_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `country_code` varchar(50) NOT NULL,
  `city_code` varchar(500) DEFAULT NULL,
  `city_name` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `city_name` (`city_name`),
  KEY `country_id` (`country_id`),
  KEY `country_code` (`country_code`),
  KEY `city_name_2` (`city_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4016 ;

--
-- Dumping data for table `country_cities`
--

INSERT INTO `country_cities` (`id`, `country_id`, `country_code`, `city_code`, `city_name`) VALUES
(1, 115, 'IN', NULL, 'Port Blair'),
(2, 115, 'IN', NULL, 'Adilabad'),
(3, 115, 'IN', NULL, 'Adoni'),
(4, 115, 'IN', NULL, 'Alwal'),
(5, 115, 'IN', NULL, 'Anakapalle'),
(6, 115, 'IN', NULL, 'Anantapur'),
(7, 115, 'IN', NULL, 'Bapatla'),
(8, 115, 'IN', NULL, 'Belampalli'),
(9, 115, 'IN', NULL, 'Bhimavaram'),
(10, 115, 'IN', NULL, 'Bhongir'),
(11, 115, 'IN', NULL, 'Bobbili'),
(12, 115, 'IN', NULL, 'Bodhan'),
(13, 115, 'IN', NULL, 'Chilakalurupet'),
(14, 115, 'IN', NULL, 'Chinna Chawk'),
(15, 115, 'IN', NULL, 'Chirala'),
(16, 115, 'IN', NULL, 'Chittur'),
(17, 115, 'IN', NULL, 'Cuddapah'),
(18, 115, 'IN', NULL, 'Dharmavaram'),
(19, 115, 'IN', NULL, 'Dhone'),
(20, 115, 'IN', NULL, 'Eluru'),
(21, 115, 'IN', NULL, 'Gaddiannaram'),
(22, 115, 'IN', NULL, 'Gadwal'),
(23, 115, 'IN', NULL, 'Gajuwaka'),
(24, 115, 'IN', NULL, 'Gudivada'),
(25, 115, 'IN', NULL, 'Gudur'),
(26, 115, 'IN', NULL, 'Guntakal'),
(27, 115, 'IN', NULL, 'Guntur'),
(28, 115, 'IN', NULL, 'Hindupur'),
(29, 115, 'IN', NULL, 'Hyderabad'),
(30, 115, 'IN', NULL, 'Jagtial'),
(31, 115, 'IN', NULL, 'Kadiri'),
(32, 115, 'IN', NULL, 'Kagaznagar'),
(33, 115, 'IN', NULL, 'Kakinada'),
(34, 115, 'IN', NULL, 'Kallur'),
(35, 115, 'IN', NULL, 'Kamareddi'),
(36, 115, 'IN', NULL, 'Kapra'),
(37, 115, 'IN', NULL, 'Karimnagar'),
(38, 115, 'IN', NULL, 'Karnul'),
(39, 115, 'IN', NULL, 'Kavali'),
(40, 115, 'IN', NULL, 'Khammam'),
(41, 115, 'IN', NULL, 'Kodar'),
(42, 115, 'IN', NULL, 'Kondukur'),
(43, 115, 'IN', NULL, 'Koratla'),
(44, 115, 'IN', NULL, 'Kottagudem'),
(45, 115, 'IN', NULL, 'Kukatpalle'),
(46, 115, 'IN', NULL, 'Lalbahadur Nagar'),
(47, 115, 'IN', NULL, 'Machilipatnam'),
(48, 115, 'IN', NULL, 'Mahbubnagar'),
(49, 115, 'IN', NULL, 'Malkajgiri'),
(50, 115, 'IN', NULL, 'Mancheral'),
(51, 115, 'IN', NULL, 'Mandamarri'),
(52, 115, 'IN', NULL, 'Mangalagiri'),
(53, 115, 'IN', NULL, 'Markapur'),
(54, 115, 'IN', NULL, 'Miryalaguda'),
(55, 115, 'IN', NULL, 'Nalgonda'),
(56, 115, 'IN', NULL, 'Nandyal'),
(57, 115, 'IN', NULL, 'Narasapur'),
(58, 115, 'IN', NULL, 'Narasaraopet'),
(59, 115, 'IN', NULL, 'Nellur'),
(60, 115, 'IN', NULL, 'Nirmal'),
(61, 115, 'IN', NULL, 'Nizamabad'),
(62, 115, 'IN', NULL, 'Nuzvid'),
(63, 115, 'IN', NULL, 'Ongole'),
(64, 115, 'IN', NULL, 'Palakollu'),
(65, 115, 'IN', NULL, 'Palasa'),
(66, 115, 'IN', NULL, 'Palwancha'),
(67, 115, 'IN', NULL, 'Patancheru'),
(68, 115, 'IN', NULL, 'Piduguralla'),
(69, 115, 'IN', NULL, 'Ponnur'),
(70, 115, 'IN', NULL, 'Proddatur'),
(71, 115, 'IN', NULL, 'Qutubullapur'),
(72, 115, 'IN', NULL, 'Rajamahendri'),
(73, 115, 'IN', NULL, 'Rajampet'),
(74, 115, 'IN', NULL, 'Rajendranagar'),
(75, 115, 'IN', NULL, 'Ramachandrapuram'),
(76, 115, 'IN', NULL, 'Ramagundam'),
(77, 115, 'IN', NULL, 'Rayachoti'),
(78, 115, 'IN', NULL, 'Rayadrug'),
(79, 115, 'IN', NULL, 'Samalkot'),
(80, 115, 'IN', NULL, 'Sangareddi'),
(81, 115, 'IN', NULL, 'Sattenapalle'),
(82, 115, 'IN', NULL, 'Serilungampalle'),
(83, 115, 'IN', NULL, 'Siddipet'),
(84, 115, 'IN', NULL, 'Sikandarabad'),
(85, 115, 'IN', NULL, 'Sirsilla'),
(86, 115, 'IN', NULL, 'Srikakulam'),
(87, 115, 'IN', NULL, 'Srikalahasti'),
(88, 115, 'IN', NULL, 'Suriapet'),
(89, 115, 'IN', NULL, 'Tadepalle'),
(90, 115, 'IN', NULL, 'Tadepallegudem'),
(91, 115, 'IN', NULL, 'Tadpatri'),
(92, 115, 'IN', NULL, 'Tandur'),
(93, 115, 'IN', NULL, 'Tanuku'),
(94, 115, 'IN', NULL, 'Tenali'),
(95, 115, 'IN', NULL, 'Tirupati'),
(96, 115, 'IN', NULL, 'Tuni'),
(97, 115, 'IN', NULL, 'Uppal Kalan'),
(98, 115, 'IN', NULL, 'Vijayawada'),
(99, 115, 'IN', NULL, 'Vinukonda'),
(100, 115, 'IN', NULL, 'Visakhapatnam'),
(101, 115, 'IN', NULL, 'Vizianagaram'),
(102, 115, 'IN', NULL, 'Vuyyuru'),
(103, 115, 'IN', NULL, 'Wanparti'),
(104, 115, 'IN', NULL, 'Warangal'),
(105, 115, 'IN', NULL, 'Yemmiganur'),
(106, 115, 'IN', NULL, 'Itanagar'),
(107, 115, 'IN', NULL, 'Barpeta'),
(108, 115, 'IN', NULL, 'Bongaigaon'),
(109, 115, 'IN', NULL, 'Dhuburi'),
(110, 115, 'IN', NULL, 'Dibrugarh'),
(111, 115, 'IN', NULL, 'Diphu'),
(112, 115, 'IN', NULL, 'Guwahati'),
(113, 115, 'IN', NULL, 'Jorhat'),
(114, 115, 'IN', NULL, 'Karimganj'),
(115, 115, 'IN', NULL, 'Lakhimpur'),
(116, 115, 'IN', NULL, 'Lanka'),
(117, 115, 'IN', NULL, 'Nagaon'),
(118, 115, 'IN', NULL, 'Sibsagar'),
(119, 115, 'IN', NULL, 'Silchar'),
(120, 115, 'IN', NULL, 'Tezpur'),
(121, 115, 'IN', NULL, 'Tinsukia'),
(122, 115, 'IN', NULL, 'Alipur Duar'),
(123, 115, 'IN', NULL, 'Arambagh'),
(124, 115, 'IN', NULL, 'Asansol'),
(125, 115, 'IN', NULL, 'Ashoknagar Kalyangarh'),
(126, 115, 'IN', NULL, 'Baharampur'),
(127, 115, 'IN', NULL, 'Baidyabati'),
(128, 115, 'IN', NULL, 'Baj Baj'),
(129, 115, 'IN', NULL, 'Bally'),
(130, 115, 'IN', NULL, 'Bally Cantonment'),
(131, 115, 'IN', NULL, 'Balurghat'),
(132, 115, 'IN', NULL, 'Bangaon'),
(133, 115, 'IN', NULL, 'Bankra'),
(134, 115, 'IN', NULL, 'Bankura'),
(135, 115, 'IN', NULL, 'Bansbaria'),
(136, 115, 'IN', NULL, 'Baranagar'),
(137, 115, 'IN', NULL, 'Barddhaman'),
(138, 115, 'IN', NULL, 'Basirhat'),
(139, 115, 'IN', NULL, 'Bhadreswar'),
(140, 115, 'IN', NULL, 'Bhatpara'),
(141, 115, 'IN', NULL, 'Bidhannagar'),
(142, 115, 'IN', NULL, 'Binnaguri'),
(143, 115, 'IN', NULL, 'Bishnupur'),
(144, 115, 'IN', NULL, 'Bolpur'),
(145, 115, 'IN', NULL, 'Calcutta'),
(146, 115, 'IN', NULL, 'Chakdaha'),
(147, 115, 'IN', NULL, 'Champdani'),
(148, 115, 'IN', NULL, 'Chandannagar'),
(149, 115, 'IN', NULL, 'Contai'),
(150, 115, 'IN', NULL, 'Dabgram'),
(151, 115, 'IN', NULL, 'Darjiling'),
(152, 115, 'IN', NULL, 'Dhulian'),
(153, 115, 'IN', NULL, 'Dinhata'),
(154, 115, 'IN', NULL, 'Dum Dum'),
(155, 115, 'IN', NULL, 'Durgapur'),
(156, 115, 'IN', NULL, 'Gangarampur'),
(157, 115, 'IN', NULL, 'Garulia'),
(158, 115, 'IN', NULL, 'Gayespur'),
(159, 115, 'IN', NULL, 'Ghatal'),
(160, 115, 'IN', NULL, 'Gopalpur'),
(161, 115, 'IN', NULL, 'Habra'),
(162, 115, 'IN', NULL, 'Halisahar'),
(163, 115, 'IN', NULL, 'Haora'),
(164, 115, 'IN', NULL, 'HugliChunchura'),
(165, 115, 'IN', NULL, 'Ingraj Bazar'),
(166, 115, 'IN', NULL, 'Islampur'),
(167, 115, 'IN', NULL, 'Jalpaiguri'),
(168, 115, 'IN', NULL, 'Jamuria'),
(169, 115, 'IN', NULL, 'Jangipur'),
(170, 115, 'IN', NULL, 'Jhargram'),
(171, 115, 'IN', NULL, 'Kaliyaganj'),
(172, 115, 'IN', NULL, 'Kalna'),
(173, 115, 'IN', NULL, 'Kalyani'),
(174, 115, 'IN', NULL, 'Kamarhati'),
(175, 115, 'IN', NULL, 'Kanchrapara'),
(176, 115, 'IN', NULL, 'Kandi'),
(177, 115, 'IN', NULL, 'Karsiyang'),
(178, 115, 'IN', NULL, 'Katwa'),
(179, 115, 'IN', NULL, 'Kharagpur'),
(180, 115, 'IN', NULL, 'Kharagpur Railway Settlement'),
(181, 115, 'IN', NULL, 'Khardaha'),
(182, 115, 'IN', NULL, 'Kharia'),
(183, 115, 'IN', NULL, 'Koch Bihar'),
(184, 115, 'IN', NULL, 'Konnagar'),
(185, 115, 'IN', NULL, 'Krishnanagar'),
(186, 115, 'IN', NULL, 'Kulti'),
(187, 115, 'IN', NULL, 'Madhyamgram'),
(188, 115, 'IN', NULL, 'Maheshtala'),
(189, 115, 'IN', NULL, 'Memari'),
(190, 115, 'IN', NULL, 'Midnapur'),
(191, 115, 'IN', NULL, 'Naihati'),
(192, 115, 'IN', NULL, 'Navadwip'),
(193, 115, 'IN', NULL, 'Ni Barakpur'),
(194, 115, 'IN', NULL, 'North Barakpur'),
(195, 115, 'IN', NULL, 'North Dum Dum'),
(196, 115, 'IN', NULL, 'Old Maldah'),
(197, 115, 'IN', NULL, 'Panihati'),
(198, 115, 'IN', NULL, 'Phulia'),
(199, 115, 'IN', NULL, 'Pujali'),
(200, 115, 'IN', NULL, 'Puruliya'),
(201, 115, 'IN', NULL, 'Raiganj'),
(202, 115, 'IN', NULL, 'Rajpur'),
(203, 115, 'IN', NULL, 'Rampur Hat'),
(204, 115, 'IN', NULL, 'Ranaghat'),
(205, 115, 'IN', NULL, 'Raniganj'),
(206, 115, 'IN', NULL, 'Rishra'),
(207, 115, 'IN', NULL, 'Shantipur'),
(208, 115, 'IN', NULL, 'Shiliguri'),
(209, 115, 'IN', NULL, 'Shrirampur'),
(210, 115, 'IN', NULL, 'Siuri'),
(211, 115, 'IN', NULL, 'South Dum Dum'),
(212, 115, 'IN', NULL, 'Titagarh'),
(213, 115, 'IN', NULL, 'Ulubaria'),
(214, 115, 'IN', NULL, 'UttarparaKotrung'),
(215, 115, 'IN', NULL, 'Araria'),
(216, 115, 'IN', NULL, 'Arrah'),
(217, 115, 'IN', NULL, 'Aurangabad'),
(218, 115, 'IN', NULL, 'Bagaha'),
(219, 115, 'IN', NULL, 'Begusarai'),
(220, 115, 'IN', NULL, 'Bettiah'),
(221, 115, 'IN', NULL, 'Bhabua'),
(222, 115, 'IN', NULL, 'Bhagalpur'),
(223, 115, 'IN', NULL, 'Bihar'),
(224, 115, 'IN', NULL, 'Buxar'),
(225, 115, 'IN', NULL, 'Chhapra'),
(226, 115, 'IN', NULL, 'Darbhanga'),
(227, 115, 'IN', NULL, 'Dehri'),
(228, 115, 'IN', NULL, 'DighaMainpura'),
(229, 115, 'IN', NULL, 'Dinapur'),
(230, 115, 'IN', NULL, 'Dumraon'),
(231, 115, 'IN', NULL, 'Gaya'),
(232, 115, 'IN', NULL, 'Gopalganj'),
(233, 115, 'IN', NULL, 'Goura'),
(234, 115, 'IN', NULL, 'Hajipur'),
(235, 115, 'IN', NULL, 'Jahanabad'),
(236, 115, 'IN', NULL, 'Jamalpur'),
(237, 115, 'IN', NULL, 'Jamui'),
(238, 115, 'IN', NULL, 'Katihar'),
(239, 115, 'IN', NULL, 'Khagaria'),
(240, 115, 'IN', NULL, 'Khagaul'),
(241, 115, 'IN', NULL, 'Kishanganj'),
(242, 115, 'IN', NULL, 'Lakhisarai'),
(243, 115, 'IN', NULL, 'Madhipura'),
(244, 115, 'IN', NULL, 'Madhubani'),
(245, 115, 'IN', NULL, 'Masaurhi'),
(246, 115, 'IN', NULL, 'Mokama'),
(247, 115, 'IN', NULL, 'Motihari'),
(248, 115, 'IN', NULL, 'Munger'),
(249, 115, 'IN', NULL, 'Muzaffarpur'),
(250, 115, 'IN', NULL, 'Nawada'),
(251, 115, 'IN', NULL, 'Patna'),
(252, 115, 'IN', NULL, 'Phulwari'),
(253, 115, 'IN', NULL, 'Purnia'),
(254, 115, 'IN', NULL, 'Raxaul'),
(255, 115, 'IN', NULL, 'Saharsa'),
(256, 115, 'IN', NULL, 'Samastipur'),
(257, 115, 'IN', NULL, 'Sasaram'),
(258, 115, 'IN', NULL, 'Sitamarhi'),
(259, 115, 'IN', NULL, 'Siwan'),
(260, 115, 'IN', NULL, 'Supaul'),
(261, 115, 'IN', NULL, 'Chandigarh'),
(262, 115, 'IN', NULL, 'Ambikapur'),
(263, 115, 'IN', NULL, 'Bhilai'),
(264, 115, 'IN', NULL, 'Bilaspur'),
(265, 115, 'IN', NULL, 'Charoda'),
(266, 115, 'IN', NULL, 'Chirmiri'),
(267, 115, 'IN', NULL, 'Dhamtari'),
(268, 115, 'IN', NULL, 'Durg'),
(269, 115, 'IN', NULL, 'Jagdalpur'),
(270, 115, 'IN', NULL, 'Korba'),
(271, 115, 'IN', NULL, 'Raigarh'),
(272, 115, 'IN', NULL, 'Raipur'),
(273, 115, 'IN', NULL, 'Rajnandgaon'),
(274, 115, 'IN', NULL, 'Bhalswa Jahangirpur'),
(275, 115, 'IN', NULL, 'Burari'),
(276, 115, 'IN', NULL, 'Chilla Saroda Bangar'),
(277, 115, 'IN', NULL, 'Dallo Pura'),
(278, 115, 'IN', 'DL', 'Delhi'),
(279, 115, 'IN', NULL, 'Deoli'),
(280, 115, 'IN', NULL, 'Dilli Cantonment'),
(281, 115, 'IN', NULL, 'Gharoli'),
(282, 115, 'IN', NULL, 'Gokalpur'),
(283, 115, 'IN', NULL, 'Hastsal'),
(284, 115, 'IN', NULL, 'Jaffrabad'),
(285, 115, 'IN', NULL, 'Karawal Nagar'),
(286, 115, 'IN', NULL, 'Khajuri Khas'),
(287, 115, 'IN', NULL, 'Kirari Suleman Nagar'),
(288, 115, 'IN', NULL, 'Mandoli'),
(289, 115, 'IN', NULL, 'Mithe Pur'),
(290, 115, 'IN', NULL, 'Molarband'),
(291, 115, 'IN', NULL, 'Mundka'),
(292, 115, 'IN', NULL, 'Mustafabad'),
(293, 115, 'IN', NULL, 'Nangloi Jat'),
(294, 115, 'IN', NULL, 'Ni Dilli'),
(295, 115, 'IN', NULL, 'Pul Pehlad'),
(296, 115, 'IN', NULL, 'Puth Kalan'),
(297, 115, 'IN', NULL, 'Roshan Pura'),
(298, 115, 'IN', NULL, 'Sadat Pur Gujran'),
(299, 115, 'IN', NULL, 'Sultanpur Majra'),
(300, 115, 'IN', NULL, 'Tajpul'),
(301, 115, 'IN', NULL, 'Tigri'),
(302, 115, 'IN', NULL, 'Ziauddin Pur'),
(303, 115, 'IN', NULL, 'Madgaon'),
(304, 115, 'IN', NULL, 'Mormugao'),
(305, 115, 'IN', NULL, 'Panaji'),
(306, 115, 'IN', NULL, 'Ahmadabad'),
(307, 115, 'IN', NULL, 'Amreli'),
(308, 115, 'IN', NULL, 'Anand'),
(309, 115, 'IN', NULL, 'Anjar'),
(310, 115, 'IN', NULL, 'Bardoli'),
(311, 115, 'IN', NULL, 'Bharuch'),
(312, 115, 'IN', NULL, 'Bhavnagar'),
(313, 115, 'IN', NULL, 'Bhuj'),
(314, 115, 'IN', NULL, 'Borsad'),
(315, 115, 'IN', NULL, 'Botad'),
(316, 115, 'IN', NULL, 'Chandkheda'),
(317, 115, 'IN', NULL, 'Chandlodiya'),
(318, 115, 'IN', NULL, 'Dabhoi'),
(319, 115, 'IN', NULL, 'Dahod'),
(320, 115, 'IN', NULL, 'Dholka'),
(321, 115, 'IN', NULL, 'Dhoraji'),
(322, 115, 'IN', NULL, 'Dhrangadhra'),
(323, 115, 'IN', NULL, 'Disa'),
(324, 115, 'IN', NULL, 'Gandhidham'),
(325, 115, 'IN', NULL, 'Gandhinagar'),
(326, 115, 'IN', NULL, 'Ghatlodiya'),
(327, 115, 'IN', NULL, 'Godhra'),
(328, 115, 'IN', NULL, 'Gondal'),
(329, 115, 'IN', NULL, 'Himatnagar'),
(330, 115, 'IN', NULL, 'Jamnagar'),
(331, 115, 'IN', NULL, 'Jamnagar'),
(332, 115, 'IN', NULL, 'Jetpur'),
(333, 115, 'IN', NULL, 'Junagadh'),
(334, 115, 'IN', NULL, 'Kalol'),
(335, 115, 'IN', NULL, 'Keshod'),
(336, 115, 'IN', NULL, 'Khambhat'),
(337, 115, 'IN', NULL, 'Kundla'),
(338, 115, 'IN', NULL, 'Mahuva'),
(339, 115, 'IN', NULL, 'Mangrol'),
(340, 115, 'IN', NULL, 'Modasa'),
(341, 115, 'IN', NULL, 'Morvi'),
(342, 115, 'IN', NULL, 'Nadiad'),
(343, 115, 'IN', NULL, 'Navagam Ghed'),
(344, 115, 'IN', NULL, 'Navsari'),
(345, 115, 'IN', NULL, 'Palitana'),
(346, 115, 'IN', NULL, 'Patan'),
(347, 115, 'IN', NULL, 'Porbandar'),
(348, 115, 'IN', NULL, 'Puna'),
(349, 115, 'IN', NULL, 'Rajkot'),
(350, 115, 'IN', NULL, 'Ramod'),
(351, 115, 'IN', NULL, 'Ranip'),
(352, 115, 'IN', NULL, 'Siddhapur'),
(353, 115, 'IN', NULL, 'Sihor'),
(354, 115, 'IN', NULL, 'Surat'),
(355, 115, 'IN', NULL, 'Surendranagar'),
(356, 115, 'IN', NULL, 'Thaltej'),
(357, 115, 'IN', NULL, 'Una'),
(358, 115, 'IN', NULL, 'Unjha'),
(359, 115, 'IN', NULL, 'Upleta'),
(360, 115, 'IN', NULL, 'Vadodara'),
(361, 115, 'IN', NULL, 'Valsad'),
(362, 115, 'IN', NULL, 'Vapi'),
(363, 115, 'IN', NULL, 'Vastral'),
(364, 115, 'IN', NULL, 'Vejalpur'),
(365, 115, 'IN', NULL, 'Veraval'),
(366, 115, 'IN', NULL, 'Vijalpor'),
(367, 115, 'IN', NULL, 'Visnagar'),
(368, 115, 'IN', NULL, 'Wadhwan'),
(369, 115, 'IN', NULL, 'Ambala'),
(370, 115, 'IN', NULL, 'Ambala Cantonment'),
(371, 115, 'IN', NULL, 'Ambala Sadar'),
(372, 115, 'IN', NULL, 'Bahadurgarh'),
(373, 115, 'IN', NULL, 'Bhiwani'),
(374, 115, 'IN', NULL, 'Charkhi Dadri'),
(375, 115, 'IN', NULL, 'Dabwali'),
(376, 115, 'IN', NULL, 'Faridabad'),
(377, 115, 'IN', NULL, 'Gohana'),
(378, 115, 'IN', NULL, 'Hisar'),
(379, 115, 'IN', NULL, 'Jagadhri'),
(380, 115, 'IN', NULL, 'Jind'),
(381, 115, 'IN', NULL, 'Kaithal'),
(382, 115, 'IN', NULL, 'Karnal'),
(383, 115, 'IN', NULL, 'Narnaul'),
(384, 115, 'IN', NULL, 'Narwana'),
(385, 115, 'IN', NULL, 'Palwal'),
(386, 115, 'IN', NULL, 'Panchkula'),
(387, 115, 'IN', NULL, 'Panipat'),
(388, 115, 'IN', NULL, 'Rewari'),
(389, 115, 'IN', NULL, 'Rohtak'),
(390, 115, 'IN', NULL, 'Sirsa'),
(391, 115, 'IN', NULL, 'Sonipat'),
(392, 115, 'IN', NULL, 'Thanesar'),
(393, 115, 'IN', NULL, 'Tohana'),
(394, 115, 'IN', NULL, 'Yamunanagar'),
(395, 115, 'IN', NULL, 'Shimla'),
(396, 115, 'IN', NULL, 'Anantnag'),
(397, 115, 'IN', NULL, 'Baramula'),
(398, 115, 'IN', NULL, 'Bari Brahmana'),
(399, 115, 'IN', NULL, 'Jammu'),
(400, 115, 'IN', NULL, 'Kathua'),
(401, 115, 'IN', NULL, 'Sopur'),
(402, 115, 'IN', NULL, 'Srinagar'),
(403, 115, 'IN', NULL, 'Udhampur'),
(404, 115, 'IN', NULL, 'Adityapur'),
(405, 115, 'IN', NULL, 'Bagbahra'),
(406, 115, 'IN', NULL, 'Bhuli'),
(407, 115, 'IN', NULL, 'Bokaro'),
(408, 115, 'IN', NULL, 'Chaibasa'),
(409, 115, 'IN', NULL, 'Chas'),
(410, 115, 'IN', NULL, 'Daltenganj'),
(411, 115, 'IN', NULL, 'Devghar'),
(412, 115, 'IN', NULL, 'Dhanbad'),
(413, 115, 'IN', NULL, 'Hazaribag'),
(414, 115, 'IN', NULL, 'Jamshedpur'),
(415, 115, 'IN', NULL, 'Jharia'),
(416, 115, 'IN', NULL, 'Jhumri Tilaiya'),
(417, 115, 'IN', NULL, 'Jorapokhar'),
(418, 115, 'IN', NULL, 'Katras'),
(419, 115, 'IN', NULL, 'Lohardaga'),
(420, 115, 'IN', NULL, 'Mango'),
(421, 115, 'IN', NULL, 'Phusro'),
(422, 115, 'IN', NULL, 'Ramgarh'),
(423, 115, 'IN', NULL, 'Ranchi'),
(424, 115, 'IN', NULL, 'Sahibganj'),
(425, 115, 'IN', NULL, 'Saunda'),
(426, 115, 'IN', NULL, 'Sindari'),
(427, 115, 'IN', NULL, 'Bagalkot'),
(428, 115, 'IN', NULL, 'Bangalore'),
(429, 115, 'IN', NULL, 'Basavakalyan'),
(430, 115, 'IN', NULL, 'Belgaum'),
(431, 115, 'IN', NULL, 'Bellary'),
(432, 115, 'IN', NULL, 'Bhadravati'),
(433, 115, 'IN', NULL, 'Bidar'),
(434, 115, 'IN', NULL, 'Bijapur'),
(435, 115, 'IN', NULL, 'Bommanahalli'),
(436, 115, 'IN', NULL, 'Byatarayanapura'),
(437, 115, 'IN', NULL, 'Challakere'),
(438, 115, 'IN', NULL, 'Chamrajnagar'),
(439, 115, 'IN', NULL, 'Channapatna'),
(440, 115, 'IN', NULL, 'Chik Ballapur'),
(441, 115, 'IN', NULL, 'Chikmagalur'),
(442, 115, 'IN', NULL, 'Chintamani'),
(443, 115, 'IN', NULL, 'Chitradurga'),
(444, 115, 'IN', NULL, 'Dasarahalli'),
(445, 115, 'IN', NULL, 'Davanagere'),
(446, 115, 'IN', NULL, 'Dod Ballapur'),
(447, 115, 'IN', NULL, 'Gadag'),
(448, 115, 'IN', NULL, 'Gangawati'),
(449, 115, 'IN', NULL, 'Gokak'),
(450, 115, 'IN', NULL, 'Gulbarga'),
(451, 115, 'IN', NULL, 'Harihar'),
(452, 115, 'IN', NULL, 'Hassan'),
(453, 115, 'IN', NULL, 'Haveri'),
(454, 115, 'IN', NULL, 'Hiriyur'),
(455, 115, 'IN', NULL, 'Hosakote'),
(456, 115, 'IN', NULL, 'Hospet'),
(457, 115, 'IN', NULL, 'Hubli'),
(458, 115, 'IN', NULL, 'Ilkal'),
(459, 115, 'IN', NULL, 'Jamkhandi'),
(460, 115, 'IN', NULL, 'Kanakapura'),
(461, 115, 'IN', NULL, 'Karwar'),
(462, 115, 'IN', NULL, 'Kolar'),
(463, 115, 'IN', NULL, 'Kollegal'),
(464, 115, 'IN', NULL, 'Koppal'),
(465, 115, 'IN', NULL, 'Krishnarajapura'),
(466, 115, 'IN', NULL, 'Mahadevapura'),
(467, 115, 'IN', NULL, 'Maisuru'),
(468, 115, 'IN', NULL, 'Mandya'),
(469, 115, 'IN', NULL, 'Mangaluru'),
(470, 115, 'IN', NULL, 'Nipani'),
(471, 115, 'IN', NULL, 'Pattanagere'),
(472, 115, 'IN', NULL, 'Puttur'),
(473, 115, 'IN', NULL, 'Rabkavi'),
(474, 115, 'IN', NULL, 'Raichur'),
(475, 115, 'IN', NULL, 'Ramanagaram'),
(476, 115, 'IN', NULL, 'Ranibennur'),
(477, 115, 'IN', NULL, 'Robertsonpet'),
(478, 115, 'IN', NULL, 'Sagar'),
(479, 115, 'IN', NULL, 'Shahabad'),
(480, 115, 'IN', NULL, 'Shahpur'),
(481, 115, 'IN', NULL, 'Shimoga'),
(482, 115, 'IN', NULL, 'Shorapur'),
(483, 115, 'IN', NULL, 'Sidlaghatta'),
(484, 115, 'IN', NULL, 'Sira'),
(485, 115, 'IN', NULL, 'Sirsi'),
(486, 115, 'IN', NULL, 'Tiptur'),
(487, 115, 'IN', NULL, 'Tumkur'),
(488, 115, 'IN', NULL, 'Udupi'),
(489, 115, 'IN', NULL, 'Ullal'),
(490, 115, 'IN', NULL, 'Yadgir'),
(491, 115, 'IN', NULL, 'Yelahanka'),
(492, 115, 'IN', NULL, 'Alappuzha'),
(493, 115, 'IN', NULL, 'Beypur'),
(494, 115, 'IN', NULL, 'Cheruvannur'),
(495, 115, 'IN', NULL, 'Edakkara'),
(496, 115, 'IN', NULL, 'Edathala'),
(497, 115, 'IN', NULL, 'Kalamassery'),
(498, 115, 'IN', NULL, 'Kannan Devan Hills'),
(499, 115, 'IN', NULL, 'Kannangad'),
(500, 115, 'IN', NULL, 'Kannur'),
(501, 115, 'IN', NULL, 'Kayankulam'),
(502, 115, 'IN', NULL, 'Kochi'),
(503, 115, 'IN', NULL, 'Kollam'),
(504, 115, 'IN', NULL, 'Kottayam'),
(505, 115, 'IN', NULL, 'Koyilandi'),
(506, 115, 'IN', NULL, 'Kozhikkod'),
(507, 115, 'IN', NULL, 'Kunnamkulam'),
(508, 115, 'IN', NULL, 'Malappuram'),
(509, 115, 'IN', NULL, 'Manjeri'),
(510, 115, 'IN', NULL, 'Nedumangad'),
(511, 115, 'IN', NULL, 'Neyyattinkara'),
(512, 115, 'IN', NULL, 'Palakkad'),
(513, 115, 'IN', NULL, 'Pallichal'),
(514, 115, 'IN', NULL, 'Payyannur'),
(515, 115, 'IN', NULL, 'Ponnani'),
(516, 115, 'IN', NULL, 'Talipparamba'),
(517, 115, 'IN', NULL, 'Thalassery'),
(518, 115, 'IN', NULL, 'Thiruvananthapuram'),
(519, 115, 'IN', NULL, 'Thrippunithura'),
(520, 115, 'IN', NULL, 'Thrissur'),
(521, 115, 'IN', NULL, 'Tirur'),
(522, 115, 'IN', NULL, 'Tiruvalla'),
(523, 115, 'IN', NULL, 'Vadakara'),
(524, 115, 'IN', NULL, 'Ashoknagar'),
(525, 115, 'IN', NULL, 'Balaghat'),
(526, 115, 'IN', NULL, 'Basoda'),
(527, 115, 'IN', NULL, 'Betul'),
(528, 115, 'IN', NULL, 'Bhind'),
(529, 115, 'IN', NULL, 'Bhopal'),
(530, 115, 'IN', NULL, 'BinaEtawa'),
(531, 115, 'IN', NULL, 'Burhanpur'),
(532, 115, 'IN', NULL, 'Chhatarpur'),
(533, 115, 'IN', NULL, 'Chhindwara'),
(534, 115, 'IN', NULL, 'Dabra'),
(535, 115, 'IN', NULL, 'Damoh'),
(536, 115, 'IN', NULL, 'Datia'),
(537, 115, 'IN', NULL, 'Dewas'),
(538, 115, 'IN', NULL, 'Dhar'),
(539, 115, 'IN', NULL, 'Gohad'),
(540, 115, 'IN', NULL, 'Guna'),
(541, 115, 'IN', NULL, 'Gwalior'),
(542, 115, 'IN', NULL, 'Harda'),
(543, 115, 'IN', NULL, 'Hoshangabad'),
(544, 115, 'IN', NULL, 'Indore'),
(545, 115, 'IN', NULL, 'Itarsi'),
(546, 115, 'IN', NULL, 'Jabalpur'),
(547, 115, 'IN', NULL, 'Jabalpur Cantonment'),
(548, 115, 'IN', NULL, 'Jaora'),
(549, 115, 'IN', NULL, 'Khandwa'),
(550, 115, 'IN', NULL, 'Khargone'),
(551, 115, 'IN', NULL, 'Mandidip'),
(552, 115, 'IN', NULL, 'Mandsaur'),
(553, 115, 'IN', NULL, 'Mau'),
(554, 115, 'IN', NULL, 'Morena'),
(555, 115, 'IN', NULL, 'Murwara'),
(556, 115, 'IN', NULL, 'Nagda'),
(557, 115, 'IN', NULL, 'Nimach'),
(558, 115, 'IN', NULL, 'Pithampur'),
(559, 115, 'IN', NULL, 'Raghogarh'),
(560, 115, 'IN', NULL, 'Ratlam'),
(561, 115, 'IN', NULL, 'Rewa'),
(562, 115, 'IN', NULL, 'Sagar'),
(563, 115, 'IN', NULL, 'Sarni'),
(564, 115, 'IN', NULL, 'Satna'),
(565, 115, 'IN', NULL, 'Sehore'),
(566, 115, 'IN', NULL, 'Sendhwa'),
(567, 115, 'IN', NULL, 'Seoni'),
(568, 115, 'IN', NULL, 'Shahdol'),
(569, 115, 'IN', NULL, 'Shajapur'),
(570, 115, 'IN', NULL, 'Sheopur'),
(571, 115, 'IN', NULL, 'Shivapuri'),
(572, 115, 'IN', NULL, 'Sidhi'),
(573, 115, 'IN', NULL, 'Singrauli'),
(574, 115, 'IN', NULL, 'Tikamgarh'),
(575, 115, 'IN', NULL, 'Ujjain'),
(576, 115, 'IN', NULL, 'Vidisha'),
(577, 115, 'IN', NULL, 'Achalpur'),
(578, 115, 'IN', NULL, 'Ahmadnagar'),
(579, 115, 'IN', NULL, 'Akola'),
(580, 115, 'IN', NULL, 'Akot'),
(581, 115, 'IN', NULL, 'Amalner'),
(582, 115, 'IN', NULL, 'Ambajogai'),
(583, 115, 'IN', NULL, 'Amravati'),
(584, 115, 'IN', NULL, 'Anjangaon'),
(585, 115, 'IN', NULL, 'Aurangabad'),
(586, 115, 'IN', NULL, 'Badlapur'),
(587, 115, 'IN', NULL, 'Ballarpur'),
(588, 115, 'IN', NULL, 'Baramati'),
(589, 115, 'IN', NULL, 'Barsi'),
(590, 115, 'IN', NULL, 'Basmat'),
(591, 115, 'IN', NULL, 'Bhadravati'),
(592, 115, 'IN', NULL, 'Bhandara'),
(593, 115, 'IN', NULL, 'Bhiwandi'),
(594, 115, 'IN', NULL, 'Bhusawal'),
(595, 115, 'IN', NULL, 'Bid'),
(596, 115, 'IN', NULL, 'Mumbai'),
(597, 115, 'IN', NULL, 'Buldana'),
(598, 115, 'IN', NULL, 'Chalisgaon'),
(599, 115, 'IN', NULL, 'Chandrapur'),
(600, 115, 'IN', NULL, 'Chikhli'),
(601, 115, 'IN', NULL, 'Chiplun'),
(602, 115, 'IN', NULL, 'Chopda'),
(603, 115, 'IN', NULL, 'Dahanu'),
(604, 115, 'IN', NULL, 'Deolali'),
(605, 115, 'IN', NULL, 'Dhule'),
(606, 115, 'IN', NULL, 'Digdoh'),
(607, 115, 'IN', NULL, 'Diglur'),
(608, 115, 'IN', NULL, 'Gadchiroli'),
(609, 115, 'IN', NULL, 'Gondiya'),
(610, 115, 'IN', NULL, 'Hinganghat'),
(611, 115, 'IN', NULL, 'Hingoli'),
(612, 115, 'IN', NULL, 'Ichalkaranji'),
(613, 115, 'IN', NULL, 'Jalgaon'),
(614, 115, 'IN', NULL, 'Jalna'),
(615, 115, 'IN', NULL, 'Kalyan'),
(616, 115, 'IN', NULL, 'Kamthi'),
(617, 115, 'IN', NULL, 'Karanja'),
(618, 115, 'IN', NULL, 'Khadki'),
(619, 115, 'IN', NULL, 'Khamgaon'),
(620, 115, 'IN', NULL, 'Khopoli'),
(621, 115, 'IN', NULL, 'Kolhapur'),
(622, 115, 'IN', NULL, 'Kopargaon'),
(623, 115, 'IN', NULL, 'Latur'),
(624, 115, 'IN', NULL, 'Lonavale'),
(625, 115, 'IN', NULL, 'Malegaon'),
(626, 115, 'IN', NULL, 'Malkapur'),
(627, 115, 'IN', NULL, 'Manmad'),
(628, 115, 'IN', NULL, 'Mira Bhayandar'),
(629, 115, 'IN', NULL, 'Nagpur'),
(630, 115, 'IN', NULL, 'Nalasopara'),
(631, 115, 'IN', NULL, 'Nanded'),
(632, 115, 'IN', NULL, 'Nandurbar'),
(633, 115, 'IN', NULL, 'Nashik'),
(634, 115, 'IN', NULL, 'Navghar'),
(635, 115, 'IN', NULL, 'Navi Mumbai'),
(636, 115, 'IN', NULL, 'Navi Mumbai'),
(637, 115, 'IN', NULL, 'Osmanabad'),
(638, 115, 'IN', NULL, 'Palghar'),
(639, 115, 'IN', NULL, 'Pandharpur'),
(640, 115, 'IN', NULL, 'Parbhani'),
(641, 115, 'IN', NULL, 'Phaltan'),
(642, 115, 'IN', NULL, 'Pimpri'),
(643, 115, 'IN', NULL, 'Pune'),
(644, 115, 'IN', NULL, 'Pune Cantonment'),
(645, 115, 'IN', NULL, 'Pusad'),
(646, 115, 'IN', NULL, 'Ratnagiri'),
(647, 115, 'IN', NULL, 'SangliMiraj'),
(648, 115, 'IN', NULL, 'Satara'),
(649, 115, 'IN', NULL, 'Shahada'),
(650, 115, 'IN', NULL, 'Shegaon'),
(651, 115, 'IN', NULL, 'Shirpur'),
(652, 115, 'IN', NULL, 'Sholapur'),
(653, 115, 'IN', NULL, 'Shrirampur'),
(654, 115, 'IN', NULL, 'Sillod'),
(655, 115, 'IN', NULL, 'Thana'),
(656, 115, 'IN', NULL, 'Udgir'),
(657, 115, 'IN', NULL, 'Ulhasnagar'),
(658, 115, 'IN', NULL, 'Uran Islampur'),
(659, 115, 'IN', NULL, 'Vasai'),
(660, 115, 'IN', NULL, 'Virar'),
(661, 115, 'IN', NULL, 'Wadi'),
(662, 115, 'IN', NULL, 'Wani'),
(663, 115, 'IN', NULL, 'Wardha'),
(664, 115, 'IN', NULL, 'Warud'),
(665, 115, 'IN', NULL, 'Washim'),
(666, 115, 'IN', NULL, 'Yavatmal'),
(667, 115, 'IN', NULL, 'Imphal'),
(668, 115, 'IN', NULL, 'Shillong'),
(669, 115, 'IN', NULL, 'Tura'),
(670, 115, 'IN', NULL, 'Aizawl'),
(671, 115, 'IN', NULL, 'Lunglei'),
(672, 115, 'IN', NULL, 'Dimapur'),
(673, 115, 'IN', NULL, 'Kohima'),
(674, 115, 'IN', NULL, 'Wokha'),
(675, 115, 'IN', NULL, 'Balangir'),
(676, 115, 'IN', NULL, 'Baleshwar'),
(677, 115, 'IN', NULL, 'Barbil'),
(678, 115, 'IN', NULL, 'Bargarh'),
(679, 115, 'IN', NULL, 'Baripada'),
(680, 115, 'IN', NULL, 'Bhadrak'),
(681, 115, 'IN', NULL, 'Bhawanipatna'),
(682, 115, 'IN', NULL, 'Bhubaneswar'),
(683, 115, 'IN', NULL, 'Brahmapur'),
(684, 115, 'IN', NULL, 'Brajrajnagar'),
(685, 115, 'IN', NULL, 'Dhenkanal'),
(686, 115, 'IN', NULL, 'Jaypur'),
(687, 115, 'IN', NULL, 'Jharsuguda'),
(688, 115, 'IN', NULL, 'Kataka'),
(689, 115, 'IN', NULL, 'Kendujhar'),
(690, 115, 'IN', NULL, 'Paradwip'),
(691, 115, 'IN', NULL, 'Puri'),
(692, 115, 'IN', NULL, 'Raurkela'),
(693, 115, 'IN', NULL, 'Raurkela Industrial Township'),
(694, 115, 'IN', NULL, 'Rayagada'),
(695, 115, 'IN', NULL, 'Sambalpur'),
(696, 115, 'IN', NULL, 'Sunabeda'),
(697, 115, 'IN', NULL, 'Karaikal'),
(698, 115, 'IN', NULL, 'Ozhukarai'),
(699, 115, 'IN', NULL, 'Pondicherry'),
(700, 115, 'IN', NULL, 'Abohar'),
(701, 115, 'IN', NULL, 'Amritsar'),
(702, 115, 'IN', NULL, 'Barnala'),
(703, 115, 'IN', NULL, 'Batala'),
(704, 115, 'IN', NULL, 'Bathinda'),
(705, 115, 'IN', NULL, 'Dhuri'),
(706, 115, 'IN', NULL, 'Faridkot'),
(707, 115, 'IN', NULL, 'Fazilka'),
(708, 115, 'IN', NULL, 'Firozpur'),
(709, 115, 'IN', NULL, 'Firozpur Cantonment'),
(710, 115, 'IN', NULL, 'Gobindgarh'),
(711, 115, 'IN', NULL, 'Gurdaspur'),
(712, 115, 'IN', NULL, 'Hoshiarpur'),
(713, 115, 'IN', NULL, 'Jagraon'),
(714, 115, 'IN', NULL, 'Jalandhar'),
(715, 115, 'IN', NULL, 'Kapurthala'),
(716, 115, 'IN', NULL, 'Khanna'),
(717, 115, 'IN', NULL, 'Kot Kapura'),
(718, 115, 'IN', NULL, 'Ludhiana'),
(719, 115, 'IN', NULL, 'Malaut'),
(720, 115, 'IN', NULL, 'Maler Kotla'),
(721, 115, 'IN', NULL, 'Mansa'),
(722, 115, 'IN', NULL, 'Moga'),
(723, 115, 'IN', NULL, 'Mohali'),
(724, 115, 'IN', NULL, 'Pathankot'),
(725, 115, 'IN', NULL, 'Patiala'),
(726, 115, 'IN', NULL, 'Phagwara'),
(727, 115, 'IN', NULL, 'Rajpura'),
(728, 115, 'IN', NULL, 'Rupnagar'),
(729, 115, 'IN', NULL, 'Samana'),
(730, 115, 'IN', NULL, 'Sangrur'),
(731, 115, 'IN', NULL, 'Sirhind'),
(732, 115, 'IN', NULL, 'Sunam'),
(733, 115, 'IN', NULL, 'Tarn Taran'),
(734, 115, 'IN', NULL, 'Ajmer'),
(735, 115, 'IN', NULL, 'Alwar'),
(736, 115, 'IN', NULL, 'Balotra'),
(737, 115, 'IN', NULL, 'Banswara'),
(738, 115, 'IN', NULL, 'Baran'),
(739, 115, 'IN', NULL, 'Bari'),
(740, 115, 'IN', NULL, 'Barmer'),
(741, 115, 'IN', NULL, 'Beawar'),
(742, 115, 'IN', NULL, 'Bharatpur'),
(743, 115, 'IN', NULL, 'Bhilwara'),
(744, 115, 'IN', NULL, 'Bhiwadi'),
(745, 115, 'IN', NULL, 'Bikaner'),
(746, 115, 'IN', NULL, 'Bundi'),
(747, 115, 'IN', NULL, 'Chittaurgarh'),
(748, 115, 'IN', NULL, 'Chomun'),
(749, 115, 'IN', NULL, 'Churu'),
(750, 115, 'IN', NULL, 'Daosa'),
(751, 115, 'IN', NULL, 'Dhaulpur'),
(752, 115, 'IN', NULL, 'Didwana'),
(753, 115, 'IN', NULL, 'Fatehpur'),
(754, 115, 'IN', NULL, 'Ganganagar'),
(755, 115, 'IN', NULL, 'Gangapur'),
(756, 115, 'IN', NULL, 'Hanumangarh'),
(757, 115, 'IN', NULL, 'Hindaun'),
(758, 115, 'IN', NULL, 'Jaipur'),
(759, 115, 'IN', NULL, 'Jaisalmer'),
(760, 115, 'IN', NULL, 'Jalor'),
(761, 115, 'IN', NULL, 'Jhalawar'),
(762, 115, 'IN', NULL, 'Jhunjhunun'),
(763, 115, 'IN', NULL, 'Jodhpur'),
(764, 115, 'IN', NULL, 'Karauli'),
(765, 115, 'IN', NULL, 'Kishangarh'),
(766, 115, 'IN', NULL, 'Kota'),
(767, 115, 'IN', NULL, 'Kuchaman'),
(768, 115, 'IN', NULL, 'Ladnun'),
(769, 115, 'IN', NULL, 'Makrana'),
(770, 115, 'IN', NULL, 'Nagaur'),
(771, 115, 'IN', NULL, 'Nawalgarh'),
(772, 115, 'IN', NULL, 'Nimbahera'),
(773, 115, 'IN', NULL, 'Nokha'),
(774, 115, 'IN', NULL, 'Pali'),
(775, 115, 'IN', NULL, 'Rajsamand'),
(776, 115, 'IN', NULL, 'Ratangarh'),
(777, 115, 'IN', NULL, 'Sardarshahr'),
(778, 115, 'IN', NULL, 'Sawai Madhopur'),
(779, 115, 'IN', NULL, 'Sikar'),
(780, 115, 'IN', NULL, 'Sujangarh'),
(781, 115, 'IN', NULL, 'Suratgarh'),
(782, 115, 'IN', NULL, 'Tonk'),
(783, 115, 'IN', NULL, 'Udaipur'),
(784, 115, 'IN', NULL, 'Alandur'),
(785, 115, 'IN', NULL, 'Ambattur'),
(786, 115, 'IN', NULL, 'Ambur'),
(787, 115, 'IN', NULL, 'Arakonam'),
(788, 115, 'IN', NULL, 'Arani'),
(789, 115, 'IN', NULL, 'Aruppukkottai'),
(790, 115, 'IN', NULL, 'Attur'),
(791, 115, 'IN', NULL, 'Avadi'),
(792, 115, 'IN', NULL, 'Avaniapuram'),
(793, 115, 'IN', NULL, 'Bodinayakkanur'),
(794, 115, 'IN', NULL, 'Chengalpattu'),
(795, 115, 'IN', NULL, 'Dharapuram'),
(796, 115, 'IN', NULL, 'Dharmapuri'),
(797, 115, 'IN', NULL, 'Dindigul'),
(798, 115, 'IN', NULL, 'Erode'),
(799, 115, 'IN', NULL, 'Gopichettipalaiyam'),
(800, 115, 'IN', NULL, 'Gudalur'),
(801, 115, 'IN', NULL, 'Gudiyattam'),
(802, 115, 'IN', NULL, 'Hosur'),
(803, 115, 'IN', NULL, 'Idappadi'),
(804, 115, 'IN', NULL, 'Kadayanallur'),
(805, 115, 'IN', NULL, 'Kambam'),
(806, 115, 'IN', NULL, 'Kanchipuram'),
(807, 115, 'IN', NULL, 'Karur'),
(808, 115, 'IN', NULL, 'Kavundampalaiyam'),
(809, 115, 'IN', NULL, 'Kovilpatti'),
(810, 115, 'IN', NULL, 'Koyampattur'),
(811, 115, 'IN', NULL, 'Krishnagiri'),
(812, 115, 'IN', NULL, 'Kumarapalaiyam'),
(813, 115, 'IN', NULL, 'Kumbakonam'),
(814, 115, 'IN', NULL, 'Kuniyamuthur'),
(815, 115, 'IN', NULL, 'Kurichi'),
(816, 115, 'IN', NULL, 'Madhavaram'),
(817, 115, 'IN', NULL, 'Madras'),
(818, 115, 'IN', NULL, 'Madurai'),
(819, 115, 'IN', NULL, 'Maduravoyal'),
(820, 115, 'IN', NULL, 'Mannargudi'),
(821, 115, 'IN', NULL, 'Mayiladuthurai'),
(822, 115, 'IN', NULL, 'Mettupalayam'),
(823, 115, 'IN', NULL, 'Mettur'),
(824, 115, 'IN', NULL, 'Nagapattinam'),
(825, 115, 'IN', NULL, 'Nagercoil'),
(826, 115, 'IN', NULL, 'Namakkal'),
(827, 115, 'IN', NULL, 'Nerkunram'),
(828, 115, 'IN', NULL, 'Neyveli'),
(829, 115, 'IN', NULL, 'Pallavaram'),
(830, 115, 'IN', NULL, 'Pammal'),
(831, 115, 'IN', NULL, 'Pannuratti'),
(832, 115, 'IN', NULL, 'Paramakkudi'),
(833, 115, 'IN', NULL, 'Pattukkottai'),
(834, 115, 'IN', NULL, 'Pollachi'),
(835, 115, 'IN', NULL, 'Pudukkottai'),
(836, 115, 'IN', NULL, 'Puliyankudi'),
(837, 115, 'IN', NULL, 'Punamalli'),
(838, 115, 'IN', NULL, 'Rajapalaiyam'),
(839, 115, 'IN', NULL, 'Ramanathapuram'),
(840, 115, 'IN', NULL, 'Salem'),
(841, 115, 'IN', NULL, 'Sankarankoil'),
(842, 115, 'IN', NULL, 'Sivakasi'),
(843, 115, 'IN', NULL, 'Srivilliputtur'),
(844, 115, 'IN', NULL, 'Tambaram'),
(845, 115, 'IN', NULL, 'Tenkasi'),
(846, 115, 'IN', NULL, 'Thanjavur'),
(847, 115, 'IN', NULL, 'Theni Allinagaram'),
(848, 115, 'IN', NULL, 'Thiruthangal'),
(849, 115, 'IN', NULL, 'Thiruvarur'),
(850, 115, 'IN', NULL, 'Thuthukkudi'),
(851, 115, 'IN', NULL, 'Tindivanam'),
(852, 115, 'IN', NULL, 'Tiruchchirappalli'),
(853, 115, 'IN', NULL, 'Tiruchengode'),
(854, 115, 'IN', NULL, 'Tirunelveli'),
(855, 115, 'IN', NULL, 'Tirupathur'),
(856, 115, 'IN', NULL, 'Tiruppur'),
(857, 115, 'IN', NULL, 'Tiruvannamalai'),
(858, 115, 'IN', NULL, 'Tiruvottiyur'),
(859, 115, 'IN', NULL, 'Udagamandalam'),
(860, 115, 'IN', NULL, 'Udumalaipettai'),
(861, 115, 'IN', NULL, 'Valparai'),
(862, 115, 'IN', NULL, 'Vaniyambadi'),
(863, 115, 'IN', NULL, 'Velampalaiyam'),
(864, 115, 'IN', NULL, 'Velluru'),
(865, 115, 'IN', NULL, 'Viluppuram'),
(866, 115, 'IN', NULL, 'Virappanchatram'),
(867, 115, 'IN', NULL, 'Virudhachalam'),
(868, 115, 'IN', NULL, 'Virudunagar'),
(869, 115, 'IN', NULL, 'Agartala'),
(870, 115, 'IN', NULL, 'Agartala MCl'),
(871, 115, 'IN', NULL, 'Badharghat'),
(872, 115, 'IN', NULL, 'Agra'),
(873, 115, 'IN', NULL, 'Aligarh'),
(874, 115, 'IN', NULL, 'Allahabad'),
(875, 115, 'IN', NULL, 'Amroha'),
(876, 115, 'IN', NULL, 'Aonla'),
(877, 115, 'IN', NULL, 'Auraiya'),
(878, 115, 'IN', NULL, 'Ayodhya'),
(879, 115, 'IN', NULL, 'Azamgarh'),
(880, 115, 'IN', NULL, 'Baheri'),
(881, 115, 'IN', NULL, 'Bahraich'),
(882, 115, 'IN', NULL, 'Ballia'),
(883, 115, 'IN', NULL, 'Balrampur'),
(884, 115, 'IN', NULL, 'Banda'),
(885, 115, 'IN', NULL, 'Baraut'),
(886, 115, 'IN', NULL, 'Bareli'),
(887, 115, 'IN', NULL, 'Basti'),
(888, 115, 'IN', NULL, 'Behta Hajipur'),
(889, 115, 'IN', NULL, 'Bela'),
(890, 115, 'IN', NULL, 'Bhadohi'),
(891, 115, 'IN', NULL, 'Bijnor'),
(892, 115, 'IN', NULL, 'Bisalpur'),
(893, 115, 'IN', NULL, 'Biswan'),
(894, 115, 'IN', NULL, 'Budaun'),
(895, 115, 'IN', NULL, 'Bulandshahr'),
(896, 115, 'IN', NULL, 'Chandausi'),
(897, 115, 'IN', NULL, 'Chandpur'),
(898, 115, 'IN', NULL, 'Chhibramau'),
(899, 115, 'IN', NULL, 'Chitrakut Dham'),
(900, 115, 'IN', NULL, 'Dadri'),
(901, 115, 'IN', NULL, 'Deoband'),
(902, 115, 'IN', NULL, 'Deoria'),
(903, 115, 'IN', NULL, 'Etah'),
(904, 115, 'IN', NULL, 'Etawah'),
(905, 115, 'IN', NULL, 'Faizabad'),
(906, 115, 'IN', NULL, 'Faridpur'),
(907, 115, 'IN', NULL, 'Farrukhabad'),
(908, 115, 'IN', NULL, 'Fatehpur'),
(909, 115, 'IN', NULL, 'Firozabad'),
(910, 115, 'IN', NULL, 'Gajraula'),
(911, 115, 'IN', NULL, 'Ganga Ghat'),
(912, 115, 'IN', NULL, 'Gangoh'),
(913, 115, 'IN', NULL, 'Ghaziabad'),
(914, 115, 'IN', NULL, 'Ghazipur'),
(915, 115, 'IN', NULL, 'Gola Gokarannath'),
(916, 115, 'IN', NULL, 'Gonda'),
(917, 115, 'IN', NULL, 'Gorakhpur'),
(918, 115, 'IN', NULL, 'Hapur'),
(919, 115, 'IN', NULL, 'Hardoi'),
(920, 115, 'IN', NULL, 'Hasanpur'),
(921, 115, 'IN', NULL, 'Hathras'),
(922, 115, 'IN', NULL, 'Jahangirabad'),
(923, 115, 'IN', NULL, 'Jalaun'),
(924, 115, 'IN', NULL, 'Jaunpur'),
(925, 115, 'IN', NULL, 'Jhansi'),
(926, 115, 'IN', NULL, 'Kadi'),
(927, 115, 'IN', NULL, 'Kairana'),
(928, 115, 'IN', NULL, 'Kannauj'),
(929, 115, 'IN', NULL, 'Kanpur'),
(930, 115, 'IN', NULL, 'Kanpur Cantonment'),
(931, 115, 'IN', NULL, 'Kasganj'),
(932, 115, 'IN', NULL, 'Khatauli'),
(933, 115, 'IN', NULL, 'Khora'),
(934, 115, 'IN', NULL, 'Khurja'),
(935, 115, 'IN', NULL, 'Kiratpur'),
(936, 115, 'IN', NULL, 'Kosi Kalan'),
(937, 115, 'IN', NULL, 'Laharpur'),
(938, 115, 'IN', NULL, 'Lakhimpur'),
(939, 115, 'IN', NULL, 'Lakhnau'),
(940, 115, 'IN', NULL, 'Lakhnau Cantonment'),
(941, 115, 'IN', NULL, 'Lalitpur'),
(942, 115, 'IN', NULL, 'Loni'),
(943, 115, 'IN', NULL, 'Mahoba'),
(944, 115, 'IN', NULL, 'Mainpuri'),
(945, 115, 'IN', NULL, 'Mathura'),
(946, 115, 'IN', NULL, 'Mau'),
(947, 115, 'IN', NULL, 'Mauranipur'),
(948, 115, 'IN', NULL, 'Mawana'),
(949, 115, 'IN', NULL, 'Mirat'),
(950, 115, 'IN', NULL, 'Mirat Cantonment'),
(951, 115, 'IN', NULL, 'Mirzapur'),
(952, 115, 'IN', NULL, 'Modinagar'),
(953, 115, 'IN', NULL, 'Moradabad'),
(954, 115, 'IN', NULL, 'Mubarakpur'),
(955, 115, 'IN', NULL, 'Mughal Sarai'),
(956, 115, 'IN', NULL, 'Muradnagar'),
(957, 115, 'IN', NULL, 'Muzaffarnagar'),
(958, 115, 'IN', NULL, 'Nagina'),
(959, 115, 'IN', NULL, 'Najibabad'),
(960, 115, 'IN', NULL, 'Nawabganj'),
(961, 115, 'IN', NULL, 'Noida'),
(962, 115, 'IN', NULL, 'Obra'),
(963, 115, 'IN', NULL, 'Orai'),
(964, 115, 'IN', NULL, 'Pilibhit'),
(965, 115, 'IN', NULL, 'Pilkhuwa'),
(966, 115, 'IN', NULL, 'Rae Bareli'),
(967, 115, 'IN', NULL, 'Ramgarh Nagla Kothi'),
(968, 115, 'IN', NULL, 'Rampur'),
(969, 115, 'IN', NULL, 'Rath'),
(970, 115, 'IN', NULL, 'Renukut'),
(971, 115, 'IN', NULL, 'Saharanpur'),
(972, 115, 'IN', NULL, 'Sahaswan'),
(973, 115, 'IN', NULL, 'Sambhal'),
(974, 115, 'IN', NULL, 'Sandila'),
(975, 115, 'IN', NULL, 'Shahabad'),
(976, 115, 'IN', NULL, 'Shahjahanpur'),
(977, 115, 'IN', NULL, 'Shamli'),
(978, 115, 'IN', NULL, 'Sherkot'),
(979, 115, 'IN', NULL, 'Shikohabad'),
(980, 115, 'IN', NULL, 'Sikandarabad'),
(981, 115, 'IN', NULL, 'Sitapur'),
(982, 115, 'IN', NULL, 'Sukhmalpur Nizamabad'),
(983, 115, 'IN', NULL, 'Sultanpur'),
(984, 115, 'IN', NULL, 'Tanda'),
(985, 115, 'IN', NULL, 'Tilhar'),
(986, 115, 'IN', NULL, 'Tundla'),
(987, 115, 'IN', NULL, 'Ujhani'),
(988, 115, 'IN', NULL, 'Unnao'),
(989, 115, 'IN', NULL, 'Varanasi'),
(990, 115, 'IN', NULL, 'Vrindavan'),
(991, 115, 'IN', NULL, 'Dehra Dun'),
(992, 115, 'IN', NULL, 'Dehra Dun Cantonment'),
(993, 115, 'IN', NULL, 'Gola Range'),
(994, 115, 'IN', NULL, 'Haldwani'),
(995, 115, 'IN', NULL, 'Haridwar'),
(996, 115, 'IN', NULL, 'Kashipur'),
(997, 115, 'IN', NULL, 'Pithoragarh'),
(998, 115, 'IN', NULL, 'Rishikesh'),
(999, 115, 'IN', NULL, 'Rudrapur'),
(1000, 115, 'IN', NULL, 'Rurki'),
(1031, 249, 'US', 'as', 'Achille'),
(1032, 249, 'US', 'tg', 'Achilles'),
(1033, 249, 'US', NULL, 'Ackerly'),
(1034, 249, 'US', NULL, 'Ackerman'),
(1035, 249, 'US', NULL, 'Ackermanville'),
(1037, 249, 'US', NULL, 'Ackworth'),
(1038, 249, 'US', NULL, 'Acme'),
(1039, 249, 'US', NULL, 'Acosta'),
(1040, 249, 'US', NULL, 'Acra'),
(1041, 249, 'US', NULL, 'Acton'),
(1042, 249, 'US', NULL, 'Acushnet'),
(1043, 249, 'US', NULL, 'Acworth'),
(1044, 249, 'US', NULL, 'Ada'),
(1045, 249, 'US', NULL, 'Adah'),
(1046, 249, 'US', NULL, 'Adair'),
(1047, 249, 'US', NULL, 'Adairsville'),
(1048, 249, 'US', NULL, 'Adairville'),
(1049, 249, 'US', NULL, 'Adak'),
(1050, 249, 'US', NULL, 'Adamant'),
(1051, 249, 'US', NULL, 'Adams'),
(1052, 249, 'US', NULL, 'Adams Basin'),
(1053, 249, 'US', NULL, 'Adams Center'),
(1054, 249, 'US', NULL, 'Adams Run'),
(1055, 249, 'US', NULL, 'Adamsburg'),
(1056, 249, 'US', NULL, 'Adamstown'),
(1057, 249, 'US', NULL, 'Adamsville'),
(1058, 249, 'US', NULL, 'Addieville'),
(1059, 249, 'US', NULL, 'Addington'),
(1060, 249, 'US', NULL, 'Addis'),
(1061, 249, 'US', NULL, 'Addison'),
(1062, 249, 'US', NULL, 'Addy'),
(1063, 249, 'US', NULL, 'Addyston'),
(1064, 249, 'US', NULL, 'Adel'),
(1065, 249, 'US', NULL, 'Adelanto'),
(1066, 249, 'US', NULL, 'Adell'),
(1067, 249, 'US', NULL, 'Adelphi'),
(1068, 249, 'US', NULL, 'Adelphia'),
(1069, 249, 'US', NULL, 'Adena'),
(1070, 249, 'US', NULL, 'Adger'),
(1071, 249, 'US', NULL, 'Adin'),
(1072, 249, 'US', NULL, 'Adirondack'),
(1073, 249, 'US', NULL, 'Adjuntas'),
(1074, 249, 'US', NULL, 'Adkins'),
(1075, 249, 'US', NULL, 'Admire'),
(1076, 249, 'US', NULL, 'Adna'),
(1077, 249, 'US', NULL, 'Adolph'),
(1078, 249, 'US', NULL, 'Adolphus'),
(1079, 249, 'US', NULL, 'Adona'),
(1080, 249, 'US', NULL, 'Adrian'),
(1081, 249, 'US', NULL, 'Advance'),
(1082, 249, 'US', NULL, 'Advent'),
(1083, 249, 'US', NULL, 'Afton'),
(1084, 249, 'US', NULL, 'Agar'),
(1085, 249, 'US', NULL, 'Agate'),
(1086, 249, 'US', NULL, 'Agawam'),
(1087, 249, 'US', NULL, 'Agency'),
(1088, 249, 'US', NULL, 'Agenda'),
(1089, 249, 'US', NULL, 'Ages Brookside'),
(1090, 249, 'US', NULL, 'Agness'),
(1091, 249, 'US', NULL, 'Agoura Hills'),
(1092, 249, 'US', NULL, 'Agra'),
(1093, 249, 'US', NULL, 'Agua Dulce'),
(1094, 249, 'US', NULL, 'Aguada'),
(1095, 249, 'US', NULL, 'Aguadilla'),
(1096, 249, 'US', NULL, 'Aguanga'),
(1097, 249, 'US', NULL, 'Aguas Buenas'),
(1098, 249, 'US', NULL, 'Aguila'),
(1099, 249, 'US', NULL, 'Aguilar'),
(1100, 249, 'US', NULL, 'Aguirre'),
(1101, 249, 'US', NULL, 'Ah Gwah Ching'),
(1102, 249, 'US', NULL, 'Ahmeek'),
(1103, 249, 'US', NULL, 'Ahoskie'),
(1104, 249, 'US', NULL, 'Ahsahka'),
(1105, 249, 'US', NULL, 'Ahwahnee'),
(1106, 249, 'US', NULL, 'Aibonito'),
(1107, 249, 'US', NULL, 'Aiea'),
(1108, 249, 'US', NULL, 'Aiken'),
(1109, 249, 'US', NULL, 'Ailey'),
(1110, 249, 'US', NULL, 'Aimwell'),
(1111, 249, 'US', NULL, 'Ainsworth'),
(1112, 249, 'US', NULL, 'Airville'),
(1113, 249, 'US', NULL, 'Airway Heights'),
(1114, 249, 'US', NULL, 'Aitkin'),
(1115, 249, 'US', NULL, 'Ajo'),
(1116, 249, 'US', NULL, 'Akaska'),
(1117, 249, 'US', NULL, 'Akeley'),
(1118, 249, 'US', NULL, 'Akers'),
(1119, 249, 'US', NULL, 'Akiachak'),
(1120, 249, 'US', NULL, 'Akiak'),
(1121, 249, 'US', NULL, 'Akin'),
(1122, 249, 'US', NULL, 'Akron'),
(1123, 249, 'US', NULL, 'Akutan'),
(1124, 249, 'US', NULL, 'Alabaster'),
(1125, 249, 'US', NULL, 'Alachua'),
(1126, 249, 'US', NULL, 'Aladdin'),
(1127, 249, 'US', NULL, 'Alakanuk'),
(1128, 249, 'US', NULL, 'Alamance'),
(1129, 249, 'US', NULL, 'Alameda'),
(1130, 249, 'US', NULL, 'Alamo'),
(1131, 249, 'US', NULL, 'Alamogordo'),
(1132, 249, 'US', NULL, 'Alamosa'),
(1133, 249, 'US', NULL, 'Alanreed'),
(1134, 249, 'US', NULL, 'Alanson'),
(1135, 249, 'US', NULL, 'Alapaha'),
(1136, 249, 'US', NULL, 'Alba'),
(1137, 249, 'US', NULL, 'Albany'),
(1138, 249, 'US', NULL, 'Albemarle'),
(1139, 249, 'US', NULL, 'Albers'),
(1140, 249, 'US', NULL, 'Albert'),
(1141, 249, 'US', NULL, 'Albert City'),
(1142, 249, 'US', NULL, 'Albert Lea'),
(1143, 249, 'US', NULL, 'Alberta'),
(1144, 249, 'US', NULL, 'Alberton'),
(1145, 249, 'US', NULL, 'Albertson'),
(1146, 249, 'US', NULL, 'Albertville'),
(1147, 249, 'US', NULL, 'Albia'),
(1148, 249, 'US', NULL, 'Albin'),
(1149, 249, 'US', NULL, 'Albion'),
(1150, 249, 'US', NULL, 'Alborn'),
(1151, 249, 'US', NULL, 'Albright'),
(1152, 249, 'US', NULL, 'Albrightsville'),
(1153, 249, 'US', NULL, 'Albuquerque'),
(1154, 249, 'US', NULL, 'Alburgh'),
(1155, 249, 'US', NULL, 'Alburnett'),
(1156, 249, 'US', NULL, 'Alburtis'),
(1157, 249, 'US', NULL, 'Alcalde'),
(1158, 249, 'US', NULL, 'Alcester'),
(1159, 249, 'US', NULL, 'Alcoa'),
(1160, 249, 'US', NULL, 'Alcolu'),
(1161, 249, 'US', NULL, 'Alcova'),
(1162, 249, 'US', NULL, 'Alcove'),
(1163, 249, 'US', NULL, 'Alda'),
(1164, 249, 'US', NULL, 'Alden'),
(1165, 249, 'US', NULL, 'Alder'),
(1166, 249, 'US', NULL, 'Alder Creek'),
(1167, 249, 'US', NULL, 'Alderpoint'),
(1168, 249, 'US', NULL, 'Alderson'),
(1169, 249, 'US', NULL, 'Aldie'),
(1170, 249, 'US', NULL, 'Aldrich'),
(1171, 249, 'US', NULL, 'Aledo'),
(1172, 249, 'US', NULL, 'Aleknagik'),
(1173, 249, 'US', NULL, 'Aleppo'),
(1174, 249, 'US', NULL, 'Alex'),
(1175, 249, 'US', NULL, 'Alexander'),
(1176, 249, 'US', NULL, 'Alexander City'),
(1177, 249, 'US', NULL, 'Alexandria'),
(1178, 249, 'US', NULL, 'Alexandria Bay'),
(1179, 249, 'US', NULL, 'Alexis'),
(1180, 249, 'US', NULL, 'Alford'),
(1181, 249, 'US', NULL, 'Alfred'),
(4001, 241, 'TR', NULL, 'Istanbul'),
(4002, 241, 'TR', NULL, 'Ankara'),
(4003, 241, 'TR', NULL, 'Izmir'),
(4004, 241, 'TR', NULL, 'Bursa'),
(4013, 5, 'DZ', 'dsf', 'sdfsf'),
(4014, 283, 'IND', 'Delhi', 'Delhi'),
(4015, 18, 'AQ', 'TR', 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(200) NOT NULL,
  `currency_usd_value` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=122 ;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `code`, `name`, `currency_usd_value`) VALUES
(118, 'USD', 'USD', 1.00),
(119, 'EUR', 'EUR', 10.00),
(120, 'TRY', 'TRY', 23.00),
(121, 'INR', 'INR', 56.00);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  `address` varchar(300) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `post_code` varchar(15) NOT NULL,
  PRIMARY KEY (`customer_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `title`) VALUES
(1, 'Abc'),
(2, 'Sales');

-- --------------------------------------------------------

--
-- Table structure for table `dorak_ratings`
--

CREATE TABLE IF NOT EXISTS `dorak_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `dorak_ratings`
--

INSERT INTO `dorak_ratings` (`id`, `title`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `dorak_users`
--

CREATE TABLE IF NOT EXISTS `dorak_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` int(11) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `dorak_users`
--

INSERT INTO `dorak_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `position`, `department`, `role`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, 'avDDT2YVYb8NiRt9JhrrBO', 1268889823, 1459923893, 1, 'Super User', '', 3, '', 0, 0, 2),
(2, '192.168.1.103', 'mahesh', '$2y$08$PlAQ6GNVBwpxt6n7HakDc.TiA4KjqsS8qVpiEMM6WW.5MKoXdxFu6', NULL, 'tisuser@gmail.com', ' ', NULL, NULL, NULL, 1456745025, 1457069584, 1, 'admin tis', 'tis', 5, '3124141415', 1, 1, 3),
(9, '192.168.1.103', NULL, '$2y$08$4.7D1jqEzDvm.qwzVN.YuuPClx1FlQ8KCCdc0yuGZ4T6zEKx7B3TK', NULL, 'mahes11hsal@gmail.com', NULL, NULL, NULL, NULL, 1456989138, 1457068024, 1, 'aaja re', 'ram', 8, '4325526462362', 2, 2, 2),
(11, '192.168.1.114', NULL, '$2y$08$WySATiqfstcqLB3tN7cp6.mqqspfy5WQQdJ7d.EI.cBEKL0S.EY76', NULL, 'erhergary23@yopmail.com', NULL, NULL, NULL, NULL, 1457004032, NULL, 1, 'egregreger', 'gregregre', 8, '6346346346', 2, 1, 5),
(12, '192.168.1.114', NULL, '$2y$08$XtsgId3pdHFxW3ci7M38N.Hdpf95eJcMQCdTNydTNri8jRmgd2YzK', NULL, 'garyfdbf23@yopmail.com', NULL, NULL, NULL, NULL, 1457004099, NULL, 1, 'dbfdbfdb', 'dfbdfb', 10, '22325325235235235325', 1, 1, 5),
(13, '192.168.1.114', NULL, '$2y$08$QIl/SSaSuKWn34XqgmPuiufwiO4ZFkvkR0jSQI6KOFMnFFucm8LEi', NULL, 'garwwy23@yopmail.com', NULL, NULL, NULL, NULL, 1457004127, NULL, 1, 'dbfdbfdb', 'dfbdfb', 8, '43523523532', 1, 1, 3),
(14, '192.168.1.114', NULL, '$2y$08$efTiwH3.5oui.OYTNMMiQ.JhATFc6Gf/q6ZKyplUurLa9sNGviTJK', NULL, 'gary2vdsvds3@yopmail.com', '037d37603f63f11ce6516ec529cb2c5c99814dcd', NULL, NULL, NULL, 1457004167, 1457067800, 0, 'gary', 'cgvbhnm', 8, '5325325325325', 2, 1, 6),
(15, '192.168.1.114', NULL, '$2y$08$jfms997GP6j0QBad.VYfcelkW1pnsWVPD7vagA6enaNp255tS/GkS', NULL, 'gary2vdghhsvds3@yopmail.com', '1ed3f860871f6e36ba90b916a6e961b9e3973c97', NULL, NULL, NULL, 1457004196, 1457067732, 0, 'gary', 'cgvbhnm', 11, '532325353253', 2, 1, 5),
(16, '192.168.1.77', NULL, '$2y$08$Ekv9wdMUzWHm4CUaMxgd2e0ycP9ANf7YZQ7I9jc0hpAA2XFZ4iBHK', NULL, 'dsgth@mail.com', NULL, NULL, NULL, NULL, 1457072938, NULL, 1, 'fsdg', 'gfsghthr', 11, '5468963210', 1, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `escalation`
--

CREATE TABLE IF NOT EXISTS `escalation` (
  `escalation_id` int(11) NOT NULL AUTO_INCREMENT,
  `estimate_id` int(11) NOT NULL,
  `estimated_hotels_id` int(11) NOT NULL,
  `estimated_price` decimal(6,2) NOT NULL,
  `negotiated_price` decimal(6,2) NOT NULL,
  `comments` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`escalation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `estimated_hotels`
--

CREATE TABLE IF NOT EXISTS `estimated_hotels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estimate_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `travel_type_id` int(11) NOT NULL,
  `property_type` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city` varchar(200) DEFAULT NULL,
  `hotal_star_rating` int(11) NOT NULL,
  `hotel_chain_id` int(11) NOT NULL,
  `adult` int(11) DEFAULT NULL,
  `child` int(11) DEFAULT NULL,
  `occ_per_room` int(11) DEFAULT NULL,
  `rooms` int(11) NOT NULL,
  `extra_adult` int(11) DEFAULT NULL,
  `extra_child` int(11) DEFAULT NULL,
  `check_in_date` datetime NOT NULL,
  `check_out_date` datetime NOT NULL,
  `created_on` datetime NOT NULL,
  `last_upated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `estimates`
--

CREATE TABLE IF NOT EXISTS `estimates` (
  `estimate_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_on` datetime NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`estimate_id`),
  KEY `estimate_id` (`estimate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE IF NOT EXISTS `facilities` (
  `facility_id` int(11) NOT NULL AUTO_INCREMENT,
  `facility_title` varchar(1000) NOT NULL,
  PRIMARY KEY (`facility_id`),
  KEY `facility_id` (`facility_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`facility_id`, `facility_title`) VALUES
(1, 'Wi-Fi'),
(2, 'Spa'),
(3, 'Non-smoking Rooms'),
(4, 'Conference'),
(5, 'Family Rooms'),
(6, 'Indoor Pool'),
(7, 'Parking'),
(8, 'Outdoor Pool'),
(9, 'Restaurant'),
(10, 'Airportffgfdg'),
(11, 'Pets Friendly'),
(12, 'Fitness Center'),
(13, 'Facilities for Disabled Guest'),
(14, 'room facility'),
(15, 'room   facility'),
(16, 'fac1'),
(17, 'h aa');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `gallery_id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_title` varchar(500) NOT NULL,
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`gallery_id`, `gallery_title`) VALUES
(1, 'Hotel Logo'),
(2, 'Exterior Gallery'),
(3, 'Interior Gallery'),
(4, 'Restaurant'),
(5, 'Kids Zone');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

CREATE TABLE IF NOT EXISTS `gallery_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `image_name` varchar(500) NOT NULL,
  `identifier` varchar(500) NOT NULL,
  `upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=114 ;

--
-- Dumping data for table `gallery_images`
--

INSERT INTO `gallery_images` (`id`, `gallery_id`, `hotel_id`, `image_name`, `identifier`, `upload_date`) VALUES
(47, 5, 100, 'Tulips.jpg', '', '2016-03-19 11:40:43'),
(48, 5, 100, 'Chrysanthemum.jpg', '', '2016-03-19 11:40:43'),
(51, 2, 63, 'attachment-iconfgdg.png', 'd8b27fd09cc017e5d509e7a5efa02080_1458536740', '2016-03-21 05:36:17'),
(60, 1, 63, 'Desert.jpg', '79e3e907464c35a122a3eb77b3e5ce2c_1458548778', '2016-03-21 08:26:38'),
(61, 1, 63, 'Hydrangeas.jpg', '79e3e907464c35a122a3eb77b3e5ce2c_1458548778', '2016-03-21 08:26:39'),
(62, 1, 63, 'Jellyfish.jpg', '79e3e907464c35a122a3eb77b3e5ce2c_1458548778', '2016-03-21 08:26:39'),
(63, 1, 63, 'Koala.jpg', '79e3e907464c35a122a3eb77b3e5ce2c_1458548778', '2016-03-21 08:26:40'),
(64, 1, 63, 'Lighthouse.jpg', '79e3e907464c35a122a3eb77b3e5ce2c_1458548778', '2016-03-21 08:26:40'),
(65, 1, 63, 'Tulips.jpg', '79e3e907464c35a122a3eb77b3e5ce2c_1458548778', '2016-03-21 08:26:41'),
(67, 1, 63, 'Desert.jpg', '79e3e907464c35a122a3eb77b3e5ce2c_1458548778', '2016-03-21 08:26:47'),
(68, 1, 63, 'Hydrangeas.jpg', '79e3e907464c35a122a3eb77b3e5ce2c_1458548778', '2016-03-21 08:26:51'),
(69, 1, 63, 'Jellyfish.jpg', '79e3e907464c35a122a3eb77b3e5ce2c_1458548778', '2016-03-21 08:26:51'),
(70, 1, 63, 'Koala.jpg', '79e3e907464c35a122a3eb77b3e5ce2c_1458548778', '2016-03-21 08:26:52'),
(71, 1, 63, 'Lighthouse.jpg', '79e3e907464c35a122a3eb77b3e5ce2c_1458548778', '2016-03-21 08:26:52'),
(72, 1, 63, 'Tulips.jpg', '79e3e907464c35a122a3eb77b3e5ce2c_1458548778', '2016-03-21 08:26:53'),
(73, 1, 63, 'blog-advert.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630739', '2016-03-22 07:12:48'),
(74, 1, 63, '442672_90401332.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:15:35'),
(76, 1, 63, 'img_3158.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:15:37'),
(77, 1, 63, 'iStock_000003390628Medium.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:15:37'),
(79, 1, 63, 'img_3158.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:15:50'),
(80, 1, 63, 'iStock_000003390628Medium.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:15:51'),
(81, 1, 63, 'iStock_000010649093Medium.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:15:51'),
(82, 1, 63, 'vintage bedding without throw.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:15:51'),
(83, 1, 63, '442672_90401332.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:16:16'),
(85, 1, 63, 'img_3158.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:16:17'),
(86, 1, 63, 'iStock_000003390628Medium.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:16:17'),
(87, 1, 63, 'iStock_000010649093Medium.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:16:18'),
(88, 1, 63, 'vintage bedding without throw.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:16:18'),
(89, 1, 63, '442672_90401332.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:17:58'),
(91, 1, 63, 'img_3158.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:17:59'),
(92, 1, 63, 'iStock_000003390628Medium.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:18:00'),
(93, 1, 63, 'iStock_000010649093Medium.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:18:00'),
(94, 1, 63, 'vintage bedding without throw.jpg', '3a38e50b008e63596f6d9975b8aa77d6_1458630918', '2016-03-22 07:18:01'),
(110, 1, 146, 'Desert.jpg', 'adfa8ef0ef4e7a0d7c02468fabb9d26e_1458652893', '2016-03-22 13:21:51'),
(111, 1, 146, 'Hydrangeas.jpg', 'adfa8ef0ef4e7a0d7c02468fabb9d26e_1458652893', '2016-03-22 13:21:53'),
(112, 1, 146, 'Jellyfish.jpg', 'adfa8ef0ef4e7a0d7c02468fabb9d26e_1458652893', '2016-03-22 13:21:53'),
(113, 1, 138, 'Airbus_Pleiades_50cm_8bit_RGB_Yogyakarta.jpg', 'f72e7530efb771702c39970e157509f2_1458658701', '2016-03-22 16:38:36');

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE IF NOT EXISTS `hotels` (
  `hotel_id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_code` varchar(500) DEFAULT NULL,
  `chain_id` int(11) DEFAULT NULL COMMENT 'hotel chain id from hotel chain',
  `purpose` varchar(200) DEFAULT NULL,
  `hotel_name` varchar(500) NOT NULL,
  `hotel_address` varchar(300) NOT NULL,
  `city` varchar(200) NOT NULL,
  `country_code` varchar(50) NOT NULL,
  `district` varchar(100) DEFAULT NULL,
  `post_code` varchar(20) DEFAULT NULL,
  `hotel_image` varchar(200) DEFAULT NULL,
  `currency` varchar(8) NOT NULL,
  `star_rating` int(11) NOT NULL,
  `dorak_rating` int(11) DEFAULT NULL,
  `commisioned` tinyint(1) NOT NULL,
  `child_age_group_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_byuser` int(11) NOT NULL,
  PRIMARY KEY (`hotel_id`),
  KEY `chain_id` (`chain_id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=161 ;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`hotel_id`, `hotel_code`, `chain_id`, `purpose`, `hotel_name`, `hotel_address`, `city`, `country_code`, `district`, `post_code`, `hotel_image`, `currency`, `star_rating`, `dorak_rating`, `commisioned`, `child_age_group_id`, `status`, `last_updated`, `created_byuser`) VALUES
(102, 'DRK-102-safa-Ank-TR', 5, '2', 'safagagag gdsgsg DDD DSSADAD DDD FF DFF ss', 'GSDGSGSGS GDSGSHS GDSGSGSggggllyes', 'Ankara', 'TR', '', '634636', NULL, 'USD', 0, 0, 0, 2, 1, '2016-04-01 11:10:51', 1),
(110, 'DRK-110-test-Ank-TR', 0, '', 'test for zip filegggggsdfsdfsdfsf', 'GSDGSGSGS GDSGSHS GDSGSGS', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 04:54:34', 1),
(111, 'DRK-111-test-Ank-TR', 0, '', 'test for zip filegggggsdfsdfsdfsfgggg', 'GSDGSGSGS GDSGSHS GDSGSGS', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 04:57:44', 1),
(112, 'DRK-112-test-Ank-TR', 0, '', 'test for zip filezxcazxcazdf', 'sdkjhfksjhdfkjshkjdfhkjshdfjhsjdfhjskdfhjsk jkshdfjhsdf', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 05:08:07', 1),
(113, 'DRK-113-test-Ank-TR', 0, '', 'test for zip filedfsdfsdfsdf', 'GSDGSGSGS GDSGSHS GDSGSGS', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 05:13:14', 1),
(114, 'DRK-114-test-Ank-TR', 0, '', 'test for zip filedsfsdfsdfs', 'GSDGSGSGS GDSGSHS GDSGSGS', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 05:16:33', 1),
(115, 'DRK-115-test-Ank-TR', 0, '', 'test for zip filedfgdgfdgfdgffg', 'sdkjhfksjhdfkjshkjdfhkjshdfjhsjdfhjskdfhjsk jkshdfjhsdftttm', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-31 10:48:28', 1),
(116, 'DRK-116-test-Ank-TR', 0, '', 'test for zip filesdfsdfsdf', 'sdkjhfksjhdfkjshkjdfhkjshdfjhsjdfhjskdfhjsk jkshdfjhsdf', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 05:27:12', 1),
(117, 'DRK-117-test-Ank-TR', 0, '', 'test for zip filesdfszdfsdf', 'GSDGSGSGS GDSGSHS GDSGSGS', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 05:37:22', 1),
(118, 'DRK-118-test-Ank-TR', 0, '', 'test for zip filesdfszdfsdfdgfedsgfdfg', 'GSDGSGSGS GDSGSHS GDSGSGS', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 05:38:29', 1),
(119, 'DRK-119-test-Ank-TR', 0, '', 'test for zip fileafdsdfsdfsdfsdf', 'sdkjhfksjhdfkjshkjdfhkjshdfjhsjdfhjskdfhjsk jkshdfjhsdf', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 05:49:55', 1),
(120, 'DRK-120-test-Ank-TR', 0, '', 'test for zip filesgfdsgdsg', 'GSDGSGSGS GDSGSHS GDSGSGS', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 05:54:40', 1),
(121, 'DRK-121-test-Ank-TR', 0, '', 'test for zip filesgadsafdsdfsdf', 'GSDGSGSGS GDSGSHS GDSGSGS', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 05:56:02', 1),
(122, 'DRK-122-hote-DL-IND', 13, '1', 'hotel2 3 aaa', 'hotel2 3 aaa', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 3, 5, 0, NULL, 2, '2016-03-11 06:19:33', 1),
(123, 'DRK-123-hote-DL-IND', 15, '5', 'hotel 5 cea a', 'hotel 5 cea a', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 3, 5, 0, NULL, 2, '2016-03-11 06:19:33', 1),
(124, 'DRK-124-hote-DL-IND', 5, '1', 'hotel ram kum', 'hotel ram kum', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 3, 5, 0, NULL, 2, '2016-03-11 06:20:12', 1),
(125, 'DRK-125-test-Ank-TR', 0, '', 'test for zip filesdfsdfsdfsdf', 'sdkjhfksjhdfkjshkjdfhkjshdfjhsjdfhjskdfhjsk jkshdfjhsdf', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 06:34:23', 1),
(126, 'DRK-126-test-Ank-TR', 0, '', 'test for zip filesdfasdfasdfsdf', 'GSDGSGSGS GDSGSHS GDSGSGS', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 06:52:52', 1),
(127, 'DRK-127- Aaj-DL-IND', 5, '1', ' Aajhtak', ' Aajhtak', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 3, 5, 0, NULL, 2, '2016-03-11 09:13:30', 1),
(128, 'DRK-128-hote-DL-IND', 13, '6', 'hotel rdff', 'hotel rdff', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 3, 5, 0, NULL, 2, '2016-03-11 09:13:30', 1),
(129, 'DRK-129- Aaj-DL-IND', 5, '1', ' Aajhtak today', ' Aajhtak today', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 3, 5, 0, NULL, 2, '2016-03-11 09:41:44', 1),
(130, 'DRK-130-test-Ank-TR', 0, '', 'test for zip filesdfsdfsdfsdfggfd', 'sdkjhfksjhdfkjshkjdfhkjshdfjhsjdfhjskdfhjsk jkshdfjhsdf', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-11 11:45:20', 1),
(131, 'DRK-131-test-Ank-TR', 0, '', 'test for zip filegggggsdfsdfsdfsfggggdfdf', 'sdkjhfksjhdfkjshkjdfhkjshdfjhsjdfhjskdfhjsk jkshdfjhsdf', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-12 04:39:23', 1),
(132, 'DRK-132-test-Ank-TR', 0, '', 'test for zip filesdfsdfsdfsdfdffd', 'sdkjhfksjhdfkjshkjdfhkjshdfjhsjdfhjskdfhjsk jkshdfjhsdf', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, NULL, 1, '2016-03-12 04:45:12', 1),
(133, 'DRK-133- Aaj-DL-IND', 5, '1', ' Aajhtak today as', ' Aajhtak today as', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 3, 5, 0, NULL, 2, '2016-03-12 12:25:05', 1),
(134, 'DRK-134-hote-DL-IND', 15, '5', 'hotel ajad hind', 'hotel ajad hind', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 3, 5, 0, NULL, 2, '2016-03-14 04:52:07', 1),
(135, 'DRK-135- Aaj-DL-IND', 5, '1', ' Aajhtak today pp', ' Aajhtak today pp', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 3, 5, 0, NULL, 2, '2016-03-14 04:54:05', 1),
(136, 'DRK-136-hote-DL-IND', 15, '5', 'hotel ajad hind dd', 'hotel ajad hind dd', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 3, 5, 0, NULL, 2, '2016-03-14 04:54:11', 1),
(137, 'DRK-137- Aaj-DL-IND', 5, '1', ' Aajhtak today hp', ' Aajhtak today hp', 'Delhi', 'IND', '', '201301', NULL, 'USD', 0, 5, 0, 2, 2, '2016-03-16 07:57:16', 1),
(138, 'DRK-138-hote-DL-IND', 15, '7', 'hotel hindfoj', 'hotel hindfoj', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 3, 5, 0, NULL, 2, '2016-03-14 05:24:56', 1),
(140, 'DRK-140-hote-DL-IND', 15, '7', 'hotel hindfoj ggg', 'hotel hindfoj gggergtfdgfdfg', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 3, 5, 0, NULL, 2, '2016-03-21 07:40:22', 1),
(141, 'DRK-141-Asho-DL-IND', 15, '7', 'Ashoka Hotel  march ', 'c 81 c sec 8 noida', 'Delhi', 'IND', 'Delhi', '201301', NULL, 'USD', 2, 2, 0, 2, 1, '2016-03-22 04:11:08', 1),
(142, 'DRK-142-sdfs-Ank-TR', 0, '', 'sdfs sgsg   gdsgsgsdg gsgsg', 'sdgsg gsdgsgsd fgsgfsdgds sdgsdgdsg', 'Ankara', 'TR', '', '3421414', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-22 10:00:18', 1),
(143, 'DRK-143-Asho-Ank-TR', 0, '', 'Ashoka Hotel  fasdafaf', 'fasf  fgsdgs ggtertehe ertwywy', 'Ankara', 'TR', '', '3421414', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-22 10:02:29', 1),
(144, 'DRK-144-Asho-Ank-TR', 0, '', 'Ashoka Hotel  fasdafaf dfafa', 'fasf  fgsdgs ggtertehe ertwywy fasfa', 'Ankara', 'TR', '', '3421414', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-22 10:02:58', 1),
(145, 'DRK-145-Asho-Ank-TR', 0, '', 'Ashoka Hotel  fasdafaf dfafa gfd', 'fasf  fgsdgs ggtertehe ertwywy fasfa', 'Ankara', 'TR', '', '3421414', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-22 10:03:23', 1),
(146, 'DRK-146-Asho-Ank-TR', 0, '', 'Ashoka Hotel  fasdafaf dfafa gfd sadad', 'fasf  fgsdgs ggtertehe ertwywy fasfa', 'Ankara', 'TR', '', '3421414', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-22 10:04:02', 1),
(147, 'DRK-147-Asha-Ank-TR', 0, '', 'Ashasfafaf fgasgdgasggasd', 'fasf  fgsdgs ggtertehe ertwywy fasfa', 'Ankara', 'TR', '', '3421414', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-22 10:04:37', 1),
(148, 'DRK-148-Asha-Ank-TR', 0, '', 'Ashasfafaf fgasgdgasggasddf', 'fasf  fgsdgs ggtertehe ertwywy fasfa', 'Ankara', 'TR', '', '3421414', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-22 10:09:38', 1),
(149, 'DRK-149-sdfs-400-TR', 0, '5', 'sdfsfdadfdfgghgdtgretyrtytyt', 'GSDGSGSGS GDSGSHS GDSGSGS', '4001', 'TR', '', '201309', NULL, 'INR', 0, 0, 0, 0, 3, '2016-03-29 05:05:03', 1),
(150, 'DRK-150-sdfa-400-TR', 0, '1', 'sdfafagagsagaga mmm', 'gasgsagsaga vsafgagsagag vagagag', '4001', 'TR', '', '43515151', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-29 05:05:13', 1),
(151, 'DRK-151-dgfd-400-TR', 0, '', 'dgfdsgghjjjjjjjjj', 'GSDGSGSGS GDSGSHS GDSGSGS', '4001', 'TR', '', '201309', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-29 05:08:28', 1),
(152, 'DRK-152-New -400-TR', 5, '7', 'New hotel demo', 'fasfasfsdfsfsfsfasfsfs', '4004', 'TR', '', '5555', NULL, 'USD', 3, 1, 0, 0, 1, '2016-03-29 10:13:53', 1),
(153, 'DRK-153-test-Ank-TR', 5, '7', 'testing hotel', 'testing hotel address dummy', 'Ankara', 'TR', '', '8985', NULL, 'EUR', 1, 1, 0, 0, 1, '2016-03-29 10:46:21', 1),
(154, 'DRK-154-firs-Ank-TR', 0, '5', 'first testsdfsdfsdfsdfsdf', 'GSDGSGSGS GDSGSHS GDSGSGS', 'Ankara', 'TR', '', '201301', NULL, 'INR', 0, 1, 0, 0, 2, '2016-03-30 11:35:08', 1),
(155, 'DRK-155-Asho-Ist-TR', 0, '', 'Ashoka Hotel  sdda', 'c 81 c dsgsgds fdggag', 'Istanbul', 'TR', 'Abc', '201301', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-30 11:44:32', 1),
(156, 'DRK-156-Rest-Ank-TR', 0, '5', 'Restaurants name upto ten  charactersddddd', 'GSDGSGSGS GDSGSHS GDSGSGS', 'Ankara', 'TR', '', '201309', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-30 11:51:20', 1),
(157, 'DRK-157-asva-Ank-TR', 0, '', 'asvasvsavsaavsavsavas', 'vsavasvsavsavsvsaavvsavvsavsav', 'Ankara', 'TR', '', '345345', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-30 12:08:25', 1),
(158, 'DRK-158-dgsd-Ank-TR', 0, '', 'dgsdadsdsgaf', 'sgfsdgfsdfsfafsgfdfdgfdgdfhgfhggffddfdsfdsfdsffdsfdsfds', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-31 11:29:11', 1),
(159, 'DRK-159-dfag-Ank-TR', 0, '', 'dfagsdfbgfsfsffdfgdggddggggf', 'sdkjhfksjhdfkjshkjdfhkjshdfjhsjdfhjskdfhjsk jkshdfjhsdfy', 'Ankara', 'TR', '', '201301', NULL, 'USD', 0, 0, 0, 0, 1, '2016-03-31 13:19:51', 1),
(160, 'DRK-160-asda-Ank-TR', 28, '5', 'asdasdasfddsf', 'adsadsafdsdfsdff', 'Ankara', 'TR', '', '12036', NULL, 'INR', 2, 1, 0, 0, 2, '2016-03-31 14:07:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_bank_accounts`
--

CREATE TABLE IF NOT EXISTS `hotel_bank_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `iban_code` varchar(100) DEFAULT NULL,
  `account_number` int(11) NOT NULL,
  `account_name` varchar(300) NOT NULL,
  `bank_name` varchar(500) NOT NULL,
  `bank_address` varchar(300) NOT NULL,
  `branch_name` varchar(300) DEFAULT NULL,
  `branch_code` varchar(100) DEFAULT NULL,
  `bank_ifsc_code` varchar(100) NOT NULL,
  `swift_code` varchar(200) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_bank_accounts_hotels_idx` (`hotel_id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=128 ;

--
-- Dumping data for table `hotel_bank_accounts`
--

INSERT INTO `hotel_bank_accounts` (`id`, `hotel_id`, `iban_code`, `account_number`, `account_name`, `bank_name`, `bank_address`, `branch_name`, `branch_code`, `bank_ifsc_code`, `swift_code`, `status`, `last_updated`) VALUES
(68, 124, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-11 06:20:12'),
(69, 127, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-11 09:13:30'),
(70, 129, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-11 09:41:45'),
(71, 129, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-12 12:21:52'),
(72, 129, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-12 12:22:28'),
(73, 129, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-12 12:23:58'),
(87, 133, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 04:52:05'),
(94, 135, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 04:54:09'),
(95, 135, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 05:22:14'),
(96, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 05:24:56'),
(97, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 05:36:47'),
(98, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 05:53:52'),
(99, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 06:00:35'),
(100, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 06:02:09'),
(101, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 06:03:37'),
(102, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 06:04:02'),
(103, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 06:05:02'),
(104, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 06:05:24'),
(105, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 06:07:08'),
(106, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 06:11:07'),
(107, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 06:11:49'),
(108, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 06:13:02'),
(109, 137, 'asdfsdg', 454125315, 'mmmmm', 'bbbbb', 'dfgsdg', 'branch Name', 'gfdhd', 'ddgyyy', 'sss', 0, '2016-03-14 09:23:29'),
(113, 141, '253252', 235252525, 'fafaf', 'fafasf', 'asgagagdsga', 'gsdag', 'ggasgd', 'gdsaga', 'gadsgasg', 0, '2016-03-22 05:35:30'),
(114, 113, 'fasfeasa', 2147483647, 'dgsgdsg', 'sdgsgsg', 'sdgsgdsg sdgsgsg', 'gsdgsg', 'gsdgds', 'gdsgs', 'gsdgs', 0, '2016-03-22 09:55:44'),
(125, 148, 'sdgdsg', 2147483647, 'sdgsggdsgsg', 'gsdgsg', 'gsdgsgsg gfsdgsg gsdagsag', '', '', '', '', 0, '2016-03-22 10:36:47'),
(126, 102, '906', 12354, 'kohli', 'sbi', 'agra uttar pradesh india', 'agra', 'sbi123', 'SBI1234568723', 'SBI67890332', 0, '2016-03-31 11:21:04'),
(127, 159, '977', 12354, 'kohli', 'sbi', 'agra uttar pradesh india', 'agra', 'sbi123', 'SBI1234568723', 'SBI67890332', 0, '2016-03-31 13:21:32');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_cancellation`
--

CREATE TABLE IF NOT EXISTS `hotel_cancellation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `seasion` varchar(20) NOT NULL COMMENT 'low and high',
  `cancelled_before` int(11) NOT NULL,
  `payment_request` decimal(10,0) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=176 ;

--
-- Dumping data for table `hotel_cancellation`
--

INSERT INTO `hotel_cancellation` (`id`, `hotel_id`, `seasion`, `cancelled_before`, `payment_request`, `last_updated`) VALUES
(2, 63, 'high', 5, 20, '2016-03-15 10:46:31'),
(5, 63, 'high', 5, 20, '2016-03-15 10:46:31'),
(49, 68, 'low', 5, 10, '2016-03-15 10:46:31'),
(50, 68, 'high', 5, 20, '2016-03-15 10:46:31'),
(51, 68, 'low', 5, 10, '2016-03-15 10:46:31'),
(52, 68, 'low', 5, 10, '2016-03-15 10:46:31'),
(53, 68, 'high', 5, 20, '2016-03-15 10:46:31'),
(54, 68, 'low', 5, 10, '2016-03-15 10:46:31'),
(55, 68, 'low', 5, 10, '2016-03-15 10:46:31'),
(56, 68, 'high', 5, 20, '2016-03-15 10:46:31'),
(57, 68, 'low', 5, 10, '2016-03-15 10:46:31'),
(58, 68, 'low', 5, 10, '2016-03-15 10:46:31'),
(59, 68, 'high', 5, 20, '2016-03-15 10:46:31'),
(60, 68, 'low', 5, 10, '2016-03-15 10:46:31'),
(61, 71, 'low', 5, 10, '2016-03-15 10:46:31'),
(62, 71, 'high', 5, 20, '2016-03-15 10:46:31'),
(63, 71, 'low', 5, 10, '2016-03-15 10:46:31'),
(64, 74, 'low', 5, 10, '2016-03-15 10:46:31'),
(65, 74, 'high', 5, 20, '2016-03-15 10:46:31'),
(66, 74, 'low', 5, 10, '2016-03-15 10:46:31'),
(67, 74, 'low', 5, 10, '2016-03-15 10:46:31'),
(68, 74, 'high', 5, 20, '2016-03-15 10:46:31'),
(69, 74, 'low', 5, 10, '2016-03-15 10:46:31'),
(70, 77, 'low', 5, 10, '2016-03-15 10:46:31'),
(71, 77, 'high', 5, 20, '2016-03-15 10:46:31'),
(72, 77, 'low', 5, 10, '2016-03-15 10:46:31'),
(73, 77, 'low', 5, 10, '2016-03-15 10:46:31'),
(74, 77, 'high', 5, 20, '2016-03-15 10:46:31'),
(75, 77, 'low', 5, 10, '2016-03-15 10:46:31'),
(76, 78, 'low', 5, 10, '2016-03-15 10:46:31'),
(77, 78, 'high', 5, 20, '2016-03-15 10:46:31'),
(78, 78, 'low', 5, 10, '2016-03-15 10:46:31'),
(79, 84, 'low', 5, 10, '2016-03-15 10:46:31'),
(80, 84, 'high', 5, 20, '2016-03-15 10:46:31'),
(81, 84, 'low', 5, 10, '2016-03-15 10:46:31'),
(82, 87, 'low', 5, 10, '2016-03-15 10:46:31'),
(83, 87, 'high', 5, 20, '2016-03-15 10:46:31'),
(84, 87, 'low', 5, 10, '2016-03-15 10:46:31'),
(85, 78, 'low', 5, 10, '2016-03-15 10:46:31'),
(86, 78, 'high', 5, 20, '2016-03-15 10:46:31'),
(87, 78, 'low', 5, 10, '2016-03-15 10:46:31'),
(88, 78, 'low', 5, 10, '2016-03-15 10:46:31'),
(89, 78, 'high', 5, 20, '2016-03-15 10:46:31'),
(90, 78, 'low', 5, 10, '2016-03-15 10:46:31'),
(91, 78, 'low', 5, 10, '2016-03-15 10:46:31'),
(92, 78, 'high', 5, 20, '2016-03-15 10:46:31'),
(93, 78, 'low', 5, 10, '2016-03-15 10:46:31'),
(94, 78, 'low', 5, 10, '2016-03-15 10:46:31'),
(95, 78, 'high', 5, 20, '2016-03-15 10:46:31'),
(96, 78, 'low', 5, 10, '2016-03-15 10:46:31'),
(97, 124, 'low', 5, 10, '2016-03-15 10:46:31'),
(98, 124, 'high', 5, 20, '2016-03-15 10:46:31'),
(99, 124, 'low', 5, 10, '2016-03-15 10:46:31'),
(100, 127, 'low', 5, 10, '2016-03-15 10:46:31'),
(101, 127, 'high', 5, 20, '2016-03-15 10:46:31'),
(108, 129, 'low', 5, 10, '2016-03-15 10:46:31'),
(109, 129, 'high', 5, 20, '2016-03-15 10:46:31'),
(127, 133, 'high', 5, 20, '2016-03-15 10:46:31'),
(128, 133, 'low', 5, 10, '2016-03-15 10:46:31'),
(129, 133, 'high', 5, 20, '2016-03-15 10:46:31'),
(130, 133, 'low', 5, 10, '2016-03-15 10:46:31'),
(131, 133, 'low', 5, 10, '2016-03-15 10:46:31'),
(132, 133, 'high', 5, 20, '2016-03-15 10:46:31'),
(133, 133, 'low', 5, 10, '2016-03-15 10:46:31'),
(134, 133, 'low', 5, 10, '2016-03-15 10:46:31'),
(135, 133, 'high', 5, 20, '2016-03-15 10:46:31'),
(136, 133, 'low', 5, 10, '2016-03-15 10:46:31'),
(137, 133, 'low', 5, 10, '2016-03-15 10:46:31'),
(138, 133, 'high', 5, 20, '2016-03-15 10:46:31'),
(139, 133, 'low', 5, 10, '2016-03-15 10:46:31'),
(140, 133, 'low', 5, 10, '2016-03-15 10:46:31'),
(141, 133, 'high', 5, 20, '2016-03-15 10:46:31'),
(142, 133, 'low', 5, 10, '2016-03-15 10:46:31'),
(143, 133, 'low', 5, 10, '2016-03-15 10:46:31'),
(144, 133, 'high', 5, 20, '2016-03-15 10:46:31'),
(145, 133, 'low', 5, 10, '2016-03-15 10:46:31'),
(166, 135, 'low', 5, 10, '2016-03-15 10:46:31'),
(167, 135, 'high', 5, 20, '2016-03-15 10:46:31'),
(168, 135, 'low', 5, 10, '2016-03-15 10:46:31'),
(169, 137, 'low', 2, 23, '2016-03-16 07:44:43'),
(170, 137, 'high', 3, 29, '2016-03-16 07:44:43'),
(172, 64, 'low', 4, 53, '2016-03-16 09:24:15'),
(173, 64, 'high', 2, 22, '2016-03-16 09:24:15'),
(174, 141, 'low', 2, 10, '2016-03-22 04:11:09'),
(175, 141, 'high', 2, 15, '2016-03-22 04:11:09');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_chain_list`
--

CREATE TABLE IF NOT EXISTS `hotel_chain_list` (
  `chain_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`chain_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Store different Hotel Groups Name' AUTO_INCREMENT=29 ;

--
-- Dumping data for table `hotel_chain_list`
--

INSERT INTO `hotel_chain_list` (`chain_id`, `title`) VALUES
(5, 'Hilton cl'),
(13, 'Marriott'),
(14, 'reddison blue'),
(15, 'Hilton ds'),
(26, 'hgujhi'),
(28, 'dadsad');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_cmplimntry_room_excluded_dates`
--

CREATE TABLE IF NOT EXISTS `hotel_cmplimntry_room_excluded_dates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cmpl_room_id` int(11) NOT NULL,
  `exclude_date_from` datetime NOT NULL,
  `excluded_date_to` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=149 ;

--
-- Dumping data for table `hotel_cmplimntry_room_excluded_dates`
--

INSERT INTO `hotel_cmplimntry_room_excluded_dates` (`id`, `cmpl_room_id`, `exclude_date_from`, `excluded_date_to`) VALUES
(69, 68, '2016-03-15 00:00:00', '2016-03-16 00:00:00'),
(73, 72, '2016-03-15 00:00:00', '2016-03-16 00:00:00'),
(83, 80, '2016-03-15 00:00:00', '2016-03-16 00:00:00'),
(84, 80, '2016-03-15 00:00:00', '2016-03-16 00:00:00'),
(85, 80, '2016-03-16 00:00:00', '2016-03-17 00:00:00'),
(86, 80, '2016-03-15 00:00:00', '2016-03-16 00:00:00'),
(87, 80, '2016-03-16 00:00:00', '2016-03-17 00:00:00'),
(88, 80, '2016-03-15 00:00:00', '2016-03-16 00:00:00'),
(89, 80, '2016-03-16 00:00:00', '2016-03-17 00:00:00'),
(90, 80, '2016-03-15 00:00:00', '2016-03-16 00:00:00'),
(91, 80, '2016-03-16 00:00:00', '2016-03-17 00:00:00'),
(92, 80, '2016-03-15 00:00:00', '2016-03-16 00:00:00'),
(93, 80, '2016-03-16 00:00:00', '2016-03-17 00:00:00'),
(94, 80, '2016-03-15 00:00:00', '2016-03-16 00:00:00'),
(95, 80, '2016-03-16 00:00:00', '2016-03-17 00:00:00'),
(109, 82, '2016-03-15 00:00:00', '2016-03-16 00:00:00'),
(110, 82, '2016-03-16 00:00:00', '2016-03-17 00:00:00'),
(138, 96, '2016-03-16 00:00:00', '2016-03-17 00:00:00'),
(148, 99, '2016-03-26 00:00:00', '2016-03-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_complimentary_room`
--

CREATE TABLE IF NOT EXISTS `hotel_complimentary_room` (
  `cmpl_room_id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `room_night` int(11) NOT NULL,
  `room_type` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `upgrade` varchar(500) NOT NULL,
  PRIMARY KEY (`cmpl_room_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=102 ;

--
-- Dumping data for table `hotel_complimentary_room`
--

INSERT INTO `hotel_complimentary_room` (`cmpl_room_id`, `hotel_id`, `room_night`, `room_type`, `start_date`, `end_date`, `upgrade`) VALUES
(68, 127, 4, NULL, '2016-03-13 00:00:00', '2016-03-14 00:00:00', '1'),
(72, 129, 4, NULL, '2016-03-13 00:00:00', '2016-03-14 00:00:00', '1'),
(80, 133, 4, NULL, '2016-03-13 00:00:00', '2016-03-14 00:00:00', '1'),
(82, 135, 4, NULL, '2016-03-13 00:00:00', '2016-03-14 00:00:00', '1'),
(96, 137, 6, NULL, '2016-03-13 00:00:00', '2016-03-14 00:00:00', '1'),
(99, 141, 4, NULL, '2016-03-24 00:00:00', '2016-03-30 00:00:00', '1'),
(101, 64, 4, NULL, '2016-03-16 00:00:00', '2016-03-17 00:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_complimentary_services`
--

CREATE TABLE IF NOT EXISTS `hotel_complimentary_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `cmpl_service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=144 ;

--
-- Dumping data for table `hotel_complimentary_services`
--

INSERT INTO `hotel_complimentary_services` (`id`, `hotel_id`, `cmpl_service_id`) VALUES
(74, 124, 7),
(75, 127, 7),
(76, 129, 7),
(77, 129, 7),
(78, 129, 7),
(79, 129, 7),
(93, 133, 7),
(100, 135, 7),
(101, 135, 7),
(133, 137, 7),
(142, 141, 1),
(143, 141, 5);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_confrences`
--

CREATE TABLE IF NOT EXISTS `hotel_confrences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `confrence_name` varchar(500) NOT NULL,
  `area` decimal(10,0) DEFAULT NULL,
  `celling_height` varchar(500) DEFAULT NULL,
  `classroom` varchar(500) DEFAULT NULL,
  `theatre` varchar(500) DEFAULT NULL,
  `banquet` varchar(500) DEFAULT NULL,
  `reception` varchar(500) DEFAULT NULL,
  `conference` varchar(500) DEFAULT NULL,
  `ushape` varchar(500) DEFAULT NULL,
  `hshape` varchar(500) DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `hotel_confrences`
--

INSERT INTO `hotel_confrences` (`id`, `hotel_id`, `confrence_name`, `area`, `celling_height`, `classroom`, `theatre`, `banquet`, `reception`, `conference`, `ushape`, `hshape`, `last_updated`) VALUES
(1, 50, 'ccc', 44, '32', '232', '23', '4tgw', 'gsfg', 'sd', '3', '3', '2016-02-03 11:56:28');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_contact_details`
--

CREATE TABLE IF NOT EXISTS `hotel_contact_details` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `position` varchar(500) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `extension` varchar(5) NOT NULL,
  PRIMARY KEY (`contact_id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=679 ;

--
-- Dumping data for table `hotel_contact_details`
--

INSERT INTO `hotel_contact_details` (`contact_id`, `hotel_id`, `position`, `name`, `email`, `phone`, `extension`) VALUES
(397, 133, '3', 'n1', 'hr@tisindia.com', '34252525', '56341'),
(398, 133, '4', 'sales tis', 'sales@tinindia.com', '423536363', '552'),
(399, 133, '5', 'n3', 'salesq@tinindia.com', '4363636', '55'),
(400, 133, '6', 'n4', 'salesr@tinindia.com', '898707', '55'),
(401, 133, '7', 'n5', 'salesd@tinindia.com', '8070', '55'),
(402, 133, '8', 'n6', 'salesd@tinindia.com', '80755', '55'),
(403, 133, '9', 'n7', 'sales@tinaindia.com', '58599', '55'),
(411, 134, '3', 'n1gg', 'hrsss@tisindia.com', '2234252525', '4341'),
(412, 134, '5', 'n3', 'salesq@tinindia.com', '4363636', '55'),
(413, 134, '6', 'n4', 'salesr@tinindia.com', '898707', '55'),
(435, 135, '3', 'n1', 'hr@tisindia.com', '34252525', '56341'),
(436, 135, '4', 'sales tis', 'sales@tinindia.com', '423536363', '552'),
(437, 135, '5', 'n3', 'salesq@tinindia.com', '4363636', '55'),
(438, 135, '6', 'n4', 'salesr@tinindia.com', '898707', '55'),
(439, 135, '7', 'n5', 'salesd@tinindia.com', '8070', '55'),
(440, 135, '8', 'n6', 'salesd@tinindia.com', '80755', '55'),
(441, 135, '9', 'n7', 'sales@tinaindia.com', '58599', '55'),
(445, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(452, 135, '3', 'n1', 'hr@tisindia.com', '34252525', '56341'),
(453, 135, '4', 'sales tis', 'sales@tinindia.com', '423536363', '552'),
(454, 135, '5', 'n3', 'salesq@tinindia.com', '4363636', '55'),
(455, 135, '6', 'n4', 'salesr@tinindia.com', '898707', '55'),
(456, 135, '7', 'n5', 'salesd@tinindia.com', '8070', '55'),
(457, 135, '8', 'n6', 'salesd@tinindia.com', '80755', '55'),
(458, 135, '9', 'n7', 'sales@tinaindia.com', '58599', '55'),
(459, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(460, 136, '3', 'n1gg', 'hrsss@tisindia.com', '2234252525', '4341'),
(461, 136, '5', 'n3', 'salesq@tinindia.com', '4363636', '55'),
(462, 136, '6', 'n4', 'salesr@tinindia.com', '898707', '55'),
(463, 136, '7', 'n5', 'salesd@tinindia.com', '8070', '55'),
(471, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(483, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(495, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(496, 138, '10', 'n1gg', 'hrsss@tisindia.com', '2234252525', '4341'),
(497, 138, '11', 'n3', 'salesq@tinindia.com', '4363636', '55'),
(498, 138, '12', 'n4', 'salesr@tinindia.com', '898707', '55'),
(499, 138, '13', 'n5', 'salesd@tinindia.com', '8070', '55'),
(507, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(519, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(531, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(543, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(555, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(567, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(579, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(591, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(603, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(615, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(627, 128, '3', 'n16', 'hqqqr@tisindia.com', '34252525', '5341'),
(632, 137, 'abcd gh', 'asdfaf', 'mm@gmail.com', '4324525325363473', ''),
(633, 137, 'abcd gh', 'fdsh', 'mmm@gmail.com', '43512531515', ''),
(634, 137, 'C', 'fwrwerwrw', 'mmm@gmail.com', '3252525', ''),
(635, 137, 'abcd gh', 'safa', 'fasaf@gmail.com', '2352522626', ''),
(636, 137, 'Dt', 'sdafaf', 'mmm@gmail.com', '352525', ''),
(637, 137, 'Dt', 'sdafaf', 'mmm@gmail.com', '352525', ''),
(638, 137, 'abcd gh', 'safa', 'fasaf@gmail.com', '2352522626', ''),
(639, 137, 'C', 'sffafa', 'mm@gmail.com', '352525243636', ''),
(640, 137, 'C', 'gsgds', 'mmmm@gmail.com', '5334647538383', ''),
(641, 137, 'C', 'gdsgsg', 'mm@gmail.com', '431215156436622', ''),
(642, 137, 'C', 'tistest mahesh', 'mahesh@tisindiasupport.com', '+554141245252', ''),
(643, 137, 'C', 'mahesh', 'mahesh@tisindiasupport.com', '+554552553256', ''),
(645, 107, 'Hr', 'kohili', 'kohali@gmail.com', '1234567890', '108'),
(648, 140, 'Hr', 'kohili', 'kohali@gmail.com', '1234567890', '108'),
(649, 141, 'abcd gh', 'fersa', 'balu1@gmail.com', '+551245525532', ''),
(650, 113, 'C', 'tistest mahesh', 'mahesh@tisindiasupport.com', '+554141245252', ''),
(651, 142, 'abcd gh', 'tistest mahesh', 'mahesh@tisindiasupport.com', '+554141245252', ''),
(652, 143, 'Ct', 'tistest mahesh', 'mahesh@tisindiasupport.com', '+554141245252', ''),
(653, 144, 'abcd gh', 'tistest mahesh', 'mahesh@tisindiasupport.com', '+554141245252', ''),
(654, 145, 'abcd gh', 'tistest mahesh', 'mahesh@tisindiasupport.com', '+554141245252', ''),
(655, 146, 'abcd gh', 'tistest mahesh', 'mahesh@tisindiasupport.com', '+554141245252', ''),
(656, 147, 'abcd gh', 'tistest mahesh', 'mahesh@tisindiasupport.com', '+554141245252', ''),
(657, 148, 'Ct', 'tistest mahesh', 'mahesh@tisindiasupport.com', '+554141245252', ''),
(658, 149, 'C', 'kohili', 'kohali@gmail.com', '1234567890', '108'),
(659, 150, 'C', 'safaf', 'mmm@gmail.com', '232r32525252', '43252'),
(660, 151, 'C', 'kohili', 'test@gmail.com', '1234567890', '108'),
(662, 152, 'abcd gh', 'fsdfasf', 'fasfsaf@gmail.com', '5465464554654', '4546'),
(663, 153, 'abcd gh', 'test', 'test@gmail.com', '4465464656', '555'),
(664, 154, 'Hr', 'kohili', 'test@gmail.com', '1234567890', '108'),
(665, 155, 'C', 'mahesh', 'balu1@gmail.com', '+551245525532', ''),
(666, 156, 'Hr', 'kohili', 'test@gmail.com', '1234567890', '108'),
(668, 102, 'Hr', 'kohili', 'kohali@gmail.com', '1234567890', '12'),
(669, 157, 'abcd gh', 'asvsav', 'savas@dds.fgjg', '435435345345345', ''),
(670, 64, 'C', 'safaf', 'mmm@gmail.com', '232r32525252', ''),
(673, 118, 'C', 'werwr', 'werw@gmail.com', '32424234241', '123'),
(674, 115, 'Hr', 'kohili', 'test@gmail.com', '1234567890', '102'),
(675, 158, 'Hr', 'kohili', 'kohali@gmail.com', '1234567890', '108'),
(676, 159, 'Hr', 'kohili', 'test@gmail.com', '+91 -123 123 123 123', '108'),
(677, 117, 'abcd gh', 'test', 'test@gmail.com', '4465464656', '555'),
(678, 160, 'abcd gh', 'sdf', 'sf@sdf.com', '78965314239', '123');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_contract_info`
--

CREATE TABLE IF NOT EXISTS `hotel_contract_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `signed_by` varchar(500) NOT NULL,
  `contract_file` varchar(500) DEFAULT NULL,
  `creared_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=122 ;

--
-- Dumping data for table `hotel_contract_info`
--

INSERT INTO `hotel_contract_info` (`id`, `hotel_id`, `start_date`, `end_date`, `signed_by`, `contract_file`, `creared_on`, `status`) VALUES
(106, 137, '2016-03-18 00:00:00', '2016-03-31 00:00:00', 'tis india', 'Dorak_database_schema_after_sprint_2.pdf', '2016-03-16 07:57:16', 1),
(113, 141, '2016-03-24 00:00:00', '2016-03-31 00:00:00', 'tis india', 'Dorak_database_schema_after_sprint_23.pdf', '2016-03-22 04:11:09', 1),
(114, 149, '2016-03-30 00:00:00', '2016-04-09 00:00:00', 'restaurant team', '', '2016-03-30 12:49:24', 1),
(115, 102, '2016-03-31 00:00:00', '2016-04-09 00:00:00', 'restaurant team', '', '2016-03-31 07:49:59', 1),
(116, 102, '2016-03-31 00:00:00', '2016-04-01 00:00:00', 'jeevan', '', '2016-03-31 15:04:18', 1),
(117, 102, '2016-03-31 00:00:00', '2016-04-01 00:00:00', 'jeevan', '', '2016-03-31 15:07:14', 1),
(118, 102, '2016-03-31 00:00:00', '2016-04-01 00:00:00', 'jeevan', '', '2016-03-31 15:08:20', 1),
(119, 102, '2016-03-31 00:00:00', '2016-04-01 00:00:00', 'jeevan', '', '2016-03-31 15:08:23', 1),
(120, 102, '2016-03-31 00:00:00', '2016-04-01 00:00:00', 'jeevan', '', '2016-03-31 15:10:55', 1),
(121, 102, '2016-03-31 00:00:00', '2016-04-02 00:00:00', 'adas', '', '2016-03-31 15:25:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_distance`
--

CREATE TABLE IF NOT EXISTS `hotel_distance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `distance_from` varchar(300) NOT NULL,
  `distance` varchar(100) NOT NULL,
  `distance_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for normal for airport 2',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=657 ;

--
-- Dumping data for table `hotel_distance`
--

INSERT INTO `hotel_distance` (`id`, `hotel_id`, `distance_from`, `distance`, `distance_type`) VALUES
(1, 60, 'agra', '10', 1),
(2, 60, 'indra', '4', 2),
(3, 61, 'agra', '10', 1),
(4, 61, 'indra', '4', 2),
(5, 60, 'agra', '10', 1),
(6, 60, 'indra', '4', 2),
(7, 60, 'gaziyabad', '6', 1),
(8, 61, 'agra', '10', 1),
(9, 61, 'indra', '4', 2),
(10, 60, 'agra', '10', 1),
(11, 60, 'indra', '4', 2),
(12, 60, 'gaziyabad', '6', 1),
(13, 61, 'agra', '10', 1),
(14, 61, 'indra', '4', 2),
(15, 60, 'agra', '10', 1),
(16, 60, 'indra', '4', 2),
(17, 60, 'gaziyabad', '6', 1),
(18, 61, 'agra', '10', 1),
(19, 61, 'indra', '4', 2),
(20, 60, 'agra', '10', 1),
(21, 60, 'indra', '4', 2),
(22, 60, 'gaziyabad', '6', 1),
(23, 61, 'agra', '10', 1),
(24, 61, 'indra', '4', 2),
(25, 60, 'agra', '10', 1),
(26, 60, 'indra', '4', 2),
(27, 60, 'gaziyabad', '6', 1),
(28, 61, 'agra', '10', 1),
(29, 61, 'indra', '4', 2),
(377, 122, 'agra', '10', 1),
(378, 122, 'indra', '4', 2),
(379, 124, 'agra', '10', 1),
(380, 124, 'indra', '4', 2),
(381, 124, 'gaziyabad', '6', 1),
(382, 122, 'agra', '10', 1),
(383, 122, 'indra', '4', 2),
(384, 127, 'agra', '10', 1),
(385, 127, 'indra', '4', 2),
(388, 129, 'agra', '10', 1),
(389, 129, 'indra', '4', 2),
(392, 129, 'agra', '10', 1),
(393, 129, 'indra', '4', 2),
(396, 129, 'agra', '10', 1),
(397, 129, 'indra', '4', 2),
(400, 129, 'agra', '10', 1),
(401, 129, 'indra', '4', 2),
(451, 133, 'agra', '10', 1),
(452, 133, 'indra', '4', 2),
(453, 133, 'gaziyabad', '6', 1),
(466, 134, 'agra', '10', 1),
(467, 134, 'indra', '4', 2),
(485, 135, 'agra', '10', 1),
(486, 135, 'indra', '4', 2),
(487, 135, 'gaziyabad', '6', 1),
(494, 128, 'agra', '10', 1),
(495, 128, 'indra', '4', 2),
(502, 135, 'agra', '10', 1),
(503, 135, 'indra', '4', 2),
(504, 135, 'gaziyabad', '6', 1),
(505, 128, 'agra', '10', 1),
(506, 128, 'indra', '4', 2),
(507, 136, 'agra', '10', 1),
(508, 136, 'indra', '4', 2),
(509, 137, 'agra', '10', 1),
(511, 137, 'gaziyabad', '6', 1),
(512, 128, 'agra', '10', 1),
(513, 128, 'indra', '4', 2),
(516, 137, 'agra', '10', 1),
(518, 137, 'gaziyabad', '6', 1),
(519, 128, 'agra', '10', 1),
(520, 128, 'indra', '4', 2),
(523, 137, 'agra', '10', 1),
(525, 137, 'gaziyabad', '6', 1),
(526, 128, 'agra', '10', 1),
(527, 128, 'indra', '4', 2),
(528, 138, 'agra', '10', 1),
(529, 138, 'indra', '4', 2),
(530, 137, 'agra', '10', 1),
(532, 137, 'gaziyabad', '6', 1),
(533, 128, 'agra', '10', 1),
(534, 128, 'indra', '4', 2),
(537, 137, 'agra', '10', 1),
(539, 137, 'gaziyabad', '6', 1),
(540, 128, 'agra', '10', 1),
(541, 128, 'indra', '4', 2),
(544, 137, 'agra', '10', 1),
(546, 137, 'gaziyabad', '6', 1),
(547, 128, 'agra', '10', 1),
(548, 128, 'indra', '4', 2),
(551, 137, 'agra', '10', 1),
(553, 137, 'gaziyabad', '6', 1),
(554, 128, 'agra', '10', 1),
(555, 128, 'indra', '4', 2),
(558, 137, 'agra', '10', 1),
(560, 137, 'gaziyabad', '6', 1),
(561, 128, 'agra', '10', 1),
(562, 128, 'indra', '4', 2),
(565, 137, 'agra', '10', 1),
(567, 137, 'gaziyabad', '6', 1),
(568, 128, 'agra', '10', 1),
(569, 128, 'indra', '4', 2),
(572, 137, 'agra', '10', 1),
(574, 137, 'gaziyabad', '6', 1),
(575, 128, 'agra', '10', 1),
(576, 128, 'indra', '4', 2),
(579, 137, 'agra', '10', 1),
(581, 137, 'gaziyabad', '6', 1),
(582, 128, 'agra', '10', 1),
(583, 128, 'indra', '4', 2),
(586, 137, 'agra', '10', 1),
(588, 137, 'gaziyabad', '6', 1),
(589, 128, 'agra', '10', 1),
(590, 128, 'indra', '4', 2),
(593, 137, 'agra', '10', 1),
(595, 137, 'gaziyabad', '6', 1),
(596, 128, 'agra', '10', 1),
(597, 128, 'indra', '4', 2),
(600, 137, 'agra', '10', 1),
(601, 137, 'indra', '4', 2),
(602, 137, 'gaziyabad', '6', 1),
(603, 128, 'agra', '10', 1),
(604, 128, 'indra', '4', 2),
(605, 140, 'agra', '10', 1),
(606, 140, 'indra', '4', 2),
(627, 141, 'Noida, Uttar Pradesh, India', '1', 1),
(646, 148, 'dasafa', '2', 1),
(647, 148, 'fasfaf', '3', 1),
(648, 148, 'sda', '2', 2),
(649, 148, 'sfaaf', '5', 2),
(650, 64, 'agra', '10', 1),
(651, 64, 'agra', '10', 1),
(652, 64, 'indra', '4', 2),
(653, 64, 'indra', '4', 2),
(654, 64, 'indra', '4', 2),
(655, 102, 'sdfadfas', '23', 1),
(656, 102, 'sdfadfa', '9', 2);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_facilities`
--

CREATE TABLE IF NOT EXISTS `hotel_facilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `facility_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1082 ;

--
-- Dumping data for table `hotel_facilities`
--

INSERT INTO `hotel_facilities` (`id`, `hotel_id`, `facility_id`) VALUES
(1, 60, 16),
(2, 61, 16),
(3, 60, 17),
(4, 61, 16),
(5, 60, 17),
(6, 61, 16),
(145, 122, 16),
(146, 124, 17),
(147, 122, 16),
(148, 127, 17),
(150, 129, 17),
(152, 129, 17),
(154, 129, 17),
(156, 129, 17),
(178, 133, 17),
(185, 134, 16),
(192, 135, 17),
(196, 128, 16),
(200, 135, 17),
(201, 128, 16),
(202, 136, 16),
(204, 128, 16),
(207, 128, 16),
(210, 128, 16),
(211, 138, 16),
(213, 128, 16),
(216, 128, 16),
(219, 128, 16),
(222, 128, 16),
(225, 128, 16),
(228, 128, 16),
(231, 128, 16),
(234, 128, 16),
(237, 128, 16),
(240, 128, 16),
(243, 128, 16),
(279, 137, 17),
(281, 140, 16),
(290, 141, 10),
(291, 141, 4),
(294, 146, 10),
(297, 156, 10),
(299, 64, 16),
(307, 149, 10),
(308, 149, 4),
(567, 115, 10),
(568, 115, 4),
(569, 115, 16),
(570, 115, 5),
(571, 115, 12),
(572, 115, 6),
(675, 158, 4),
(775, 160, 6),
(1004, 153, 10),
(1005, 153, 4),
(1048, 159, 4),
(1049, 159, 16),
(1050, 159, 5),
(1051, 159, 6),
(1052, 159, 15),
(1053, 159, 14),
(1078, 102, 10),
(1079, 102, 4),
(1080, 102, 16),
(1081, 102, 5);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_location_points`
--

CREATE TABLE IF NOT EXISTS `hotel_location_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `point_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `hotel_location_points`
--

INSERT INTO `hotel_location_points` (`id`, `country_id`, `city_id`, `point_name`) VALUES
(1, 0, NULL, ''),
(2, 0, NULL, ''),
(3, 0, NULL, ''),
(4, 241, 4001, 'XDS'),
(5, 241, 4001, 'dgsdgds'),
(6, 241, 4001, 'RRR'),
(7, 241, 4001, 'fffff'),
(8, 5, 4013, 'XYZ df'),
(9, 5, 4013, 'Test'),
(11, 5, 4013, 'ngiukgh'),
(12, 5, 4013, 'asdasd'),
(13, 5, 4013, 'sdsd'),
(15, 5, 4013, 'qwe'),
(16, 5, 4013, 'jhkjhk'),
(17, 5, 4013, 'asdsda'),
(18, 5, 4013, 'fghfgh'),
(19, 5, 4013, 'sef'),
(20, 5, 4013, 'tykytk'),
(21, 5, 4013, 'jklljk'),
(22, 5, 4013, 'JJJJJJ'),
(23, 18, 4015, 'HHH'),
(24, 241, 4002, 'Ankara-loc'),
(25, 241, 4002, 'anakar-loctest');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_payment_policy`
--

CREATE TABLE IF NOT EXISTS `hotel_payment_policy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `travel_type_id` int(11) NOT NULL,
  `payment_value` int(11) NOT NULL,
  `payment_in` varchar(10) NOT NULL COMMENT '% or fixed',
  `description` text NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_payment_shedules`
--

CREATE TABLE IF NOT EXISTS `hotel_payment_shedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `payment_value` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=116 ;

--
-- Dumping data for table `hotel_payment_shedules`
--

INSERT INTO `hotel_payment_shedules` (`id`, `hotel_id`, `payment_option_id`, `payment_value`) VALUES
(1, 63, 7, 10),
(26, 63, 8, 10),
(27, 63, 7, 10),
(33, 68, 7, 10),
(34, 68, 8, 10),
(35, 68, 7, 10),
(36, 68, 8, 10),
(37, 68, 7, 10),
(38, 68, 8, 10),
(39, 68, 7, 10),
(40, 68, 8, 10),
(41, 71, 7, 10),
(42, 71, 8, 10),
(43, 74, 7, 10),
(44, 74, 8, 10),
(45, 74, 7, 10),
(46, 74, 8, 10),
(47, 77, 7, 10),
(48, 77, 8, 10),
(49, 77, 7, 10),
(50, 77, 8, 10),
(51, 78, 7, 10),
(52, 78, 8, 10),
(53, 84, 7, 10),
(54, 84, 8, 10),
(55, 87, 7, 10),
(56, 87, 8, 10),
(57, 78, 7, 10),
(58, 78, 8, 10),
(59, 78, 7, 10),
(60, 78, 8, 10),
(61, 78, 7, 10),
(62, 78, 8, 10),
(63, 78, 7, 10),
(64, 78, 8, 10),
(65, 124, 7, 10),
(66, 124, 8, 10),
(67, 127, 7, 10),
(68, 129, 7, 10),
(69, 129, 7, 10),
(70, 129, 7, 10),
(71, 129, 7, 10),
(92, 133, 7, 10),
(93, 133, 8, 10),
(105, 135, 7, 10),
(106, 135, 8, 10),
(107, 135, 7, 10),
(108, 135, 8, 10),
(109, 137, 7, 10),
(110, 137, 4, 30),
(111, 64, 8, 24),
(112, 141, 1, 10),
(113, 102, 1, 19),
(114, 159, 1, 10),
(115, 153, 8, 45);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_property_types`
--

CREATE TABLE IF NOT EXISTS `hotel_property_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `property_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=249 ;

--
-- Dumping data for table `hotel_property_types`
--

INSERT INTO `hotel_property_types` (`id`, `hotel_id`, `property_type_id`) VALUES
(7, 7, 7),
(8, 129, 2),
(9, 129, 15),
(10, 128, 2),
(11, 123, 2),
(12, 129, 2),
(13, 129, 15),
(14, 128, 2),
(15, 123, 2),
(16, 129, 2),
(17, 129, 15),
(18, 128, 2),
(19, 123, 2),
(20, 129, 2),
(21, 129, 15),
(22, 128, 2),
(23, 123, 2),
(24, 133, 2),
(25, 133, 15),
(26, 128, 2),
(27, 123, 2),
(28, 133, 2),
(29, 133, 15),
(30, 128, 2),
(31, 123, 2),
(32, 133, 2),
(33, 133, 15),
(34, 128, 2),
(35, 123, 2),
(36, 133, 2),
(37, 133, 15),
(38, 128, 2),
(39, 123, 2),
(40, 133, 2),
(41, 133, 15),
(42, 128, 2),
(43, 123, 2),
(44, 133, 2),
(45, 133, 15),
(46, 128, 2),
(47, 123, 2),
(48, 133, 2),
(49, 133, 15),
(50, 128, 2),
(51, 123, 2),
(52, 133, 2),
(53, 133, 15),
(54, 128, 2),
(55, 134, 2),
(56, 135, 2),
(57, 135, 15),
(58, 128, 2),
(59, 136, 2),
(60, 135, 2),
(61, 135, 15),
(62, 128, 2),
(63, 136, 2),
(66, 128, 2),
(67, 138, 2),
(70, 128, 2),
(71, 138, 2),
(74, 128, 2),
(75, 138, 2),
(78, 128, 2),
(82, 128, 2),
(86, 128, 2),
(90, 128, 2),
(94, 128, 2),
(98, 128, 2),
(102, 128, 2),
(106, 128, 2),
(110, 128, 2),
(114, 128, 2),
(118, 128, 2),
(132, 107, 3),
(184, 137, 15),
(185, 137, 5),
(186, 137, 2),
(188, 140, 2),
(205, 141, 12),
(206, 141, 10),
(207, 141, 11),
(208, 141, 15),
(209, 152, 3),
(213, 154, 1),
(214, 154, 3),
(215, 118, 3),
(216, 118, 12),
(217, 118, 10),
(218, 118, 11),
(219, 118, 15),
(220, 118, 7),
(221, 118, 5),
(222, 118, 9),
(223, 118, 13),
(224, 118, 2),
(225, 118, 8),
(226, 117, 3),
(227, 117, 12),
(228, 117, 10),
(229, 117, 11),
(230, 117, 15),
(231, 117, 7),
(232, 117, 5),
(241, 160, 12),
(248, 153, 12);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_purpose`
--

CREATE TABLE IF NOT EXISTS `hotel_purpose` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `hotel_purpose`
--

INSERT INTO `hotel_purpose` (`id`, `title`) VALUES
(2, 'Leisure'),
(5, 'Airport hotel'),
(6, 'Business gard'),
(9, 'Business');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_renovation_schedule`
--

CREATE TABLE IF NOT EXISTS `hotel_renovation_schedule` (
  `rnv_id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `date_from` datetime NOT NULL,
  `date_to` datetime NOT NULL,
  `renovation_type` int(11) DEFAULT NULL,
  `area_effected` text,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`rnv_id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=171 ;

--
-- Dumping data for table `hotel_renovation_schedule`
--

INSERT INTO `hotel_renovation_schedule` (`rnv_id`, `hotel_id`, `date_from`, `date_to`, `renovation_type`, `area_effected`, `last_updated`) VALUES
(65, 124, '2016-03-19 00:00:00', '2016-03-20 00:00:00', 0, 'ascv', '2016-03-11 06:20:13'),
(66, 124, '2016-03-20 00:00:00', '2016-03-21 00:00:00', 0, 'bhfdd', '2016-03-11 06:20:13'),
(67, 127, '2016-03-19 00:00:00', '2016-03-20 00:00:00', 0, 'ascv', '2016-03-11 09:13:30'),
(68, 129, '2016-03-19 00:00:00', '2016-03-20 00:00:00', 0, 'ascv', '2016-03-11 09:41:45'),
(69, 129, '2016-03-19 00:00:00', '2016-03-20 00:00:00', 0, 'ascv', '2016-03-12 12:21:52'),
(70, 129, '2016-03-19 00:00:00', '2016-03-20 00:00:00', 0, 'ascv', '2016-03-12 12:22:29'),
(71, 129, '2016-03-19 00:00:00', '2016-03-20 00:00:00', 0, 'ascv', '2016-03-12 12:23:58'),
(92, 133, '2016-03-19 00:00:00', '2016-03-20 00:00:00', 0, 'ascv', '2016-03-14 04:52:06'),
(93, 133, '2016-03-20 00:00:00', '2016-03-21 00:00:00', 0, 'bhfdd', '2016-03-14 04:52:06'),
(105, 135, '2016-03-19 00:00:00', '2016-03-20 00:00:00', 0, 'ascv', '2016-03-14 04:54:09'),
(106, 135, '2016-03-20 00:00:00', '2016-03-21 00:00:00', 0, 'bhfdd', '2016-03-14 04:54:09'),
(107, 135, '2016-03-19 00:00:00', '2016-03-20 00:00:00', 0, 'ascv', '2016-03-14 05:22:15'),
(108, 135, '2016-03-20 00:00:00', '2016-03-21 00:00:00', 0, 'bhfdd', '2016-03-14 05:22:15'),
(165, 137, '2016-03-21 00:00:00', '2016-03-31 00:00:00', 2, 'dfsfs', '2016-03-16 06:31:11'),
(169, 64, '2016-03-28 00:00:00', '2016-03-30 00:00:00', 1, 'jjghgjg', '2016-03-30 12:12:43'),
(170, 64, '2016-03-22 00:00:00', '2016-03-24 00:00:00', 2, 'rtwt', '2016-03-30 12:12:44');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_rooms_pricing`
--

CREATE TABLE IF NOT EXISTS `hotel_rooms_pricing` (
  `pricing_id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `room_type` int(11) DEFAULT NULL COMMENT 'type_id from hotel room types tables',
  `inclusions` text,
  `curency_code` varchar(20) DEFAULT NULL,
  `max_adult` int(11) DEFAULT NULL,
  `max_child` int(11) DEFAULT NULL,
  `inventory` int(11) NOT NULL COMMENT 'no of rooms',
  `period_from` date DEFAULT NULL,
  `period_to` date DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pricing_id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=377 ;

--
-- Dumping data for table `hotel_rooms_pricing`
--

INSERT INTO `hotel_rooms_pricing` (`pricing_id`, `hotel_id`, `room_type`, `inclusions`, `curency_code`, `max_adult`, `max_child`, `inventory`, `period_from`, `period_to`, `last_updated`) VALUES
(60, 124, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-11 06:20:12'),
(61, 124, 5, '', 'inr', 3, 3, 2, '2016-03-25', '2016-04-25', '2016-03-11 06:20:12'),
(62, 127, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-11 09:13:30'),
(63, 129, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-11 09:41:45'),
(64, 129, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-12 12:21:51'),
(65, 129, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-12 12:22:28'),
(66, 129, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-12 12:23:57'),
(67, 133, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-12 12:25:05'),
(68, 133, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-12 12:25:49'),
(69, 133, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-12 12:27:27'),
(70, 133, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-12 12:33:23'),
(71, 133, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-12 12:36:24'),
(72, 133, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-14 04:45:28'),
(73, 133, 5, '', 'inr', 3, 3, 2, '2016-03-25', '2016-04-25', '2016-03-14 04:45:28'),
(74, 133, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-14 04:47:13'),
(75, 133, 5, '', 'inr', 3, 3, 2, '2016-03-25', '2016-04-25', '2016-03-14 04:47:13'),
(76, 133, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-14 04:52:01'),
(77, 133, 5, '', 'inr', 3, 3, 2, '2016-03-25', '2016-04-25', '2016-03-14 04:52:02'),
(78, 135, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-14 04:54:05'),
(79, 135, 5, '', 'inr', 3, 3, 2, '2016-03-25', '2016-04-25', '2016-03-14 04:54:05'),
(80, 135, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-14 05:22:14'),
(81, 135, 5, '', 'inr', 3, 3, 2, '2016-03-25', '2016-04-25', '2016-03-14 05:22:14'),
(82, 137, 4, 'test inclution', 'INR', 2, 2, 5, '2016-03-11', '2016-03-25', '2016-03-14 05:24:55'),
(108, 140, 4, 'test inclution', 'INR', 12, 12, 15, '2016-03-11', '2016-03-25', '2016-03-14 08:23:26'),
(144, 137, 3, ' yrsgdds', 'INR', 4, 5, 6, '2016-03-15', '2016-03-24', '2016-03-21 06:40:40'),
(148, 140, 5, '', '', 3, 3, 2, '2016-03-25', '2016-04-25', '2016-03-21 09:56:19'),
(149, 140, 5, '', '', 13, 13, 12, '2016-03-25', '2016-08-25', '2016-03-21 09:56:19'),
(261, 149, 3, '', 'INR', 0, 0, 124, '0000-00-00', '0000-00-00', '2016-03-30 12:51:24'),
(265, 118, 3, '4534', 'INR', 2, 2, 43, '2016-03-31', '2016-04-09', '2016-03-31 08:27:25'),
(366, 160, 2, '', 'TRY', 4, 5, 222, '2016-04-06', '2016-05-07', '2016-03-31 14:07:55'),
(375, 102, 3, 'fas', 'INR', 0, 0, 23, '0001-11-30', '0001-11-30', '2016-03-31 14:54:46'),
(376, 102, 3, 'ffsdaf', 'INR', 3, 56, 23, '2016-04-01', '2016-04-29', '2016-03-31 15:15:50');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_rooms_pricing_complimentary`
--

CREATE TABLE IF NOT EXISTS `hotel_rooms_pricing_complimentary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pricing_id` int(11) NOT NULL,
  `cmpl_service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=995 ;

--
-- Dumping data for table `hotel_rooms_pricing_complimentary`
--

INSERT INTO `hotel_rooms_pricing_complimentary` (`id`, `pricing_id`, `cmpl_service_id`) VALUES
(62, 62, 8),
(63, 62, 11),
(64, 63, 8),
(65, 63, 11),
(66, 64, 8),
(67, 64, 11),
(68, 65, 8),
(69, 65, 11),
(70, 66, 8),
(71, 66, 11),
(72, 67, 8),
(73, 67, 11),
(74, 68, 8),
(75, 68, 11),
(76, 69, 8),
(77, 69, 11),
(78, 70, 8),
(79, 70, 11),
(80, 71, 8),
(81, 71, 11),
(82, 72, 8),
(83, 72, 11),
(84, 74, 8),
(85, 74, 11),
(86, 76, 8),
(87, 76, 11),
(88, 78, 8),
(89, 78, 11),
(90, 80, 8),
(91, 80, 11),
(94, 84, 8),
(95, 84, 11),
(96, 86, 8),
(97, 86, 11),
(98, 88, 8),
(99, 88, 11),
(100, 90, 8),
(101, 90, 11),
(102, 92, 8),
(103, 92, 11),
(104, 94, 8),
(105, 94, 11),
(106, 96, 8),
(107, 96, 11),
(108, 98, 8),
(109, 98, 11),
(110, 100, 8),
(111, 100, 11),
(112, 102, 8),
(113, 102, 11),
(114, 104, 8),
(115, 104, 11),
(116, 106, 8),
(117, 106, 11),
(124, 111, 8),
(125, 111, 11),
(134, 113, 3),
(135, 113, 5),
(138, 114, 3),
(139, 114, 5),
(142, 115, 3),
(143, 115, 5),
(153, 116, 3),
(154, 116, 5),
(158, 117, 3),
(159, 117, 5),
(208, 118, 3),
(209, 118, 5),
(213, 119, 3),
(214, 119, 5),
(218, 120, 3),
(219, 120, 5),
(223, 121, 3),
(224, 121, 5),
(228, 122, 3),
(229, 122, 5),
(233, 123, 3),
(234, 123, 5),
(237, 124, 3),
(238, 124, 5),
(241, 125, 3),
(242, 125, 5),
(245, 126, 3),
(246, 126, 5),
(249, 127, 3),
(250, 127, 5),
(253, 128, 3),
(254, 128, 5),
(257, 129, 3),
(258, 129, 5),
(261, 130, 3),
(262, 130, 5),
(265, 131, 3),
(266, 131, 5),
(269, 132, 3),
(270, 132, 5),
(274, 133, 3),
(275, 133, 5),
(278, 134, 3),
(279, 134, 5),
(282, 135, 3),
(283, 135, 5),
(286, 136, 3),
(287, 136, 5),
(291, 137, 3),
(292, 137, 5),
(296, 138, 3),
(297, 138, 5),
(300, 139, 3),
(301, 139, 5),
(304, 140, 3),
(305, 140, 5),
(308, 141, 3),
(309, 141, 5),
(312, 142, 3),
(313, 142, 5),
(316, 143, 3),
(317, 143, 5),
(318, 82, 8),
(319, 82, 11),
(320, 144, 3),
(321, 144, 5),
(327, 147, 3),
(328, 147, 5),
(329, 108, 8),
(330, 108, 11),
(331, 150, 8),
(332, 150, 11),
(333, 152, 3),
(334, 152, 5),
(335, 153, 8),
(336, 153, 11),
(340, 155, 8),
(341, 155, 11),
(342, 155, 8),
(343, 155, 11),
(347, 157, 8),
(348, 157, 11),
(352, 159, 8),
(353, 159, 11),
(357, 161, 8),
(358, 161, 11),
(362, 163, 8),
(363, 163, 11),
(367, 165, 8),
(368, 165, 11),
(372, 167, 8),
(373, 167, 11),
(377, 169, 8),
(378, 169, 11),
(382, 171, 8),
(383, 171, 11),
(387, 173, 8),
(388, 173, 11),
(392, 175, 8),
(393, 175, 11),
(397, 177, 8),
(398, 177, 11),
(402, 179, 8),
(403, 179, 11),
(407, 181, 8),
(408, 181, 11),
(412, 183, 8),
(413, 183, 11),
(417, 185, 8),
(418, 185, 11),
(422, 187, 8),
(423, 187, 11),
(427, 189, 8),
(428, 189, 11),
(432, 191, 8),
(433, 191, 11),
(437, 193, 8),
(438, 193, 11),
(442, 195, 8),
(443, 195, 11),
(447, 197, 8),
(448, 197, 11),
(452, 199, 8),
(453, 199, 11),
(457, 201, 8),
(458, 201, 11),
(462, 203, 8),
(463, 203, 11),
(467, 205, 8),
(468, 205, 11),
(472, 207, 8),
(473, 207, 11),
(477, 209, 8),
(478, 209, 11),
(482, 211, 8),
(483, 211, 11),
(487, 213, 8),
(488, 213, 11),
(492, 215, 8),
(493, 215, 11),
(497, 217, 8),
(498, 217, 11),
(502, 219, 8),
(503, 219, 11),
(507, 221, 8),
(508, 221, 11),
(512, 223, 8),
(513, 223, 11),
(517, 225, 8),
(518, 225, 11),
(522, 227, 8),
(523, 227, 11),
(527, 229, 8),
(528, 229, 11),
(532, 231, 8),
(533, 231, 11),
(537, 233, 8),
(538, 233, 11),
(542, 235, 8),
(543, 235, 11),
(547, 237, 8),
(548, 237, 11),
(552, 239, 8),
(553, 239, 11),
(557, 241, 8),
(558, 241, 11),
(562, 243, 8),
(563, 243, 11),
(567, 245, 8),
(568, 245, 11),
(572, 247, 8),
(573, 247, 11),
(577, 249, 8),
(578, 249, 11),
(582, 251, 8),
(583, 251, 11),
(587, 253, 8),
(588, 253, 11),
(592, 255, 8),
(593, 255, 11),
(597, 257, 8),
(598, 257, 11),
(604, 261, 4),
(626, 263, 3),
(627, 263, 5),
(628, 264, 3),
(629, 264, 5),
(634, 266, 3),
(636, 267, 3),
(637, 268, 3),
(638, 269, 3),
(641, 270, 12),
(642, 271, 12),
(643, 272, 12),
(644, 273, 12),
(647, 274, 11),
(649, 275, 11),
(651, 276, 11),
(653, 277, 11),
(655, 278, 11),
(657, 279, 11),
(659, 280, 11),
(661, 281, 11),
(663, 282, 11),
(665, 283, 11),
(669, 284, 11),
(671, 285, 11),
(673, 286, 11),
(675, 287, 11),
(677, 288, 11),
(679, 289, 11),
(681, 290, 11),
(683, 291, 11),
(685, 292, 11),
(687, 293, 11),
(689, 294, 11),
(691, 295, 11),
(693, 296, 11),
(695, 297, 11),
(697, 298, 11),
(699, 299, 11),
(701, 300, 11),
(703, 301, 11),
(705, 302, 11),
(707, 303, 11),
(709, 304, 11),
(711, 305, 11),
(713, 306, 11),
(715, 307, 11),
(717, 308, 11),
(719, 309, 11),
(721, 310, 11),
(723, 311, 11),
(725, 312, 11),
(727, 313, 11),
(729, 314, 11),
(731, 315, 11),
(733, 316, 11),
(735, 317, 11),
(737, 318, 11),
(739, 319, 11),
(741, 320, 11),
(743, 321, 11),
(745, 322, 11),
(747, 323, 11),
(749, 324, 11),
(751, 325, 11),
(753, 326, 11),
(755, 327, 11),
(757, 328, 11),
(759, 329, 11),
(761, 330, 11),
(763, 331, 11),
(765, 332, 11),
(767, 333, 11),
(769, 334, 11),
(771, 335, 11),
(773, 336, 11),
(775, 337, 11),
(777, 338, 11),
(779, 339, 11),
(781, 340, 11),
(783, 341, 11),
(785, 342, 11),
(787, 343, 11),
(789, 344, 11),
(791, 345, 11),
(793, 346, 11),
(795, 347, 11),
(797, 348, 11),
(799, 349, 11),
(801, 350, 11),
(803, 351, 11),
(805, 352, 11),
(806, 265, 3),
(807, 265, 5),
(808, 353, 11),
(809, 354, 11),
(810, 355, 11),
(811, 356, 11),
(815, 357, 3),
(817, 358, 5),
(820, 359, 1),
(821, 360, 1),
(824, 361, 4),
(825, 362, 2),
(826, 363, 4),
(827, 364, 2),
(829, 365, 4),
(841, 366, 3),
(858, 367, 3),
(859, 367, 5),
(861, 368, 3),
(862, 368, 5),
(863, 369, 3),
(864, 369, 5),
(865, 370, 3),
(866, 370, 5),
(867, 371, 3),
(868, 371, 5),
(869, 372, 3),
(870, 372, 5),
(871, 373, 3),
(872, 373, 5),
(949, 262, 3),
(960, 374, 3),
(961, 374, 5),
(992, 375, 3),
(993, 375, 5),
(994, 376, 4);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_rooms_pricing_details`
--

CREATE TABLE IF NOT EXISTS `hotel_rooms_pricing_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pricing_id` int(11) NOT NULL,
  `market_id` int(11) DEFAULT NULL,
  `double_price` decimal(6,2) DEFAULT NULL,
  `triple_price` decimal(6,2) DEFAULT NULL,
  `quad_price` decimal(6,2) DEFAULT NULL,
  `breakfast_price` decimal(6,2) DEFAULT NULL,
  `half_board_price` decimal(6,2) DEFAULT NULL,
  `all_incusive_adult_price` decimal(6,0) DEFAULT NULL,
  `extra_adult_price` decimal(6,2) DEFAULT NULL,
  `extra_child_price` decimal(6,2) DEFAULT NULL,
  `extra_bed_price` decimal(6,2) DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `room_id` (`double_price`),
  KEY `hotel_id` (`pricing_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=421 ;

--
-- Dumping data for table `hotel_rooms_pricing_details`
--

INSERT INTO `hotel_rooms_pricing_details` (`id`, `pricing_id`, `market_id`, `double_price`, `triple_price`, `quad_price`, `breakfast_price`, `half_board_price`, `all_incusive_adult_price`, `extra_adult_price`, `extra_child_price`, `extra_bed_price`, `last_updated`) VALUES
(59, 60, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-11 06:20:13'),
(60, 61, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-11 06:20:13'),
(61, 62, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-11 09:13:30'),
(62, 63, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-11 09:41:45'),
(63, 64, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-12 12:21:52'),
(64, 65, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-12 12:22:29'),
(65, 66, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-12 12:23:58'),
(66, 67, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-12 12:25:06'),
(67, 68, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-12 12:25:50'),
(68, 69, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-12 12:27:27'),
(69, 70, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-12 12:33:24'),
(70, 71, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-12 12:36:25'),
(71, 72, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:45:30'),
(72, 73, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:45:30'),
(73, 74, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:47:14'),
(74, 75, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:47:14'),
(75, 76, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:52:02'),
(76, 76, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:52:03'),
(77, 77, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:52:03'),
(78, 76, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:52:03'),
(79, 77, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:52:03'),
(80, 76, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:52:04'),
(81, 77, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:52:04'),
(82, 76, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:52:05'),
(83, 77, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:52:05'),
(84, 76, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:52:05'),
(85, 77, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:52:05'),
(86, 76, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:52:06'),
(87, 77, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:52:06'),
(88, 78, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:54:05'),
(89, 78, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:54:06'),
(90, 79, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:54:06'),
(91, 78, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:54:07'),
(92, 79, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:54:07'),
(93, 78, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:54:08'),
(94, 79, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:54:08'),
(95, 78, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:54:08'),
(96, 79, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:54:08'),
(97, 78, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:54:09'),
(98, 79, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:54:09'),
(99, 78, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 04:54:10'),
(100, 79, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 04:54:10'),
(101, 80, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 05:22:15'),
(102, 81, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 05:22:15'),
(103, 82, 11, 2.00, 23.00, 4.00, 5.00, 6.00, 74, 8.00, 9.00, 10.00, '2016-03-18 05:01:47'),
(104, 83, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 05:24:57'),
(105, 84, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 05:36:47'),
(106, 85, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 05:36:47'),
(107, 86, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 05:53:52'),
(108, 87, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 05:53:52'),
(109, 88, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 06:00:35'),
(110, 89, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 06:00:35'),
(111, 90, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 06:02:10'),
(112, 91, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 06:02:10'),
(113, 92, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 06:03:37'),
(114, 93, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 06:03:37'),
(115, 94, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 06:04:03'),
(116, 95, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 06:04:03'),
(117, 96, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 06:05:02'),
(118, 97, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 06:05:02'),
(119, 98, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 06:05:24'),
(120, 99, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 06:05:24'),
(121, 100, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 06:07:08'),
(122, 101, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 06:07:08'),
(123, 102, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 06:11:08'),
(124, 103, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 06:11:08'),
(125, 104, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 06:11:49'),
(126, 105, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 06:11:49'),
(127, 106, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 06:13:02'),
(128, 107, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 06:13:02'),
(129, 111, 11, 2.00, 3.00, 4.00, 5.00, 6.00, 7, 8.00, 9.00, 10.00, '2016-03-14 09:23:29'),
(130, 112, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-14 09:23:29'),
(135, 113, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-18 05:01:48'),
(136, 82, 11, 345.00, 2.00, 23.00, 23.00, 3.00, 4, 5.00, 6.00, 6.00, '2016-03-18 05:03:24'),
(137, 114, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-18 05:03:24'),
(138, 115, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-18 05:05:04'),
(139, 115, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-18 05:05:04'),
(151, 116, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-19 09:31:24'),
(152, 117, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-19 09:33:52'),
(153, 118, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-19 09:34:08'),
(154, 119, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-19 09:37:31'),
(155, 120, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-19 09:46:41'),
(156, 121, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-21 04:43:14'),
(157, 122, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-21 04:58:11'),
(158, 123, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-21 05:05:40'),
(159, 124, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 05:44:09'),
(160, 124, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 05:44:09'),
(161, 125, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 05:50:05'),
(162, 125, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 05:50:05'),
(163, 126, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 05:50:24'),
(164, 126, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 05:50:24'),
(165, 127, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 05:58:36'),
(166, 127, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 05:58:36'),
(167, 128, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 05:59:36'),
(168, 128, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 05:59:36'),
(169, 129, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:00:37'),
(170, 129, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:00:37'),
(171, 130, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:01:43'),
(172, 130, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:01:43'),
(173, 131, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:02:06'),
(174, 131, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:02:06'),
(175, 132, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:03:29'),
(176, 132, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:03:29'),
(177, 133, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-21 06:04:39'),
(178, 134, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:06:07'),
(179, 134, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:06:07'),
(180, 135, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:09:08'),
(181, 135, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:09:08'),
(182, 136, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:10:48'),
(183, 136, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:10:48'),
(184, 137, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-21 06:12:17'),
(185, 138, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-21 06:13:10'),
(186, 139, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:15:17'),
(187, 139, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:15:17'),
(188, 140, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:17:38'),
(189, 140, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:17:38'),
(190, 141, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:19:22'),
(191, 141, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:19:22'),
(192, 142, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:30:47'),
(193, 142, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:30:47'),
(194, 143, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:38:27'),
(195, 143, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:38:28'),
(196, 144, 5, 45.00, 67.00, 78.00, 9.00, 9.00, 8, 3.00, 34.00, 3.00, '2016-03-21 06:40:40'),
(197, 144, 11, 455.00, 2.00, 252.00, 25.00, 252.00, 252, 252.00, 52.00, 25.00, '2016-03-21 06:40:40'),
(198, 147, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-21 08:26:56'),
(199, 152, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-21 11:32:12'),
(200, 155, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-21 12:27:22'),
(201, 156, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-21 12:27:22'),
(202, 157, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-21 12:56:12'),
(203, 158, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-21 12:56:12'),
(204, 159, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-21 12:56:57'),
(205, 160, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-21 12:56:57'),
(206, 161, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-21 13:02:43'),
(207, 162, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-21 13:02:43'),
(208, 163, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-21 13:35:21'),
(209, 164, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-21 13:35:21'),
(210, 165, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-21 13:41:06'),
(211, 166, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-21 13:41:06'),
(212, 167, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-21 13:42:07'),
(213, 168, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-21 13:42:07'),
(214, 169, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-22 05:08:28'),
(215, 170, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-22 05:08:28'),
(216, 171, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 05:13:19'),
(217, 172, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 05:13:20'),
(218, 173, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 06:26:44'),
(219, 174, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 06:26:44'),
(220, 175, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 06:37:52'),
(221, 176, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 06:37:52'),
(222, 177, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 06:38:11'),
(223, 178, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 06:38:11'),
(224, 179, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 06:41:27'),
(225, 180, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 06:41:27'),
(226, 181, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 06:41:31'),
(227, 182, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 06:41:31'),
(228, 183, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 06:41:59'),
(229, 184, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 06:41:59'),
(230, 185, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 06:42:03'),
(231, 186, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 06:42:03'),
(232, 187, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:13:03'),
(233, 188, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:13:03'),
(234, 189, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:14:27'),
(235, 190, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:14:27'),
(236, 191, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:18:30'),
(237, 192, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:18:30'),
(238, 193, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:18:53'),
(239, 194, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:18:53'),
(240, 195, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:18:58'),
(241, 196, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:18:58'),
(242, 197, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:19:06'),
(243, 198, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:19:06'),
(244, 199, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:22:15'),
(245, 200, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:22:15'),
(246, 201, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:22:37'),
(247, 202, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:22:37'),
(248, 203, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:22:41'),
(249, 204, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:22:42'),
(250, 205, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:29:40'),
(251, 206, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:29:40'),
(252, 207, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:29:48'),
(253, 208, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:29:48'),
(254, 209, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:39:07'),
(255, 210, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:39:07'),
(256, 211, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:39:10'),
(257, 212, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:39:10'),
(258, 213, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:42:58'),
(259, 214, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:42:58'),
(260, 215, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:43:02'),
(261, 216, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:43:02'),
(262, 217, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:47:33'),
(263, 218, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:47:33'),
(264, 219, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:51:10'),
(265, 220, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:51:10'),
(266, 221, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 07:51:13'),
(267, 222, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 07:51:13'),
(268, 223, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:01:09'),
(269, 224, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:01:09'),
(270, 225, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:01:13'),
(271, 226, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:01:13'),
(272, 227, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:01:16'),
(273, 228, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:01:16'),
(274, 229, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:17:27'),
(275, 230, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:17:27'),
(276, 231, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:17:49'),
(277, 232, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:17:49'),
(278, 233, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:18:23'),
(279, 234, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:18:23'),
(280, 235, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:18:28'),
(281, 236, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:18:28'),
(282, 237, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:18:32'),
(283, 238, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:18:32'),
(284, 239, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:19:21'),
(285, 240, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:19:21'),
(286, 241, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:19:51'),
(287, 242, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:19:51'),
(288, 243, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:20:39'),
(289, 244, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:20:39'),
(290, 245, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:20:42'),
(291, 246, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:20:42'),
(292, 247, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:20:50'),
(293, 248, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:20:50'),
(294, 249, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 08:21:16'),
(295, 250, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 08:21:16'),
(296, 251, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 10:22:30'),
(297, 252, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 10:22:30'),
(298, 253, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 11:42:21'),
(299, 254, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 11:42:21'),
(300, 255, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 11:42:26'),
(301, 256, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 11:42:26'),
(302, 257, 11, 2.00, 3.00, 44.00, 5.00, 46.00, 7, 8.00, 9.00, 10.00, '2016-03-30 11:42:33'),
(303, 258, 12, 3.00, 4.00, 5.00, 0.00, 0.00, 4, 0.00, 0.00, 0.00, '2016-03-30 11:42:33'),
(306, 261, 5, 12.00, 22.00, 32.00, 21.00, 22.00, 33, 44.00, 44.00, 44.00, '2016-03-30 12:51:24'),
(307, 261, 4, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-30 12:51:24'),
(308, 266, 4, 12.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:03:01'),
(309, 267, 4, 12.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:03:19'),
(310, 268, 4, 12.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:04:07'),
(311, 269, 4, 12.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:04:18'),
(312, 262, 12, 12.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:50:44'),
(313, 270, 5, 23.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:05:19'),
(314, 271, 5, 23.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:05:37'),
(315, 272, 5, 23.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:06:30'),
(316, 273, 5, 23.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:07:02'),
(317, 274, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:08:05'),
(318, 275, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:13:21'),
(319, 276, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:13:52'),
(320, 277, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:14:08'),
(321, 278, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:14:35'),
(322, 279, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:14:55'),
(323, 280, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:36:56'),
(324, 281, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:37:50'),
(325, 282, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:39:45'),
(326, 283, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 09:40:56'),
(327, 284, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:00:06'),
(328, 285, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:00:14'),
(329, 286, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:02:29'),
(330, 287, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:02:36'),
(331, 288, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:02:57'),
(332, 289, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:03:14'),
(333, 290, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:06:28'),
(334, 291, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:07:27'),
(335, 292, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:20:56'),
(336, 293, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:21:06'),
(337, 294, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:22:17'),
(338, 295, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:22:23'),
(339, 296, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:25'),
(340, 297, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:28'),
(341, 298, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:30'),
(342, 299, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:32'),
(343, 300, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:35'),
(344, 301, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:37'),
(345, 302, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:40'),
(346, 303, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:42'),
(347, 304, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:44'),
(348, 305, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:47'),
(349, 306, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:49'),
(350, 307, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:52'),
(351, 308, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:54'),
(352, 309, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:57'),
(353, 310, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:26:59'),
(354, 311, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:27:02'),
(355, 312, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:27:04'),
(356, 313, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:27:07'),
(357, 314, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:27:09'),
(358, 315, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:27:12'),
(359, 316, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:27:15'),
(360, 317, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:28:33'),
(361, 318, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:30:13'),
(362, 319, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:30:17'),
(363, 320, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:33:25'),
(364, 321, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:33:30'),
(365, 322, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:33:35'),
(366, 323, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:34:18'),
(367, 324, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:34:23'),
(368, 325, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:34:40'),
(369, 326, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:53:05'),
(370, 327, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 10:53:13'),
(371, 328, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:00:20'),
(372, 329, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:00:36'),
(373, 330, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:01:53'),
(374, 331, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:03:32'),
(375, 332, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:04:47'),
(376, 333, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:04:55'),
(377, 334, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:05:38'),
(378, 335, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:05:47'),
(379, 336, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:05:57'),
(380, 337, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:06:22'),
(381, 338, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:07:11'),
(382, 339, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:07:17'),
(383, 340, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:08:20'),
(384, 341, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:08:24'),
(385, 342, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:16:08'),
(386, 343, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:16:24'),
(387, 344, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:18:34'),
(388, 345, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:19:49'),
(389, 346, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:20:49'),
(390, 347, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:20:57'),
(391, 348, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:21:05'),
(392, 349, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:21:15'),
(393, 350, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:21:20'),
(394, 351, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:22:41'),
(395, 352, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 11:22:50'),
(396, 353, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:03:21'),
(397, 354, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:03:45'),
(398, 355, 5, 12.00, 45.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:04:16'),
(399, 356, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:04:19'),
(400, 356, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:04:20'),
(401, 357, 2, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:04:59'),
(402, 358, 2, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:05:11'),
(403, 359, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:05:44'),
(404, 360, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:06:03'),
(405, 361, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:07:20'),
(406, 362, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:07:20'),
(407, 363, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:07:41'),
(408, 364, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:07:41'),
(409, 365, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:07:52'),
(410, 366, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:07:55'),
(411, 367, 5, 4.00, 4.00, 556.00, 6.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:39:26'),
(412, 368, 5, 4.00, 4.00, 556.00, 6.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:40:25'),
(413, 369, 5, 345.00, 33.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:40:25'),
(414, 370, 5, 4.00, 4.00, 556.00, 6.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:43:18'),
(415, 371, 5, 345.00, 33.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:43:18'),
(416, 372, 5, 4.00, 4.00, 556.00, 6.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:43:54'),
(417, 373, 5, 345.00, 33.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:43:54'),
(418, 374, 5, 4.00, 4.00, 556.00, 6.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:54:46'),
(419, 375, 5, 345.00, 33.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 14:54:47'),
(420, 376, 5, 2.00, 2.00, 2.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, '2016-03-31 15:15:50');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_room_rate_for_travel_types`
--

CREATE TABLE IF NOT EXISTS `hotel_room_rate_for_travel_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `travel_type_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `rate_id` int(11) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`),
  KEY `travel_type_id` (`travel_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_room_types`
--

CREATE TABLE IF NOT EXISTS `hotel_room_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  PRIMARY KEY (`type_id`),
  KEY `type_id` (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `hotel_room_types`
--

INSERT INTO `hotel_room_types` (`type_id`, `title`) VALUES
(1, 'Duplex'),
(2, 'Luxury'),
(3, 'Deluxe'),
(4, 'Super Delux'),
(5, 'delux');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_status`
--

CREATE TABLE IF NOT EXISTS `hotel_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `hotel_status`
--

INSERT INTO `hotel_status` (`id`, `title`) VALUES
(3, 'Blacklisted'),
(2, 'Disable'),
(1, 'Enable');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `markets`
--

CREATE TABLE IF NOT EXISTS `markets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `markets`
--

INSERT INTO `markets` (`id`, `title`, `status`) VALUES
(1, 'South Asia', 1),
(2, 'London', 1),
(3, 'US', 1),
(4, 'UK', 1),
(5, 'Turkey', 1),
(6, 'Nordic', 1),
(7, 'Europe', 1),
(8, 'North US', 1),
(10, 'asia india pak sree lunks', 1),
(11, 'Noida', 1),
(12, 'India', 1);

-- --------------------------------------------------------

--
-- Table structure for table `meal_plans`
--

CREATE TABLE IF NOT EXISTS `meal_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(800) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`(767))
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `meal_plans`
--

INSERT INTO `meal_plans` (`id`, `title`) VALUES
(1, 'qwer sd'),
(3, 'sgdggdsgds');

-- --------------------------------------------------------

--
-- Table structure for table `payment_options`
--

CREATE TABLE IF NOT EXISTS `payment_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_title` varchar(600) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `payment_options`
--

INSERT INTO `payment_options` (`id`, `option_title`) VALUES
(1, '15 days before'),
(2, '45 days'),
(4, '10 days before'),
(5, 'On check out'),
(6, 'on entry- time'),
(7, '20 day before'),
(8, '3 day before');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE IF NOT EXISTS `positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `title`) VALUES
(1, 'abcd gh'),
(5, 'C'),
(11, 'Ct'),
(6, 'D'),
(2, 'dfbfdb'),
(12, 'Dt'),
(7, 'e'),
(13, 'et'),
(8, 'f'),
(9, 'g'),
(3, 'Hr'),
(10, 'Hrt'),
(4, 'sales');

-- --------------------------------------------------------

--
-- Table structure for table `property_types`
--

CREATE TABLE IF NOT EXISTS `property_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `property_types`
--

INSERT INTO `property_types` (`id`, `title`, `status`) VALUES
(1, 'Airport Hotel', 1),
(2, 'Spa Hotel', 1),
(3, 'Beach Hotel', 1),
(5, 'Luxury Hotel', 1),
(7, 'Extended Stay Hotels', 1),
(8, 'Suite Hotels', 1),
(9, 'Resort Hotels', 1),
(10, 'Casino Hotels', 1),
(11, 'Conference and Convention hotel', 1),
(12, 'Business Hotel', 1),
(13, 'Serviced Apartments', 1),
(14, 'Wellness & SPA Hotel', 1),
(15, 'DD hotels', 1);

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE IF NOT EXISTS `quotation` (
  `quote_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `total_price` decimal(7,2) NOT NULL,
  `country_id` int(11) NOT NULL,
  `cities` text NOT NULL,
  `approval_status` tinyint(1) NOT NULL,
  `ceo_approval` tinyint(1) NOT NULL,
  `client_approval` tinyint(1) NOT NULL,
  `quote_ref` text,
  `proposal` text,
  `quote_date` datetime NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`quote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

CREATE TABLE IF NOT EXISTS `quote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_variance_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city` varchar(200) NOT NULL,
  `occupancy` int(11) NOT NULL,
  `no_of_rooms` int(11) NOT NULL,
  `child` int(11) DEFAULT NULL,
  `occ_per_room` int(11) DEFAULT NULL,
  `extra_adult` int(11) DEFAULT NULL,
  `extra_child` int(11) DEFAULT NULL,
  `price` decimal(6,2) NOT NULL,
  `quote_date` datetime NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quote_variance`
--

CREATE TABLE IF NOT EXISTS `quote_variance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_quote_id` int(11) NOT NULL,
  `property_types` int(11) NOT NULL,
  `quote_title` varchar(500) NOT NULL,
  `approval_status` tinyint(1) NOT NULL,
  `ceo_approval` tinyint(1) NOT NULL,
  `comments` text NOT NULL,
  `created_on` datetime NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `renovation_types`
--

CREATE TABLE IF NOT EXISTS `renovation_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `renovation_type` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `renovation_types`
--

INSERT INTO `renovation_types` (`id`, `renovation_type`) VALUES
(1, 'Electricity'),
(2, 'Interior'),
(3, 'Plumbing');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `access_level_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `title`, `access_level_id`) VALUES
(2, 'Manager', 1),
(3, 'Team Lead', 3),
(5, 'Guest User', 4),
(6, 'Dep Manager', 2);

-- --------------------------------------------------------

--
-- Table structure for table `room_rate_management`
--

CREATE TABLE IF NOT EXISTS `room_rate_management` (
  `rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `travel_type_id` int(11) DEFAULT NULL,
  `occupancy` int(11) DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `currency` varchar(10) NOT NULL,
  `extra_child` int(11) DEFAULT NULL,
  `extra_adult` int(11) DEFAULT NULL,
  `meal_plan` int(11) DEFAULT NULL,
  `number_of_rooms` int(11) DEFAULT NULL,
  `from_date` datetime DEFAULT NULL,
  `to_date` datetime DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`rate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `star_ratings`
--

CREATE TABLE IF NOT EXISTS `star_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `star_ratings`
--

INSERT INTO `star_ratings` (`id`, `title`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `sub_companies`
--

CREATE TABLE IF NOT EXISTS `sub_companies` (
  `sub_company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `company_name` varchar(500) NOT NULL,
  `address` varchar(500) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `post_code` varchar(20) NOT NULL,
  `email` varchar(300) DEFAULT NULL,
  `website` varchar(600) DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sub_company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `travel_types`
--

CREATE TABLE IF NOT EXISTS `travel_types` (
  `travel_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `travel_type_title` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`travel_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `travel_types`
--

INSERT INTO `travel_types` (`travel_type_id`, `travel_type_title`, `status`) VALUES
(1, 'Market', 1),
(2, 'Company', 1),
(3, 'B2b1237868gk56869', 1),
(4, 'B2C s', 1),
(5, 'FIT', 1),
(6, 'GIT', 1),
(7, 'MICE', 1),
(8, 'dsfs dsg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_authorization`
--

CREATE TABLE IF NOT EXISTS `user_authorization` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `access_level_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`access_level_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`access_level_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `user_authorization`
--

INSERT INTO `user_authorization` (`id`, `user_id`, `access_level_id`) VALUES
(48, 1, 1),
(76, 2, 3),
(75, 9, 1),
(61, 11, 4),
(62, 12, 4),
(74, 13, 3),
(80, 14, 2),
(72, 15, 4),
(77, 16, 4);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `country_cities`
--
ALTER TABLE `country_cities`
  ADD CONSTRAINT `countrytable-idfk` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `hotel_bank_accounts`
--
ALTER TABLE `hotel_bank_accounts`
  ADD CONSTRAINT `fk_hotel_bank_accounts_hotels` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`hotel_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_authorization`
--
ALTER TABLE `user_authorization`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`access_level_id`) REFERENCES `access_levels` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `dorak_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
